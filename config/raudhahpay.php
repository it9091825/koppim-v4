<?php

return [
    'api_key' => env('RAUDHAH_API_KEY'),
    'signature_key' => env('RAUDHAH_X_SIGNATURE', null),
    'is_sandbox' => env('RAUDHAH_SANDBOX', env('APP_ENV') != 'production'),
    'collection_code' => env('RAUDHAH_COLLECTION_CODE', ''),
];
