<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/cams_statistic','Api\TransaksiBayaran@fetch')->name('api.transaction.statistic');

Route::name('api.')->group(function() {
    Route::get('/charts_statistic', 'Api\ChartsData@fetch')->name('charts.statistic');
    Route::get('/charts_statistic/sub', 'Api\ChartsData@subfetch')->name('charts.substatistic');
});

Route::get('states','Api\StatesController@index');
Route::get('/state/parliaments','Api\ParliamentsController@byState');


