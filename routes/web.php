<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */


use Maatwebsite\Excel\Facades\Excel;
use App\Exports\Dividen\DividenExport;
use Illuminate\Support\Facades\Artisan;
use App\Http\Controllers\Admin\Dividen\DividenController;

Route::view('503', 'errors.test');

$investor = function () {

    Route::get('/daftar-dan-langgan', 'Investor\InvestorController@signup')->name('langganan-syer');

    Route::get('/Daftar', 'Investor\InvestorController@signup')->name('langganan-syer');
    Route::get('/daftar', 'Investor\InvestorController@signup')->name('langganan-syer');
    Route::get('/langganan-syer', 'Investor\InvestorController@signup')->name('langganan-syer');
    Route::get('/affiliate', 'Investor\InvestorController@signup');
    Route::post('/langganan-syer', 'Investor\InvestorController@create_order')->name('langganan-syer-exe');
    Route::get('/langganan-syer/pengesahan', 'Investor\InvestorController@verify')->name('langganan-syer-pengesahan');
    // semak keanggotaaan
    Route::get('/keanggotaan/cari', 'Investor\SemakKeanggotaanController@findUser')->name('member.getuser');
    Route::get('/keanggotaan/semak', 'Investor\SemakKeanggotaanController@index')->name('member.verify');


    //Pembayaran manual
    Route::get('/langganan-syer/pembayaran-manual', 'Investor\InvestorController@buy_manual')->name('langganan-syer-manual');
    Route::post('/langganan-syer/pembayaran-manual', 'Investor\InvestorController@buy_manual_exe');

    Route::get('/langganan-syer/terima-kasih', 'SenangPayController@thankyou');


    Route::get('/hantar-bukti-pembayaran', 'Investor\InvestorController@paymentproof')->name('hantar-bukti-pembayaran');
    Route::post('/hantar-bukti-pembayaran', 'Investor\InvestorController@paymentproofexe')->middleware('optimizeImages');
    //

    Route::get('/login', 'Investor\InvestorController@index')->name('login');
    Route::get('/logmasuk', 'Investor\InvestorController@index')->name('login');
    Route::get('/LogMasuk', 'Investor\InvestorController@index')->name('login');
    Route::get('/', 'Investor\InvestorController@index')->name('login-root');
    Route::get('/logout', 'Investor\InvestorController@logout')->name('logout');
    // LOGIN FOR ANGGOTA
    Route::post('/auth/login/anggota', 'Investor\InvestorController@login_by_email_exe')->name('login-exe');
    Route::get('/forgot-password', 'Investor\InvestorController@forgot_password')->name('login');

    Route::get('/ikoop-migration', 'Investor\InvestorController@ikoopfirsttime')->name('ikoopfirsttime');
    Route::post('/ikoop-migration', 'Investor\InvestorController@ikoopfirsttimeexe');

    Route::get('/ikoop-migration/verification', 'Investor\InvestorController@ikoopfirsttime_2');

    Route::get('/ikoop-migration/verification/otp', 'Investor\InvestorController@ikoopfirsttime_3')->name('ikoopfirsttime3');

    Route::get('/ikoop-migration/verification/otp/send', 'Investor\InvestorController@sendOtp')->name('ikoopfirsttime3');

    Route::post('/ikoop-migration/verification/otp/submit', 'Investor\InvestorController@submitOTP')->name('ikoopfirsttime4');

    Route::get('/ikoop-migration/verification/thank-you', 'Investor\InvestorController@ikoopfirsttimethankyou')->name('ikoopfirsttimethankyou');

    Route::get('/email-activation/{token}', 'Investor\InvestorController@email_activation');

    Route::get('/dashboard', 'Investor\InvestorLoggedController@home')->name('dashboard');
    Route::get('/home', 'Investor\InvestorLoggedController@home')->name('dashboard');

    Route::get('/profile', 'Investor\InvestorLoggedController@edit_org_2');
    Route::get('/profile2', 'Investor\InvestorLoggedController@edit_org_2');

    Route::get('/syer', 'Investor\InvestorLoggedController@invest');
    Route::get('/syer/{id}', 'Investor\InvestorLoggedController@invest_detail');

    Route::get('/statement', 'Investor\InvestorLoggedController@statement');

    //DOWNLOAD PDF FOR INVESTOR
    Route::get('download_public', 'Investor\PdfController@download_public');

    Route::get('/linkstorage', function () {
        Artisan::call('storage:link');
    });

	if (env('ENABLE_DIVIDEN_MODULE', true)) {

	Route::get('/dividen', 'Investor\DividenController@index');

	Route::get('/dividen/download', 'Investor\DividenController@dividen_statement_download');


	Route::get('/dividen/withdraw', 'Investor\DividenController@withdraw');
	Route::post('/dividen/withdraw', 'Investor\DividenController@withdraw_exe');
	}


    Route::get('/change-password', 'Investor\InvestorLoggedController@changepassword');
    Route::post('/change-password', 'Investor\InvestorLoggedController@changePasswordExe');

    Route::get('/investor/edit/resend-email', 'Investor\InvestorLoggedController@activation_email');


    if (env('ENABLE_AFFILIATES', false)) {
        Route::get('/affiliates', 'Investor\AffiliatesController@invest');
        Route::post('/affiliates/withdraw', 'Investor\AffiliatesController@withdrawal');
        Route::get('/affiliates/penyata-komisen', 'Investor\AffiliatesController@getStatement');
        Route::get('/affiliates/penyata-komisen/cetak/{id}', 'Investor\AffiliatesController@printStatement');
        //        Route::get('/affiliates/withdraw', 'Investor\AffiliatesController@affliate_withdraw')->name('index');
        //        Route::post('/affiliates/withdraw', 'Investor\AffiliatesController@affliate_withdraw_exe')->name('index');
        Route::post('/affiliates/apply', 'Investor\AffiliatesController@apply_affiliate');
    }

    Route::view('terma-syarat', 'terma-syarat');
    Route::view('success-proof-payment-upload', 'investor.after-upload-proof-payment');

    Route::get('/dapatkan-kata-laluan', 'Auth\ResetPasswordController@showResetForm');
    Route::post('/kata-laluan/sahkan-kad-pengenalan', 'Auth\ResetPasswordController@verifyIc');
    Route::get('/kata-laluan/sahkan-no-telefon', 'Auth\ResetPasswordController@verifyMobileNumber');
    Route::any('/kata-laluan/hantar-kata-laluan-sementara', 'Auth\ResetPasswordController@sendTemporaryPassword');

    Route::get('/set-kata-laluan-baru', 'Investor\InvestorLoggedController@resetPassword');
    Route::post('/set-kata-laluan-baru', 'Investor\InvestorLoggedController@resetPasswordExe');

    // Permohonan Berhenti
    Route::get('/permohonan-berhenti', 'Investor\InvestorLoggedController@terminate_membership');
    Route::post('/permohonan-berhenti', 'Investor\InvestorLoggedController@terminate_membership_submit');
    Route::get('/permohonan-aktif', 'Investor\InvestorLoggedController@reactivate_membership');
    Route::post('/permohonan-aktif', 'Investor\InvestorLoggedController@reactivate_membership_submit');
    // Buy Share
    Route::prefix('langgan-syer')->name('share.')->namespace('Investor')->group(function () {
        Route::get('', 'BuyShareController@buy')->name('buy');
        Route::post('', 'BuyShareController@store_buy')->name('buy');
        Route::get('pengesahan', 'BuyShareController@verify')->name('verify');
    });

    //SenangPay: Do not use auth middleware
    Route::post('/payment-gateway/senang-pay/submit', 'SenangPayController@submit')->name('senang-pay-submit');
    Route::post('/payment-gateway/senang-pay/submit-recurring', 'SenangPayController@submitRecurring')->name('senang-pay-submit-recurring');
    Route::get('/pembelian-share/terima-kasih-s', 'SenangPayController@result'); //temp route
    Route::get('/payment-gateway/senang-pay/result', 'SenangPayController@result');
    Route::any('/payment-gateway/call-backend/senang-pay', 'SenangPayController@callback')->name('senang-pay-callback');

    //Profile
    Route::middleware(['auth'])->prefix('investor')->name('investor.')->namespace('Investor')->group(function () {
        Route::post('edit/info', 'ProfileController@editInfoPemohon');
        Route::post('edit/attachment', 'ProfileController@editInfoMykad');
        Route::post('edit/address', 'ProfileController@editAlamatPemohon');
        Route::post('edit/contact', 'ProfileController@editKontak');
        Route::post('edit/jawatan', 'ProfileController@editPekerjaan');
        Route::post('edit/bank', 'ProfileController@editBank');
        Route::post('edit/waris', 'ProfileController@editWaris');
        Route::post('profile/submit', 'ProfileController@submit')->name('profile.submit');
    });

    Route::group(['middleware' => 'auth', 'prefix' => 'transaksi', 'namespace' => 'Investor\Transactions'], function () {

        Route::group(['prefix' => 'log'], function () {
            Route::get('/', 'LogsController@index')->name('investor.transactions.logs.index');
        });
    });
};

$admin = function () {

    //temp solution for when admin close browser, then open back and go to admin route while still logged in, the system
    //will redirect to /home. Need to fix this later
    Route::get('/home', function () {
        return redirect('admin/dashboard');
    });

    //SenangPay, no auth
    Route::any('/payment-gateway/call-backend/senang-pay', 'SenangPayController@callback')->name('senang-pay-callback');

    //Members
    Route::middleware(['auth'])->prefix('admin')->name('admin.')->namespace('Admin')->group(function () {
        Route::get('members/dashboard', 'MemberController@viewDashboard')->name('member.dashboard');
        Route::get('members', 'MemberController@index')->name('member.list');
        Route::get('members/new', 'Members\NewApplicantsController@index')->name('member.new');
        Route::get('members/ikoop', 'Members\IkoopApplicantsController@index')->name('member.ikoop');
        Route::get('members/reviewed', 'Members\ReviewedApplicantsController@index')->name('member.reviewed');
        Route::get('members/active', 'Members\ActiveApplicantsController@index')->name('member.active');
        Route::get('members/no-mykad', 'Members\MykadNotExistsController@index')->name('member.no-mykad');
        Route::get('members/create', 'MemberController@create')->name('member.create');
        Route::post('members/create', 'MemberController@store')->name('member.store')->middleware('optimizeImages');
        Route::get('members/view/{user_id}', 'MemberController@view')->name('member.view');
        Route::post('members/edit/{user_id}/detail', 'MemberController@edit_detail')->name('member.edit.detail');
        Route::post('members/edit/{user_id}/account', 'MemberController@edit_account')->name('member.edit.account');
        Route::post('members/edit/{user_id}/password', 'MemberController@edit_password')->name('member.edit.password');
        Route::post('members/edit/{user_id}/attachment', 'MemberController@edit_attachment')->name('member.edit.attachment');
        Route::post('members/edit/{user_id}/bank', 'MemberController@edit_bank')->name('member.edit.bank');
        Route::post('members/edit/{user_id}/status', 'MemberController@edit_status')->name('member.edit.status');
        Route::get('members/terminations', 'TerminationController@index')->name('member.termination');
        Route::post('members/terminations/approve', 'TerminationController@approve_tmt')->name('member.termination.send');
        Route::post('members/terminations/revoke', 'TerminationController@revoke_tmt')->name('member.termination.revoke');
        Route::get('members/pending-update', 'Members\PendingApplicantsController@index')->name('member.pending_update');
        Route::post('members/send-reminder', 'Members\PendingApplicantsController@sendReminder')->name('member.send_reminder');
        Route::post('members/archive/{user}', 'Members\ArchiveMemberController@store')->name('member.archive');
        Route::post('members/change-status-untuk-disemak/{user}', 'MemberController@changeStatusUntukDisemak')->name('member.change.status.untuk.disemak');

        //dihantar untuk kelulusan
        Route::post('members/membership/send-for-approval', 'Members\SendApplicantForApprovalController@store')->name('membership.approval.send');
        // approving

        Route::post('members/membership/approve', 'Members\ApproveReviewedMembersController@store')->name('membership.approve');

        Route::get('my-profile', 'MyProfileController@index')->name('my-profile');





    });


    //Admin
    Route::get('/', 'Admin\AdminController@index')->name('login-e');
    Route::get('/admin/login', 'Admin\AdminController@index')->name('login');
    Route::get('/admin/logout', 'Admin\AdminLoggedController@logout')->name('logout');
    Route::post('/auth/login/exe', 'Admin\AdminController@login_by_email_exe')->name('login-exe');
    Route::get('/admin/dashboard', 'Admin\AdminLoggedController@home')->name('admin.dashboard');


    //Transaction success
    Route::get('/admin/tx', 'Admin\AdminLoggedController@txs')->name('admin.transaction.online.index');

    Route::get('admin/tx/print/{id}', 'Admin\TransactionManualController@print')->name('admin.tx.print');

    Route::get('/admin/tx/verified', 'Admin\VerifiedTransactionsController@index')->name('admin.transaction.verified.index');

    // API

    Route::get('payment/details/{id}', 'Admin\Payments\FetchDetailsController@show')->name('payment.details.show');

    //Transaction manual
    Route::middleware(['auth'])->prefix('admin/tx/approval')->name('admin.tx.approval.')->namespace('Admin')->group(function () {
        Route::get('/', 'TransactionApprovalController@index')->name('index');
        Route::post('/luluskan', 'TransactionApprovalController@luluskan')->name('luluskan');
        Route::post('/batalkan', 'TransactionApprovalController@batalkan')->name('batalkan');
        Route::post('/hantar-kembali', 'TransactionApprovalController@hantarKembali')->name('return.back');
    });

    Route::middleware(['auth'])->prefix('admin/tx/manual')->name('admin.tx.manual.')->namespace('Admin')->group(function () {
        Route::get('/', 'TransactionManualController@txs_manual')->name('index');
        Route::get('/data/{id}', 'TransactionManualController@getData')->name('getData');
        Route::post('/sahkan', 'TransactionManualController@sahkan')->name('sahkan');
        Route::post('/batalkan', 'TransactionManualController@batalkan')->name('batalkan');
        Route::post('/simpan', 'TransactionManualController@simpan')->name('simpan');

        Route::post('/simpan-transaksi', 'TransactionManualController@storeTransaction')->name('simpan-transaksi')->middleware('optimizeImages');
        Route::get('kemaskini-transaksi/{id}', 'TransactionManualController@editTransactionPage')->name('kemaskini-transaksi-page');
        Route::post('kemaskini-transaksi', 'TransactionManualController@editTransaction')->name('kemaskini-transaksi')->middleware('optimizeImages');
        Route::post('/batal-transaksi', 'TransactionManualController@cancelTransaction')->name('batal-transaksi');
    });

    Route::middleware(['auth'])->prefix('admin/transaksi-bayaran/untuk-semakan-lanjut')->name('admin.transaksi-bayaran.untuk-semakan-lanjut.')->namespace('Admin')->group(function () {
        Route::get('/', 'TransactionReviewController@index')->name('index');
        Route::post('/', 'TransactionReviewController@submit')->name('untuk-disahkan');
    });

    Route::middleware(['auth'])->prefix('admin/transaksi-bayaran/telah-dibatalkan')->name('admin.transaksi-bayaran.telah-dibatalkan.')->namespace('Admin')->group(function () {
        Route::get('/', 'TransactionCancelController@index')->name('index');
    });

    Route::middleware(['auth'])->prefix('admin/tx/manual/bukti-bayaran')->name('admin.tx.manual.proof.')->namespace('Admin')->group(function () {
        Route::get('edit/{id}', 'Payments\ProofController@edit')->name('edit');
    });

    //Maintenance
    Route::get('/admin/maintenance', 'Admin\MaintenanceModeController@index');
    Route::post('/admin/maintenance/store', 'Admin\MaintenanceModeController@store');
    Route::post('/admin/maintenance/destroy', 'Admin\MaintenanceModeController@destroy');

    //Message
    Route::get('/admin/mesej', 'Admin\MessagesController@index')->name('admin.mesej.index');







    // Roles & Permissions
    Route::group(['middleware' => 'auth', 'prefix' => 'admin/roles'], function () {

        Route::get('/', 'Admin\Roles\RolesController@index')->name('roles.index');
        Route::post('store', 'Admin\Roles\RolesController@store')->name('roles.store');
        Route::get('edit/{id}', 'Admin\Roles\RolesController@edit')->name('roles.edit');
        Route::post('update/{id}', 'Admin\Roles\RolesController@update')->name('roles.update');
        Route::delete('destroy/{id}', 'Admin\Roles\RolesController@destroy')->name('roles.destroy');
        Route::post('permission/store/{role_id}', 'Admin\Permissions\AttachPermissionToRoleController')->name('roles.permission.store');
    });

    Route::group(['middleware' => 'auth', 'prefix' => 'admin/permissions'], function () {

        Route::get('/', 'Admin\Permissions\PermissionsController@index')->name('permissions.index');
        Route::post('store', 'Admin\Permissions\PermissionsController@store')->name('permissions.store');
        Route::get('edit/{id}', 'Admin\Permissions\PermissionsController@edit')->name('permissions.edit');
        Route::post('update/{id}', 'Admin\Permissions\PermissionsController@update')->name('permissions.update');
        Route::delete('destroy/{id}', 'Admin\Permissions\PermissionsController@destroy')->name('permissions.destroy');
        Route::post('user/{user_id}', 'Admin\Permissions\AttachPermissionToUserController')->name('users.permission.store');
    });

    //administrators
    Route::group(['middleware' => 'auth', 'prefix' => 'admin/administrators', 'as' => 'administrators.'], function () {

        Route::get('', 'Admin\AdministratorsController@index')->name('index');
        Route::post('store', 'Admin\AdministratorsController@store')->name('store');
        Route::get('edit/{id}', 'Admin\AdministratorsController@edit')->name('edit');
        Route::post('update/{id}', 'Admin\AdministratorsController@update')->name('update');
        Route::delete('destroy/{id}', 'Admin\AdministratorsController@destroy')->name('destroy');
    });

    //announcements
    Route::group(['middleware' => 'auth', 'prefix' => 'admin/announcements', 'as' => 'announcements.'], function () {

        Route::get('/', 'Admin\AnnouncementsController@index')->name('index');
        Route::post('/store', 'Admin\AnnouncementsController@store')->name('store');
        Route::get('edit/{id}', 'Admin\AnnouncementsController@edit')->name('edit');
        Route::post('update/{id}', 'Admin\AnnouncementsController@update')->name('update');
        Route::delete('/destroy/{id}', 'Admin\AnnouncementsController@destroy')->name('destroy');
    });

    //activity log
    Route::group(['middleware' => 'auth', 'prefix' => 'admin/logs', 'as' => 'logs.'], function () {
        Route::get('/', 'Admin\Logs\ActivityController@index')->name('index');
    });

    Route::group(['middleware' => 'auth', 'prefix' => 'admin/sms', 'as' => 'sms.'], function () {

        Route::get('/', 'Admin\Notice\SmsController@index')->name('index');
        Route::post('/store', 'Admin\Notice\SmsController@store')->name('store');
        Route::get('edit/{id}', 'Admin\Notice\SmsController@edit')->name('edit');
        Route::post('update/{id}', 'Admin\Notice\SmsController@update')->name('update');
        Route::delete('/destroy/{id}', 'Admin\Notice\SmsController@destroy')->name('destroy');
        Route::get('/find/user', 'Admin\Notice\SmsController@find')->name('find.user');
    });



    Route::group(['middleware' => 'auth', 'prefix' => 'admin/reports', 'as' => 'report.'], function () {

        Route::get('/', 'Admin\ReportsController@index')->name('index');
        Route::post('getResult', 'Admin\ReportsController@getResult')->name('getResult');
        Route::get('reports/export', 'Admin\ReportsController@export')->name('export');
        Route::get('summary', 'Admin\ReportsController@showSummaryPage')->name('summary.index');
        Route::get('/byparliament', 'Admin\KeanggotaanReportsController@byParliament');
        Route::get('/bygender', 'Admin\KeanggotaanReportsController@byGender');
        Route::get('/byrace', 'Admin\KeanggotaanReportsController@byRace');
        Route::get('/byreligion', 'Admin\KeanggotaanReportsController@byReligion');
        Route::get('/byincome', 'Admin\KeanggotaanReportsController@byIncome');
        Route::get('/byage', 'Admin\KeanggotaanReportsController@byAge');
        Route::get('/bystate', 'Admin\KeanggotaanReportsController@byState');
        Route::get('/byregistrationstatus', 'Admin\KeanggotaanReportsController@byPendaftaranStatus')->name('summary.byregistrationstatus');
        Route::get('/no-mykad', 'Admin\KeanggotaanReportsController@mykadNotExists');
        Route::get('/byinfokoppim', 'Admin\KeanggotaanReportsController@byInfoKoppim')->name('summary.byinfokoppim');
        Route::get('/auditview', 'Admin\ReportsController@auditViewPage')->name('audit.index');
        Route::post('/auditexport', 'Admin\ReportsController@auditExport')->name('audit');

        // Route::get('/no-mykad', 'Admin\KeanggotaanReportsController@mykadNotExists');
    });

    //Module Affiliates
    if (env('ENABLE_AFFILIATES', false)) {
        Route::group(['middleware' => 'auth', 'prefix' => 'admin/affiliates', 'as' => 'affiliates.'], function () {
            //Utama
            // Route::get('/utama', 'Admin\Affiliates\DashboardController@index')->name('index');
            //Permohonan
            Route::get('/senarai-permohonan', 'Admin\Affiliates\NewApplicationController@index');
            Route::post('/senarai-permohonan/kelulusan', 'Admin\Affiliates\NewApplicationController@kelulusan')->name('application.result');
            Route::post('/senarai-permohonan/lulus-semua', 'Admin\Affiliates\NewApplicationController@lulusSemua')->name('application.approveAll');
            Route::post('/senarai-permohonan/batal-semua', 'Admin\Affiliates\NewApplicationController@batalSemua')->name('application.cancelAll');
            //Rekod Komisen
            Route::get('/transaksi', 'Admin\Affiliates\ApprovalController@for_approval');
            Route::get('/ejen/{no_koppim}/senarai-komisen', 'Admin\Affiliates\ApprovalController@view_commissions')->name('view_commissions');
            Route::get('/ejen/{user_id}/senarai-penyata-komisen', 'Admin\Affiliates\CommissionStatementController@index')->name('view_statements');
            Route::get('/ejen/{user_id}/senarai-penyata-komisen/cetak/{_id}', 'Admin\Affiliates\CommissionStatementController@print');
            //Pengeluaran
            Route::get('/pengeluaran', 'Admin\Affiliates\WithdrawController@index');
            Route::get('/pengeluaran/terperinci', 'Admin\Affiliates\WithdrawController@detail');
            Route::post('/pengeluaran/terperinci', 'Admin\Affiliates\WithdrawController@approval');
            //Jana Pengeluaran Seperti Dalam Cronjob
            Route::get('/pengeluaran/jana-pengeluaran', 'Admin\Affiliates\WithdrawController@processCommissionForWithdrawal');
            //Export data pengeluaran
            Route::get('/pengeluaran/export', 'Admin\Affiliates\WithdrawController@export');
            //Tetapan Affiliate
            Route::group(['prefix' => 'settings', 'as' => 'settings.'], function () {
                Route::get('/', 'Admin\Affiliates\Settings\SettingsController@index')->name('index');
                Route::post('/store-general', 'Admin\Affiliates\Settings\SettingsController@storeGeneral')->name('store.general');
                Route::post('/store-agent', 'Admin\Affiliates\Settings\SettingsController@storeAgent')->name('store.agent');
                Route::post('/edit-agent', 'Admin\Affiliates\Settings\SettingsController@editAgent')->name('edit.agent');
                Route::get('/find/user', 'Admin\Affiliates\Settings\SettingsController@find')->name('find.user');
            });


            //Tak Pakai
//            Route::get('/senarai-semua', 'Admin\Affiliates\AllApplicationController@index');
//            Route::get('/transaksi/approval', 'Investor\AffiliatesController@admin_approve_transaction')->name('index');
//            Route::get('/transaksi/lulus', 'Investor\AffiliatesController@admin_transaction_lulus')->name('index');
//            Route::get('/transaksi/semakan', 'Investor\AffiliatesController@admin_transaction_semakan')->name('index');
//            Route::get('/transaksi/batal', 'Investor\AffiliatesController@admin_transaction_batal')->name('index');
//            Route::get('/transaksi/pengeluaran', 'Investor\AffiliatesController@admin_transaction_pengeluaran')->name('index');
//            Route::get('/transaksi/pengeluaran/terperinci', 'Investor\AffiliatesController@admin_transaction_pengeluaran_detail')->name('index');
//            Route::post('/transaksi/pengeluaran/terperinci', 'Investor\AffiliatesController@admin_transaction_pengeluaran_detail_exe')->name('index');
//            Route::post('/transaksi/pengeluaran/terperinci/refund', 'Investor\AffiliatesController@admin_transaction_pengeluaran_refund_exe')->name('index');
//            Route::post('/transaksi/pengeluaran/terperinci/semak', 'Investor\AffiliatesController@admin_transaction_pengeluaran_semak_exe')->name('index');
//            Route::get('/transaksi/pengeluaran-diluluskan', 'Investor\AffiliatesController@admin_transaction_pengeluaran_diluluskan')->name('index');
//            Route::get('/transaksi/pengeluaran-refund', 'Investor\AffiliatesController@admin_transaction_pengeluaran_refund')->name('index');
//            Route::get('/transaksi/pengeluaran-semakan', 'Investor\AffiliatesController@admin_transaction_pengeluaran_semakan')->name('index');
        });
    }

    Route::group(['middleware' => 'auth', 'prefix' => 'admin/migrations', 'as' => 'migrations.'], function () {
        Route::get('/duplicate-users', 'Admin\Migrations\DuplicateUsers@index');
    });


    if (env('ENABLE_LETTER_MODULE', false)) {
        Route::group(['middleware' => 'auth', 'prefix' => 'admin/letter', 'as' => 'letter.'], function () {
            Route::get('/utama', 'Admin\Letter\LetterController@index')->name('index');
            Route::get('/create', 'Admin\Letter\LetterController@create')->name('index');
            Route::post('/create', 'Admin\Letter\LetterController@create_exe')->name('create-exe');

            Route::get('/edit', 'Admin\Letter\LetterController@edit')->name('edit');
            Route::post('/edit', 'Admin\Letter\LetterController@edit_exe')->name('edit-exe');

            Route::get('/preview', 'Admin\Letter\LetterController@preview')->name('index');
            Route::get('/preview/user', 'Admin\Letter\LetterController@preview_user_letter')->name('index');

            Route::post('/preview/email-sample', 'Admin\Letter\LetterController@send_email_sample')->name('edit-exe');

            Route::get('/send', 'Admin\Letter\LetterController@send')->name('send');
            Route::post('/send/user', 'Admin\Letter\LetterController@send_letter_to_user')->name('send-exe');

            Route::post('/send/group', 'Admin\Letter\LetterController@send_letter_to_group')->name('send-group-exe');

            Route::get('/user', 'Admin\Letter\LetterController@get_all_user_letter_ajax')->name('send');

            Route::get('/jobs', 'Admin\Letter\LetterController@job_list')->name('send');
			Route::get('/job/detail', 'Admin\Letter\LetterController@job_detail')->name('job_detail');

        });

		 Route::get('/admin/letter/cron/jobs', 'Admin\Letter\LetterController@cron_job')->name('cron-letter-job');

    }


    if (env('ENABLE_DIVIDEN_MODULE', true)) {
        Route::group(['middleware' => 'auth', 'prefix' => 'admin/dividen', 'as' => 'dividen.'], function () {
            Route::get('/utama', 'Admin\Dividen\DividenController@index')->name('index');

            Route::get('/create', 'Admin\Dividen\DividenController@create')->name('create');
            Route::post('/create', 'Admin\Dividen\DividenController@dividencreate')->name('dividen-create');

            //csv-detail-dividen
            Route::get('/detail/csvdividen/{id}', 'Admin\Dividen\DividenController@exportCsv');

            Route::get('/detail/{id}', 'Admin\Dividen\DividenController@detail')->name('dividen-detail');

            Route::get('/detail/{id}/calculate', 'Admin\Dividen\DividenController@calculate_dividen_exe')->name('dividen-calculation');

            Route::get('/detail/{id}/publish', 'Admin\Dividen\DividenController@publish_in_progress')->name('dividen-publish');


            Route::get('/withdraw', 'Admin\Dividen\DividenController@withdraw')->name('withdraw');
            Route::get('/withdraw/export', 'Admin\Dividen\DividenController@export')->name('withdraw');


            Route::get('/withdraw/{id}', 'Admin\Dividen\DividenController@withdraw_detail')->name('withdraw');
            Route::post('/withdraw/{id}', 'Admin\Dividen\DividenController@approval')->name('withdraw-exe');


        });

        Route::get('/admin/dividen/cron/worker1', 'Admin\Dividen\DividenController@calculate_cron')->name('cron-job-dividen');
        Route::get('/admin/dividen/cron/worker2', 'Admin\Dividen\DividenController@calculate_2_cron')->name('cron-job-dividen');
        Route::get('/admin/dividen/cron/worker3', 'Admin\Dividen\DividenController@publish_cron_2')->name('cron-job-dividen');
    }


};

Route::group(array('domain' => env('INVESTOR_DOMAIN')), $investor);
Route::group(array('domain' => env('ADMIN_INVESTOR_DOMAIN')), $admin);


