# KoPPIM V3

## Installation
1. Git Clone This Repo
2. ```composer install```
3. ```php artisan migrate```
4. ```php artisan db:seed```
5. ```cp .env.example .env```
6. Set credentials in .env
7. Set folder and file permission
8. Setup supervisor for queues

## Server Requirement
1. Install PHP7.2
2. Install PHP Mongodb extension
3. Install linux package below for image optimizer
- ```sudo apt-get install jpegoptim```
- ```sudo apt-get install optipng```
- ```sudo apt-get install pngquant```
- ```sudo npm install -g svgo```
- ```sudo apt-get install gifsicle```
- ```sudo apt-get install webp```


## Letter Module Requirement
1. composer require barryvdh/laravel-dompdf or Composer Update
2. cron job URL for wan or ather method (Buat cron setiap 3 Minit)
https://[URL]/admin/letter/cron/jobs



## Dividen Requirement
1. set cronjob https://[ADMIN-URL]/admin/dividen/cron/worker1 // 10 minutes 
2. set cronjob https://[ADMIN-URL]/admin/dividen/cron/worker2 // 10 minutes 
3. set cronjob https://[ADMIN-URL]/admin/dividen/cron/worker3 // 10 minutes 