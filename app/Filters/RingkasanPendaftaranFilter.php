<?php

namespace App\Filters;

use App\Concerns\Filter;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Schema;

class RingkasanPendaftaranFilter extends Filter
{

    public function year(string $value = null): Builder
    {
        return $this->builder->whereYear('created_at', $value);
    }
}
