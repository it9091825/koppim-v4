<?php

namespace App\Mail\User;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AddShare extends Mailable
{
    use Queueable, SerializesModels;

    public $name;
    public $ref_no;
    public $transaction_date;
    public $amount;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name,$ref_no,$transaction_date, $amount)
    {
        $this->name = $name;
        $this->ref_no = $ref_no;
        $this->transaction_date = $transaction_date;
        $this->amount = $amount;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->subject('[KoPPIM] Penerimaan Tambahan Langganan Syer Baharu')->markdown('email.user.add-share');
    }
}
