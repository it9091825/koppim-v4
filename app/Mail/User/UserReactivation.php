<?php

namespace App\Mail\User;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserReactivation extends Mailable
{
    use Queueable, SerializesModels;

    public $name;
    public $ic;
    public $mobile_no;
    public $email;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name,$ic,$mobile_no,$email)
    {
        $this->name = $name;
        $this->ic = $ic;
        $this->mobile_no = $mobile_no;
        $this->email = $email;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
        ->subject('[KoPPIM] Pengaktifan Semula Keanggotaan')->markdown('email.user.reactivate-registration');
    }
}

