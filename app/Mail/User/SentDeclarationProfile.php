<?php

namespace App\Mail\User;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SentDeclarationProfile extends Mailable
{
    use Queueable, SerializesModels;
    
    public $name;
    public $ic;
    public $registration_date;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name, $ic, $registration_date)
    {
        $this->name = $name;
        $this->ic = $ic;
        $this->registration_date = $registration_date;
                
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
        ->subject('[KoPPIM] Penerimaan Maklumat Permohonan Pendaftaran Anggota')
        ->markdown('email.user.send-declaration-profile');
    }
}
