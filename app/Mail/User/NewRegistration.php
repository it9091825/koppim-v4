<?php

namespace App\Mail\User;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewRegistration extends Mailable
{
    use Queueable, SerializesModels;

    public $name;
    public $ic;
    public $email;
    public $mobile_no;
    public $no_koppim;
    public $user_created_at;
    public $share;
    public $additionalShare;
    public $totalAmount;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name,$ic,$mobile_no,$email,$user_created_at,$share,$additionalShare,$totalAmount)
    {
        $this->name = $name;
        $this->ic = $ic;
        $this->mobile_no = $mobile_no;
        $this->email = $email;
        $this->user_created_at = $user_created_at;
        $this->share = $share;
        $this->additionalShare = $additionalShare;
        $this->totalAmount = $totalAmount;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
        ->subject('[KoPPIM] Penerimaan Permohonan Pendaftaran Anggota Baharu')->markdown('email.user.new-registration');
    }
}
