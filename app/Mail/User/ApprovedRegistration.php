<?php

namespace App\Mail\User;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ApprovedRegistration extends Mailable
{
    use Queueable, SerializesModels;

    public $name;
    public $no_koppim;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name, $no_koppim)
    {
        $this->name = $name;
        $this->no_koppim = $no_koppim;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->subject('[KoPPIM] Pengesahan Keanggotaan KoPPIM Anda')->markdown('email.user.approve-registration');
    }
}
