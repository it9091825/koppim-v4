<?php

namespace App\Mail\Password;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewPassword extends Mailable
{
    use Queueable, SerializesModels;

    public $name;
    public $ic;
    public $pin;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name,$ic,$pin)
    {
        $this->name = $name;
        $this->ic = $ic;
        $this->pin = $pin;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('[KoPPIM] Penukaran Kata Laluan Baharu')
        ->markdown('email.password.new');
    }
}
