<?php

namespace App\Mail\Payment;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SentPaymentProof extends Mailable
{
    use Queueable, SerializesModels;

    public $name;
    // public $amount;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($name)
    {
        $this->name = $name;
        // $this->amount = $amount;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('[KoPPIM] Penerimaan Bukti Pembayaran Untuk Pengesahan & Kelulusan')
        ->markdown('email.payment.sent-payment-proof',['name' => $this->name]);
    }
}
