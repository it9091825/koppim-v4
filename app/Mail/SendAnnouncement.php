<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Admin\Announcement;

class SendAnnouncement extends Mailable
{
    use Queueable, SerializesModels;

    public $announcement;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Announcement $announcement)
    {
        $this->announcement = $announcement;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('no-reply@koppim.com.my')->subject($this->announcement->title)->markdown('email.announcement');
    }
}
