<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;
use App\Mongo\Payment;
use Carbon\Carbon;

class TransaksiBayaranSenangPay extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [
        'month' => 'all',
        'year' => 'all'
    ];

    public function placeholder()
    {
        return 'Memuatkan data...';
    }

    public $reloadTimeout = 120;

    public $cacheTime = 2;
    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $dateObject = Carbon::createFromFormat('m/Y', $this->config['month'] . '/' . $this->config['year']);
        $start = $dateObject->startOfMonth();
        $end = $dateObject->endOfMonth();

        if ($this->config['month'] == 'all') {

            $totalFeeSenangPay = Payment::where('channel', 'online')->where('gateway', 'senangpay')
                ->where('returncode', '100')->sum('first_time_fees');
            $totalShareSenangPay = Payment::where('channel', 'online')->where('gateway', 'senangpay')
                ->where('returncode', '100')->sum('share_amount');
        } else {


            $totalFeeSenangPay = Payment::where('channel', 'online')->where('gateway', 'senangpay')->where('returncode', '100')->where(function ($query) use ($start, $end) {
                $query->where('created_at', '>=', $start)->orWhere('created_at', '<=', $end);
            })->sum('first_time_fees');

            $totalShareSenangPay = Payment::where('channel', 'online')->where('gateway', 'senangpay')
                ->where('returncode', '100')->where(function ($query) use ($start, $end) {
                    $query->where('created_at', '>=', $start)->orWhere('created_at', '<=', $end);
                })->sum('share_amount');
        }

        $totalKeseluruhanSenangPay = $totalFeeSenangPay + $totalShareSenangPay;


        return view('widgets.transaksi_bayaran_senang_pay', [
            'totalFeeSenangPay' => $totalFeeSenangPay,
            'totalShareSenangPay' => $totalShareSenangPay,
            'totalKeseluruhanSenangPay' => $totalKeseluruhanSenangPay
        ]);
    }
}
