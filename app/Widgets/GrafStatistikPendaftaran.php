<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;
use App\User;

class GrafStatistikPendaftaran extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $users = User::where('type', 'user')->where('ikoop', 0)->orderBy('created_at')->get()->groupBy(function ($item) {
            return $item->created_at->format('d/m/Y');
        });

        return view('widgets.graf_statistik_pendaftaran', [
            'users' => $users,
        ]);
    }
}
