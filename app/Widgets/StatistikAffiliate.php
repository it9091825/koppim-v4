<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;
use App\Mongo\CommissionWithdraw;
use App\Mongo\Payment as MPayment;
use App\User;
use Carbon\Carbon;

class StatistikAffiliate extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    public function placeholder()
    {
        return 'Memuatkan data...';
    }

    public $reloadTimeout = 120;

    public $cacheTime = 2;
    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $agent_count = User::where('type', 'user')->where('is_affiliate', 'yes')->count();
        $new_agents = User::where('type', 'user')->where('is_affiliate', 'apply')->where(function ($query) {
            $query->where('created_at', '>', Carbon::today()->startOfDay())->where('created_at', '<', Carbon::today()->endOfDay());
        })->count();

        $commission_paid = CommissionWithdraw::where('status', 'paid')->sum('amount');
        $commission_all = MPayment::where('returncode', '100')->where('introducer', true)->sum('introducer_commission');

        return view('widgets.statistik_affiliate', [
            'agent_count' => $agent_count,
            'new_agents' => $new_agents,
            'commission_paid' => $commission_paid,
            'commission_all' => $commission_all,
        ]);
    }
}
