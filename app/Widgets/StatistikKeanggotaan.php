<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;
use App\User;
use Carbon\Carbon;

class StatistikKeanggotaan extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    public function placeholder()
    {
        return 'Memuatkan data...';
    }

    public $reloadTimeout = 120;

    public $cacheTime = 2;
    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {

        $today_user = User::where('type', 'user')->where(function ($query) {
            $query->where('created_at', '>', Carbon::today()->startOfDay())->where('created_at', '<', Carbon::today()->endOfDay());
        })->count();

        $new_user = User::where('type', 'user')->whereIn('status', [0,2,3])->count();
        $semak = User::where('type', 'user')->whereIn('status', [2,3])->count();
        $lulus = User::where('type', 'user')->where('status', 1)->count();
        $belumDikemaskini = User::where('type', 'user')->where('status', 0)->count();
        $berhenti = User::where('type', 'user')->where('status', 97)->count();


        return view('widgets.statistik_keanggotaan', [
            'new_user' => $new_user,
            'today_user' => $today_user,
            'semak' => $semak,
            'lulus' => $lulus,
            'belumDikemaskini' => $belumDikemaskini,
            'berhenti' => $berhenti
        ]);
    }
}
