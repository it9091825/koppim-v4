<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;
use App\User;


class PencapaianAffiliate extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [
        'month' => 'all',
        'year' => 'all'
    ];

    public function placeholder()
    {
        return 'Memuatkan data...';
    }

    public $reloadTimeout = 120;

    public $cacheTime = 2;
    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $affiliate_stats = User::with('downlines')->get()
            ->filter(function($value){
                return $value->is_affiliate == "yes";
            })->map(function($item){
                return [
                    'name' => $item->name,
                    'id' => $item->id,
                    'count' => count($item->downlines)
                ];
            })->sortByDesc('count')->take(5);

        return view('widgets.pencapaian_affiliate', [
            'affiliate' => $affiliate_stats->values()->all(),
        ]);
    }
}
