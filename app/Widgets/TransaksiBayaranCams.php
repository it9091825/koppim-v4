<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;
use App\Mongo\Payment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use DateTime;

class TransaksiBayaranCams extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [
        'month' => 'all',
        'year' => 'all'
    ];

    public function placeholder()
    {
        return 'Memuatkan data...';
    }

    public $reloadTimeout = 120;

    public $cacheTime = 2;
    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $start = Carbon::parse($this->config['year'] . '-' . $this->config['month'] . '-01 00:00:00');
        $end = Carbon::parse($this->config['year'] . '-' . $this->config['month'] . '-31 23:59:59');


        if ($this->config['month'] == 'all') {
            $jumlahFi = Payment::where('returncode', '100')->sum('first_time_fees');
            $jumlahSyer = Payment::where('returncode', '100')->sum('share_amount');
        } else {
            $jumlahFi = Payment::whereBetween('created_at', [$start, $end])->where('returncode', '100')->sum('first_time_fees');

            $jumlahSyer = Payment::whereBetween('created_at', [$start, $end])->where('returncode', '100')->sum('share_amount');
        }


        $jumlahKeseluruhan = $jumlahFi + $jumlahSyer;
        return view('widgets.transaksi_bayaran_cams', [
            'jumlahFi' => $jumlahFi,
            'jumlahSyer' => $jumlahSyer,
            'jumlahKeseluruhan' => $jumlahKeseluruhan
        ]);
    }
}
