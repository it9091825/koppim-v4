<?php

namespace App\Widgets;

use Arrilot\Widgets\AbstractWidget;
use App\Mongo\Payment;

class TransaksiBayaranIkoop extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config = [];

    public function placeholder()
    {
        return 'Memuatkan data...';
    }

    public $reloadTimeout = 120;

    public $cacheTime = 2;
    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $totalFeeIkoop = Payment::where('channel', 'ikoop')
            ->where('returncode', '100')->sum('first_time_fees');
        $totalShareIkoop = Payment::where('channel', 'ikoop')
            ->where('returncode', '100')->sum('share_amount');
        $totalKeseluruhanIkoop = $totalFeeIkoop + $totalShareIkoop;


        return view('widgets.transaksi_bayaran_ikoop', [
            'totalFeeIkoop' => $totalFeeIkoop,
            'totalShareIkoop' => $totalShareIkoop,
            'totalKeseluruhanIkoop' => $totalKeseluruhanIkoop
        ]);
    }
}
