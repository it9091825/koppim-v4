<?php

namespace App\Helpers;

use Auth;

use App\User;
use App\Mongo\LetterModule as LetterModule;
use App\Mongo\LetterJob as LetterJob;
use App\Mongo\LetterJobItems as LetterJobItems;
use Carbon\Carbon;
use Illuminate\Support\Facades\URL;

class LetterHelper
{

    public static function count_user_letter($ic)
    {
		$count = LetterJobItems::where('ic',$ic)->count();
		
		return $count;
       
    }

    
    public static function count_progress($job_id)
    {
		$count = LetterJobItems::where('job_id',$job_id)->where('status','sent')->count();
		
		return $count;
       
    }
    
}
