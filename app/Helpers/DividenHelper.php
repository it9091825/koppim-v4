<?php

namespace App\Helpers;

use Auth;

use App\User;
use App\Mongo\Dividen as Dividen;
use App\Mongo\DividenCalculation as DividenCalculation;
use App\Mongo\DividenWallet as DividenWallet;
use Carbon\Carbon;
use Illuminate\Support\Facades\URL;

class DividenHelper
{

	public static function get_dividen($calculation_id)
    {
		$detail = Dividen::where('uid',$calculation_id)->first();
		return $detail;

    }

    public static function get_dividen_detail($id)
    {
		$detail = DividenCalculation::where('ic',$id)->first();


		return $detail;

    }


	public static function status_helper($status)
    {
		if($status == 'calculate-in-progress')
		{
			return "CALCULATING";
		}
		else if($status == 'calculate-in-progress-2')
		{
			return "CALCULATING...";
		}
		else if($status == 'published-in-progress')
		{
			return "PUBLISHING - PLEASE WAIT";
		}
		else
		{
			return $status;
		}


    }


}
