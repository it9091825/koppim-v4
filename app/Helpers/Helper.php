<?php

namespace App\Helpers;

use Auth;
use App\Payment;
use App\Mongo\Payment as MPayment;
use App\Mongo\CommissionWithdraw;
use App\User;
use App\Course;
use Carbon\Carbon;
use Illuminate\Support\Facades\URL;

class Helper
{

    public static function get_user_commission_balance($introducer_no_anggota)
    {

        $jumlah_commision = MPayment::where('introducer_no_anggota', (string)$introducer_no_anggota)->where('returncode', '100')->where('introducer', true)->where('introducer_status', 'LULUS')->sum('introducer_commission');
        $withdraws = CommissionWithdraw::where('no_anggota', (int)$introducer_no_anggota)->whereIn('status', ['MENUNGGU KELULUSAN', 'LULUS', 'SEMAKAN'])->sum('amount');
        $balance = $jumlah_commision - $withdraws;

        return $balance;
    }

    public static function get_user_from_no_anggota($no_anggota)
    {

        $anggota = User::where('no_koppim', $no_anggota)->first();

        return $anggota;
    }

    public static function get_user_no_anggota_from_mykad_no($mykad_no){

        $no_anggota = User::where('ic',$mykad_no)->first();

        return $no_anggota->no_koppim ?? 'TIADA NO. ANGGOTA KoPPIM';
    }

    public static function get_user_from_no_anggota_jumlah_diluluskan($no_anggota, $status)
    {

        $jumlah_commission_month = MPayment::where('introducer_no_anggota', (string)$no_anggota)->where('returncode', '100')->where('introducer', true)->where('introducer_status', $status)->sum('introducer_commission');

        return $jumlah_commission_month;
    }

    public static function get_user_from_no_anggota_jumlah_untuk_diluluskan($no_anggota)
    {

        $jumlah_commission_month = MPayment::where('introducer_no_anggota', (string)$no_anggota)->where('returncode', '100')->where('introducer', true)->orWhereNull('introducer_status')->sum('introducer_commission');

        return $jumlah_commission_month;
    }

    public static function get_user_from_no_anggota_jumlah_pengeluaran($no_anggota, $status)
    {

        $jumlah_commission_month = CommissionWithdraw::where('no_anggota', (int)$no_anggota)->where('status', $status)->sum('amount');

        return $jumlah_commission_month;
    }

    public static function get_user_from_id($id)
    {

        $anggota = User::where('id', $id)->first();

        return $anggota;
    }


    public static function get_user_from_ic($ic)
    {

        $anggota = User::where('ic', $ic)->first();

        return $anggota;
    }

    public static function to_campaign($campaign_id)
    {

        $campaign = Course::where('id', $campaign_id)->first();

        return $campaign;
    }

    public static function ccMasking($number, $maskingCharacter = 'X')
    {
        return substr($number, 0, 2) . str_repeat($maskingCharacter, strlen($number) - 6) . substr($number, -4);
    }

    public static function transaction_count_by_campaign_id($campaign_id)
    {

        $user = Payment::where('campaign_id', $campaign_id)->where('returncode', '100')->count();

        return $user;
    }

    public static function transaction_amount_by_campaign_id($campaign_id)
    {

        $user = Payment::where('campaign_id', $campaign_id)->where('returncode', '100')->sum('amount');

        return $user;
    }


    public static function last_transaction_by_campaign_id($campaign_id)
    {

        $user = Payment::where('campaign_id', $campaign_id)->where('returncode', '100')->orderBy('created_at', 'DESC')->first();

        if ($user) {
            return $user->created_at;
        } else {
            return '-';
        }
    }


    public static function donation_between($campaign_id, $from, $to)
    {

        $from_s = Carbon::parse('' . $from . ' 00:00:00');
        $to_s = Carbon::parse('' . $to . ' 23:59:59');

        $user = Payment::where('campaign_id', $campaign_id)->where('returncode', '100')->whereBetween('created_at', [$from_s, $to_s])->sum('amount');

        return $user;
    }


    public static function donation_count_between($campaign_id, $from, $to)
    {

        $from_s = Carbon::parse('' . $from . ' 00:00:00');
        $to_s = Carbon::parse('' . $to . ' 23:59:59');

        $user = Payment::where('campaign_id', $campaign_id)->where('returncode', '100')->whereBetween('created_at', [$from_s, $to_s])->count();

        return $user;
    }

    //Status text keanggotaan untuk paparan admin
    public static function status_text($status)
    {



        if ($status == 0) {
            return " <div class='badge badge-info' >BELUM DIKEMASKINI</div>";
        }
        if ($status == 96) {
            return "<div class='badge badge-warning' >MOHON BERHENTI</div>";
        }
        if ($status == 97) {
            return "<div class='badge badge-danger' >TIDAK AKTIF</div>";
        }
        if ($status == 98) {
            return "<div class='badge badge-danger' >DISEKAT</div>";
        }
        if ($status == 99) {
            return "<div class='badge badge-warning' >GAGAL</div>";
        }
        if ($status == 1) {
            return "<div class='badge badge-success'>DILULUSKAN</div>";
        }

        if ($status == 2) {
            return "<div class='badge badge-warning'>UNTUK DISEMAK</div>";
        }

        if ($status == 3) {
            return "<div class='badge badge-warning'>UNTUK DILULUSKAN</div>";
        }
    }

    //Status text keanggotaan untuk paparan anggota
    public static function anggota_status_text($status)
    {
        if ($status == 0) {
            return "<div class='badge badge-info'>BELUM DIKEMASKINI</div>";
        }
        if ($status == 96) {
            return "<div class='badge badge-warning'>PROSES PEMBERHENTIAN</div>";
        }
        if ($status == 97) {
            return "<div class='badge badge-danger'>TIDAK AKTIF</div>";
        }
        if ($status == 98) {
            return "<div class='badge badge-danger'>DISEKAT</div>";
        }
        if ($status == 99) {
            return "<div class='badge badge-warning'>TIDAK LULUS</div>";
        }
        if ($status == 1) {
            return "<div class='badge badge-success'>DILULUSKAN</div>";
        }

        if ($status == 2) {
            return "<div class='badge badge-warning'>MENUNGGU KELULUSAN</div>";
        }

        if ($status == 3) {
            return "<div class='badge badge-warning'>MENUNGGU KELULUSAN</div>";
        }
    }

    public static function payment_status_text($status)
    {
        if ($status == 'waiting-for-verification') {
            return " <div class='badge badge-warning' >UNTUK DISEMAK</div>";
        }

        if ($status == 'waiting-for-approval') {
            return " <div class='badge badge-info' >UNTUK DISAHKAN</div>";
        }

        if ($status == 'payment-completed') {
            return " <div class='badge badge-success' >TELAH DISAHKAN</div>";
        }

        if ($status == 'verification-cancelled') {
            return " <div class='badge badge-danger' >PENGESAHAN GAGAL</div>";
        }

        if ($status == 'waiting-proof-upload') {
            return " <div class='badge badge-danger' >TIADA BUKTI BAYARAN</div>";
        }

        if ($status == 'verification') {
            return " <div class='badge badge-danger' >TIADA BAYARAN DIBUAT</div>";
        }

        if ($status == 'waiting-for-reverification') {
            return " <div class='badge badge-warning' >UNTUK DISEMAK SEMULA</div>";
        }
    }


    //Status text keanggotaan untuk paparan status komisen
    public static function commission_status_text($status)
    {
        if ($status == 'available') {
            return " <div class='badge badge-info' >BELUM DIPROSES</div>";
        }
        if ($status == 'processing') {
            return "<div class='badge badge-warning' >DALAM PROSES</div>";
        }
        if ($status == 'redeemed') {
            return "<div class='badge badge-success'>SUDAH DIPROSES</div>";
        }
    }

    //Status text keanggotaan dan admin untuk paparan status pengeluaran
    public static function withdrawal_status_text($status)
    {
        if ($status == 'processing') {
            return " <div class='badge badge-warning' >DALAM PROSES</div>";
        }
        if ($status == 'cancelled') {
            return "<div class='badge badge-danger' >DIBATALKAN</div>";
        }
        if ($status == 'paid') {
            return "<div class='badge badge-success'>SUDAH DIPROSES</div>";
        }
    }

    public static function campaign_status_text($status)
    {

        if ($status == 0) {
            return " <div class='badge badge-info' >DRAF</div>";
        } else if ($status == 97) {
            return "<div class='badge badge-warning' >PERLU PERUBAHAN</div>";
        } else if ($status == 98) {
            return "<div class='badge badge-danger' >DISEKAT</div>";
        } else if ($status == 99) {
            return "<div class='badge badge-warning' >TIDAK LULUS</div>";
        } else if ($status == 1) {
            return "<div class='badge badge-success' >DILULUSKAN</div>";
        } else if ($status == 2) {
            return "<div class='badge badge-warning' >SEDANG DISEMAK</div>";
        }
    }


    public static function campaign_status_text_array()
    {

        return [
            0 => 'DRAF',
            97 => 'PERLU PERUBAHAN',
            98 => 'DISEKAT',
            99 => 'TIDAK LULUS',
            1 => 'DILULUSKAN',
        ];
    }

    public static function status_text_array()
    {

        return [
            0 => 'PERMOHONAN',
            1 => 'AKTIF',
            2 => 'SEDANG DISEMAK',
            97 => 'TIDAK AKTIF',
            98 => 'DISEKAT',
            99 => 'TIDAK LULUS',

        ];
    }


    public static function withdraw_status_text($status)
    {

        if ($status == 0) {
            return " <div class='badge badge-info' >MENUNGGU KEPUTUSAN</div>";
        } else if ($status == 99) {
            return "<div class='badge badge-warning' >TIDAK LULUS</div>";
        } else if ($status == 1) {
            return "<div class='badge badge-success' >DILULUSKAN</div>";
        } else if ($status == 2) {
            return "<div class='badge badge-warning' >SEDANG DISEMAK</div>";
        }
    }

    /**
     * Untuk tambah class 'active' pada menu current url
     * @param any $url
     */
    public static function active($url)
    {

        if (URL::full() == $url) {
            return 'active';
        }
    }

    public static function getFirstNameOnly($name)
    {
        return explode(' ', trim($name))[0];
    }

    public static function moneyFormat($money)
    {
        return number_format($money, 2, '.', ',');
    }
}
