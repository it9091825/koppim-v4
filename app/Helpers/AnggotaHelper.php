<?php

namespace App\Helpers;

class AnggotaHelper
{
    public static function check_user_profile_filled_except_mykad($user){
        if ($user->name == NULL || $user->sex == NULL || $user->race == NULL ||
            $user->religion == NULL || $user->marital_status == NULL ||
            $user->dob == NULL || $user->address_line_1 == NULL || $user->postcode == NULL ||
            $user->city == NULL || $user->state == NULL || $user->parliament == NULL ||
            $user->mobile_no == NULL || $user->email == NULL || $user->job == NULL ||
            $user->monthly_income == NULL || $user->bank_name == NULL ||
            $user->bank_account == NULL || $user->heir_name == NULL || $user->heir_ic == NULL ||
            $user->heir_phone == NULL || $user->heir_relationship == NULL) {
            return false;
        }

        return true;

    }

}
