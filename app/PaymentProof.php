<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentProof extends Model
{
	protected $table = 'payment_proof';
    protected $guarded = [];


    protected $casts = [
        'payment_date' => 'datetime',
        'approval_datetime' => 'datetime'
    ];

}
