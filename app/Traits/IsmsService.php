<?php


namespace App\Traits;


use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;

class IsmsService
{
    public $message;
    public $destination;

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param mixed $message
     * @return IsmsService
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDestination()
    {
        return $this->destination;
    }

    /**
     * @param mixed $destination
     * @return IsmsService
     */
    public function setDestination($destination)
    {
        $this->destination = $destination;

        return $this;
    }

    public static function sendMessage($destinationNo, $message) {
        $message = preg_replace('/[^A-Za-z0-9:.,#\/?=\- ]/', '', $message);

        $message = 'KoPPIM: ' . $message;

        $client = new Client();

        $response = $client->post('https://www.isms.com.my/isms_send.php', [
            'form_params' => [
                'un' => config('services.isms.username'),
                'pwd' => config('services.isms.password'),
                'dstno' => $destinationNo,
                'msg' => $message,
                'type' => 1,
                'sendid' => 66300,
            ],
            'http_errors' => false
        ]);

        Log::info($response->getStatusCode());
        Log::info($response->getBody()->getContents());
    }

}
