<?php

namespace App\Traits;

use App\User;
use Illuminate\Http\Request;

trait MemberTrait
{


    public function generate_no_koppim(User $user)
    {
        $last_no = User::orderBy('no_koppim', 'desc')->first();
        //check if user already have no_koppim to prevent from regenerate new number
        if ($user->no_koppim == null || $user->no_koppim == '') {
            //generate no_koppim for user when approved(status=1)
            $user->no_koppim = $last_no->no_koppim + 1;
            $user->save();
        }
    }


}
