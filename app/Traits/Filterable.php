<?php

namespace App\Traits;

use App\Concerns\Filter;
use Illuminate\Database\Eloquent\Builder;

trait Filterable
{

    public function scopeFilter(Builder $query, Filter $filter): Builder
    {
        return $filter->apply($query);
    }
}