<?php

namespace App\Traits;

trait MobileNumberTrait{

    public function mobile_no_trim($mobile_no)
    {

        $mobile_no = $this->clean($mobile_no);

        $firstChar = mb_substr($mobile_no, 0, 1, "UTF-8");

        // $new_number = preg_replace("/^\+?6/", '',$mobile_no);

        if ($firstChar == 1) {
            $new_number = "0" . $mobile_no;
        } else if ($firstChar == 6) {
            $str = ltrim($mobile_no, '6');
            $new_number = $str;
        } else {
            $new_number = $mobile_no;
        }

        return $new_number;
    }

    public function clean($string)
    {
        $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
        $sdsd = preg_replace('/[^A-Za-z0-9\-]/', '', $string);

        $sdsd2 = str_replace("-", "", $sdsd);

        return $sdsd2; // Removes special chars.
    }
}