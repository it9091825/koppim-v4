<?php

namespace App\Traits;

use App\Mongo\Smslog;

trait SmsTrait
{

    public function send_sms($ic, $mobile_no, $text)
    {
        //execute only ENABLE_SMS_SEND in .env is true
        if (env('ENABLE_SMS_SEND')) {

            IsmsService::sendMessage($mobile_no, $text);

            $smsLog = new Smslog;
            $smsLog->ic = $ic;
            $smsLog->mobile_no = $mobile_no;
            $smsLog->text = $text;
            $smsLog->provider = 'isms';

            $smsLog->save();
        }

    }
}
