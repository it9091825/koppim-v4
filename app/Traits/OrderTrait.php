<?php

namespace App\Traits;
use App\Mongo\Payment as MPayment;
use Hashids\Hashids;

trait OrderTrait{

    public function create_order_ref($ic)
    {
        //todo: validate ic aka only numbers
        $hashids = new Hashids('koppim',0, 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789');
        $order_ref = $hashids->encode(time(),(integer)$ic) . '-' . date("y-m-d");

        //Check if order_ref already available, sleep for 1 second and generate new order ref
        $payment = MPayment::where('order_ref',$order_ref)->first();
        if($payment) {
            sleep(1);
            return $this->create_order_ref($ic);
        }

        return $order_ref;
    }

    public function check_order_ref($orderRef)
    {

    }

    public function decode_order_ref($order_ref)
    {

    }
}
