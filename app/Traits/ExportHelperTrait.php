<?php

namespace App\Traits;


trait ExportHelperTrait
{
    /**
     * Mapp columns in excel export
     *
     * @param array $columnsName
     * @param [type] $key
     * @return array
     */
    public function mapColumns(array $columnsName,$key):array
    {
        $columns = [];

        foreach ($columnsName as $column) {
            $columns[] = $key->$column;
        }

        return $columns;
    }

    /**
     * Format and capitalize excel headers
     *
     * @param array $columns
     * @return void
     */
    public function formatHeader(array $columns):array
    {

        $headers = [];

        foreach ($columns as $column) {

            $headers[] = $this->capitalizeHeader($column);
        }

        return $headers;
    }

    /**
     * Capitalize excel headers
     *
     * @param string $string
     * @return void
     */
    public function capitalizeHeader(string $string):string
    {
        if (strpos($string, "_") !== false) {
            $new_string = str_replace("_", " ", $string);
            return strtoupper($new_string);
        }

        return strtoupper($string);
    }
}
