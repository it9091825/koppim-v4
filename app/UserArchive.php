<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserArchive extends Model
{
    protected $guarded = [];

    protected $casts = [
        'email_verified_at' => 'datetime',
        'dob' => 'datetime',
        'approved_date' => 'datetime',
        'affiliate_apply' => 'datetime',
        'affiliate_apply_result' => 'datetime',
    ];
}
