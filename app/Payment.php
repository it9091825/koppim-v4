<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $fillable = ['name','ic','phone','email','amount','share_amount','first_time_fees', 'status', 'type', 'gateway', 'returncode', 'campaign_id', 'order_ref'];



}
