<?php

namespace App\Jobs;

use App\Mongo\CommissionWithdraw;
use App\Mongo\Payment;
use App\User;
use Carbon\Carbon;
use Hashids\Hashids;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ProcessCommissionForWithdrawal implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $users = User::where('status', 1)->where('is_affiliate', 'yes')->get();

        foreach ($users as $user) {
            $balance = Payment::where('introducer_no_anggota', (string)$user->no_koppim)
                ->where('returncode', '100')
                ->where('introducer', true)
                ->where('introducer_commission_status', 'available')
                ->where('introducer_commission_date', '<', new Carbon('last day of last month'))
                ->sum('introducer_commission');

            $hashids = new Hashids('withdrawal', 0, 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789');
            $ids = round(microtime(true) * 1000);
            $reference_no = 'AF-' . $hashids->encode((integer)$ids);
            $end = new Carbon('last day of last month');

            if ($balance && $balance > 0.00) {

                $withdraw = CommissionWithdraw::create([
                    'no_anggota' => (string)$user->no_koppim,
                    'name' => (string)$user->name,
                    'user_id' => (int)$user->id,
                    'ic' => (string)$user->ic,
                    'amount' => (float)$balance,
                    'bank' => $user->bank_name,
                    'account_name' => $user->bank_account_name,
                    'account_no' => $user->bank_account,
                    'status' => 'processing',
                    'statement_date' => $end,
                    'statement_month' => $end->month,
                    'statement_year' => $end->year,
                    'reference_no' => $reference_no
                ]);

                Payment::where('introducer_no_anggota', (string)$user->no_koppim)
                    ->where('returncode', '100')
                    ->where('introducer', true)
                    ->where('introducer_commission_status', 'available')
                    ->update(['introducer_commission_status' => 'processing', 'affiliate_commission_withdraw_id' => $withdraw->id]);

            } else {

                $withdraw = CommissionWithdraw::where('user_id', (int)$user->id)
                    ->where('statement_year', $end->year)
                    ->where('statement_month', $end->month)
                    ->first();

                if(!$withdraw) {
                    CommissionWithdraw::create([
                        'user_id' => (int)$user->id,
                        'statement_date' => $end,
                        'no_anggota' => (string)$user->no_koppim,
                        'name' => (string)$user->name,
                        'ic' => (string)$user->ic,
                        'amount' => 0.00,
                        'bank' => $user->bank_name,
                        'account_name' => $user->bank_account_name,
                        'account_no' => $user->bank_account,
                        'status' => 'paid',
                        'payment_ref' => 'Tiada',
                        'statement_month' => $end->month,
                        'statement_year' => $end->year,
                        'reference_no' => $reference_no
                    ]);
                }
            }

        }
    }
}
