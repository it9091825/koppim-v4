<?php

namespace App\Jobs;

use App\AffiliateSetting;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Carbon\Carbon;

class ProcessCommission implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $user;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        if ($this->user->payments) {

            //Default setting
            $commission_amount = 10.00;

            //Global setting
            $affSetting = AffiliateSetting::first();
            if ($affSetting) {
                $commission_amount = (double)$affSetting->registration_amount;
            }

            foreach ($this->user->payments as $payment) {

                //Check if registration fee available
                if ($payment->first_time_fees == 30) {

                    //check if introducer == true
                    if($payment->introducer) {
                        //Introducer setting
                        $introducer = User::where('no_koppim', (integer)$payment->introducer_no_anggota)->first();
                        if ($introducer) {
                            if ($introducer->affiliate_setting()->exists()) {
                                $commission_amount = (double)$introducer->affiliate_setting->registration_amount;
                            }
                        }

                        $payment->introducer_commission = (double)$commission_amount;
                        $payment->introducer_commission_status = 'available';//Belum Ditebus = available //Sudah Ditebus = redeemed, Dalam Process = processing
                        $payment->introducer_commission_date = Carbon::now();
                        $payment->save();
                    }
                }
            }
        }
    }
}
