<?php

namespace App\Jobs;

use App\Mail\SendAnnouncement;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\User;
use App\Admin\Announcement;
use Illuminate\Support\Facades\Mail;

class ProcessAnnouncementEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $users;
    public $announcement;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($users, Announcement $announcement)
    {
        $this->users = $users;
        $this->announcement = $announcement;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            foreach ($this->users as $user){
                Mail::to($user->email)->queue(new SendAnnouncement($this->announcement));
                //dd($user->email);
            }
        } catch (Exception $e) {
            report($e);
            return false;
        }
    }
}
