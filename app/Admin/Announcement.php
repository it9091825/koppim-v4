<?php

namespace App\Admin;

use Illuminate\Database\Eloquent\Model;

class Announcement extends Model
{
    protected $table = 'announcements';

    protected $fillable = ['title','message','published_from','published_until'];
}
