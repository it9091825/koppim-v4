<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class user_terminations extends Model
{
    protected $fillable = ['leave_reason', 'terminate_letter'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
