<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Blade;

class BladeComponentServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Blade::component('components.table.no-result', 'tablenoresult');
        Blade::component('components.card', 'card');
        Blade::component('components.maintenance', 'maintenance');
        Blade::component('components.css','css');
        Blade::component('components.page-title','pagetitle');
    }
}
