<?php

namespace App\Providers;

use App\Permission;
use Exception;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;

class PermissionServiceProvider extends ServiceProvider
{

    public function register()
    {
        
    }

    public function boot()
    {
        try {
            Permission::get()->map(function ($permission) {
                Gate::define($permission->slug, function ($user) use ($permission) {
                    return $user->hasPermissionTo($permission);
                });
            });
        } catch (Exception $e) {
            return false;
        }

        //Blade directives
        Blade::directive('role', function ($role) {
            return "<?php if(auth()->check() && auth()->user()->hasRole({$role})){ ?>"; //
        });

        Blade::directive('endrole', function ($role) {
            return "<?php } ?>";
        });

        Blade::directive('can', function ($permission) {
            return "<?php if(auth()->check() && auth()->user()->can({$permission})){ ?>"; 
        });

        Blade::directive('endcan', function ($permission) {
            return "<?php } ?>";
        });

        Blade::directive('cant', function ($permission) {
            return "<?php if(auth()->check() && !auth()->user()->can({$permission})){ ?>"; 
        });

        Blade::directive('endcant', function ($permission) {
            return "<?php } ?>";
        });

    }
}
