<?php

namespace App\Providers;

use Illuminate\Support\Facades\App;
use Illuminate\Support\ServiceProvider;
use Illuminate\Contracts\Routing\UrlGenerator;
use Carbon\Carbon;
use App\Admin\Announcement;
use Exception;
use Illuminate\Support\Facades\Blade;


class AppServiceProvider extends ServiceProvider

{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(UrlGenerator $url)
    {
        if (!App::environment('local')) {
            $url->forceScheme('https');
        }

        \Carbon\Carbon::setLocale(config('app.locale'));

        // check for announcements
        try {

            $announcements = collect(Announcement::get());

            $announcement = $announcements->filter(function ($item, $key) {

                $date_range = $this->generateDateRange(Carbon::parse($item->published_from), Carbon::parse($item->published_until), 'Y-m-d');

                if (in_array(Carbon::today()->format('Y-m-d'), $date_range)) {
                    return $this;
                }
            });

            $message = $announcement->first()->message;

            view()->composer('announcements.body', function ($view) use ($message) {
                $view->with('message', $message);
            });
        } catch (Exception $e) {
            return false;
        }

    }

    protected function generateDateRange(Carbon $start_date, Carbon $end_date, string $date_format = null): array
    {
        $dates = [];
        for ($date = $start_date; $date->lte($end_date); $date->addDay()) {
            $dates[] = $date_format != null ? $date->format($date_format) : $date;
        }
        return $dates;
    }
}
