<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentItems extends Model
{
    protected $table = 'payment_items';
    protected $guarded = [];

    protected $dates = ['created_at', 'updated_at'];

}
