<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\State;
use Carbon\Carbon;
use App\Mongo\Dividen;
use App\Mongo\Payment;
use Illuminate\Http\Request;
use App\Exports\Admin\UsersExport;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Schema;
use App\Exports\Admin\PermohonanExport;
use App\Exports\Admin\KeanggotaanExport;
use App\Exports\Admin\MykadNotExistsExport;
use App\Exports\Admin\PaymentTransactionsExport;



class ReportsController extends Controller
{
    public function index()
    {

        return view('admin.reports.index');
    }

    public function getResult(Request $request)
    {


        if ($request->filled('report_type')) {

            if ($request->report_type == 'permohonan') {
                $query = $this->permohonanFilter($request);
            }

            if ($request->report_type == 'keanggotaan') {
                $query = $this->keanggotaanFilter($request);
            }
            if ($request->report_type == 'bayaran') {
                $query = $this->bayaranFilter($request);
            }
            if ($request->report_type == 'mykad') {
                $query = $this->mykadNotExistsFilter($request);
            }

            $response = $query->count();

            return response()->json(['result' => $response]);
        }
    }

    public function export(Request $request)
    {

        if ($request->report_type == 'permohonan') {
            return (new PermohonanExport($request))->download($request->filled('fileName') ? $request->fileName . '.xlsx' : 'PermohonanKoPPIM.xlsx');
        }

        if ($request->report_type == 'keanggotaan') {
            return (new KeanggotaanExport($request))->download($request->filled('fileName') ? $request->fileName . '.xlsx' : 'KeanggotaanKoPPIM.xlsx');
        }

        if ($request->report_type == 'bayaran') {
            return (new PaymentTransactionsExport($request))->download($request->filled('fileName') ? $request->fileName . '.xlsx' : 'TransaksiBayaranKoPPIM.xlsx');
        }

        if ($request->report_type == 'mykad') {
            return (new MykadNotExistsExport($request))->download($request->filled('fileName') ? $request->fileName . '.xlsx' : 'TidakMuatNaikMykadKoPPIM.xlsx');
        }
    }

    public function permohonanFilter(Request $request)
    {

        $query = DB::table('users')->where('type', 'user');

        if ($request->filled('permohonan_registration_type')) {
            $query = $query->where('registration_type', $request->permohonan_registration_type);
        }

        if ($request->filled('permohonan_states')) {
            if ($request->permohonan_states != 'all') {

                if ($request->permohonan_parliaments == 'all') {
                    // buat camni sebab state tidak disimpan sebagai ID di users table. So kena cari dulu nama state with given $request->state
                    $state = State::find($request->permohonan_states);
                    $query = $query->where('state', 'LIKE', '%' . $state->name . '%');
                } else {
                    $query = $query->where('parliament', 'LIKE', '%' . $request->permohonan_parliaments . '%');
                }
            }
        }

        if ($request->filled('permohonan_status')) {
            $query = $query->where('status', $request->permohonan_status);
        }

        if ($request->filled('permohonan_start_date')) {
            $query = $query->where('created_at', '>=', Carbon::parse($request->permohonan_start_date));
        }

        if ($request->filled('permohonan_end_date')) {
            $query = $query->where('created_at', '<=', Carbon::parse($request->permohonan_end_date));
        }

        return $query;
    }

    public function keanggotaanFilter(Request $request)
    {

        $query = DB::table('users')->where('type', 'user');

        if ($request->filled('mykad_status')) {
            $query = $query->where('status', $request->mykad_status);
        }

        if ($request->filled('mykad_states')) {

            if ($request->mykad_states != 'all') {

                if ($request->mykad_parliaments == 'all') {
                    // buat camni sebab state tidak disimpan sebagai ID di users table. So kena cari dulu nama state with given $request->state
                    $state = State::find($request->mykad_states);
                    $query = $query->where('state', 'LIKE', '%' . $state->name . '%');
                } else {
                    $query = $query->where('parliament', 'LIKE', '%' . $request->mykad_parliaments . '%');
                }
            }
        }


        if ($request->filled('mykad_start_date')) {
            $query = $query->where('created_at', '>=', Carbon::parse($request->mykad_start_date));
        }

        if ($request->filled('mykad_end_date')) {
            $query = $query->where('created_at', '<=', Carbon::parse($request->mykad_end_date));
        }

        return $query;
    }

    public function bayaranFilter(Request $request)
    {

        $query = DB::connection('mongodb')->collection('payments');

        if ($request->filled('payment_method')) {
            $query = $query->where('channel', $request->payment_method);
        }

        if ($request->filled('payment_status')) {

            $query = $query->where('status', $request->payment_status);
        }

        if ($request->filled('payment_start_date')) {
            $query = $query->where('created_at', '>=', Carbon::parse($request->payment_start_date));
        }

        if ($request->filled('payment_end_date')) {
            $query = $query->where('created_at', '<=', Carbon::parse($request->payment_end_date));
        }

        return $query;
    }

    public function mykadNotExistsFilter(Request $request)
    {

        $query = DB::table('users')->where('type', 'user')->where(function ($query) {
            $query->where('mykad_front', '')->orWhereNull('mykad_front');
        });

        if ($request->filled('mykad_states')) {

            if ($request->mykad_states != 'all') {

                if ($request->mykad_parliaments == 'all') {
                    // buat camni sebab state tidak disimpan sebagai ID di users table. So kena cari dulu nama state with given $request->state
                    $state = State::find($request->mykad_states);
                    $query = $query->where('state', 'LIKE', '%' . $state->name . '%');
                } else {
                    $query = $query->where('parliament', 'LIKE', '%' . $request->mykad_parliaments . '%');
                }
            }
        }


        if ($request->filled('mykad_start_date')) {
            $query = $query->where('created_at', '>=', Carbon::parse($request->mykad_start_date));
        }

        if ($request->filled('mykad_end_date')) {
            $query = $query->where('created_at', '<=', Carbon::parse($request->mykad_end_date));
        }

        return $query;
    }

    public function showSummaryPage()
    {
        return view('admin.reports.summary.index');
    }

    public function auditViewPage()
    {
        return view('admin.reports.audit.index');
    }

    public function auditExport(Request $request)
    {

        $file_name =  $request->filename;
        $from = $request->from;
        $to  = $request->to;
        $audit_type = $request->audit_type;

        $payment_from = Carbon::createFromDate($from);
        $payment_to = Carbon::createFromDate($to);

        $users = User::select('id', 'email', 'name', 'ic', 'sex', 'religion', 'race', 'parliament', 'created_at')
            ->with(['payments' => function ($query) use($payment_from, $payment_to) {
                $query->where('status', 'payment-completed')
                    ->where('created_at', '>=', $payment_from)
                    ->where('created_at', '<=', $payment_to);
            }])
            ->where('type', 'user')
            ->where('status', 1)
            ->whereNotNull('no_koppim')
            ->get();


        $fileName = "" . $file_name . ".csv";

        $headers = array(
            "Content-type"        => "text/csv",
            "Content-Disposition" => "attachment; filename=$fileName",
            "Pragma"              => "no-cache",
            "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
            "Expires"             => "0"
        );

        // if admin memilih report pembayaran jumlah syer specific
        if ($audit_type == 'specific') {

            $columns = array('NO', 'EMEL', 'NAMA', 'IC', 'JANTINA', 'AGAMA', 'BANGSA', 'PARLIAMENT', 'JUMLAH_PENDAFTARAN', 'JUMLAH KESELURUHAN SYER',  'JUMLAH MODAL SYER', 'JUMLAH PENAMBAHAN SYER', 'DAFTAR DARI', 'TARIKH DAFTAR', );

            $callback = function () use ($users, $columns) {

                $file = fopen('php://output', 'w');
                fputcsv($file, $columns);

                $i = 1;
                foreach ($users as $user) {
                    $syer = 0;
                    $first_fees = 0;
                    $add_fees = 0;
                    $first_time_share = 0;
                    foreach($user->payments as $payment) {
                        $syer += ($payment->share_amount == 0) ? $payment->first_time_share : $payment->share_amount;
                        $first_time_share += $payment->first_time_share;
                        $first_fees += $payment->first_time_fees;
                        $add_fees += $payment->additional_share;
                        $channel = $payment->channel;
                    }

                    fputcsv($file, array($i++, $user['email'], $user['name'], $user['ic'], $user['sex'], $user['religion'], $user['race'], $user['parliament'], $first_fees, $syer, $first_time_share, $add_fees, $channel, date($user['created_at'])));
                }

                fclose($file);
            };

            return response()->stream($callback, 200, $headers);

        }

        // if admin memilih report semua pembayaran transaksi
        elseif ($audit_type == 'all') {
            $columns = array('NO', 'NAMA', 'IC', 'JANTINA', 'AGAMA', 'BANGSA', 'PARLIAMENT', 'JUMLAH PENDAFTARAN', 'TARIKH PEMBAYARAN', 'JUMLAH SYER', 'PENAMBAHAN SYER', 'TARIKH DAFTAR', 'RUJUKAN PEMBAYARAN');

            $callback = function () use ($users, $columns) {

                $file = fopen('php://output', 'w');
                fputcsv($file, $columns);

                $i = 1;
                foreach ($users as $user) {
                    foreach($user->payments as $payment) {
                        fputcsv($file, array($i++, $user['name'], $user['ic'], $user['sex'], $user['religion'], $user['race'], $user['parliament'], $payment['first_time_fees'], date($payment['created_at']), $payment['share_amount'], $payment['additional_share'], date($user['created_at']), $payment['order_ref']));
                    }
                }

                fclose($file);
            };

            return response()->stream($callback, 200, $headers);
        }
    }
}
