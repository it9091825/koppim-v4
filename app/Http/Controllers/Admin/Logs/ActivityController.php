<?php

namespace App\Http\Controllers\Admin\Logs;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Spatie\Activitylog\Models\Activity;

class ActivityController extends Controller
{
    public function index(){
        $activities = Activity::orderBy('created_at','desc')->paginate(20);
        return view('admin.logs.activity',compact('activities'));
    }

}
