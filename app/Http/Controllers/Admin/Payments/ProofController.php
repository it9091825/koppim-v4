<?php

namespace App\Http\Controllers\Admin\Payments;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Mongo\Payment;
use Exception;


class ProofController extends Controller
{
    public function edit($id)
    {
        try {
            $payment = Payment::find($id);

            return view('admin.payments.proof.edit', ['proof' => $payment->payment_proof]);
        } catch (Exception $e) {

            session()->flash('error', $e->getMessage());
            return back();
        }
    }
}
