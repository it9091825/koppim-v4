<?php

namespace App\Http\Controllers\Admin\Payments;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Mongo\Payment;

class FetchDetailsController extends Controller
{
    public function show($id){
        
        $payment = Payment::findOrFail($id);

        return $payment;

    }
}
