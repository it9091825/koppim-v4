<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Message;

class MessagesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request){

        $search = $request->input('search');

        if ($search) {

            $messages = Message::where('ic',$search)->orderBy('created_at','desc')->paginate(10);

            return view('admin.messages',compact('messages','search'));
            
        } else {

            $messages = Message::orderBy('created_at','desc')->paginate(10);

            return view('admin.messages',compact('messages','search'));
        }
        
    }
}
