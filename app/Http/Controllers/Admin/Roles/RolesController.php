<?php

namespace App\Http\Controllers\Admin\Roles;

use App\Role;
use Exception;
use App\Permission;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class RolesController extends Controller
{
    public function index()
    {

        $roles = Role::paginate();

        return view('roles.index', compact('roles'));
    }

    public function edit($id)
    {

        $role = Role::find($id);
        $permissions = Permission::get()->sortBy('name');
        return view('roles.show', compact('role', 'permissions'));
    }

    public function store(Request $request)
    {
        
        Role::firstOrCreate([
            'slug' => Str::slug($request->name),
            'name' => $request->name,
        ]);

        return redirect()->back();
    }

    public function update(Request $request, $id)
    {

        Role::find($id)->update([
            'slug' => Str::slug($request->name),
            'name' => $request->name,
        ]);

        return redirect()->back()->with('success','Role '. $request->name.' berjaya dikemaskini');
    }

    public function destroy($id)
    {
        try {

            $role = Role::find($id);

            $role->permissions()->detach();

            $role->users()->detach();

            $role->delete();

            session()->flash('success','Role '.$role->name.' berjaya dihapuskan');
            

        } catch (ModelNotFoundException $e) {
            session()->flash('success','Role tidak dijumpai');
        } catch (Exception $e) {
            session()->flash('success',$e->getMessage());
        }

        return redirect()->back();
    }
}
