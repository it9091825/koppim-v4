<?php

namespace App\Http\Controllers\Admin;

use App\Payment;
use App\Traits\SmsTrait;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Validator;
use Storage;
use Carbon\Carbon;
use App\Mongo\Payment as MPayment;
use App\Mongo\LetterJobItems as LetterJobItems;
use App\Mongo\Dividen as Dividen;
use App\Mongo\DividenCalculation as DividenCalculation;
use App\Mongo\DividenWallet as DividenWallet;
use App\Mongo\Smslog;
use Illuminate\Support\Facades\Auth;
use App\Traits\OrderTrait;
use Spatie\Activitylog\Models\Activity;
use Illuminate\Validation\Rule;
use App\user_terminations;

class MemberController extends Controller
{
    use OrderTrait, SmsTrait;

    public function viewDashboard()
    {
        $users = User::where('type', 'user')->where('ikoop', 0)->orderBy('created_at')->get()->groupBy(function ($item) {
            return $item->created_at->format('d/m/Y');
        });
        return view('admin.users.dashboard')->with('users', $users);
    }

    public function index(Request $request)
    {
        $search = $request->input('search');
        if ($search) {

            $members = User::where('type', 'user')->where('name', 'LIKE', '%' . $search . '%')->orWhere('ic', 'LIKE', '%' . $search . '%')->orWhere('mobile_no', 'LIKE', '%' . $search . '%')->orWhere('home_no', 'LIKE', '%' . $search . '%')->sortable(['created_at' => 'desc'])->paginate(20);

            return view('admin.users')->with('members', $members)->with('search', $search);
        } else {

            $status = $request->input('status');

            if ($status != null) {

                $members = User::where('type', 'user')->where('status', $status)->sortable(['created_at' => 'desc'])->paginate(20);
            }
            else {
                $members = User::where('type', 'user')->sortable(['created_at' => 'desc'])->paginate(20);
            }

            return view('admin.users')->with('members', $members)->with('search', $search);
        }
    }


    public function create()
    {

        return view('admin.add-user');
    }

    public function store(Request $request)
    {

        $check = User::where('ic', $request->ic)->first();
        if ($check) {
            return redirect('/admin/members/create')->with('error', 'Pendaftaran Tidak Berjaya, Nombor Kad Pengenalan Sudah Ada Dalam Rekod');
        }

        $day = $request->input('org_dob_day');
        $month = $request->input('org_dob_month');
        $year = $request->input('org_dob_year');

        if ($day == null || $month == null || $year == null) {
            $dob = null;
        } else {
            $dob = Carbon::parse('' . $year . '-' . $month . '-' . $day . ' 00:00:00');
        }

        if ($request->registration_date == null || $request->registration_date == '') {
            $reg_date = null;
        } else {
            $reg_date = $request->registration_date;
        }

        $pin = rand(111111, 999999);
        $password = Hash::make($pin);

        $user = User::create([
            'type' => 'user',
            'name' => strtoupper($request->name),
            'email' => $request->email,
            'ic' => $request->ic,
            'password' => $password,
            'password_reset' => 1,
            'home_no' => $request->home_no,
            'mobile_no' => $request->mobile_no,
            'status' => 0,
            'dob' => $dob,
            'address_line_1' => $request->address_line_1,
            'address_line_2' => $request->address_line_2,
            'address_line_3' => $request->address_line_3,
            'postcode' => $request->postcode,
            'city' => $request->city,
            'state' => $request->state,
            'parliament' => $request->org_parlimen,
            'bank_name' => $request->bank_name,
            'bank_account' => $request->bank_account,
            'bank_account_name' => $request->bank_account_name,
            'registration_type' => 'manual',
            'registration_date' => $reg_date,
            'no_borang' => $request->no_borang
        ]);

        if ($request->hasFile('mykad_front')) {

            $store = Storage::disk('do_spaces')->putFile(env('DO_SPACES_UPLOAD') . 'uploads/ic', request()->mykad_front, 'public');
            $user->mykad_front = $store;
            $user->save();
        }

        if ($request->hasFile('mykad_back')) {

            $store = Storage::disk('do_spaces')->putFile(env('DO_SPACES_UPLOAD') . 'uploads/ic', request()->mykad_back, 'public');
            $user->mykad_back = $store;
            $user->save();
        }

        foreach ($request->amount as $key => $amount) {

            if ($request->amount[$key]) {

                $order_ref = $this->create_order_ref($request->user_ic);
                $payment = new MPayment;
                $payment->name = strtoupper($request->user_name);
                $payment->ic = (string)$request->user_ic;
                $payment->phone = (string)$request->user_mobile_no;
                $payment->email = $request->user_email;
                $payment->amount = (float)$request->amount[$key];
                $payment->share_amount = (float)$request->modalsyer[$key] + (float)$request->additionalsyer[$key];
                $payment->first_time_fees = (float)$request->regfee[$key];
                $payment->first_time_share = (float)$request->modalsyer[$key];
                $payment->additional_share = (float)$request->additionalsyer[$key];
                $payment->status = (string)$request->status[$key];
                //check status
                if ((string)$request->status[$key] == 'payment-completed') {
                    $payment->returncode = '100';
                }

                $payment->type = 'one-time';
                $payment->order_ref = $order_ref;


                $payment->channel = (string)$request->channel[$key];
                //check channel
                if ((string)$request->channel[$key] == 'ikoop') {
                    $payment->payment_desc = 'Migrasi Dari Sistem Ikoop';
                } else {
                    $payment->payment_desc = 'Admin Tambah Transaksi Langganan';
                    //                    $payment->bank = 'bank islam';
                }


                if ($request->hasFile('proof')) {
                    $store = Storage::disk('do_spaces')->putFile(env('DO_SPACES_UPLOAD') . 'upload/payment_proof', $request->proof[$key], 'public');
                    $payment->payment_proof = $store;
                }

                $payment->payment_date = $request->paid_at[$key];
                $payment->note = $request->remark[$key];
                $payment->user_id = $request->user_id;
                $payment->save();

                activity('admin_add_payment_transaction')
                    ->withProperties(
                        [
                            'payment_id' => $payment->id,
                            'payment_name' => $payment->name,
                            'payment_ic' => $payment->ic,
                            'payment_email' => $payment->email,
                            'payment_phone' => $payment->phone,
                            'Oleh' => Auth::User()->name,
                            'Tarikh' => Carbon::now(),
                        ]
                    )
                    ->log('Admin Tambah Transaksi Langganan Baharu');
            }
        }

        //        send sms/email
        try {
            $text = "Permohonan anda telah diterima. Log masuk akaun di anggota.koppim.com.my dengan kata laluan sementara " . $pin . " & kemaskini profil anda.";
            $this->send_sms($request->ic, $request->mobile_no, $text);
            //send email
            Mail::to($request->email)->send(new NewRegistration(strtoupper($request->name), $request->ic, $request->mobile_no, $request->email, $user->created_at, $request->first_time_share, $request->additional_share, $request->amount));
        } catch (\Exception $e) {
            activity('exception')->log($e->getMessage());
        }

        activity('admin_member_create')
            ->performedOn($user)
            ->withProperties(
                [
                    'Oleh' => Auth::User()->name,
                    'Tarikh' => Carbon::now(),
                ]
            )
            ->log('Pendaftaran Anggota');

        return redirect('/admin/members/create')->with('success', 'Pendaftaran Berjaya');
    }

    public function view(Request $request, $user_id)
    {

        //For button kembali
        if (!strpos(url()->previous(), '/admin/members/view')) {
            Session::put('admin_member_back', url()->previous());
        }

        $user = User::where('id', $user_id)->where('type', 'user')->firstOrFail();
        $fee = MPayment::where('ic', $user->ic)->where('returncode', '100')->sum('first_time_fees');
        $share = MPayment::where('ic', $user->ic)->where('returncode', '100')->sum('first_time_share');
        $addShare = MPayment::where('ic', $user->ic)->where('returncode', '100')->sum('additional_share');
        $smslogs = Smslog::where('ic', $user->ic)->get();
        $payments = MPayment::where('ic', $user->ic)->orderBy('created_at', 'desc')->get();
        $successPayments = MPayment::where('ic', $user->ic)->where('returncode', '100')->where('status', 'payment-completed')->get();
        $statuslogs = Activity::where('log_name', 'admin_member_status_edit')->where('subject_type', 'App\User')->where('subject_id', $user_id)->orderBy('created_at', 'desc')->get();


        $surats = LetterJobItems::where('ic', $user->ic)->orderBy('created_at', 'DESC')->get();

        $dividens = DividenWallet::where('ic', (int)$user->ic)->orderBy('created_at', 'DESC')->get();

        if ($user->status == 96) {
            $terminations = user_terminations::where('user_id', $user_id)->firstOrFail();
        } else {
            $terminations = [];
        }
        // get previous user id
        $previous = User::where('id', '<', $user->id)->where('type', 'user')->max('id');

        // get next user id
        $next = User::where('id', '>', $user->id)->where('type', 'user')->min('id');

        return view('admin.edit-user')
            ->with('user', $user)
            ->with('share', $share)
            ->with('addShare', $addShare)
            ->with('fee', $fee)
            ->with('smslogs', $smslogs)
            ->with('payments', $payments)
            ->with('statuslogs', $statuslogs)
            ->with('previous', $previous)
            ->with('next', $next)
            ->with('successPayments', $successPayments)
            ->with('terminations', $terminations)
            ->with('surats', $surats)
            ->with('statements', $dividens);
    }

    public function edit_account(Request $request, $user_id)
    {

        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'mobile_no' => 'required',
            'name' => 'required',
            'ic' => [
                'required',
                Rule::unique('users')->ignore($user_id),
            ],
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            if ($errors->has('ic')) {
                session()->flash('error', 'No. Mykad sudah wujud dalam sistem! Sila semak No. Mykad tersebut.');
            } else {
                session()->flash('error', 'Terdapat ralat sewaktu kemaskini maklumat anggota');
            }
            return redirect()->route('admin.member.view', $user_id)->withErrors($validator);
        } else {

            $day = $request->input('org_dob_day');
            $month = $request->input('org_dob_month');
            $year = $request->input('org_dob_year');

            if ($day == null || $month == null || $year == null) {
                $dob = null;
            } else {
                $dob = Carbon::parse('' . $year . '-' . $month . '-' . $day . ' 00:00:00');
            }

            $user = User::findOrFail($user_id);

            $saveStatus = User::where('id', $user_id)->update([
                'name' => $request->input('name'),
                'email' => $request->input('email'),
                'mobile_no' => $request->input('mobile_no'),
                'home_no' => $request->input('home_no'),
                'dob' => $dob,
                'registration_date' => $request->input('registration_date'),
                'no_borang' => $request->input('no_borang'),
                'ic' => $request->input('ic'),
            ]);

            if ($saveStatus && $user->ic != $request->input('ic')) {
                foreach ($user->payments as $payment) {
                    $payment->ic = (string) $request->input('ic');
                    $payment->save();
                }
            }

            activity('admin_member_edit')
                ->performedOn($user)
                ->withProperties(
                    [
                        'old' => [
                            'Nama' => $user->name,
                            'Emel Anggota' => $user->email,
                            'No. Telefon Bimbit' => $user->mobile_no,
                            'No. Telefon Rumah' => $user->home_no,
                            'Tarikh Pendaftaran' => $user->registration_date,
                            'No. Borang' => $user->no_borang,
                            'Tarikh Lahir' => $user->dob,
                            'No. MyKad' => $user->ic,
                        ],
                        'new' => [
                            'Nama' => $request->input('name'),
                            'Emel Anggota' => $request->input('email'),
                            'No. Telefon Bimbit' => $request->input('mobile_no'),
                            'No. Telefon Rumah' => $request->input('home_no'),
                            'Tarikh Pendaftaran' => $request->input('registration_date'),
                            'No. Borang' => $request->input('no_borang'),
                            'Tarikh Lahir' => $dob,
                            'No. MyKad' => $request->input('ic'),
                        ]
                    ]
                )
                ->log('Kemaskini Akaun');

            return redirect()->route('admin.member.view', $user_id)->with('success', 'Kemaskini Berjaya');
        }
    }

    public function edit_detail(Request $request, $user_id)
    {
        $user = User::findOrFail($user_id);


        User::where('id', $user_id)->update([
            'address_line_1' => strtoupper($request->input('address_line_1')),
            'address_line_2' => strtoupper($request->input('address_line_2')),
            'address_line_3' => strtoupper($request->input('address_line_3')),
            'postcode' => $request->input('postcode'),
            'city' => strtoupper($request->input('city')),
            'state' => strtoupper($request->input('state')),
            'sex' => strtoupper($request->input('sex')),
            'race' => strtoupper($request->input('race')),
            'religion' => strtoupper($request->input('religion')),
            'marital_status' => strtoupper($request->input('marital_status')),
            'job' => strtoupper($request->input('job')),
            'monthly_income' => strtoupper($request->input('monthly_income')),
            'parliament' => $request->input('org_parlimen')

        ]);


        activity('admin_member_edit')
            ->performedOn($user)
            ->withProperties(
                [
                    'old' => [
                        'Alamat Baris 1' => $user->address_line_1,
                        'Alamat Baris 2' => $user->address_line_2,
                        'Alamat Baris 3' => $user->address_line_3,
                        'Poskod' => $user->postcode,
                        'Daerah' => $user->city,
                        'Negeri' => $user->state,
                        'Jantina' => $user->sex,
                        'Bangsa' => $user->race,
                        'Agama' => $user->religion,
                        'Status Perkahwinan' => $user->marital_status,
                        'Pekerjaan' => $user->job,
                        'Pendapatan Bulanan Di Bawah' => $user->monthly_income,
                    ],
                    'new' => [
                        'Alamat Baris 1' => strtoupper($request->input('address_line_1')),
                        'Alamat Baris 2' => strtoupper($request->input('address_line_2')),
                        'Alamat Baris 3' => strtoupper($request->input('address_line_3')),
                        'Poskod' => $request->input('postcode'),
                        'Daerah' => strtoupper($request->input('city')),
                        'Negeri' => strtoupper($request->input('state')),
                        'Jantina' => strtoupper($request->input('sex')),
                        'Bangsa' => strtoupper($request->input('race')),
                        'Agama' => strtoupper($request->input('religion')),
                        'Status Perkahwinan' => strtoupper($request->input('marital_status')),
                        'Pekerjaan' => strtoupper($request->input('job')),
                        'Pendapatan Bulanan Di Bawah' => strtoupper($request->input('monthly_income')),
                    ]
                ]
            )
            ->log('Kemaskini Maklumat Peribadi');

        return redirect()->route('admin.member.view', $user_id)->with('success', 'Kemaskini Berjaya');
    }

    public function edit_password(Request $request, $user_id)
    {

        $validator = Validator::make($request->all(), [
            'pass' => 'required|min:6',
        ]);

        if ($validator->fails()) {
            return redirect()->route('admin.member.view', $user_id)->withErrors($validator);
        } else {
            $user = User::findOrFail($user_id);

            User::where('id', $user_id)->update([
                'password' => Hash::make($request->input('pass'))
            ]);

            activity('admin_member_edit')
                ->performedOn($user)
                ->withProperties(
                    [
                        'old' => [
                            'Kata Laluan' => '',
                        ],
                        'new' => [
                            'Kata Laluan' => '',
                        ]
                    ]
                )
                ->log('Kemaskini Kata Laluan');

            return redirect()->route('admin.member.view', $user_id)->with('success', 'Kemaskini Berjaya');
        }
    }

    public function edit_attachment(Request $request, $user_id)
    {

        if ($request->hasFile('mykad_front')) {

            $store = Storage::disk('do_spaces')->putFile(env('DO_SPACES_UPLOAD') . 'uploads/ic', request()->mykad_front, 'public');

            $attach = User::where('id', $user_id)->first();
            Storage::disk('do_spaces')->delete($attach->mykad_front);

            $user = User::findOrFail($user_id);

            User::where('id', $user_id)->update([
                'mykad_front' => $store
            ]);

            activity('admin_member_edit')
                ->performedOn($user)
                ->withProperties(
                    [
                        'old' => [
                            'MyKad Hadapan' => '',
                        ],
                        'new' => [
                            'MyKad Hadapan' => '',
                        ]
                    ]
                )
                ->log('Kemaskini MyKad Hadapan');
        }

        // if ($request->hasFile('mykad_back')) {

        //     $store = Storage::disk('do_spaces')->putFile(env('DO_SPACES_UPLOAD') . 'uploads/ic', request()->mykad_back, 'public');

        //     $attach = User::where('id', $user_id)->first();
        //     Storage::disk('do_spaces')->delete($attach->mykad_back);

        //     $user = User::findOrFail($user_id);

        //     User::where('id', $user_id)->update([
        //         'mykad_back' => $store
        //     ]);

        //     activity('admin_member_edit')
        //         ->performedOn($user)
        //         ->withProperties(
        //             [
        //                 'old' => [
        //                     'MyKad Belakang' => '',
        //                 ],
        //                 'new' => [
        //                     'MyKad Belakang' => '',
        //                 ]
        //             ]
        //         )
        //         ->log('Kemaskini MyKad Belakang');
        // }

        return redirect()->route('admin.member.view', $user_id)->with('success', 'Kemaskini Berjaya');
    }

    public function edit_bank(Request $request, $user_id)
    {
        $user = User::findOrFail($user_id);

        if ($request->hasFile('bank_attachment')) {
            $store = Storage::disk('do_spaces')->putFile(env('DO_SPACES_UPLOAD') . 'uploads/bank', request()->bank_attachment, 'public');

            $attach = User::where('id', $user_id)->first();
            Storage::disk('do_spaces')->delete($attach->bank_attachment);

            User::where('id', $user_id)->update([
                'bank_name' => $request->input('bank_name'),
                'bank_account' => $request->input('bank_account'),
                'bank_account_name' => $request->input('bank_account_name'),
                'bank_attachment' => $store,
                'blacklist' => $request->input('blacklist'),
                'blacklist_dividend' => $request->input('blacklist_dividend')

            ]);
        } else {
            User::where('id', $user_id)->update([
                'bank_name' => $request->input('bank_name'),
                'bank_account' => $request->input('bank_account'),
                'bank_account_name' => $request->input('bank_account_name'),
                'blacklist' => $request->input('blacklist'),
                'blacklist_dividend' => $request->input('blacklist_dividend')
            ]);
        }

        activity('admin_member_edit')
            ->performedOn($user)
            ->withProperties(
                [
                    'old' => [
                        'Bank' => $user->bank_name,
                        'No. Akaun' => $user->bank_account,
                        'Nama Akaun' => $user->bank_account_name,
                    ],
                    'new' => [
                        'Bank' => $request->input('bank_name'),
                        'No. Akaun' => $request->input('bank_account'),
                        'Nama Akaun' => $request->input('bank_account_name'),
                    ]
                ]
            )
            ->log('Kemaskini Akaun Bank');

        return redirect()->route('admin.member.view', $user_id)->with('success', 'Kemaskini Berjaya');
    }

    public function edit_status(Request $request, $user_id)
    {
        $userLog = User::findOrFail($user_id);
        $user = User::findOrFail($user_id);
        $user->status = $request->input('status');
        $user->status_reason = $request->input('status_reason');

        //if aktif(lulus), generate no_koppim
        if ($request->input('status') == "1" || $request->input('status') == 1) {
            //get last no_koppim number
            $last_no = User::orderBy('no_koppim', 'desc')->first();
            //check if user already have no_koppim to prevent from regenerate new number
            if ($user->no_koppim == null || $user->no_koppim == '') {
                //generate no_koppim for user when approved(status=1)
                $user->no_koppim = $last_no->no_koppim + 1;
            }
        }

        $user->save();

        activity('admin_member_edit')
            ->performedOn($userLog)
            ->withProperties(
                [
                    'old' => [
                        'Status' => $userLog->status,
                        'Justifikasi Penukaran Status' => $userLog->status_reason,
                    ],
                    'new' => [
                        'Status' => $request->input('status'),
                        'Justifikasi Penukaran Status' => $request->input('status_reason'),
                    ]
                ]
            )
            ->log('Kemaskini Akaun Bank');

        // kalau nak enable bawah ni, kena check user dah ada no_koppim, takyah hantar dah emel
        // if ($request->status == '1' || $request->status == 1) {
        //     try {

        //         $smsText = "Tahniah! Pendaftaran anda sebagai Anggota KoPPIM telah berjaya. Sila semak emel bagi mendapatkan maklumat Keanggotaan KoPPIM anda. Terima kasih.";

        //         $this->send_sms($user->mobile_no, $smsText);


        //         \Mail::to($user->email)->send(new NewRegistration($user->name, $user->ic, $user->mobile_no, $user->email, $user->no_koppim));


        //     } catch (Exception $e) {
        //         \Log::info($e->getMessage());
        //     }
        // }


        return redirect()->route('admin.member.view', $user_id)->with('success', 'Kemaskini Berjaya');
    }

    public function changeStatusUntukDisemak(User $user){
        if(!strpos(url()->previous(), '/admin/members/view'))
        {
            Session::put('admin_member_back', url()->previous());
        }

        $user->status = 2;
        $user->save();

        return redirect(Session::get('admin_member_back'))->with('success', 'Status anggota telah ditukar kepada untuk disemak');
    }
}
