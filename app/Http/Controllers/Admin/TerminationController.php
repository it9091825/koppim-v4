<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Traits\OrderTrait;
use App\Traits\SmsTrait;
use Carbon\Carbon;
use App\user_terminations;
use App\Mail\User\UserTermination;
use App\Mail\User\UserReactivation;
use Spatie\Activitylog\Models\Activity;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Log;
use App\Traits\MemberTrait;
use App\Traits\NexmoTrait;

class TerminationController extends Controller
{
    use OrderTrait, SmsTrait, MemberTrait;

    public function index(Request $request){

        $search = $request->input('search');

        if($search) {
            $members = User::where('type', 'user')->whereIn('status', [96, 97, 98])->where('name', 'LIKE', '%' . $search . '%')->orWhere('ic', 'LIKE', '%' . $search . '%')->orWhere('mobile_no', 'LIKE', '%' . $search . '%')->orWhere('home_no', 'LIKE', '%' . $search . '%')->sortable()->paginate(20);

            return view('admin.terminations.terminate-applicants')->with('members', $members)->with('search', $search);
        } else {
            $status = $request->input('status');
            if ($status !== null) {
                if($status == 'all') {
                    $members = User::where('type', 'user')->whereIn('status', [96, 97, 98])->sortable()->paginate(20);
                } else {
                    $members = User::where('type', 'user')->where('status', $status)->orderBy('updated_at', 'DESC')->paginate(20);
                }
            } else {
                $members = User::where('type', 'user')->where('status', 96)->sortable()->paginate(20);
            }


            return view('admin.terminations.terminate-applicants')->with('members', $members)->with('search', $search);
        }
    }

    public function approve_tmt(Request $request)
    {
        $user = User::findorFail($request->user_id);
        if($request->type == 'lulus') {
            $new_status = 97;
            $new_reason = 'Berhenti Keanggotaan';
            User::where('id', $request->user_id)->update([
                'status' => $new_status,
                'status_reason' => $new_reason
            ]);
        } else {
            $new_status = 1;
            $new_reason = '';
            User::where('id', $request->user_id)->update([
                'status' => $new_status,
                'status_reason' => $new_reason
            ]);
        }

        activity('admin_member_edit')
            ->performedOn($user)
            ->withProperties(
                [
                    'old' => [
                        'status' => $user->status,
                        'status_reason' => $user->status_reason,
                    ],
                    'new' => [
                        'status' => $new_status,
                        'status_reason' => $new_reason,
                    ]
                ]
            )
            ->log('Berhenti Anggota');

        if($new_status == 97) {
            try {
                $text = "Permohonan untuk pembatalan akaun anda telah diluluskan. Akaun anda telah dinyahaktif.";
                $this->send_sms($user->ic, $user->mobile_no, $text);
                Mail::to($user->email)->send(new UserTermination(strtoupper($user->name), $user->ic, $user->mobile_no, $user->email));
            } catch (\Exception $e){
                activity('exception')->log($e->getMessage());
            }
        } else {
            try {
                $text = "Permohonan untuk pembatalan akaun anda telah gagal. Sila hubungi KoPPIM untuk maklumat lanjut";
                $this->send_sms($user->ic, $user->mobile_no, $text);
                 Mail::to($user->email)->send(new UserTermination(strtoupper($user->name), $user->ic, $user->mobile_no, $user->email));
            } catch (\Exception $e){
                activity('exception')->log($e->getMessage());
            }
        }



        return redirect()->route('admin.member.termination')->with('success', 'Kemaskini Berjaya');

    }

    public function revoke_tmt(Request $request)
    {
        $user = User::findorFail($request->user_id);
        if($request->type == 'lulus') {
            $new_status = 1;
            $new_reason = 'Pengaktifan Semula';
            User::where('id', $request->user_id)->update([
                'status' => $new_status,
                'status_reason' => $new_reason
            ]);
        } else {
            $new_status = 97;
            $new_reason = 'Reactivation Denied';
            User::where('id', $request->user_id)->update([
                'status' => $new_status,
                'status_reason' => $new_reason
            ]);
        }

        activity('admin_member_edit')
            ->performedOn($user)
            ->withProperties(
                [
                    'old' => [
                        'status' => $user->status,
                        'status_reason' => $user->status_reason,
                    ],
                    'new' => [
                        'status' => $new_status,
                        'status_reason' => $new_reason,
                    ]
                ]
            )
            ->log('Pengaktifan Anggota');

        if($new_status == 1) {
            try {
                $text = "Akaun anda telah diaktifkan semula. Sila daftar masuk ke akaun ada untuk melihat status semasa.";
                $this->send_sms($user->ic, $user->mobile_no, $text);
                Mail::to($user->email)->send(new UserReactivation(strtoupper($user->name), $user->ic, $user->mobile_no, $user->email));
            } catch (\Exception $e){
                activity('exception')->log($e->getMessage());
            }
        } else {
            try {
                $text = "Permohonan untuk pengaktifan semula akaun anda telah gagal. Sila hubungi KoPPIM untuk maklumat lanjut";
                $this->send_sms($user->ic, $user->mobile_no, $text);
                 Mail::to($user->email)->send(new UserTermination(strtoupper($user->name), $user->ic, $user->mobile_no, $user->email));
            } catch (\Exception $e){
                activity('exception')->log($e->getMessage());
            }
        }



        return redirect()->route('admin.member.termination')->with('success', 'Kemaskini Berjaya');

    }
}
