<?php

namespace App\Http\Controllers\Admin\Affiliates;

use App\Exports\Affiliate\WithdrawalExport;
use App\Http\Controllers\Controller;
use App\Jobs\ProcessCommissionForWithdrawal;
use App\Mongo\CommissionWithdraw;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Session;
use Maatwebsite\Excel\Facades\Excel;
use Storage;
use PDF;
use Validator;

class WithdrawController extends Controller
{
    public function index(Request $request)
    {
        $search = $request->input('search');

        if ($search) {

            $withdraws = CommissionWithdraw::where('name', 'LIKE', '%' . $search . '%')->orWhere('ic', 'LIKE', '%' . $search . '%')->sortable()->paginate(20);
        } else {

            $status = $request->input('status');

            if ($status !== null) {
                $withdraws = CommissionWithdraw::where('status', $status)->sortable()->paginate(20);
            } else {
                $withdraws = CommissionWithdraw::sortable()->paginate(20);
            }
        }

        $pageTitle = 'Senarai Pengeluaran';
        return view('affiliates.pengeluaran', compact('withdraws', 'pageTitle','search'));
    }

    public function detail(Request $request)
    {
        if(!strpos(url()->previous(), 'admin/affiliates/pengeluaran/terperinci'))
        {
            Session::put('admin_member_back', url()->previous());
        }

        $withdraw = CommissionWithdraw::where('_id', $request->id)->first();
        return view('affiliates.pengeluaran-detail')->with('withdraw', $withdraw);
    }

    public function approval(Request $request)
    {
        if(!strpos(url()->previous(), 'admin/affiliates/pengeluaran/terperinci'))
        {
            Session::put('admin_member_back', url()->previous());
        }

        $id = $request->input('id');

        $messages = [
            'transaction_ref.required' => 'Sila Isi Rujukan Transaksi',
        ];

        $validator = Validator::make($request->all(), [
            'transaction_ref' => 'required',
        ],$messages);

        if ($validator->fails()) {
            return redirect('admin/affiliates/pengeluaran/terperinci?id='.$id)->withErrors($validator);
        } else {

            $ref = $request->input('transaction_ref');


            $withdraw = CommissionWithdraw::where('_id', $id)->first();

            if ($request->kelulusan == 'lulus') {
                if ($request->hasFile('tx_receipt') == true) {

                    $store = Storage::disk('do_spaces')->putFile(env('DO_SPACES_UPLOAD') . 'uploads/withdrawreceipt', request()->tx_receipt, 'public');

                    CommissionWithdraw::where('_id', $id)->update([
                        'payment_receipt' => $store
                    ]);
                }

                CommissionWithdraw::where('_id', $id)->update([
                    'payment_ref' => $ref,
                    'status' => "paid",
                    'admin_id' => Auth::user()->id,
                    'admin_datetime' => new \MongoDB\BSON\UTCDateTime(new \DateTime('now')),
                ]);

                foreach ($withdraw->payments as $payment) {
                    $payment->introducer_commission_status = 'redeemed'; //Belum Ditebus = available //Sudah Ditebus = redeemed, Dalam Process = processing
                    $payment->save();
                }

                //Not used since we're using it on affiliatecontroller when user click download button
//            $statement = CommissionWithdraw::find($id);
//            $data = [
//                'statement' => $withdraw
//            ];
//
//            $pdf = PDF::loadView('affiliates.print-statement',$data)->setOptions(['defaultPaperSize', 'a4']);
//            $commission_statement = Storage::disk('do_spaces')->put(env('DO_SPACES_UPLOAD') . 'uploads/commissionstatement/'.$withdraw->reference_no.'.pdf', $pdf->output(), 'public');
//            $withdraw->update([
//                'commission_statement' => $commission_statement
//            ]);

            } else {

                CommissionWithdraw::where('_id', $id)->update([
                    'status' => "cancelled",
                    'admin_id' => Auth::user()->id,
                    'admin_datetime' => new \MongoDB\BSON\UTCDateTime(new \DateTime('now')),
                ]);

                //reset back all the payments under the withdraw to available
                foreach ($withdraw->payments as $payment) {
                    $payment->introducer_commission_status = 'available'; //Belum Ditebus = available //Sudah Ditebus = redeemed, Dalam Process = processing
                    $payment->save();
                }
            }


            return redirect(Session::get('admin_member_back'));
        }
    }

    public function export(Request $request){
        if($request->status){
            $status = $request->status;
        } else {
            $status = 'all';
        }

        return (new WithdrawalExport($status))->download('pengeluaran.xlsx');
    }

    public function processCommissionForWithdrawal(Request $request){
        ProcessCommissionForWithdrawal::dispatch();
        session()->flash('success', 'Pengeluaran Sedang Dijana.');
        return redirect('/admin/affiliates/pengeluaran');
    }
}
