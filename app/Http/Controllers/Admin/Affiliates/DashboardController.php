<?php

namespace App\Http\Controllers\Admin\Affiliates;

use App\Http\Controllers\Controller;
use App\Mongo\CommissionWithdraw as CommissionWithdraw;
use App\Mongo\Payment as MPayment;
use Illuminate\Http\Request;
use App\User;

class DashboardController extends Controller
{
    public function index(Request $request)
    {

        $permohonan_baharu = User::where('is_affiliate','apply')->count();
        $jumlah_ejen = User::where('is_affiliate','yes')->count();

        //Yg ni dia check payment tu ada introducer sahaja tapi commission belum generate lagi jika user belum diluluskan.
//        $jumlah_komisen = MPayment::where('returncode', '100')->where('introducer', true)->count();

        $jumlah_komisen = MPayment::where('returncode', '100')->where('introducer', true)->where('introducer_commission','>',0)->count();
        $jumlah_nilai_komisen = MPayment::where('returncode', '100')->where('introducer', true)->sum('introducer_commission');

        $transaksi_pengeluaran_baharu = CommissionWithdraw::where('status', 'processing')->sum('amount');
        $transaksi_pengeluaran_batal= CommissionWithdraw::where('status', 'cancelled')->count();
        $transaksi_pengeluaran_lulus= CommissionWithdraw::where('status', 'paid')->count();
        $transaksi_pengeluaran_lulus_nilai = CommissionWithdraw::where('status', 'paid')->sum('amount');

        return view('affiliates.dashboard')
            ->with('permohonan_baharu', $permohonan_baharu)
            ->with('jumlah_ejen', $jumlah_ejen)
            ->with('jumlah_komisen', $jumlah_komisen)
            ->with('jumlah_nilai_komisen', $jumlah_nilai_komisen)
            ->with('transaksi_pengeluaran_baharu', $transaksi_pengeluaran_baharu)
            ->with('transaksi_pengeluaran_batal', $transaksi_pengeluaran_batal)
            ->with('transaksi_pengeluaran_lulus', $transaksi_pengeluaran_lulus)
            ->with('transaksi_pengeluaran_lulus_nilai', $transaksi_pengeluaran_lulus_nilai);

    }
}
