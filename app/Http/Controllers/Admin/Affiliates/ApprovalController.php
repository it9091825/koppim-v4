<?php

namespace App\Http\Controllers\Admin\Affiliates;

use App\Http\Controllers\Controller;
use App\Mongo\Payment as MPayment;
use Illuminate\Http\Request;
use App\User;

class ApprovalController extends Controller
{
    public function for_approval(Request $request)
    {
        $search = $request->input('search');

        if ($search) {
            $users = User::where('is_affiliate', 'yes')
                ->where('name', 'LIKE', '%' . $search . '%')
                ->orWhere('ic', 'LIKE', '%' . $search . '%')->orWhere('mobile_no', 'LIKE', '%' . $search . '%')->orWhere('home_no', 'LIKE', '%' . $search . '%')
                ->sortable()
                ->paginate(20);
        } else {
            $users = User::where('is_affiliate', 'yes')->sortable()->paginate(20);
        }

        return view('affiliates.transaction')->with('users', $users)->with('search', $search)
            ->with('pageTitle', 'Senarai Rekod Komisen');
    }

    public function view_commissions($no_koppim)
    {
        $introducer = User::where('no_koppim', (string)$no_koppim)->first();
        $transactions = MPayment::where('introducer_no_anggota', (string)$no_koppim)
            ->where('returncode', '100')
            ->where('introducer', true)
            ->paginate();

        return view('affiliates.senarai-komisen-ejen')->with('transactions', $transactions)
            ->with('pageTitle', 'Senarai Komisen Ejen ' . ' - ' . $introducer->name ?? '');
    }
    
}
