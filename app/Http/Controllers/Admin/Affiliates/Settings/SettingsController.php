<?php

namespace App\Http\Controllers\Admin\Affiliates\Settings;

use App\AffiliateSetting;
use App\Http\Controllers\Controller;
use App\Traits\MemberTrait;
use App\UserAffiliateSetting;
use Illuminate\Http\Request;
use App\User;

use Illuminate\Support\Facades\DB;

class SettingsController extends Controller
{


    public function index()
    {
        $settings = AffiliateSetting::first();
        $userSettings = UserAffiliateSetting::paginate(10);
        return view('affiliates.settings', compact('settings','userSettings'));
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function find(Request $request)
    {

        $term = trim($request->q);

        if (empty($term)) {
            return response()->json([]);
        }

        $users = User::where('type', 'user')->where('is_affiliate','yes')->where(function ($query) use ($term) {
            $query->where('name', 'LIKE', '%' . $term . '%')
                ->orWhere('ic', 'LIKE', '%' . $term . '%')
                ->orWhere('mobile_no', 'LIKE', '%' . $term . '%');
        })->get();

        $formatted_tags = [];

        foreach ($users as $user) {
            $formatted_tags[] = ['id' => $user->id, 'text' => $user->name];
        }

        return response()->json($formatted_tags);
    }

    public function storeGeneral(Request $request){
        $settings = AffiliateSetting::first();
        $settings->registration_amount = (double)$request->registration_amount;
        $settings->save();
        return redirect()->back();
    }

    public function storeAgent(Request $request){
//        dd($request->all());
        $check = UserAffiliateSetting::where('user_id',$request->q)->first();
        if(!$check){
            $userSetting = new UserAffiliateSetting;
            $userSetting->user_id = $request->q;
            $userSetting->registration_amount = (double)$request->registration_amount;
            $userSetting->save();
        }
        return redirect()->back();
    }

    public function editAgent(Request $request){
//        dd($request->all());
        $userSetting = UserAffiliateSetting::findOrFail($request->user_setting_id);
        if($request->type == 'tukar'){
            $userSetting->registration_amount = (double)$request->registration_amount;
            $userSetting->save();
        } else {
            $userSetting->delete();
        }
        return redirect()->back();
    }
}
