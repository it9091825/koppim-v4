<?php

namespace App\Http\Controllers\Admin\Affiliates;

use App\Http\Controllers\Controller;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class NewApplicationController extends Controller
{
    public function index(Request $request)
    {
        $search = $request->input('search');
        if ($search) {
            $users = User::whereIn('is_affiliate', ['yes', 'apply', 'reject'])
                ->where('name', 'LIKE', '%' . $search . '%')
                ->orWhere('ic', 'LIKE', '%' . $search . '%')->orWhere('mobile_no', 'LIKE', '%' . $search . '%')->orWhere('home_no', 'LIKE', '%' . $search . '%')
                ->sortable()->orderBy('affiliate_apply', 'asc')
                ->paginate(20);
        } else {
            $status = $request->input('status');

            if ($status !== null) {
                $users = User::where('is_affiliate', $status)->sortable()->orderBy('affiliate_apply', 'asc')->paginate(20);
            } else {
                $users = User::whereIn('is_affiliate', ['yes', 'apply', 'reject'])->sortable()->orderBy('affiliate_apply', 'asc')->paginate(20);
            }
        }


        return view('affiliates.senarai-pemohon')->with('users', $users)->with('search',$search)
            ->with('pageTitle', 'Senarai Permohonan');
    }

    public function kelulusan(Request $request)
    {
        //        dd($request->all());
        $user = User::findOrFail($request->user_id);
        if ($request->type == 'lulus') {
            $user->is_affiliate = 'yes';
        } else {
            $user->is_affiliate = 'reject';
        }
        $user->affiliate_apply_result = Carbon::now();
        $user->save();
        return redirect()->back();
    }

    public function lulusSemua(Request $request){
        if($request->users){
            foreach($request->users as $user){
                User::find($user)->update(['is_affiliate' => 'yes', 'affiliate_apply_result' => Carbon::now() ]);
            }

            return response()->json([
                'status' => 'Selesai Luluskan Semua Permohonan',
            ]);
        }

        return response()->json([
            'status' => 'Ralat',
        ]);
    }

    public function batalSemua(Request $request){
        if($request->users){
            foreach($request->users as $user){
                User::find($user)->update(['is_affiliate' => 'reject', 'affiliate_apply_result' => Carbon::now()]);
            }

            return response()->json([
                'status' => 'Selesai Batalkan Semua Permohonan',
            ]);
        }

        return response()->json([
            'status' => 'Ralat',
        ]);
    }
}
