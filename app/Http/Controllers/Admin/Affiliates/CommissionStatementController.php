<?php

namespace App\Http\Controllers\Admin\Affiliates;

use App\Http\Controllers\Controller;
use App\Mongo\CommissionWithdraw;
use Carbon\Carbon;
use Illuminate\Http\Request;
use PDF;

class CommissionStatementController extends Controller
{
    public function index(Request $request, $user_id)
    {
        $current_month = Carbon::now();
        $year = $request->input('year');
        $commission_query = CommissionWithdraw::where('user_id', (int)$user_id);

        if ($request->filled('year')) {

            $start = Carbon::parse($year.'-01-01 00:00:00');
            $end = Carbon::parse($year . '-12-31 23:59:59');

            $list = $commission_query->whereBetween('created_at', [$start, $end])->get();
        } else {

            $start = Carbon::parse($current_month->format('Y').'-01-01 00:00:00');

            $end = Carbon::parse($current_month->format('Y').'-12-31 23:59:59');

            $list = $commission_query->whereBetween('created_at', [$start, $end])->get();
        }

        return view('affiliates.admin-commission-statement', compact('list','user_id'));
    }

    public function print($user_id,$_id)
    {
        //Not secure, user can type anything for the url id and get other user statement
//        $statement = CommissionWithdraw::findOrFail($id);

        $statement = CommissionWithdraw::where('_id',$_id)->where('user_id',(int)$user_id)->first();

        $data = [
            'statement' => $statement
        ];

        $pdf = PDF::loadView('affiliates.print-statement',$data);


//        $store = Storage::disk('do_spaces')->put(env('DO_SPACES_UPLOAD') . 'uploads/commissionstatement/'.$statement->reference_no.'.pdf', $pdf->output(), 'public');

        return $pdf->download('penyata_komisen.pdf');

    }
}
