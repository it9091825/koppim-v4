<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Mail\User\NewRegistration;
use App\Payment;
use App\PaymentProof;
use App\temp_paid_data;
use App\Traits\SmsTrait;
use App\User;
use Auth;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Mail;
use Storage;
use Validator;
use App\Mongo\Payment as MPayment;
use Carbon\Carbon;

class AdminLoggedController extends Controller
{
    use SmsTrait;

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function home(Request $request)
    {
        // the widgets used here are using package Arrilot\Widgets. You guys can refer the documentation here https://github.com/arrilot/laravel-widgets

        return view('admin.dashboard');
    }


    public function donaters(Request $request)
    {

        $search = $request->input('search');

        if ($search) {
            $donates = Payment::where('returncode', 100)->where('phone', 'LIKE', '%' . $search . '%')->orWhere('email', 'LIKE', '%' . $search . '%')->orWhere('name', 'LIKE', '%' . $search . '%')->orderBy('name', 'ASC')->paginate(50);
        } else {
            $donates = Payment::where('returncode', 100)->orderBy('name', 'ASC')->paginate(50);
        }

        return view('admin.penyumbang')->with('donates', $donates);
    }

    public function donater_detail($mobile_no)
    {

        $donater = Payment::where('phone', $mobile_no)->orderBy('id', 'DESC')->first();

        $donates = Payment::where('phone', $mobile_no)->where('returncode', 100)->orderBy('id', 'DESC')->get();

        return view('admin.penyumbang-detail')->with('donater', $donater)->with('donates', $donates);
    }

    public function logout()
    {
        $user = Auth::User();
        activity('admin_auth')
            ->performedOn($user)
            ->withProperties(
                [
                    'Nama Admin' => strtoupper($user->name),
                    'Emel' => $user->email,
                ]
            )
            ->log('Admin Log Keluar');
        Auth::logout();

        return redirect('/admin/login');
    }

    public function temporary_data_cal(Request $request)
    {

        $temps = temp_paid_data::where('cron', 0)->limit(2000)->get();

        $array = array();

        foreach ($temps as $temp) {

            array_push($array, $temp);
        }

        return $array;
    }

    public function temporary_data_cal_2(Request $request)
    {

        $temps = Payment::where('campaign_id', 3)->where('gateway', 'senangpay')->limit(5000)->get();

        foreach ($temps as $temp) {

            Payment::where('id', $temp->id)->update([
                'campaign_id' => 2,
            ]);
        }
    }

    public function txs(Request $request)
    {
        $search = $request->input('search');

        if (env('USE_MPAYMENT_ADMIN', 'true')) {
            if ($search) {
                $orgs = MPayment::where('returncode', '100')
                    ->where(function ($query) use ($search) {
                        $query->where('name', 'LIKE', '%' . $search . '%')
                            ->orWhere('ic', 'LIKE', '%' . $search . '%')->orWhere('mobile_no', 'LIKE', '%' . $search . '%')->orWhere('home_no', 'LIKE', '%' . $search . '%');
                    })
                    ->orderBy('created_at', 'DESC')->paginate(20);
            } else {
                $orgs = MPayment::where('returncode', '100')
                    ->orderBy('created_at', 'DESC')->paginate(20);
            }

            $jumlah = MPayment::where('returncode', '100')->sum('amount');
            $share_amount = MPayment::where('returncode', '100')->sum('share_amount');
            $first_time_fees = MPayment::where('returncode', '100')->sum('first_time_fees');
        } else {
            if ($search) {
                $orgs = Payment::where('returncode', 100)
                    ->where(function ($query) use ($search) {
                        $query->where('name', 'LIKE', '%' . $search . '%')
                            ->orWhere('ic', 'LIKE', '%' . $search . '%')->orWhere('mobile_no', 'LIKE', '%' . $search . '%')->orWhere('home_no', 'LIKE', '%' . $search . '%');
                    })
                    ->orderBy('created_at', 'DESC')->paginate(10);
            } else {
                $orgs = Payment::where('returncode', 100)->orderBy('created_at', 'DESC')->paginate(20);
            }

            $jumlah = Payment::where('returncode', 100)->sum('amount');
            $share_amount = Payment::where('returncode', 100)->sum('share_amount');
            $first_time_fees = Payment::where('returncode', 100)->sum('first_time_fees');
        }

        return view('admin.transactions')->with('orgs', $orgs)->with('jumlah', $jumlah)->with('syer', $share_amount)->with('firsttime', $first_time_fees)->with('search', $search);
    }
}
