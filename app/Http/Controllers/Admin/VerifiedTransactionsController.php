<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\PaymentProof;
use Illuminate\Http\Request;
use App\Mongo\Payment as MPayment;

class VerifiedTransactionsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {

        $status = request()->get('type');
        $search = $request->input('search');

        if ($status == 'online') {

            if ($search) {

                $payments = MPayment::where('returncode', '100')
                    ->where('channel', 'online')
                    ->where(function ($query) use ($search) {
                        $query->where('name', 'LIKE', '%' . $search . '%')
                            ->orWhere('ic', 'LIKE', '%' . $search . '%')
                            ->orWhere('phone', 'LIKE', '%' . $search . '%');
                    })
                    ->sortable(['name'])->paginate(20);

                return view('admin.transaction_verified')->with('payments', $payments)->with('search', $search)->with('type',$status);
            } else {

                $payments = MPayment::where('returncode', '100')->where('channel', 'online')->sortable(['name'])->paginate(20);

                return view('admin.transaction_verified')->with('payments', $payments)->with('search', $search)->with('type',$status);
            }
        }

        if ($status == 'manual') {

            if ($search) {

                $payments = MPayment::where('returncode', '100')->where('channel', 'manual')
                    ->where(function ($query) use ($search) {
                        $query->where('name', 'LIKE', '%' . $search . '%')
                            ->orWhere('ic', 'LIKE', '%' . $search . '%');
                    })
                    ->sortable(['name'])->paginate(20);

                return view('admin.transaction_verified')->with('payments', $payments)->with('search', $search)->with('type',$status);
            } else {

                $payments = MPayment::where('returncode', '100')->where('channel', 'manual')
                    ->sortable(['name'])->paginate(20);

                return view('admin.transaction_verified')->with('payments', $payments)->with('search', $search)->with('type',$status);
            }
        }

        return redirect('/admin/tx/verified?type=manual');
    }
}
