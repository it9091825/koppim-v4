<?php

namespace App\Http\Controllers\Admin;

use App\Jobs\ProcessAnnouncementEmail;
use App\Mail\SendAnnouncement;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Admin\Announcement;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Exception;
use Illuminate\Support\Facades\Mail;
use App\User;

class AnnouncementsController extends Controller
{
    public function index()
    {

        $announcements = Announcement::paginate(5);

        return view('announcements.index', compact('announcements'));
    }

    public function edit($id)
    {

        $announcement = Announcement::find($id);
        return view('announcements.show', compact('announcement'));
    }

    public function store(Request $request)
    {
        //dd($request->all());
        try {
            $announcement = Announcement::create([
                'title' => $request->title,
                'message' => $request->message,
                'published_from' => Carbon::parse($request->published_from),
                'published_until' => Carbon::parse($request->published_until),
            ]);

            $users = User::where('type','user')->whereIn('status', [0, 1, 2, 3])->get();
            $users = $users->unique('email');

            if($request->has('send_email')) {
                ProcessAnnouncementEmail::dispatch($users, $announcement);
            }

            return redirect()->back()->with('success', 'Announcement added successfully');

        } catch (Exception $e) {
            return redirect()->back()->with('error', $e->getMessage());
        }
    }

    public function update(Request $request, $id)
    {
        try {
            Announcement::find($id)->update([
                'title' => $request->title,
                'message' => $request->message,
                'published_from' => Carbon::parse($request->published_from),
                'published_until' => Carbon::parse($request->published_until),
            ]);
            return redirect()->back()->with('success', 'Announcement updated successfully');
        } catch (Exception $e) {
            return redirect()->back()->with('error', $e->getMessage());
        }
    }

    public function destroy($id)
    {
        try {

            Announcement::find($id)->delete();

            return redirect()->back()->with('success', 'Announcement deleted successfully');
        } catch (ModelNotFoundException $e) {
            return redirect()->back()->with('error', $e->getMessage());
        } catch (Exception $e) {
            return redirect()->back()->with('error', $e->getMessage());
        }
    }
}
