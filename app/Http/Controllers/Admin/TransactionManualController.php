<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Mail\User\NewRegistration;
use App\Mongo\Payment;
use App\Traits\OrderTrait;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Traits\NexmoTrait;
use App\Traits\SortableMongo;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Mail;
use Storage;

class TransactionManualController extends Controller
{
    use NexmoTrait;
    use OrderTrait;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function txs_manual(Request $request)
    {
        $search = $request->input('search');

        if ($search) {

            $payments = Payment::where('channel', 'manual')
                ->where(function ($query) {
                    $query->where('status', 'waiting-for-verification')
                        ->orWhere('status', 'waiting-for-reverification');
                })
                ->where(function ($query) use ($search) {
                    $query->where('name', 'LIKE', '%' . $search . '%')
                        ->orWhere('ic', 'LIKE', '%' . $search . '%')
                        ->orWhere('phone', 'LIKE', '%' . $search . '%');
                })->sortable(['created_at','desc'])->paginate(10);

            return view('admin.transaction_manual')->with('payments', $payments)->with('search', $search);
        } else {

            $payments = Payment::where('channel', 'manual')
                ->where(function ($query) {
                    $query->where('status', 'waiting-for-verification')
                        ->orWhere('status', 'waiting-for-reverification');
                })->sortable(['created_at','desc'])->paginate(10);

            return view('admin.transaction_manual')->with('payments', $payments)->with('search', $search);
        }
    }

    public function getData($id)
    {
        return Payment::find($id);
    }

    public function print($id)
    {
        $payment = Payment::find($id);
        return view('admin.print_transaction_manual')->with('payment', $payment);
    }

    public function sahkan(Request $request)
    {

        if(!strpos(url()->previous(), '/admin/members/view'))
        {
            Session::put('admin_member_back', url()->previous());
        }

        $messages = [
            'name.required' => 'Ruang Nama Perlu Diisi',
            'ic.required' => 'Ruang No. Mykad Perlu Diisi',
            'mobile_no.required' => 'Ruang No. Telefon Perlu Diisi',
            'email.required' => 'Ruang Emel Perlu Diisi',
            'amount.required' => 'Ruang Jumlah Keseluruhan Perlu Diisi',
            'payment_date.required' => 'Ruang Tarikh Transaksi Perlu Diisi',
        ];

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'ic' => 'required',
            'mobile_no' => 'required',
            'email' => 'required',
            'amount' => 'required',
            'payment_date' => 'required'
        ], $messages);

        if ($validator->fails()) {
            return redirect('/admin/tx/manual')
                ->withErrors($validator)
                ->withInput();
        }

        $payment = Payment::findOrFail($request->proof_id);
        if ($payment) {

            if($request->submitButton == "hantar") {
                $payment->update([
                    'name' => strtoupper($request->name),
                    'ic' => (string)$request->ic,
                    'phone' => (string)$request->mobile_no,
                    'email' => $request->email,
                    'amount' => (float)$request->amount,
                    'share_amount' => (float)$request->first_time_share + (float)$request->additional_share,
                    'first_time_fees' => (float)$request->first_time_fees,
                    'first_time_share' => (float)$request->first_time_share,
                    'additional_share' => (float)$request->additional_share,
                    'status' => 'waiting-for-approval',
                    'payment_date' => $request->payment_date,
                    'verified_date' => Carbon::now(),
                    'verified_by' => Auth::User()->id,
                    'note' => $request->note,
                ]);

                activity('admin_manual_payment_verification_verified')
                    ->withProperties(
                        [
                            'payment_id' => $payment->id,
                            'payment_name' => $payment->name,
                            'payment_ic' => $payment->ic,
                            'payment_email' => $payment->email,
                            'payment_phone' => $payment->phone,
                            'Oleh' => Auth::User()->name,
                            'Tarikh' => Carbon::now(),
                        ]
                    )
                    ->log('Verifikasi Bayaran Manual Disahkan');

                $success = 'Bayaran telah dihantar untuk pengesahan';
            } else {

                $payment->status = 'waiting-for-review';
                $payment->verified_by = Auth::User()->id;
                $payment->verified_date = Carbon::now();
                $payment->note = $request->note;
                $payment->save();

                activity('admin_manual_payment_send_for_review')
                    ->withProperties(
                        [
                            'payment_id' => $payment->id,
                            'payment_name' => $payment->name,
                            'payment_ic' => $payment->ic,
                            'payment_email' => $payment->email,
                            'payment_phone' => $payment->phone,
                            'Oleh' => Auth::User()->name,
                            'Tarikh' => Carbon::now(),
                        ]
                    )
                    ->log('Verifikasi Bayaran Manual Telah Dihantar Untuk Semakan Lanjut');

                $success = 'Bayaran telah dihantar untuk semakan lanjut';
            }

            return redirect(Session::get('admin_member_back'))->with('success', $success);
        }
    }

    //Not used
    public function batalkan(Request $request)
    {
        $payment = Payment::findOrFail($request->proof_id);
        $payment->status = 'verification-cancelled';
        $payment->verified_by = Auth::User()->id;
        $payment->verified_date = Carbon::now();
        $payment->note = $request->note;
        $payment->save();

        activity('admin_manual_payment_verification_cancelled')
            ->withProperties(
                [
                    'payment_id' => $payment->id,
                    'payment_name' => $payment->name,
                    'payment_ic' => $payment->ic,
                    'payment_email' => $payment->email,
                    'payment_phone' => $payment->phone,
                    'Oleh' => Auth::User()->name,
                    'Tarikh' => Carbon::now(),
                ]
            )
            ->log('Verifikasi Bayaran Manual Dibatalkan');

        return redirect('/admin/tx/manual');
    }

    public function simpan(Request $request)
    {
        $payment = Payment::findOrFail($request->proof_id);
        if ($payment) {

            activity('admin_manual_payment_verification_edit')
                ->withProperties(
                    [
                        'old' => [
                            'name' => strtoupper($payment->name),
                            'ic' => (string)$payment->ic,
                            'phone' => (string)$payment->mobile_no,
                            'email' => $payment->email,
                            'amount' => (float)$payment->amount,
                            'share_amount' => (float)$payment->first_time_share + (float)$payment->additional_share,
                            'first_time_fees' => (float)$payment->first_time_fees,
                            'first_time_share' => (float)$payment->first_time_share,
                            'additional_share' => (float)$payment->additional_share,
                            'payment_date' => $payment->payment_date,
                            'note' => $payment->note,
                        ],
                        'new' => [
                            'name' => strtoupper($request->name),
                            'ic' => (string)$request->ic,
                            'phone' => (string)$request->mobile_no,
                            'email' => $request->email,
                            'amount' => (float)$request->amount,
                            'share_amount' => (float)$request->first_time_share + (float)$request->additional_share,
                            'first_time_fees' => (float)$request->first_time_fees,
                            'first_time_share' => (float)$request->first_time_share,
                            'additional_share' => (float)$request->additional_share,
                            'payment_date' => $request->payment_date,
                            'note' => $request->note,
                        ],
                        'payment_id' => $payment->id,
                        'changes_by' => Auth::user()->id,
                    ]
                )
                ->log('Verifikasi Bayaran Manual Dikemaskini');

            $payment->update([
                'name' => strtoupper($request->name),
                'ic' => (string)$request->ic,
                'phone' => (string)$request->mobile_no,
                'email' => $request->email,
                'amount' => (float)$request->amount,
                'share_amount' => (float)$request->first_time_share + (float)$request->additional_share,
                'first_time_fees' => (float)$request->first_time_fees,
                'first_time_share' => (float)$request->first_time_share,
                'additional_share' => (float)$request->additional_share,
                'payment_date' => $request->payment_date,
                'note' => $request->note,
            ]);

            return response()->json([
                'data' => 'Perubahan Selesai Disimpan.',
            ]);
        }

        return response()->json([
            'data' => 'Maklumat Tidak Dijumpai Dalam Sistem.',
        ]);
    }

    //Semua function dibawah ni dan route kena tukar ke controller lain. Ni bahagian lihat maklumat anggota punya transaksi langganan
    public function storeTransaction(Request $request)
    {


        foreach ($request->amount as $key => $amount) {

            if ($request->amount[$key]) {

                $order_ref = $this->create_order_ref($request->user_ic);
                $payment = new Payment;
                $payment->name = strtoupper($request->user_name);
                $payment->ic = (string)$request->user_ic;
                $payment->phone = (string)$request->user_mobile_no;
                $payment->email = $request->user_email;
                $payment->amount = (float)$request->amount[$key];
                $payment->share_amount = (float)$request->modalsyer[$key] + (float)$request->additionalsyer[$key];
                $payment->first_time_fees = (float)$request->regfee[$key];
                $payment->first_time_share = (float)$request->modalsyer[$key];
                $payment->additional_share = (float)$request->additionalsyer[$key];
                $payment->status = (string)$request->status[$key];
                //check status
                if ((string)$request->status[$key] == 'payment-completed') {
                    $payment->returncode = '100';
                }

                $payment->type = 'one-time';
                $payment->order_ref = $order_ref;


                $payment->channel = (string)$request->channel[$key];
                //check channel
                if ((string)$request->channel[$key] == 'ikoop') {
                    $payment->payment_desc = 'Migrasi Dari Sistem Ikoop';
                } else {
                    $payment->payment_desc = 'Admin Tambah Transaksi Langganan';
                    //                    $payment->bank = 'bank islam';
                }


                if ($request->hasFile('proof')) {
                    $store = Storage::disk('do_spaces')->putFile(env('DO_SPACES_UPLOAD') . 'upload/payment_proof', $request->proof[$key], 'public');
                    $payment->payment_proof = $store;
                }

                $payment->payment_date = $request->paid_at[$key];
                $payment->note = $request->remark[$key];
                $payment->user_id = $request->user_id;
                $payment->save();

                activity('admin_add_payment_transaction')
                    ->withProperties(
                        [
                            'payment_id' => $payment->id,
                            'payment_name' => $payment->name,
                            'payment_ic' => $payment->ic,
                            'payment_email' => $payment->email,
                            'payment_phone' => $payment->phone,
                            'Oleh' => Auth::User()->name,
                            'Tarikh' => Carbon::now(),
                        ]
                    )
                    ->log('Admin Tambah Transaksi Langganan Baharu');
            }
        }

        session()->flash('success', 'Simpan transaksi langganan pelangaan berjaya.');
        return back();
    }

    public function editTransactionPage($id)
    {

        $payment = Payment::find($id);

        return view('admin.users.partials.edit-user.edit-transaksi-langganan', compact('payment'));
    }

    public function editTransaction(Request $request)
    {

        $payment = Payment::find($request->payment_id);

        if ($payment) {

            $payment->amount = (float)$request->amount;
            $payment->share_amount = (float)$request->modalsyer + (float)$request->additionalsyer;
            $payment->first_time_fees = (float)$request->regfee;
            $payment->first_time_share = (float)$request->modalsyer;
            $payment->additional_share = (float)$request->additionalsyer;
            if ($request->hasFile('proof') == true) {
                $store = Storage::disk('do_spaces')->putFile(env('DO_SPACES_UPLOAD') . 'upload/payment_proof', request()->proof, 'public');
                $payment->payment_proof = $store;
            }

            $payment->payment_date = $request->paid_at;
            $payment->note = $request->remark;
            $payment->save();

            activity('admin_edit_payment_transaction')
                ->withProperties(
                    [
                        'payment_id' => $payment->id,
                        'payment_name' => $payment->name,
                        'payment_ic' => $payment->ic,
                        'payment_email' => $payment->email,
                        'payment_phone' => $payment->phone,
                        'Oleh' => Auth::User()->name,
                        'Tarikh' => Carbon::now(),
                    ]
                )
                ->log('Admin Kemaskini Transaksi Langganan');
        }

        session()->flash('success', 'Simpan transaksi langganan pelangaan berjaya.');
        return back();
    }

    public function cancelTransaction(Request $request)
    {
        $payment = Payment::find($request->payment_id);

        if ($payment) {

            $payment->status = 'payment-cancelled';
            $payment->returncode = '200';
            $payment->save();

            activity('admin_cancel_payment_transaction')
                ->withProperties(
                    [
                        'payment_id' => $payment->id,
                        'payment_name' => $payment->name,
                        'payment_ic' => $payment->ic,
                        'payment_email' => $payment->email,
                        'payment_phone' => $payment->phone,
                        'Oleh' => Auth::User()->name,
                        'Tarikh' => Carbon::now(),
                    ]
                )
                ->log('Admin Batal Transaksi Langganan');
        }

        session()->flash('success', 'Batal transaksi langganan pelangaan berjaya.');
        return back();
    }
}
