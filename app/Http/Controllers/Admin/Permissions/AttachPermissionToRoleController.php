<?php

namespace App\Http\Controllers\Admin\Permissions;

use App\Http\Controllers\Controller;
use App\Role;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

class AttachPermissionToRoleController extends Controller
{
    public function __invoke($role_id, Request $request)
    {
        try {

            $role = Role::find($role_id);

            if ($request->has('permission')) {
                $role->permissions()->sync($request->permission);
            } else {
                $role->permissions()->detach();
            }
            
            return redirect()->back()->with('success','Permission attached to Role successfully');        

        } catch (ModelNotFoundException $e) {

            return redirect()->back()->with('error',$e->getMessage());        
            
        } catch (Exception $e) {
            return redirect()->back()->with('error',$e->getMessage());
        }
        
    }
}
