<?php

namespace App\Http\Controllers\Admin\Permissions;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;

class AttachPermissionToUserController extends Controller
{
    public function __invoke($user_id, Request $request)
    {

        $user = User::find($user_id);

        if ($request->has('permission')) {
            $user->permissions()->sync($request->permission);
        }else{
            $user->permissions()->detach();
        }
        return redirect()->back()->with('success','The administrator\'s permissions has been updated');        

        return redirect()->back();
    }
}
