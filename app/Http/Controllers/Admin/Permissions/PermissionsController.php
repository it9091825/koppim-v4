<?php

namespace App\Http\Controllers\Admin\Permissions;

use App\Http\Controllers\Controller;
use App\Permission;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class PermissionsController extends Controller
{
    public function index()
    {
        $permissions = Permission::paginate(5);
        return view('permissions.index')->with('permissions', $permissions);
    }

    public function edit($id)
    {

        $permission = Permission::find($id);
        return view('permissions.show', compact('permission'));
    }

    public function store(Request $request)
    {

        Permission::create([
            'slug' => Str::slug($request->permission_name),
            'name' => $request->permission_name,
            'description' => $request->permission_description,
        ]);
        return redirect()->back()->with('success', 'Permission '.$request->permission_name.' berjaya ditambah');
    }

    public function update(Request $request, $id)
    {

        Permission::find($id)->update([
            'slug' => Str::slug($request->permission_name),
            'name' => $request->permission_name,
            'description' => $request->permission_description,
        ]);

        return redirect()->back()->with('success', 'Permission '.$request->permission_name.' berjaya dikemaskini');
    }

    public function destroy($id)
    {

        $permission = Permission::find($id);
        $permission->delete();
        
        return redirect()->back()->with('success', 'Permission '.$permission->name.' berjaya dihapuskan');
    }
}
