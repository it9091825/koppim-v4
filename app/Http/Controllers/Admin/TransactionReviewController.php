<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Mongo\Payment;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class TransactionReviewController extends Controller
{
    public function index(Request $request){
        $search = $request->input('search');

        if ($search) {

            $payments = Payment::where('channel', 'manual')
                ->where('status', 'waiting-for-review')
                ->where(function ($query) use ($search) {
                    $query->where('name', 'LIKE', '%' . $search . '%')
                        ->orWhere('ic', 'LIKE', '%' . $search . '%')
                        ->orWhere('phone', 'LIKE', '%' . $search . '%');
                })->sortable(['created_at','desc'])->paginate(10);

            return view('admin.transaction_review')->with('payments', $payments)->with('search', $search);
        } else {

            $payments = Payment::where('channel', 'manual')
                ->where('status', 'waiting-for-review')
                ->sortable(['created_at','desc'])->paginate(10);

            return view('admin.transaction_review')->with('payments', $payments)->with('search', $search);
        }
    }

    public function submit(Request $request)
    {
        if(!strpos(url()->previous(), '/admin/members/view'))
        {
            Session::put('admin_member_back', url()->previous());
        }

        $messages = [
            'name.required' => 'Ruang Nama Perlu Diisi',
            'ic.required' => 'Ruang No. Mykad Perlu Diisi',
            'mobile_no.required' => 'Ruang No. Telefon Perlu Diisi',
            'email.required' => 'Ruang Emel Perlu Diisi',
            'amount.required' => 'Ruang Jumlah Keseluruhan Perlu Diisi',
            'payment_date.required' => 'Ruang Tarikh Transaksi Perlu Diisi',
        ];

        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'ic' => 'required',
            'mobile_no' => 'required',
            'email' => 'required',
            'amount' => 'required',
            'payment_date' => 'required'
        ], $messages);

        if ($validator->fails()) {
            return redirect('/admin/transaksi-bayaran/untuk-semakan-lanjut')
                ->withErrors($validator)
                ->withInput();
        }

        $payment = Payment::findOrFail($request->proof_id);
        if ($payment) {

            if($request->submitButton == "hantar") {
                $payment->update([
                    'name' => strtoupper($request->name),
                    'ic' => (string)$request->ic,
                    'phone' => (string)$request->mobile_no,
                    'email' => $request->email,
                    'amount' => (float)$request->amount,
                    'share_amount' => (float)$request->first_time_share + (float)$request->additional_share,
                    'first_time_fees' => (float)$request->first_time_fees,
                    'first_time_share' => (float)$request->first_time_share,
                    'additional_share' => (float)$request->additional_share,
                    'status' => 'waiting-for-approval',
                    'payment_date' => $request->payment_date,
                    'verified_date' => Carbon::now(),
                    'verified_by' => Auth::User()->id,
                    'note' => $request->note,
                ]);

                activity('admin_manual_payment_verification_verified')
                    ->withProperties(
                        [
                            'payment_id' => $payment->id,
                            'payment_name' => $payment->name,
                            'payment_ic' => $payment->ic,
                            'payment_email' => $payment->email,
                            'payment_phone' => $payment->phone,
                            'Oleh' => Auth::User()->name,
                            'Tarikh' => Carbon::now(),
                        ]
                    )
                    ->log('Verifikasi Bayaran Manual Disahkan');

                $success = 'Bayaran telah dihantar untuk pengesahan';

            } else {


                $payment->status = 'verification-cancelled';
                $payment->verified_by = Auth::User()->id;
                $payment->verified_date = Carbon::now();
                $payment->note = $request->note;
                $payment->save();

                activity('admin_manual_payment_verification_cancelled')
                    ->withProperties(
                        [
                            'payment_id' => $payment->id,
                            'payment_name' => $payment->name,
                            'payment_ic' => $payment->ic,
                            'payment_email' => $payment->email,
                            'payment_phone' => $payment->phone,
                            'Oleh' => Auth::User()->name,
                            'Tarikh' => Carbon::now(),
                        ]
                    )
                    ->log('Verifikasi Bayaran Manual Dibatalkan');

                $success = 'Bayaran telah dibatalkan';

            }

            return redirect(Session::get('admin_member_back'))->with('success', $success);
        }
    }
}
