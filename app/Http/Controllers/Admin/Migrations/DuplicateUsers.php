<?php

namespace App\Http\Controllers\Admin\Migrations;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\UserDuplicate;

class DuplicateUsers extends Controller
{
    public function index(){

        $users = UserDuplicate::paginate(50);
        return view('admin.migrations.duplicate_users')->with('users', $users);

    }
}
