<?php

namespace App\Http\Controllers\Admin\Notice;

use App\Http\Controllers\Controller;
use App\Jobs\SendSmsJob;
use Illuminate\Http\Request;
use App\Sms;
use App\State;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class SmsController extends Controller
{
    public function index()
    {

        $sms = Sms::orderBy('created_at', 'desc')->paginate(5);

        return view('admin.sms.index', compact('sms'));
    }

    public function store(Request $request)
    {

        $this->validate($request,[
            'message' => 'required'
        ],[
            'message.required' => 'Ruangan MESEJ perlu diisi'
        ]);

        $message = $request->message;

        $users = DB::table('users')->select('ic', 'mobile_no')->where('type', 'user');

        // kalau checkbox semua pelanggan ticked
        if ($request->filled('bystatus')) {
            if (env('APP_ENV') == 'production') {
                if ($request->bystatus != '' || $request->bystatus != null) {

                    if ($request->bystatus != 'all') {
                        $users = $users->where('status', $request->bystatus);
                    }
                }
            } else {
                Log::info('sms sent to all users:' . $request->bystatus);
                activity()->log('sent to all users with: ' . $request->bystatus);
            }
        }

        if($request->filled('affiliate')){
            $users = $users->where('is_affiliate','yes');
        }

        // 'q' input mmg dah array daripada blade so tak perlu wrap request->q dlm clause whereIn dgn array

        // bila parliament == 'all', pilih negeri
        // bila parliament != 'all', pilih parliament
        if($request->filled('states')){

            if($request->filled('parliaments')){
                if($request->parliaments == 'all'){
                    // buat camni sebab state tidak disimpan sebagai ID di users table. So kena cari dulu nama state with given $request->state
                    $state = State::find($request->states);
                    $users = $users->where('state','LIKE','%'.$state.'%');
                }else{
                    $users = $users->where('parliament','LIKE','%'.$request->parliaments.'%');
                }
            }

        }

        if ($request->filled('q')) {
            if (count($request->q)) {
                $users = $users->whereIn('id', $request->q);
            }
        }

        if (isset($users)) {

            $users = $users->get();

            if ($users) {
                foreach ($users as $user) {
                    try {
                        SendSmsJob::dispatch($user, $message);
                    } catch (Exception $e) {
                        Log::error($e->getMessage());
                        continue;
                    }
                }
            }
        }

        Sms::create([
            'title' => $request->title,
            'message' => $request->message,
            'recepient' => $request->filled('all') ? json_encode(['all']) : json_encode($request->q)
        ]);

        return redirect()->back()->with('success', 'SMS berjaya dihantar');
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function find(Request $request)
    {
        $term = trim($request->q);

        if (empty($term)) {
            return response()->json([]);
        }

        $users = User::where('type', 'user')->where(function ($query) use ($term) {
            $query->where('name', 'LIKE', '%' . $term . '%')
                ->orWhere('ic', 'LIKE', '%' . $term . '%')
                ->orWhere('mobile_no', 'LIKE', '%' . $term . '%');
        })->get();

        $formatted_tags = [];

        foreach ($users as $user) {
            $formatted_tags[] = ['id' => $user->id, 'text' => $user->name];
        }

        return response()->json($formatted_tags);
    }
}
