<?php

namespace App\Http\Controllers\Admin\Dividen;
use PDF;
use Auth;
use Mail;
use Storage;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\DataTables\UserDataTable;
use App\Mongo\Dividen as Dividen;
use App\Mongo\Payment as MPayment;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;
use App\Exports\Dividen\WithdrawalExport;
use App\Mongo\DividenWallet as DividenWallet;
use App\Mongo\DividenCalculation as DividenCalculation;

class DividenController extends Controller
{

     public function export(Request $request){
        if($request->status){
            $status = $request->status;
        } else {
            $status = 'all';
        }



		$time = time();
        return (new WithdrawalExport($status))->download(''.$time.'.xlsx');
    }

    public function index(Request $request)
    {
	    $status = $request->input('status');

	    if($status)
	    {
		    $dividens = Dividen::where('status',$status)->orderBy('created_at','DESC')->get();
	    }
	    else
	    {
		    $dividens = Dividen::orderBy('created_at','DESC')->get();
	    }



		return view('dividen.dashboard')->with('dividens',$dividens);

    }



    public function withdraw(Request $request)
    {
	    $status = $request->input('status');

	    if($status)
	    {
		    $dividens = DividenWallet::where('type','out')->where('status',$status)->orderBy('created_at','ASC')->get();
	    }
	    else
	    {

		   $dividens = DividenWallet::where('type','out')->orderBy('created_at','ASC')->get();
	    }



		return view('dividen.withdraw')->with('dividens',$dividens)->with('status',$status);

    }



     public function withdraw_detail($id)
    {

	    $withdraw = DividenWallet::where('_id',$id)->first();

	    return view('dividen.withdraw-detail')->with('withdraw',$withdraw);

	 }




	 public function approval(Request $request)
    {
        $id = $request->input('id');
        $ref = $request->input('transaction_ref');

        $withdraw = DividenWallet::where('_id',$id)->first();

         $store = Storage::disk('do_spaces')->putFile(env('DO_SPACES_UPLOAD') . 'uploads/dividenreceipt', request()->tx_receipt, 'public');

		 DividenWallet::where('_id',$id)->update([
			 'status'=>'telah-dibayar',
			 'payment_ref'=>$ref,
			 'payment_receipt'=>$store,
			 'approved_by'=>Auth::id()
		 ]);

        return redirect('/admin/dividen/withdraw/'.$id.'')->with('success','Berjaya');
    }


	public function create(Request $request)
    {



		return view('dividen.create');

    }

    public function dividencreate(Request $request)
    {
	    $from = $request->input('pembayaran-dari');
	    $to = $request->input('pembayaran-sehingga');
	    $tarikhagm = $request->input('tarikh-agm');

	    $tempoh = $request->input('tempoh');

	    $dividen = $request->input('dividen');
	    $sds = md5(time());

	    if($from == NULL || $to == NULL || $dividen == NULL)
	    {
		    return back()->with('error','Sila masukkan tetapan anda');
	    }
	    else
	    {
		    $pieces = explode("-", $from);

		    $year = $pieces[0];

		    $from_carbon = Carbon::createFromFormat('Y-m-d H:i:s', ''.$from.' 00:00:00');
		   $to_carbon = Carbon::createFromFormat('Y-m-d H:i:s', ''.$to.' 23:59:59');
		    // $to_carbon = $from_carbon->addMonths($to);
		   // $diff_in_months_choose = $to;

		    $diff_in_months_choose = $from_carbon->diffInMonths($to_carbon);

			Dividen::create([
				'uid'=>$sds,
				'year'=>$year,
				'agm'=>new \MongoDB\BSON\UTCDateTime(new \DateTime(''.$tarikhagm.' 00:00:01')),
				'from'=>new \MongoDB\BSON\UTCDateTime(new \DateTime(''.$from.' 00:00:01')),
				'to'=>new \MongoDB\BSON\UTCDateTime(new \DateTime(''.$to_carbon->format('Y-m-d').' 23:59:59')),
				'dividen_precentage'=>$dividen,
				'tempoh'=>$tempoh,
				'month_total'=>$diff_in_months_choose,
				'status'=>'draft',
			]);




			return redirect('/admin/dividen/detail/'.$sds.'');

	    }



    }


    public function exportCsv($id)
    {

        $users = DividenCalculation::where('calculation_id', $id)->get();


        $fileName = 'dividenusers.csv';


        $headers = array(
            "Content-type"        => "text/csv",
            "Content-Disposition" => "attachment; filename=$fileName",
            "Pragma"              => "no-cache",
            "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
            "Expires"             => "0"
        );

        $columns = array('NAMA', 'K/P', 'JUMLAH SYER', 'TARIKH PELABURAN', 'SEHINGGA', 'JUMLAH BULAN', 'JUMLAH PELABURAN', 'DIVIDEN');

        $callback = function () use ($users, $columns) {
            $file = fopen('php://output', 'w');
            fputcsv($file, $columns);

        foreach ($users as $user) {
            $row['Nama']    = $user->name;
            $row['IC']    = $user->ic;
            $row['Share']  = number_format($user->share, 2);
            $row['dividen'] = number_format($user->dividen, 2);
            $row['contribute'] = $user->contributions;
            $row['to'] = $user->to;
            $sehingga = Carbon::createFromFormat('Y-m-d H:i:s',$row['to'])->format('Y-m-d');
            $jsondata = $row['contribute'];

            $rm = 'RM';
            $jumlahsyer = $rm . ' ' .  $row['Share'];
            $dividen = $rm . ' ' . $row['dividen'];

            //if exist data contributions
            if (isset($jsondata[0])){
            $payment_date = $jsondata[0]['payment_date']['date'];
            $timestamp = strtotime($payment_date);
            $tarikh_pelaburan = date('Y-m-d', $timestamp);
            $month = $jsondata[0]['month'];
            $amount = $jsondata[0]['amount'];


            $rm = 'RM';

            $totalamount = $rm . ' ' . $amount;


        fputcsv($file, array($row['Nama'], $row['IC'], $jumlahsyer, $tarikh_pelaburan, $sehingga, $month, $totalamount, $dividen));

         //if tak exist data contributions
        } elseif (!isset($jsondata[0])) {

                    $tarikh_pelaburan = 'TIADA DATA';
                    $month = 'TIADA DATA';
                    $totalamount = 'TIADA DATA';

        fputcsv($file, array($row['Nama'], $row['IC'], $jumlahsyer, $tarikh_pelaburan, $sehingga, $month, $totalamount, $dividen));


        }

    }
            fclose($file);

    };

        return response()->stream($callback, 200, $headers);
    }



	public function detail($id)
    {

		$dividen = Dividen::where('uid',$id)->first();


		$users = DividenCalculation::where('calculation_id',$id)->paginate(100);


		return view('dividen.detail')->with('dividen',$dividen)->with('users',$users);

    }


    public function calculate_dividen_exe($id)
    {
	    Dividen::where('uid',$id)->update([
		    	'status'=>'calculate-in-progress'
	    	]);

	      return back()->with('success','Berjaya');
	}

    public function calculate_cron()
    {
	    $cal = Dividen::where('status','calculate-in-progress')->get();

	    foreach($cal as $ca)
	    {
		   User::where('no_koppim','!=',NULL)->where('type','user')->where('status',1)->update([
		    'dividend_flag'=>0
			]);

			$usercount = User::where('no_koppim','!=',NULL)->where('type','user')->where('status',1)->count();

		    Dividen::where('_id',$ca->_id)->update([
			    'status'=>'calculate-in-progress-2',
			    'user_count'=>$usercount,
			    'progress_count'=>0
		    ]);
		}




	}

	public function calculate_2_cron()
    {
	    $cal = Dividen::where('status','calculate-in-progress-2')->get();

	   $count =0;

	    foreach($cal as $ca)
	    {
		    $count = $this->calculate_dividen($ca->uid);

		    if($count == 0)
		    {
			    $sum = DividenCalculation::where('calculation_id',$ca->uid)->sum('dividen');

			    $count22 = DividenCalculation::where('calculation_id',$ca->uid)->count();

			    Dividen::where('uid',$ca->uid)->update([
				'user_count'=>$count22,
				'total_dividen'=>$sum,
		    	'status'=>'calculated'
				]);
		    }
		    else
		    {
			     $count22 = DividenCalculation::where('calculation_id',$ca->uid)->count();

			    Dividen::where('uid',$ca->uid)->update([
				'progress_count'=>$count22
				]);
		    }

	    }

	    return $count;

	}


    public function calculate_dividen($id)
    {

	    	$users = User::where('no_koppim','!=',NULL)->where('type','user')->where('status',1)->where('dividend_flag',0)->limit(100)->get();

	    	$user_count = User::where('no_koppim','!=',NULL)->where('type','user')->where('status',1)->where('dividend_flag',0)->count();

	    	$dividen = Dividen::where('uid',$id)->first();

	    	$diffss = strtotime($dividen->to) - strtotime($dividen->from);
			$dayss = abs(round($diffss / 86400));

	    	$total_day_choose = $dayss;

	    	$diff_in_months_choose = $dividen->to->diffInMonths($dividen->from);
			//dd($diff_in_months_choose);

	    	foreach($users as $user)
	    	{
		    	$share_amount = MPayment::where('ic',$user->ic)->where('status','payment-completed')->where('created_at','<',$dividen->to)->sum('share_amount');

		    	$contributions = MPayment::where('ic',$user->ic)->where('status','payment-completed')->where('created_at','<',$dividen->to)->get();



		    	$contr = [];
		    	$dividen_count = 0;
		    	foreach($contributions as $contribution)
		    	{

			    	$payment_date = $contribution->created_at; //boleh ubah ubah



			    	//Day Diff
			    	$diff = strtotime($dividen->to) - strtotime($payment_date);

			    	$day = abs(round($diff / 86400));

			    	if($day >= $total_day_choose)
			    	{
				    	// ignore last year
				    	$day = $total_day_choose;
			    	}

			    	$dividen_perday = ((($dividen->dividen_precentage/100)*$contribution->share_amount)/$total_day_choose);
			    	 $dividen_calculate = $dividen_perday*$day;
			    	 //.............................................


			    	 //month diffrent
			    	 $to =$dividen->to;
					 $from = $payment_date;
					 $diff_in_months = $to->diffInMonths($from);


					 if($diff_in_months > 12)
					 {
						 $diff_in_months = 12;
					 }


			    	 $dividen_permonth = ((($dividen->dividen_precentage/100)*$contribution->share_amount)/$diff_in_months_choose);
			    	  $dividen_month_calculate = $dividen_permonth*$diff_in_months;
			    	 //...............................


			    	 //kalau nak kire hari
			    	// $dividen_count = $dividen_count + $dividen_calculate;
			    	 $dividen_count = $dividen_count + $dividen_month_calculate;


			    	array_push($contr,array(
				    	'amount'=>$contribution->share_amount,
				    	'day'=>$day,
				    	'dividen_per_day'=>$dividen_perday,
				    	'dividen_day'=>$dividen_calculate,
				    	'month'=>$diff_in_months,
				    	'dividen_per_month'=>$dividen_permonth,
				    	'dividen_month'=>$dividen_month_calculate,
				    	'dividen'=>$dividen_month_calculate,
				    	'payment_date'=>$payment_date //boleh ubah kalo ader proper paymentdate
			    	));
		    	}

		    	$new_array = array(
			    	'name'=>$user->name,
			    	'ic'=>(int)$user->ic,
			    	'calculation_id'=>$id,
			    	'dividen_percentage'=>$dividen->dividen_precentage,
			    	'from'=>$dividen->from,
			    	'to'=>$dividen->to,
			    	'share'=>$share_amount,
			    	'dividen'=>$dividen_count,
			    	'contributions'=>$contr,
			    	'cron'=>0
		    	);




		    	DividenCalculation::create($new_array);

		    	User::where('id','=',$user->id)->update([
			    	'dividend_flag'=>3,
			    	'dividend_datetime'=>Carbon::now(),
		    	]);

	    	}



	    	return (int)$user_count;

	}

    public function publish_in_progress($id)
    {
	    Dividen::where('uid',$id)->update([
		    	'status'=>'published-in-progress',
		    	'published_count'=>0
		]);

		return back()->with('success','Sedang di prosess');

	}

    public function publish_cron_2()
    {
	     $cal = Dividen::where('status','published-in-progress')->get();

		 $count =0;

	    foreach($cal as $ca)
	    {
		    $count = $this->publish_calculation($ca->uid);

		    if($count == 0)
		    {
			    $count22 = DividenCalculation::where('calculation_id',$ca->uid)->where('cron',1)->count();
			    Dividen::where('_id',$ca->_id)->update([
				    'status'=>'published',
				    'published_count'=>$count22
			    ]);
		    }
		    else
		    {

			   $count22 = DividenCalculation::where('calculation_id',$ca->uid)->where('cron',1)->count();

			    Dividen::where('_id',$ca->_id)->update([
				    'published_count'=>$count22
			    ]);

		    }
		}

	    return $count;
	}


    public function publish_calculation($id)
    {
	    $dividens = DividenCalculation::where('calculation_id',$id)->where('cron',0)->limit(500)->get();

	    foreach($dividens as $dividen)
	    {
		    DividenWallet::create([
			    'ic'=>$dividen->ic,
			    'type'=>'in',
			    'amount'=>$dividen->dividen,
			    'calculation_id'=>$dividen->calculation_id,
			    'dividen_id'=>$dividen->_id,
			    'from'=>$dividen->from,
			    'to'=>$dividen->to,
			    'dividen_percentage'=>$dividen->dividen_percentage,
		    ]);

		    DividenCalculation::where('_id',$dividen->_id)->update([
			    'cron'=>1
		    ]);
	    }

	    $count = DividenCalculation::where('calculation_id',$id)->where('cron',0)->count();

	    return $count;

	}


    private function cal_days_in_year($year){
    $days=0;
    for($month=1;$month<=12;$month++){
        $days = $days + cal_days_in_month(CAL_GREGORIAN,$month,$year);
     }
	 return $days;
	}



}
?>
