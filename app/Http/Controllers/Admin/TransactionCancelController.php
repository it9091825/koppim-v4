<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Mongo\Payment;
use Illuminate\Http\Request;

class TransactionCancelController extends Controller
{
    public function index(Request $request){

        $search = $request->input('search');

        if ($search) {

            $payments = Payment::where('channel', 'manual')
                ->whereIn('status',['verification-cancelled','approval-cancelled'])
                ->where(function ($query) use ($search) {
                    $query->where('name', 'LIKE', '%' . $search . '%')
                        ->orWhere('ic', 'LIKE', '%' . $search . '%')
                        ->orWhere('phone', 'LIKE', '%' . $search . '%');
                })
                ->sortable(['name'])->paginate(20);

            return view('admin.transaction_cancel')->with('payments', $payments)->with('search', $search);
        }

        $payments = Payment::where('channel', 'manual')->whereIn('status',['verification-cancelled','approval-cancelled'])->paginate(20);
        return view('admin.transaction_cancel')->with('payments', $payments)->with('search', $search);
    }
}
