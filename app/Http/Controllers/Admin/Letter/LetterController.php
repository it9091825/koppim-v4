<?php

namespace App\Http\Controllers\Admin\Letter;

use App\Http\Controllers\Controller;
use App\Mongo\Payment as MPayment;
use App\Mongo\CommissionWithdraw as CommissionWithdraw;
use App\Mongo\LetterModule as LetterModule;
use App\Mongo\LetterJob as LetterJob;
use App\Mongo\LetterJobItems as LetterJobItems;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\User;
use Auth;
use Mail;
use PDF;
class LetterController extends Controller
{
    public function index(Request $request)
    {
		$search = $request->input('search');

		if($search){
			$letters = LetterModule::where('name','LIKE','%'.$search.'%')->paginate(10);
		}else{
			$letters = LetterModule::paginate(10);
		}

        return view('Letter.dashboard')->with('letters',$letters);

    }

	private function short_code_settings()
    {
	    $array = [
		    'no_koppim'=>'[NO-ANGGOTA]',
		    'name'=>'[NAMA]',
		    'approved_date'=>'[TARIKH-LULUS]',
		    'email'=>'[EMEL]',
		    'ic'=>'[KAD-PENGENALAN]',
		    'home_no'=>'[NO-TELEFON-RUMAH]',
		    'mobile_no'=>'[NO-TELEFON-BIMBIT]',
		    'alamat'=>'[ALAMAT]',
		    'address_line_1'=>'[ALAMAT-BARIS-1]',
		    'address_line_2'=>'[ALAMAT-BARIS-2]',
		    'address_line_3'=>'[ALAMAT-BARIS-3]',
		    'postcode'=>'[POSKOD]',
		    'city'=>'[DAERAH]',
		    'state'=>'[NEGERI]',
		    'parliament'=>'[PARLIMEN]',
		    'race'=>'[BANGSA]',
		    'sex'=>'[JANTINA]',
		    'dob'=>'[TARIKH-LAHIR]',
		    'job'=>'[PEKERJAAN]',
		    'monthly_income'=>'[PENDAPATAN-BULANAN]',
		    'heir_name'=>'[NAMA-WARIS]',
		    'heir_ic'=>'[KAD-PENGENALAN-WARIS]',
		    'heir_phone'=>'[NO-TELEFON-WARIS]',
		    'heir_relationship'=>'[HUBUNGAN-WARIS]',
		    'bank_name'=>'[NAMA-BANK]',
		    'bank_account'=>'[NO-AKAUN-BANK]',
		    'bank_account_name'=>'[NAMA-AKAUN-BANK]',
		    'tuanpuan'=>'[TUAN/PUAN]',
	    ];

	    return $array;
	}

	private function settings_array()
    {
	    $array = [
		    'ic_sample'=>'871106145413',
		    'cron_user_per_request'=>100,
		    'send_email_per_request'=>50
	    ];

	    return $array;
	}


	public function create(Request $request)
    {
	    $short_codes = $this->short_code_settings();

	    return view('Letter.create_letter')->with('short_codes',$short_codes)->with('created',true)->with('letter',[]);
	}

	public function edit(Request $request)
    {
	    $short_codes = $this->short_code_settings();

	    $id = $request->input('id');

	    $letter = LetterModule::where('id',$id)->first();

	    $content = $this->decode_content($letter['content']);

	    $setting = $this->settings_array();

	    $ic_sample = $setting['ic_sample'];

	    return view('Letter.create_letter')->with('short_codes',$short_codes)->with('created',false)->with('letter',$letter)->with('content',$content)->with('ic_sample',$ic_sample);
	}

	public function edit_exe(Request $request)
    {
	    $id = $request->input('id');
	    $name = $request->input('name');
	    $subject = $request->input('subject');
	    $content = $request->input('content');

	    if($subject == "")
	    {
		    return redirect('/admin/letter/edit?id='.$id.'')->with('error','Sila Kemaskini Tajuk Surat Anda');
	    }
	    else
	    {
		    LetterModule::where('id',$id)->update([
		    'name'=>strtoupper($name),
		    'subject'=>strtoupper($subject),
		    'content'=>$this->encode_content($content),
			]);

	      return redirect('/admin/letter/edit?id='.$id.'')->with('success','Berjaya Disimpan');
	    }


	}

	private function encode_content($content)
	{
		 $text = nl2br($content);
	    $text2 = preg_replace("/\r|\n/", "", $text);
	    $text3 = htmlEntities($text2);

	    return $text3;
	}

	private function decode_content($content)
	{
		$content = html_entity_decode($content);

	    return $content;
	}

	public function create_exe(Request $request)
    {
	    $name = $request->input('name');
	    $subject = $request->input('subject');
	    $content = $request->input('content');

	    $letter_id = uniqid();

	    if($subject == "")
	    {
		    return redirect('/admin/letter/create')->with('error','Sila Kemaskini Tajuk Surat Anda');
	    }
	    else
	    {

	    LetterModule::create([
		    'id'=>$letter_id,
		    'name'=>strtoupper($name),
		    'subject'=>strtoupper($subject),
		    'content'=>$this->encode_content($content),
		    'created_by'=>Auth::id()
	    ]);

	    return redirect('/admin/letter/edit?id='.$letter_id.'');
	    }
	}


	private function preview_decode_content($content,$ic)
	{
		$codes = $this->short_code_settings();

	    $content = html_entity_decode($content);

	   $user = User::where('ic',$ic)->first();

		 foreach($codes as $key=>$value)
		 {
			 	if($value == "[ALAMAT]")
			 	{

				 	if($user['address_line_2'] == "" || $user['address_line_2'] == NULL)
				 	{

					 	$address_line_2 = "";
				 	}
				 	else
				 	{
					 	$address_line_2 = $user['address_line_2'].",<br>";
				 	}

				 	if($user['address_line_3'] == "" || $user['address_line_3'] == NULL )
				 	{

					 	$address_line_3 = "";
				 	}
				 	else
				 	{
					 	$address_line_3 = $user['address_line_3'].",<br>";
				 	}

				 	$address = $user['address_line_1'].",<br>".$address_line_2.$address_line_3.$user['postcode']." ".$user['city'].",<br>".$user['state'];

				 	$content = str_replace($value,"".$address."",$content);
			 	}
			 	else if($value == "[TUAN/PUAN]")
			 	{
				 	if($user['sex'] == "LELAKI")
				 	{
					 	$content = str_replace($value,"TUAN",$content);
				 	}
				 	else
				 	{
					 	$content = str_replace($value,"PUAN",$content);
				 	}
				}
				else if($value == "[TARIKH-LULUS]")
			 	{
				 	if($user->approved_date == NULL)
				 	{
					 	$content = str_replace($value,"",$content);
				 	}
				 	else
				 	{
					 $tarikh_lulus = $user->approved_date->format('d/m/Y');

				 	$content = str_replace($value,"".$tarikh_lulus."",$content);
				 	}

				}
			 	else
			 	{
				 	$content = str_replace($value,"".$user[$key]."",$content);
			 	}


		 }




		 return $content;
	}



	public function preview(Request $request)
    {
	    $id = $request->input('id');

	    $ic = $request->input('ic');

	    $type = $request->input('type');
	    $header = $request->input('header');

	    $letter = LetterModule::where('id',$id)->first();

	    $content = $this->preview_decode_content($letter->content,$ic);

	    if($type == "email")
	    {
		    return view('Letter.email')->with('letters',$letter)->with('content',$content)->with('type',$type)->with('header',$header);
	    }
	    elseif($type == "pdf")
	    {

		    $data = [
			    'letters'=>$letter,
			    'content'=>$content,
			    'type'=>$type,
			    'header'=>$header
		    ];

		    $pdf = PDF::loadView('Letter.email', $data);
			return $pdf->download('sample.pdf');
	    }
	    else
	    {
		    return view('Letter.letter')->with('letters',$letter)->with('content',$content)->with('type',$type)->with('header',$header);
	    }


	}


	public function send_email_sample(Request $request)
    {
	    $id = $request->input('id');
	    $email_sample = $request->input('email_sample');

	    $ic = $request->input('ic');

	    if($email_sample != "" || $email_sample != NULL)
	    {
		    $letter = LetterModule::where('id',$id)->first();
	    $content = $this->preview_decode_content($letter->content,$ic);
	    $header = $request->input('header');



	    		$data = [
			            'no-reply' => 'koppim@ppim.org.my',
			            'reply-to' => 'koppim@ppim.org.my',
			            'to' => $email_sample,
			            'to_name' => $email_sample,
			            'subject' => $letter->subject
						];

						Mail::send('Letter.email', ['content' => $content,'header' => $header],
			            function ($message) use ($data)
			            {
			                $message->from($data['no-reply'],'koPPIM')->replyTo($data['reply-to'], 'koPPIM')->to($data['to'], $data['to_name'])->subject($data['subject']);
			            });

	     return redirect('/admin/letter/edit?id='.$id.'')->with('success','Berjaya Dihantar');

	    }
	    else
	    {
		    return redirect('/admin/letter/edit?id='.$id.'')->with('error','Sila masukkan email');
	    }

	 }



	 public function send(Request $request)
    {
	    $letter_id = $request->input('letter_id');
	    $searchtype = $request->input('searchtype');
	    $search = $request->input('search');
	    $cawangan = $request->input('cawangan');
	    $status = $request->input('status');

	    $letters = LetterModule::all();


	    if($searchtype == "single" && !empty($search))
	    {
		    $users = User::where('type','user')->where('ic','LIKE','%'.$search.'%')->orWhere('email','LIKE','%'.$search.'%')->orWhere('mobile_no','LIKE','%'.$search.'%')->paginate(100);
	    }
	    else if($searchtype == "filter")
	    {
		    if(empty($cawangan) && empty($status))
		    {
			      $users = User::where('type','user')->paginate(100);
		    }
		    else if(!empty($cawangan) && empty($status))
		    {
			     $users = User::where('type','user')->where('parliament',$cawangan)->paginate(100);
		    }
		    else if(!empty($status) && empty($cawangan))
		    {
			     $users = User::where('type','user')->where('status',$status)->paginate(100);
		    }
		    else
		    {
			     $users = User::where('type','user')->where('parliament',$cawangan)->where('status',$status)->paginate(100);
		    }

		}
		 else if($searchtype == "filteraff")
	    {
		    if($status == "")
		    {
			    $users = User::where('type','user')->paginate(100);
		    }
		    else
		    {
			   $users = User::where('type','user')->where('is_affiliate',$status)->paginate(100); 
		    }
		    

		}
	    else
	    {
		    $users = User::where('type','user')->paginate(100);
	    }

	    if($letter_id)
	    {
		    $letter = LetterModule::where('id',$letter_id)->first();

		     return view('Letter.send')
		     	->with('letterdetail',$letter)
			    ->with('letters',$letters)
			    ->with('users',$users)
			    ->with('search',$search)
			    ->with('letter_id',$letter_id)
			    ->with('searchtype',$searchtype)
			    ->with('cawangan',$cawangan)
			    ->with('status',$status);
	    }
	    else
	    {


		     return view('Letter.send')
			    ->with('letters',$letters)
			    ->with('users',$users)
			    ->with('search',$search)
			    ->with('letter_id',$letter_id)
			    ->with('searchtype',$searchtype)
			    ->with('cawangan',$cawangan)
			    ->with('status',$status);
	    }




	}


	private function set_ref_no($running_no)
	{
		$date = Carbon::now()->format('dmy');

		return "KOPPIM-".$date."-".str_pad($running_no, 7, '0', STR_PAD_LEFT);
	}


	public function send_letter_to_user(Request $request)
    {
	    $id = $request->input('id');
	    $email = $request->input('emailtosend');
	    $ic = $request->input('ic');
	    $header = $request->input('header');


	    if($email != "" || $email != NULL)
	    {

		    $letter = LetterModule::where('id',$id)->first();
			$content = $this->preview_decode_content($letter->content,$ic);

			$running_no = LetterJobItems::count() + 1;
			$ref_no = $this->set_ref_no($running_no);

			$content = "NO. RUJUKAN: <b>".$ref_no."</b><br><br>".$content;

			$data = [
			            'no-reply' => 'koppim@ppim.org.my',
			            'reply-to' => 'koppim@ppim.org.my',
			            'to' => $email,
			            'to_name' => $email,
			            'subject' => $letter->subject
						];

						Mail::send('Letter.email', ['content' => $content,'header' => $header],
			            function ($message) use ($data)
			            {
			                $message->from($data['no-reply'],'koPPIM')->replyTo($data['reply-to'], 'koPPIM')->to($data['to'], $data['to_name'])->subject($data['subject']);
			            });


						LetterJobItems::create([
							'job_id'=>null,
							'letter_ref_no'=>$ref_no,
							'subject'=>$data['subject'],
							'content'=>$content,
							'ic'=>$ic,
							'header'=>''.$header.'',
							'letter_id'=>$id,
							'email'=>$email,
							'status'=>"sent",
							'by'=>Auth::user()->id,
						]);

	    }
	    else
	   {

	   }


	}





	public function send_letter_to_group(Request $request)
    {
	    $id = $request->input('id');
	    $subject = $request->input('subject');
	    $cawangan = $request->input('cawangan');
	    $cawanganlbl = $request->input('cawanganlbl');
	    $status = $request->input('status');
	    $statuslbl = $request->input('statuslbl');
	    $header = $request->input('header');
		
		$statusaff = $request->input('statusaff');
		
		$filter = $request->input('filter');

		    $total_user = User::where('type','user')->where(function($query ) use ($cawangan,$status)
							{
								if($cawangan != NULL)
								{
									$query->where('parliament', $cawangan);
								}

							    if($status != NULL)
								{
									$query->where('status', $status);
								}

							})->count();



	    $letter = LetterModule::where('id',$id)->first();
		
		

	    LetterJob::create([
		    'letter_id'=>$id,
		    'subject'=>$letter->subject,
		    'content'=>$letter->content,
		    'filter'=>$filter,
		    'cawangan'=>$cawangan,
		    'cawanganlbl'=>$cawanganlbl,
		    'status_id'=>$status,
		    'status_lbl'=>$statuslbl,
		    'statusaff'=>$statusaff,
		    'header'=>$header,
		    'jumlah_pengguna'=>$total_user,
		    'cron_status'=>"MEMPROSES-DATA-PENGGUNA",
		    'created_by'=>Auth::id(),
	    ]);

	}

	public function preview_user_letter(Request $request)
	{
		$ref_no = $request->input('ref_no');
		$type = $request->input('type');
		$letter = LetterJobItems::where('letter_ref_no',$ref_no)->first();

		if($type == "pdf")
		{
			$data = [
			    'content'=>$letter->content,
			    'type'=>'email',
			    'header'=>$letter->header
		    ];

		    $pdf = PDF::loadView('Letter.email', $data);
			return $pdf->download(''.$ref_no.'.pdf');
		}
		else
		{
			return view('Letter.letter')->with('content',$letter->content)->with('header',$letter->header)->with('type',$type);
		}

	}

	public function get_all_user_letter_ajax(Request $request)
    {
	    $ic = $request->input('ic');

	    $letters = LetterJobItems::where('ic',$ic)->orderBy('created_at','DESC')->get();

	    ?>

	    <table class="table">
		    <thead>

			    <tr>
				    <th>NO. RUJUKAN</th>
				    <th>SUBJEK</th>
				    <th>HEADER</th>
				    <th>STATUS</th>
				    <th>TINDAKAN</th>
			    </tr>
		    </thead>
		    <tbody>
			    <?php
				    if(count($letters) > 0){
				    foreach($letters as $letter){ ?>
			    <tr>
				    <td><?php echo $letter->letter_ref_no ?></td>
				    <td><?php echo $letter->subject ?></td>
				    <td><?php echo $letter->header ?></td>
				     <td><?php echo $letter->status ?></td>
				      <td>
					      <div class='badge badge-success'><a target="_blank" style="color: white;" href="/admin/letter/preview/user?ref_no=<?php echo $letter->letter_ref_no ?>&type=preview">Lihat</a></div>
					      <div class='badge badge-success'> <a target="_blank" style="color: white;" href="/admin/letter/preview/user?ref_no=<?php echo $letter->letter_ref_no ?>&type=print">Cetak</a> </div>
					      <div class='badge badge-success'><a target="_blank" style="color: white;" href="/admin/letter/preview/user?ref_no=<?php echo $letter->letter_ref_no ?>&type=pdf">PDF</a></div>

					      </td>
			    </tr>
			    <?php
				    }
				    }
				    else
				    {
					?>
					<tr>
				    <td colspan="4">Tiada Rekod</td>
			    </tr>

					<?php
					}
				     ?>
		    </tbody>
	    </table>

	    <?php
	}


	public function job_list(Request $request)
	{
		$jobs = LetterJob::orderBy('created_at','DESC')->get();

		return view('Letter.job_list')->with('jobs',$jobs);

	}

	//buat lah per 5 minit ker
	public function cron_job(Request $request)
	{
		$jobs = LetterJob::whereIn('cron_status',['MEMPROSES-DATA-PENGGUNA','MEMPROSES-SURAT-PENGGUNA','MENGHANTAR-SURAT-PENGGUNA'])->get();

        $setting = $this->settings_array();

		foreach($jobs as $job)
		{
			if($job->cron_status == "MEMPROSES-DATA-PENGGUNA")
			{
				$cawangan = $job->cawangan;
				$status = $job->status_id;
				
				$filter = $job->filter;
				$statusaff = $job->statusaff;
				
				if($filter == "filteraff")
				{
					
					User::where('type','user')->where(function($query ) use ($statusaff)
							{
								if($statusaff != NULL)
								{
									$query->where('is_affiliate', $statusaff);
								}

							    

							})->update([
								'cron_flag'=>1
							]);
					
					
				}
				else
				{
					User::where('type','user')->where(function($query ) use ($cawangan,$status)
							{
								if($cawangan != NULL)
								{
									$query->where('parliament', $cawangan);
								}

							    if($status != NULL)
								{
									$query->where('status', $status);
								}

							})->update([
								'cron_flag'=>1
							]);
				}
				
				LetterJob::where('_id',$job->_id)->update([
					'cron_status'=>'MEMPROSES-SURAT-PENGGUNA'
				]);
			}
			else if($job->cron_status == "MEMPROSES-SURAT-PENGGUNA")
			{
				$users = User::where('cron_flag',1)->limit($setting['cron_user_per_request'])->get();

				if(count($users) > 0)
				{
					foreach($users as $user)
					{
						$running_no = LetterJobItems::count() + 1;
						$ref_no = $this->set_ref_no($running_no);


						$content = $this->preview_decode_content($job->content,$user->ic);

						$content = "NO. RUJUKAN: <b>".$ref_no."</b><br><br>".$content;

						LetterJobItems::create([
								'job_id'=>$job->_id,
								'letter_ref_no'=>$ref_no,
								'subject'=>$job->subject,
								'content'=>$content,
								'ic'=>$user->ic,
								'header'=>''.$job->header.'',
								'letter_id'=>$job->letter_id,
								'email'=>$user->email,
								'status'=>"pending",
								'by'=>0,
							]);

						User::where('id',$user->id)->update([
							'cron_flag'=>2,
							'cron_datetime'=>Carbon::now()
						]);
					}
				}
				else
				{
					LetterJob::where('_id',$job->_id)->update([
						'cron_status'=>'MENGHANTAR-SURAT-PENGGUNA'
					]);

				}


			}
			else if($job->cron_status == "MENGHANTAR-SURAT-PENGGUNA")
			{

				$items = LetterJobItems::where('job_id',$job->_id)->where('status','pending')->limit($setting['send_email_per_request'])->get();

				if(count($items) > 0)
				{
					foreach($items as $item)
					{


						$data = [
				            'no-reply' => 'koppim@ppim.org.my',
				            'reply-to' => 'koppim@ppim.org.my',
				            'to' => $item->email,
				            'to_name' => $item->email,
				            'subject' => $item->subject
							];

							Mail::send('Letter.email', ['content' => $item->content,'header' => $item->header],
				            function ($message) use ($data)
				            {
				                $message->from($data['no-reply'],'koPPIM')->replyTo($data['reply-to'], 'koPPIM')->to($data['to'], $data['to_name'])->subject($data['subject']);
				            });

				            LetterJobItems::where('_id',$item->_id)->update([
					            'status'=>'sent'
				            ]);

					}
				}
				else
				{
					LetterJob::where('_id',$job->_id)->update([
						'cron_status'=>'SELESAI'
					]);

				}

			}


		}

		//return $jobs;

	}
	
	
	public function job_detail(Request $request)
	{
		$id = $request->input('id');
		
		$job = LetterJob::where('_id',$id)->first();
		
		$users = LetterJobItems::where('job_id',$id)->orderBy('created_at','DESC')->paginate(50);
		
		return view('Letter.job-detail')->with('job',$job)->with('users',$users);

	}
	

}
?>
