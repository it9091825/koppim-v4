<?php

namespace App\Http\Controllers\Admin;

use App\Role;
use App\User;
use Exception;
use Carbon\Carbon;
use App\Permission;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class AdministratorsController extends Controller
{
    public function index()
    {
        $admins = User::where('type', 'admin')->paginate(10);
        $roles = Role::get();

        return view('administrators.index', compact('admins','roles'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string',
            'email' => 'required|email',
            'password' => 'required|confirmed',
            'role_id' => 'required'                        
        ]);

        $user = new User;

        $user->password = Hash::make($request->password);
        $user->name = $request->name;
        $user->email = $request->email;                
        $user->status = 1;     
        $user->ic = microtime();   
        $user->type = 'admin';        
        $user->save();
        
        $user->roles()->attach($request->role_id);

        session()->flash('success', 'Admin '. $request->name .' berjaya ditambah');
        return redirect()->back();
        
    }

    public function edit($id)
    {

        $admin = User::find($id);
        $permissions = Permission::get();
        $roles = Role::get();
        return view('administrators.show', compact('admin', 'permissions', 'roles'));
    }

    public function update(Request $request, $id)
    {

        $this->validate($request, [
            'name' => 'required|string',
            'email' => 'required|email',            
        ]);
        
        if($request->filled('password')){
            $this->validate($request,[
                'password' => 'confirmed'
            ]);
        }
        try {

            $admin = User::find($id);

            $admin->email = $request->email;
            $admin->name = $request->name;            

            if ($request->filled('role_id')) {
                $admin->roles()->sync($request->role_id);
            }

            if ($request->filled('password')) {
                
                $admin->password = Hash::make($request->password);
            }

            $admin->save();
            session()->flash('success', 'Maklumat '. $request->name .' berjaya dikemaskini');
            return redirect()->back();

        } catch (Exception $e) {
            session()->flash('error', $e->getMessage());
            return redirect()->back();
        }
    }

    public function destroy($id)
    {

        try {
            $admin = User::find($id);
            $admin->roles()->detach();
            $admin->delete();
            session()->flash('success', 'Admin '.$admin->name.' berjaya dihapuskan');
            return redirect()->back();
        } catch (Exception $e) {
            session()->flash('error', $e->getMessage());
            return back();
        }
    }
}
