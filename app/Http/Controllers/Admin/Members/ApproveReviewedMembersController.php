<?php

namespace App\Http\Controllers\Admin\Members;

use Auth;
use App\User;
use Exception;
use Carbon\Carbon;
use App\Mongo\Payment;
use App\Traits\SmsTrait;
use App\Traits\NexmoTrait;
use App\Traits\MemberTrait;
use Illuminate\Http\Request;
use App\Jobs\ProcessCommission;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use App\Mail\Payment\SentPaymentProof;
// use Spatie\Activitylog\Contracts\Activity;
use App\Mail\User\ApprovedRegistration;
use Spatie\Activitylog\Models\Activity;

class ApproveReviewedMembersController extends Controller
{
    use SmsTrait, MemberTrait;

    public function store(Request $request)
    {

        $membersId = $request->member;

        foreach ($membersId as $id) {

            $user = User::find($id);

            if ($user) {

                $userCurrentStatus = $user->status;

                $user_ic = $user->ic;

                // echo $user_ic;

                $user->update([
                    'status' => 1,
                    'approved_date' => Carbon::now()
                ]);

                // generate no. koppim
                $this->generate_no_koppim($user);

                // set log status
                activity('admin_member_approved')
                    ->performedOn($user)
                    ->causedBy(Auth::user())->withProperties(
                        [
                            'old' => [
                                'Status' => $userCurrentStatus,
                                'Justifikasi Penukaran Status' => 'UNTUK DILULUSKAN'
                            ],
                            'new' => [
                                'Status' => $user->status,
                                'Justifikasi Penukaran Status' => 'AKTIF',
                            ]
                        ]
                    )->log('Telah Diluluskan');

                //Dispatch Job for processing commission if there's introducer on anggota payment
                // ProcessCommission::dispatch($user);

                //update comission payment for affiliate
                $payment =  Payment::where('introducer', true)
                ->where('status', 'payment-completed')
                ->where('introducer_commission', '!=', 10)
                ->where('ic', $user_ic)
                ->first();

                if (isset($payment->introducer)) {

                    $payment_affiliate_id = $payment->introducer_no_anggota;

                    //Introducer setting
                    $introducer = User::where('no_koppim', (int)$payment->introducer_no_anggota)->first();
                    $introducer_id = $introducer->no_koppim;
                    if ($payment_affiliate_id == $introducer_id) {

                    //add columnn  komisen and status available for affiliates
                        $payment->introducer_commission = 10;
                        $payment->introducer_commission_status = 'available';
                        $payment->save();


                    }


                }


                // send nexmo_sms
                $text = "Tahniah! Permohonan anda telah diluluskan dan akaun anda sudah diaktifkan.";
                try {
                    $this->send_sms($user->ic, $user->mobile_no, $text);
                    // send email
                    Mail::to($user->email)->send(new ApprovedRegistration($user->name, $user->no_koppim));
                } catch (\Exception $e) {
                    Log::error($e->getMessage());
                }


            }
        }

        session()->flash('success', 'Pengaktifan akaun anggota telah berjaya');
        return redirect()->back();
    }
}
