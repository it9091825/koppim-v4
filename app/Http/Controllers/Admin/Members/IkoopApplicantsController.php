<?php

namespace App\Http\Controllers\Admin\Members;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;

class IkoopApplicantsController extends Controller
{
    public function index(Request $request)
    {

        $search = $request->input('search');

        if ($search) {

            $members = User::where('type', 'user')->where('ikoop','!=',0)->where(function ($query) use ($search) {

                $query->where('name', 'LIKE', '%' . $search . '%')->orWhere('ic', 'LIKE', '%' . $search . '%')->orWhere('mobile_no', 'LIKE', '%' . $search . '%')->orWhere('home_no', 'LIKE', '%' . $search . '%');
            })->sortable()->paginate(20);

            return view('admin.users.senarai-ikoop')->with('members', $members)->with('search', $search);
        } else {

            $status = $request->input('status');

            if ($status !== null) {
                $members = User::where('type', 'user')->where('ikoop','!=',0)->where('status', $status)->sortable()->paginate(20);
            } else {
                $members = User::where('type', 'user')->where('ikoop','!=',0)->sortable()->paginate(20);
            }

            return view('admin.users.senarai-ikoop')->with('members', $members)->with('search', $search);
        }
    }
}
