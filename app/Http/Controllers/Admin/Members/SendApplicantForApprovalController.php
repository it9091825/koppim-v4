<?php

namespace App\Http\Controllers\Admin\Members;

use App\Http\Controllers\Controller;
use App\User;
use Exception;
use Illuminate\Http\Request;
use Auth;

class SendApplicantForApprovalController extends Controller
{
    public function store(Request $request){

        try{

            $user = User::find($request->user_id);
            $userCurrentStatus = $user->status;

            $user->update([
                'status' => 3
            ]);

            activity('admin_member_send_for_approval')
                ->performedOn($user)
                ->causedBy(Auth::user())->withProperties(
                    [
                        'old' => [
                            'Status' => $userCurrentStatus,
                            'Justifikasi Penukaran Status' => 'TELAH DIKEMASKINI'
                        ],
                        'new' => [
                            'Status' => $user->status,
                            'Justifikasi Penukaran Status' => 'UNTUK DILULUSKAN',
                        ]
                    ]
                )->log('Untuk Diluluskan');

            session()->flash('success',$user->name.' telah berjaya dihantar untuk kelulusan');

        }catch(Exception $e){
            session()->flash('error',$e->getMessage());
        }

        return redirect()->back();

    }
}
