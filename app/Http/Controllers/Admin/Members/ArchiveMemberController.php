<?php

namespace App\Http\Controllers\Admin\Members;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\UserArchive;
use Illuminate\Support\Facades\Session;

class ArchiveMemberController extends Controller
{
    public function store(User $user){

        if(!strpos(url()->previous(), '/admin/members/view'))
        {
            Session::put('admin_member_back', url()->previous());
        }

        $archive = $user->replicate()->toArray();

        try {
            UserArchive::create($archive);
            $user->delete();
        } catch (Throwable $e) {
            report($e);
            return false;
        }

        return redirect(Session::get('admin_member_back'))->with('success', 'Anggota telah diarkib');

    }
}
