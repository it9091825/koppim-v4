<?php

namespace App\Http\Controllers\Admin\Members;

use App\Http\Controllers\Controller;
use App\Jobs\SendSmsJob;
use Illuminate\Http\Request;
use App\User;
use App\UserSmsReminderLog;
use Exception;
use Illuminate\Support\Facades\Log;

class PendingApplicantsController extends Controller
{
    public function index(Request $request)
    {

        $search = $request->input('search');

        if ($search) {

            $members = User::where('type', 'user')->where('status', 0)->where(function ($query) use ($search) {

                $query->where('name', 'LIKE', '%' . $search . '%')->orWhere('ic', 'LIKE', '%' . $search . '%')->orWhere('mobile_no', 'LIKE', '%' . $search . '%')->orWhere('home_no', 'LIKE', '%' . $search . '%');
            })->sortable()->paginate(20);

            return view('admin.users.pending-applicants')->with('members', $members)->with('search', $search);
        } else {

            $members = User::where('type', 'user')->where(function ($query) {

                $query->where('status', 0);
            })->sortable()->paginate(20);

            return view('admin.users.pending-applicants')->with('members', $members)->with('search', $search);
        }
    }

    public function sendReminder(Request $request)
    {

        $member_ids = $request->members;
        $message = $request->message;

        $users = User::whereIn('id', $member_ids)->get();

        if ($message != '' || $message != null) {
            if ($users) {

                foreach ($users as $user) {
                    try {
                        SendSmsJob::dispatch($user, $message);
                        UserSmsReminderLog::create([
                            'user_id' => $user->id
                        ]);
                    } catch (Exception $e) {
                        activity()->log('SMS Peringatan tidak berjaya dihantar. Id Pelanggan:' . $user->id);
                        Log::error($e->getMessage());
                        continue;
                    }
                }

                return response()->json(['success' => true]);
            }
        }
    }
}
