<?php

namespace App\Http\Controllers\Admin\Members;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;

class NewApplicantsController extends Controller
{
    public function index(Request $request)
    {

        $search = $request->input('search');

        if ($search) {

            $members = User::where('type', 'user')->where(function ($query) {

                $query->where('status', 2);
            })->where(function ($query) use ($search) {

                $query->where('name', 'LIKE', '%' . $search . '%')->orWhere('ic', 'LIKE', '%' . $search . '%')->orWhere('mobile_no', 'LIKE', '%' . $search . '%')->orWhere('home_no', 'LIKE', '%' . $search . '%');
            })->sortable(['created_at'=>'desc'])->paginate(20);

            return view('admin.users.new-applicants')->with('members', $members)->with('search', $search);
        } else {

            $status = $request->input('status');

            if ($status !== null) {
                $members = User::where('type', 'user')->where('status', $status)->sortable(['created_at'=>'desc'])->paginate(20);
            } else {
                $members = User::where('type', 'user')->where(function ($query) {

                    $query->where('status', 2);
                })->sortable(['created_at'=>'desc'])->paginate(20);
            }

            return view('admin.users.new-applicants')->with('members', $members)->with('search', $search);
        }
    }
}
