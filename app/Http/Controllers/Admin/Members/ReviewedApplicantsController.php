<?php

namespace App\Http\Controllers\Admin\Members;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;

class ReviewedApplicantsController extends Controller
{
    public function index(Request $request)
    {

        $search = $request->input('search');

        if ($search) {

            $members = User::where('type', 'user')->where('status', 3)->where(function ($query) use ($search) {

                $query->where('name', 'LIKE', '%' . $search . '%')->orWhere('ic', 'LIKE', '%' . $search . '%')->orWhere('mobile_no', 'LIKE', '%' . $search . '%')->orWhere('home_no', 'LIKE', '%' . $search . '%');

            })->sortable()->paginate(20);

            return view('admin.users.reviewed-applicants')->with('members', $members)->with('search', $search);
        } else {

            $members = User::where('type', 'user')->where(function ($query) {

                $query->where('status', 3);
            })->sortable()->paginate(20);

            return view('admin.users.reviewed-applicants')->with('members', $members)->with('search', $search);
        }
    }
}
