<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Exception;
// use Illuminate\Support\Facades\Artisan;
use Illuminate\Http\Request;

class MaintenanceModeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {

        return view('admin.maintenance');
    }

    public function store(Request $request)
    {
        $messages = [
            'from.required' => 'Ruangan ini wajib diisi',
            'to.required' => 'Rouangan ini wajib diisi',
        ];

        $request->validate([
            'from' => 'required',
            'to' => 'required',
        ], $messages);

        try {


            $date = now()->format('d F Y H:m:s');
            $from = $request->from;
            $to = $request->to;

            $message = "<small>{$date}</small><br><br>Harap Maaf. Laman Web ini sedang dinaiktaraf bermula dari <b>{$from}</b> sehingga <b>{$to}</b>. Kami memohon maaf di atas sebarang kesulitan. Sila hubungi Hotline KoPPIM di talian <b>016-2829075</b> atau emel kepada <b>hello@koppim.com.my</b> sekiranya anda mempunyai sebarang pertanyaan atau pun maklum balas. Terima kasih.";
            \Artisan::call("down --message='" . $message . "'");

        } catch(Exception $e) {
            \Log::info($e->getMessage());
        }
        return redirect()->back();
    }

    public function destroy()
    {
        try {
            \Artisan::call('up');
        } catch (Exception $e) {
            \Log::info($e->getMessage());
        }

        return redirect()->back();
    }
}
