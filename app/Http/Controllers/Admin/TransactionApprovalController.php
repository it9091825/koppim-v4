<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Mail\User\NewRegistration;
use App\Mongo\Payment;
use App\Traits\NexmoTrait;
use App\Traits\OrderTrait;
use App\Traits\SmsTrait;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Mail;

class TransactionApprovalController extends Controller
{
    use SmsTrait, OrderTrait;

    public function index(Request $request)
    {
        $search = $request->input('search');

        if ($search) {
            $payments = Payment::where('channel', 'manual')
                ->where('status', 'waiting-for-approval')
                ->where(function ($query) use ($search) {
                    $query->where('name', 'LIKE', '%' . $search . '%')
                        ->orWhere('ic', 'LIKE', '%' . $search . '%')
                        ->orWhere('phone', 'LIKE', '%' . $search . '%');
                })
                ->sortable(['name'])->paginate(10);

            return view('admin.transaction_approval')
                ->with('payments', $payments)->with('search', $search);
        } else {

            $payments = Payment::where('channel', 'manual')->where('status', 'waiting-for-approval')->sortable(['name'])->paginate(10);
            return view('admin.transaction_approval')
                ->with('payments', $payments)->with('search', $search);
        }
    }

    public function luluskan(Request $request)
    {
        if(!strpos(url()->previous(), '/admin/members/view'))
        {
            Session::put('admin_member_back', url()->previous());
        }

        $payment = Payment::findOrFail($request->proof_id);

        if ($payment) {

            if($request->sah_button == "sah") {

                //Check or create user:
                $user = User::where('type', 'user')->where('ic', $payment->ic)->first();

                //Create user if not exist
                if (!$user) {
                    $pin = rand(111111, 999999);
                    $password = Hash::make($pin);

                    $user = new User;
                    $user->name = $payment->name;
                    $user->ic = $payment->ic;
                    $user->mobile_no = $payment->phone;
                    $user->email = $payment->email;
                    $user->password = $password;
                    $user->type = 'user';
                    $user->status = 0;
                    $user->password_reset = 1;
                    $user->registration_type = 'online';
                    $user->save();

                    //send sms/email
                    try {
                        $text = "Permohonan anda telah diterima. Log masuk akaun di anggota.koppim.com.my dengan kata laluan sementara " . $pin . " & kemaskini profil anda.";
                        $this->send_sms($payment->ic, $payment->phone, $text);
                        //send email
                        Mail::to($payment->email)->send(new NewRegistration(strtoupper($payment->name), $payment->ic, $payment->phone, $payment->email, $user->created_at, $payment->first_time_share, $payment->additional_share, $payment->amount));
                    } catch (\Exception $e) {
                        activity('exception')->log($e->getMessage());
                    }
                }

                $payment->update([
                    'status' => 'payment-completed',
                    'returncode' => '100',
                    'approved_date' => Carbon::now(),
                    'approved_by' => Auth::User()->id,
                    'note' => $request->note,
                    'user_id' => $user->id,
                ]);

                activity('admin_manual_payment_approval_approved')
                    ->withProperties(
                        [
                            'payment_id' => $payment->id,
                            'payment_name' => $payment->name,
                            'payment_ic' => $payment->ic,
                            'payment_email' => $payment->email,
                            'payment_phone' => $payment->phone,
                            'note' => $payment->note,
                            'Oleh' => Auth::User()->name,
                            'Tarikh' => Carbon::now(),
                        ]
                    )
                    ->log('Pengesahan Bayaran Manual Telah Disahkan');

                session()->flash('success', 'Transaksi Bayaran Manual: ' . $payment->name. ' telah diluluskan');

            } else {

                $payment->status = 'approval-cancelled';
                $payment->approved_by = Auth::User()->id;
                $payment->approved_date = Carbon::now();
                $payment->note = $request->note;
                $payment->save();

                activity('admin_manual_payment_approval_cancelled')
                    ->withProperties(
                        [
                            'payment_id' => $payment->id,
                            'payment_name' => $payment->name,
                            'payment_ic' => $payment->ic,
                            'payment_email' => $payment->email,
                            'payment_phone' => $payment->phone,
                            'note' => $payment->note,
                            'Oleh' => Auth::User()->name,
                            'Tarikh' => Carbon::now(),
                        ]
                    )
                    ->log('Pengesahan Bayaran Manual Telah Dibatalkan');

                session()->flash('success', 'Transaksi Bayaran Manual: ' . $payment->name . ' telah dibatalkan');

            }

            return redirect(Session::get('admin_member_back'));
        }

    }

    //Not used
    public function batalkan(Request $request)
    {
        $payment = Payment::findOrFail($request->proof_id);
        $payment->status = 'approval-cancelled';
        $payment->approved_by = Auth::User()->id;
        $payment->approved_date = Carbon::now();
        $payment->note = $request->note;
        $payment->save();

        activity('admin_manual_payment_approval_cancelled')
            ->withProperties(
                [
                    'payment_id' => $payment->id,
                    'payment_name' => $payment->name,
                    'payment_ic' => $payment->ic,
                    'payment_email' => $payment->email,
                    'payment_phone' => $payment->phone,
                    'note' => $payment->note,
                    'Oleh' => Auth::User()->name,
                    'Tarikh' => Carbon::now(),
                ]
            )
            ->log('Kelulusan Bayaran Manual Dibatalkan');


        session()->flash('success', 'Transaksi Bayaran Manual:' . $request->proof_id . '  telah dibatalkan');

        return redirect('/admin/tx/approval');
    }

    public function hantarKembali(Request $request){
        $payment = Payment::findOrFail($request->proof_id);
        if($payment){
            $payment->status = 'waiting-for-reverification';
            $payment->note = $request->note;
            $payment->save();

            activity('admin_manual_payment_approval_cancelled')
                ->withProperties(
                    [
                        'payment_id' => $payment->id,
                        'payment_name' => $payment->name,
                        'payment_ic' => $payment->ic,
                        'payment_email' => $payment->email,
                        'payment_phone' => $payment->phone,
                        'Oleh' => Auth::User()->name,
                        'Tarikh' => Carbon::now(),
                    ]
                )
                ->log('Kelulusan Bayaran Manual Telah Dihantar Kembali');

            return response()->json([
                'data' => 'Bayaran telah dihantar kembali ke bayaran manual.',
            ]);
        }

        return response()->json([
            'data' => 'Maklumat Tidak Dijumpai Dalam Sistem.',
        ]);
    }
}
