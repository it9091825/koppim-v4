<?php

namespace App\Http\Controllers\Admin;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\temp_paid_data;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Validator;
use Mail;
use Helper;


class AdminController extends Controller
{

    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        return view('admin.login');
    }


    public function login_by_email_exe(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required'
        ], [
            'email.required' => 'E-mel wajib dimasukkan',
            'password.required' => 'Kata Laluan wajib dimasukkan',
            'email.email' => 'Sila masukkan e-mel yang sah'
        ]);

        $email = $request->input('email');
        $password = $request->input('password');

        $log = User::where('email', $email)->where('type', 'admin')->first();

        if ($log) {


            if (Hash::check($password, $log->password)) {

                Auth::loginUsingId($log->id);

                activity('admin_auth')
                    ->performedOn($log)
                    ->withProperties(
                        [
                            'Nama Admin' => strtoupper($log->name),
                            'Emel' => $log->email,
                            'Alamat IP' => $request->ip(),
                        ]
                    )
                    ->log('Admin Log Masuk');

                return redirect('/admin/dashboard')->with('success', 'Anda telah berjaya log masuk ke akaun anda.');
            } else {

                return redirect()->back()->withErrors(['E-mel atau Kata Laluan tidak betul.']);
            }
        } else {

            return redirect()->back()->withErrors(['E-mel atau Kata Laluan tidak betul.']);
        }
    }

    //    public function login_by_email_auth(Request $request)
    //    {
    //        $token = $request->input('token');
    //        $log = User::where('token', $token)->first();
    //
    //        if ($log) {
    //            Auth::loginUsingId($log->id);
    //
    //            if ($log->first_time_register == 0) {
    //                User::where('id', $log->id)->update([
    //                    'first_time_register' => 1
    //                ]);
    //
    //                Merchant::where('phone', $log->mobile_no)->update([
    //                    'user_id' => $log->id
    //                ]);
    //
    //            }
    //
    //            return redirect('/dashboard')->with('success', 'Anda telah berjaya log masuk ke akaun anda.');
    //        } else {
    //
    //        }
    //
    //    }


    public function temporary_data_cal(Request $request)
    {

        $temps = temp_paid_data::where('cron', 0)->limit(2)->get();

        $array = array();

        foreach ($temps as $temp) {
            array_push($array, $temp);
        }

        return $array;
    }
}
