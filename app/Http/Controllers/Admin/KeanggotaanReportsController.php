<?php

namespace App\Http\Controllers\Admin;

use App\Filters\RingkasanPendaftaranFilter;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\User;
use Carbon\Carbon;
use Exception;

class KeanggotaanReportsController extends Controller
{
    public function __construct()
    {
        $this->user = DB::table('users')->where('type', 'user');
    }

    public function byParliament()
    {
        $users = $this->user->count();
        $parliament = $this->user->where('status', 1)->select('parliament', DB::raw('count(*) as jumlah'))
            ->whereNotNull('parliament')
            ->groupBy('parliament')
            ->get();
        return view('admin.reports.summary.byparliament', compact('parliament', 'users'));
    }

    public function byGender()
    {
        $gender = $this->user
            ->select('sex', 'parliament')
            ->whereNotNull('parliament')
            ->whereNotNull('sex')
            ->get()->groupBy('parliament');

        $g = [];

        foreach ($gender as $key => $ge) {

            $b = collect($ge);
            $v = $b->map(function ($item, $key) {
                return ['sex' => strtoupper($item->sex)];
            })->groupBy('sex')->map(function ($item) {
                return count($item);
            });
            $g[] = $v->merge(['PARLIAMENT' => strtoupper($key)])->all();
        }

        return view('admin.reports.summary.bygender')->with('gender', $g);
        // return response()->json($g);
    }

    public function byRace()
    {
        $race = $this->user
            ->select('race', 'parliament')
            ->whereNotNull('parliament')
            ->whereNotNull('race')
            ->get()->groupBy('parliament');

        $g = [];

        foreach ($race as $key => $ge) {

            $b = collect($ge);
            $v = $b->map(function ($item, $key) {
                return ['race' => strtoupper($item->race)];
            })->groupBy('race')->map(function ($item) {
                return count($item);
            });
            $g[] = $v->merge(['PARLIAMENT' => strtoupper($key)])->all();
        }

        return view('admin.reports.summary.byrace')->with('race', $g);
        // return response()->json($g);
    }

    public function byReligion()
    {
        $religion = $this->user
            ->select('religion', 'parliament')
            ->whereNotNull('parliament')
            ->whereNotNull('religion')
            ->get()->groupBy('parliament');

        $g = [];

        foreach ($religion as $key => $ge) {

            $b = collect($ge);
            $v = $b->map(function ($item, $key) {
                return ['religion' => strtoupper($item->religion)];
            })->groupBy('religion')->map(function ($item) {
                return count($item);
            });
            $g[] = $v->merge(['PARLIAMENT' => strtoupper($key)])->all();
        }

        return view('admin.reports.summary.byreligion')->with('religion', $g);
        // return response()->json($g);
    }

    public function byIncome()
    {
        $income_ranges = [
            'less1000' => 1000,
            'between10012000' => 1001,
            'between20013000' => 2001,
            'between30014000' => 3001,
            'between40015000' => 4001,
            'greater5001' => 5001
        ];

        $monthly_income = \App\User::where('type', 'user')->select('parliament', 'monthly_income')->get()
            ->map(function ($user) use ($income_ranges) {

                $income = $user->monthly_income;
                foreach ($income_ranges as $key => $breakpoint) {

                    if ($income) {
                        if ($key == 'less100') {
                            if ($breakpoint <= $income) {
                                $user->range = $key;
                                break;
                            }
                        } else {
                            if ($breakpoint >= $income) {
                                $user->range = $key;
                                break;
                            }
                        }
                    }
                }

                return $user;
            })->mapToGroups(function ($user) {
                return [$user->range => $user];
            })->map(function ($group) {
                return count($group);
            })->sortKeysDesc()->toArray();

        // return response()->json($monthly_income);
        return view('admin.reports.summary.byincome', compact('monthly_income'));
    }

    public function byAge()
    {
        $age_ranges = [
            'less20' => 20,
            'between2125' => 21,
            'between2630' => 26,
            'between3135' => 31,
            'between3640' => 36,
            'between4145' => 41,
            'between4650' => 46,
            'between5155' => 51,
            'greater55' => 55
        ];

        $ages = \App\User::where('type', 'user')->where('dob', '!=', null)->select('dob')->get()
            ->map(function ($user) use ($age_ranges) {


                $ic_year = $user->dob;
                if ($ic_year) {
                    $ic_year = $ic_year->format('Y');
                    $age = Carbon::now()->year - (int)$ic_year;
                }

                foreach ($age_ranges as $key => $breakpoint) {

                    if (isset($age)) {
                        if ($key == 'less20') {
                            if ($breakpoint <= $age) {
                                $user->range = $key;
                                break;
                            }
                        }

                        if ($key == 'between2125') {
                            if ($breakpoint >= $age && $breakpoint <= 25) {
                                $user->range = $key;
                                break;
                            }
                        }
                        if ($key == 'between2630') {
                            if ($breakpoint >= $age && $breakpoint <= 30) {
                                $user->range = $key;
                                break;
                            }
                        }
                        if ($key == 'between3135') {
                            if ($breakpoint >= $age && $breakpoint <= 35) {
                                $user->range = $key;
                                break;
                            }
                        }
                        if ($key == 'between3640') {
                            if ($breakpoint >= $age && $breakpoint <= 40) {
                                $user->range = $key;
                                break;
                            }
                        }
                        if ($key == 'between4145') {
                            if ($breakpoint >= $age && $breakpoint <= 45) {
                                $user->range = $key;
                                break;
                            }
                        }
                        if ($key == 'between4650') {
                            if ($breakpoint >= $age && $breakpoint <= 50) {
                                $user->range = $key;
                                break;
                            }
                        }
                        if ($key == 'between5155') {
                            if ($breakpoint >= $age && $breakpoint <= 55) {
                                $user->range = $key;
                                break;
                            }
                        }
                        if ($key == 'greater55') {
                            if ($breakpoint > $age) {
                                $user->range = $key;
                                break;
                            }
                        }
                    }
                }

                return $user;
            })->mapToGroups(function ($user) {
                return [$user->range => $user];
            })->map(function ($group) {
                return count($group);
            })->sortKeysDesc()->toArray();

        // return response()->json($ages);
        return view('admin.reports.summary.byage', compact('ages'));
    }


    public function byState()
    {
        $users = $this->user->count();
        $states = $this->user->where('status', 1)->select('state', DB::raw('count(*) as jumlah'))
            ->whereNotNull('state')
            ->groupBy('state')
            ->get();
        return view('admin.reports.summary.bystate', compact('states', 'users'));
    }

    public function byPendaftaranStatus(Request $request)
    {

        $users = User::whereYear('created_at', $request->year ?? Carbon::now()->year)->get();

        $status = [
            'BelumDikemaskini' => 0,
            'Diluluskan' => 1,
            'SedangDisemak' => 2,
            'Gagal' => 99
        ];

        $users = $users->groupBy(function ($d) {
            return $d->created_at->format('F');
        })->map(function ($user) use ($status) {
            return $user->map(function ($user) use ($status) {
                foreach ($status as $key => $s) {
                    if ($s == $user->status) {
                        $user->collected_status = $key;
                        break;
                    }
                }

                return $user;
            })->mapToGroups(function ($user) {
                return [$user->collected_status => $user];
            })->map(function ($g) {
                return count($g);
            });
        });

        // return response()->json($users);

        return view('admin.reports.summary.byregistrationstatus', compact('users'));
    }

    public function mykadNotExists(Request $request)
    {

        $users = User::select('name', 'ic', 'mobile_no')->where('type', 'user')->where(function ($query) {
            $query->where('mykad_front', '')->orWhereNull('mykad_front');
        })->get();

        // return response()->json($users);

        return view('admin.reports.summary.no-mykad', compact('users'));
    }

    public function byInfoKoppim()
    {
        $users = $this->user->count();

        // $get_froms = DB::table('users')
        //          ->select('know_from')
        //          ->distinct()
        //          ->orderBy('know_from', 'DESC')
        //          ->get();


        $radio_ads = User::where('know_from', 'RADIO_ADS')->count();
        $billboards = User::where('know_from', 'BILLBOARDS')->count();
        $website = User::where('know_from', 'WEBSITE')->count();
        $facebook = User::where('know_from', 'FACEBOOK')->count();
        $instagram = User::where('know_from', 'INSTAGRAM')->count();
        $affiliate = User::where('know_from', 'AFFILIATE')->count();
        $others = User::where('know_from', 'OTHERS')->count();
        $old_user = User::where('know_from', '')->count();


        $data1 = (array)$website;
        $data2 = (array)$radio_ads;
        $data3 = (array)$others;
        $data4 = (array)$instagram;
        $data5 = (array)$facebook;
        $data6 = (array)$billboards;
        $data7 = (array)$affiliate;
        $data8 = (array)$old_user;


        $all_infos = array_merge($data1, $data2, $data3, $data4, $data5,  $data6, $data7, $data8);


        return view('admin.reports.summary.byinfokoppim', compact('users', 'all_infos'));

    }
}
