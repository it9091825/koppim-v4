<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Mongo\Payment;
use Carbon\Carbon;

class TransaksiBayaran extends Controller
{
    public function fetch(Request $request)
    {

        if ($request->filled('month') && $request->filled('year')) {

            $start = Carbon::parse($request->year . '-' . $request->month . '-01 00:00:00');
            $end = Carbon::parse($request->year . '-' . $request->month . '-31 23:59:59');

            //CAMS
            $jumlahFi = Payment::where('channel', '!=', 'ikoop')->whereBetween('created_at', [$start, $end])->where('returncode', '100')->sum('first_time_fees');
            $jumlahSyer = Payment::where('channel', '!=', 'ikoop')->whereBetween('created_at', [$start, $end])->where('returncode', '100')->sum('share_amount');

            //SENANGPAY
            $jumlahFiSenangpay = Payment::where('channel', 'online')->where('gateway', 'senangpay')->where('returncode', '100')->whereBetween('created_at', [$start, $end])->sum('first_time_fees');
            $jumlahSyerSenangpay = Payment::where('channel', 'online')->where('gateway', 'senangpay')
                ->where('returncode', '100')->whereBetween('created_at', [$start, $end])->sum('share_amount');

            //RAUDHAHPAY
            $jumlahFiRaudhahpay = Payment::where('gateway', 'raudhahpay')->where('returncode', '100')->whereBetween('created_at', [$start, $end])->sum('first_time_fees');
            $jumlahSyerRaudhahpay = Payment::where('gateway', 'raudhahpay')
                ->where('returncode', '100')->whereBetween('created_at', [$start, $end])->sum('share_amount');

            //IKOOP
            $jumlahFiIkoop = Payment::where('channel', 'ikoop')->where('returncode', '100')->whereBetween('created_at', [$start, $end])->sum('first_time_fees');
            $jumlahSyerIkoop = Payment::where('channel', 'ikoop')->where('returncode', '100')->whereBetween('created_at', [$start, $end])->sum('share_amount');
        } else {

            $start = Carbon::parse($request->year . '-01-01 00:00:00');
            $end = Carbon::parse($request->year . '-12-31 23:59:59');

            //CAMS
            $jumlahFi = Payment::where('channel', '!=', 'ikoop')->whereBetween('created_at', [$start, $end])->where('returncode', '100')->sum('first_time_fees');
            $jumlahSyer = Payment::where('channel', '!=', 'ikoop')->whereBetween('created_at', [$start, $end])->where('returncode', '100')->sum('share_amount');

            //SENANGPAY
            $jumlahFiSenangpay = Payment::where('channel', 'online')->where('gateway', 'senangpay')->where('returncode', '100')->whereBetween('created_at', [$start, $end])->sum('first_time_fees');
            $jumlahSyerSenangpay = Payment::where('channel', 'online')->where('gateway', 'senangpay')
                ->where('returncode', '100')->whereBetween('created_at', [$start, $end])->sum('share_amount');

            //RAUDHAHPAY
            $jumlahFiRaudhahpay = Payment::where('gateway', 'raudhahpay')->where('returncode', '100')->whereBetween('created_at', [$start, $end])->sum('first_time_fees');
            $jumlahSyerRaudhahpay = Payment::where('gateway', 'raudhahpay')
                ->where('returncode', '100')->whereBetween('created_at', [$start, $end])->sum('share_amount');

            //IKOOP
            $jumlahFiIkoop = Payment::where('channel', 'ikoop')->where('returncode', '100')->whereBetween('created_at', [$start, $end])->sum('first_time_fees');
            $jumlahSyerIkoop = Payment::where('channel', 'ikoop')->where('returncode', '100')->whereBetween('created_at', [$start, $end])->sum('share_amount');
        }


        $jumlahKeseluruhan = $jumlahFi + $jumlahSyer;
        $jumlahKeseluruhanSenangpay = $jumlahFiSenangpay + $jumlahSyerSenangpay;
        $jumlahKeseluruhanRaudhahpay = $jumlahFiRaudhahpay + $jumlahSyerRaudhahpay;
        $jumlahKeseluruhanIkoop = $jumlahFiIkoop + $jumlahSyerIkoop;

        return response()->json([
            'jumlahFiCams' => $this->moneyFormat($jumlahFi),
            'jumlahSyerCams' =>  $this->moneyFormat($jumlahSyer),
            'jumlahKeseluruhanCams' =>  $this->moneyFormat($jumlahKeseluruhan),
            'jumlahFiSenangpay' =>  $this->moneyFormat($jumlahFiSenangpay),
            'jumlahSyerSenangpay' =>  $this->moneyFormat($jumlahSyerSenangpay),
            'jumlahKeseluruhanSenangpay' =>  $this->moneyFormat($jumlahKeseluruhanSenangpay),
            'jumlahFiRaudhahpay' =>  $this->moneyFormat($jumlahFiRaudhahpay),
            'jumlahSyerRaudhahpay' =>  $this->moneyFormat($jumlahSyerRaudhahpay),
            'jumlahKeseluruhanRaudhahpay' =>  $this->moneyFormat($jumlahKeseluruhanRaudhahpay),
            'jumlahFiIkoop' =>  $this->moneyFormat($jumlahFiIkoop),
            'jumlahSyerIkoop' =>  $this->moneyFormat($jumlahSyerIkoop),
            'jumlahKeseluruhanIkoop' =>  $this->moneyFormat($jumlahKeseluruhanIkoop),
            'month' => $request->month,
            'year' => $request->year
        ]);
    }

    public function moneyFormat($money)
    {
        return 'RM ' . number_format($money, 2, '.', ',');
    }
}
