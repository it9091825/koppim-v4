<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ParliamentsController extends Controller
{
    public function byState(Request $request)
    {
        $parliaments = DB::table('parliaments')->where('state_id', $request->state_id)->pluck('name');

        return response()->json($parliaments);
    }
}
