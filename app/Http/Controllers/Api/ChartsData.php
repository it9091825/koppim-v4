<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Carbon\Carbon;
use App\Mongo\Payment;
use Illuminate\Support\Facades\DB;

class ChartsData extends Controller
{
    public function fetch(Request $request)
    {
        $data_online = array();
        $data_manual = array();
        $date_data = array();
        $age_range = [
            '18-24' => 18,
            '25-29' => 25,
            '30-34' => 30,
            '35-39' => 35,
            '40-44' => 40,
            '45-49' => 45,
            '50-54' => 50,
            '55-59' => 55,
            '60+' => 60
        ];

        if ($request->filled('month') && $request->filled('type')) {
            $periods = Carbon::parse($request->year . '-' . $request->month . '-01 00:00:00')->toPeriod($request->year . '-' . $request->month . '-31 23:59:59');
            $arr = $periods->toArray();
            $plength = count($arr);
            $start = $arr[0];
            $end = $arr[$plength-1];
            $monthName = utf8_encode(strftime(date("F", mktime(0, 0, 0, $request->month, 1))));
            switch($request->type) {
                case "reg_stat":
                    foreach($periods as $period) {
                        $date = date('Y-m-d', strtotime($period));
                        $date_data[] = $date;
                        $data_online[] = User::whereRaw("DATE(created_at) = '".$date."'")-> where('type', 'user')->where('ikoop', 0)->count();
                    }
                    return response()->json([
                        'data_online' => $data_online,
                        'date' => $date_data,
                        'month' => $monthName,
                        'year' => $request->year,
                    ]);
                    break;
                case "payment_stat":
                    $channels = Payment::where('payment_desc', 'Pendaftaran Anggota Baharu Dan Langganan Modal Syer')
                        ->whereBetween('created_at', [$start, $end])
                        ->whereNotNull('channel')
                        ->where(function ($query) {
                            $query->where('channel', 'online')
                                ->orWhere('channel', 'manual');
                        })
                        ->groupBy('channel')
                        ->get();
                    $data = [];
                    foreach ($channels as $channel) {
                        $data[$channel->channel] = Payment::where('payment_desc', 'Pendaftaran Anggota Baharu Dan Langganan Modal Syer')
                                            ->whereBetween('created_at', [$start, $end])
                                            ->where('channel', $channel->channel)
                                            ->count();
                    };
                    $label = [];
                    $count = [];
                    foreach ($data as $key => $item) {
                        array_push($label, strtoupper($key));
                        array_push($count, $item);
                    }
                    return response()->json([
                       'label' => $label,
                       'count' => $count,
                    ]);
                    break;
                case "state_stat":
                    $data = User::where('type', 'user')->whereBetween('created_at', [$start, $end])
                        ->whereNotNull('state')
                        ->whereNotNull('parliament')
                        ->whereNotNull('sex')
                        ->whereNotNull('race')
                        ->whereNotNull('religion')
                        ->select('state', DB::raw('count(*) as jumlah'))
                        ->groupBy('state')
                        ->get();

                    return response()->json([
                        'data' => $data,
                    ]);
                    break;
                case "sex_stat":
                    $data = User::where('type', 'user')->whereBetween('created_at', [$start, $end])
                        ->whereNotNull('state')
                        ->whereNotNull('parliament')
                        ->whereNotNull('sex')
                        ->whereNotNull('race')
                        ->whereNotNull('religion')
                        ->select('sex', DB::raw('count(*) as jumlah'))
                        ->groupBy('sex')
                        ->get();

                    return response()->json([
                        'data' => $data,
                    ]);
                    break;
                case "race_stat":
                    $data = User::where('type', 'user')->whereBetween('created_at', [$start, $end])
                        ->whereNotNull('state')
                        ->whereNotNull('parliament')
                        ->whereNotNull('sex')
                        ->whereNotNull('race')
                        ->whereNotNull('religion')
                        ->select('race', DB::raw('count(*) as jumlah'))
                        ->groupBy('race')
                        ->get();

                    return response()->json([
                        'data' => $data,
                    ]);
                    break;
                case "religon_stat":
                    $data = User::where('type', 'user')->whereBetween('created_at', [$start, $end])
                        ->whereNotNull('state')
                        ->whereNotNull('parliament')
                        ->whereNotNull('sex')
                        ->whereNotNull('race')
                        ->whereNotNull('religion')
                        ->select('religion', DB::raw('count(*) as jumlah'))
                        ->groupBy('religion')
                        ->get();

                    return response()->json([
                        'data' => $data,
                    ]);
                    break;
                case "age_stat":
                    $data = User::where('type', 'user')->whereBetween('created_at', [$start, $end])
                        ->whereNotNull('state')
                        ->whereNotNull('parliament')
                        ->whereNotNull('sex')
                        ->whereNotNull('race')
                        ->whereNotNull('religion')
                        ->whereRaw('LENGTH(ic) = 12')
                        ->get()
                        ->map(function ($user) use ($age_range) {
                            $age = date("Y") - (1900 + substr($user->ic, 0,2));
                            foreach($age_range as $key => $breakpoint)
                            {
                                if ($breakpoint >= $age) {
                                    $user->range = $key;
                                break;
                                }
                            }
                            return $user;
                        })
                        ->mapToGroups(function ($user, $key) {
                            return [$user->range => $user];
                        })
                        ->map(function($group) {
                            return count($group);
                        })
                        ->sortKeys();
                    $age_group = [];
                    $age_count = [];
                    foreach($data as $key => $item) {
                        if($key == null) {
                            $key = "18-24";
                        }
                        array_push($age_group, $key);
                        array_push($age_count, $item);
                    }
                    return response()->json([
                        'age_group' => $age_group,
                        'age_count' => $age_count,
                    ]);
                    break;
            }
        } elseif ($request->filled('type')){
            switch($request->type) {
                case "reg_stat":
                    for ($i = 0; $i < 20; ++$i) {
                        $date = date('Y-m-d', strtotime('today - '.$i.' days'));
                        $date_data[] = $date;
                        $data_online[] = User::whereRaw("DATE(created_at) = '".$date."'")-> where('type', 'user')->where('ikoop', 0)->count();
                    }
                    return response()->json([
                        'data_online' => $data_online,
                        'date' => $date_data,
                    ]);
                    break;
                case "payment_stat":
                    $channels = Payment::where('payment_desc', 'Pendaftaran Anggota Baharu Dan Langganan Modal Syer')
                        ->whereNotNull('channel')
                        ->where(function ($query) {
                            $query->where('channel', 'online')
                                ->orWhere('channel', 'manual');
                        })
                        ->groupBy('channel')
                        ->get();
                    $data = [];
                    foreach ($channels as $channel) {
                        $data[$channel->channel] = Payment::where('payment_desc', 'Pendaftaran Anggota Baharu Dan Langganan Modal Syer')
                                            ->where('channel', $channel->channel)
                                            ->count();
                    };
                    $label = [];
                    $count = [];
                    foreach ($data as $key => $item) {
                        array_push($label, strtoupper($key));
                        array_push($count, $item);
                    }
                    return response()->json([
                       'label' => $label,
                       'count' => $count,
                    ]);
                    break;
                case "state_stat":
                    $data = User::where('type', 'user')
                        ->select('state', DB::raw('count(*) as jumlah'))
                        ->whereNotNull('state')
                        ->whereNotNull('parliament')
                        ->whereNotNull('sex')
                        ->whereNotNull('race')
                        ->whereNotNull('religion')
                        ->groupBy('state')
                        ->get();
                    return response()->json([
                        'data' => $data,
                    ]);
                    break;
                case "sex_stat":
                    $data = User::where('type', 'user')
                        ->select('sex', DB::raw('count(*) as jumlah'))
                        ->whereNotNull('state')
                        ->whereNotNull('parliament')
                        ->whereNotNull('sex')
                        ->whereNotNull('race')
                        ->whereNotNull('religion')
                        ->groupBy('sex')
                        ->get();
                    return response()->json([
                        'data' => $data,
                    ]);
                    break;
                case "race_stat":
                    $data = User::where('type', 'user')
                        ->select('race', DB::raw('count(*) as jumlah'))
                        ->whereNotNull('state')
                        ->whereNotNull('parliament')
                        ->whereNotNull('sex')
                        ->whereNotNull('race')
                        ->whereNotNull('religion')
                        ->groupBy('race')
                        ->get();
                    return response()->json([
                        'data' => $data,
                    ]);
                    break;
                case "religion_stat":
                    $data = User::where('type', 'user')
                        ->select('religion', DB::raw('count(*) as jumlah'))
                        ->whereNotNull('state')
                        ->whereNotNull('parliament')
                        ->whereNotNull('sex')
                        ->whereNotNull('race')
                        ->whereNotNull('religion')
                        ->groupBy('religion')
                        ->get();
                    return response()->json([
                        'data' => $data,
                    ]);
                    break;
                case "age_stat":
                    $data = User::where('type', 'user')
                        ->whereNotNull('state')
                        ->whereNotNull('parliament')
                        ->whereNotNull('sex')
                        ->whereNotNull('race')
                        ->whereNotNull('religion')
                        ->whereRaw('LENGTH(ic) = 12')
                        ->get()
                        ->map(function ($user) use ($age_range) {
                            $age = date("Y") - (1900 + substr($user->ic, 0,2));
                            foreach($age_range as $key => $breakpoint)
                            {
                                if ($breakpoint >= $age) {
                                    $user->range = $key;
                                break;
                                }
                            }
                            return $user;
                        })
                        ->mapToGroups(function ($user, $key) {
                            return [$user->range => $user];
                        })
                        ->map(function($group) {
                            return count($group);
                        })
                        ->sortKeys();
                    $age_group = [];
                    $age_count = [];
                    foreach($data as $key => $item) {
                        if($key == null) {
                            $key = "18-24";
                        }
                        array_push($age_group, $key);
                        array_push($age_count, $item);
                    }
                    return response()->json([
                        'age_group' => $age_group,
                        'age_count' => $age_count,
                    ]);
                    break;
            }
        } else {
            for ($i = 0; $i < 20; ++$i) {
                $date = date('Y-m-d', strtotime('today - '.$i.' days'));
                $date_data[] = $date;
                $data_online[] = User::whereRaw("DATE(created_at) = '".$date."'")-> where('type', 'user')->where('registration_type', 'online')->where('ikoop', 0)->count();
                $data_manual[] = User::whereRaw("DATE(created_at) = '".$date."'")-> where('type', 'user')->where('registration_type', 'manual')->where('ikoop', 0)->count();
            }

            return response()->json([
                'data_online' => $data_online,
                'date' => $date_data,
                'data_manual' => $data_manual,
            ]);
        }
    }

    public function subfetch(Request $request) {
        if ($request->filled('month') && $request->filled('type') && $request->filled('state_select')){
            $periods = Carbon::parse($request->year . '-' . $request->month . '-01 00:00:00')->toPeriod($request->year . '-' . $request->month . '-31 23:59:59');
            $arr = $periods->toArray();
            $plength = count($arr);
            $start = $arr[0];
            $end = $arr[$plength-1];
            $data = User::where('type', 'user')->whereBetween('created_at', [$start, $end])
                ->where('state', $request->state_select)
                ->whereNotNull('parliament')
                ->select('parliament', DB::raw('count(*) as jumlah'))
                ->whereNotNull('state')
                ->whereNotNull('sex')
                ->whereNotNull('race')
                ->groupBy('parliament')
                ->get();
            
            return response()->json([
                'data' => $data,
                'test' => '1',
            ]);
        } elseif ($request->filled('type') && $request->filled('state_select')) {
            $data = User::where('type', 'user')
                ->where('state', $request->state_select)
                ->whereNotNull('parliament')
                ->whereNotNull('state')
                ->whereNotNull('sex')
                ->whereNotNull('race')
                ->select('parliament', DB::raw('count(*) as jumlah'))
                ->groupBy('parliament')
                ->get();
            
            return response()->json([
                'data' => $data,
                'test' => '2',
            ]);
        } 
        else {
            return response()->json([
                'data' => 'none',
                'test' => '3',
            ]);
        }
    }
}