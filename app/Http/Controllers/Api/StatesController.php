<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StatesController extends Controller
{
    public function index(){

        $states = DB::table('states')->pluck('id','name');

        return response()->json($states);
    }
}
