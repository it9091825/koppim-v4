<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Mail\TemporaryPasswordMail;
use App\Traits\MobileNumberTrait;
use App\Traits\SmsTrait;
use App\User;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Traits\NexmoTrait;
use Illuminate\Support\Facades\Mail;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
     */

    use ResetsPasswords, MobileNumberTrait, SmsTrait, NexmoTrait;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Display the password reset view for the given token.
     *
     * If no token is present, display the link request form.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string|null  $token
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showResetForm()
    {
        return view('auth.passwords.reset');
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function verifyIc(Request $request)
    {

        $ic = $request->ic;

        $data = User::where('ic', $ic)->where('type','user')->first();

        if($data){
            return view('auth.passwords.ic-verification')->with('user', $data);
        } else{
            return back()->withErrors(['error' => 'NO. MYKAD YANG DIMASUKKAN TIADA DALAM REKOD KAMI ATAUPUN MASIH BELUM DIKEMASKINI. SILA HUBUNGI TALIAN HOTLINE KoPPIM YANG TERTERA DI BAWAH UNTUK PERTANYAAN.<br>03-84081878']);
        }
    }

    public function verifyMobileNumber(Request $request)
    {
        $ic = $request->input('ic');

        $data = User::where('ic', $ic)->first();
    }

    public function sendTemporaryPassword(Request $request)
    {

//        $last4Digit = $request->last4Digit;
//        $ic = $request->ic;

        $email = $request->email;

        $data = User::where('email', $email)->where('type','user')->first();

        if(!$data){
//            return redirect('/dapatkan-kata-laluan')->withErrors(['error' => 'NO. MYKAD YANG DIMASUKKAN TIADA DALAM REKOD KAMI ATAUPUN MASIH BELUM DIKEMASKINI. SILA HUBUNGI TALIAN HOTLINE KoPPIM YANG TERTERA DI BAWAH UNTUK PERTANYAAN.<br>03-84081878']);
            return redirect('/dapatkan-kata-laluan')->withErrors(['error' => 'EMEL YANG DIMASUKKAN TIADA DALAM REKOD KAMI ATAUPUN MASIH BELUM DIKEMASKINI. SILA HUBUNGI TALIAN HOTLINE KoPPIM YANG TERTERA DI BAWAH UNTUK PERTANYAAN.<br>03-84081878']);
        }

        $mobile_no = $data->mobile_no;
//        $originalLast4Digit = substr($mobile_no, -4);

//        if ($originalLast4Digit == $last4Digit) {
        if ($request->email == $data->email) {
            $temporaryPassword = rand(111111, 999999);

            User::where('email', $email)->update([
                'password_reset' => 1,
                'password' => Hash::make($temporaryPassword)
            ]);

            if($data->ikoop == 1){
                $data->ikoop = 2;
                $data->save();
            }

//            $text = "Anda telah membuat permohonan untuk set semula kata laluan. Sila gunakan kata laluan sementara di bawah untuk log masuk.%0aKata Laluan Sementara%3A".$temporaryPassword."%0aSila hubungi pihak KoPPIM di 03-84081878 sekiranya anda tidak membuat permohonan set semula kata laluan.";
//
//            $this->send_sms($data->ic,$mobile_no, $text);

            $text = "Kata laluan sementara anda adalah ".$temporaryPassword.". Hubungi KoPPIM di 0384081878 sekiranya anda tidak membuat permohonan ini.";
            $this->send_sms($data->ic,$mobile_no, $text);

            Mail::to($data->email)->send(new TemporaryPasswordMail($temporaryPassword));

            return view('auth.passwords.success-send-temporary-password', compact('mobile_no', 'email'));
        } else {
            //return redirect()->back()->withErrors(['error' => '4 digit terakhir no. telefon bimbit yang dimasukkan adalah tidak tepat.']);

            return redirect('/dapatkan-kata-laluan')->withErrors([
//                'error' => '4 digit terakhir no. telefon bimbit yang dimasukkan adalah tidak tepat.'
                'error' => 'Emel yang dimasukkan tidak tepat'
            ]);
        }
    }
}
