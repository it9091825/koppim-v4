<?php

namespace App\Http\Controllers;

use Afiqiqmal\Rpclient\RaudhahPay;
use App\Mail\User\AddShare;
use App\Mail\User\NewRegistration;
use App\Traits\OrderTrait;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use App\Traits\SmsTrait;
use Illuminate\Support\Facades\Log;
use Mail;
use App\Mongo\Payment as MPayment;
use Carbon\Carbon;
use App\Traits\NexmoTrait;

class SenangPayController extends Controller
{
    use SmsTrait, NexmoTrait, OrderTrait;

    const RECURRING_MONTHLY_TYPE = 3;

    const FI_PRICE = 30;

    public function submit(Request $request)
    {
        $order_ref = $request->input('order_ref');

        $detail = "Pembayaran Kepada KoPPIM";

        $mpayment = MPayment::where('order_ref', $order_ref)->first();

        $amount = $mpayment->amount;
        $phone = ltrim($mpayment->phone, '6');
        $output = RaudhahPay::make()
            ->bill()
            ->makeBill(config('raudhahpay.collection_code'))
            ->setCustomer($mpayment->name, "-", $mpayment->email, '6' . $phone, "Address")
            ->setReference($mpayment->order_ref)
            ->setProduct($detail, $amount, 1)
            ->create()
            ->output();

        if ($output['status_code'] === 200) {
            return redirect()->to($output['body']['payment_url']);
        } else {
            Log::error($output['error']);
            abort('Payment Gateway error. Please contact administrator to report this problem.' . $output['error']);
        }
    }

    public function submitRecurring(Request $request)
    {

        $order_ref = $request->input('order_ref');

        $mpayment = MPayment::where('order_ref', $order_ref)->first();

        $url = null;

        if (!isset($mpayment->last_url)) {
            $amount = $mpayment->amount;

            if ($mpayment->first_time_fees != 0) {
                $fi_pendaftaran = RaudhahPay::make()
                    ->product()
                    ->create('Fi Pendaftaran Anggota Baharu', 'FP', 'Fi Pendaftaran Anggota Baharu', self::FI_PRICE)
                    ->output();

                $langganan = RaudhahPay::make()
                    ->product()
                    ->create('Pembayaran Kepada KoPPIM', 'LGNN', 'Pembayaran Kepada KoPPIM', $amount - self::FI_PRICE)
                    ->output();

                $raudhahPay = new \App\Http\Controllers\RaudhahPay();

                $date = Carbon::today()->toDateString();
                $data = $raudhahPay
                    ->setProduct($fi_pendaftaran, 1)
                    ->setProduct($langganan, 1, true)
                    ->setRCustomer($mpayment->name, "-", $mpayment->email, '6' . $mpayment->phone, "Address")
                    ->createRecurringBill($date, $mpayment->order_ref, 'ref2', $date, self::RECURRING_MONTHLY_TYPE, 'MYR')
                    ->getOutput();
            } else {
                $langganan = RaudhahPay::make()
                    ->product()
                    ->create('Pembayaran Kepada KoPPIM', 'LGNN', 'Pembayaran Kepada KoPPIM', $amount)
                    ->output();

                $raudhahPay = new \App\Http\Controllers\RaudhahPay();

                $date = Carbon::today()->toDateString();
                $data = $raudhahPay
                    ->setProduct($langganan, 1, true)
                    ->setRCustomer($mpayment->name, "-", $mpayment->email, '6' . $mpayment->phone, "Address")
                    ->createRecurringBill($date, $mpayment->order_ref, 'ref2', $date, self::RECURRING_MONTHLY_TYPE, 'MYR')
                    ->getOutput();
            }

            try {
                $mpayment->last_url = $data['body']['data']['payment_url'];
                $url = $mpayment->last_url;
            } catch (\Exception $exception) {
                return [
                    'error' => 'Ralat. Sila cuba semula.'
                ];
            }
        } else {
            $url = $mpayment->last_url;
        }

        $mpayment->save();

        return redirect($url) ?? ['error' => 'Masalah capaian Raudhah Pay.'];
    }

    public function callback(Request $request)
    {
        if (!RaudhahPay::make()->isCheckSumValid($request->all())) {
            Log::error($request);
            Log::error('Checksum Fail');
            abort(Response::HTTP_INTERNAL_SERVER_ERROR, 'Checksum Failed');
        }

        if (!$request->post('paid')) {
            return response('BAD', 200);
        }

        $ref_no = $request->input('ref1');
        $tx_id = $request->input('ref_id');
        $msg = 'Payment Payload';
        $txn_status = $request->input('status');
        $txn_type = 'one-time';

        $mpayment = MPayment::where('order_ref', $ref_no)->first();
        $user = ($mpayment != null) ? User::where('ic', $mpayment->ic)->first() : null;
        if ($user != null && $mpayment->last_url != null && $mpayment->status == 'payment-completed') {

            // check if recurring payment already recorded
            if (MPayment::where('gateway_tx.confirmation_no', $tx_id)->exists()) {
                return response("Payment Already Recoreded", 200);
            }

            // Recurring
            $newOrderRef = $this->create_order_ref($mpayment->ic);

            $mpayment = $mpayment->replicate();

            $mpayment->type = 'recurring';
            $mpayment->amount = (float) $request->input('amount');
            $mpayment->additional_share = (float) $request->input('amount');
            $mpayment->first_time_fees = 0.00;
            $mpayment->first_time_share = 0.00;
            $mpayment->payment_desc = 'Langganan Modal Tambahan';
            $mpayment->status = "verification";
            $mpayment->payment_date = Carbon::now();
            $txn_type = 'recurring';
            $mpayment->order_ref = $newOrderRef;

            if ($mpayment) {
                $mpayment->gateway_tx = [[
                    'order_id' => $ref_no,
                    'confirmation_no' => $tx_id,
                    'msg' => $msg,
                    'status' => $txn_status,
                    'txn_type' => $txn_type,
                    'order_ref' => $newOrderRef
                ]];
            }
        } else {
            if ($mpayment) {
                $txn_type = ($mpayment->last_url != null) ? 'recurring' : 'one-time';
                $mpayment->type = $txn_type;
                $mpayment->amount = ($mpayment->last_url != null) ? (float) $request->input('amount'): $mpayment->amount;
                $mpayment->additional_share = ($mpayment->last_url != null) ? (float) $request->input('amount'): $mpayment->additional_share;
                $mpayment->share_amount = ($mpayment->last_url != null) ? (float) $request->input('amount'): $mpayment->share_amount;
                $mpayment->push('gateway_tx', [
                    'order_id' => $ref_no,
                    'confirmation_no' => $tx_id,
                    'msg' => $msg,
                    'status' => $txn_status,
                    'txn_type' => $txn_type
                ]);
            }
        }

        if ($request->post('paid')) {

            if ($mpayment && $mpayment->status != "payment-completed") {

                $mpayment->payment_date = Carbon::now();
                $mpayment->returncode = "100";
                $mpayment->status = "payment-completed";
                $mpayment->wcID = "";
                $mpayment->ord_key = $tx_id;
                $mpayment->gateway = "raudhahpay";
                $mpayment->payment_date = Carbon::now();
                $mpayment->payment_proof = env('PAYMENT_URL') . '/payment/receipt/' . $tx_id;
                $mpayment->save();

                $user = User::where('ic', $mpayment->ic)->first();

                if ($user) {

                    $mpayment->user_id = $user->id;
                    $mpayment->save();
                    //send payment success sms for tambahan share
                    try {
                        $text = "Terima kasih. Langganan tambahan anda dengan KoPPIM telah diterima dan sedang diproses. Sila semak emel untuk maklumat lanjut. Sekian.";
                        $this->send_sms($user->ic, $user->mobile_no, $text);
                        //send email notification

                        Mail::to($user->email)->send(new AddShare(strtoupper($user->name), $mpayment->order_ref, $mpayment->created_at, $mpayment->share_amount));
                    } catch (\Exception $e) {
                        activity('exception')->log($e->getMessage());
                    }
                } else {

                    $pin = rand(111111, 999999);

                    $password = Hash::make($pin);

                    $share = $mpayment->share_amount;

                    $additionalShare = 0.00;

                    if ($share > 100) {
                        $additionalShare = $share - 100.00;
                        $share = 100.00;
                    }

                    $user = User::create([
                        'name' => strtoupper($mpayment->name),
                        'password' => $password,
                        'ic' => $mpayment->ic,
                        'email' => $mpayment->email,
                        'mobile_no' => $mpayment->phone,
                        'type' => 'user',
                        'status' => 0,
                        'password_reset' => 1,
                        'know_from' => $mpayment->know_from
                    ]);

                    $mpayment->user_id = $user->id;
                    $mpayment->save();

                    try {
                        $text = "Permohonan anda telah diterima. Log masuk akaun di anggota.koppim.com.my dengan kata laluan sementara " . $pin . " & kemaskini profil anda.";
                        $this->nexmo_send_sms($mpayment->ic, $mpayment->phone, $text);
                        //send email
                        Mail::to($mpayment->email)->send(new NewRegistration(strtoupper($mpayment->name), $mpayment->ic, $mpayment->phone, $mpayment->email, $user->created_at, $share, $additionalShare, $mpayment->amount));
                    } catch (\Exception $e) {
                        activity('exception')->log($e->getMessage());
                    }

                    //for user that use langgan-syer without having account
                    if ($mpayment->first_time_fees == 0.00) {
                        try {
                            //send payment success sms for tambahan share
                            $text = "Terima kasih. Langganan tambahan anda dengan KoPPIM telah diterima dan sedang diproses. Sila semak emel untuk maklumat lanjut. Sekian.";
                            $this->send_sms($mpayment->ic, $mpayment->phone, $text);
                            //send email notification
                            Mail::to($mpayment->email)->send(new AddShare(strtoupper($mpayment->name), $mpayment->order_ref, $mpayment->created_at, $mpayment->share_amount));
                        } catch (\Exception $e) {
                            activity('exception')->log($e->getMessage());
                        }
                    }
                }
            }
        }
        //Need to do else statement here for changing status verification to unsuccessful/cancelled

        return response('OK', 200);
    }

    public function result(Request $request)
    {

        $status_id = $request->input('status');
        $order_ref = $request->input('ref1');

        //Note: can do some payment update here if dont want to use callback
        //Note: For security purposes need to verify hash if want to do payment update here

        return redirect('/langganan-syer/terima-kasih?order_ref=' . $order_ref . '&status_id=' . $status_id);
    }

    public function thankyou(Request $request)
    {
        //Please Don't use any create or update code in this function, since if user refresh this page, the payment will get created or updated

        $status_id = 0;

        if ($request->input('status_id') == 4) {
            $status_id = 1;
        }

        $order_ref = $request->input('order_ref');

        if ($order_ref != null) {
            $mpayment = MPayment::where('order_ref', $order_ref)->first();
        } else {
            $mpayment = null;
        }

        return view('investor.buy-thank-you')->with('payment', $mpayment)->with('status_id', $status_id);
    }
}
