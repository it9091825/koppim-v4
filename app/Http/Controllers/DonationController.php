<?php

namespace App\Http\Controllers;

use App\Mail\User\AddShare;
use App\Traits\SmsTrait;
use Illuminate\Http\Request;
use App\Payment;
use App\User;
use Illuminate\Support\Facades\Hash;
use App\payment_gateway_tx;
use Mail;
use Log;
use App\Mail\User\NewRegistration;

class DonationController extends Controller
{
    use SmsTrait;

    public function index(Request $request)
    {
        return view('index-2');
    }

    public function index2(Request $request)
    {
        return view('index-2');

    }


    public function index3(Request $request)
    {
        return view('index-3-senang-pay');

    }

    public function index4(Request $request)
    {
        return view('index-3-senang-pay-recurrance');

    }



//	public function index_with_course(Request $request)
//    {
//
//	    $courses = Course::where('status',1)->where('status_approval',1)->get();
//
//	    return view('index-4')->with('courses',$courses);
//
//	}


//	public function course_detail($slug)
//    {
//
//	    $detail = Course::where('course_slug',$slug)->first();
//
//	    if($detail)
//	    {
//		    $courses = Course::where('status',1)->get();
//
//			$clientIP = \Request::getClientIp(true);
//			course_visit::create([
//				'course_id'=>$detail->id,
//				'ip_address'=>$clientIP
//			]);
//
//			return view('campaign-detail')->with('courses',$courses)->with('detail',$detail);
//	    }
//	    else
//	    {
//		    return redirect('/');
//	    }
//
//
//	}


//	public function course_detail_2($slug)
//    {
//
//	    $detail = Course::where('course_slug',$slug)->first();
//
//	    if($detail)
//	    {
//		    $courses = Course::where('status',1)->get();
//			$clientIP = \Request::getClientIp(true);
//			course_visit::create([
//				'course_id'=>$detail->id,
//				'ip_address'=>$clientIP
//			]);
//
//			return view('campaign-detail-2')->with('courses',$courses)->with('detail',$detail);
//	    }
//	    else
//	    {
//		    return redirect('/');
//	    }
//
//
//	}


//	public function course_detail_desc($slug)
//    {
//
//	    $detail = Course::where('course_slug',$slug)->first();
//
//
//	    $courses = Course::where('status',1)->get();
//
//	    return view('campaign-detail-desc')->with('courses',$courses)->with('detail',$detail);
//
//	}


//	public function course_detail_ebook($slug)
//    {
//
//
//	    return view('campaign-ebook');
//
//	}


    public function create(Request $request)
    {

        //dd($request);
        $donation = Payment::create(['name' => strtoupper($request->name), 'ic' => $request->ic, 'phone' => $request->phone, 'email' => $request->email, 'amount' => $request->amount, 'status' => 'verification', 'type' => $request->input('type'), 'gateway' => 'senangpay']);
        $don = Payment::findOrFail($donation->id);
        $don->order_ref = time() . '-' . $don->id . '-' . date("y-m-d");
        $don->save();

        $request->session()->put('donation', $don->id);
        return redirect()->route('verify');

    }


    public function create_2_senang_pay(Request $request)
    {

        //dd($request);
        $donation = Payment::create(['name' => strtoupper($request->name), 'ic' => $request->ic, 'phone' => $request->phone, 'email' => $request->email, 'amount' => $request->amount, 'status' => 'verification', 'type' => $request->input('type'), 'gateway' => 'senangpay', 'campaign_id' => $request->campaign_id]);
        $don = Payment::findOrFail($donation->id);
        $don->order_ref = time() . '-' . $don->id . '-' . date("y-m-d");
        $don->save();

        $request->session()->put('donation', $don->id);
        return redirect('/verify-senang-pay?id=' . $don->id . '');

    }

    public function verify(Request $request)
    {
        if ($request->session()->has('donation')) {
            $donationId = $request->session()->get('donation');

            $donation = Payment::findOrFail($donationId);

            if (env('KIPLE_TEST_PRICE', false) == true) {
                $price = '1.00';
            } else {
                $price = $donation->amount;
            }

            $url = env('KIPLE_URL', 'https://kiplepay.com/wcgatewayinit.php');
            $merchant = env('KIPLE_MERCHANT', '80000749');
            $secret = env('KIPLE_SECRET', '1JYS9SD020N');

            $returnUrl = secure_url("/return/{$donation->order_ref}");

            $amount = str_replace('.', '', $price);
            $amount = str_replace(',', '', $amount);
            $hashValue = sha1($secret . $merchant . $donation->order_ref . $amount);

            return view('verify')
                ->with('donation', $donation)
                ->with('amount', $price)
                ->with('hashValue', $hashValue)
                ->with('merchant', $merchant)
                ->with('url', $url)
                ->with('returnUrl', $returnUrl);
        }

        return redirect()->route('index');
    }


    public function verify_senang_pay(Request $request)
    {
        if ($request->session()->has('donation')) {
            $donationId = $request->session()->get('donation');

            $donation = Payment::findOrFail($donationId);
            $secret = "22533-326618554";
            $merchant = "353159797940278";
            $recurring_id = [
                '10.00' => 158553672531,
                '20.00' => 158554620949,
                '25.00' => 158553677787,
                '50.00' => 158553685392,
                '100.00' => 158553728279,
                '200.00' => 158553733412,
                '500.00' => 158553879374,
                '1000.00' => 158553666976,
            ];

            if (env('KIPLE_TEST_PRICE', false) == true) {
                $price = '1.00';
            } else {
                $price = $donation->amount;
            }

            $url = "https://app.senangpay.my/payment/" . $merchant . "";
            $amount = $price;
            if ($donation->type == "recurring") {
                $url = "https://app.senangpay.my/payment/" . $recurring_id['' . $amount . ''] . "";

                $hashValue = hash('sha256', $secret . urldecode($recurring_id['' . $amount . '']) . urldecode($donation->order_ref));

            } else {
                $url = "https://app.senangpay.my/payment/" . $merchant . "";
                $hashValue = md5($secret . urldecode("Sumbangan") . urldecode($amount) . urldecode($donation->order_ref));
            }


            $returnUrl = secure_url("/return/{$donation->order_ref}");


            // $hashValue = sha1($secret . $merchant . $donation->order_ref . $amount);


            return view('verify-senang-pay')
                ->with('donation', $donation)
                ->with('amount', $price)
                ->with('hashValue', $hashValue)
                ->with('merchant', $merchant)
                ->with('url', $url)
                ->with('returnUrl', $returnUrl)
                ->with('recurring_id', $recurring_id);
        }

        return redirect()->route('index');
    }


    public function kiplepay(Request $request, $order_ref)
    {
        $donation = Payment::where('order_ref', $request->ord_mercref)->first();

        if ($donation) {
            if ($request->returncode == "E2") {
                //Abort Transaction
                $donation->returncode = $request->returncode;
                $donation->status = "Payment Aborted";
                $donation->wcID = $request->wcID;
                $donation->ord_key = $request->ord_key;
                $donation->save();

                $request->session()->forget('donation');
                return redirect()->route('index');

            } elseif ($request->returncode == "E1") {
                //Fail Transaction
                $donation->returncode = $request->returncode;
                $donation->status = "Payment Failed";
                $donation->wcID = $request->wcID;
                $donation->ord_key = $request->ord_key;
                $donation->save();

                $request->session()->forget('donation');
                return redirect()->route('index');

            } elseif ($request->returncode == "100") {

                $merchant = env('KIPLE_MERCHANT', '80000749');
                $secret = env('KIPLE_SECRET', '1JYS9SD020N');

                $amount = str_replace('.', '', $donation->amount);
                $amount = str_replace(',', '', $amount);
                $hashValue = sha1($secret . $merchant . $donation->order_ref . $amount . "100");

                if ($hashValue == $request->ord_key) {

                    $donation->returncode = $request->returncode;
                    $donation->status = "Payment Successful";
                    $donation->wcID = $request->wcID;
                    $donation->ord_key = $request->ord_key;
                    $donation->save();


                } else {

                    $donation->returncode = $request->returncode;
                    $donation->status = "Payment Successful, Hash Key Invalid";
                    $donation->wcID = $request->wcID;
                    $donation->ord_key = $request->ord_key;
                    $donation->save();

                }

                $data = [
                    'no-reply' => 'sumbangan@ppim.org.my',
                    'reply-to' => 'sumbangan@ppim.org.my',
                    'to' => $donation->email,
                    'to_name' => $donation->name,
                    'name' => $donation->name,
                    'created_at' => $donation->created_at,
                    'value' => $donation->amount,
                    'ref' => $donation->order_ref
                ];

                Mail::send('email', ['data' => $data],
                    function ($message) use ($data) {
                        $message->from($data['no-reply'], 'PPIM')->replyTo($data['reply-to'], 'PPIM')->to($data['to'], $data['to_name'])->subject('Terima Kasih Dari PPIM');
                    });

                return redirect()->route('paymentStatus');
            }
        }

        return redirect()->route('index');
    }


    public function senangpayccallback(Request $request)
    {

        $ref_no = $request->input('order_id');
        $tx_id = $request->input('transaction_id');

        $donation = Payment::where('order_ref', $ref_no)->first();

        if ($donation) {
            if ($request->input('status_id') == 1) {

                if ($request->session()->has('donation')) {

                    $donation->returncode = "100";
                    $donation->status = "payment-completed";
                    $donation->wcID = "";
                    $donation->ord_key = $tx_id;
                    $donation->gateway = "senangpay";
                    $donation->save();

                    /*
                    $data = [
                'no-reply' => 'sumbangan@ppim.org.my',
                'reply-to' => 'sumbangan@ppim.org.my',
                'to' => $donation->email,
                'to_name' => $donation->name,
                'name' => $donation->name,
                'created_at' => $donation->created_at,
                'value' => $donation->amount,
                'ref' => $donation->order_ref
            ];

             Mail::send('email', ['data' => $data],
                function ($message) use ($data)
                {
                    $message->from($data['no-reply'],'PPIM')->replyTo($data['reply-to'], 'PPIM')->to($data['to'], $data['to_name'])->subject('Terima Kasih Dari PPIM');
                });
                    */

                    $donationId = $request->session()->get('donation');
                    $donation = Payment::findOrFail($donationId);

                    $request->session()->forget('donation');

                    return view('status-2')->with('donation', $donation);

                } else {

                    return redirect()->route('index');
                }

            } else {

                //Abort Transaction


                if ($request->session()->has('donation')) {

                    $donation->returncode = "E1";
                    $donation->status = "Payment Failed";
                    $donation->wcID = "";
                    $donation->ord_key = $tx_id;
                    $donation->gateway = "senangpay";
                    $donation->save();

                    $donationId = $request->session()->get('donation');
                    $donation = Payment::findOrFail($donationId);

                    $request->session()->forget('donation');

                    return view('status-2-cancel')->with('donation', $donation);
                } else {

                    return redirect()->route('index');
                }


            }


        }


    }


    public function senang_pay_call_backend(Request $request)
    {
        $ref_no = $request->input('order_id');
        $tx_id = $request->input('transaction_id');
        $msg = $request->input('msg');
        $txn_status = $request->input('status_id');
        $txn_type = 'one-time';

        payment_gateway_tx::create([
            'order_id' => $ref_no,
            'confirmation_no' => $tx_id,
            'msg' => $msg,
            'status' => $txn_status,
            'txn_type' => $txn_type
        ]);


        if ($txn_status == 1) {
            $donation = Payment::where('order_ref', $ref_no)->first();
            if($donation && $donation->status != "payment-completed") {
                $donation->returncode = "100";
                //$donation->status = "payment-completed";
                $donation->wcID = "";
                $donation->ord_key = $tx_id;
                $donation->gateway = "senangpay";
                $donation->save();

                $user = User::where('ic', $donation->ic)->first();

                if ($user) {

                    Payment::where('id', $donation->id)->update([
                        'status' => 'payment-completed',
                        'ord_key' => $tx_id,
                        'gateway' => 'senangpay',
                    ]);
                    $payment = Payment::findOrFail($donation->id);

                    //send payment success sms for tambahan share
                    $text = "Terima kasih. Langganan tambahan anda dengan KoPPIM telah diterima dan sedang diproses. Sila semak emel untuk maklumat lanjut. Sekian.";
                    $this->send_sms($user->mobile_no, $text);
                    //send email notification
                    Mail::to($user->email)->send(new AddShare(strtoupper($user->name),$payment->order_ref,$payment->created_at,$payment->share_amount));

                } else {

                    $pin = rand(111111, 999999);

                    $password = Hash::make($pin);

                    Payment::where('id', $donation->id)->update([
                        'status' => 'payment-completed',
                        'ord_key' => $tx_id,
                        'gateway' => 'senangpay',
                    ]);

                    $payment = Payment::findOrFail($donation->id);

                    $share = $payment->share_amount;

                    $additionalShare = 0.00;

                    if($share > 100){
                        $additionalShare =  $share - 100.00;
                        $share = 100.00;
                    }


                    $user = User::create([
                        'name' => strtoupper($donation->name),
                        'password' => $password,
                        'ic' => $donation->ic,
                        'email' => $donation->email,
                        'mobile_no' => $donation->phone,
                        'type' => 'user',
                        'status' => 0,
                        'password_reset' => 1,
                    ]);



                    // Kata laluan sementara: 169226
                    //send temprory password
                    $text = "Kata laluan sementara anda adalah ". $pin.". Sila log masuk ke akaun anda di https://anggota.koppim.com.my dan kemaskini maklumat peribadi.";
                    $this->send_sms($donation->phone, $text);

                    //send email
                    Mail::to($donation->email)->send(new NewRegistration(strtoupper($donation->name),$donation->ic,$donation->phone,$donation->email,$user->created_at,$share,$additionalShare,$payment->amount));

                }
            }
            /*
            $data = [
        'no-reply' => 'sumbangan@ppim.org.my',
        'reply-to' => 'sumbangan@ppim.org.my',
        'to' => $donation->email,
        'to_name' => $donation->name,
        'name' => $donation->name,
        'created_at' => $donation->created_at,
        'value' => $donation->amount,
        'ref' => $donation->order_ref
    ];

     Mail::send('email', ['data' => $data],
        function ($message) use ($data)
        {
            $message->from($data['no-reply'],'PPIM')->replyTo($data['reply-to'], 'PPIM')->to($data['to'], $data['to_name'])->subject('Terima Kasih Dari PPIM');
        });
        */

        }

        return 'OK';
    }


    function send_sms($mobile_no, $text)
    {
        $cURLConnection = curl_init();

        curl_setopt($cURLConnection, CURLOPT_URL, 'https://www.klasiksms.com/api/sendsms.php?email=zharifjohor@gmail.com&key=60d7535c39dd181f021008afc249d4c3&recipient=' . $mobile_no . '&message=KoPPIM%20:%20' . $text . '&referenceID=koppim283');
        curl_setopt($cURLConnection, CURLOPT_RETURNTRANSFER, true);

        $phoneList = curl_exec($cURLConnection);
        curl_close($cURLConnection);
    }

    public function senang_pay_call_recurring_backend(Request $request)
    {
        $ref_no = $request->input('order_id');
        $tx_id = $request->input('transaction_id');
        $msg = $request->input('msg');

        $amount = $request->input('amount_paid');

        $txn_status = $request->input('status_id');
        $txn_type = 'recurring';

        payment_gateway_tx::create([
            'order_id' => $ref_no,
            'confirmation_no' => $tx_id,
            'msg' => $msg,
            'amount' => $amount,
            'status' => $txn_status,
            'txn_type' => $txn_type
        ]);

        if ($txn_status == 1) {
            $donation = Payment::where('order_ref', $ref_no)->first();

            $donation->returncode = "100";
            $donation->status = "Payment Successful";
            $donation->wcID = "";
            $donation->ord_key = $tx_id;
            $donation->gateway = "senangpay";
            $donation->save();


            $data = [
                'no-reply' => 'sumbangan@ppim.org.my',
                'reply-to' => 'sumbangan@ppim.org.my',
                'to' => $donation->email,
                'to_name' => $donation->name,
                'name' => $donation->name,
                'created_at' => $donation->created_at,
                'value' => $donation->amount,
                'ref' => $donation->order_ref
            ];

            Mail::send('email', ['data' => $data],
                function ($message) use ($data) {
                    $message->from($data['no-reply'], 'PPIM')->replyTo($data['reply-to'], 'PPIM')->to($data['to'], $data['to_name'])->subject('Terima Kasih Dari PPIM');
                });

        }

    }


    public function senang_pay_return(Request $request)
    {

        return view('senang-pay-return');
    }


    public function paymentStatusSenangPay(Request $request)
    {
        if ($request->session()->has('donation')) {
            $donationId = $request->session()->get('donation');
            $donation = Payment::findOrFail($donationId);

            $request->session()->forget('donation');

            return view('status-2')->with('donation', $donation);
        }

        return redirect()->route('index');

    }


    public function paymentStatus(Request $request)
    {
        if ($request->session()->has('donation')) {
            $donationId = $request->session()->get('donation');
            $donation = Payment::findOrFail($donationId);

            $request->session()->forget('donation');

            return view('status-2')->with('donation', $donation);
        }

        return redirect()->route('index');

    }


    public function paymentStatus2(Request $request)
    {

        $donation = Payment::findOrFail(1017);

        $request->session()->forget('donation');

        return view('status-2')->with('donation', $donation);


        //return redirect()->route('index');

    }


    public function emailtest(Request $request)
    {

        $data = [
            'no-reply' => 'sumbangan@ppim.org.my',
            'reply-to' => 'sumbangan@ppim.org.my',
            'to' => 'zharifjohor@gmail.com',
            'to_name' => 'Zharif johor',
            'name' => 'Zharif johor',
            'created_at' => '10 OGOS 2019',
            'value' => '50.00',
            'ref' => '6c99e5728e348fc8962fbef5d678833680bdc738'
        ];

        Mail::send('email', ['data' => $data],
            function ($message) use ($data) {
                $message->from($data['no-reply'], 'PPIM')->replyTo($data['reply-to'], 'PPIM')->to($data['to'], $data['to_name'])->subject('Terima Kasih Dari PPIM');
            });


        return view('email');

    }


    public function verify_test(Request $request)
    {

        $donationId = $request->session()->get('donation');

        $donation = Payment::findOrFail(1017);

        if (env('KIPLE_TEST_PRICE', false) == true) {
            $price = '1.00';
        } else {
            $price = $donation->amount;
        }

        $url = env('KIPLE_URL', 'https://kiplepay.com/wcgatewayinit.php');
        $merchant = env('KIPLE_MERCHANT', '80000749');
        $secret = env('KIPLE_SECRET', '1JYS9SD020N');

        $returnUrl = secure_url("/return/{$donation->order_ref}");

        $amount = str_replace('.', '', $price);
        $amount = str_replace(',', '', $amount);
        $hashValue = sha1($secret . $merchant . $donation->order_ref . $amount);

        return view('verify')
            ->with('donation', $donation)
            ->with('amount', $price)
            ->with('hashValue', $hashValue)
            ->with('merchant', $merchant)
            ->with('url', $url)
            ->with('returnUrl', $returnUrl);

    }


    public function summary_payment(Request $request)
    {
        $donations = Payment::where('returncode', 100)->orderBy('id', 'DESC')->paginate(2000);
        $sum = Payment::where('returncode', 100)->sum('amount');

        return view('summary-payment')->with('donations', $donations)->with('sum', $sum);
    }


}
