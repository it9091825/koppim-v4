<?php

namespace App\Http\Controllers\Investor;

use App\AffiliateSetting;
use App\Http\Controllers\Controller;
use App\Payment;
use App\Traits\SmsTrait;
use App\User;
use Auth;
use Carbon\Carbon;
use Hashids\Hashids;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Mail;
use Spatie\Activitylog\Models\Activity;
use Storage;
use Validator;
use App\Mongo\Payment as MPayment;
use App\Mongo\AffiliateLinkVisitor as AffiliateLinkVisitor;
use App\Mongo\CommissionWithdraw as CommissionWithdraw;
use PDF;

class AffiliatesController extends Controller
{
    use SmsTrait;

    public function __construct()
    {

        $this->middleware('auth');
        // $this->middleware('resetpassword');
        $this->middleware('firsttime');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */


    public function current_percentage()
    {
        return 0.01;
    }

    public function get_user_balance($introducer_no_anggota)
    {

        //dekat helper pun ada
        //Dah tak pakai sebab flow dah lain
        //        $jumlah_commision = MPayment::where('introducer_no_anggota', (string)$introducer_no_anggota)->where('returncode', '100')->where('introducer', true)->sum('introducer_commission');
        //        $withdraws = CommissionWithdraw::where('user_id', Auth::user()->id)->whereIn('status', ['MENUNGGU KELULUSAN', 'LULUS', 'SEMAKAN'])->sum('amount');
        //        $balance = $jumlah_commision - $withdraws;

        $balance = MPayment::where('introducer_no_anggota', (string)$introducer_no_anggota)
            ->where('returncode', '100')
            ->where('introducer', true)
            ->where('introducer_commission_status', 'available')
            ->sum('introducer_commission');

        return $balance;
    }

    public function invest(Request $request)
    {
        $current_month = Carbon::now();

        $month = $request->input('month');
        $year = $request->input('year');

        if ($month == NULL || $year == NULL) {

            $start = Carbon::parse('' . $current_month->format('Y') . '-' . $current_month->format('m') . '-01 00:00:00');
            $end = Carbon::parse('' . $current_month->format('Y') . '-' . $current_month->format('m') . '-31 23:59:59');

            $transaksi_berjaya = MPayment::where('introducer_no_anggota', (string)Auth::user()->no_koppim)
                ->where('returncode', '100')
                ->where('introducer', true)
                ->whereBetween('created_at', [$start, $end])
                ->sortable()
                ->paginate(15);

            $jumlah_commission_month = MPayment::where('introducer_no_anggota', (string)Auth::user()->no_koppim)
                ->where('returncode', '100')->where('introducer', true)
                ->whereBetween('created_at', [$start, $end])->sum('introducer_commission');

            $jumlah_pautan_unik_month = AffiliateLinkVisitor::where('no_anggota', (string)Auth::user()->no_koppim)
                ->whereBetween('created_at', [$start, $end])->count('ip'); //->groupBy('ip')

            $transaksi_berjaya_count_month = MPayment::where('introducer_no_anggota', (string)Auth::user()->no_koppim)
                ->where('returncode', '100')->where('introducer', true)->whereBetween('created_at', [$start, $end])->count();
        }elseif($month == 'all'){

            $transaksi_berjaya = MPayment::where('introducer_no_anggota', (string)Auth::user()->no_koppim)
                ->sortable()
                ->where('returncode', '100')
                ->where('introducer', true)
                ->paginate(15);

            $jumlah_commission_month = MPayment::where('introducer_no_anggota', (string)Auth::user()->no_koppim)
                ->where('returncode', '100')->where('introducer', true)
                ->sum('introducer_commission');

            $jumlah_pautan_unik_month = AffiliateLinkVisitor::where('no_anggota', (string)Auth::user()->no_koppim)
                ->count('ip'); //->groupBy('ip')

            $transaksi_berjaya_count_month = MPayment::where('introducer_no_anggota', (string)Auth::user()->no_koppim)
                ->where('returncode', '100')->where('introducer', true)->count();

        }else {
            $start = Carbon::parse('' . $year . '-' . $month . '-01 00:00:00');
            $end = Carbon::parse('' . $year . '-' . $month . '-31 23:59:59');

            $current_month = Carbon::parse('' . $year . '-' . $month . '-01 00:00:00');

            $transaksi_berjaya = MPayment::where('introducer_no_anggota', (string)Auth::user()->no_koppim)
                ->where('returncode', '100')
                ->where('introducer', true)
                ->whereBetween('created_at', [$start, $end])
                ->sortable()
                ->paginate(15);

            $jumlah_commission_month = MPayment::where('introducer_no_anggota', (string)Auth::user()->no_koppim)
                ->where('returncode', '100')->where('introducer', true)
                ->whereBetween('created_at', [$start, $end])->sum('introducer_commission');

            $jumlah_pautan_unik_month = AffiliateLinkVisitor::where('no_anggota', (string)Auth::user()->no_koppim)
                ->whereBetween('created_at', [$start, $end])->count('ip'); //->groupBy('ip')

            $transaksi_berjaya_count_month = MPayment::where('introducer_no_anggota', (string)Auth::user()->no_koppim)
                ->where('returncode', '100')->where('introducer', true)->whereBetween('created_at', [$start, $end])->count();
        }


        $jumlah_pautan_unik = AffiliateLinkVisitor::where('no_anggota', (string)Auth::user()->no_koppim)
            ->count('ip'); //->groupBy('ip')


        $transaksi_berjaya_count = MPayment::where('introducer_no_anggota', (string)Auth::user()->no_koppim)
            ->where('returncode', '100')->where('introducer', true)->count();

        $jumlah_commission = MPayment::where('introducer_no_anggota', (string)Auth::user()->no_koppim)
            ->where('returncode', '100')->where('introducer', true)->sum('introducer_commission');

        $balance = $this->get_user_balance(Auth::user()->no_koppim);

        $processing = CommissionWithdraw::where('user_id', Auth::user()->id)
            ->where('status', 'processing')->sum('amount');

        $withdraws = CommissionWithdraw::where('user_id', Auth::user()->id)
            ->where('status', 'paid')->sum('amount');


        return view('investor.affiliates')->with('jumlah_pautan_unik', $jumlah_pautan_unik)
            ->with('transaksi', $transaksi_berjaya)
            ->with('commission', $jumlah_commission)
            ->with('transaksi_count', $transaksi_berjaya_count)
            ->with('current_date', $current_month)
            ->with('jumlah_commision_month', $jumlah_commission_month)
            ->with('jumlah_pautan_unik_month', $jumlah_pautan_unik_month)
            ->with('transaksi_berjaya_count_month', $transaksi_berjaya_count_month)
            ->with('balance', $balance)
            ->with('processing',$processing)
            ->with('withdraw', $withdraws);
    }

    public function getStatement(Request $request)
    {
        $current_month = Carbon::now();
        $year = $request->input('year');
        $commission_query = CommissionWithdraw::where('user_id', auth()->user()->id);

        if ($request->filled('year')) {

            $start = Carbon::parse($year.'-01-01 00:00:00');
            $end = Carbon::parse($year . '-12-31 23:59:59');

            $list = $commission_query->whereBetween('created_at', [$start, $end])->get();
        } else {

            $start = Carbon::parse($current_month->format('Y').'-01-01 00:00:00');

            $end = Carbon::parse($current_month->format('Y').'-12-31 23:59:59');

            $list = $commission_query->whereBetween('created_at', [$start, $end])->get();
        }

        return view('affiliates.statement', compact('list'));
    }

    public function printStatement($id)
    {
        //Not secure, user can type anything for the url id and get other user statement
//        $statement = CommissionWithdraw::findOrFail($id);

        $statement = CommissionWithdraw::where('_id',$id)->where('user_id',Auth::user()->id)->first();

        $data = [
            'statement' => $statement
        ];

       $pdf = PDF::loadView('affiliates.print-statement',$data);


//        $store = Storage::disk('do_spaces')->put(env('DO_SPACES_UPLOAD') . 'uploads/commissionstatement/'.$statement->reference_no.'.pdf', $pdf->output(), 'public');

        return $pdf->download('penyata_komisen.pdf');

    }

    public function withdrawal(Request $request)
    {
        $balance = $this->get_user_balance(Auth::user()->no_koppim);

        MPayment::where('introducer_no_anggota', (string)Auth::user()->no_koppim)
            ->where('returncode', '100')
            ->where('introducer', true)
            ->where('introducer_commission_status', 'available')
            ->update(['introducer_commission_status' => 'apply']);

        $newBalance = MPayment::where('introducer_no_anggota', (string)Auth::user()->no_koppim)
            ->where('returncode', '100')
            ->where('introducer', true)
            ->where('introducer_commission_status', 'apply')
            ->sum('introducer_commission');

        if ($balance && $balance > 0.00 && $balance == $newBalance) {
            $hashids = new Hashids('withdrawal', 0, 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789');
            $ids = round(microtime(true) * 1000);
            $reference_no = $hashids->encode((integer)$ids);

            $withdraw = CommissionWithdraw::create([
                'no_anggota' => (string)Auth::user()->no_koppim,
                'name' => Auth::user()->name,
                'user_id' => (int)Auth::user()->id,
                'ic' => (string)Auth::user()->ic,
                'amount' => (float)$balance,
                'bank' => Auth::user()->bank_name,
                'account_name' => Auth::user()->bank_account_name,
                'account_no' => Auth::user()->bank_account,
                'status' => 'processing',
                'reference_no' => $reference_no
            ]);

            MPayment::where('introducer_no_anggota', (string)Auth::user()->no_koppim)
                ->where('returncode', '100')
                ->where('introducer', true)
                ->where('introducer_commission_status', 'apply')
                ->update(['introducer_commission_status' => 'processing', 'affiliate_commission_withdraw_id' => $withdraw->id]);


            return redirect('/affiliates/penyata-komisen')->with('success', 'PERMINTAAN ANDA SEDANG DIPROSES');
        }
            return redirect('/affiliates')->with('success', 'BAKI ANDA TIDAK MENCUKUPI');
        //        } else {
        //            //insufient fund
        //            return redirect('/affiliates/withdraw')->with('error', 'TELAH MELEBIHI HAD PENGELUARAN');
        //        }
    }

    //    public function affliate_withdraw(Request $request)
    //    {
    //        $jumlah_commision = $this->get_user_balance(Auth::user()->no_koppim);
    //
    //        $withdraws = CommissionWithdraw::where('user_id', Auth::user()->id)->get();
    //
    //
    //        return view('investor.affiliates-in-construction ');
    //        // return view('investor.affiliates-withdraw')->with('commisyen',$jumlah_commision)->with('withdraws',$withdraws);
    //
    //
    //    }
    //
    //
    //    public function affliate_withdraw_exe(Request $request)
    //    {
    //
    //        $balance = $this->get_user_balance(Auth::user()->no_koppim);
    //        $type = $request->input('withdraw_type');
    //        $withdraw = $request->input('jumlah_pengeluaran');
    //
    //        if ($balance >= $withdraw && $withdraw >= 5) {
    //            CommissionWithdraw::create([
    //                'no_anggota' => Auth::user()->no_koppim,
    //                'name' => Auth::user()->name,
    //                'user_id' => (int)Auth::user()->id,
    //                'ic' => Auth::user()->ic,
    //                'type' => $type,
    //                'amount' => (float)$withdraw,
    //                'bank' => Auth::user()->bank_name,
    //                'account_name' => Auth::user()->bank_account_name,
    //                'account_no' => Auth::user()->bank_account,
    //                'status' => 'MENUNGGU KELULUSAN'
    //            ]);
    //
    //
    //            return redirect('/affiliates/withdraw')->with('success', 'PERMINTAAN ANDA SEDANG DIPROSES');
    //
    //        } else {
    //            //insufient fund
    //            return redirect('/affiliates/withdraw')->with('error', 'TELAH MELEBIHI HAD PENGELUARAN');
    //        }
    //
    //    }





//    public function admin_transaction_lulus(Request $request)
//    {
//
//        $transaksi_berjaya = MPayment::where('returncode', '100')->where('introducer', true)->where('introducer_status', '=', 'LULUS')->get();
//
//        $current_percentage = $this->current_percentage();
//
//
//        return view('affiliates.transaction')->with('transactions', $transaksi_berjaya)->with('current_percentage', $current_percentage)->with('pageTitle', 'Telah Diluluskan');
//    }
//
//    public function admin_transaction_semakan(Request $request)
//    {
//
//        $transaksi_berjaya = MPayment::where('returncode', '100')->where('introducer', true)->where('introducer_status', '=', 'PERLU DISEMAK')->get();
//
//        $current_percentage = $this->current_percentage();
//
//
//        return view('affiliates.transaction')->with('transactions', $transaksi_berjaya)->with('current_percentage', $current_percentage)->with('pageTitle', 'Perlukan Semakan');
//    }
//
//
//    public function admin_transaction_batal(Request $request)
//    {
//
//        $transaksi_berjaya = MPayment::where('returncode', '100')->where('introducer', true)->where('introducer_status', '=', 'BATAL')->get();
//
//        $current_percentage = $this->current_percentage();
//
//
//        return view('affiliates.transaction')->with('transactions', $transaksi_berjaya)->with('current_percentage', $current_percentage)->with('pageTitle', 'batal');
//    }

    //Not used anymore since process flow changed
//    public function admin_approve_transaction(Request $request)
//    {
//        //Get global setting for percentage commission
//        $affSetting = AffiliateSetting::first();
//        $reg_percent = $affSetting->registration_percent;
//        $modal_percent = $affSetting->modal_percent;
//        $share_percent = $affSetting->share_percent;
//
//        $tx = $request->input('tx');
//        $status = $request->input('status');
//        $payment = MPayment::where('order_ref', $tx)->first();
//
//        //check if introducer have specific setting for commission then use that setting
//        $introducer = User::where('no_koppim', (int)$payment->introducer_no_anggota)->first();
//        if ($introducer->affiliate_setting()->exists()) {
//            $reg_percent = $introducer->affiliate_setting->registration_percent;
//            $modal_percent = $introducer->affiliate_setting->modal_percent;
//            $share_percent = $introducer->affiliate_setting->share_percent;
//        }
//
//        $reg_commission = (float)$payment->first_time_fees * ($reg_percent / 100);
//        $modal_commission = (float)$payment->first_time_share * ($modal_percent / 100);
//        $share_commission = (float)$payment->additional_share * ($share_percent / 100);
//
//        $amountkomisyen = round((float)$reg_commission, 2) + round((float)$modal_commission, 2) + round((float)$share_commission, 2);
//
//        MPayment::where('order_ref', $tx)->update([
//            'introducer_status' => $status,
//            'introducer_status_change_by' => Auth::user()->id,
//            'introducer_status_change_datetime' => new \MongoDB\BSON\UTCDateTime(new \DateTime('now')),
//            'introducer_commission' => (float)$amountkomisyen,
//            'introducer_commission_detail' => [
//                'reg_commission' => (float)$reg_commission,
//                'reg_percent' => (float)$reg_percent,
//                'modal_commission' => (float)$modal_commission,
//                'modal_percent' => (float)$modal_percent,
//                'share_commission' => (float)$share_commission,
//                'share_percent' => (float)$share_percent,
//            ],
//        ]);
//
//        return back();
//    }
//



//    public function admin_transaction_pengeluaran(Request $request)
//    {
//        $withdraws = CommissionWithdraw::where('status', 'MENUNGGU KELULUSAN')->get();
//
//
//        return view('affiliates.pengeluaran')->with('withdraws', $withdraws)->with('pageTitle', 'Pengeluaran Untuk Diluluskan');
//    }

//    public function admin_transaction_pengeluaran_detail(Request $request)
//    {
//        $id = $request->input('id');
//
//        $withdraw = CommissionWithdraw::where('_id', $id)->first();
//
//
//        return view('affiliates.pengeluaran-detail')->with('withdraw', $withdraw);
//    }


//    public function admin_transaction_pengeluaran_detail_exe(Request $request)
//    {
//        $id = $request->input('id');
//        $ref = $request->input('transaction_ref');
//
//        if ($request->hasFile('tx_receipt') == true) {
//
//            $store = Storage::disk('do_spaces')->putFile(env('DO_SPACES_UPLOAD') . 'uploads/withdrawreceipt', request()->tx_receipt, 'public');
//
//
//            $withdraw = CommissionWithdraw::where('_id', $id)->update([
//                'tx_receipt' => $store
//            ]);
//        }
//
//
//        $withdraw = CommissionWithdraw::where('_id', $id)->update([
//            'ref_no' => $ref,
//            'status' => "LULUS",
//            'by' => Auth::user()->id,
//            'datetime' => new \MongoDB\BSON\UTCDateTime(new \DateTime('now')),
//        ]);
//
//
//        return redirect('/admin/affiliates/transaksi/pengeluaran');
//    }

//
//    public function admin_transaction_pengeluaran_refund_exe(Request $request)
//    {
//        $id = $request->input('id');
//        $justifikasi_refund = $request->input('justifikasi_refund');
//
//
//        $withdraw = CommissionWithdraw::where('_id', $id)->update([
//            'justification' => $justifikasi_refund,
//            'status' => "REFUND",
//            'by' => Auth::user()->id,
//            'datetime' => new \MongoDB\BSON\UTCDateTime(new \DateTime('now')),
//        ]);
//
//
//        return redirect('/admin/affiliates/transaksi/pengeluaran');
//    }
//
//
//    public function admin_transaction_pengeluaran_semak_exe(Request $request)
//    {
//        $id = $request->input('id');
//        $justifikasi_refund = $request->input('justifikasi_refund');
//
//        $withdraw = CommissionWithdraw::where('_id', $id)->update([
//            'justification' => $justifikasi_refund,
//            'status' => "SEMAKAN",
//            'by' => Auth::user()->id,
//            'datetime' => new \MongoDB\BSON\UTCDateTime(new \DateTime('now')),
//        ]);
//
//        return redirect('/admin/affiliates/transaksi/pengeluaran');
//    }
//
//    public function admin_transaction_pengeluaran_diluluskan(Request $request)
//    {
//        $withdraws = CommissionWithdraw::where('status', 'LULUS')->get();
//
//        return view('affiliates.pengeluaran')->with('withdraws', $withdraws)
//            ->with('pageTitle', 'Pengeluaran Diluluskan');
//    }
//
//    public function admin_transaction_pengeluaran_refund(Request $request)
//    {
//        $withdraws = CommissionWithdraw::where('status', 'REFUND')->get();
//
//        return view('affiliates.pengeluaran')->with('withdraws', $withdraws)
//            ->with('pageTitle', 'Pengeluaran Refund');
//    }
//
//    public function admin_transaction_pengeluaran_semakan(Request $request)
//    {
//        $withdraws = CommissionWithdraw::where('status', 'SEMAKAN')->get();
//
//        return view('affiliates.pengeluaran')->with('withdraws', $withdraws)
//            ->with('pageTitle', 'Pengeluaran Perlu Semakan');
//    }

    public function apply_affiliate()
    {
        $user = Auth::User();
        $user->is_affiliate = 'apply';
        $user->affiliate_apply = Carbon::now();
        $user->save();

        return back();
    }
}
