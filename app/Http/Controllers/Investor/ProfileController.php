<?php

namespace App\Http\Controllers\Investor;

use App\Mail\User\SentDeclarationProfile;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Traits\SmsTrait;
use Auth;
use Storage;
use Illuminate\Support\Facades\Validator;
use App\Message;


class ProfileController extends Controller
{
    use SmsTrait;

    public function editInfoPemohon(Request $request)
    {
        $messages = [
            'org_name.required' => 'Nama Penuh Anda Perlu Diisi',
            'org_jantina.required' => 'Sila Nyatakan Jantina Anda',
            'org_bangsa.required' => 'Sila Nyatakan Bangsa Anda',
            'org_agama.required' => 'Sila Nyatakan Agama Anda',
            'org_kahwin.required' => 'Sila Nyatakan Status Perkahwinan Anda',
            'org_dob_day.required' => 'Tarik Lahir (Hari) Perlu Diisi',
            'org_dob_month.required' => 'Tarik Lahir (Bulan) Perlu Diisi',
            'org_dob_year.required' => 'Tarik Lahir (Tahun) Perlu Diisi',
        ];

        $validator = Validator::make($request->all(), [
            'org_name' => 'required',
            'org_jantina' => 'required',
            'org_bangsa' => 'required',
            'org_agama' => 'required',
            'org_kahwin' => 'required',
            'org_dob_day' => 'required',
            'org_dob_day' => 'required',
            'org_dob_year' => 'required',
        ], $messages);

        if ($validator->fails()) {

            return redirect('/profile')
                ->withErrors($validator)
                ->withInput();
        }

        $user_id = Auth::id();

        $day = $request->input('org_dob_day');
        $month = $request->input('org_dob_month');
        $year = $request->input('org_dob_year');

        if ($day == null || $month == null || $year == null) {
            $dob = null;
        } else {
            $dob = Carbon::parse('' . $year . '-' . $month . '-' . $day . ' 00:00:00');
        }

        $user = User::findOrFail($user_id);

        User::where('id', $user_id)->update([
            'name' => strtoupper($request->input('org_name')),
            'sex' => $request->input('org_jantina'),
            'religion' => $request->input('org_agama'),
            'race' => $request->input('org_bangsa'),
            'marital_status' => $request->input('org_kahwin'),
            'dob' => $dob,
        ]);

        activity('user_profile_edit')
            ->performedOn($user)
            ->withProperties(
                [
                    'old' => [
                        'Nama' => $user->name,
                        'Jantina' => $user->sex,
                        'Agama' => $user->religion,
                        'Bangsa' => $user->race,
                        'Status Perkahwinan' => $user->marital_status,
                        'Tarikh Lahir' => $user->dob,
                    ],
                    'new' => [
                        'Nama' => strtoupper($request->input('org_name')),
                        'Jantina' => $request->input('org_jantina'),
                        'Agama' => $request->input('org_agama'),
                        'Bangsa' => $request->input('org_bangsa'),
                        'Status Perkahwinan' => $request->input('org_kahwin'),
                        'Tarikh Lahir' => $dob,
                    ]
                ]
            )
            ->log('Kemaskini Info Pemohon');
        session()->flash('success', 'Info Peribadi telah berjaya dikemaskini');
        return redirect('/profile');
    }

    public function editInfoMykad(Request $request)
    {
        // $messages = [
        //     'mycard_front.required' => 'Mykad Muka Hadapan Perlu Dimuatnaik',
        //     'mycard_front.max' => 'Saiz Gambar Mykad Muka Hadapan Perlu Kurang Dari 2MB',
        //     'mycard_front.image' => 'Mykad Muka Hadapan Perlu Dimuatnaik Dalam Format Gambar .png .jpg .bmp Sahaja.',
        //     // 'mycard_back.required' => 'Mykad Muka Belakang Perlu Dimuatnaik',
        //     // 'mycard_back.max' => 'Saiz Gambar Mykad Muka Belakang Perlu Kurang Dari 2MB',
        //     // 'mycard_back.image' => 'Mykad Muka Belakang Perlu Dimuatnaik Dalam Format Gambar .png .jpg .bmp Sahaja.'
        // ];

        // $validator = Validator::make($request->all(), [
        //     // 'mycard_front' => 'required|image|max:2048',
        //     // 'mycard_back' => 'required|image|max:2048',
        // ], $messages);

        // if ($validator->fails()) {
        //     return redirect('/profile')
        //         ->withErrors($validator)
        //         ->withInput();
        // }

        $user_id = Auth::id();

        if ($request->hasFile('mycard_front') == true) {

            $store = Storage::disk('do_spaces')->putFile(env('DO_SPACES_UPLOAD') . 'uploads/ic', request()->mycard_front, 'public');

            $attac = User::where('id', $user_id)->first();
            Storage::disk('do_spaces')->delete($attac->mykad_front);

            $user = User::where('id', $user_id)->update([
                'mykad_front' => $store,

            ]);
        }

        // if ($request->hasFile('mycard_back') == true) {

        //     $store = Storage::disk('do_spaces')->putFile(env('DO_SPACES_UPLOAD') . 'uploads/ic', request()->mycard_back, 'public');

        //     $attac = User::where('id', $user_id)->first();
        //     Storage::disk('do_spaces')->delete($attac->mykad_back);

        //     $user = User::where('id', $user_id)->update([
        //         'mykad_back' => $store,

        //     ]);
        // }
        session()->flash('success', 'Info MYKAD telah berjaya dikemaskini');
        return redirect('/profile');
    }

    public function editAlamatPemohon(Request $request)
    {
        $messages = [
            'org_address_line_1.required' => 'Alamat Baris 1 Perlu Diisi',
            'org_address_line_2.required' => 'Alamat Baris 2 Perlu Diisi',
            'org_poscode.required' => 'Poskod Perlu Diisi',
            'org_city.required' => 'Bandar Perlu Diisi',
            'org_state.required' => 'Negeri Perlu Diisi',
            'org_parlimen.required' => 'Kawasan Parlimen Perlu Diisi'
        ];

        $validator = Validator::make($request->all(), [
            'org_address_line_1' => 'required',
            'org_address_line_2' => 'required',
            'org_poscode' => 'required',
            'org_city' => 'required',
            'org_state' => 'required',
            'org_parlimen' => 'required',
        ], $messages);

        if ($validator->fails()) {

            return redirect('/profile')
                ->withErrors($validator)
                ->withInput();
        }

        $user_id = Auth::id();

        $user = User::findOrFail($user_id);

        User::where('id', $user_id)->update([
            'address_line_1' => $request->input('org_address_line_1'),
            'address_line_2' => $request->input('org_address_line_2'),
            'address_line_3' => $request->input('org_address_line_3'),
            'postcode' => $request->input('org_poscode'),
            'city' => $request->input('org_city'),
            'state' => $request->input('org_state'),
            'parliament' => $request->input('org_parlimen'),
        ]);

        activity('user_profile_edit')
            ->performedOn($user)
            ->withProperties(
                [
                    'old' => [
                        'Alamat Baris 1' => $user->address_line_1,
                        'Alamat Baris 2' => $user->address_line_2,
                        'Alamat Baris 3' => $user->address_line_3,
                        'Poskod' => $user->postcode,
                        'Bandar' => $user->city,
                        'Negeri' => $user->state,
                        'Parlimen' => $user->parliament
                    ],
                    'new' => [
                        'Alamat Baris 1' => $request->input('org_address_line_1'),
                        'Alamat Baris 2' => $request->input('org_address_line_2'),
                        'Alamat Baris 3' => $request->input('org_address_line_3'),
                        'Poskod' => $request->input('org_poscode'),
                        'Bandar' => $request->input('org_city'),
                        'Negeri' => $request->input('org_state'),
                        'Parlimen' => $request->input('org_parlimen'),
                    ]
                ]
            )
            ->log('Kemaskini Alamat Pemohon');
        session()->flash('success', 'Info Alamat telah berjaya dikemaskini');
        return redirect('/profile');
    }

    public function editKontak(Request $request)
    {
        $messages = [
            'email.required' => 'Alamat Emel Perlu Diisi',
            'mobile_no.required' => 'No. Tel. Bimbit Perlu Diisi',
        ];

        $validator = Validator::make($request->all(), [
            'mobile_no' => 'required',
            'email' => 'required',
        ], $messages);

        if ($validator->fails()) {
            return redirect('/profile')
                ->withErrors($validator)
                ->withInput();
        }

        $user_id = Auth::id();
        $user = User::findOrFail($user_id);

        User::where('id', $user_id)->update([
            'mobile_no' => $request->input('mobile_no'),
            'email' => $request->input('email'),
            'home_no' => $request->input('house_tel'),

        ]);

        activity('user_profile_edit')
            ->performedOn($user)
            ->withProperties(
                [
                    'old' => [
                        'No. Tel. Bimbit' => $user->mobile_no,
                        'Emel' => $user->email,
                        'No. Tel. Rumah' => $user->home_no,
                    ],
                    'new' => [
                        'No. Tel. Bimbit' => $request->input('mobile_no'),
                        'Emel' => $request->input('email'),
                        'No. Tel. Rumah' => $request->input('house_tel'),
                    ]
                ]
            )
            ->log('Kemaskini Kontak Pemohon');
        session()->flash('success', 'Info Kontak telah berjaya dikemaskini');
        return redirect('/profile');
    }

    public function editPekerjaan(Request $request)
    {
        $messages = [

            'jawatan_pekerjaan.required' => 'Jawatan Pekerjaan Perlu Diisi',
            'pendapatan_bulanan.required' => 'Pendapatan Bulanan Perlu Diisi'
        ];

        $validator = Validator::make($request->all(), [
            'jawatan_pekerjaan' => 'required',
            'pendapatan_bulanan' => 'required',
        ], $messages);

        if ($validator->fails()) {

            return redirect('/profile')
                ->withErrors($validator)
                ->withInput();
        }

        $user_id = Auth::id();
        $user = User::findOrFail($user_id);
        User::where('id', $user_id)->update([
            'job' => $request->input('jawatan_pekerjaan'),
            'monthly_income' => $request->input('pendapatan_bulanan'),
        ]);

        activity('user_profile_edit')
            ->performedOn($user)
            ->withProperties(
                [
                    'old' => [
                        'Jawatan Pekerjaan' => $user->job,
                        'Pendapatan Bulanan Di Bawah' => $user->monthly_income,
                    ],
                    'new' => [
                        'Jawatan Pekerjaan' => $request->input('jawatan_pekerjaan'),
                        'Pendapatan Bulanan Di Bawah' => $request->input('pendapatan_bulanan'),
                    ]
                ]
            );
        session()->flash('success', 'Info Pekerjaan telah berjaya dikemaskini');
        return redirect('/profile');
    }

    public function editBank(Request $request)
    {
        $messages = [
            'org_bank_name.required' => 'Nama Bank  Perlu Diisi',
            'org_bank_acc.required' => 'No. Akaun Bank Perlu Diisi',
            'bank_account_name.required' => 'Nama Pemegang Akaun Perlu Diisi',
            'org_blacklist.required' => 'Status Senarai Hitam Perlu Diisi',
        ];

        $validator = Validator::make($request->all(), [
            'org_bank_name' => 'required',
            'org_bank_acc' => 'required',
            'bank_account_name' => 'required',
            'org_blacklist' => 'required',
        ], $messages);

        if ($validator->fails()) {

            return redirect('/profile')
                ->withErrors($validator)
                ->withInput();
        }
        $user_id = Auth::id();

        //temp solution for null || "" select input
        // $blacklist_dividend = $request->input('org_dividen');
        // if ($request->input('org_dividen') == "" || $request->input('org_dividen') == null) {
        //     $blacklist_dividend = 0;
        // }
        $blacklist = $request->input('org_blacklist');
        if ($request->input('org_blacklist') == "" || $request->input('org_blacklist') == null) {
            $blacklist = 0;
        }

        $user = User::findOrFail($user_id);

        User::where('id', $user_id)->update([
            'bank_name' => $request->input('org_bank_name'),
            'bank_account' => $request->input('org_bank_acc'),
            'bank_account_name' => $request->input('bank_account_name'),
            'blacklist' => $blacklist,

        ]);

        activity('user_profile_edit')
            ->performedOn($user)
            ->withProperties(
                [
                    'old' => [
                        'Nama Bank' => $user->bank_name,
                        'No. Akaun Bank' => $user->bank_account,
                        'Nama Pemegang Akaun' => $user->bank_account_name,
                        'Status Senarai Hitam' => $user->blacklist,
                    ],
                    'new' => [
                        'Nama Bank' => $request->input('org_bank_name'),
                        'No. Akaun Bank' => $request->input('org_bank_acc'),
                        'Nama Pemegang Akaun' => $request->input('bank_account_name'),
                        'Status Senarai Hitam' => $blacklist,
                    ]
                ]
            )
            ->log('Kemaskini Bank Pemohon');

        session()->flash('success', 'Info Perbankan telah berjaya dikemaskini');
        return redirect('/profile');
    }

    public function editWaris(Request $request)
    {
        $messages = [
            'org_nama_waris.required' => 'Nama Waris Perlu Diisi',
            'org_no_mykad_waris.required' => 'No. MYKAD Waris Perlu Diisi',
            'org_no_tel_bimbit_waris.required' => 'No. Telefon Bimbit Waris Perlu Diisi',
            'org_hubungan.required' => 'Sila Nyatakan Hubungan Dengan Waris',
        ];

        $validator = Validator::make($request->all(), [
            'org_nama_waris' => 'required',
            'org_no_mykad_waris' => 'required',
            'org_no_tel_bimbit_waris' => 'required',
            'org_hubungan' => 'required',
        ], $messages);

        if ($validator->fails()) {

            return redirect('/profile')
                ->withErrors($validator)
                ->withInput();
        }

        $user_id = Auth::id();

        $user = User::findOrFail($user_id);

        User::where('id', $user_id)->update([
            'heir_name' => $request->input('org_nama_waris'),
            'heir_ic' => $request->input('org_no_mykad_waris'),
            'heir_phone' => $request->input('org_no_tel_bimbit_waris'),
            'heir_relationship' => $request->input('org_hubungan'),

        ]);

        activity('user_profile_edit')
            ->performedOn($user)
            ->withProperties(
                [
                    'old' => [
                        'Nama Penuh Waris' => $user->heir_name,
                        'No. MyKad Waris' => $user->heir_ic,
                        'No. Tel Bimbit Waris' => $user->heir_phone,
                        'Hubungan' => $user->heir_relationship,
                    ],
                    'new' => [
                        'Nama Penuh Waris' => $request->input('org_nama_waris'),
                        'No. MyKad Waris' => $request->input('org_no_mykad_waris'),
                        'No. Tel Bimbit Waris' => $request->input('org_no_tel_bimbit_waris'),
                        'Hubungan' => $request->input('org_hubungan'),
                    ]
                ]
            )
            ->log('Kemaskini Waris Pemohon');
        session()->flash('success', 'Info Waris telah berjaya dikemaskini');
        return redirect('/profile');
    }

    public function submit(Request $request)
    {
        $user_id = Auth::id();

        $user = User::where('id', $user_id)->first();

        $array = array();

        if ($user->name == NULL || $user->sex == NULL || $user->race == NULL || $user->religion == NULL || $user->marital_status == NULL || $user->dob == NULL) {
            array_push($array, "INFO PEMOHON TIDAK LENGKAP");
        }
        // if ($user->mykad_front == NULL) {
        //     array_push($array, "INFO MYKAD TIDAK LENGKAP");
        // }
        if ($user->address_line_1 == NULL || $user->postcode == NULL || $user->city == NULL || $user->state == NULL || $user->parliament == NULL) {
            array_push($array, "INFO ALAMAT TIDAK LENGKAP");
        }
        if ($user->mobile_no == NULL || $user->email == NULL) {
            array_push($array, "INFO KONTAK TIDAK LENGKAP");
        }
        if ($user->job == NULL || $user->monthly_income == NULL) {
            array_push($array, "INFO PEKERJAAN TIDAK LENGKAP");
        }
        if ($user->bank_name == NULL || $user->bank_account == NULL) {
            array_push($array, "INFO PERBANKAN TIDAK LENGKAP");
        }
        if ($user->heir_name == NULL || $user->heir_ic == NULL || $user->heir_phone == NULL || $user->heir_relationship == NULL) {
            array_push($array, "INFO WARIS TIDAK LENGKAP");
        }
        if (count($array) > 0) {
            return redirect('/profile')->withErrors($array);
        } else {

            User::where('id', $user_id)->update([
                'status' => 2
            ]);

            try {

                $smsText = "Pendaftaran baharu anda berjaya dihantar. Sila tunggu emel pengesahan dalam tempoh 7-14 hari waktu bekerja. Terima kasih.";

                $this->send_sms($user->ic, $user->mobile_no, $smsText);

                \Mail::to($user->email)->send(new SentDeclarationProfile($user->name, $user->ic, $user->created_at));
            } catch (\Exception $e) {
                \Log::info($e->getMessage());
            }

            if ($user->mykad_front == NULL) {
                Message::create([
                    'ic' => $user->ic,
                    'messages' => $request->mesej,
                    'info' => ['nama' => $user->name, 'phone' => $user->mobile_no, 'email' => $user->email],
                    'remarks' => 'Tidak Muat Naik Gambar MYKAD',
                ]);
            }
            // if ($request->filled('mesej')) {
            //     Message::create([
            //         'ic' => $user->ic,
            //         'messages' => $request->mesej,
            //         'info' => ['nama' => $user->name, 'phone' => $user->mobile_no, 'email' => $user->email],
            //         'remarks' => 'Sewaktu Hantar Maklumat',
            //     ]);
            // }

            return redirect('/home')->with('profile_submitted', 'profile_submitted');
        }
    }
}
