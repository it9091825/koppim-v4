<?php

namespace App\Http\Controllers\Investor\Transactions;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Payment;
use Auth;
use App\Mongo\Payment as MPayment;

class LogsController extends Controller
{
    public function index()
    {

        $transactions = MPayment::where('ic', Auth::user()->ic)->where('returncode', '100')->orderBy('payment_date', 'desc')->get();

        return view('investor.transactions.logs.index', compact('transactions'));
    }
}
