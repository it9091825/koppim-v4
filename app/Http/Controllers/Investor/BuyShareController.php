<?php

namespace App\Http\Controllers\Investor;

use App\Message;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Mongo\Payment as MPayment;
use App\Traits\OrderTrait;

class BuyShareController extends Controller
{
    use OrderTrait;

    public function buy()
    {
        return view('investor.langgan.buy');
    }

    public function store_buy(Request $request)

    {
        $user = User::where('ic', $request->ic)->first();
        $order_ref = $this->create_order_ref($request->ic);

        if ($user) {

            if($user->status != 1)
            {
                return redirect('/langgan-syer')->withErrors(['Sebarang pertambahan syer dan transaksi tidak dapat dilakukan sehingga status keanggotaan diluluskan.']);
            }

            MPayment::create([
                'name' => strtoupper($user->name),
                'ic' => $request->ic,
                'phone' => $user->mobile_no,
                'email' => $user->email,
                'amount' => (double)$request->amount,
                'first_time_fees' => 0.00,
                'share_amount' => (double)$request->amount,
                'first_time_share' => 0.00,
                'additional_share' => (double)$request->amount,
                'status' => 'verification',
                'type' => 'one-time',
                'order_ref' => $order_ref,
                'payment_desc' => 'Langganan Modal Tambahan',
            ]);

            if ($request->filled('mesej')) {
                Message::create([
                    'ic' => $request->ic,
                    'messages' => $request->mesej,
                    'info' => ['name' => $user->name, 'phone' => $user->mobile_no, 'email' => $user->email],
                    'remarks' => 'Langganan Syer',
                ]);
            }
        } else {
            MPayment::create([
                'name' => strtoupper($request->name),
                'ic' => $request->ic,
                'phone' => $request->phone,
                'email' => $request->email,
                'amount' => (double)$request->amount,
                'first_time_fees' => 0.00,
                'share_amount' => (double)$request->amount,
                'first_time_share' => 0.00,
                'additional_share' => (double)$request->amount,
                'status' => 'verification',
                'type' => 'one-time',
                'order_ref' => $order_ref,
                'payment_desc' => 'Langganan Modal Tambahan',
            ]);

            if ($request->filled('mesej')) {
                Message::create([
                    'ic' => $request->ic,
                    'messages' => $request->mesej,
                    'info' => ['nama' => $request->name, 'phone' => $request->phone, 'email' => $request->email],
                    'remarks' => 'Langganan Syer',
                ]);
            }
        }

        return redirect('/langgan-syer/pengesahan?order_ref=' . $order_ref . '');
    }

    public function verify(Request $request)
    {

        $mpayment = MPayment::where('order_ref', $request->input('order_ref'))->first();
        $amount = $mpayment->amount;
        $url = '/payment-gateway/senang-pay/submit';
        $url_recurring = '/payment-gateway/senang-pay/submit-recurring';
        $user = User::where('ic', $mpayment->ic)->first();

        $recurringCheck = MPayment::where('user_id', $user->id)->where('type', 'recurring')->exists();

        if ($user) {
            $registered = true;
        } else {
            $registered = false;
        }

        return view('investor.langgan.verify')
            ->with('registered', $registered)
            ->with('payment', $mpayment)
            ->with('amount', $amount)
            ->with('recurringCheck', $recurringCheck)
            ->with('url_recurring', $url_recurring)
            ->with('url', $url);
    }
}
