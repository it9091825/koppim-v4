<?php

namespace App\Http\Controllers\Investor;

use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Validator;

class SemakKeanggotaanController extends Controller
{
    public function index()
    {
        return view('investor.member.verify.index');
    }

    public function findUser(Request $request)
    {
        $messages = [
            'ic.required' => 'No. MYKAD wajib diisi',
            'ic.min' => 'Sila isi No. MYKAD yang sah. Tidak kurang daripada 12 digit.',
            'ic.max' => 'Sila isi No. MYKAD yang sah. Tidak lebih daripada 12 digit.',
        ];

        $validator = Validator::make($request->all(), [
            'ic' => 'required|min:12|max:12'
        ], $messages);

        if ($validator->fails()) {

            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        try {

            $info = User::where('type', 'user')->where('ic', $request->ic)->first();
            if ($info) {
                session()->flash('success', 'Carian berjaya!');
                return view('investor.member.verify.index', compact('info'));
            }
            session()->flash('anggota-tidak-wujud', 'Maklumat keanggotaan tidak wujud. Sila cuba semula.');
            return redirect()->route('member.verify');

        } catch (Exception $e) {
            return redirect()->back()->with(['errors', 'Proses carian tidak dapat dijalankan pada waktu ini. Sila hubungi pihak KoPPIM untuk pertanyaan.']);
        }
    }
}
