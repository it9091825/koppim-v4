<?php

namespace App\Http\Controllers\Investor;

use App\Http\Controllers\Controller;
use App\Mail\IkoopFirstTimeMail;
use App\Mail\Payment\SentPaymentProof;
use App\Message;
use App\Prospect;
use App\User;
use Auth;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Log;
use Mail;
use Storage;
use App\Mongo\Payment as MPayment;
use App\Traits\OrderTrait;
use App\Traits\SmsTrait;
use App\Traits\MobileNumberTrait;
use App\Traits\NexmoTrait;
use App\Mongo\AffiliateLinkVisitor as AffiliateLinkVisitor;

class InvestorController extends Controller
{
    use SmsTrait, OrderTrait, MobileNumberTrait, NexmoTrait;

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('investor.login');
    }

    public function forgot_password()
    {
        return view('investor.forgot-password');
    }

    public function signup(Request $request)
    {
        $no_anggota = $request->input('u');

        if ($no_anggota) {
            Session::put('no_anggota', $no_anggota);

            AffiliateLinkVisitor::create([
                'no_anggota' => $no_anggota,
                'ip' => $request->ip() //$this->getIp()
            ]);

        }

        //Get no anggota from session, incase user close browser or remove url affiliate, the no anggota still exist in session.
        $val = Session::get('no_anggota');

        return view('investor.buy')->with('no_anggota', $val);
    }

    //Not used. Using laravel request ip
    public function getIp()
    {

        foreach (array('HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR') as $key) {
            if (array_key_exists($key, $_SERVER) === true) {
                foreach (explode(',', $_SERVER[$key]) as $ip) {
                    $ip = trim($ip); // just to be safe
                    if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE) !== false) {
                        return $ip;
                    }
                }
            }
        }

    }

    public function paymentproof()
    {
        return view('investor.buy-proof');
    }

    public function ikoopfirsttime()
    {
        return view('investor.ikoop-firsttime');
    }

    public function ikoopfirsttimethankyou()
    {
        return view('investor.ikoop-firsttime-thankyou');
    }

    public function ikoopfirsttimeexe(Request $request)
    {
        $ic = $request->input('ic');

        $data = User::where('ic', $ic)->first();

        if ($data) {
            $mobile_no = $this->mobile_no_trim($data->mobile_no);

            User::where('ic', $ic)->update([
                'mobile_no' => $mobile_no,
            ]);

            if ($data->ikoop != 1) {
                return back()->withErrors(['MyKad Telah Didaftarkan. Sila Log Masuk Ke Akaun.']);
            } else {
                return redirect('/ikoop-migration/verification?ic=' . $ic);
            }

        } else {
            return back()->withErrors(['MyKad Anda Tidak Dijumpai.']);
        }

    }

    public function ikoopfirsttime_2(Request $request)
    {
        $ic = $request->input('ic');
        $data = User::where('ic', $ic)->first();
        if ($data) {
            return view('investor.ikoop-firsttime-4-digit')->with('user', $data);
        } else {
            return redirect()->route('ikoopfirsttime')->withErrors(['MyKad Anda Tidak Dijumpai.']);
        }
    }

    //Last migration process
    public function ikoopfirsttime_3(Request $request)
    {
        $ic = $request->input('ic');
        $code = $request->input('code');
        $data = User::where('ic', $ic)->first();
        $str1 = $data->mobile_no;
        $sdsd = substr($str1, -4);

        if ($sdsd == $code) {

            $pin = rand(111111, 999999);
            $password = Hash::make($pin);
            User::where('ic', $ic)->update([
                'otp' => $pin,
                'password' => $password,
                'ikoop' => 2,
            ]);

            try {
                $text = "Sila log masuk ke Akaun Anggota KoPPIM anda di https://anggota.koppim.com.my/ dengan kata laluan ini: " . $pin . ". Terima kasih.";
                $mobile = $data->mobile_no;
                $this->send_sms($data->ic, $mobile, $text);

                Mail::to($data->email)->send(new IkoopFirstTimeMail($pin));
            } catch (\Exception $e) {
                activity('exception')->log($e->getMessage());
            }

            return redirect('/ikoop-migration/verification/thank-you');

        } else {
            return back();
        }

    }

    public function paymentproofexe(Request $request)
    {
        $messages = [
            'email.required' => 'Alamat Emel Perlu Diisi',
            'phone.required' => 'No. Telefon Perlu Diisi',
            'ic.required' => 'No. Mykad Perlu Diisi',
            'name.required' => 'Nama Penuh Perlu Diisi',
            'jumlah.required' => 'Jumlah Perlu Diisi',
            'proof.required' => 'Bukti Pembayaran Perlu Dimuatnaik',
            'proof.max' => 'Saiz Gambar Bukti Pembayaran Perlu Kurang Dari 2MB',
        ];

        $validator = Validator::make($request->all(), [
            'proof' => 'required|image|max:2048',
            'ic' => 'required|digits:12',
            'phone' => 'required|numeric',
            'name' => 'required',
            'email' => 'required',
            'jumlah' => 'required|numeric',
        ], $messages);

        if ($validator->fails()) {
            return redirect('/hantar-bukti-pembayaran')
                ->withErrors($validator)
                ->withInput();
        }

        $name = $request->input('name');
        $ic = $request->input('ic');
        $mobile_no = $request->input('phone');
        $email = $request->input('email');
        $amount = $request->input('jumlah');
        $hour = $request->input('hour');
        $minute = $request->input('minute');
        $day = $request->input('day');
        $month = $request->input('month');
        $year = $request->input('year');
        $note = $request->input('note');

        $payment_date = Carbon::create($year, $month, $day, $hour, $minute, 0);

        //Checking if user send duplicate based on payment date
        $uploadCheck = MPayment::where('ic', (string)$ic)->where('amount',(double)$amount)->where('payment_date',$payment_date)->first();
        if($uploadCheck){
            return back()->withErrors(['Rekod Bukti Pembayaran Telah Dimuatnaik Sebelum Ini.']);
        }

        if ($request->hasFile('proof') == true) {

            $store = Storage::disk('do_spaces')->putFile(env('DO_SPACES_UPLOAD') . 'upload/payment_proof', request()->proof, 'public');
//            $payment_date = Carbon::create($year, $month, $day, $hour, $minute, 0);

            if ($request->has('order_ref')) {
                $order_ref = $request->input('order_ref');
                $mpayment = MPayment::where('order_ref', $order_ref)->firstOrFail();
                if ($mpayment && $mpayment->status != 'payment-completed') {
                    $mpayment->payment_proof = $store;
                    $mpayment->payment_date = $payment_date;
                    $mpayment->payment_desc = 'Bayaran Manual';
                    $mpayment->note = $note;
                    $mpayment->status = 'waiting-for-verification';
                    $mpayment->amount = (double)$amount;
//                    $mpayment->share_amount = 0.00;
//                    $mpayment->first_time_fees = 0.00;
//                    $mpayment->first_time_share = 0.00;
//                    $mpayment->additional_share = 0.00;
                    $mpayment->save();
                }

            } else {

                $order_ref = $this->create_order_ref($ic);
                MPayment::create([
                    'name' => strtoupper($name),
                    'ic' => (string)$ic,
                    'phone' => (string)$mobile_no,
                    'email' => $email,
                    'amount' => (double)$amount,
                    'share_amount' => 0.00,
                    'first_time_fees' => 0.00,
                    'first_time_share' => 0.00,
                    'additional_share' => 0.00,
                    'status' => 'waiting-for-verification',
                    'type' => 'one-time',
                    'order_ref' => $order_ref,
                    'payment_desc' => 'Bayaran Manual',
                    'channel' => 'manual',
                    'bank' => 'bank islam',
                    'payment_proof' => $store,
                    'payment_date' => $payment_date,
                    'note' => $note,
                ]);
            }

            if ($request->filled('note')) {

                Message::create([
                    'ic' => $ic,
                    'messages' => $note,
                    'info' => ['nama' => $name, 'phone' => $mobile_no, 'email' => $email],
                    'remarks' => 'Hantar Bukti Pembayaran',
                ]);

            }

            try {

                $smsText = "Kami telah menerima maklumat pembayaran yang dikemukakan. Pengesahan dan kelulusan akan dibuat dalam tempoh 7-14 hari waktu bekerja. Terima kasih.";

                $this->send_sms($ic, $request->input('phone'), $smsText);

                \Mail::to($email)->send(new SentPaymentProof($name));

            } catch (\Exception $e) {
                \Log::info($e->getMessage());
            }

            return redirect('/success-proof-payment-upload');
        } else {
            return back();
        }

    }

    public function create_order(Request $request)
    {
        $user = User::where('ic', $request->ic)->first();
        $order_ref = $this->create_order_ref($request->ic);

        $no_anggota = $request->input('no_anggota');

        if ($no_anggota == NULL) {
            $introducer_no_anggota = 0;
            $introducer_id = 0;
            $introducer = false;
        } else {
            $anggotaaffiliates = User::where('no_koppim', $no_anggota)->where('is_affiliate','yes')->first();
            if ($anggotaaffiliates) {
                $introducer_no_anggota = $no_anggota;
                $introducer_id = $anggotaaffiliates->id;
                $introducer = true;
            } else {
                $introducer_no_anggota = 0;
                $introducer_id = 0;
                $introducer = false;
            }
        }

        if ($user) {

            // check if user status not Diluluskan, cannot add share
            if($user->status != 1)
            {
                return redirect('/langganan-syer')->withErrors(['Sebarang pertambahan syer dan transaksi tidak dapat dilakukan sehingga status keanggotaan diluluskan.']);
            }

            $check_processing_fee = MPayment::where('ic', $user->ic)->where('returncode', '100')->where('first_time_fees', 30.00)->first();

            if ($check_processing_fee) {
                //check if already paid processing fee, then only need to pay for shares

                $totalamt = (double)$request->amount;

                $mpayment = MPayment::create([
                    'name' => strtoupper($user->name),
                    'ic' => $request->ic,
                    'phone' => $user->mobile_no,
                    'email' => $user->email,
                    'amount' => (double)$totalamt,
                    'first_time_fees' => 0.00,
                    'share_amount' => (double)$request->amount,
                    'first_time_share' => 0.00,
                    'additional_share' => (double)$request->amount,
                    'status' => 'verification',
                    'type' => 'one-time',
                    'order_ref' => $order_ref,
                    'payment_desc' => 'Langganan Modal Tambahan',
                    'user_id' => $user->id,

                ]);

            } else {
                //else processing fee not paid, need to add processing fee

                $processingfee = 30.00;
                $totalamt = (double)$processingfee + (double)$request->amount;

                $mpayment = MPayment::create([
                    'name' => strtoupper($user->name),
                    'ic' => $request->ic,
                    'phone' => $user->mobile_no,
                    'email' => $user->email,
                    'amount' => (double)$totalamt,
                    'share_amount' => (double)$request->amount,
                    'first_time_fees' => (double)$processingfee,
                    'first_time_share' => 100.00,
                    'additional_share' => (double)$request->amount - 100.00,
                    'status' => 'verification',
                    'type' => 'one-time',
                    'introducer' => $introducer,
                    'introducer_id' => $introducer_id,
                    'introducer_no_anggota' => $introducer_no_anggota,
                    'order_ref' => $order_ref,
                    'payment_desc' => 'Pendaftaran Anggota Baharu Dan Langganan Modal Syer',
                    'user_id' => $user->id,
                    'know_from' => $request->know_from
                ]);

            }

            if ($request->filled('mesej')) {

                Message::create([
                    'ic' => $request->ic,
                    'messages' => strtoupper($request->mesej),
                    'info' => ['name' => strtoupper($user->name), 'phone' => $user->mobile_no, 'email' => $user->email],
                    'remarks' => strtoupper('Langganan Syer'),
                ]);

            }

        } else {
            //If user not registered yet, create new user with processing fee

            $processingfee = 30.00;
            $totalamt = (double)$processingfee + (double)$request->amount;

            $mpayment = MPayment::create([
                'name' => strtoupper($request->name),
                'ic' => $request->ic,
                'phone' => $request->phone,
                'email' => $request->email,
                'amount' => (double)$totalamt,
                'share_amount' => (double)$request->amount,
                'first_time_fees' => (double)$processingfee,
                'first_time_share' => 100.00,
                'additional_share' => (double)$request->amount - 100.00,
                'status' => 'verification',
                'type' => 'one-time',
                'introducer' => $introducer,
                'introducer_id' => $introducer_id,
                'introducer_no_anggota' => $introducer_no_anggota,
                'order_ref' => $order_ref,
                'payment_desc' => 'Pendaftaran Anggota Baharu Dan Langganan Modal Syer',
                'know_from' => $request->know_from
            ]);

            if ($request->filled('mesej')) {

                Message::create([
                    'ic' => $request->ic,
                    'messages' => strtoupper($request->mesej),
                    'info' => ['nama' => strtoupper($request->name), 'phone' => $request->phone, 'email' => $request->email],
                    'remarks' => 'Langganan Syer',
                ]);

            }

        }

        return redirect('/langganan-syer/pengesahan?order_ref=' . $order_ref . '');
    }

    public function verify(Request $request)
    {
        $mpayment = MPayment::where('order_ref', $request->input('order_ref'))->first();

        if($mpayment){
            $amount = $mpayment->amount;
            $url = '/payment-gateway/senang-pay/submit';
            $url_recurring = '/payment-gateway/senang-pay/submit-recurring';

            return view('investor.buy-verify')
                ->with('payment', $mpayment)
                ->with('amount', $amount)
                ->with('url_recurring', $url_recurring)
                ->with('url', $url);
        }

        return redirect('/langganan-syer');
    }

    public function buy_manual(Request $request)
    {
        $order_ref = $request->input('order_ref');

        $prospect = Prospect::where('order_ref', $order_ref)->first();

        $payment = MPayment::where('order_ref', $order_ref)->first();

        if (!$prospect || $prospect == null) {
            Prospect::create([
                'order_ref' => $order_ref,
                'name' => strtoupper($request->name),
                'ic' => $request->ic,
                'email' => $request->email,
                'phone' => $request->phone,
                'amount' => $payment->amount,
                'first_time_fees' => $payment->first_time_fees,
                'share_amount' => $payment->share_amount
            ]);
        }

        return view('investor.buy-manual')
            ->with('payment', $payment);

    }

    public function buy_manual_exe(Request $request)
    {
        $order_ref = $request->input('order_ref');

        $mpayment = MPayment::where('order_ref', $order_ref)->firstOrFail();
        $mpayment->channel = 'manual';
        $mpayment->bank = 'bank islam';
        $mpayment->status = 'waiting-proof-upload';
        $mpayment->save();

        return redirect('/hantar-bukti-pembayaran?order_ref=' . $order_ref . '&name=' . $request->name . '&ic=' . $request->ic . '&phone=' . $request->phone . '&email=' . $request->email);

    }

    public function login_by_email_exe(Request $request)
    {

        $email = $request->input('email');
        $password = $request->input('password');

        $log = User::where('ic', $email)->where('type', 'user')->first();

        if ($log) {
            if ($log->status === 98) {
                return redirect()->back()->withErrors(['Maklumat Yang Dimasukkan Tidak Tepat']);
            } else {
                if (Hash::check($password, $log->password)) {

                    Auth::loginUsingId($log->id);
                    activity('user_auth')
                        ->performedOn($log)
                        ->withProperties(
                            [
                                'Nama' => $log->name,
                                'No. MyKad' => $log->ic,
                                'IP' => $request->ip(),
                            ]
                        )
                        ->log('Anggota Log Masuk');
                    return redirect('/dashboard')->with('success', 'Anda telah berjaya log masuk ke akaun anda.');

                } else {

                    return redirect()->back()->withErrors(['Maklumat Yang Dimasukkan Tidak Tepat']);
                }

            }

        } else {
            return back()->withErrors(['Maklumat Yang Dimasukkan Tidak Tepat']);
        }

    }

    public function email_activation($token)
    {
        $user = User::where('email_token', $token)->first();

        if ($user) {
            User::where('id', $user->id)->update([
                'email_verified_at' => Carbon::now(),
            ]);

            return redirect('/profile')->with('success', 'Emel ' . $user->email . ' telah disahkan.');
        }

    }

    public function logout()
    {
        Auth::logout();
        Session::flush();
        return redirect('/login');
    }

}
