<?php

namespace App\Http\Controllers\Investor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class PdfController extends Controller
{
    /*get file from local storage */
    public function download_local(Request $request){
        if(Storage::disk('local')->exists("pdf/$request->file")) {
            $path = Storage::disk('local')->path("pdf/$request->file");
            $content=file_get_contents($path);
            return response($content)->withHeaders([
                'Content-Type'=>mime_content_type($path)
            ]);
        }
        return redirect('/404');
    }
    /*get file from public storage */
    public function download_public(Request $request)
    {
        if (Storage::disk('public')->exists("pdf/$request->file")) {
            $path = Storage::disk('public')->path("pdf/$request->file");
            $content = file_get_contents($path);
            return response($content)->withHeaders([
                'Content-Type' => mime_content_type($path)
            ]);
        }
        return redirect('/404');
    }
}
