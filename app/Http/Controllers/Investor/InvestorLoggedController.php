<?php

namespace App\Http\Controllers\Investor;

use App\Http\Controllers\Controller;
use App\Payment;
use App\Traits\SmsTrait;
use App\User;
use Auth;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Mail;
use Spatie\Activitylog\Models\Activity;
use Storage;
use Validator;
use App\Mongo\Payment as MPayment;
use App\user_terminations;

class InvestorLoggedController extends Controller
{
    use SmsTrait;

    public function __construct()
    {

        $this->middleware('auth');
        // $this->middleware('resetpassword');
        $this->middleware('firsttime');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

    public function home()
    {
        $last_login = Activity::where('log_name', 'user_auth')->where('subject_id', Auth::id())->latest()->take(2)->get();

//        $latest_transaction = MPayment::where('ic', Auth::user()->ic)->where(function ($query) {
//            $query->where('channel', 'online')->orWhere('channel', 'manual')->orWhere('channel', 'ikoop');
//        })->orderBy('payment_date', 'desc')->first();
        //Only get latest successful transaction
        $latest_transaction = MPayment::where('ic', Auth::user()->ic)->where('returncode','100')->orderBy('payment_date', 'desc')->first();

        $first_time_fees = MPayment::where('ic', auth()->user()->ic)->where('returncode', '100')->sum('first_time_fees');
        $first_time_share = MPayment::where('ic', auth()->user()->ic)->where('returncode', '100')->sum('first_time_share');
        $additional_share = MPayment::where('ic', auth()->user()->ic)->where('returncode', '100')->sum('additional_share');

        $total_share = $first_time_share+$additional_share;

        $path = storage_path() . "/telegram.json";

        $jsonData = json_decode(file_get_contents($path), true);

        return view('investor.dashboard')
            ->with('last_login', $last_login ?? '')
            ->with('latest_transaction', $latest_transaction)
            ->with('first_time_fees', $first_time_fees)
            ->with('first_time_share', $first_time_share)
            ->with('additional_share', $additional_share)
            ->with('total_share', $total_share)
            ->with('jsonData', $jsonData);
    }

    public function invest(Request $request)
    {
        $status = $request->status;

        // check on recurring
        $recurringExist = MPayment::where('ic', auth()->user()->ic)->where('returncode', '100')->where('type', 'recurring')->exists();

        $recurringData = [];

        if ($recurringExist) {
            $recurringData = MPayment::raw()->aggregate([
                0 => [
                    '$match' => [
                        'ic' => (string) auth()->user()->ic,
                        'returncode' => '100',
                        'type' => 'recurring'
                    ],
                ],
                1 => [
                    '$group' => [
                        '_id' => [
                            'month' => [
                                '$month' => '$created_at',
                            ],
                            'year' => [
                                '$year' => '$created_at',
                            ],
                        ],
                        'total' => [
                            '$sum' => '$additional_share',
                        ],
                        'status' => [
                            '$push' => [
                                'paymentAmount' => '$additional_share',
                                'paymentDate' => '$created_at',
                                'status' => [
                                    '$cond' => [
                                        'if' => [
                                            '$eq' => [
                                                0 => '$returncode',
                                                1 => "100",
                                            ],
                                        ],
                                        'then' => 'success',
                                        'else' => 'fail',
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
                2 => [
                    '$project' => [
                        '_id' => 0,
                        'month' => '$_id.month',
                        'year' => '$_id.year',
                        'total' => '$total',
                        'status' => '$status',
                    ],
                ],
                3 => [
                    '$sort' => [
                        'year' => 1,
                        'month' => 1,
                    ],
                ],
            ]);
            $recurringData = json_decode(json_encode( $recurringData->toArray()), true);
        }

        $first_time_fees = MPayment::where('ic', auth()->user()->ic)->where('returncode', '100')->sum('first_time_fees');
        $first_time_share = MPayment::where('ic', auth()->user()->ic)->where('returncode', '100')->sum('first_time_share');
        $additional_share = MPayment::where('ic', auth()->user()->ic)->where('returncode', '100')->sum('additional_share');

        $payments = MPayment::where('ic', Auth::user()->ic)->where(function ($query) {
            $query->where('channel', 'online')->orWhere('channel', 'manual')->orWhere('channel', 'ikoop');
        });

        if ($status) {
            $payments = $payments->where('status', $status)->orderBy('payment_date', 'desc')->paginate(5);
        } else {
            $payments = $payments->orderBy('payment_date', 'desc')->paginate(5);
        }
        return view('investor.invested', compact(
                'payments',
                'first_time_fees',
                'first_time_share',
                'additional_share',
                'recurringData'
            )
        );
    }

    public function invest_detail($id)
    {
        return view('investor.invested-detail');
    }

    public function statement()
    {
        return view('investor.wallet');
    }

    public function changepassword()
    {

        return view('investor.change-password');
    }

    public function changePasswordExe(Request $request)
    {

        $new_password = $request->input('new_password');
        $new_password_retype = $request->input('new_password_retype');

        if ($new_password == $new_password_retype) {

            $password = Hash::make($new_password);

            $token = md5(time());

            User::where('id', Auth::user()->id)->where('type', 'user')->update([
                'password_reset' => 0,
                'email_token' => $token,
                'password' => $password,
            ]);

            return redirect('/profile');
        } else {
            return back()->withErrors(['Kata Laluan Baru Anda Tidak Sah']);
        }
    }

    //    public function withdraw()
    //    {
    //
    //        $kempen = Course::where('created_by', Auth::id())->where('status_approval', '!=', 0)->get();
    //        $withdraw_amt = Withdraw::where('user_id', Auth::id())->where('status', 1)->sum('value');
    //
    //        $withdraws = Withdraw::where('user_id', Auth::id())->get();
    //
    //        return view('organisation.withdraw')->with('campaigns', $kempen)->with('withdraw_amt', $withdraw_amt)->with('withdraws', $withdraws);
    //
    //    }

    //    public function withdraw_exe(Request $request)
    //    {
    //        $withdraw_amount = $request->input('withdraw_amount');
    //
    //        if ($withdraw_amount > 500) {
    //            Withdraw::create([
    //                'user_id' => Auth::id(),
    //                'campaign_id' => 0,
    //                'value' => $withdraw_amount,
    //            ]);
    //
    //            return redirect('/withdraw')->with('success', 'Kemaskini Berjaya');
    //        } else {
    //            return redirect('/withdraw')->withErrors(['Tebusan mestilah melebihi RM 500 ke atas']);
    //        }
    //
    //
    //    }

    public function edit_org(Request $request)
    {

        $user = User::where('id', Auth::id())->first();

        return view('investor.profil')->with('user', $user);
    }

    public function edit_org_2(Request $request)
    {

        $user = User::where('id', Auth::id())->first();

        $payments = MPayment::where('ic', Auth::user()->ic)->orderby('payment_date', 'desc')->paginate(5);

        return view('investor.profile-redesign')->with('user', $user)->with('payments', $payments);
    }

    public function activation_email(Request $request)
    {

        $user = User::where('id', Auth::id())->first();
        $data = [
            'no-reply' => 'koppim@ppim.org.my',
            'reply-to' => 'koppim@ppim.org.my',
            'to' => $user->email,
            'to_name' => $user->name,
            'name' => $user->name,
            'email_token' => $user->email_token,
            'value' => '',
            'ref' => '',
        ];

        Mail::send(
            'email.email-activation',
            ['data' => $data],
            function ($message) use ($data) {
                $message->from($data['no-reply'], 'KoPPIM')->replyTo($data['reply-to'], 'KoPPIM')->to($data['to'], $data['to_name'])->subject('PENGESAHAN EMEL');
            }
        );

        return redirect('/profile')->with('success', 'Emel telah dihantar');
    }

    //    public function edit_org_info_jan_exe(Request $request)
    //    {
    //        $user_id = Auth::id();
    //
    //        $day = $request->input('org_dob_day');
    //        $month = $request->input('org_dob_month');
    //        $year = $request->input('org_dob_year');
    //
    //        if ($day == null || $month == null || $year == null) {
    //            $dob = null;
    //        } else {
    //            $dob = Carbon::parse('' . $year . '-' . $month . '-' . $day . ' 00:00:00');
    //        }
    //
    //        $user = User::where('id', $user_id)->update([
    //            'name' => strtoupper($request->input('org_name')),
    //            'sex' => $request->input('org_jantina'),
    //            'religion' => $request->input('org_agama'),
    //            'race' => $request->input('org_bangsa'),
    //            'marital_status' => $request->input('org_kahwin'),
    //            'dob' => $dob,
    //            'witness' => $request->input('org_saksi'),
    //
    //        ]);
    //
    //        return redirect('/profile')->with('success', 'Kemaskini Berjaya');
    //    }

    //    public function edit_org_info_exe(Request $request)
    //    {
    //        $user_id = Auth::id();
    //        $user = User::where('id', $user_id)->update([
    //            'address_line_1' => $request->input('org_address_line_1'),
    //            'address_line_2' => $request->input('org_address_line_2'),
    //            'address_line_3' => $request->input('org_address_line_3'),
    //            'postcode' => $request->input('org_poscode'),
    //            'city' => $request->input('org_city'),
    //            'state' => $request->input('org_state'),
    //            'parliament' => $request->input('org_parlimen'),
    //        ]);
    //
    //        return redirect('/profile')->with('success', 'Kemaskini Berjaya');
    //    }

    public function edit_org_change_password_exe(Request $request)
    {
        $user_id = Auth::id();
        $validator = Validator::make($request->all(), [
            'org_pass' => 'required|min:6',
        ]);

        if ($validator->fails()) {
            return redirect('/profile')->withErrors($validator);
        } else {
            $user = User::where('id', $user_id)->update([
                'password' => Hash::make($request->input('org_pass')),

            ]);

            return redirect('/profile')->with('success', 'Kemaskini Berjaya');
        }
    }

    public function edit_org_change_email_exe(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'email' => 'required|unique:users',
        ]);

        if ($validator->fails()) {
            return redirect('/profile')->withErrors($validator);
        } else {

            $token = md5(time());

            User::where('id', Auth::user()->id)->update([
                'email' => $request->input('email'),
                'email_verified_at' => null,
                'email_token' => $token,
            ]);

            return redirect('/profile')->with('success', 'Kemaskini Berjaya');
        }
    }

    //    public function edit_org_contact_exe(Request $request)
    //    {
    //        $user_id = Auth::id();
    //        User::where('id', $user_id)->update([
    //            'mobile_no' => $request->input('mobile_no'),
    //            'email' => $request->input('email'),
    //            'home_no' => $request->input('house_tel'),
    //
    //        ]);
    //
    //        return redirect('/profile')->with('success', 'Kemaskini Berjaya');
    //    }

    //    public function edit_org_employ_exe(Request $request)
    //    {
    //        $user_id = Auth::id();
    //        $user = User::where('id', $user_id)->update([
    //            'job' => $request->input('jawatan_pekerjaan'),
    //            'monthly_income' => $request->input('pendapatan_bulanan'),
    //        ]);
    //
    //        return redirect('/profile')->with('success', 'Kemaskini Berjaya');
    //    }

    //    public function edit_org_upload_exe(Request $request)
    //    {
    //        $user_id = Auth::id();
    //
    //        if ($request->hasFile('mycard_front') == true) {
    //
    //            $store = Storage::disk('do_spaces')->putFile(env('DO_SPACES_UPLOAD') . 'uploads/ic', request()->mycard_front, 'public');
    //
    //            $attac = User::where('id', $user_id)->first();
    //            Storage::disk('do_spaces')->delete($attac->mykad_front);
    //
    //            $user = User::where('id', $user_id)->update([
    //                'mykad_front' => $store,
    //
    //            ]);
    //        }
    //
    //        if ($request->hasFile('mycard_back') == true) {
    //
    //            $store = Storage::disk('do_spaces')->putFile(env('DO_SPACES_UPLOAD') . 'uploads/ic', request()->mycard_back, 'public');
    //
    //            $attac = User::where('id', $user_id)->first();
    //            Storage::disk('do_spaces')->delete($attac->mykad_back);
    //
    //            $user = User::where('id', $user_id)->update([
    //                'mykad_back' => $store,
    //
    //            ]);
    //        }
    //
    //        return redirect('/profile')->with('success', 'Kemaskini Berjaya');
    //    }

    //    public function edit_org_status_change_exe(Request $request)
    //    {
    //        $user_id = Auth::id();
    //
    //        $user = User::where('id', $user_id)->first();
    //
    //        $array = array();
    //
    //        if ($user->name == NULL || $user->sex == NULL || $user->race == NULL || $user->religion == NULL || $user->marital_status == NULL || $user->dob == NULL) {
    //            array_push($array, "INFO PEMOHON TIDAK LENGKAP");
    //
    //        }
    //        if ($user->mykad_front == NULL || $user->mykad_back == NULL) {
    //            array_push($array, "INFO MYKAD TIDAK LENGKAP");
    //
    //        }
    //        if ($user->address_line_1 == NULL || $user->postcode == NULL || $user->city == NULL || $user->state == NULL || $user->parliament == NULL) {
    //            array_push($array, "INFO ALAMAT TIDAK LENGKAP");
    //
    //        }
    //        if ($user->mobile_no == NULL || $user->email == NULL) {
    //            array_push($array, "INFO KONTAK TIDAK LENGKAP");
    //
    //        }
    //        if ($user->job == NULL || $user->monthly_income == NULL) {
    //            array_push($array, "INFO PEKERJAAN TIDAK LENGKAP");
    //
    //        }
    //        if ($user->bank_name == NULL || $user->bank_account == NULL) {
    //            array_push($array, "INFO PERBANKAN TIDAK LENGKAP");
    //
    //        }
    //        if ($user->heir_name == NULL || $user->heir_ic == NULL || $user->heir_phone == NULL || $user->heir_relationship == NULL) {
    //            array_push($array, "INFO WARIS TIDAK LENGKAP");
    //
    //        }
    //
    //        if (count($array) > 0) {
    //            return redirect('/profile')->withErrors($array);
    //        } else {
    //
    //            User::where('id', $user_id)->update([
    //                'status' => 2
    //            ]);
    //
    //            try {
    //
    //                $smsText = "Pendaftaran baharu anda berjaya dihantar. Sila tunggu emel pengesahan dalam tempoh 14 hari waktu bekerja. Terima kasih.";
    //
    //                $this->send_sms($user->mobile_no,$smsText);
    //
    //                \Mail::to($user->email)->send(new SentDeclarationProfile($user->name, $user->ic,$user->created_at));
    //
    //            } catch (Exception $e) {
    //                \Log::info($e->getMessage());
    //            }
    //
    //            return redirect('/home')->with('success', 'Pendaftaran baharu anda berjaya dihantar Sila tunggu emel pengesahan dalam tempoh 14 hari waktu bekerja. Terima kasih.');
    //        }
    //
    //
    //    }

    //    public function edit_org_bank_exe(Request $request)
    //    {
    //        $user_id = Auth::id();
    //
    //        //temp solution for null || "" select input
    //        $blacklist_dividend = $request->input('org_dividen');
    //        if ($request->input('org_dividen') == "" || $request->input('org_dividen') == null) {
    //            $blacklist_dividend = 0;
    //        }
    //        $blacklist = $request->input('org_blacklist');
    //        if ($request->input('org_blacklist') == "" || $request->input('org_blacklist') == null) {
    //            $blacklist = 0;
    //        }
    //
    //        $user = User::where('id', $user_id)->update([
    //            'bank_name' => $request->input('org_bank_name'),
    //            'bank_account' => $request->input('org_bank_acc'),
    //            'blacklist_dividend' => $blacklist_dividend,
    //            'blacklist' => $blacklist,
    //
    //        ]);
    //
    //        return redirect('/profile')->with('success', 'Kemaskini Berjaya');
    //    }
    //
    //    public function edit_org_waris_exe(Request $request)
    //    {
    //        $user_id = Auth::id();
    //
    //        $user = User::where('id', $user_id)->update([
    //            'heir_name' => $request->input('org_nama_waris'),
    //            'heir_ic' => $request->input('org_no_mykad_waris'),
    //            'heir_phone' => $request->input('org_no_tel_bimbit_waris'),
    //            'heir_relationship' => $request->input('org_hubungan'),
    //
    //        ]);
    //
    //        return redirect('/profile')->with('success', 'Kemaskini Berjaya');
    //    }

    public function view_org(Request $request)
    {
        $search = $request->input('search');

        if ($search) {

            $orgs = User::where('type', 'user')->where('name', 'LIKE', '%' . $search . '%')->orderBy('created_at', 'DESC')->paginate(10);

            return view('admin.org')->with('orgs', $orgs)->with('search', $search);
        } else {
            $orgs = User::where('type', 'user')->orderBy('created_at', 'DESC')->paginate(10);

            return view('admin.org')->with('orgs', $orgs)->with('search', $search);
        }
    }

    public function resetPassword()
    {

        return view('auth.passwords.change-password');
    }

    public function resetPasswordExe(Request $request)
    {

        $new_password = $request->input('new_password');
        $new_password_retype = $request->input('new_password_retype');

        if ($new_password == $new_password_retype) {

            $password = Hash::make($new_password);

            $token = md5(time());

            User::where('id', Auth::user()->id)->update([
                'email_token' => $token,
                'password' => $password,
            ]);

            return redirect('/profile')->with('success', 'Kata Laluan Anda Telah Berjaya Diset Semula');
        } else {
            return back()->withErrors(['Kata Laluan Baru Anda Tidak Sah']);
        }
    }

    public function terminate_membership(Request $request)
    {
        $status = User::where('id', Auth::user()->id)->get('status');
        return view('investor.terminate_membership')->with('status', $status);
    }

    public function terminate_membership_submit(Request $request)
    {
        $messages = [
            'leave_reason.required' => 'Alasan Pemberhentian Perlu Diisi',
            'terminate_letter.required' => 'Anda Perlu Memuat Naik Surat Permohonan Berhenti Untuk Memberhentikan Keanggotaan',
            'terminate_letter.mimes' => 'Surat Perlu Dimuat Naik Dalam Format .jpeg, .png, .doc, .docx atau .pdf',
            'terminate_letter.max' => 'Saiz Surat Yang Dimuat Naik Mestilah Tidak Melebihi 2MB',
        ];

        $validator = Validator::make($request->all(), [
            'leave_reason' => 'required',
            'terminate_letter' => 'required|mimes:jpeg,png,jpg,zip,,doc,docx,pdf|max:2048',
        ], $messages);

        if ($validator->fails()) {
            return redirect('/permohonan-berhenti')
                ->withErrors($validator)
                ->withInput();
        }

        $leave_reason = $request->input('leave_reason');

        if($request->hasFile('terminate_letter') == true) {
            $store = Storage::disk('do_spaces')->putFile(env('DO_SPACES_UPLOAD') . 'upload/termination_letter', request()->terminate_letter, 'public');

            User::where('id', Auth::user()->id)->update([
                'status' => 96,
                'status_reason' => 'Terminate Request',
            ]);

            $check = user_terminations::where('user_id', $request->input('user_id'))->first();
            if(!$check){
                $tmt_req = new user_terminations;
                $tmt_req->user_id = $request->input('user_id');
                $tmt_req->name = $request->input('name');
                $tmt_req->leave_reason = $leave_reason;
                $tmt_req->termination_letter = $store;
                $tmt_req->save();
            } else {
                $check->delete();
                $tmt_req = new user_terminations;
                $tmt_req->user_id = $request->input('user_id');
                $tmt_req->name = $request->input('name');
                $tmt_req->leave_reason = $leave_reason;
                $tmt_req->termination_letter = $store;
                $tmt_req->save();
            }
        }

        return redirect('/home')->with('success', 'Permohonan Berhenti Telah Dihantar');

    }

    public function reactivate_membership(Request $request)
    {
        $status = User::where('id', Auth::user()->id)->get('status');
        return view('investor.reactivate_membership')->with('status', $status);
    }

    public function reactivate_membership_submit(Request $request)
    {
        User::where('id', Auth::user()->id)->update([
            'status' => 1,
            'status_reason' => 'Member Reactivate',
        ]);

        return redirect('/home')->with('success', 'Akaun anda telah diaktifkan');

    }
}
