<?php

namespace App\Http\Controllers\Investor;

use App\Http\Controllers\Controller;
use App\Payment;
use App\Traits\SmsTrait;
use App\User;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Mail;
use Spatie\Activitylog\Models\Activity;

use App\Mongo\Dividen as Dividen;
use App\Mongo\DividenCalculation as DividenCalculation;
use App\Mongo\Payment as MPayment;
use App\Mongo\DividenWallet as DividenWallet;
use Storage;
use Validator;
use PDF;

class DividenController extends Controller
{


    public function __construct()
    {

        $this->middleware('auth');
        // $this->middleware('resetpassword');
        $this->middleware('firsttime');
    }


	public function announcement()
    {
	    return view('investor.maklumat-dividen');
	}

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */

	 public function index()
    {
	    $total_dividen = DividenWallet::where('ic',(int)Auth::user()->ic)->sum('amount');

	    $all_dividen = DividenWallet::where('ic',(int)Auth::user()->ic)->where('type','in')->sum('amount');


	    $statements = DividenWallet::where('ic',(int)Auth::user()->ic)->where('type','out')->orderBy('created_at','DESC')->get();

	    $dividen_cals = DividenCalculation::where('ic',(int)Auth::user()->ic)->orderBy('created_at','DESC')->get();

	    $start_date = Auth::user()->created_at;
	    $end_date = Carbon::now();
	    $diff = $start_date->diffInMonths($end_date);
        $dividen_static = config('dividen_edit.dividen_amount');

        /* This is for Temporary Malakat Eco Info Video Tutorial */
        return view('investor.dividen-temporary');

        /* This is for Dividen amount */
        // return view('investor.dividen')->with('total',$total_dividen)->with('all_dividen',$all_dividen)->with('statements',$statements)->with('dividen_cals',$dividen_cals)->with('withdraw_bool',$diff)->with('dividen_static',$dividen_static);
    }


	 public function dividen_statement_download()
    {
	    $statements = DividenWallet::where('ic',(int)Auth::user()->ic)->get();

	    $all_dividen = DividenWallet::where('ic',(int)Auth::user()->ic)->where('type','in')->sum('amount');

	    $share_amount = MPayment::where('ic',Auth::user()->ic)->where('status','payment-completed')->sum('share_amount');

	    	$address = Auth::user()->address_line_1;
	    	if(Auth::user()->address_line_2 != NULL)
	    	{
		    	$address .= "<br>".Auth::user()->address_line_2;
	    	}


	    	if(Auth::user()->address_line_3 != NULL)
	    	{
		    	$address .= "<br>".Auth::user()->address_line_3;
	    	}

	    	$address .= "<br>".Auth::user()->postcode." ".Auth::user()->city." <br>";

	    	$address .= Auth::user()->state;


	    	$data = [
		    	'name'=>Auth::user()->name,
		    	'no_anggota'=>Auth::user()->no_koppim,
		    	'kp'=>Auth::user()->ic,
		    	'address'=>$address,
		    	'statements'=>$statements,
		    	'share_amount'=>$share_amount,
		    	'dividen'=>$all_dividen

	    	];



		    $pdf = PDF::loadView('dividen.dividen-statement-pdf', $data)->setOptions(
		    [
		    'dpi' => 150,
		    'defaultFont' => 'arial',
		    'isRemoteEnabled'=>true,
		    'fontDir'=>''.storage_path("fonts/").'',
		    'fontCache'=>''.storage_path("fonts/").'',
		    'isFontSubsettingEnabled'=>true
		    ]);
			return $pdf->download('penyata-tahunan-'.Auth::user()->ic.'.pdf');






			//return view('dividen.dividen-statement-pdf')->with('statements',$statements);

	}

    public function withdraw()
    {

	    $total_dividen = DividenWallet::where('ic',(int)Auth::user()->ic)->sum('amount');

	    $start_date = Auth::user()->created_at;
	    $end_date = Carbon::now();
	    $diff = $start_date->diffInMonths($end_date);

	    return view('investor.dividen-pengeluaran')->with('total',$total_dividen)->with('withdraw_bool',$diff);
    }



    public function withdraw_exe()
    {

	    $total_dividen = DividenWallet::where('ic',(int)Auth::user()->ic)->sum('amount');
	    if($total_dividen < 10)
	    {
		    return redirect('/dividen/withdraw')->with('error','Pengeluaran  minima adalah RM 10.00');
	    }
	    else
	    {
		    DividenWallet::create([
			    'ic'=>(int)Auth::user()->ic,
			    'type'=>'out',
			    'amount'=>-$total_dividen,
			    'calculation_id'=>null,
			    'dividen_id'=>null,
			    'from'=>null,
			    'to'=>null,
			    'dividen_percentage'=>null,
			    'status'=>'sedang-disemak',
			    'bank'=>Auth::user()->bank_name,
			    'account_no'=>Auth::user()->bank_account,
			    'account_name'=>Auth::user()->bank_account_name,
			    'status'=>'sedang-disemak',
		    ]);


		    return redirect('/dividen')->with('success','Berjaya. Permohanan anda sedang disemak');
	    }


    }

    public function export(Request $request){
        if($request->status){
            $status = $request->status;
        } else {
            $status = 'all';
        }

        return (new WithdrawalExport($status))->download('pengeluaran.xlsx');
    }


}
