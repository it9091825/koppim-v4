<?php


namespace App\Http\Controllers;


use Afiqiqmal\Rpclient\RaudhahPay as RaudhahPayClient;
use Afiqiqmal\Rpclient\RPay\RPBillDetail;
use GuzzleHttp\Client;

class RaudhahPay
{
    private $raudhahPayClient = null;

    private $products = [];
    /**
     * @var string
     */
    private $firstName;
    /**
     * @var string
     */
    private $address;
    /**
     * @var string
     */
    private $lastName;
    /**
     * @var string
     */
    private $email;
    /**
     * @var string
     */
    private $phoneNumber;
    private $due;
    private $ref1;
    private $rbill;
    private $ref2;
    private $recurring_type;
    private $currency;


    public function createRecurringBill($due, $ref1, $ref2, $rbill, $recurring_type, $currency)
    {
        $this->due = $due;
        $this->ref1 = $ref1;
        $this->ref2 = $ref2;
        $this->rbill = $rbill;
        $this->recurring_type = $recurring_type;
        $this->currency = $currency;

        return $this;
    }

    public function setProduct($product, $quantity, $recurred = false)
    {
        $this->products[] = [
            'id' => $product['body']['id'],
            'enabled_autocharge' => $recurred,
            'quantity' => $quantity
        ];

        return $this;
    }

    public function setRCustomer($firstName, $lastName, $email, $phoneNumber, $address)
    {
        $this->firstName = trim($firstName);
        $this->lastName = trim($lastName);
        $this->email = trim($email);
        $this->phoneNumber = trim($phoneNumber);
        $this->address = trim($address);

        return $this;
    }

    public function getProducts() {
        return $this->products;
    }

    public function getOutput() {

        $client = \Afiqiqmal\Rpclient\RaudhahPay::make();

        return $client->getClient()->urlSegment('/v2/collections/'.config('raudhahpay.collection_code').'/recurring')
            ->postMethod()
            ->setRequestBody($this->buildPaymentDetail())
            ->fetch()
            ->output();
    }

    private function buildPaymentDetail() : array
    {
        return [
            'due' => $this->due,
            'ref1' => $this->ref1,
            'ref2' => $this->ref2,
            'rbill_date' => $this->rbill,
            'recurring' => $this->recurring_type,
            'currency' => $this->currency,
            'customer' => [
                'first_name' => $this->firstName,
                'last_name' => $this->lastName,
                'address' => $this->address,
                'email' => $this->email,
                'mobile' => $this->phoneNumber,
            ],
            'product' => $this->products
        ];
    }
}
