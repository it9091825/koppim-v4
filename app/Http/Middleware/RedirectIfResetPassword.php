<?php

namespace App\Http\Middleware;

use Auth;
use Closure;

class RedirectIfResetPassword
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        // if (Auth::user()->password_reset == 1) {

        //     $user = Auth::user();
        //     $user->password_reset = 0;
        //     $user->save();

        //     // if ($request->is('set-kata-laluan-baru')) {

        //     // } else {
        //         return redirect('/set-kata-laluan-baru');
        //     // }

        // }

        return $next($request);
    }
}
