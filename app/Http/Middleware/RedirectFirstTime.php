<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectFirstTime
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {

        if (Auth::user()->password_reset == 1) {

            $user = Auth::user();

            $user->password_reset = 0;
            $user->save();

            return redirect('/set-kata-laluan-baru');

        }else if (Auth::user()->status == 0 ) {

            if ($request->is('home') || $request->is('dashboard') || $request->is('statement')) {
                return redirect('/profile')->with('firsttime', 'Sila Lengkapkan Maklumat Permohonan Dan Tekan Butang Hantar');
            }

        }

//        if (Auth::user()->status == 0) {
//
//            if ($request->is('change-password')) {
//
//            } else {
//                return redirect('/change-password');
//            }
//
//        } else if (Auth::user()->status == 96) {
//
//            if ($request->is('home') || $request->is('statement') || $request->is('invested')) {
//                return redirect('/profile')->with('firsttime', 'Sila Lengkapkan Maklumat Permohonan Dan Tekan Butang Hantar');
//            }
//
//        }

        return $next($request);
    }
}
