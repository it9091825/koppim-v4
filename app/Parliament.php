<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Parliament extends Model
{
    protected $table = 'parliaments';

    protected $timestamp = false;

    public function states()
    {
        return $this->belongsTo(State::class);
    }
}
