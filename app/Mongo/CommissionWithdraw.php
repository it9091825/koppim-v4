<?php

namespace App\Mongo;

use Jenssegers\Mongodb\Eloquent\Model;
use App\Traits\SortableMongo;

class CommissionWithdraw extends Model
{
    use SortableMongo;

    protected $connection = 'mongodb';

	protected $collection = 'affiliate_commission_withdraws';

    protected $dates = ['created_at', 'updated_at','admin_datetime', 'statement_date'];

    protected $guarded = [];

    public $sortable = [
        'name','created_at'
    ];

    public function payments(){
        return $this->hasMany(\App\Mongo\Payment::class,'affiliate_commission_withdraw_id');
    }

}
