<?php

namespace App\Mongo;

use Jenssegers\Mongodb\Eloquent\Model;

class Dividen extends Model
{
    protected $connection = 'mongodb';

	protected $collection = 'dividen';

    protected $dates = ['created_at', 'updated_at','from','to','agm'];

    protected $guarded = [];


}
