<?php

namespace App\Mongo;

use Jenssegers\Mongodb\Eloquent\Model;

class LetterJobItems extends Model
{
    protected $connection = 'mongodb';

	protected $collection = 'letter_job_items';

    protected $dates = ['created_at', 'updated_at'];

    protected $guarded = [];


}
