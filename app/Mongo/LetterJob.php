<?php

namespace App\Mongo;

use Jenssegers\Mongodb\Eloquent\Model;

class LetterJob extends Model
{
    protected $connection = 'mongodb';

	protected $collection = 'letter_job';

    protected $dates = ['created_at', 'updated_at'];

    protected $guarded = [];


}
