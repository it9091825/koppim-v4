<?php

namespace App\Mongo;

use App\Traits\SortableMongo;
use Jenssegers\Mongodb\Eloquent\Model;

class PaymentBak extends Model
{
   use SortableMongo;

    protected $connection = 'mongodb';

    protected $collection = 'payments_bak';

    protected $dates = ['created_at', 'updated_at'];

    protected $guarded = [];
}
