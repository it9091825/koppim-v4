<?php

namespace App\Mongo;

use Jenssegers\Mongodb\Eloquent\Model;

class DividenCalculation extends Model
{
    protected $connection = 'mongodb';

	protected $collection = 'dividen_calculation';

    protected $dates = ['created_at', 'updated_at','from','to'];

    protected $guarded = [];


}
