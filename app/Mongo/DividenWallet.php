<?php

namespace App\Mongo;

use Jenssegers\Mongodb\Eloquent\Model;

class DividenWallet extends Model
{
    protected $connection = 'mongodb';

	protected $collection = 'dividen_wallet';

    protected $dates = ['created_at', 'updated_at','from','to'];

    protected $guarded = [];


}
