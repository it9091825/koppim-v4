<?php

namespace App\Mongo;

use Jenssegers\Mongodb\Eloquent\Model;

class Smslog extends Model
{
    protected $connection = 'mongodb';

    protected $dates = ['created_at', 'updated_at', 'sms_send'];

    protected $guarded = [];
}
