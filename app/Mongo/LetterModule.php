<?php

namespace App\Mongo;

use Jenssegers\Mongodb\Eloquent\Model;

class LetterModule extends Model
{
    protected $connection = 'mongodb';

	protected $collection = 'letter_module';

    protected $dates = ['created_at', 'updated_at'];

    protected $guarded = [];


}
