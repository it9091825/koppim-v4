<?php

namespace App\Mongo;

use App\Traits\SortableMongo;
use Jenssegers\Mongodb\Eloquent\Model;

class Payment extends Model
{
    use SortableMongo;

    protected $connection = 'mongodb';

    protected $dates = ['created_at', 'updated_at', 'payment_date', 'verified_date', 'approved_date', 'introducer_status_change_datetime', 'introducer_commission_date'];

    protected $guarded = [];

    public $sortable = [
        'created_at','name'
    ];

    public function user()
    {
        return $this->belongsTo(\App\User::class);
    }

    public function withdraw()
    {
        return $this->belongsTo(\App\Mongo\CommissionWithdraw::class);
    }

    public function verified_user()
    {
        return $this->belongsTo(\App\User::class,'verified_by');
    }

    public function approved_user()
    {
        return $this->belongsTo(\App\User::class,'approved_by');
    }
}
