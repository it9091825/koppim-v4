<?php

namespace App\Mongo;

use Jenssegers\Mongodb\Eloquent\Model;

class AffiliateLinkVisitor extends Model
{
    protected $connection = 'mongodb';

	protected $collection = 'affiliate_link_visitors';

    protected $dates = ['created_at', 'updated_at'];

    protected $guarded = [];


}
