<?php

namespace App\Imports;

use App\Mongo\Payment;
use App\Traits\OrderTrait;
use App\User;
use DateTime;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Support\Collection;
use Symfony\Component\Console\Output\ConsoleOutput;

class PaymentImport implements ToCollection, WithHeadingRow
{
    use OrderTrait;
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function collection(Collection $rows)
    {
        $output = new ConsoleOutput();

        foreach ($rows as $row) {
            $existingUser = User::where('email', $row['email'])->first();

            if ($existingUser == null) {
                $output->writeln("<error>User not found, " . $row['email'] . "</error>");
                $existingUser = User::whereIn('mobile_no', [$row['mobile'], substr($row['mobile'], 1)])->first();
            }

            if ($existingUser == null) {
                $output->writeln("<error>User not found, " . $row['mobile'] . ", " . substr($row['mobile'], 1) . "</error>");
                continue;
            }

            $contains30 = $this->isAmountContainsRegistrationFee($row['total_payment']);
            $existingPayment = ($contains30) ?
                Payment::where('email', $row['email'])
                ->whereIn('amount', [(float) $row['total_payment'], ((float) $row['total_payment']) - 30.0])
                ->where('channel', 'ikoop')
                ->where('status', 'payment-completed')
                ->first() : null;

            if ($contains30 && $existingPayment != null) {
                $existingPayment->amount = (float) $row['total_payment'];
                $existingPayment->first_time_share = 100.0;
                $existingPayment->first_time_fees = 30.0;
                $existingPayment->share_amount = (float) $row['total_payment'] - $existingPayment->first_time_share - $existingPayment->first_time_fees;
                $existingPayment->additional_share = $existingPayment->share_amount - $existingPayment->firstTimeShare;
                $existingPayment->payment_date = new \MongoDB\BSON\UTCDateTime(new DateTime($row["created_at"]));
                $existingPayment->created_at = new \MongoDB\BSON\UTCDateTime(new DateTime($row["created_at"]));
                $existingPayment->save();
                $output->writeln("Payment already exists, " . $row['email'] . " amount: " . $row['total_payment'] . ", updating payment date");
                continue;
            }

            $existingIKoopPayment = Payment::where('email', $row['email'])
                ->where('amount', (float) $row['total_payment'])
                ->where('channel', 'ikoop')
                ->where('status', 'payment-completed')
                ->first();

            if ($existingIKoopPayment != null) {
                $existingIKoopPayment->payment_date = new \MongoDB\BSON\UTCDateTime(new DateTime($row["created_at"]));
                $existingIKoopPayment->created_at = new \MongoDB\BSON\UTCDateTime(new DateTime($row["created_at"]));
                $existingIKoopPayment->save();
                $output->writeln("Payment already exists, " . $row['email'] . " amount: " . $row['total_payment'] . ", updating payment date");
                continue;
            }

            // $existingFirstTimePayment = Payment::where('email', $row['email'])
            //     ->where('first_time_fees', 30.0)
            //     ->where('status', 'payment-completed')
            //     ->first();

            // $orderRef = $this->create_order_ref($existingUser->ic);

            // $firstTimePayment = (float) (($existingFirstTimePayment != null) ? 0.0 : 30.0);
            // $firstTimeShare = (float) (($existingFirstTimePayment != null) ? 0.0 : 100.0);
            // $shareAmount = (float) $row['total_payment'] - $firstTimePayment;
            // $additionalShare = (float) $shareAmount - $firstTimeShare;

            // Payment::firstOrCreate(
            //     [
            //         "user_id" => $existingUser->id,
            //         "ic" => $existingUser->ic,
            //         "gateway" => "raudhahpay",
            //         "ord_key" => $row['ref_id'],
            //     ],
            //     [
            //         "name" => strtoupper($existingUser->name),
            //         "phone" => $existingUser->mobile_no,
            //         "email" => $row["email"],
            //         "amount" => $row["total_payment"],
            //         "first_time_fees" => $firstTimePayment,
            //         "share_amount" => $shareAmount,
            //         "first_time_share" => $firstTimeShare,
            //         "additional_share" => $additionalShare,
            //         "status" => "payment-completed",
            //         "type" => "one-time",
            //         "order_ref" => $orderRef,
            //         "payment_desc" => ($firstTimePayment > 0.0) ? "Pendaftaran Anggota Baharu Dan Langganan Modal Syer" : (($firstTimeShare >= 0.) ? "Langganan Modal Syer" : "Langganan Modal Tambahan"),
            //         "gateway_tx" => [
            //             [
            //                 "order_id" => $orderRef,
            //                 "confirmation_no" => $row['ref_id'],
            //                 "msg" => "Payment Payload",
            //                 "status" => "4",
            //                 "txn_type" => "one-time"
            //             ]
            //         ],
            //         "payment_date" =>  new \MongoDB\BSON\UTCDateTime(new DateTime($row["created_at"])),
            //         "created_at" =>  new \MongoDB\BSON\UTCDateTime(new DateTime($row["created_at"])),
            //         "updated_at" =>  new \MongoDB\BSON\UTCDateTime(new DateTime($row["created_at"])),
            //         "payment_proof" => "/payment/receipt/" . $row['ref_id'],
            //         "insert_ref" => "2019_data"
            //     ]
            // );

            // $output->writeln("<info>Payment inserted, email: " . $row['email'] . "</info>");
        }
    }

    public function isAmountContainsRegistrationFee($amount)
    {
        $stringAmount = strval($amount);

        return "30.00" == substr($stringAmount, strlen($stringAmount) - 5, 5);
    }
}
