<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class UserSmsReminderLog extends Model
{
    protected $table = 'user_sms_reminder_log';

    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
