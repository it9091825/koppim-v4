<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class IkoopTwo extends Model
{
    protected $table = 'ikoops_two';
    protected $guarded = [];
    public $timestamps = false;
}
