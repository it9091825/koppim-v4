<?php

namespace App;

use App\Traits\Filterable;
use App\Traits\HasPermissionsTrait;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
//use Spatie\Activitylog\Traits\LogsActivity;
use Jenssegers\Mongodb\Eloquent\HybridRelations;
use Spatie\ModelStatus\HasStatuses;
use Kyslik\ColumnSortable\Sortable;
use App\UserSmsReminderLog;

class User extends Authenticatable
{
    //use LogsActivity;
    use HasPermissionsTrait, HybridRelations, Sortable, Filterable, Notifiable;

    protected $connection = 'mysql';
    protected $table = 'users';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    public $sortable = [
        'name', 'created_at', 'approved_date'
    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    //taktau kenapa fahmi padam ni.
    protected $hidden = [
        'password', 'email_token'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    //    fahmi punya
    //    protected $casts = [
    //        'email_verified_at' => 'datetime',
    //        'start' => 'datetime',
    //        'end' => 'datetime',
    //        'dob' => 'datetime',
    //    ];
    protected $casts = [
        'email_verified_at' => 'datetime',
        'dob' => 'datetime',
        'approved_date' => 'datetime',
        'affiliate_apply' => 'datetime',
        'affiliate_apply_result' => 'datetime',
    ];

    public function payments()
    {
        return $this->hasMany(\App\Mongo\Payment::class, 'user_id');
    }

    public function commission_withdraws()
    {
        return $this->hasMany(\App\Mongo\CommissionWithdraw::class);
    }

    public function downlines()
    {
        return $this->hasMany(\App\Mongo\Payment::class, 'introducer_id')->where('status','payment-completed');
    }

    public function affiliate_setting()
    {
        return $this->hasOne('App\UserAffiliateSetting');
    }

    public function sms_reminder_logs()
    {
        return $this->hasMany(UserSmsReminderLog::class);
    }
}
