<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Payment;
use App\Mongo\Payment as MPayment;

class MigratePayments extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mpayment:migrate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Migrate mysql payments table into mongodb payments collection';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $paymentsCount = Payment::all();
        $bar = $this->output->createProgressBar(count($paymentsCount));
        $bar->start();

        $payments = Payment::chunkById(100, function ($payments) use ($bar) {
            foreach ($payments as $payment) {
                $mpayment = new MPayment;
                $mpayment->from_mysql = $payment->id;
                $mpayment->name = $payment->name;
                $mpayment->ic = $payment->ic;
                $mpayment->phone = $payment->phone;
                $mpayment->email = $payment->email;
                $mpayment->amount = (double)$payment->amount;
                $mpayment->share_amount = (double)$payment->share_amount;
                $mpayment->first_time_fees = (double)$payment->first_time_fees;
                if ($payment->first_time_fees > 0.00) {
                    if ($payment->share_amount > 100.00) {
                        $mpayment->first_time_share = 100.00;
                        $mpayment->additional_share = (double)$payment->share_amount - 100.00;
                    } elseif ($payment->share_amount > 0.00) {
                        $mpayment->first_time_share = (double)$payment->share_amount;
                        $mpayment->additional_share = 0.00;
                    } else {
                        $mpayment->first_time_share = 0.00;
                        $mpayment->additional_share = 0.00;
                    }
                    $mpayment->payment_desc = 'Pendaftaran Anggota Baharu Dan Langganan Modal Syer';
                } else {
                    $mpayment->first_time_share = 0.00;
                    $mpayment->additional_share = (double)$payment->share_amount;
                    $mpayment->payment_desc = 'Langganan Modal Tambahan';
                }

                $mpayment->status = $payment->status;
                $mpayment->wcID = $payment->wcID;
                $mpayment->ord_key = (string)$payment->ord_key;
                $mpayment->returncode = (string)$payment->returncode;
                if ($payment->gateway == 'senangpay') {
                    $mpayment->channel = 'online';
                    $mpayment->gateway = 'senangpay';
                    $mpayment->senang_pay_hash = $payment->senang_pay_hash;
                    $mpayment->order_ref = $payment->order_ref;
                } else {
                    $mpayment->channel = 'ikoop';
                    $mpayment->order_ref = 'ikoop-'.$payment->id;
                }
                $mpayment->type = $payment->type;
                $mpayment->created_at = $payment->created_at;
                $mpayment->updated_at = $payment->updated_at;
                $mpayment->save();

                if ($payment->first_time_fees > 0.00) {

                    $mpayment->push('items', [
                        'item' => 'Fi Pendaftaran Anggota',
                        'amount' => (double)$payment->first_time_fees,
                    ]);

                    $mpayment->push('items', [
                        'item' => 'Jumlah Syer',
                        'amount' => (double)$payment->share_amount,
                    ]);

                    if ($payment->share_amount > 100.00) {
                        $mpayment->push('items', [
                            'item' => 'Langganan Modal Syer',
                            'amount' => 100.00,
                        ]);

                        $mpayment->push('items', [
                            'item' => 'Langganan Tambahan',
                            'amount' => (double)$payment->share_amount - 100.00,
                        ]);
                    } elseif ($payment->share_amount > 0.00) {
                        $mpayment->first_time_share = (double)$payment->share_amount;
                        $mpayment->additional_share = 0.00;
                        $mpayment->push('items', [
                            'item' => 'Langganan Modal Syer',
                            'amount' => (double)$payment->share_amount,
                        ]);

                        $mpayment->push('items', [
                            'item' => 'Langganan Tambahan',
                            'amount' => 0.00,
                        ]);
                    } else {
                        $mpayment->push('items', [
                            'item' => 'Langganan Modal Syer',
                            'amount' => 0.00,
                        ]);

                        $mpayment->push('items', [
                            'item' => 'Langganan Tambahan',
                            'amount' => 0.00,
                        ]);
                    }

                } else {
                    $mpayment->push('items', [
                        'item' => 'Jumlah Syer',
                        'amount' => (double)$payment->share_amount,
                    ]);

                    $mpayment->push('items', [
                        'item' => 'Langganan Tambahan',
                        'amount' => (double)$payment->share_amount,
                    ]);
                }
                $bar->advance();
            }
        });
        $bar->finish();
    }
}
