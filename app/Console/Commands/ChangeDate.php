<?php

namespace App\Console\Commands;

use DateTime;
use App\Mongo\PaymentBak;
use Illuminate\Console\Command;
use App\Mongo\Payment as MPayment;

class ChangeDate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'change:paymentdate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Change ikoop payment date';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        //then update date
        MPayment::where('channel', 'ikoop')
        ->update([
            'created_at' => new \MongoDB\BSON\UTCDateTime(new DateTime('2020-06-11 02:01:09.144967')),
            'payment_date' => new \MongoDB\BSON\UTCDateTime(new DateTime('2020-06-11 02:01:09.144967'))
        ]);

        echo 'successfully change all ikoop payment date to june 2020';

    }
}
