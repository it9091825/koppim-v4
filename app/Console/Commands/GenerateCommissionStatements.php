<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use App\Mongo\Payment;
use App\Mongo\CommissionWithdraw;
use Hashids\Hashids;
use Storage;
use PDF;
use Carbon\Carbon;

class GenerateCommissionStatements extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'commission:generate-statement';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Affiliate auto generate commission statement every first day of every month for last month';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $users = User::where('status', 1)->where('is_affiliate','yes')->get();

        foreach($users as $user){
            $balance = Payment::where('introducer_no_anggota', (string)$user->no_koppim)
                ->where('returncode','100')
                ->where('introducer', true)
                ->where('introducer_commission_status','available')
                ->sum('introducer_commission');

            $hashids = new Hashids('withdrawal', 0, 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789');
            $ids = round(microtime(true) * 1000);
            $reference_no = 'AF-' . $hashids->encode((integer)$ids);
            $end = new Carbon('last day of last month');

            if ($balance && $balance > 0.00 ) {

                $withdraw = CommissionWithdraw::create([
                    'no_anggota' => (string)$user->no_koppim,
                    'name' => (string)$user->name,
                    'user_id' => (int)$user->id,
                    'ic' => (string)$user->ic,
                    'amount' => (float)$balance,
                    'bank' => $user->bank_name,
                    'account_name' => $user->bank_account_name,
                    'account_no' => $user->bank_account,
                    'status' => 'processing',
                    'statement_date' => $end,
                    'statement_month' => $end->month,
                    'statement_year' => $end->year,
                    'reference_no' => $reference_no
                ]);

                Payment::where('introducer_no_anggota', (string)$user->no_koppim)
                    ->where('returncode', '100')
                    ->where('introducer', true)
                    ->where('introducer_commission_status', 'available')
                    ->update(['introducer_commission_status' => 'processing', 'affiliate_commission_withdraw_id' => $withdraw->id]);

            } else {

                $withdraw = CommissionWithdraw::create([
                    'no_anggota' => (string)$user->no_koppim,
                    'name' => (string)$user->name,
                    'user_id' => (int)$user->id,
                    'ic' => (string)$user->ic,
                    'amount' => 0.00,
                    'bank' => $user->bank_name,
                    'account_name' => $user->bank_account_name,
                    'account_no' => $user->bank_account,
                    'status' => 'paid',
                    'payment_ref' => 'Tiada',
                    'statement_date' => $end,
                    'statement_month' => $end->month,
                    'statement_year' => $end->year,
                    'reference_no' => $reference_no
                ]);
            }

//            $data = [
//                'statement' => $withdraw
//            ];

//            $pdf = PDF::loadView('affiliates.print-statement',$data);
//            $commission_statement = Storage::disk('do_spaces')->put(env('DO_SPACES_UPLOAD') . 'uploads/commissionstatement/'.$withdraw->reference_no.'.pdf', $pdf->output(), 'public');
//            $withdraw->update([
//                'commission_statement' => $commission_statement
//            ]);

        }
    }
}
