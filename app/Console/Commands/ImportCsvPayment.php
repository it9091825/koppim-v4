<?php

namespace App\Console\Commands;

use App\Imports\PaymentImport;
use Illuminate\Console\Command;
use Maatwebsite\Excel\Facades\Excel;

class ImportCsvPayment extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:payment {file_name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import Csv Payment';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info("Import Started");
        $fileName = $this->argument("file_name");
        Excel::import(new PaymentImport, base_path('resources/import_csv/' . $fileName));
        $this->info("Import Ended");
    }
}
