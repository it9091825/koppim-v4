<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use App\Mongo\Payment;

class AddUserIdToPayment extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mpayment:add-user-id';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'add user_id to mongodb payments collection if there is no user_id found, based on IC.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $payments = Payment::whereNull('user_id')->get();
        $bar = $this->output->createProgressBar(count($payments));
        $bar->start();

        foreach($payments as $payment){
            $user = User::where('ic',$payment->ic)->first();
            if($user){
                $payment->user_id = $user->id;
                $payment->save();
            }

            $bar->advance();
        }

        $bar->finish();
    }
}
