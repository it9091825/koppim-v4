<?php

namespace App\Console\Commands;

use App\IkoopTwo;
use App\Mongo\Payment as MPayment;
use App\User;
use App\UserDuplicate;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Hash;

class MigrateIkoopTwo extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ikoop:migrate-two';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Migrate ikoops_two table into users table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $ikoopsCount = IkoopTwo::all();
        $bar = $this->output->createProgressBar(count($ikoopsCount));
        $bar->start();

        $ikoops = IkoopTwo::chunkById(100, function ($ikoops) use ($bar) {
            foreach ($ikoops as $ikoop) {

                //Duplicate IC
                $checkIC = User::where('ic', $ikoop->newIC)->first();
                if ($checkIC) {
                    $reason = 'No. Mykad sudah didaftarkan';
                    $dup = $this->storeDuplicate($ikoop, $reason);
                    continue;
                }

                //IC less than 12 digits
//                if (strlen($ikoop->newIC) < 12) {
//                    $reason = 'No. Mykad kurang dari 12 digit';
//                    $dup = $this->storeDuplicate($ikoop, $reason);
//                    continue;
//                }

                //IC not number
                if (!is_numeric($ikoop->newIC)) {
                    $reason = 'No. Mykad bukan digit';
                    $dup = $this->storeDuplicate($ikoop, $reason);
                    continue;
                }

//                //mobileNo not number
//                if (!is_numeric($ikoop->mobileNo)) {
//                    $reason = 'No. Telefon bukan digit';
//                    $dup = $this->storeDuplicate($ikoop, $reason);
//                    continue;
//                }
//
//                //mobileNo null or empty
//                if ($ikoop->mobileNo == '' || $ikoop->mobileNo == null) {
//                    $reason = 'No. Telefon kosong';
//                    $dup = $this->storeDuplicate($ikoop, $reason);
//                    continue;
//                }

                $password = Hash::make(rand(11111111, 99999999));
                if (strtolower($ikoop->status) == 'diluluskan') {
                    $status = 1;
                } elseif (strtolower($ikoop->status) == 'dalam proses') {
                    $status = 0;
                } else {
                    $status = 0;
                }

                if ($ikoop->approvedDate == '' || $ikoop->approvedDate == '00/00/0000') {
                    $approved_date = null;
                } else {
                    $approved_date = date('Y-m-d 00:00:00', strtotime(str_replace("/", "-", $ikoop->approvedDate)));
                }


                $user = new User;
                $user->name = strtoupper($ikoop->name);
                $user->password = $password;
                $user->ic = $ikoop->newIC;
                $user->email = $ikoop->email;
                $user->mobile_no = $ikoop->mobileNo;
                $user->type = 'user';
                $user->postcode = $ikoop->postcode;
                $user->city = $ikoop->city;
                $user->parliament = $ikoop->cawangan;
                $user->heir_name = $ikoop->w_name_1;
                $user->heir_ic = $ikoop->w_ic1;
                $user->heir_relationship = $ikoop->w_relation1;
                $user->ikoop_id = $ikoop->ikoopId;
                $user->ikoop = 1;
                $user->status = $status;
                $user->job = $ikoop->job;
                $user->home_no = $ikoop->homeNo;
                $user->monthly_income = $ikoop->grossPay;
                $user->staff_no = $ikoop->staffNo;
                $user->witness = $ikoop->saksi1;
                $user->no_koppim = $ikoop->ikoopId;
                $user->password_reset = 1;
                $user->registration_type = 'ikoop';

                if ($approved_date != null) {
                    $user->approved_date = $approved_date;
                }
                $user->save();

                if ($ikoop->yuran != "-") {
                    $fee = str_replace("RM ", "", $ikoop->yuran);
                    $fee = str_replace(",", "", $fee);
                    $first_time_fees = (double)$fee;
                } else {
                    $first_time_fees = 0.00;
                }

                if ($ikoop->sahamTerkumpul != "-") {
                    $share = str_replace("RM ", "", $ikoop->sahamTerkumpul);
                    $share = str_replace(",", "", $share);
                    $share_amount = (double)$share;
                } else {
                    $share_amount = 0.00;
                }

                if ($ikoop->yuran != "-" && $ikoop->sahamTerkumpul != "-") {
                    //Create payment
                    $mpayment = new MPayment;
                    $mpayment->name = $user->name;
                    $mpayment->ic = $user->ic;
                    $mpayment->phone = $user->mobile_no;
                    $mpayment->email = $user->email;

                    $mpayment->amount = (double)$first_time_fees + (double)$share_amount;
                    $mpayment->share_amount = (double)$share_amount;
                    $mpayment->first_time_fees = (double)$first_time_fees;

                    if ((double)$share_amount > 100.00) {
                        $mpayment->first_time_share = 100.00;
                        $mpayment->additional_share = (double)$share_amount - 100.00;
                    } elseif ((double)$share_amount > 0.00) {
                        $mpayment->first_time_share = (double)$share_amount;
                        $mpayment->additional_share = 0.00;
                    } else {
                        $mpayment->first_time_share = 0.00;
                        $mpayment->additional_share = 0.00;
                    }
                    $mpayment->payment_desc = 'Migrasi Dari Sistem Ikoop';


                    $mpayment->status = "payment-completed";
                    $mpayment->wcID = null;
                    $mpayment->ord_key = "";
                    $mpayment->returncode = "100";

                    $mpayment->channel = 'ikoop';
                    $mpayment->order_ref = 'ikoop-' . $ikoop->ikoopId;

                    $mpayment->type = "one-time";
//                    $mpayment->created_at = $payment->created_at;
//                    $mpayment->updated_at = $payment->updated_at;
                    $mpayment->save();

                    if ($mpayment->first_time_fees > 0.00) {

                        $mpayment->push('items', [
                            'item' => 'Fi Pendaftaran Anggota',
                            'amount' => (double)$mpayment->first_time_fees,
                        ]);

                        $mpayment->push('items', [
                            'item' => 'Jumlah Syer',
                            'amount' => (double)$mpayment->share_amount,
                        ]);

                        if ($mpayment->share_amount > 100.00) {
                            $mpayment->push('items', [
                                'item' => 'Langganan Modal Syer',
                                'amount' => 100.00,
                            ]);

                            $mpayment->push('items', [
                                'item' => 'Langganan Tambahan',
                                'amount' => (double)$mpayment->share_amount - 100.00,
                            ]);
                        } elseif ($mpayment->share_amount > 0.00) {

                            $mpayment->push('items', [
                                'item' => 'Langganan Modal Syer',
                                'amount' => (double)$mpayment->share_amount,
                            ]);

                            $mpayment->push('items', [
                                'item' => 'Langganan Tambahan',
                                'amount' => 0.00,
                            ]);
                        } else {
                            $mpayment->push('items', [
                                'item' => 'Langganan Modal Syer',
                                'amount' => 0.00,
                            ]);

                            $mpayment->push('items', [
                                'item' => 'Langganan Tambahan',
                                'amount' => 0.00,
                            ]);
                        }

                    } else {
                        $mpayment->push('items', [
                            'item' => 'Jumlah Syer',
                            'amount' => (double)$mpayment->share_amount,
                        ]);

                        $mpayment->push('items', [
                            'item' => 'Langganan Tambahan',
                            'amount' => (double)$mpayment->share_amount,
                        ]);
                    }

                }
                $bar->advance();
            }
        });

        $bar->finish();
    }

    public function storeDuplicate($ikoop, $reason)
    {

        $password = Hash::make(rand(11111111, 99999999));
        if (strtolower($ikoop->status) == 'diluluskan') {
            $status = 1;
        } elseif (strtolower($ikoop->status) == 'dalam proses') {
            $status = 0;
        } else {
            $status = 0;
        }

        if ($ikoop->approvedDate == '' || $ikoop->approvedDate == '00/00/0000') {
            $approved_date = null;
        } else {
            $approved_date = date('Y-m-d 00:00:00', strtotime(str_replace("/", "-", $ikoop->approvedDate)));
        }

        if ($ikoop->yuran != "-") {
            $fee = str_replace("RM ", "", $ikoop->yuran);
            $fee = str_replace(",", "", $fee);
            $first_time_fees = (double)$fee;
        } else {
            $first_time_fees = 0.00;
        }

        if ($ikoop->sahamTerkumpul != "-") {
            $share = str_replace("RM ", "", $ikoop->sahamTerkumpul);
            $share = str_replace(",", "", $share);
            $share_amount = (double)$share;
        } else {
            $share_amount = 0.00;
        }

        $user = new UserDuplicate;
        $user->name = strtoupper($ikoop->name);
        $user->password = $password;
        $user->ic = $ikoop->newIC;
        $user->email = $ikoop->email;
        $user->mobile_no = $ikoop->mobileNo;
        $user->type = 'user';
        $user->postcode = $ikoop->postcode;
        $user->city = $ikoop->city;
        $user->parliament = $ikoop->cawangan;
        $user->heir_name = $ikoop->w_name_1;
        $user->heir_ic = $ikoop->w_ic1;
        $user->heir_relationship = $ikoop->w_relation1;
        $user->ikoop_id = $ikoop->ikoopId;
        $user->ikoop = 1;
        $user->status = $status;
        $user->job = $ikoop->job;
        $user->home_no = $ikoop->homeNo;
        $user->monthly_income = $ikoop->grossPay;
        $user->staff_no = $ikoop->staffNo;
        $user->witness = $ikoop->saksi1;
        $user->no_koppim = $ikoop->ikoopId;
        $user->password_reset = 1;
        if ($approved_date != null) {
            $user->approved_date = $approved_date;
        }
        $user->share_amount = $share_amount;
        $user->first_time_fees = $first_time_fees;
        $user->ikoop_reason = $reason;
        $user->registration_type = 'ikoop';
        $user->save();

        return 1;
    }
}
