<?php

namespace App\Console\Commands;

use DateTime;
use App\Mongo\PaymentBak;
use Illuminate\Console\Command;
use App\Mongo\Payment as MPayment;

class RevertDate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'revert:paymentdate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Revert ikoop payment date';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $paymentbak =
        PaymentBak::where('channel', 'ikoop')
            ->get();

        foreach ($paymentbak as $key => $value) {


            MPayment::where('channel', 'ikoop')
            ->update([
                'created_at' => new \MongoDB\BSON\UTCDateTime(new DateTime($value["created_at"])),
                'payment_date' => new \MongoDB\BSON\UTCDateTime(new DateTime($value["payment_date"]))
            ]);
        }

        echo 'successfully revert all ikoop payment date';
    }
}
