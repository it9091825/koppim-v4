<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Ikoop;
use App\User;
use Illuminate\Support\Facades\Hash;
use App\Payment;
use App\PaymentItems;

class MigrateIkoop extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ikoop:migrate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Migrate ikoop table into users table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $ikoopsCount = Ikoop::all();
        $bar = $this->output->createProgressBar(count($ikoopsCount));
        $bar->start();

        $ikoops = Ikoop::chunkById(100, function ($ikoops) use($bar){
            foreach ($ikoops as $ikoop) {
                if ($ikoop->newIC == 0) {
                    continue;
                }

                $checkIC = User::where('ic', $ikoop->newIC)->first();
                if ($checkIC) {
                    continue;
                }

//            if (strtolower($ikoop->status) != 'diluluskan') {
//                continue;
//            }

                $password = Hash::make(rand(11111111, 99999999));
                if(strtolower($ikoop->status) == 'diluluskan'){
                    $status = 1;
                } elseif(strtolower($ikoop->status) == 'dalam proses'){
                    $status = 2;
                } else {
                    $status = 0;
                }

                //todo:dob, approvedDate
                User::create([
                    'name' => strtoupper($ikoop->name),
                    'password' => $password,
                    'ic' => $ikoop->newIC,
                    'email' => $ikoop->email,
                    'mobile_no' => $ikoop->mobileNo,
                    'type' => 'user',
                    'postcode' => $ikoop->postcode,
                    'city' => $ikoop->city,
                    'parliament' => $ikoop->cawangan,
                    'heir_name' => $ikoop->w_name_1,
                    'heir_ic' => $ikoop->w_ic1,
                    'heir_relationship' => $ikoop->w_relation1,
                    'ikoop_id' => $ikoop->ikoopId,
                    'ikoop' => 1,
                    'status' => $status,
                    'job' => $ikoop->job,
                    'home_no' => $ikoop->homeNo,
                    'monthly_income' => $ikoop->grossPay,
                    'staff_no' => $ikoop->staffNo,
                    'witness' => $ikoop->saksi1,
                    'no_koppim' => $ikoop->ikoopId,
                    'password_reset' => 1,
                ]);

                if($ikoop->mobileNo == '' || $ikoop->mobileNo == null){
                    $phone = "0000000000";
                } else {
                    $phone = $ikoop->mobileNo;
                }

                if ($ikoop->yuran != "-" && $ikoop->sahamTerkumpul != "-") {

                    $share = str_replace("RM ", "", $ikoop->sahamTerkumpul);
                    $share = str_replace(",", "", $share);
                    $fee = str_replace("RM ", "", $ikoop->yuran);
                    $total = (double)$share + (double)$fee;

                    $payment = Payment::create([
                        'name' => strtoupper($ikoop->name),
                        'ic' => $ikoop->newIC,
                        'phone' => $phone,
                        'email' => $ikoop->email,
                        'amount' => $total,
                        'first_time_fees' => (double)$fee,
                        'share_amount' => (double)$share,
                        'status' => 'payment-completed',
                        'returncode' => "100",
                        'type' => 'one-time',
                        'gateway' => 'ikoop',
                    ]);

                    PaymentItems::create([
                        'item' => 'Jumlah Syer',
                        'amount' => (double)$share,
                        'order_no' => $payment->id
                    ]);

                    PaymentItems::create([
                        'item' => 'Fi Pendaftaran Anggota',
                        'amount' => (double)$fee,
                        'order_no' => $payment->id
                    ]);

                } elseif($ikoop->yuran != "-"){
                    $fee = str_replace("RM ", "", $ikoop->yuran);
                    $total = (double)$fee;

                    $payment = Payment::create([
                        'name' => strtoupper($ikoop->name),
                        'ic' => $ikoop->newIC,
                        'phone' => $phone,
                        'email' => $ikoop->email,
                        'amount' => $total,
                        'first_time_fees' => (double)$fee,
                        'share_amount' => 0.00,
                        'status' => 'payment-completed',
                        'returncode' => "100",
                        'type' => 'one-time',
                        'gateway' => 'ikoop',
                    ]);

                    PaymentItems::create([
                        'item' => 'Jumlah Syer',
                        'amount' => 0.00,
                        'order_no' => $payment->id
                    ]);

                    PaymentItems::create([
                        'item' => 'Fi Pendaftaran Anggota',
                        'amount' => (double)$fee,
                        'order_no' => $payment->id
                    ]);
                } elseif($ikoop->sahamTerkumpul != "-") {

                    $share = str_replace("RM ", "", $ikoop->sahamTerkumpul);
                    $share = str_replace(",", "", $share);
                    $total = (double)$share;

                    $payment = Payment::create([
                        'name' => strtoupper($ikoop->name),
                        'ic' => $ikoop->newIC,
                        'phone' => $phone,
                        'email' => $ikoop->email,
                        'amount' => $total,
                        'first_time_fees' => 0.00,
                        'share_amount' => (double)$share,
                        'status' => 'payment-completed',
                        'returncode' => "100",
                        'type' => 'one-time',
                        'gateway' => 'ikoop',
                    ]);

                    PaymentItems::create([
                        'item' => 'Jumlah Syer',
                        'amount' => (double)$share,
                        'order_no' => $payment->id
                    ]);

                    PaymentItems::create([
                        'item' => 'Fi Pendaftaran Anggota',
                        'amount' => 0.00,
                        'order_no' => $payment->id
                    ]);

                }
                $bar->advance();
            }
        });

        $bar->finish();

    }
}
