<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\PaymentProof;
use App\Mongo\Payment;
use App\Traits\OrderTrait;

class MigratePaymentProof extends Command
{
    use OrderTrait;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'paymentproof:migrate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Migrate all data in mysql table payment_proof into mongodb collection payments';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $proofsCount = PaymentProof::all();
        $bar = $this->output->createProgressBar(count($proofsCount));
        $bar->start();

        $proofs = PaymentProof::chunkById(100, function ($proofs) use ($bar) {
            foreach ($proofs as $proof) {
                if($proof->approval_status == 'waiting-for-approval') {
                    $order_ref = $this->create_order_ref($proof->ic);
                    Payment::create([
                        'name' => strtoupper($proof->name),
                        'ic' => (string)$proof->ic,
                        'phone' => (string)$proof->mobile_no,
                        'email' => $proof->email,
                        'amount' => (double)$proof->amount,
                        'share_amount' => 0.00,
                        'first_time_fees' => 0.00,
                        'first_time_share' => 0.00,
                        'additional_share' => 0.00,
                        'status' => 'waiting-for-verification',
                        'type' => 'one-time',
                        'order_ref' => $order_ref,
                        'payment_desc' => 'Bayaran Manual',
                        'channel' => 'manual',
                        'bank' => 'bank islam',
                        'payment_proof' => $proof->payment_proof,
                        'payment_date' => $proof->payment_date,
                        'note' => $proof->note,
                        'created_at' => $proof->created_at,
                        'updated_at' => $proof->updated_at,
                    ]);
                }
                $bar->advance();
            }
        });
        $bar->finish();
    }
}
