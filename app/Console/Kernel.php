<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Get the timezone that should be used by default for scheduled events.
     *
     * @return \DateTimeZone|string|null
     */
    protected function scheduleTimezone()
    {
        return 'Asia/Kuala_Lumpur';
    }

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        if(env('APP_ENV') == 'production') {
            //backup
            $schedule->command('backup:clean')->daily()->at('04:00');
            $schedule->command('backup:run')->weekly()->at('05:00');
            $schedule->command('backup:run --only-db')->daily()->at('05:00');
            $schedule->command('backup:run --only-db')->daily()->at('17:00');
        }
        //generate commission statement
        $schedule->command('commission:generate-statement')->monthly();

        //clear activity logs that are more than 365days
        //$schedule->command('activitylog:clean')->daily();

        //For Dividen
        if(env('APP_ENV') == 'staging') {
//            $schedule->exec('curl '.env('ADMIN_INVESTOR_DOMAIN').'/admin/dividen/cron/worker1')->everyMinute();
//            $schedule->exec('curl '.env('ADMIN_INVESTOR_DOMAIN').'/admin/dividen/cron/worker2')->everyMinute();
//            $schedule->exec('curl '.env('ADMIN_INVESTOR_DOMAIN').'/admin/dividen/cron/worker3')->everyMinute();
        }
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
