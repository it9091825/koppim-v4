<?php

namespace App\Exports\Admin;

use App\Traits\ExportHelperTrait;
use App\User;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Concerns\WithCustomValueBinder;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use App\State;

class KeanggotaanExport extends \PhpOffice\PhpSpreadsheet\Cell\StringValueBinder implements WithCustomValueBinder, FromQuery, ShouldAutoSize, WithStyles, WithMapping, WithHeadings
{
    use Exportable, ExportHelperTrait;

    public $titleName;

    /**
     * @var Request
     */


    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->titleName = strtoupper('Keanggotaan-KoPPIM-' . now()->format('dmYHms'));
    }

    public function columns()
    {
        return ['name', 'ic', 'dob', 'sex', 'religion', 'race', 'marital_status', 'email', 'mobile_no', 'home_no', 'job', 'monthly_income', 'heir_name', 'heir_ic', 'heir_relationship', 'heir_phone', 'address_line_1', 'address_line_2', 'address_line_3', 'postcode', 'city', 'state', 'parliament', 'created_at', 'is_affiliate'];
    }

    public function styles(Worksheet $sheet)
    {
        return [
            1    => [
                'font' => ['bold' => true],
            ],
            2    => [
                'font' => ['bold' => true],
            ],
        ];
    }

    public function headings(): array
    {
        if ($this->request->keanggotaan_columns) {

            return [
                [
                    $this->request->fileName ? strtoupper($this->request->fileName) : $this->titleName
                ],
                $this->formatHeader($this->request->keanggotaan_columns)
            ];
        }

        return [
            [
                $this->request->fileName ? strtoupper($this->request->fileName) : $this->titleName
            ],
            $this->formatHeader($this->columns())
        ];
    }

    public function query()
    {


        $users =  User::query()->where('type', 'user');

        if ($this->request->filled('keanggotaan_columns')) {

            $users = $users->select($this->request->keanggotaan_columns);
        } else {
            $users = $users->select($this->columns());
        }

        if ($this->request->filled('keanggotaan_states')) {

            if ($this->request->keanggotaan_states != 'all') {

                if ($this->request->keanggotaan_parliaments == 'all') {
                    // buat camni sebab state tidak disimpan sebagai ID di users table. So kena cari dulu nama state with given $this->request->state
                    $state = State::find($this->request->keanggotaan_states);
                    $users = $users->where('state', 'LIKE', '%' . $state->name . '%');
                } else {
                    $users = $users->where('parliament', 'LIKE', '%' . $this->request->keanggotaan_parliaments . '%');
                }
            }
        }

        if ($this->request->filled('keanggotaan_email')) {

            if ($this->request->keanggotaan_email == 'yes') {
                $users = $users->whereNotNull('email');
            } elseif ($this->request->keanggotaan_email == 'no') {
                $users = $users->whereNull('email');
            }
        }

        if ($this->request->filled('keanggotaan_heir')) {

            if ($this->request->keanggotaan_heir == 'yes') {
                $users = $users->whereNotNull('heir_ic');
            } elseif ($this->request->keanggotaan_heir == 'no') {
                $users = $users->whereNull('heir_ic');
            }
        }

        if ($this->request->filled('keanggotaan_status')) {
            $users = $users->where('status', $this->request->keanggotaan_status);
        }

        if ($this->request->filled('keanggotaan_start_date')) {
            $users = $users->where('created_at', '>=', Carbon::parse($this->request->keanggotaan_start_date));
        }

        if ($this->request->filled('keanggotaan_end_date')) {
            $users = $users->where('created_at', '<=', Carbon::parse($this->request->keanggotaan_end_date));
        }

        return $users;
    }

    public function map($user): array
    {

        if ($this->request->keanggotaan_columns) {
            return $this->mapColumns($this->request->keanggotaan_columns, $user);
        }

        return $this->mapColumns($this->columns(), $user);
    }
}
