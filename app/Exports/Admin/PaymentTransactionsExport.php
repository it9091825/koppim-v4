<?php

namespace App\Exports\Admin;

use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Concerns\WithCustomValueBinder;
use Maatwebsite\Excel\Concerns\WithHeadings;
use App\Mongo\Payment;
use App\Traits\ExportHelperTrait;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class PaymentTransactionsExport extends \PhpOffice\PhpSpreadsheet\Cell\StringValueBinder implements WithCustomValueBinder, FromQuery, WithHeadings, ShouldAutoSize, WithStyles, WithMapping
{
    use Exportable, ExportHelperTrait;

    public $titleName;

    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->titleName = strtoupper('TransaksiBayaran-KoPPIM-' . now()->format('dmYHms'));
    }

    public function columns()
    {
        return ['order_ref', 'name', 'ic', 'phone', 'email', 'amount', 'share_amount', 'first_time_fees', 'first_time_share', 'additional_share', 'payment_desc', 'status', 'channel', 'gateway', 'created_at'];
    }

    public function styles(Worksheet $sheet)
    {
        return [
            1    => [
                'font' => ['bold' => true],
            ],
            2    => [
                'font' => ['bold' => true],
            ],
        ];
    }

    public function headings(): array
    {
        if ($this->request->payment_columns) {

            return [
                [
                    $this->request->fileName ? strtoupper($this->request->fileName) : $this->titleName
                ],
                $this->formatHeader($this->request->payment_columns)
            ];
        }


        return [
            [
                $this->request->fileName ? strtoupper($this->request->fileName) : $this->titleName
            ],
            $this->formatHeader($this->columns())
        ];
    }


    public function query()
    {

        $payments =  Payment::query();

        if ($this->request->filled('payment_columns')) {

            $payments = $payments->select($this->request->payment_columns);
        } else {
            $payments = $payments->select($this->columns());
        }

        if ($this->request->filled('payment_method')) {
            $payments = $payments->where('channel', $this->request->payment_method);
        }

        if ($this->request->filled('payment_status')) {
            $payments = $payments->where('status', $this->request->payment_status);
        }

        if ($this->request->filled('start_date')) {
            $payments = $payments->where('created_at', '>=', Carbon::parse($this->request->start_date));
        }

        if ($this->request->filled('end_date')) {
            $payments = $payments->where('created_at', '<=', Carbon::parse($this->request->end_date));
        }

        return $payments;
    }

    public function map($payment): array
    {
        if ($this->request->payment_columns) {

            return $this->mapColumns($this->request->payment_columns, $payment);
        }

        return $this->mapColumns($this->columns(), $payment);
    }
}
