<?php

namespace App\Exports\Admin;

use App\Traits\ExportHelperTrait;
use App\User;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Concerns\WithCustomValueBinder;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use App\State;

class PermohonanExport extends \PhpOffice\PhpSpreadsheet\Cell\StringValueBinder implements WithCustomValueBinder, FromQuery, ShouldAutoSize, WithStyles, WithMapping, WithHeadings
{
    use Exportable, ExportHelperTrait;

    public $titleName;

    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->titleName = strtoupper('Pendaftaran-KoPPIM-' . now()->format('dmYHms'));
    }

    public function columns()
    {
        return ['name', 'ic', 'dob', 'sex', 'religion', 'race', 'marital_status', 'email', 'mobile_no', 'home_no', 'job', 'monthly_income', 'heir_name', 'heir_ic', 'heir_relationship', 'heir_phone', 'address_line_1', 'address_line_2', 'address_line_3', 'postcode', 'city', 'state', 'parliament', 'created_at', 'is_affiliate'];
    }

    public function styles(Worksheet $sheet)
    {
        return [
            1    => [
                'font' => ['bold' => true],
            ],
            2    => [
                'font' => ['bold' => true],
            ],
        ];
    }

    public function headings(): array
    {
        if ($this->request->permohonan_columns) {

            return [
                [
                    $this->request->fileName ? strtoupper($this->request->fileName) : $this->titleName
                ],
                $this->formatHeader($this->request->permohonan_columns)
            ];
        }


        return [
            [
                $this->request->fileName ? strtoupper($this->request->fileName) : $this->titleName
            ],
            $this->formatHeader($this->columns())
        ];
    }


    public function query()
    {

        $users =  User::query()->where('type', 'user');

        if ($this->request->filled('permohonan_columns')) {

            $users = $users->select($this->request->permohonan_columns);
        } else {
            $users = $users->select($this->columns());
        }

        if ($this->request->filled('permohonan_registration_type')) {
            $users = $users->where('registration_type', $this->request->permohonan_registration_type);
        }

        if($this->request->filled('permohonan_states')){

            if ($this->request->filled('permohonan_states')) {
                if ($this->request->permohonan_states != 'all') {

                    if ($this->request->permohonan_parliaments == 'all') {
                        // buat camni sebab state tidak disimpan sebagai ID di users table. So kena cari dulu nama state with given $request->state
                        $state = State::find($this->request->permohonan_states);
                        $users = $users->where('state', 'LIKE', '%' . $state->name . '%');
                    } else {
                        $users = $users->where('parliament', 'LIKE', '%' . $this->request->permohonan_parliaments . '%');
                    }
                }
            }

        }

        if ($this->request->filled('permohonan_status')) {
            $users = $users->where('status', $this->request->permohonan_status);
        }

        if ($this->request->filled('permohonan_start_date')) {
            $users = $users->where('created_at', '>=', Carbon::parse($this->request->permohonan_start_date));
        }

        if ($this->request->filled('permohonan_end_date')) {
            $users = $users->where('created_at', '<=', Carbon::parse($this->request->permohonan_end_date));
        }

        return $users;
    }

    public function map($user): array
    {
        if ($this->request->permohonan_columns) {
            return $this->mapColumns($this->request->permohonan_columns, $user);
        }

        return $this->mapColumns($this->columns(), $user);
    }
}
