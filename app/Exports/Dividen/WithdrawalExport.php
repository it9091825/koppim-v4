<?php

namespace App\Exports\Dividen;

use App\Mongo\CommissionWithdraw;
use App\Mongo\DividenWallet as DividenWallet;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithCustomValueBinder;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class WithdrawalExport extends \PhpOffice\PhpSpreadsheet\Cell\StringValueBinder implements WithCustomValueBinder, FromQuery, WithMapping, WithStrictNullComparison, WithHeadings, ShouldAutoSize, WithStyles
{
    use Exportable;

    public function __construct(string $status)
    {
        $this->status = $status;
    }

    public function styles(Worksheet $sheet)
    {
        return [
            // Style the first row as bold text.
            1 => ['font' => ['bold' => true]],

        ];
    }

    public function headings(): array
    {
        return [
            'TARIKH',
            'NAMA',
            'NO. MYKAD',
            'NAMA AKAUN',
            'NO. AKAUN BANK',
            'NAMA BANK',
            'STATUS',
            'JUMLAH',
            'NO. RUJUKAN BAYARAN',
            'BUKTI PEMBAYARAN'
        ];
    }

    public function query()
    {
        if ($this->status == 'all') {
            $withdraws = DividenWallet::query()->where('type','out');
        } else {
            $withdraws = DividenWallet::query()->where('type','out')->where('status', $this->status);
        }

        return $withdraws;
    }

    public function map($withdraw): array
    {
       

        $amount = 'RM' . number_format($withdraw->amount, 2, '.', ',');

        $statement_date = $withdraw->statement_date ? strtoupper($withdraw->statement_date->format('F Y')) : '';

        return [
            $withdraw->created_at,
            $withdraw->account_name,
            (string)$withdraw->ic,
            $withdraw->account_name,
            $withdraw->account_no,
            $withdraw->bank,
            strtoupper($withdraw->status),
            $amount,
            $withdraw->payment_ref,
            env('DO_SPACES_FULL_URL').$withdraw->payment_receipt
        ];
    }

}
