<?php

namespace App\Exports\Affiliate;

use App\Mongo\CommissionWithdraw;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithStrictNullComparison;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithCustomValueBinder;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class WithdrawalExport extends \PhpOffice\PhpSpreadsheet\Cell\StringValueBinder implements WithCustomValueBinder, FromQuery, WithMapping, WithStrictNullComparison, WithHeadings, ShouldAutoSize, WithStyles
{
    use Exportable;

    public function __construct(string $status)
    {
        $this->status = $status;
    }

    public function styles(Worksheet $sheet)
    {
        return [
            // Style the first row as bold text.
            1 => ['font' => ['bold' => true]],

        ];
    }

    public function headings(): array
    {
        return [
            'TARIKH',
            'NAMA EJEN',
            'NO. MYKAD',
            'NO. ANGGOTA',
            'NO. RUJUKAN',
            'BULAN PENYATA',
            'NAMA AKAUN',
            'NO. AKAUN BANK',
            'NAMA BANK',
            'STATUS',
            'JUMLAH',
            'NO. RUJUKAN BAYARAN'
        ];
    }

    public function query()
    {
        if ($this->status == 'all') {
            $withdraws = CommissionWithdraw::query();
        } else {
            $withdraws = CommissionWithdraw::query()->where('status', $this->status);
        }

        return $withdraws;
    }

    public function map($withdraw): array
    {
        if ($withdraw->status == 'paid') {
            $status = 'SUDAH DIBAYAR';
        } elseif ($withdraw->status == 'processing') {
            $status = 'DALAM PROSES';
        } else {
            $status = 'DIBATALKAN';
        }

        $amount = 'RM' . number_format($withdraw->amount, 2, '.', ',');

        $statement_date = $withdraw->statement_date ? strtoupper($withdraw->statement_date->format('F Y')) : '';

        return [
            $withdraw->created_at,
            $withdraw->name,
            (string)$withdraw->ic,
            (string)$withdraw->no_anggota,
            (string)$withdraw->reference_no,
            $statement_date,
            $withdraw->account_name,
            $withdraw->account_no,
            $withdraw->bank,
            $status,
            $amount,
            $withdraw->payment_ref
        ];
    }

}
