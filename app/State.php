<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Parliament;

class State extends Model
{
    protected $table = 'states';

    protected $timestamp = false;

    public function parliaments(){

        return $this->hasMany(Parliament::class);

    }
}
