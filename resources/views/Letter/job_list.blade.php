@extends('admin.layouts')
@css
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/jquery-datetimepicker@2.5.21/jquery.datetimepicker.css">
@endcss
@pagetitle(['title'=>'Senarai Penghantaran','links' => ['Surat']])@endpagetitle
@section('content')

<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body pd-b-0">
                    Cron Job URL : <a target="_blank" href="https://{{ env('ADMIN_INVESTOR_DOMAIN') }}/admin/letter/cron/jobs">https://{{ env('ADMIN_INVESTOR_DOMAIN') }}/admin/letter/cron/jobs</a>
                </div>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body pd-b-0">

                    <table class="table">
                        <thead>
                            <tr>
                                <th>Filter</th>
                                <th>Surat</th>
                                <th>Progress</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($jobs as $job)
                            
	                            @if($job->filter == "filteraff")
	                            <tr>
	                                <td>
	                                    <a href="/admin/letter/job/detail/?id={{ $job->_id }}">Filter : <b>AFFLIATES</b> <br> STATUS : <b>{{ strtoupper($job->statusaff) }}</b></a>
	                                </td>
	                                <td><a href="/admin/letter/job/detail/?id={{ $job->_id }}">{{ $job->subject }}</a></td>
	                                <td><a href="/admin/letter/job/detail/?id={{ $job->_id }}">{{ LetterHelper::count_progress($job->_id) }} / {{ $job->jumlah_pengguna }}</a></td>
	                                <td>{{ $job->cron_status }}</td>
	                            </tr>
	                            @else
	                            <tr>
	                                <td>
	                                    <a href="/admin/letter/job/detail/?id={{ $job->_id }}">CAWANGAN : <b>{{ $job->cawanganlbl }}</b> <br> STATUS : <b>{{ $job->status_lbl }}</b></a>
	                                </td>
	                                <td><a href="/admin/letter/job/detail/?id={{ $job->_id }}">{{ $job->subject }}</a></td>
	                                <td><a href="/admin/letter/job/detail/?id={{ $job->_id }}">{{ LetterHelper::count_progress($job->_id) }} / {{ $job->jumlah_pengguna }}</a></td>
	                                <td>{{ $job->cron_status }}</td>
	                            </tr>
	                            @endif
                            @empty

                            <tr>
                                <td colspan="4">
                                    TIADA REKOD
                                </td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>

                </div><!-- card-body -->
            </div><!-- card -->
        </div>
    </div>
</div>
@endsection
