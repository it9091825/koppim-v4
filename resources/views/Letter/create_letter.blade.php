@extends('admin.layouts')
@css
<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
@endcss
@pagetitle(['title'=>'Tambah Template Surat','links' => ['Surat']])@endpagetitle
@section('content')
<div class="container">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="float-right" style="margin-bottom: 30px;">
                        @if($created == false)
                        <a href="/admin/letter/send?letter_id={{ $letter->id }}" class="btn btn-success btn-sm">HANTAR SURAT INI</a>
                        @endif
                    </div>
                    <div style="clear: both;"></div>
                    @if($created == false)
                    <form enctype="multipart/form-data" method="post" id="form" action="/admin/letter/edit?id={{ $letter->id }}">
                        @else
                        <form enctype="multipart/form-data" method="post" id="form" action="/admin/letter/create">
                            @endif
                            @csrf
                            <div class="row" style="margin-bottom: 20px;">
                                <div class="col-12" style="margin-bottom: 20px;">


                                    <select class="form-control" name="name" id="name">
                                        <option value="SURAT KOPPIM KEPADA ANGGOTA">SURAT KOPPIM KEPADA ANGGOTA</option>
                                        <option value="SURAT DIVIDEN">SURAT DIVIDEN</option>
                                        <option value="SURAT ANGGOTA KEPADA KOPPIM">SURAT ANGGOTA KEPADA KOPPIM</option>
                                        <option value="SURAT INSURANS">SURAT INSURANS</option>
                                    </select>
                                </div>



                                <div class="col-12">
                                    <input type="text" class="form-control" name="subject" id="subject" placeholder="TAJUK SURAT" value="@if($created == false) {{ $letter['subject'] }} @endif" />
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-8">

                                    <textarea class="form-control" style="height: 500px; display: none;" name="content" id="content">@if($created == false) {!! $content !!} @endif</textarea>
                                    <div id="summernote-editor">
                                        @if($created == false) {!! $content !!} @endif
                                    </div> <!-- end summernote-editor-->

                                </div>

                                <div class="col-4">
                                    <select class="form-control" id="listbox" style="height: 500px;" multiple>
                                        @foreach($short_codes as $key => $value)
                                        <option value="{{ $value }}" onclick="InsertAtCaret('{{ $value }}')">{{ $value }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>


                            <div style="margin-top: 20px;" align="center">
                                <button type="button" onclick="submits()" class="btn btn-primary">SIMPAN</button>



                            </div>
                            <hr />
                            <div style="margin-top: 20px;" align="center">

                                @if($created == false)
                                <div style="margin-bottom: 10px;">
                                    <input type="checkbox" id="withheader" name="withheader" onchange="checkbox()"  value=""> Dengan Letterhead
                                </div>

                                <a target="_blank" id="lihatsample" href="/admin/letter/preview?id={{ $letter->id }}&type=preview&ic={{ $ic_sample }}" class="btn btn-info btn-sm">LIHAT SAMPLE</a>
                                <a target="_blank" id="cetaksample" href="/admin/letter/preview?id={{ $letter->id }}&type=print&ic={{ $ic_sample }}" class="btn btn-info btn-sm">CETAK SAMPLE</a>
                                <a target="_blank" id="pdfsample" href="/admin/letter/preview?id={{ $letter->id }}&type=pdf&ic={{ $ic_sample }}" class="btn btn-info btn-sm">PDF SAMPLE</a>
                                @endif

                            </div>
                        </form>

                        @if($created == false)
                        <div style="margin-top: 20px;" align="center">
                            <form enctype="multipart/form-data" method="post" id="form" action="/admin/letter/preview/email-sample?id={{ $letter->id }}">
                                @csrf
                                <div><input type="text" class="form-control" name="email_sample" id="email_sample" placeholder="EMEL" style="text-align: center;" /></div>
                                <div style="margin-top: 20px;"><input type="hidden" class="form-control" name="header" id="header" value="false" /></div>
                                <div style="margin-top: 20px;"><input type="hidden" class="form-control" name="ic" id="ic" value="{{ $ic_sample }}" /></div>

                                <button class="btn btn-info btn-sm">HANTAR SAMPEL EMAIL</button>
                            </form>
                        </div>
                        @endif

                        <hr />
                        @if($created == false)
                        <div style="margin-top: 20px;" align="center">
                            <a href="/admin/letter/send?letter_id={{ $letter->id }}" class="btn btn-success btn-sm">HANTAR SURAT INI</a>
                        </div>
                        @endif

                </div>
            </div> <!-- end card-->
        </div> <!-- end col-->
    </div>
</div>
<!-- end row-->

@endsection

@section('js')
<script src="{{asset('admin/libs/summernote/summernote-bs4.min.js')}}"></script>
<script>
    $(document).ready(function() {
        $("#summernote-editor").summernote({
            height: 500
            , minHeight: null
            , maxHeight: null
            , focus: !1
        }), $("#summernote-inline").summernote({
            airMode: !0
        })
    });

    @if($created == false)

    $('#name').val('{{ $letter->name }}').change();

    @endif

    function add_code(val) {

        $('#content').val($('#content').val() + val);

    }

    function submits() {
        var textareaValue = $('#summernote-editor').summernote('code');
        $('#content').val(textareaValue);

        $('#form').submit();
    }

    function checkbox() {
		
		
		if (document.getElementById('withheader').checked) 
		  {
		      
		      @if($created == false)
                $("#lihatsample").attr("href", "/admin/letter/preview?id={{ $letter->id }}&type=preview&header=true&ic={{ $ic_sample }}");
                $("#cetaksample").attr("href", "/admin/letter/preview?id={{ $letter->id }}&type=print&header=true&ic={{ $ic_sample }}");
                $("#pdfsample").attr("href", "/admin/letter/preview?id={{ $letter->id }}&type=pdf&header=true&ic={{ $ic_sample }}");
                $('#header').val('true');
                @endif
		      
		  } else {
			  
			  @if($created == false)
                $("#lihatsample").attr("href", "/admin/letter/preview?id={{ $letter->id }}&type=preview&header=false&ic={{ $ic_sample }}");
                $("#cetaksample").attr("href", "/admin/letter/preview?id={{ $letter->id }}&type=print&header=false&ic={{ $ic_sample }}");
                $("#pdfsample").attr("href", "/admin/letter/preview?id={{ $letter->id }}&type=pdf&header=false&ic={{ $ic_sample }}");
                $('#header').val('false');
                @endif
		     
		  }
		
        //alert(textareaValue);
       
    }


    function InsertAtCaret(text) {
        $("#summernote-editor").summernote('editor.insertText', text);
    }
    /*
	function InsertAtCaret(myValue) {

    return $("#summernote-editor").each(function(i) {
        if (document.selection) {
            //For browsers like Internet Explorer
            this.focus();
            sel = document.selection.createRange();
            sel.text = myValue;
            this.focus();
        } else if (this.selectionStart || this.selectionStart == '0') {
            //For browsers like Firefox and Webkit based
            var startPos = this.selectionStart;
            var endPos = this.selectionEnd;
            var scrollTop = this.scrollTop;
            this.value = this.value.substring(0, startPos) + myValue + this.value.substring(endPos, this.value.length);
            this.focus();
            this.selectionStart = startPos + myValue.length;
            this.selectionEnd = startPos + myValue.length;
            this.scrollTop = scrollTop;
        } else {
            this.value += myValue;
            this.focus();
        }
    })
}
*/

</script>

@endsection
