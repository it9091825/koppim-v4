@extends('admin.layouts')
@css
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/jquery-datetimepicker@2.5.21/jquery.datetimepicker.css">
@endcss
@pagetitle(['title'=>'Senarai Kategori Surat','links' => ['Surat']])@endpagetitle
@section('content')

<div class="container">
    <div class="row" style="">
        @include('include.search',['url' => 'admin/letter/utama','placeholder'=>'BUAT CARIAN MENGGUNAKAN NAMA KATEGORI SURAT'])
    </div>
    <div class="row">
        @include('include.pagination',['results' => $letters])
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body pd-b-0">

                    <table class="table">
                        <thead>
                            <tr>
                                <th>Kategori</th>
                                <th>Tajuk</th>
                                <th>Tindakan</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($letters as $letter)
                            <tr>

                                <td>{{ $letter->name }}</td>
                                <td>{{ $letter->subject }}</td>
                                <td><a href="/admin/letter/edit?id={{ $letter->id }}">Edit</a> | <a href="/admin/letter/send?letter_id={{ $letter->id }}">Hantar</a></td>
                            </tr>
                            @empty
                            <tr>

                                <td colspan="3">Tiada Rekod. <a href="/admin/letter/create">Sila buat Template Surat Anda</a></td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>

                </div><!-- card-body -->
            </div><!-- card -->
        </div>
    </div>
</div>



@endsection
