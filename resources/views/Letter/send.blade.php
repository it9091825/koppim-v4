@extends('admin.layouts')
@css
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/jquery-datetimepicker@2.5.21/jquery.datetimepicker.css">

@endcss
@pagetitle(['title'=>'Hantar Surat','links' => ['Surat']])@endpagetitle
@section('content')

<div class="container">

    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body pd-b-0">

                    <div class="row">
                        <div class="col-12" style="margin-bottom: 20px;">
                            <b>1. SILA PILIH JENIS SURAT</b>
                        </div>
                        <div class="col-12" style="margin-bottom: 20px;">
                            <select class="form-control select2" name="letters" id="letters">
                                <option value="">SILA PILIH SURAT UNTUK DIHANTAR</option>
                                @foreach($letters as $letter)
                                <option value="{{ $letter->id }}">{{ $letter->name." - ".$letter->subject }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                </div><!-- card-body -->
            </div><!-- card -->
        </div>


    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body pd-b-0">
                    <div class="row">
                        <div class="col-12" style="margin-bottom: 20px;">
                            <b>2. SILA PILIH INDIVIDU ATAU KUMPULAN</b>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-10">
                            <input type="text" class="form-control" name="search" id="search" placeholder="CARIAN MENGGUNAKAN NO. ANGGOTA, NO. MYKAD ATAU NO. TELEFON" value="{{ $search }}" />
                        </div>

                        <div class="col-2" align="right">
                            <button onclick="search_personal()" class="btn btn-primary btn-md">Cari</button>
                        </div>
                    </div>

                    <hr />

                    <div class="row">
                        <div class="col-5">
                            <select class="form-control select2" name="cawangan" id="cawangan">
                                <option value="">[SEMUA CAWANGAN]</option>
                                <option value="Padang Besar">Padang Besar</option>
                                <option value="Kangar">Kangar</option>
                                <option value="Arau">Arau</option>
                                <option value="Langkawi">Langkawi</option>
                                <option value="Jerlun">Jerlun</option>
                                <option value="Kubang Pasu">Kubang Pasu</option>
                                <option value="Padang Terap">Padang Terap</option>
                                <option value="Pokok Sena">Pokok Sena</option>
                                <option value="Alor Setar">Alor Setar</option>
                                <option value="Kuala Kedah">Kuala Kedah</option>
                                <option value="Pendang">Pendang</option>
                                <option value="Jerai">Jerai</option>
                                <option value="Sik">Sik</option>
                                <option value="Merbok">Merbok</option>
                                <option value="Sungai Petani">Sungai Petani</option>
                                <option value="Baling">Baling</option>
                                <option value="Padang Serai">Padang Serai</option>
                                <option value="Kulim-Bandar Baharu">Kulim-Bandar Baharu</option>
                                <option value="Tumpat">Tumpat</option>
                                <option value="Pengkalan Chepa">Pengkalan Chepa</option>
                                <option value="Kota Bharu">Kota Bharu</option>
                                <option value="Pasir Mas">Pasir Mas</option>
                                <option value="Rantau Panjang">Rantau Panjang</option>
                                <option value="Kubang Kerian">Kubang Kerian</option>
                                <option value="Bachok">Bachok</option>
                                <option value="Ketereh">Ketereh</option>
                                <option value="Tanah Merah">Tanah Merah</option>
                                <option value="Pasir Puteh">Pasir Puteh</option>
                                <option value="Machang">Machang</option>
                                <option value="Jeli">Jeli</option>
                                <option value="Kuala Krai">Kuala Krai</option>
                                <option value="Gua Musang">Gua Musang</option>
                                <option value="Besut">Besut</option>
                                <option value="Setiu">Setiu</option>
                                <option value="Kuala Nerus">Kuala Nerus</option>
                                <option value="Kuala Terengganu">Kuala Terengganu</option>
                                <option value="Marang">Marang</option>
                                <option value="Hulu Terengganu">Hulu Terengganu</option>
                                <option value="Dungun">Dungun</option>
                                <option value="Kemaman">Kemaman</option>
                                <option value="Kepala Batas">Kepala Batas</option>
                                <option value="Tasek Gelugor">Tasek Gelugor</option>
                                <option value="Bagan">Bagan</option>
                                <option value="Permatang Pauh">Permatang Pauh</option>
                                <option value="Bukit Mertajam">Bukit Mertajam</option>
                                <option value="Batu Kawan">Batu Kawan</option>
                                <option value="Nibong Tebal">Nibong Tebal</option>
                                <option value="Bukit Bendera">Bukit Bendera</option>
                                <option value="Tanjong">Tanjong</option>
                                <option value="Jelutong">Jelutong</option>
                                <option value="Bukit Gelugor">Bukit Gelugor</option>
                                <option value="Bayan Baru">Bayan Baru</option>
                                <option value="Balik Pulau">Balik Pulau</option>
                                <option value="Gerik">Gerik</option>
                                <option value="Lenggong">Lenggong</option>
                                <option value="Larut">Larut</option>
                                <option value="Parit Buntar">Parit Buntar</option>
                                <option value="Bagan Serai">Bagan Serai</option>
                                <option value="Bukit Gantang">Bukit Gantang</option>
                                <option value="Taiping">Taiping</option>
                                <option value="Padang Rengas">Padang Rengas</option>
                                <option value="Sungai Siput (U)">Sungai Siput (U)</option>
                                <option value="Tambun">Tambun</option>
                                <option value="Ipoh Timur">Ipoh Timur</option>
                                <option value="Ipoh Barat">Ipoh Barat</option>
                                <option value="Batu Gajah">Batu Gajah</option>
                                <option value="Kuala Kangsar">Kuala Kangsar</option>
                                <option value="Beruas">Beruas</option>
                                <option value="Parit">Parit</option>
                                <option value="Kampar">Kampar</option>
                                <option value="Gopeng">Gopeng</option>
                                <option value="Tapah">Tapah</option>
                                <option value="Pasir Salak">Pasir Salak</option>
                                <option value="Lumut">Lumut</option>
                                <option value="Bagan Datuk">Bagan Datuk</option>
                                <option value="Teluk Intan">Teluk Intan</option>
                                <option value="Tanjung Malim">Tanjung Malim</option>
                                <option value="Cameron Highlands">Cameron Highlands</option>
                                <option value="Lipis">Lipis</option>
                                <option value="Raub">Raub</option>
                                <option value="Jerantut">Jerantut</option>
                                <option value="Indera Mahkota">Indera Mahkota</option>
                                <option value="Kuantan">Kuantan</option>
                                <option value="Paya Besar">Paya Besar</option>
                                <option value="Maran">Maran</option>
                                <option value="Kuala Krau">Kuala Krau</option>
                                <option value="Temerloh">Temerloh</option>
                                <option value="Bentong">Bentong</option>
                                <option value="Bera">Bera</option>
                                <option value="Rompin">Rompin</option>
                                <option value="Sabak Bernam">Sabak Bernam</option>
                                <option value="Sungai Besar">Sungai Besar</option>
                                <option value="Hulu Selangor">Hulu Selangor</option>
                                <option value="Tanjong Karang">Tanjong Karang</option>
                                <option value="Kuala Selangor">Kuala Selangor</option>
                                <option value="Selayang">Selayang</option>
                                <option value="Gombak">Gombak</option>
                                <option value="Ampang">Ampang</option>
                                <option value="Pandan">Pandan</option>
                                <option value="Hulu Langat">Hulu Langat</option>
                                <option value="Bangi">Bangi</option>
                                <option value="Puchong">Puchong</option>
                                <option value="Subang">Subang</option>
                                <option value="Petaling Jaya">Petaling Jaya</option>
                                <option value="Damansara">Damansara</option>
                                <option value="Sungai Buloh">Sungai Buloh</option>
                                <option value="Shah Alam">Shah Alam</option>
                                <option value="Kapar">Kapar</option>
                                <option value="Klang">Klang</option>
                                <option value="Kota Raja">Kota Raja</option>
                                <option value="Kuala Langat">Kuala Langat</option>
                                <option value="Sepang">Sepang</option>
                                <option value="Kepong">Kepong</option>
                                <option value="Batu">Batu</option>
                                <option value="Wangsa Maju">Wangsa Maju</option>
                                <option value="Segambut">Segambut</option>
                                <option value="Setiawangsa">Setiawangsa</option>
                                <option value="Titiwangsa">Titiwangsa</option>
                                <option value="Bukit Bintang">Bukit Bintang</option>
                                <option value="Lembah Pantai">Lembah Pantai</option>
                                <option value="Seputeh">Seputeh</option>
                                <option value="Cheras">Cheras</option>
                                <option value="Bandar Tun Razak">Bandar Tun Razak</option>
                                <option value="Putrajaya">Putrajaya</option>
                                <option value="Jelebu">Jelebu</option>
                                <option value="Jempol">Jempol</option>
                                <option value="Seremban">Seremban</option>
                                <option value="Kuala Pilah">Kuala Pilah</option>
                                <option value="Rasah">Rasah</option>
                                <option value="Rembau">Rembau</option>
                                <option value="Port Dickson">Port Dickson</option>
                                <option value="Tampin">Tampin</option>
                                <option value="Masjid Tanah">Masjid Tanah</option>
                                <option value="Alor Gajah">Alor Gajah</option>
                                <option value="Tangga Batu">Tangga Batu</option>
                                <option value="Hang Tuah Jaya">Hang Tuah Jaya</option>
                                <option value="Kota Melaka">Kota Melaka</option>
                                <option value="Jasin">Jasin</option>
                                <option value="Segamat">Segamat</option>
                                <option value="Sekijang">Sekijang</option>
                                <option value="Labis">Labis</option>
                                <option value="Pagoh">Pagoh</option>
                                <option value="Ledang">Ledang</option>
                                <option value="Bakri">Bakri</option>
                                <option value="Muar">Muar</option>
                                <option value="Parit Sulong">Parit Sulong</option>
                                <option value="Ayer Hitam">Ayer Hitam</option>
                                <option value="Sri Gading">Sri Gading</option>
                                <option value="Batu Pahat">Batu Pahat</option>
                                <option value="Simpang Renggam">Simpang Renggam</option>
                                <option value="Kluang">Kluang</option>
                                <option value="Sembrong">Sembrong</option>
                                <option value="Mersing">Mersing</option>
                                <option value="Tenggara">Tenggara</option>
                                <option value="Kota Tinggi">Kota Tinggi</option>
                                <option value="Pengerang">Pengerang</option>
                                <option value="Tebrau">Tebrau</option>
                                <option value="Pasir Gudang">Pasir Gudang</option>
                                <option value="Johor Bahru">Johor Bahru</option>
                                <option value="Pulai">Pulai</option>
                                <option value="Iskandar Puteri">Iskandar Puteri</option>
                                <option value="Kulai">Kulai</option>
                                <option value="Pontian">Pontian</option>
                                <option value="Tanjong Piai">Tanjong Piai</option>
                                <option value="Labuan">Labuan</option>
                                <option value="Kudat">Kudat</option>
                                <option value="Kota Marudu">Kota Marudu</option>
                                <option value="Kota Belud">Kota Belud</option>
                                <option value="Tuaran">Tuaran</option>
                                <option value="Sepanggar">Sepanggar</option>
                                <option value="Kota Kinabalu">Kota Kinabalu</option>
                                <option value="Putatan">Putatan</option>
                                <option value="Penampang">Penampang</option>
                                <option value="Papar">Papar</option>
                                <option value="Kimanis">Kimanis</option>
                                <option value="Beaufort">Beaufort</option>
                                <option value="Sipitang">Sipitang</option>
                                <option value="Ranau">Ranau</option>
                                <option value="Keningau">Keningau</option>
                                <option value="Tenom">Tenom</option>
                                <option value="Pensiangan">Pensiangan</option>
                                <option value="Beluran">Beluran</option>
                                <option value="Libaran">Libaran</option>
                                <option value="Batu Sapi">Batu Sapi</option>
                                <option value="Sandakan">Sandakan</option>
                                <option value="Kinabatangan">Kinabatangan</option>
                                <option value="Silam">Silam</option>
                                <option value="Semporna">Semporna</option>
                                <option value="Tawau">Tawau</option>
                                <option value="Kalabakan">Kalabakan</option>
                                <option value="Mas Gading">Mas Gading</option>
                                <option value="Santubong">Santubong</option>
                                <option value="Petra Jaya">Petra Jaya</option>
                                <option value="Bandar Kuching">Bandar Kuching</option>
                                <option value="Stampin">Stampin</option>
                                <option value="Kota Samarahan">Kota Samarahan</option>
                                <option value="Puncak Borneo">Puncak Borneo</option>
                                <option value="Serian">Serian</option>
                                <option value="Batang Sadong">Batang Sadong</option>
                                <option value="Batang Lupar">Batang Lupar</option>
                                <option value="Sri Aman">Sri Aman</option>
                                <option value="Lubok Antu">Lubok Antu</option>
                                <option value="Betong">Betong</option>
                                <option value="Saratok">Saratok</option>
                                <option value="Tanjung Manis">Tanjung Manis</option>
                                <option value="Igan">Igan</option>
                                <option value="Sarikei">Sarikei</option>
                                <option value="Julau">Julau</option>
                                <option value="Kanowit">Kanowit</option>
                                <option value="Lanang">Lanang</option>
                                <option value="Sibu">Sibu</option>
                                <option value="Mukah">Mukah</option>
                                <option value="Selangau">Selangau</option>
                                <option value="Kapit">Kapit</option>
                                <option value="Hulu Rajang">Hulu Rajang</option>
                                <option value="Bintulu">Bintulu</option>
                                <option value="Sibuti">Sibuti</option>
                                <option value="Miri">Miri</option>
                                <option value="Baram">Baram</option>
                                <option value="Limbang">Limbang</option>
                                <option value="Lawas">Lawas</option>
                            </select>
                        </div>

                        <div class="col-5">
                            <select class="form-control" name="statusss" id="statusss">
                                <option value="">[SEMUA STATUS PELANGGAN]</option>
                                <option value="0">BELUM DIKEMASKINI</option>
                                <option value="1">DILULUSKAN (ANGGOTA AKTIF)</option>
                                <option value="2">UNTUK DISEMAK</option>
                                <option value="3">UNTUK DILULUSKAN</option>
                                <option value="99">DITOLAK</option>
                            </select>
                        </div>

                        <div class="col-2" align="right">
                            <button class="btn btn-primary btn-md" onclick="search_filter()">Cari</button>
                        </div>

                    </div>

                    <hr />
                     <div class="row">
                        <div class="col-5">
                            <select class="form-control select2" name="afflite" id="afflite">
                                <option value="">[SEMUA AFFLIATES]</option>

                            </select>
                        </div>

                        <div class="col-5">
                            <select class="form-control" name="statusss_aff" id="statusss_aff">
                                <option value="">[SEMUA STATUS AFFILIATE]</option>

                                <option value="yes">DILULUSKAN</option>
                                <option value="no">TIDAK LULUS</option>
                                <option value="reject">DITOLAK</option>
                                <option value="apply">PERMOHONAN </option>
                            </select>
                        </div>

                        <div class="col-2" align="right">
                            <button class="btn btn-primary btn-md" onclick="search_afflites()">Cari</button>
                        </div>

                    </div>


                    @if($searchtype == "filter" || $searchtype == "filteraff")

                    <div class="row" style="margin-top: 30px;">
                        <div class="col-12" align="center">
                            <button class="btn btn-success btn-sm" onclick="send_to_filer()">Hantar Kepada Semua</button>
                        </div>
                    </div>

                    @endif


                </div><!-- card-body -->
            </div><!-- card -->
        </div>


    </div>


    @if(!empty($searchtype))

    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body pd-b-0">
                    <div class="row">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>CAWANGAN</th>
                                    <th>NAMA</th>
                                    <th>K/P</th>
                                    <th>EMEL</th>
                                    <th>NO. TEL.</th>
                                    <th>STATUS</th>
                                    <th>JUMLAH SURAT</th>
                                    <th>TINDAKAN</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse($users as $user)
                                <tr>
                                    <td>{{ strtoupper($user->parliament) }}</td>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->ic }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>{{ $user->mobile_no }}</td>
                                    <td>{!! Helper::status_text($user->status) !!}</td>
                                    <td><a href="javascript:open_user_history_letter('{{ $user->name }}','{{ $user->ic }}')">{!! LetterHelper::count_user_letter($user->ic) !!}</a></td>
                                    <td>
                                        @if($letter_id == "")
                                        Sila Pilih Surat
                                        @else
                                        <a href="javascript:sample_fun('{{ $letter_id  }}','{{ $user->ic }}')">Sampel</a> | <a href="javascript:send_to_user('{{ $letterdetail->subject }}','{{ $user->name }}','{{ $user->ic }}','{{ $user->email }}','{{ $letter_id  }}')">Hantar</a>
                                        @endif
                                    </td>
                                </tr>

                                @empty
                                <tr>
                                    <td colspan="6">Tiada Rekod</td>
                                </tr>
                                @endforelse
                            </tbody>
                        </table>

                    </div>

                </div><!-- card-body -->
            </div><!-- card -->
        </div>


    </div>
</div>

<!-- The Modal -->
<div class="modal" id="user_letter_modal">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title" id="modal_letter_header_name"></h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body" id="modal_body">
                Sedang di proses
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Tutup</button>
            </div>

        </div>
    </div>
</div>

@endif
@endsection

@section('js')
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script>

<script>
    $(document).ready(function() {
        $('.select2').select2({
            theme: 'bootstrap4'
        });
    });
    $('#letters').val('{{ $letter_id }}');
    $('#cawangan').val('{{ $cawangan }}');
    $('#statusss').val('{{ $status }}');

	$('#statusss_aff').val('{{ $status }}');

    function search_personal() {
        var search = $('#search').val();
        var letter_id = $('#letters').val();
        window.location = "/admin/letter/send?searchtype=single&search=" + search + "&letter_id=" + letter_id + "";
    }

    function search_filter() {
        var cawangan = $('#cawangan').val();
        var statusss = $('#statusss').val();
        var letter_id = $('#letters').val();
        window.location = "/admin/letter/send?searchtype=filter&cawangan=" + cawangan + "&status=" + statusss + "&letter_id=" + letter_id + "";
    }


    function search_afflites() {

        var statusss = $('#statusss_aff').val();
        var letter_id = $('#letters').val();
        window.location = "/admin/letter/send?searchtype=filteraff&status=" + statusss + "&letter_id=" + letter_id + "";
    }


    function sample_fun(id, ic) {
        Swal.fire({
            html: `
		  <div align='left' >
		  <ul style="margin-left:100px;">
		  <li><a target="_blank" href="/admin/letter/preview?id=` + id + `&type=preview&ic=` + ic + `&header=false">Preview No Letterhead</a></li>
		  <li><a target="_blank"  href="/admin/letter/preview?id=` + id + `&type=preview&ic=` + ic + `&header=true">Preview With Letterhead</a></li>
		  <li><a target="_blank"  href="/admin/letter/preview?id=` + id + `&type=print&ic=` + ic + `&header=false">Print No Letterhead</a></li>
		  <li><a target="_blank"  href="/admin/letter/preview?id=` + id + `&type=print&ic=` + ic + `&header=true">Print With Letterhead</a></li>
		  <li><a target="_blank"  href="/admin/letter/preview?id=` + id + `&type=pdf&ic=` + ic + `&header=false">PDF No Letterhead</a></li>
		  <li><a target="_blank"  href="/admin/letter/preview?id=` + id + `&type=pdf&ic=` + ic + `&header=true">PDF With Letterhead</a></li>
		  </ul>
		  </div>
		  `
        });
    }


    function send_to_filer() {
        var cawangan = $('#cawangan').val();
        var statusss = $('#statusss').val();
        var leter_id = $('#letters').val();

var statusssaff = $('#statusss_aff').val();

        var subject = $('#letters option:selected').text();

        var statuslbl = "SEMUA STATUS";

        var cawanganlbl = "SEMUA CAWANGAN";

        if (statusss == 1) {
            statuslbl = "DILULUSKAN";
        }
        if (statusss == 2) {
            statuslbl = "DIPROSESS";
        }
        if (statusss == 99) {
            statuslbl = "DITOLAK";
        }

        if (cawangan == "") {
            cawanganlbl = "SEMUA CAWANGAN";
        } else {
            cawanganlbl = cawangan.toUpperCase();
        }




        Swal.fire({
            title: 'Pengesahan'
            , html: `Adakah anda menghantar emel kepada semua pengguna dibawah <br><br>
	@if($searchtype == "filteraff")

		 <div align='left' >
		  <table class="table">
		  		<tr>
		  			<td>Afflites</td>
		  			<td>:</td>
		  			<td><b>Semua Affliates</b></td>
		  		</tr>
		  		<tr>
		  			<td>Status</td>
		  			<td>:</td>
		  			<td><b>` + statuslbl + `</b></td>
		  		</tr>
		  		<tr>
		  			<td>Surat</td>
		  			<td>:</td>
		  			<td><b>` + subject + `</b></td>
		  		</tr>
		  </table>
		  <select class="form-control" id="header_box_2">
			  		<option value="false">Tiada Header</option>
			  		<option value="true">Dengan Header</option>
			  </select>
		  </div>

	@else
			  <div align='left' >
		  <table class="table">
		  		<tr>
		  			<td>Cawangan</td>
		  			<td>:</td>
		  			<td><b>` + cawanganlbl + `</b></td>
		  		</tr>
		  		<tr>
		  			<td>Status</td>
		  			<td>:</td>
		  			<td><b>` + statuslbl + `</b></td>
		  		</tr>
		  		<tr>
		  			<td>Surat</td>
		  			<td>:</td>
		  			<td><b>` + subject + `</b></td>
		  		</tr>
		  </table>
		  <select class="form-control" id="header_box_2">
			  		<option value="false">Tiada Header</option>
			  		<option value="true">Dengan Header</option>
			  </select>
		  </div>
	@endif
			  `
            , icon: 'warning'
            , showCancelButton: true
            , confirmButtonColor: '#3085d6'
            , cancelButtonColor: '#d33'
            , confirmButtonText: 'Hantar'
        }).then((result) => {
            if (result.isConfirmed) {

                var header_box = $('#header_box_2').val();

                var data = {
                    _token: "{{ csrf_token() }}"
                    , id: leter_id
                    , subject: '' + subject + ''
                    , cawangan: cawangan
                    , cawanganlbl: cawanganlbl
                    , status: "" + statusss + ""
                    , statuslbl: "" + statuslbl + ""
                    , statusaff: "" + statusssaff + ""
                    , header: "" + header_box + ""
                    , filter: "{{ $searchtype }}"
                };


                $.post("/admin/letter/send/group", data).done(function(datas) {

                    window.location = "/admin/letter/jobs";

                });

            }
        });

    }


    function send_to_user(subject, nama, ic, emails, letter_id) {
        Swal.fire({
            title: 'Pengesahan'
            , html: `Adakah anda ingin Emel Dokumen Ini <br><br>

			  <table class="table">
			  <tr>
			  	<td>Surat</td>
			  	<td>:</td>
			  	<td align="left"><b>` + subject + `</b></td>
			  </tr>
			  <tr>
			  	<td>Nama</td>
			  	<td>:</td>
			  	<td align="left"><b>` + nama + `</b></td>
			  </tr>
			  <tr>
			  	<td>Emel</td>
			  	<td>:</td>
			  	<td align="left"><b>` + emails + `</b></td>
			  </tr>
			  <tr>
			  	<td>K/P</td>
			  	<td>:</td>
			  	<td align="left"><b>` + ic + `</b></td>
			  </tr>

			  </table>

			  <select class="form-control" id="header_box">
			  		<option value="false">Tiada Header</option>
			  		<option value="true">Dengan Header</option>
			  </select>

			  `
            , icon: 'warning'
            , showCancelButton: true
            , confirmButtonColor: '#3085d6'
            , cancelButtonColor: '#d33'
            , confirmButtonText: 'Hantar'
        }).then((result) => {
            if (result.isConfirmed) {

                var header_box = $('#header_box').val();

                var data = {
                    _token: "{{ csrf_token() }}"
                    , id: letter_id
                    , emailtosend: '' + emails + ''
                    , ic: ic
                    , header: "" + header_box + ""
                };


                $.post("/admin/letter/send/user", data).done(function(datas) {
                    Swal.fire(
                        'Berjaya Dihantar'
                        , 'Emel telah berjaya dihantar ke ' + emails + ''
                        , 'success'
                    );
                });

            }
        });
    }

    function open_user_history_letter(name, ic) {
        $('#modal_letter_header_name').html(name);
        $('#user_letter_modal').modal('show');

        $("#modal_body").load("/admin/letter/user?ic=" + ic + "");
    }

</script>

@endsection
