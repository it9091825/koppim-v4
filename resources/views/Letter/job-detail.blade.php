@extends('admin.layouts')
@css
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/jquery-datetimepicker@2.5.21/jquery.datetimepicker.css">
@endcss
@pagetitle(['title'=>'Senarai Job','links' => ['Surat']])@endpagetitle
@section('content')

<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body pd-b-0">
                    Cron Job URL : <a target="_blank" href="https://{{ env('ADMIN_INVESTOR_DOMAIN') }}/admin/letter/cron/jobs">https://{{ env('ADMIN_INVESTOR_DOMAIN') }}/admin/letter/cron/jobs</a>
                </div>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body pd-b-0">

                    <table class="table">
                        <thead>
                            <tr>
                                <th>Filter</th>
                                <th>Surat</th>
                                <th>Progress</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            
                            <tr>
                                <td>
                                    CAWANGAN : <b>{{ $job->cawanganlbl }}</b> <br> STATUS : <b>{{ $job->status_lbl }}</b>
                                </td>
                                <td>{{ $job->subject }}</td>
                                <td>{{ LetterHelper::count_progress($job->_id) }} / {{ $job->jumlah_pengguna }}</td>
                                <td>{{ $job->cron_status }}</td>
                            </tr>
                            
                        </tbody>
                    </table>

                </div><!-- card-body -->
            </div><!-- card -->
            
            <div class="card">
                <div class="card-body pd-b-0">

                    <table class="table">
                            <thead>
                                <tr>
                                    <th>NO RUJUKAN SURAT</th>
                                    <th>NAMA</th>
                                    <th>K/P</th>
                                    <th>EMEL</th>
                                    <th>NO. TEL.</th>
                                    <th>STATUS</th>
                                    <th>TINDAKAN</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse($users as $user)
                                <tr>
                                    <td>{{ $user->letter_ref_no }}</td>
                                    <td>{{ Helper::get_user_from_ic($user->ic)->name }}</td>
                                    <td><a href="/admin/members/view/{{ Helper::get_user_from_ic($user->ic)->id }}">{{ $user->ic }}</a></td>
                                    <td>{{ $user->email }}</td>
                                    <td>{{ Helper::get_user_from_ic($user->ic)->mobile_no }}</td>
                                    <td>{!! $user->status !!}</td>
                                    <td>
                                        <a target="_blank" href="/admin/letter/preview/user?ref_no={{ $user->letter_ref_no }}&type=preview">Lihat Surat</a>
                                    </td>
                                </tr>

                                @empty
                                <tr>
                                    <td colspan="6">Tiada Rekod</td>
                                </tr>
                                @endforelse
                            </tbody>
                        </table>
						{{ $users->appends(['id' => $job->_id])->links() }}
                </div><!-- card-body -->
            </div><!-- card -->
            
        </div>
    </div>
</div>

<!-- The Modal -->
<div class="modal" id="user_letter_modal">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title" id="modal_letter_header_name"></h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body" id="modal_body">
                Sedang di proses
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Tutup</button>
            </div>

        </div>
    </div>
</div>

@endsection

@section('js')
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script>

<script>
    $(document).ready(function() {
        $('.select2').select2({
            theme: 'bootstrap4'
        });
    });
    $('#letters').val('{{ $job->letter_id }}');
    $('#cawangan').val('{{ $job->cawanganlbl }}');
    $('#statusss').val('{{ $job->status_lbl }}');

    function search_personal() {
        var search = $('#search').val();
        var letter_id = $('#letters').val();
        window.location = "/admin/letter/send?searchtype=single&search=" + search + "&letter_id=" + letter_id + "";
    }

    function search_filter() {
        var cawangan = $('#cawangan').val();
        var statusss = $('#statusss').val();
        var letter_id = $('#letters').val();
        window.location = "/admin/letter/send?searchtype=filter&cawangan=" + cawangan + "&status=" + statusss + "&letter_id=" + letter_id + "";
    }

    function sample_fun(id, ic) {
        Swal.fire({
            html: `
		  <div align='left' >
		  <ul style="margin-left:100px;">
		  <li><a target="_blank" href="/admin/letter/preview?id=` + id + `&type=preview&ic=` + ic + `&header=false">Preview No Letterhead</a></li>
		  <li><a target="_blank"  href="/admin/letter/preview?id=` + id + `&type=preview&ic=` + ic + `&header=true">Preview With Letterhead</a></li>
		  <li><a target="_blank"  href="/admin/letter/preview?id=` + id + `&type=print&ic=` + ic + `&header=false">Print No Letterhead</a></li>
		  <li><a target="_blank"  href="/admin/letter/preview?id=` + id + `&type=print&ic=` + ic + `&header=true">Print With Letterhead</a></li>
		  <li><a target="_blank"  href="/admin/letter/preview?id=` + id + `&type=pdf&ic=` + ic + `&header=false">PDF No Letterhead</a></li>
		  <li><a target="_blank"  href="/admin/letter/preview?id=` + id + `&type=pdf&ic=` + ic + `&header=true">PDF With Letterhead</a></li>
		  </ul>
		  </div>
		  `
        });
    }


    function send_to_filer() {
        var cawangan = $('#cawangan').val();
        var statusss = $('#statusss').val();
        var leter_id = $('#letters').val();



        var subject = $('#letters option:selected').text();

        var statuslbl = "SEMUA STATUS";

        var cawanganlbl = "SEMUA CAWANGAN";

        if (statusss == 1) {
            statuslbl = "DILULUSKAN";
        }
        if (statusss == 2) {
            statuslbl = "DIPROSESS";
        }
        if (statusss == 99) {
            statuslbl = "DITOLAK";
        }

        if (cawangan == "") {
            cawanganlbl = "SEMUA CAWANGAN";
        } else {
            cawanganlbl = cawangan.toUpperCase();
        }




        Swal.fire({
            title: 'Pengesahan'
            , html: `Adakah anda menghantar emel kepada semua pengguna dibawah <br><br>

			  <div align='left' >
		  <table class="table">
		  		<tr>
		  			<td>Cawangan</td>
		  			<td>:</td>
		  			<td><b>` + cawanganlbl + `</b></td>
		  		</tr>
		  		<tr>
		  			<td>Status</td>
		  			<td>:</td>
		  			<td><b>` + statuslbl + `</b></td>
		  		</tr>
		  		<tr>
		  			<td>Surat</td>
		  			<td>:</td>
		  			<td><b>` + subject + `</b></td>
		  		</tr>
		  </table>
		  <select class="form-control" id="header_box_2">
			  		<option value="false">Tiada Header</option>
			  		<option value="true">Dengan Header</option>
			  </select>
		  </div>

			  `
            , icon: 'warning'
            , showCancelButton: true
            , confirmButtonColor: '#3085d6'
            , cancelButtonColor: '#d33'
            , confirmButtonText: 'Hantar'
        }).then((result) => {
            if (result.isConfirmed) {

                var header_box = $('#header_box_2').val();

                var data = {
                    _token: "{{ csrf_token() }}"
                    , id: leter_id
                    , subject: '' + subject + ''
                    , cawangan: cawangan
                    , cawanganlbl: cawanganlbl
                    , status: "" + statusss + ""
                    , statuslbl: "" + statuslbl + ""
                    , header: "" + header_box + ""
                };


                $.post("/admin/letter/send/group", data).done(function(datas) {

                    window.location = "/admin/letter/jobs";

                });

            }
        });

    }


    function send_to_user(subject, nama, ic, emails, letter_id) {
        Swal.fire({
            title: 'Pengesahan'
            , html: `Adakah anda ingin Emel Dokumen Ini <br><br>

			  <table class="table">
			  <tr>
			  	<td>Surat</td>
			  	<td>:</td>
			  	<td align="left"><b>` + subject + `</b></td>
			  </tr>
			  <tr>
			  	<td>Nama</td>
			  	<td>:</td>
			  	<td align="left"><b>` + nama + `</b></td>
			  </tr>
			  <tr>
			  	<td>Emel</td>
			  	<td>:</td>
			  	<td align="left"><b>` + emails + `</b></td>
			  </tr>
			  <tr>
			  	<td>K/P</td>
			  	<td>:</td>
			  	<td align="left"><b>` + ic + `</b></td>
			  </tr>

			  </table>

			  <select class="form-control" id="header_box">
			  		<option value="false">Tiada Header</option>
			  		<option value="true">Dengan Header</option>
			  </select>

			  `
            , icon: 'warning'
            , showCancelButton: true
            , confirmButtonColor: '#3085d6'
            , cancelButtonColor: '#d33'
            , confirmButtonText: 'Hantar'
        }).then((result) => {
            if (result.isConfirmed) {

                var header_box = $('#header_box').val();

                var data = {
                    _token: "{{ csrf_token() }}"
                    , id: letter_id
                    , emailtosend: '' + emails + ''
                    , ic: ic
                    , header: "" + header_box + ""
                };


                $.post("/admin/letter/send/user", data).done(function(datas) {
                    Swal.fire(
                        'Berjaya Dihantar'
                        , 'Emel telah berjaya dihantar ke ' + emails + ''
                        , 'success'
                    );
                });

            }
        });
    }

    function open_user_history_letter(name, ic) {
        $('#modal_letter_header_name').html(name);
        $('#user_letter_modal').modal('show');

        $("#modal_body").load("/admin/letter/user?ic=" + ic + "");
    }

</script>

@endsection
