@extends('layouts.main')
@section('page-css')

<style>
    .btn-group button,
    .btn {
        background-color: #3fac87;
    }

    .input-group-text,
    .form-control,
    .btn {
        border-radius: 0;
    }

    .btn {
        border-color: inherit;
    }

    input[type="text"]:disabled {
        background-color: white;
    }

</style>


@endsection
@section('content')
<div class="card">
    <div class="card-body">
        <img src="{{url('/img/header-new.jpeg')}}" class="img-fluid rounded mx-auto d-block" alt="Sumbangan PPIM Banner">
        <div style="margin-top:20px;margin-bottom:20px;text-align:center">
            <h4>Sila Semak Maklumat Sebelum Meneruskan Transaksi.</h4>
        </div>
        <form method="POST" action="{{ $url }}">


            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text" id="btnGroupAddon2" style="background-color: #3fac87; color: white;">JUMLAH</div>
                    </div>
                    <input type="text" class="form-control" value="RM {{ $donation->amount }}" disabled name="amount" id="amount" aria-describedby="btnGroupAddon2">
                </div>
            </div>

            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text" id="btnGroupAddon2" style="background-color: #3fac87; color: white;">NAMA</div>
                    </div>
                    <input type="text" class="form-control" value="{{ $donation->name }}" disabled name="name" id="name" aria-describedby="btnGroupAddon2">
                </div>
            </div>

            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text" id="btnGroupAddon2" style="background-color: #3fac87; color: white;">NO. K/P</div>
                    </div>
                    <input type="text" class="form-control" value="{{ $donation->ic }}" disabled name="ic" id="ic" aria-describedby="btnGroupAddon2">
                </div>
            </div>

            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text" id="btnGroupAddon2" style="background-color: #3fac87; color: white;">NO. TEL.</div>
                    </div>
                    <input type="text" class="form-control" value="{{ $donation->phone }}" disabled name="phone" id="phone" aria-describedby="btnGroupAddon2">
                </div>
            </div>

            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text" id="btnGroupAddon2" style="background-color: #3fac87; color: white;">EMEL</div>
                    </div>
                    <input type="text" class="form-control" disabled value="{{ ($donation->email == '') ? '' : $donation->email }}" name="email" id="email" aria-describedby="btnGroupAddon2">

                </div>
                <small class="form-text text-muted" align="center">* Setiap Transaksi Dikenakan Caj Perkhidmatan RM1.00 Oleh Penyedia Gerbang Pembayaran, kiplePay.
                    <br>* Dengan Menyumbang, Anda Bersetuju Dengan Terma & Syarat Yang Ditetapkan.</small>
            </div>






            <div>
                <input type="hidden" name="ord_date" value="{{$donation->created_at}}">
                <input type="hidden" name="ord_totalamt" value="{{$amount}}">
                <input type="hidden" name=" ord_gstamt" value="0.00">
                <input type="hidden" name="ord_shipname" value="{{$donation->name}}">
                <input type="hidden" name="ord_shipcountry" value="Malaysia">
                <input type="hidden" name="ord_mercref" value="{{$donation->order_ref}}">
                <input type="hidden" name="ord_telephone" value="{{$donation->phone}}">
                <input type="hidden" name="ord_email" value="{{ ($donation->email == '') ? '' : $donation->email }}">
                <input type="hidden" name="ord_delcharges" value="0.00">
                <input type="hidden" name="ord_svccharges" value="0.00">
                <input type="hidden" name="ord_mercID" value="{{$merchant}}">
                <input type="hidden" name="merchant_hashvalue " value="{{$hashValue}}">
                <input type="hidden" name="ord_returnURL" value="{{$returnUrl}}">
            </div>

            <div style="text-align:center">
                <img class="bid_plate img-fluid" src="{{url('/img/kiplePay.png')}}" alt=""> <br>
            </div>

            <div style="margin-top:10px;margin-bottom:10px;text-align:center">
                <button type="submit" class="btn btn-primary">SAYA IKHLAS MENYUMBANG</button>
            </div>

        </form>
    </div>
</div>
@endsection

@section('page-js')
<script src="{{url('/js/jquery.mask.js')}}"></script>
<script type="text/javascript" src="{{url('')}}/js/bootstrap-inputmask.min.js"></script>
<script>

</script>
@endsection
