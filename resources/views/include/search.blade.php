<div class="col-lg-6">
    <div class="input-group mb-3">
        <input type="text" id="search" class="form-control" value="{!! $search ?? ''!!}" placeholder="{!! isset($placeholder) ? strtoupper($placeholder):'' !!}">
        <div class="input-group-append">
            <button class="btn btn-primary font-weight-bold submit" onclick="search_data()">CARI</button>
        </div>
    </div>
</div>

@push('script')

<script>
    function search_data() {
        var search = $('#search').val();

        window.location.href = "{{url($url ?? '')}}?search=" + search + "";
    }

    $(document).on("keypress", ".input-group:has(input:input, span.input-group-btn:has(.btn)) input:input", function(e) {
        if (e.which == 13) {
            $(this).closest(".input-group").find(".input-group-append > .submit").click();
        }
    });

</script>
@endpush
