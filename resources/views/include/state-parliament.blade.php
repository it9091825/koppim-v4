<div class="form-group">
    <label for="" class="form-label">Pilih Negeri</label>
    <select name="states" id="states" class="states form-control select2"></select>
</div>
<div class="form-group">
    <label for="" class="form-label">Pilih Cawangan Parlimen</label>
    <select name="parliaments" id="parliaments" class="parliaments form-control select2"></select>
</div>
@push('script')
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });


    $(document).ready(function() {
        var permohonan_states = $('#permohonan_states');
        getStatesList();
    });

    getParliamentListByState()

    function getStatesList() {

        $.ajax({
            type: "GET"
            , url: "/api/states"
            , success: function(res) {
                if (res) {
                    $("#states").empty();
                    $("#states").append('<option value="all">SEMUA NEGERI</option>');
                    $.each(res, function(key, value) {
                        $("#states").append('<option value="' + value + '">' + key + '</option>');
                    });

                } else {
                    $("#state").empty();
                }
            }
        });

    }

    function getParliamentListByState() {

        $('#states').on('change', function() {
            var state_id = $(this).val();

            if (state_id) {
                $.ajax({
                    type: "GET"
                    , url: "/api/state/parliaments?state_id=" + state_id
                    , success: function(res) {
                        if (res) {
                            $("#parliaments").empty();
                            if (res.name != 'LUAR NEGARA') {
                                $('#parliaments').append('<option value="all">SEMUA CAWANGAN PARLIMEN</option>');
                                $.each(res, function(key, value) {
                                    $("#parliaments").append('<option value="' + value + '">' + value + '</option>');
                                });
                            } else {
                                $("#parliaments").empty();
                            }
                        } else {
                            $("#parliaments").empty();
                        }
                    }
                });
            } else {
                $("#parliaments").empty();
            }

        });
    }

</script>
@endpush
