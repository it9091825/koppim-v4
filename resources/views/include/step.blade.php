 @if(URL::current() == url('langganan-syer') || URL::current() == url('daftar') || URL::current() == url('langganan-syer/pengesahan') || URL::current() == url('langganan-syer/pembayaran-manual'))
<div class="stepwizard">
    <div class="stepwizard-row setup-panel">
        <div class="stepwizard-step">
            <a href="javascript:void;" type="button" class="btn
            @if(URL::current() == url('langganan-syer') || URL::current() == url('daftar'))
                btn-primary
            @else
                btn-default
            @endif
             btn-circle">1</a>
            <p>Pendaftaran</p>
        </div>
        <div class="stepwizard-step">
            <a href="javascript:void;" type="button" class="btn
            @if(URL::current() == url('langganan-syer/pengesahan'))
                btn-primary
            @else
                btn-default
            @endif

            btn-circle" disabled="disabled">2</a>
            <p>Pengesahan</p>
        </div>
        <div class="stepwizard-step">
            <a href="javascript:void;" type="button" class="btn
            @if(URL::current() == url('langganan-syer/pembayaran-manual'))
                btn-primary
            @else
            btn-default
            @endif
            btn-circle" disabled="disabled">3</a>
            <p>Pembayaran</p>
        </div>
    </div>
</div>
@endif
