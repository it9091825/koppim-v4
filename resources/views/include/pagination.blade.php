<div class="col-lg-12">
    <div class="d-flex justify-content-between align-items-center">
        <div class="d-flex justify-content-center align-items-center mb-3">
            @if($results)
            Paparan {{number_format($results->firstItem(),0,"",",")}} Hingga {{ number_format($results->lastItem(),0,"",",")}} Daripada {{number_format($results->total(),0,"",",")}} Dalam Senarai
            @endif
        </div>
        <div class="d-flex justify-content-center align-items-center">
            {{ $results->appends(request()->query())->links() }}
        </div>
    </div>
</div>