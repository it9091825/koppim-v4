
<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Twitter -->
    <meta name="twitter:site" content="@themepixels">
    <meta name="twitter:creator" content="@themepixels">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="Slim">
    <meta name="twitter:description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="twitter:image" content="http://themepixels.me/slim/img/slim-social.png">

    <!-- Facebook -->
    <meta property="og:url" content="http://themepixels.me/slim">
    <meta property="og:title" content="Slim">
    <meta property="og:description" content="Premium Quality and Responsive UI for Dashboard.">

    <meta property="og:image" content="http://themepixels.me/slim/img/slim-social.png">
    <meta property="og:image:secure_url" content="http://themepixels.me/slim/img/slim-social.png">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="600">

    <!-- Meta -->
    <meta name="description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="author" content="ThemePixels">

    <title>KoPPIM</title>

    <!-- vendor css -->
    <link href="/investor/lib/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="/investor/lib/Ionicons/css/ionicons.css" rel="stylesheet">
    <link href="/investor/lib/jqvmap/css/jqvmap.min.css" rel="stylesheet">
    <link href="/investor/lib/flag-icon-css/css/flag-icon.min.css" rel="stylesheet">
	<link href="/investor/lib/jquery.steps/css/jquery.steps.css" rel="stylesheet">
    <!-- Slim CSS -->
    <link rel="stylesheet" href="/investor/css/slim.css">
    <style>
      /* @media only screen and (min-width:640px){

      } */
      .logged-user .menu-utama{
        display:none;
      }
      @media only screen and (min-width:290px) and (max-width:1020px){
        .logged-user .menu-utama{
          display:inline-block;
        }
      }
    </style>
  </head>
  <body class="dashboard-4">
    <div class="slim-header">
      <div class="container">
        <div class="slim-header-left">
        <h2 class="slim-logo"><a href="{{env('APP_URL')}}"><img src="{{asset('img/logo-koppim.png')}}" width="150" /></a></h2>


        </div><!-- slim-header-left -->
        <div class="slim-header-right">
          <div class="dropdown dropdown-c">
            <a href="#" class="logged-user" data-toggle="dropdown">
              {{-- <img src="/investor/img/userplaceholder.png" alt=""> --}}
              <span class="menu-utama font-weight-bold"><h5>MENU</h5></span>
              <span>{{ strtoupper(Auth::user()->name) }}</span>
              <i class="fa fa-angle-down"></i>
            </a>
            <div class="dropdown-menu dropdown-menu-right">
              <nav class="nav">
                <a href="/profile" class="nav-link"><i class="icon ion-person"></i> Lihat Profil</a>
                <a href="/logout" class="nav-link"><i class="icon ion-forward"></i> Log Keluar</a>
              </nav>
            </div><!-- dropdown-menu -->
          </div><!-- dropdown -->
        </div><!-- header-right -->
      </div><!-- container -->
    </div><!-- slim-header -->



	@yield('navigation')
	<div align="center">
		<div style="max-width: 770px; min-width: 320px; width: 90%;" align="left" >
	 @if($errors->any())



				                 <div style=" padding-top: 10px;">
												<div class="alert alert-danger">
													<ul style=" margin-left: -20px; margin-top: 10px;;">
												   @foreach ($errors->all() as $error)
												      <li style="margin-bottom: 10px;">{!! $error !!}</li>

													  @endforeach
													</ul>
												</div>
									</div>



							@endif

	    @if (session('success'))
				            <div style=" padding-top: 10px;">

											    <div class="alert alert-success" align="center">
											        {{ session('success') }}
											    </div>

				            </div>
							@endif



	</div>
	</div>

   @yield('content')

    <div class="slim-footer">
      <div class="container">
      <p>&copy; KoPPIM Akaun Anggota 2020</p>
        <p>Version: 1.1.9</p>
      </div><!-- container -->
    </div><!-- slim-footer -->

    <script src="/investor/lib/jquery/js/jquery.js"></script>
    <script src="/investor/lib/popper.js/js/popper.js"></script>
    <script src="/investor/lib/bootstrap/js/bootstrap.js"></script>
    <script src="/investor/lib/jquery.cookie/js/jquery.cookie.js"></script>
    <script src="/investor/lib/jqvmap/js/jquery.vmap.min.js"></script>
    <script src="/investor/lib/jqvmap/js/jquery.vmap.world.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <script src="/investor/js/slim.js"></script>
    @yield('js')
  </body>
</html>
