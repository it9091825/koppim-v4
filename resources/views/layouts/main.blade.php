<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="{{ asset('images/cropped-favicon_SADA-01-32x32.png') }}" sizes="32x32">
    
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    
    {{-- <title>{{ config('app.name', 'SADA') }}</title> --}}
    <title>KoPPIM</title>
    @yield('ogshare')
    <!-- Fonts -->
    <link rel="shortcut icon" href="https://www.ppim.org.my/wp-content/uploads/2019/09/Logo-PPIM-Final-Edition-1.png" type="image/x-icon" />
		<link rel="apple-touch-icon" sizes="180x180" href="https://www.ppim.org.my/wp-content/uploads/2019/09/Logo-PPIM-Final-Edition-1.png">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">    
    <!-- Styles -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css2?family=Roboto+Condensed:ital,wght@0,300;0,400;0,700;1,300;1,400;1,700&display=swap" rel="stylesheet">
    {{-- <link href="{{ asset('vendor/select2/dist/css/select2.min.css" rel="stylesheet') }}" /> --}}
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/css/select2.min.css" rel="stylesheet" />
    
    
    
    <style>
	    
	    body {
        font-family: 'Roboto Condensed', serif;
      }
	    
		.image {
  opacity: 1;
  display: block;
  width: 100%;
  height: auto;
  transition: .5s ease;
  backface-visibility: hidden;
}

.middle {
	width: 70%;
  transition: .5s ease;
  opacity: 0;
  position: absolute;
  top: 25%;
  left: 50%;
  transform: translate(-50%, -50%);
  -ms-transform: translate(-50%, -50%);
  text-align: center;
}

.imgcontainer:hover .image {
  opacity: 0.3;
}

.imgcontainer:hover .middle {
  opacity: 1;
}


	</style>
    <style>
.btn-group,.btn-primary {
			
			color: white;
		}
		
.btn {
			background-color: #22445d;
			color: white;
			border-color: white;
			border-radius: 0px;
		}
		
.form-control, .input-group-text {
  border-radius: 0px;
}				
.active, .btn:hover {
  background-color: #3fac87;
  color: white;
}

.btn-group button:active {
   background-color: #3fac87;
  color: white;
}


h6 a{
	color: black;
	font-weight: bold;
	font-size: 20px;
}
		
</style>
    @yield('page-css')        
    
    <!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window,document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
 fbq('init', '1356408217865862'); 
fbq('track', 'PageView');
</script>
<noscript>
 <img height="1" width="1" 
src="https://www.facebook.com/tr?id=1356408217865862&ev=PageView
&noscript=1"/>
</noscript>
<!-- End Facebook Pixel Code -->
    @yield('jsfb')
    
</head>
<body>
    <div id="app">
        <main class="py-4">
            <div class="container">
                @yield('content')
                
               
                
            </div>
        </main>
    </div>
    
    <!-- script -->
    {{-- <script src="{{ asset('vendor/jquery.min.js') }}"></script>
    <script defer src="{{ asset('vendor/fontawesome-free-5.8.2-web/js/all.min.js') }}"></script> --}}
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
    {{-- <script src="{{ asset('vendor/select2/dist/js/select2.min.js') }}"></script> --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.7/js/select2.min.js"></script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-151526150-2"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-151526150-2');
</script>

    @yield('page-js')
</body>
</html>
