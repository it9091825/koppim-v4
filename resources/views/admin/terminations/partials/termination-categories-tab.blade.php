<ul class="nav nav-pills navtab-bg nav-justified mb-4" id="pills-tab" role="tablist">
    <li class="nav-item">
    <a href="{{url('/admin/members/terminations?status=all')}}" class="nav-link text-uppercase {{ Helper::active(url('/admin/members/terminations?status=all')) }} {{Helper::active(url(''))}}">
            Semua
        </a>
    </li>
    <li class="nav-item">
        <a href="{{url('/admin/members/terminations')}}" class="nav-link text-uppercase {{ Helper::active(url('/admin/members/terminations')) }} {{request()->query('status') == '96' ? 'active':''}}" id="pills-tasks-tab">
            Untuk Diproses
        </a>
    </li>

    <li class="nav-item">
        <a href="{{url('/admin/members/terminations?status=97')}}" class="nav-link text-uppercase {{ Helper::active(url('/admin/members/terminations?status=2')) }} {{request()->query('status') == '97' ? 'active':''}}" id="pills-projects-tab">
            Berhenti
        </a>
    </li>
</ul>