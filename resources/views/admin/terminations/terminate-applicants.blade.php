@extends('admin.layouts')
@pagetitle(['title'=>'Senarai Berhenti', 'links' => ['Keanggotaan']])@endpagetitle
@section('content')
<div class="container">
    <div class="row">
        @include('include.search',['url'=>url()->current(),'placeholder'=>'carian menggunakan nama, no. telefon atau no. mykad'])
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="d-flex justify-content-between align-items-center">
                <div class="d-flex justify-content-center align-items-center mb-3">
                    @if($members)
                    Paparan {{number_format($members->firstItem(),0,"",",")}} Hingga {{ number_format($members->lastItem(),0,"",",")}} Daripada {{number_format($members->total(),0,"",",")}} Dalam Senarai
                    @endif
                </div>
                <div class="d-flex justify-content-center align-items-center">
                    {{ $members->appends(request()->query())->links() }}
                </div>
            </div>
        </div>
        <div class="col-lg-12 col-xl-12">
            <div class="card">
                <div class="card-body">
                    @include('admin.terminations.partials.termination-categories-tab')
                    @if(request()->query('status') == null)
                        <div class="table-responsive">
                            <table class="table">
                                <thead class="thead-primary">
                                    <tr>
                                        <th class="text-center">@sortablelink('updated_at', strtoupper('tarikh permohonan'))</th>
                                        <th>@sortablelink('name', strtoupper('nama'))</th>
                                        <th class="text-center">NO. Anggota KoPPIM</th>
                                        <th class="text-center">NO. MYKAD</th>
                                        <th class="text-center">STATUS KEANGGOTAAN</th>
                                        <th class="text-center">SEBAB BERHENTI</th>
                                        <th class="text-center">SURAT BERHENTI</th>
                                        <th class="text-center">TINDAKAN</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse($members as $member)
                                    <tr>
                                        <td class="text-center">{!! $member->updated_at->format("d/m/Y \<\b\\r\> h:i:sa") !!}</td>
                                        <td><a href="{{route('admin.member.view',$member->id)}}">{{ $member->name }}</a></td>
                                        <td class="text-center">{{ $member->no_koppim ?? 'TIADA NO. ANGGOTA KoPPIM' }}</td>
                                        <td class="text-center"><a href="{{route('admin.member.view',$member->id)}}">{{ $member->ic }}</a></td>
                                        <td class="text-center">{!! Helper::status_text($member->status) !!}
                                        </td>
                                        <td class="text-center">
                                            @php
                                                $leave_reason = App\user_terminations::where('user_id', $member->id)->value('leave_reason');
                                            @endphp
                                            {!! $leave_reason !!}
                                        </td>
                                        <td class="text-center">
                                            @php
                                                $termination_letter = App\user_terminations::where('user_id', $member->id)->value('termination_letter');
                                            @endphp
                                            <a target="_blank" href="{{ env('DO_SPACES_FULL_URL').$termination_letter }}">[SURAT BERHENTI]</a>
                                        </td>
                                        @if($member->status == 96)
                                            <td class="text-center">
                                                <form action="{{ route('admin.member.termination.send') }}" method="POST">
                                                    @csrf
                                                    <input type="hidden" name="user_id" value="{{$member->id}}">
                                                    <button style="border: none;padding: 0;background: none;"
                                                                name="type" type="submit" class="btn btn-info font-bold"
                                                                value="lulus">
                                                    <span data-toggle="tooltip" data-placement="auto" data-original-title="Luluskan Pemberhentian" class="text-success">
                                                    <svg class="w-6 h-6" fill="none" stroke="currentColor"
                                                        viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path
                                                            stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                                            d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z"></path></svg></span>
                                                    </button>
                                                    <button style="border: none;padding: 0;background: none;"
                                                                name="type" type="submit" class="btn btn-info font-bold"
                                                                value="gagal">
                                                    <span data-toggle="tooltip" data-placement="auto"
                                                        data-original-title="Batalkan Pemberhentian" class="text-danger">
                                                        <svg class="w-6 h-6" fill="none" stroke="currentColor"
                                                            viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path
                                                                stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                                                d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z"></path></svg></span>
                                                    </button>
                                                </form>
                                            </td>
                                        @else
                                            <td class="text-center">
                                                <form action="{{ route('admin.member.termination.revoke') }}" method="POST">
                                                    @csrf
                                                    <input type="hidden" name="user_id" value="{{$member->id}}">
                                                    <button style="border: none;padding: 0;background: none;"
                                                                name="type" type="submit" class="btn btn-info font-bold"
                                                                value="lulus">
                                                    <span data-toggle="tooltip" data-placement="auto" data-original-title="Aktifkan Semula Anggota" class="text-success">
                                                    <svg class="w-6 h-6" fill="none" stroke="currentColor"
                                                        viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path
                                                            stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                                            d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z"></path></svg></span>
                                                    </button>
                                                </form>
                                            </td>
                                        @endif
                                    </tr>
                                    @empty
                                    @tablenoresult
                                    @endtablenoresult
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                    @elseif(request()->query('status') == 97)
                        <div class="table-responsive">
                            <table class="table">
                                <thead class="thead-primary">
                                    <tr>
                                        <th class="text-center">@sortablelink('updated_at', strtoupper('tarikh berhenti'))</th>
                                        <th>@sortablelink('name', strtoupper('nama'))</th>
                                        <th class="text-center">NO. MYKAD</th>
                                        <th class="text-center">STATUS KEANGGOTAAN</th>
                                        <th class="text-center">SEBAB BERHENTI</th>
                                        <th class="text-center">TINDAKAN</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse($members as $member)
                                    <tr>
                                        <td class="text-center">{!! $member->updated_at->format("d/m/Y \<\b\\r\> h:i:sa") !!}</td>
                                        <td><a href="{{route('admin.member.view',$member->id)}}">{{ $member->name }}</a></td>
                                        <td class="text-center"><a href="{{route('admin.member.view',$member->id)}}">{{ $member->ic }}</a></td>
                                        <td class="text-center">{!! Helper::status_text($member->status) !!}
                                        </td>
                                        <td class="text-center">
                                            @php
                                            $leave_reason = App\user_terminations::where('user_id', $member->id)->value('leave_reason');
                                            @endphp
                                            {!! $leave_reason !!}
                                        </td>
                                        <td class="text-center">
                                            <form action="{{ route('admin.member.termination.revoke') }}" method="POST">
                                                @csrf
                                                <input type="hidden" name="user_id" value="{{$member->id}}">
                                                <button style="border: none;padding: 0;background: none;"
                                                            name="type" type="submit" class="btn btn-info font-bold"
                                                            value="lulus">
                                                <span data-toggle="tooltip" data-placement="auto" data-original-title="Aktifkan Semula Anggota" class="text-success">
                                                <svg class="w-6 h-6" fill="none" stroke="currentColor"
                                                     viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path
                                                        stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                                        d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z"></path></svg></span>
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                    @empty
                                    @tablenoresult
                                    @endtablenoresult
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                    @else
                        <div class="table-responsive">
                            <table class="table">
                                <thead class="thead-primary">
                                    <tr>
                                        <th class="text-center">@sortablelink('created_at', strtoupper('tarikh daftar'))</th>
                                        <th>@sortablelink('name', strtoupper('nama'))</th>
                                        <th class="text-center">NO. MYKAD</th>
                                        <th class="text-center">STATUS KEANGGOTAAN</th>
                                        <th class="text-center">LOG MASUK</th>
                                        <th class="text-center">JUMLAH BAYARAN</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse($members as $member)
                                    <tr>
                                        <td class="text-center">{!! $member->created_at->format("d/m/Y \<\b\\r\> h:i:sa") !!}</td>
                                        <td><a href="{{route('admin.member.view',$member->id)}}">{{ $member->name }}</a></td>
                                        <td class="text-center"><a href="{{route('admin.member.view',$member->id)}}">{{ $member->ic }}</a></td>
                                        <td class="text-center">{!! Helper::status_text($member->status) !!}
                                        </td>
                                        <td class="text-center">
                                            @php
                                            $last_login = \Spatie\Activitylog\Models\Activity::where('log_name','user_auth')->where('subject_id',$member->id)->latest()->first();
                                            @endphp
                                            {!! ($last_login) ? $last_login->created_at->format('d/m/Y \<\b\\r\> h:i:sa') : 'TIADA' !!}
                                        </td>
                                        <td class="text-center">
                                            @php
                                            $total = App\Mongo\Payment::where('ic',$member->ic)->where('returncode','100')->sum('amount');
                                            @endphp
                                            RM{{number_format($total,2,".",",")}}
                                        </td>
                                    </tr>
                                    @empty
                                    @tablenoresult
                                    @endtablenoresult
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                    @endif
                </div>

            </div>

        </div>
    </div>
</div>
@endsection
@section('js')
<script>
    function search_data() {
        var search = $('#search').val();

        window.location.href = "{{route('admin.member.termination')}}?search=" + search + "";
    }

    $(document).on("keypress", ".input-group:has(input:input, span.input-group-btn:has(.btn)) input:input", function(e) {
        if (e.which == 13) {
            $(this).closest(".input-group").find(".input-group-append > .submit").click();
        }
    });

</script>

@endsection
