@extends('admin.layouts')
@pagetitle(['title'=>'Telah Dibatalkan','links' => ['Transaksi Bayaran']])@endpagetitle
@section('css')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/jquery-datetimepicker@2.5.21/jquery.datetimepicker.css">
@endsection
@section('content')
    <div class="container">
        <div class="row" style="">
            <div class="col-lg-6">
                <div class="input-group mb-3">
                    <input type="text" id="search" class="form-control" value="{!! $search ?? ''!!}"
                           placeholder="CARIAN MENGGUNAKAN NAMA, NO. TELEFON ATAU NO. MYKAD">
                    <div class="input-group-append">
                        <button class="btn btn-primary font-weight-bold submit" onclick="search_data()">CARI</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="d-flex justify-content-between align-items-center">
                    <div class="d-flex justify-content-center align-items-center mb-3">
                        @if($payments)
                            Paparan {{number_format($payments->firstItem(),0,"",",")}}
                            Hingga {{ number_format($payments->lastItem(),0,"",",")}}
                            Daripada {{number_format($payments->total(),0,"",",")}} Dalam Senarai
                        @endif
                    </div>
                    <div class="d-flex justify-content-center align-items-center">
                        {{ $payments->appends(request()->query())->links() }}
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-xl-12 mg-t-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead class="thead-primary">
                                <tr>
                                    <th>@sortablelink('name','NAMA')</th>
                                    <th class="text-center">NO. Anggota KoPPIM</th>
                                    <th class="text-center">JUMLAH</th>
                                    <th class="text-center">BUKTI PEMBAYARAN</th>
                                    <th class="text-center">TARIKH TRANSAKSI</th>
                                    <th class="text-center">DIBATALKAN OLEH</th>
                                    <th class="text-center">TARIKH DIBATALKAN</th>
                                </tr>
                                </thead>

                                @forelse($payments as $key=>$payment)
                                    <tbody>
                                    <tr>
                                        <td>{{ strtoupper($payment->name) }}</td>
                                        <td class="text-center">{{Helper::get_user_no_anggota_from_mykad_no($payment->ic)}}</td>
                                        <td class="text-center">{{ $payment->amount != 0.00 ? 'RM '.number_format($payment->amount,2,'.',',') : 'MASIH BELUM DIKEMASKINI'}}</td>
                                        <td class="text-center"><a target="_blank"
                                                                   href="{{ env('DO_SPACES_FULL_URL').$payment->payment_proof }}">LAMPIRAN</a>
                                        </td>
                                        <td class="text-center">{{ $payment->created_at->format('d/m/Y H:i') }}</td>
                                        @if($payment->status == 'verification-cancelled')
                                            <td class="text-center">
                                                {{$payment->verified_user ? $payment->verified_user->name : ''}}
                                            </td>
                                            <td class="text-center">
                                                {{$payment->verified_date ?? ''}}
                                            </td>
                                        @else
                                            <td class="text-center">
                                                {{$payment->approved_user ? $payment->approved_user->name : ''}}
                                            </td>
                                            <td class="text-center">
                                                {{$payment->approved_date ?? ''}}
                                            </td>
                                        @endif
                                    </tr>

                                @empty
                                    <tbody>
                                    <td colspan="10" class="text-center font-weight-bold text-uppercase">
                                        <h3>tiada rekod</h3>
                                    </td>
                                    </tbody>
                                @endforelse
                            </table>
                        </div>

                    </div><!-- card-body -->
                </div><!-- card -->
            </div>
        </div>
    </div><!-- container -->


@endsection

@section('js')
    <script>

        function search_data() {
            var search = $('#search').val();
            window.location.href = "{{route('admin.transaksi-bayaran.telah-dibatalkan.index')}}?search=" + search;
        }

        $(document).on("keypress", ".input-group:has(input:input, span.input-group-btn:has(.btn)) input:input", function (e) {
            if (e.which == 13) {
                $(this).closest(".input-group").find(".input-group-append > .submit").click();
            }
        });

    </script>

@endsection
