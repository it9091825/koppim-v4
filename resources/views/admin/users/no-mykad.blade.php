@extends('admin.layouts')

@pagetitle(['title'=>'Senarai Tiada Lampiran MYKAD','links'=>['Keanggotaan']])@endpagetitle

@section('content')
<div class="container">
    <div class="row" style="">
        <div class="col-lg-6">
            <div class="input-group mb-3">
                <input type="text" id="search" class="form-control" value="{!! $search ?? ''!!}" placeholder="CARIAN MENGGUNAKAN NAMA, NO. TELEFON ATAU NO. MYKAD">
                <div class="input-group-append">
                    <button class="btn btn-primary font-weight-bold submit" onclick="search_data()">CARI</button>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="d-flex justify-content-between align-items-center">
                <div class="d-flex justify-content-center align-items-center mb-3">
                    @if($members)
                    Paparan {{number_format($members->firstItem(),0,"",",")}} Hingga {{ number_format($members->lastItem(),0,"",",")}} Daripada {{number_format($members->total(),0,"",",")}} Dalam Senarai
                    @endif
                </div>
                <div class="d-flex justify-content-center align-items-center">
                    {{ $members->appends(request()->query())->links() }}
                </div>
            </div>
        </div>
        {{-- only will display on belum dikemaskini page --}}

        <div class="col-lg-12 col-xl-12">
            <div class="card">
                <div class="card-body">
                    <ul class="nav nav-pills navtab-bg nav-justified mb-4" id="pills-tab" role="tablist">
                        <li class="nav-item">
                            <a href="{{url('/admin/members/no-mykad')}}" class="nav-link text-uppercase {{ Helper::active(url('/admin/members/no-mykad')) }} {{Helper::active(url(''))}}">
                                Semua
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{url('/admin/members/no-mykad?status=0')}}" class="nav-link text-uppercase {{ Helper::active(url('/admin/members/no-mykad?status=0')) }}" id="pills-messages-tab">
                                Belum Dikemaskini
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{url('/admin/members/no-mykad?status=2')}}" class="nav-link text-uppercase {{ Helper::active(url('/admin/members/no-mykad?status=2')) }} {{request()->query('status') == '2' ? 'active':''}}" id="pills-projects-tab">
                                Untuk Disemak
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{url('/admin/members/no-mykad?status=1')}}" class="nav-link text-uppercase {{ Helper::active(url('/admin/members/no-mykad?status=1')) }} {{request()->query('status') == '1' ? 'active':''}}" id="pills-tasks-tab">
                                Diluluskan
                            </a>
                        </li>
                    </ul>

                    <div class="table-responsive">
                        <table class="table">
                            <thead class="thead-primary">
                                <tr>

                                    <th class="text-center">@sortablelink('created_at', new HtmlString('TARIKH<br>DAFTAR'))</th>
                                    <th>@sortablelink('name', strtoupper('nama'))</th>
                                    <th class="text-center">NO. Anggota KoPPIM</th>
                                    <th class="text-center">NO. MYKAD</th>
                                    <th class="text-center">STATUS ANGGOTA</th>
                                    <th class="text-center">TINDAKAN</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse($members as $key=> $member)
                                <tr>
                                    {{-- only will display on belum dikemaskini page --}}
                                    <td class="text-center">{!! $member->created_at->translatedFormat("d/m/Y \<\b\\r\> h:i:sa") !!}</td>
                                    <td><a href="{{route('admin.member.view',$member->id)}}">{{ $member->name }}</a></td>
                                    <td class="text-center">{{ $member->no_koppim ?? 'TIADA NO. ANGGOTA KoPPIM' }}</td>
                                    <td class="text-center"><a href="{{route('admin.member.view',$member->id)}}">{{ $member->ic }}</a></td>
                                    <td class="text-center">
                                        {!! Helper::status_text($member->status) !!}
                                    </td>
                                    <td class="text-center">
                                        @if(App\Helpers\AnggotaHelper::check_user_profile_filled_except_mykad($member) && $member->status == 0)
                                            <span data-toggle="tooltip" data-placement="auto" data-original-title="Tukar Ke Status Untuk Disemak" class="btn">
                                            <svg data-toggle="modal" data-route="{{route('admin.member.change.status.untuk.disemak',$member)}}" data-member="'{{$member}}'" data-name="{!!$member->name!!}" data-ic="{{$member->ic}}" data-target="#modalChangeStatus" class="w-6 h-6 text-info" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 5H7a2 2 0 00-2 2v12a2 2 0 002 2h10a2 2 0 002-2V7a2 2 0 00-2-2h-2M9 5a2 2 0 002 2h2a2 2 0 002-2M9 5a2 2 0 012-2h2a2 2 0 012 2m-6 9l2 2 4-4"></path>
                                            </svg>
                                            </span>
                                            <div class="modal" tabindex="-1" id="modalChangeStatus" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog modal-lg">
                                                    <div class="modal-content">
                                                        <div class="modal-body">
                                                            <form enctype="multipart/form-data" method="post" class="deleteForm">
                                                                @csrf
                                                                <h4>Adakah anda pasti mahu tukar status anggota ini kepada untuk disemak?</h4>
                                                                <h5><span class="text-danger"></span></h5>
                                                                <table class="table table-bordered">
                                                                    <tbody>
                                                                    <tr>
                                                                        <td class="font-weight-bold">Nama</td>
                                                                        <td><span class="name"></span></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td class="font-weight-bold">No. MYKAD</td>
                                                                        <td><span class="ic"></span></td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>
                                                                <button class="btn btn-danger btn-lg font-weight-bold">
                                                                    Tukar
                                                                </button>
                                                                <button type="button" class="btn btn-light btn-lg font-weight-bold" data-dismiss="modal">
                                                                    Batal
                                                                </button>
                                                            </form>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    </td>
                                </tr>
                                @empty
                                @tablenoresult
                                @endtablenoresult
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div><!-- card-body -->
            </div><!-- card -->
        </div>
    </div>

</div><!-- container -->

@endsection

@section('js')

<script>
    function search_data() {
        var search = $('#search').val();
        @if(request()->has('status'))
        window.location.href = "{{route('admin.member.no-mykad')}}?status={{request()->get('status')}}&search=" + search;
        @else
        window.location.href = "{{route('admin.member.no-mykad')}}?search=" + search;
        @endif
    }

    $(document).on("keypress", ".input-group:has(input:input, span.input-group-btn:has(.btn)) input:input", function(e) {
        if (e.which == 13) {
            $(this).closest(".input-group").find(".input-group-append > .submit").click();
        }
    });

    $('#modalChangeStatus').on('show.bs.modal', function(event) {
        var member = $(event.relatedTarget).data('member');
        var route = $(event.relatedTarget).data('route');
        var name = $(event.relatedTarget).data('name');
        var ic = $(event.relatedTarget).data('ic');
        $(this).find(".modal-body .member").text(member);
        $(this).find(".modal-body .name").text(name);
        $(this).find(".modal-body .ic").text(ic);
        $(this).find(".modal-body .deleteForm").attr('action', route);
    });
</script>
@endsection
