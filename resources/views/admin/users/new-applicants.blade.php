@extends('admin.layouts')
@pagetitle(['title'=>'Untuk Disemak ','links'=>['Pendaftaran']])@endpagetitle
@section('content')
<div class="container">
    <div class="row">
        @include('include.search',['url'=>url()->current(),'placeholder'=>'carian menggunakan nama, no. telefon atau no. mykad'])
        <div class="col-lg-6 text-right">
            @can("tambah-anggota")
            <a class="btn btn-primary mb-3 text-uppercase" href="{{url('admin/members/create')}}">Daftarkan Anggota</a>
            @endcan
        </div>
    </div>

    <div class="row ">
        @include('include.pagination',['results'=>$members])
        <div class="col-lg-12 col-xl-12 mg-t-12">
            <div class="card">
                <div class="card-body p-0">

                    <div class="table-responsive">
                        <table class="table">
                            <thead class="">
                                <tr>
                                    <th class="text-center">@sortablelink('created_at',new HtmlString('TARIKH<BR>DAFTAR'))</th>
                                    <th>@sortablelink('name','NAMA')</th>
                                    <th class="text-center">NO. Anggota KoPPIM</th>
                                    <th class="text-center">NO. MYKAD</th>
                                    <th class="text-center">STATUS KEANGGOTAAN</th>
                                    <th class="text-center">LOG MASUK</th>
                                    <th class="text-center">KAEDAH PEMBAYARAN</th>
                                    <th class="text-center">JUMLAH BAYARAN</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse($members as $member)
                                <tr>
                                    <td class="text-center">{!! $member->created_at->format("d/m/Y \<\b\\r\> h:i:sa") !!}</td>
                                    <td><a href="{{route('admin.member.view',$member->id)}}">{{ $member->name }}</a></td>
                                    <td class="text-center">{{ $member->no_koppim ?? 'TIADA NO. ANGGOTA KoPPIM' }}</td>
                                    <td class="text-center"><a href="{{route('admin.member.view',$member->id)}}">{{ $member->ic }}</a></td>
                                    <td class="text-center">
                                        {!! Helper::status_text($member->status) !!}
                                    </td>
                                    <td class="text-center">
                                        @php
                                        $last_login = \Spatie\Activitylog\Models\Activity::where('log_name','user_auth')->where('subject_id',$member->id)->latest()->first();
                                        @endphp
                                        {!! ($last_login) ? $last_login->created_at->format('d/m/Y \<\b\\r\> h:i:sa') : 'TIADA' !!}
                                    </td>
                                    <td class="text-center">
                                        @php
                                            $channel = App\Mongo\Payment::where('ic',$member->ic)->where('returncode','100')->first();
                                        @endphp
                                        {{($channel && $channel->channel) ? ucfirst($channel->channel) : 'Tiada Rekod'}}
                                    </td>
                                    <td class="text-center">
                                        @php
                                        $total = App\Mongo\Payment::where('ic',$member->ic)->where('returncode','100')->sum('amount');
                                        @endphp
                                        RM{{number_format($total,2,".",",")}}
                                    </td>
                                </tr>
                                @empty
                                <tr>
                                    <td colspan="10" class="text-center font-weight-bold text-uppercase">
                                        <h3>tiada rekod</h3>
                                    </td>
                                </tr>
                                @endforelse
                            </tbody>
                        </table>

                    </div>


                </div><!-- card-body -->
            </div><!-- card -->
        </div>


    </div>




</div><!-- container -->

@endsection

@section('js')
<script>
    function search_data() {
        var search = $('#search').val();

        window.location.href = "{{route('admin.member.new')}}?search=" + search + "";
    }

    $(document).on("keypress", ".input-group:has(input:input, span.input-group-btn:has(.btn)) input:input", function(e){
    if (e.which == 13){
        $(this).closest(".input-group").find(".input-group-append > .submit").click();
    }});

</script>

@endsection
