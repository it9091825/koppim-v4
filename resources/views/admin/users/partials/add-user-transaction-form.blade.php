<div class="card">
    <div class="card-body">
        <div class="row" id="section-transation">
            <div class="col px-5 pb-1">
                <h4>Transaksi Langganan</h4>
                <p>Setiap transaksi langganan wajib disertakan dengan bukti bayaran yang sah.</p>
            </div>
        </div>
        <div class="row">
            <div class="col px-2">
                <div class="card">
                    <div class="card-body">
                        <div class="row mb-3">
                            <div class="col text-right">
                                <a id="addrow" class="btn btn-primary btn-sm font-weight-bold" href="javascript:void;"><svg class="w-4 h-4" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 9v3m0 0v3m0-3h3m-3 0H9m12 0a9 9 0 11-18 0 9 9 0 0118 0z"></path>
                                    </svg> Tambah Transaksi Langganan</a>
                            </div>
                        </div>
                        <div class="row">
                            <style>
                              
                            </style>
                            <div class="col">
                                <table id="myTable" class="table order-list">
                                    <thead>
                                        <tr>
                                            <td class="font-weight-bold">Jumlah Syer</td>
                                            <td class="font-weight-bold">Fi Pendaftaran Anggota</td>
                                            <td class="font-weight-bold">Langganan Modal Syer</td>
                                            <td class="font-weight-bold">Langganan Tambahan</td>
                                            <td class="font-weight-bold">Tarikh Transaksi Bayaran</td>
                                            <td class="font-weight-bold">Bukti Bayaran</td>
                                            <td class="font-weight-bold">Nota Transaksi</td>
                                            <td class="font-weight-bold">Tindakan</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="col-1">
                                                <input type="text" class="form-control mask" id="amount" name="amount[1]" data-inputmask="'alias': 'numeric', 'autoGroup': true, 'digits': 2,'rightAlign': false, 'digitsOptional': false, 'placeholder': '0'">
                                            </td>
                                            <td class="col-1">
                                                <input type="text" class="form-control mask" id="regfee" name="regfee[1]" data-inputmask="'alias': 'numeric', 'autoGroup': true, 'digits': 2,'rightAlign': false, 'digitsOptional': false, 'placeholder': '0'">
                                            </td>
                                            <td class="col-1">
                                                <input type="text" class="form-control mask" id="modalsyer" name="modalsyer[1]" data-inputmask="'alias': 'numeric', 'autoGroup': true, 'digits': 2,'rightAlign': false, 'digitsOptional': false, 'placeholder': '0'">
                                            </td>
                                            <td class="col-1">
                                                <input type="text" class="form-control mask" id="additionalsyer" name="additionalsyer[1]" data-inputmask="'alias': 'numeric', 'autoGroup': true, 'digits': 2,'rightAlign': false, 'digitsOptional': false, 'placeholder': '0'">
                                            </td>
                                            <td class="col-3">
                                                <input type="text" name="paid_at[1]" class="form-control paid_at" value="">
                                            </td>
                                            <td>
                                                <input type="file" id="file" class="" name="proof[1]">
                                            </td>
                                            <td>
                                                <textarea name="remark[1]" id="" cols="10" rows="3" class="form-control"></textarea>
                                            </td>
                                            <td>
                                                <a class="deleteRow"></a>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@push('script')
<script>
    $(document).ready(function() {

        var counter = 2;

        $("#addrow").on("click", function() {
            var newRow = $("<tr>");
            var cols = `
            <td class="col-1">
                <input type="text" class="form-control mask" id="amount${counter}" name="amount[${counter}]" data-inputmask="'alias': 'numeric', 'autoGroup': true, 'digits': 2,'rightAlign': false, 'digitsOptional': false, 'placeholder': '0'">
            </td>
            <td class="col-1">
                <input type="text" class="form-control mask" id="regfee${counter}" name="regfee[${counter}]" data-inputmask="'alias': 'numeric', 'autoGroup': true, 'digits': 2,'rightAlign': false, 'digitsOptional': false, 'placeholder': '0'">
            </td>
            <td class="col-1">
                <input type="text" class="form-control mask" id="modalsyer${counter}" name="modalsyer[${counter}]" data-inputmask="'alias': 'numeric', 'autoGroup': true, 'digits': 2,'rightAlign': false, 'digitsOptional': false, 'placeholder': '0'">
            </td>
            <td class="col-1">
                <input type="text" class="form-control mask" id="additionalsyer${counter}" name="additionalsyer[${counter}]" data-inputmask="'alias': 'numeric', 'autoGroup': true, 'digits': 2,'rightAlign': false, 'digitsOptional': false, 'placeholder': '0'">
            </td>
            <td class="col-3">
                <input type="text" name="paid_at[${counter}]" class="form-control paid_at" id="paid_at${counter}">
            </td>

            <td class="col-1">

            <input type="file" id="file" class="" name="proof[${counter}]">

            </td>
            <td class="col-2">
                <textarea name="remark[${counter}]" id="" cols="10" rows="3" class="form-control"></textarea>
            </td>
            <td class=""><a href="javascript:void;" class="ibtnDel text-danger"><svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16"></path></svg></a></td>
        `;

            newRow.append(cols);
            $("table.order-list").append(newRow);

            counter++;
            //initialize balik script
            utilitiesinit();
        });

        $("table.order-list").on("click", ".ibtnDel", function(event) {
            $(this).closest("tr").remove();
            counter -= 1

        });


        // $('table.order-list').rows[0].index
    });
</script>
@endpush
