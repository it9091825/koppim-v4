<div class="row">
    <div class="col">
        <h4>Maklumat Peribadi</h4>
        <p>Ruangan ini mengandungi maklumat peribadi dan maklumat residensi pemohon.</p>
    </div>
    <div class="col">
        <div class="form-group">
            <label for="">No. Borang</label>
            <input type="text" name="no_borang" class="form-control" value="{{ old('no_borang') }}">
        </div>
        <div class="form-group">
            <label for="">Tarikh Pendaftaran</label>
            <input type="text" name="registration_date" class="form-control registration_date" value="{{ old('registration_date') }}">
        </div>        
        <div class="form-group">
            <label for="">Nama Penuh<span class="ml-1 badge badge-sm badge-soft-secondary">WAJIB DIISI</span></span></label>
            <input type="text" class="form-control text-uppercase" id="name" name="name" placeholder="" value="{{ old('name') }}" required>
        </div>
        <div class="form-group">
            <label for="">No. MyKad<span class="ml-1 badge badge-sm badge-soft-secondary">WAJIB DIISI</span></span></label>
            <input type="text" id="mykad" class="form-control mykad-mask" name="ic" placeholder="" value="{{ old('ic') }}" required>

        </div>
        <div class="form-group">
            <label for="">E-mel<span class="ml-1 badge badge-sm badge-soft-secondary">WAJIB DIISI</span></span></label>
            <input type="email" id="email" class="form-control mask" name="email" placeholder="" value="{{ old('email') }}" required>
        </div>
        <div class="form-group">
            <label for="">No. Telefon Bimbit<span class="ml-1 badge badge-sm badge-soft-secondary">WAJIB DIISI</span></span></label>
            <input type="text" id="mobileno" class="form-control mask mobile-mask" name="mobile_no" placeholder="" value="" required>
        </div>
        <div class="form-group">
            <label for="">No. Telefon Rumah</label>
            <input type="text" id="landline" class="form-control phone-mask" name="home_no" placeholder="" value="">
        </div>
        <div class="form-group">            
            <div class="row">
                <div class="col">
                    <label for="">Tarikh Lahir <br>(Hari)</label>
                    <select class="form-control select2" name="org_dob_day" id="org_dob_day">
                        <option value=""></option>
                        @for ($i = 1; $i <= 31; $i++) <option value="{{ $i }}">{{ $i }}</option>
                            @endfor
                    </select>
                </div>
                <div class="col">
                    <label class="form-control-label">Tarikh Lahir <br>(Bulan)</label>
                    <select class="form-control select2" name="org_dob_month" id="org_dob_month" >
                        <option value=""></option>
                        @for ($i = 1; $i <= 12; $i++) <option value="{{ $i }}">{{ $i }}</option>
                            @endfor
                    </select>
                </div>
                <div class="col">
                    <label class="form-control-label">Tarikh Lahir <br>(Tahun)</label>
                    <select class="form-control select2" name="org_dob_year" id="org_dob_year">
                        <option value=""></option>
                        @for ($i = 1920; $i <= 2020; $i++) <option value="{{ $i }}">{{ $i }}</option>
                            @endfor
                    </select>
                </div>
            </div>
        </div> {{--form-group --}}
        <div class="form-group">
            <label for="">Alamat Baris 1</label>
            <input type="text" class="form-control" name="address_line_1" placeholder="" value="{{ old('address_line_1') }}">
        </div>
        <div class="form-group">
            <label for="">Alamat Baris 2</label>
            <input type="text" class="form-control" name="address_line_2" placeholder="" value="{{ old('address_line_2') }}">
        </div>
        <div class="form-group">
            <label for="">Alamat Baris 3</label>
            <input type="text" class="form-control" name="address_line_3" placeholder="" value="{{ old('address_line_3') }}">
        </div>
        <div class="form-group">
            <div class="row">
                <div class="col">
                    <div class="form-group">
                    <label for="">Poskod</label>
                    <input type="text" class="form-control poskod" name="postcode" placeholder="" value="{{ old('postcode') }}">
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                    <label class="form-control-label">Daerah</label>
                    <input type="text" class="form-control" name="city" placeholder="" value="{{ old('city') }}">
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                    <label class="form-control-label">Negeri</label>
                    <select class="form-control select2" name="state" id="state">
                        @include('admin.users.partials.add-user-states')
                    </select>
                    </div>
                </div>
            </div>
        </div> {{--form-group --}}
        <div class="form-group">
            <label for="">Kawasan Parlimen</label>
            <select class="form-control select2" name="org_parlimen" id="org_parlimen" placeholder="[Pilih Kawasan Parlimen]">
                @include('admin.users.partials.add-user-parliaments')
            </select>
        </div>
    </div>
</div><!-- row-->
@push('script')

<script>   
    $(document).ready(function() {
    $('#peribadiForm').bootstrapValidator({
        live: 'enabled',
        excluded: ':disabled',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            name: {
                trigger: 'change keyup',
                message: '',
                validators: {
                    notEmpty: {
                        message: 'Nama Penuh Pemohon wajib diisi'
                    },
                    stringLength: {
                        min: 2,
                        message: 'Nama Penuh Pemohon wajib diisi'
                    },
                    regexp: {
                        regexp: /^[a-zA-Z]+$/,
                        message: 'Nombor dan simbol tidak dibenarkan'
                    }
                }
            },
            ic: {
                trigger: 'change keyup',
                message: 'The username is not valid',
                validators: {
                    notEmpty: {
                        message: 'No. MYKAD Pemohon wajib diisi'
                    },
                    regexp: {
                        regexp: /^[0-9]+$/,
                        message: 'Hanya nombor dibenarkan di ruangan ini'
                    }
                }
            },
            email: {
                trigger: 'change keyup',
                validators: {
                    notEmpty: {
                        message: 'Ruangan E-mel pemohon wajib diisi'
                    },
                    emailAddress: {
                        message: 'E-mel yang dimasukkan tidak sah!'
                    }
                }
            },
            mobile_no: {
                trigger: 'change keyup',
                validators: {
                    notEmpty: {
                        message: 'Ruangan No. Telefon Bimbit Pemohon wajib diisi'
                    },
                }
            },            
        }
    });
});
</script>
@endpush
