<div class="row">
    <div class="col">
        <h4>Maklumat Akaun Bank</h4>
        <p>Ruangan ini mengandungi maklumat akaun bank pemohon.</p>
    </div>
    <div class="col">
        <div class="form-group">
            <label for="">Nama Bank</label>
            <select id="bank_name" name="bank_name" value="" class="form-control select2">
                <option value=""></option>
                @include('admin.users.partials.add-user-banks')
            </select>
        </div>
        <div class="form-group">
            <label for="">Nama Akaun</label>
            <input type="text" class="form-control" name="bank_account" value="{{ old('bank_account') }}">
        </div>
        <div class="form-group">
            <label for="">No. Akaun</label>
            <input type="text" class="form-control" name="bank_account_name" value="{{ old('bank_account_name') }}">
        </div>
    </div>
</div>