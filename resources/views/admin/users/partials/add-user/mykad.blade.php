<div class="row">
    <div class="col">
        <h4>Maklumat MYKAD</h4>
        <p>Sila pastikan pemohon mengemukakan lampiran hadapan dan belakang MYKAD.</p>
    </div>
    <div class="col">
        <div class="form-group">
            <label for="">MyKad Hadapan</label>
            <div class="custom-file">
                <input type="file" class="custom-file-input" name="mykad_front" id="mykad_front">
                <label class="custom-file-label" for="customFile">Choose file</label>
            </div>
        </div>
        <div class="form-group">
            <label for="">MyKad Belakang</label>
            <div class="custom-file">
                <input type="file" class="custom-file-input" name="mykad_back" id="mykad_back">
                <label class="custom-file-label" for="customFile">Choose file</label>
            </div>
        </div>
    </div>
</div>