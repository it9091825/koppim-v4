<div class="card">
    <div class="card-body">
        <div class="row" id="section-transation">
            <div class="col px-5 pb-1">
                <h4>Transaksi Langganan</h4>
                <p>Setiap transaksi langganan wajib disertakan dengan bukti bayaran yang sah.</p>
            </div>
        </div>
        <div class="row">
            <div class="col px-2">
                <div class="card">
                    <div class="card-body">
                        <div class="row mb-3">
                            <div class="col text-right">
                                <a id="addrow" class="btn btn-primary btn-sm font-weight-bold" href="javascript:void;"><svg class="w-4 h-4" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 9v3m0 0v3m0-3h3m-3 0H9m12 0a9 9 0 11-18 0 9 9 0 0118 0z"></path>
                                    </svg> Tambah Transaksi Langganan</a>
                            </div>
                        </div>
                        <div class="row order-list">
                            <div class="col-6">
                                <div class="card border">
                                    <div class="card-header d-flex justify-content-between align-items-center">
                                        <h4>Transaksi #<span class="indexId">1</span></h4>
                                    </div>
                                    <div class="card-body check1">
                                        {{-- <div class="row mb-3">
                                            <div class="col text-right">
                                                <button type="button" class="btn btn-primary btn-sm" onclick="clicker(this)" data-id="check1" data-toggle="tooltip" data-placement="auto" data-original-title="Hanya masukkan jumlah keseluruhan dan klik butang ini untuk mengira pecahan transaksi pertama pelanggan ini secara automatik"><svg class="w-4 h-4" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 7h6m0 10v-3m-3 3h.01M9 17h.01M9 14h.01M12 14h.01M15 11h.01M12 11h.01M9 11h.01M7 21h10a2 2 0 002-2V5a2 2 0 00-2-2H7a2 2 0 00-2 2v14a2 2 0 002 2z"></path>
                                                    </svg> Set Sebagai Transaksi Pertama </button>
                                            </div>

                                        </div> --}}
                                        <div class="row">
                                            <div class="col">
                                                <div class="form-group">
                                                    <label for="">JUMLAH KESELURUHAN</label>
                                                    <input type="text" class="form-control mask amount" id="amount" name="amount[1]" data-inputmask="'alias': 'numeric', 'autoGroup': true, 'digits': 2,'rightAlign': false, 'digitsOptional': false, 'placeholder': '0'">
                                                </div>
                                            </div>
                                            <div class="col">
                                                <div class="form-group">
                                                    <label for="">fi pendaftaran Anggota</label>
                                                    <input type="text" class="form-control mask first_time_fees" id="regfee" name="regfee[1]" data-inputmask="'alias': 'numeric', 'autoGroup': true, 'digits': 2,'rightAlign': false, 'digitsOptional': false, 'placeholder': '0'">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col">
                                                <div class="form-group">
                                                    <label for="">langganan modal syer</label>
                                                    <input type="text" class="form-control mask first_time_share" id="modalsyer" name="modalsyer[1]" data-inputmask="'alias': 'numeric', 'autoGroup': true, 'digits': 2,'rightAlign': false, 'digitsOptional': false, 'placeholder': '0'">
                                                </div>
                                            </div>
                                            <div class="col">
                                                <div class="form-group">
                                                    <label for="">langganan tambahan</label>
                                                    <input type="text" class="form-control mask additional_share" id="additionalsyer" name="additionalsyer[1]" data-inputmask="'alias': 'numeric', 'autoGroup': true, 'digits': 2,'rightAlign': false, 'digitsOptional': false, 'placeholder': '0'">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col">
                                                <div class="form-group">
                                                    <label for="">Tarikh Transaksi Bayaran</label>
                                                    <input type="text" name="paid_at[1]" class="form-control paid_at" value="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col">
                                                <div class="form-group">
                                                    <label for="">bukti pembayaran</label><br>
                                                    <input type="file" id="proof" class="" name="proof[1]">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col">
                                                <div class="form-group">
                                                    <label for="">Status Transaksi</label><br>
                                                    <select name="status[1]" class="form-control select2" id="">
                                                        <option value="">Sila Pilih</option>
                                                        <option value="payment-completed">Transaksi Berjaya</option>
                                                        <option value="waiting-for-verification">Untuk Disemak</option>
                                                        <option value="waiting-for-approval">Untuk Diluluskan</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col">
                                                <div class="form-group">
                                                    <label for="">Channel Transaksi</label><br>
                                                    <select name="channel[1]" class="form-control select2" id="">
                                                        <option value="">Sila Pilih</option>
                                                        <option value="manual">Manual</option>
                                                        <option value="ikoop">IKoop</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col">
                                                <div class="form-group">
                                                    <label for="">nota transaksi</label><br>
                                                    <textarea name="remark[1]" id="" cols="10" rows="3" class="form-control"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div> <!-- end card-body-->
                                </div> <!-- end card-->
                            </div>
                            <!-- end col -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@push('script')
<script>
    $(document).ready(function() {

        var counter = 2;

        $("#addrow").on("click", function() {
            //initialize tooltip
            $('[data-toggle="tooltip"]').tooltip('dispose');
            $('[data-toggle="tooltip"]').tooltip('enable');
            var newRow = $("<div class='col-6'>");
            var cols = `
                    <div class="card border">
                        <div class="card-header d-flex align-items-center justify-content-between">
                            <h4>Transaksi #<span class="indexId"></span></h4>
                            <a href="javascript:void;" class="ibtnDel btn btn-danger btn-sm mt-2 float-right"><svg class="w-4 h-4" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16"></path></svg> Padam</a>     
                        </div>
                        <div class="card-body check${counter}">
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="">JUMLAH KESELURUHAN</label>
                                        <input type="text" class="form-control mask amount" id="amount${counter}" name="amount[${counter}]" data-inputmask="'alias': 'numeric', 'autoGroup': true, 'digits': 2,'rightAlign': false, 'digitsOptional': false, 'placeholder': '0'">
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <label for="">fi pendaftaran Anggota</label>
                                        <input type="text" class="form-control mask first_time_fees" id="regfee${counter}" name="regfee[${counter}]" data-inputmask="'alias': 'numeric', 'autoGroup': true, 'digits': 2,'rightAlign': false, 'digitsOptional': false, 'placeholder': '0'">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="">langganan modal syer</label>
                                        <input type="text" class="form-control mask first_time_share" id="modalsyer${counter}" name="modalsyer[${counter}]" data-inputmask="'alias': 'numeric', 'autoGroup': true, 'digits': 2,'rightAlign': false, 'digitsOptional': false, 'placeholder': '0'">
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <label for="">langganan tambahan</label>
                                        <input type="text" class="form-control mask additional_share" id="additionalsyer${counter}" name="additionalsyer[${counter}]"data-inputmask="'alias': 'numeric', 'autoGroup': true, 'digits': 2,'rightAlign': false, 'digitsOptional': false, 'placeholder': '0'">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="">Tarikh Transaksi Bayaran</label>
                                        <input type="text"  id="paid_at${counter}" name="paid_at[${counter}]" class="form-control paid_at" value="">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="">bukti pembayaran</label><br>
                                        <input type="file" id="proof${counter}" class="" name="proof[${counter}]">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="">Status Transaksi</label><br>
                                        <select name="status[${counter}]" class="form-control select2" id="status${counter}">
                                            <option value="">Sila Pilih</option>
                                            <option value="payment-completed">Transaksi Berjaya</option>
                                            <option value="waiting-for-verification">Untuk Disemak</option>
                                            <option value="waiting-for-approval">Untuk Diluluskan</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="">Channel Transaksi</label><br>
                                        <select name="channel[${counter}]" class="form-control select2" id="channel${counter}">
                                            <option value="">Sila Pilih</option>
                                            <option value="manual">Manual</option>
                                            <option value="ikoop">IKoop</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="">nota transaksi</label><br>
                                        <textarea name="remark[${counter}]" id="remark${counter}" cols="10" rows="3" class="form-control"></textarea>
                                    </div>
                                </div>
                            </div>                            
                        </div> <!-- end card-body-->
                    </div> <!-- end card-->
            `;

            newRow.append(cols);
            $(".order-list").append(newRow);

            counter++;
            reIndex();
            //initialize balik script
            utilitiesinit();
        });

        $(".order-list").on("click", ".ibtnDel", function(event) {
            $(this).closest(".col-6").remove();
            counter--;
            reIndex();
        });

        function reIndex() {
            var rows = $('.col-6');
            rows.each(function(i, row) {
                $(row).find('.indexId').html(i + 1);
            });
        }
    });


    var clicker = function(obj) {

        var id = $(obj).data('id');
        var amountSelector = $('.' + id).find('.amount');
        var amount = amountSelector.val();

        if (amount > 100) {
            var first_time_fees = 30;
            var first_time_share = 100;
            var additional_share = amount - 130;

            $('.' + id).find('.first_time_fees').val(first_time_fees);
            $('.' + id).find('.first_time_share').val(first_time_share);
            $('.' + id).find('.additional_share').val(additional_share);
        }







    };

</script>
@endpush
