<div id="batalkanModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="batalkanModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="batalkanModalLabel">BATALKAN BAYARAN MANUAL</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="POST" action="{{route('admin.tx.manual.batalkan')}}">
                @csrf
                <div class="modal-body">
                    <div class="row">
                        <div class="col">
                            <input type="hidden" class="form-control" id="proof_id" name="proof_id">
                            <div class="form-group mb-1">
                                <label for="name" class="col-form-label">Nama:</label>
                                <input type="text" class="form-control" id="name">
                            </div>
                            <div class="form-group mb-1">
                                <label for="ic" class="col-form-label">No. Mykad:</label>
                                <input type="text" class="form-control" id="ic">
                            </div>
                            <div class="form-group mb-1">
                                <label for="mobile_no" class="col-form-label">No. Telefon:</label>
                                <input type="text" class="form-control" id="mobile_no">
                            </div>
                            <div class="form-group mb-1">
                                <label for="email" class="col-form-label">Emel:</label>
                                <input type="text" class="form-control" id="email">
                            </div>
                            <div class="form-group mb-1">
                                <label for="payment_date" class="col-form-label">TARIKH TRANSAKSI:</label>
                                <input type="text" class="form-control" id="payment_date">
                            </div>
                            <div class="form-group mb-1">
                                <label for="amount" class="col-form-label">Jumlah Keseluruhan:</label>
                                <input type="text" class="form-control mask" id="amount" data-inputmask="'alias': 'numeric', 'autoGroup': true, 'digits': 2,'rightAlign': false, 'digitsOptional': false, 'placeholder': '0'">
                            </div>
                            <div class="form-group mb-1">
                                <label for="note" class="col-form-label">Nota Admin:</label>
                                <textarea name="note" id="note" cols="10" rows="3" class="form-control"></textarea>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group mb-1">
                                <div id="originalProof"></div>
                                <div id="proof"></div>                                
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-dismiss="modal">Tutup</button>
                    <button type="submit" class="btn btn-danger">Batalkan</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@push('script')
<script>
     $('#batalkanModal').on('show.bs.modal', function(event) {
        var button = $(event.relatedTarget)
        var userId = button.data('userid')
        var name = button.data('username')
        var ic = button.data('useric')
        var mobile_no = button.data('userphone')
        var email = button.data('useremail')
        var amount = button.data('useramount')
        var transaction = button.data('usertransaction')
        var proof = button.data('paymentproof')

        var modal = $(this)
        modal.find('#proof_id').val(userId)
        modal.find('#name').val(name)
        modal.find('#ic').val(ic)
        modal.find('#mobile_no').val(mobile_no)
        modal.find('#email').val(email)
        modal.find('#amount').val(amount)
        modal.find('#payment_date').val(transaction)
        modal.find('#proof').html(`<img src="${proof}" class="img-fluid img-thumbnail">`);
        modal.find('#originalProof').html(`<a href="${proof}" target="_blank">Lihat Bukti Pembayaran dalam saiz asal</a>`)
    })
</script>    
@endpush