<div class="row">
    <div class="col">
        <h4>Maklumat Akaun</h4>
        @if($user->dob == NULL)
            <p style="color: red">Remark:<br> Tarikh Lahir Belum Dikemaskini</p>
        @endif
    </div>
    <div class="col">
        @can('edit-anggota')<!--can edit anggota -->
        <form enctype="multipart/form-data" method="post" action="{{route('admin.member.edit.account',$user->id)}}">
            @csrf
            @endcan<!--/can edit anggota -->
                @if($user->ikoop != 0)
                <div class="form-group">
                    <h4><span for="" class="badge badge-primary">Pendaftaran Anggota Ini Dari Sistem Ikoop</span></h4>
                </div>
                @endif
            <div class="form-group">
                <label for="">No. Borang</label>
                <input type="text" class="form-control" name="no_borang" value="{{$user->no_borang ?: 'TIADA'}}">
            </div>
            <div class="form-group">
                <label for="">Tarikh Pendaftaran</label>
                <input type="text" name="registration_date" class="form-control registration_date" value="{{ $user->registration_date ?? $user->created_at }}">
            </div>
            <div class="form-group">
                <label for="">No. Anggota KoPPIM</label>
                <p>{{$user->no_koppim ?: 'TIADA'}}</p>
            </div>
            <div class="form-group">
                <label for="">Nama Penuh<span class="ml-1 badge badge-sm badge-soft-secondary">WAJIB DIISI</span></label>
                <input type="text" class="form-control" name="name" placeholder="Nama seperti dalam MYKAD" value="{{ $user->name }}">
            </div>
            <div class="form-group">
                <label for="">No. MYKAD<span class="ml-1 badge badge-sm badge-soft-secondary">WAJIB DIISI</span></label>
                <input type="text" class="form-control" id="mykad" name="ic" placeholder="No. MyKad" value="{{ $user->ic }}" @cant('edit-mykad-anggota') disabled @endcant>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col">
                        <label for="">Tarikh Lahir <br>(Hari)</label>
                        <select class="form-control select2" name="org_dob_day" id="org_dob_day">
                            <option value=""></option>
                            @for ($i = 1; $i <= 31; $i++) <option value="{{ $i }}" >{{ $i }}</option>
                            @endfor
                        </select>
                    </div>
                    <div class="col">
                        <label class="form-control-label">Tarikh Lahir <br>(Bulan)</label>
                        <select class="form-control select2" name="org_dob_month" id="org_dob_month" data-placeholder="">
                            <option value=""></option>
                            @for ($i = 1; $i <= 12; $i++) <option value="{{ $i }}">{{ $i }}</option>
                                @endfor
                        </select>
                    </div>
                    <div class="col">
                        <label class="form-control-label">Tarikh Lahir <br>(Tahun)</label>
                        <select class="form-control select2" name="org_dob_year" id="org_dob_year" data-placeholder="">
                            <option value=""></option>
                            @for ($i = 1920; $i <= 2020; $i++) <option value="{{ $i }}">{{ $i }}</option>
                                @endfor
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="">E-mel<span class="ml-1 badge badge-sm badge-soft-secondary">WAJIB DIISI</span></label>
                <input type="email" class="form-control" name="email" placeholder="Emel" value="{{ $user->email }}">
            </div>
            <div class="form-group">
                <label for="">No. Telefon Bimbit<span class="ml-1 badge badge-sm badge-soft-secondary">WAJIB DIISI</span></label>
                <input type="text" class="form-control" name="mobile_no" id="mobile_no" placeholder="No. Telefon Bimbit" value="{{ $user->mobile_no }}">
            </div>
            <div class="form-group">
                <label for="">No. Telefon Rumah</label>
                <input type="text" class="form-control" name="home_no" id="home_no" placeholder="No. Telefon Rumah" value="{{ $user->home_no }}">
            </div>
            @can('edit-anggota') <!--can edit anggota -->
            <div class="form-group">
                <button type="submit" class="btn btn-primary font-weight-bold">Kemaskini</button>
            </div>
        </form>
        @endcan <!--/can edit anggota -->
    </div>
</div>
