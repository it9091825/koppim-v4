<div class="card">
    <div class="card-body pd-20">
        <div style="height:300px;overflow-y:scroll">
            <div class="left-timeline mt-3 mb-3 pl-4">
                <ul class="list-unstyled events mb-0">
                    @forelse($smslogs as $smslog)
                    <li class="event-list">
                        <div class="pb-4">
                            <div class="media">
                                <div class="event-date text-center mr-4">
                                    <div class="bg-soft-primary p-1 rounded text-primary font-size-14">
                                        {{$smslog->created_at->format('d/m/Y h:i:sa')}}</div>
                                </div>
                                <div class="media-body">
                                    <p>
                                        <ul class="list-unstyled">
                                            <li><b>No. Telefon:</b> {{$smslog->mobile_no}}</li>
                                            @if($smslog->provider == 'klasiksms')
                                            <li><b>Status Code:</b> {{$smslog->status_code[0]}}</li>
                                            <li><b>Status Mesej:</b> {{$smslog->status_msg[0]}}</li>
                                            @endif
                                            <li><b>Provider:</b> {{$smslog->provider}}</li>
                                            @if($smslog->provider == 'klasiksms')
                                            <li><b>SMSKlasik Ref. ID:</b> {{$smslog->reference_id[0]}}</li>
                                            @endif
                                        </ul>
                                    </p>
                                    <p><b>Mesej:</b> {{$smslog->text}}</p>
                                </div>
                            </div>
                        </div>
                    </li>
                    @empty
                    <div class="text-center">
                        <h3 class="text-uppercase font-weight-bold">tiada rekod</h3>
                    </div>
                    @endforelse
                </ul>
            </div>
        </div>
    </div>
</div><!-- card -->