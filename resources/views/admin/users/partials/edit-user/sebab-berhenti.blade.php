<hr>
<div class="row">
    <div class="col">
        <h4>Permohonan Berhenti Keanggotaan</h4>
    </div>
    <div class="col">
        @can('edit-anggota')
        <form enctype="multipart/form-data" action="{{ route('admin.member.termination.send') }}" method="POST">
            @csrf
            @endcan
            <div class="form-group">
                <label for="">Tarikh Permohonan</label>
                <input type="text" class="form-control" value="{{ $terminations->updated_at->format('d/m/Y') }}" readonly>
            </div>
            <div class="form-group">
                <label for="">Sebab Berhenti</label>
                <textarea class="form-control" readonly>{{ $terminations->leave_reason }}</textarea>
            </div>
            <div class="form-group">
                <label for="">Surat Permohonan Berhenti</label>
                @if($terminations->termination_letter == NULL)
                    <div style="margin-bottom: 20px;">[TIADA LAMPIRAN]</div>
                @else
                    <div style="margin-bottom: 20px;">
                        <a target="_blank" href="{{ env('DO_SPACES_FULL_URL').$terminations->termination_letter }}">[SURAT BERHENTI]</a>
                    </div>
                @endif
            </div>
            @can('edit-anggota')
            <div class="form-group">
                <input type="hidden" name="user_id" value="{{$user->id}}">
                <button type="submit" class="btn btn-primary font-weight-bold" value="lulus">Luluskan Pemberhentian</button>
                <button type="submit" class="btn btn-danger font-weight-bold" value="gagal">Batalkan Pemberhentian</button>
            </div>
        </form>
        @endcan
    </div>
</div>
