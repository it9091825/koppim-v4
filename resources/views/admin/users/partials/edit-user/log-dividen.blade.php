<div class="card">
    <div class="card-body pd-20">
        <div style="height:300px;overflow-y:scroll">
            <div class="left-timeline mt-3 mb-3 pl-4">
               <table class="table">
		                        <thead>
			                        <tr>
				                        <th>Tarikh</th>
				                        <th>Keterangan</th>
				                        <th>Tindakan</th>
			                        </tr>
		                        </thead>
		                         <tbody>
               @forelse($statements as $statement)
				                        @if($statement->type == 'in' )
				                        <tr>
					                        <td><a href="javascript:open_dividen('{{ DividenHelper::get_dividen_detail("$statement->ic") }}','{{ $statement->to->format("d M Y") }}')">{{ $statement->created_at->format('d M Y') }}</a></td>
					                        <td><a href="javascript:open_dividen('{{ DividenHelper::get_dividen_detail("$statement->ic") }}','{{ $statement->to->format("d M Y") }}')">Dividen Untuk Durasi {{ $statement->from->format('d M Y') }} - {{ $statement->to->format('d M Y') }}</th></td>

					                        <td class="text-success">Kredit</td>

					                        <td><b class="text-success">RM {{ number_format($statement->amount,2) }}</b></td>

				                        </tr>
				                        @else
				                        <tr>
					                        <td><a href="javascript:open_withdraw('{{ strtoupper($statement->status) }}','{{ $statement->bank }}','{{ $statement->account_no }}','{{ $statement->account_name }}','{{ $statement->amount*-1 }}','{{ $statement->payment_ref }}','{{ $statement->payment_receipt }}')">{{ $statement->created_at->format('d M Y') }}</a></td>
					                        <td><a href="javascript:open_withdraw('{{ strtoupper($statement->status) }}','{{ $statement->bank }}','{{ $statement->account_no }}','{{ $statement->account_name }}','{{ number_format($statement->amount*-1,2) }}','{{ $statement->payment_ref }}','{{ $statement->payment_receipt }}')">Pengeluaran Dividen ke {{ $statement->bank }}</a></td>

					                        <td class="text-danger">Pengeluaran</td>

					                        <td><b class="text-danger">RM {{ number_format($statement->amount*-1,2) }}</b></td>

				                        </tr>
				                        @endif

				                        @empty

				                        <tr>
					                        <td colspan="4">Tiada rekod</td>


				                        </tr>

			                        @endforelse
			                 </tbody>
	                        </table>

            </div>
        </div>
    </div>
</div><!-- card -->


<!-- The Modal -->
<div class="modal" id="user_letter_modal">
    <div class="modal-dialog modal-md">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title" id="modal_letter_header_name">Terperinci</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body" style="width: 500px;" id="modal_body">
                Sedang di proses
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Tutup</button>
            </div>

        </div>
    </div>
</div>

<!-- The Modal -->
<div class="modal" id="withdraw_status">
    <div class="modal-dialog modal-md">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title" id="modal_letter_header_name">Status Pengeluaran</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body" style="width: 500px;" id="w_modal_body">
                Sedang di proses
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Tutup</button>
            </div>

        </div>
    </div>
</div>
