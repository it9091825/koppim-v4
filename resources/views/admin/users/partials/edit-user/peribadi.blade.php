<hr>
<div class="row">
    <div class="col">
        <h4>Maklumat Peribadi</h4>
    </div>
    <div class="col">
        @can('edit-anggota')<!--can edit anggota -->
        <form enctype="multipart/form-data" method="post" action="{{route('admin.member.edit.detail',$user->id)}}">
            @csrf
            @endcan
            <div class="form-group">
                <label for="">Alamat Baris 1</label>
                <input type="text" class="form-control" name="address_line_1" placeholder="" value="{{ $user->address_line_1 }}">
            </div>
            <div class="form-group">
                <label for="">Alamat Baris 2</label>
                <input type="text" class="form-control" name="address_line_2" placeholder="" value="{{ $user->address_line_2 }}">
            </div>
            <div class="form-group">
                <label for="">Alamat Baris 3</label>
                <input type="text" class="form-control" name="address_line_3" placeholder="" value="{{ $user->address_line_3 }}">
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col">
                        <label for="">Poskod</label>
                        <input type="text" class="form-control" name="postcode" placeholder="" value="{{ $user->postcode }}">
                    </div>
                    <div class="col">
                        <label class="form-control-label">Daerah</label>
                        <input type="text" class="form-control" name="city" placeholder="" value="{{ $user->city }}">
                    </div>
                    <div class="col">
                        <label class="form-control-label">Negeri</label>
                        <select class="form-control select2" name="state" id="state">
                            @include('admin.users.partials.add-user-states')
                        </select>
                    </div>
                </div>
            </div> {{--form-group --}}
            <div class="form-group">
                <label for="">Kawasan Parlimen</label>
                <select class="form-control select2" name="org_parlimen" id="org_parlimen" placeholder="[Pilih Kawasan Parlimen]">
                    @include('admin.users.partials.add-user-parliaments')
                </select>
            </div>
            <div class="form-group">
                <label for="">Jantina</label>
                <select class="form-control select2" name="sex" id="sex">
                    <option value="">Sila Pilih</option>
                    <option value="LELAKI" {{strtoupper($user->sex) == "LELAKI" ? 'selected' : ''}}>
                        LELAKI
                    </option>
                    <option value="PEREMPUAN" {{strtoupper($user->sex) == "PEREMPUAN" ? 'selected' : ''}}>
                        PEREMPUAN
                    </option>
                </select>
            </div>
            <div class="form-group">
                <label for="">Bangsa</label>
                <select class="form-control select2" name="race" id="race">
                    <option value="">Sila Pilih</option>
                    <option value="MELAYU" {{strtoupper($user->race) == "MELAYU" ? 'selected' : ''}}>
                        MELAYU
                    </option>
                    <option value="BUMIPUTERA" {{strtoupper($user->race) == "BUMIPUTERA" ? 'selected' : ''}}>
                        BUMIPUTERA
                    </option>
                </select>
            </div>
            <div class="form-group">
                <label for="">Agama</label>
                <select class="form-control select2" name="religion" id="religion">
                    <option value="">Sila Pilih</option>
                    <option value="ISLAM" {{strtoupper($user->religion) == "ISLAM" ? 'selected' : ''}}>
                        ISLAM
                    </option>
                </select>
            </div>
            <div class="form-group">
                <label for="">Status Perkahwinan</label>
                <select class="form-control select2" name="marital_status" id="marital_status">
                    <option value="">Sila Pilih</option>
                    <option value="BUJANG" {{strtoupper($user->marital_status) == "BUJANG" ? 'selected' : ''}}>
                        BUJANG
                    </option>
                    <option value="BERKAHWIN" {{strtoupper($user->marital_status) == "BERKAHWIN" ? 'selected' : ''}}>
                        BERKAHWIN
                    </option>
                    <option value="BERCERAI" {{strtoupper($user->marital_status) == "BERCERAI" ? 'selected' : ''}}>
                        BERCERAI
                    </option>
                </select>
            </div>
            <div class="form-group">
                <label for="">Pekerjaan</label>
                <input type="text" class="form-control select2" name="job" placeholder="Pekerjaan" value="{{ $user->job }}">
            </div>
            <div class="form-group">
                <label for="">Pendapatan Bulanan Di Bawah</label>
                <select class="form-control select2" name="monthly_income" id="monthly_income">
                    <option value="">Sila Pilih</option>
                    <option value="BUJANG" {{strtoupper($user->monthly_income) == "BUJANG" ? 'selected' : ''}}>
                        BUJANG
                    </option>
                    <option value="RM 0 - RM 1000" {{strtoupper($user->monthly_income) == "RM 0 - RM 1000" ? 'selected' : ''}}>
                        RM 0 - RM 1000
                    </option>
                    <option value="RM 1000 - RM 2000" {{strtoupper($user->monthly_income) == "RM 1000 - RM 2000" ? 'selected' : ''}}>
                        RM 1000 - RM 2000
                    </option>
                    <option value="RM 3000 - RM 4000" {{strtoupper($user->monthly_income) == "RM 3000 - RM 4000" ? 'selected' : ''}}>
                        RM 3000 - RM 4000
                    </option>
                    <option value="RM 4000 - RM 5000" {{strtoupper($user->monthly_income) == "RM 4000 - RM 5000" ? 'selected' : ''}}>
                        RM 4000 - RM 5000
                    </option>
                    <option value="RM 5000 - RM 10000" {{strtoupper($user->monthly_income) == "RM 5000 - RM 10000" ? 'selected' : ''}}>
                        RM 5000 - RM 10000
                    </option>
                    <option value="RM 10000 - RM 20000" {{strtoupper($user->monthly_income) == "RM 10000 - RM 20000" ? 'selected' : ''}}>
                        RM 10000 - RM 20000
                    </option>
                    <option value="RM 20000 - RM 30000" {{strtoupper($user->monthly_income) == "RM 20000 - RM 30000" ? 'selected' : ''}}>
                        RM 20000 - RM 30000
                    </option>
                    <option value="RM 30000 - RM 40000" {{strtoupper($user->monthly_income) == "RM 30000 - RM 40000" ? 'selected' : ''}}>
                        RM 30000 - RM 40000
                    </option>
                    <option value="RM 40000 - RM 50000" {{strtoupper($user->monthly_income) == "RM 40000 - RM 50000" ? 'selected' : ''}}>
                        RM 40000 - RM 50000
                    </option>
                    <option value="RM 50000 Keatas" {{strtoupper($user->monthly_income) == "RM 50000 Keatas" ? 'selected' : ''}}>
                        RM 50000 Keatas
                    </option>
                </select>
            </div>
            @can('edit-anggota')<!--can edit anggota -->
            <div class="form-group">
                <button type="submit" class="btn btn-primary font-weight-bold">Kemaskini</button>
            </div>
        </form>
        @endcan
    </div>
</div><!-- row-->