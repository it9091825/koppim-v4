<hr>
<div class="row">
    <div class="col">
        <h4>Maklumat Akaun Bank</h4>
    </div>
    <div class="col">
        @can('edit-anggota')
        <!--can edit anggota -->
        <form enctype="multipart/form-data" method="post" action="{{route('admin.member.edit.bank',$user->id)}}">
            @csrf
            @endcan
            <div class="form-group">
                <label for="">Nama Bank</label>
                <select id="bank_name" name="bank_name" class="form-control select2">
                    @include('admin.users.partials.add-user-banks')
                </select>
            </div>
            <div class="form-group">
                <label for="">Nama Pemegang Akaun</label>
                <input type="text" class="form-control" name="bank_account_name" value="{{ $user->bank_account_name }}">
            </div>
            <div class="form-group">
                <label for="">No. Akaun</label>
                <input type="text" class="form-control" name="bank_account" value="{{ $user->bank_account }}">
            </div>
            <div class="form-group">
                <label class="form-control-label">Status Senarai Hitam: <span class="tx-danger">*</span></label>

                <select class="form-control select2 @error('blacklist') is-warning @enderror" name="blacklist" id="blacklist" data-placeholder="[Status Senarai Hitam]">

                    <option value="">[Status Senarai Hitam]</option>
                    <option value="1">YA</option>
                    <option value="0">TIDAK</option>

                </select>
            </div>
            @can('edit-anggota')
            <!--can edit anggota -->
            <div class="form-group">
                <button type="submit" class="btn btn-primary font-weight-bold">Kemaskini</button>
            </div>
        </form>
        @endcan
    </div>
</div>
