<div class="card">
    <div class="card-body pd-20">
        <div style="height:300px;overflow-y:scroll">
            <div class="left-timeline mt-3 mb-3 pl-4">
               
               <table class="table">
                            <thead>
                                <tr>
	                                <th>Tarikh</th>
                                    <th>NO RUJUKAN SURAT</th>
                                    <th>SUBJEK</th>
                                    
                                    <th>STATUS</th>
                                    <th>TINDAKAN</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse($surats as $user)
                                <tr>
	                                <td>{{ $user->created_at->format('d M Y h:i') }}</td>
                                    <td>{{ $user->letter_ref_no }}</td>
                                    <td>{{ $user->subject }}</td>
                                    
                                    <td>{!! $user->status !!}</td>
                                    <td>
                                        <a target="_blank" href="/admin/letter/preview/user?ref_no={{ $user->letter_ref_no }}&type=preview">Lihat Surat</a>
                                    </td>
                                </tr>

                                @empty
                                <tr>
                                    <td colspan="6">Tiada Rekod</td>
                                </tr>
                                @endforelse
                            </tbody>
                        </table>
               
            </div>
        </div>
    </div>
</div><!-- card -->
