<hr>
<div class="row">
    <div class="col">
        <h4>Maklumat MYKAD</h4>
        @if($user->mykad_front == NULL)
        <h4 style="color: red">Remark:<br> MyKad Anggota Belum Dimuatnaik</h4>
        @endif
    </div>
    <div class="col">
        <form enctype="multipart/form-data" method="post" action="{{route('admin.member.edit.attachment',$user->id)}}">
            @csrf
            <div class="form-group">
                <label for="">MyKad Hadapan</label>
                @if($user->mykad_front == NULL)
                <div style="margin-bottom: 20px;">[TIADA LAMPIRAN]</div>
                @else
                <div style="margin-bottom: 20px;">
                    <a target="_blank" href="{{ env('DO_SPACES_FULL_URL').$user->mykad_front }}">[LAMPIRAN]</a>
                </div>
                @endif
                <div class="custom-file">
                    <input type="file" class="custom-file-input" name="mykad_front" id="mykad_front">
                    <label class="custom-file-label" for="customFile">Choose file</label>
                </div>
            </div>
            {{-- <div class="form-group">
                <label for="">MyKad Belakang</label>
                @if($user->mykad_back == NULL)
                <div style="margin-bottom: 20px;">[TIADA LAMPIRAN]</div>
                @else
                <div style="margin-bottom: 20px;">
                    <a target="_blank" href="{{ env('DO_SPACES_FULL_URL').$user->mykad_back }}">[LAMPIRAN]</a>
                </div>
                @endif
                <div class="custom-file">
                    <input type="file" class="custom-file-input" name="mykad_back" id="mykad_back">
                    <label class="custom-file-label" for="customFile">Choose file</label>
                </div>
            </div> --}}
            <div class="form-group">
                <button type="submit" class="btn btn-primary font-weight-bold">Kemaskini</button>
            </div>
        </form>
    </div>
</div>
