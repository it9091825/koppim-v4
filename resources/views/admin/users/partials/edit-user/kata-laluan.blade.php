@can('edit-anggota')
<hr>
<div class="row">
    <div class="col">
        <h4>Set Kata Laluan</h4>
    </div>
    <div class="col">
        @can('edit-anggota')<!--can edit anggota -->
        <form enctype="multipart/form-data" method="post" action="{{route('admin.member.edit.password',$user->id)}}">
            @csrf
            @endcan
            <div class="form-group">
                <label for="">Kata Laluan</label>
                <input type="text" class="form-control" name="pass" id="pass" placeholder="Tukar Kata Laluan">
                <div style="margin-top: 10px;"><a href="javascript:generatePassword()">[Jana Kata Laluan]</a>
                </div>
            </div>
            @can('edit-anggota')<!--can edit anggota -->
            <div class="form-group">
                <button type="submit" class="btn btn-primary font-weight-bold">Kemaskini</button>
            </div>
        </form>
        @endcan
    </div>
</div>
@endcan