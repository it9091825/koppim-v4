@extends('admin.layouts')
@section('css')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/jquery-datetimepicker@2.5.21/jquery.datetimepicker.css">
@endsection
@section('header')

<div class="container">
    <div class="row page-title">
        <div class="col-md-12">
            <nav aria-label="breadcrumb" class="float-right mt-1">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item active" aria-current="page">Transaksi</li>
                    <li class="breadcrumb-item active" aria-current="page">Edit Transaksi Langganan</li>
                </ol>
            </nav>
            <h4 class="mb-1 mt-0">Edit Transaksi Langganan - {{ $payment->name }} </h4>
        </div>
    </div>
</div> <!-- container -->

@endsection
@section('content')
<div class="container">
    <div class="row mb-3">
        <div class="col">
        <a href="{{route('admin.member.view',['user_id'=>request()->get('user_id')])}}">Kembali</a>
        </div>
    </div>
    <div class="row">
        <div class="col">
            @can('edit-transaksi')
            <form action="{{route('admin.tx.manual.kemaskini-transaksi',['id'=>$payment->_id])}}" method="POST" enctype="multipart/form-data">
                @csrf
                @endcan
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col pb-1">
                                <h4>Transaksi Langganan</h4>
                                <p>Setiap transaksi langganan wajib disertakan dengan bukti bayaran yang sah.</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="row mb-3">
                                    <input type="hidden" name="payment_id" value="{{$payment->_id}}">
                                </div>
                                <div class="row order-list">
                                    <div class="col-6">
                                        <div class="card border">
                                            <div class="card-body check1">
                                                {{-- <div class="row mb-3">
                                                    <div class="col text-right">
                                                        <button type="button" class="btn btn-primary btn-sm" onclick="clicker(this)" data-id="check1" data-toggle="tooltip" data-placement="auto" data-original-title="Hanya masukkan jumlah keseluruhan dan klik butang ini untuk mengira pecahan transaksi pertama pelanggan ini secara automatik"><svg class="w-4 h-4" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 7h6m0 10v-3m-3 3h.01M9 17h.01M9 14h.01M12 14h.01M15 11h.01M12 11h.01M9 11h.01M7 21h10a2 2 0 002-2V5a2 2 0 00-2-2H7a2 2 0 00-2 2v14a2 2 0 002 2z"></path>
                                                            </svg> Set Sebagai Transaksi Pertama </button>
                                                    </div>
                                                </div> --}}
                                                <div class="row">
                                                    <div class="col">
                                                        <div class="form-group">
                                                            <label for="">JUMLAH KESELURUHAN (RM)</label>
                                                            <input type="text" class="form-control mask amount" id="amount" name="amount" data-inputmask="'alias': 'numeric', 'autoGroup': true, 'digits': 2,'rightAlign': false, 'digitsOptional': false, 'placeholder': '0'" value="{{$payment->amount}}">
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="form-group">
                                                            <label for="">fi pendaftaran Anggota (RM)</label>
                                                            <input type="text" class="form-control mask first_time_fees" id="regfee" name="regfee" data-inputmask="'alias': 'numeric', 'autoGroup': true, 'digits': 2,'rightAlign': false, 'digitsOptional': false, 'placeholder': '0'" value="{{$payment->first_time_fees}}">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col">
                                                        <div class="form-group">
                                                            <label for="">langganan modal syer (RM)</label>
                                                            <input type="text" class="form-control mask first_time_share" id="modalsyer" name="modalsyer" data-inputmask="'alias': 'numeric', 'autoGroup': true, 'digits': 2,'rightAlign': false, 'digitsOptional': false, 'placeholder': '0'" value="{{$payment->first_time_share}}">
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <div class="form-group">
                                                            <label for="">langganan tambahan (RM)</label>
                                                            <input type="text" class="form-control mask additional_share" id="additionalsyer" name="additionalsyer" data-inputmask="'alias': 'numeric', 'autoGroup': true, 'digits': 2,'rightAlign': false, 'digitsOptional': false, 'placeholder': '0'" value="{{$payment->additional_share}}">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col">
                                                        <div class="form-group">
                                                            <label for="">Tarikh Transaksi Bayaran</label>
                                                            <input type="text" name="paid_at" class="form-control paid_at" value="{{$payment->payment_date}}">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col">
                                                        <div class="form-group">
                                                            <label for="">bukti pembayaran</label><br>
                                                            @if($payment->payment_proof)
                                                            <a href="{{env('DO_SPACES_FULL_URL').$payment->payment_proof}}" target="_blank">
                                                                <img src="{{ env('DO_SPACES_FULL_URL').$payment->payment_proof}}" alt="" class="img-thumbnail" height="500">
                                                            </a>
                                                            @endif
                                                            <input type="file" id="proof" class="" name="proof">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col">
                                                        <div class="form-group">
                                                            <label for="">Status Transaksi</label><br>
                                                            {!! Helper::payment_status_text($payment->status) ?? '' !!}
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col">
                                                        <div class="form-group">
                                                            <label for="">nota transaksi</label><br>
                                                            <textarea name="remark" id="" cols="10" rows="3" class="form-control">{!!$payment->note !!}</textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                                @can('edit-transaksi')
                                                <div class="row">
                                                    <div class="col">
                                                        <div class="form-group">
                                                            <button type="submit" class="btn btn-success btn-lg btn-block font-weight-bold" href="javascript:void;">Kemaskini Transaksi</button>
                                                            <button type="button" class="btn btn-danger btn-lg btn-block font-weight-bold" data-toggle="modal" data-target="#batalModal" data-paymentid="{{$payment->id}}">Batalkan Transaksi</button>

                                                        </div>
                                                    </div>
                                                </div>
                                                @endcan
                                            </div> <!-- end card-body-->
                                        </div> <!-- end card-->
                                    </div>
                                    <!-- end col -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @can('edit-transaksi')
            </form>
            @endcan
        </div>
    </div>
</div>

<div class="modal fade" id="batalModal" tabindex="-1" role="dialog" aria-labelledby="batalModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <meta name="csrf-token" content="{{ csrf_token() }}">
            <form method="POST" action="{{route('admin.tx.manual.batal-transaksi')}}">
                @csrf
            <div class="modal-header">
                <h5 class="modal-title" id="batalModalLabel">Batalkan Transaksi Ini?</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <input type="hidden" class="form-control" id="payment_id" name="payment_id" value="">
                Adakah Anda Pasti?
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light" data-dismiss="modal">Tidak</button>
                <button type="submit" class="btn btn-primary">Ya</button>
            </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@endsection
@push('script')
<script src="https://cdn.jsdelivr.net/npm/jquery-datetimepicker@2.5.21/build/jquery.datetimepicker.full.min.js" integrity="sha256-JADorIjphY5ztERiDARizkSMD2NW4HpsDlAyageYtFc=" crossorigin="anonymous"></script>
<script>
    $('.mask').inputmask();
    jQuery('.paid_at, .registration_date').datetimepicker({
        datepicker: true
        , timepicker: true
        , format: 'Y-m-d H:i:s'
    });

    var clicker = function(obj) {

        var id = $(obj).data('id');
        var amountSelector = $('.' + id).find('.amount');
        var amount = amountSelector.val();

        if (amount > 100) {
            var first_time_fees = 30;
            var first_time_share = 100;
            var additional_share = amount - 130;

            $('.' + id).find('.first_time_fees').val(first_time_fees);
            $('.' + id).find('.first_time_share').val(first_time_share);
            $('.' + id).find('.additional_share').val(additional_share);
        }
    }

    $('#batalModal').on('show.bs.modal', function(event) {

        var button = $(event.relatedTarget)
        var paymentid = button.data('paymentid')

        var modal = $(this)
        modal.find('#payment_id').val(paymentid)
    })

</script>
@endpush
