<div class="card">
    <div class="card-body pd-20">
        <div style="height:300px;overflow-y:scroll">
            <div class="left-timeline mt-3 mb-3 pl-4">
                <ul class="list-unstyled events mb-0">
                    @forelse($payments as $payment)
                    <li class="event-list">
                        <div class="pb-4">
                            <div class="media">
                                <div class="event-date text-center mr-4">
                                    <div class="bg-soft-primary p-1 rounded text-primary font-size-14">
                                        {{$payment->created_at->translatedFormat('d/m/Y h:i:sa')}}</div>
                                </div>
                                <div class="media-body">
                                    <p>
                                        <ul class="list-unstyled">
                                            @if($payment->gateway == 'senangpay')
                                            <li>
                                                <b>Order Ref. No. :</b> <a target="_blank" href="https://app.senangpay.my/payment/receipt/{{$payment->ord_key}}">{{ $payment->order_ref }}</a>
                                            </li>
                                            @elseif($payment->gateway == 'raudhahpay')
                                                @php($bill_no = explode('__', $payment->ord_key))

                                                @if($bill_no[0] == 'CD')
                                                    @php($bill_no[0] = $bill_no[1])
                                                @endif
                                            <li>
                                                @if(config('raudhahpay.is_sandbox'))
                                                    <b>Order Ref. No. :</b> <a target="_blank" href="https://stg.raudhahpay.com/billing/bills/bill-payment?code={{$bill_no[0]}}">{{ $payment->order_ref }}</a>
                                                @else
                                                    <b>Order Ref. No. :</b> <a target="_blank" href="https://cloud.raudhahpay.com/billing/bills/bill-payment?code={{$bill_no[0]}}">{{ $payment->order_ref }}</a>
                                                @endif
                                            </li>
                                            @else
                                            <li>
                                                <b>Order Ref. No. :</b> {{ $payment->order_ref }}
                                            </li>
                                            @endif
                                            <li><b>Cara Pembayaran:</b> {{strtoupper($payment->channel?? '')}}</li>
                                            <li><b>Status:</b> {{strtoupper($payment->status ?? '')}}</li>
                                            <li><b>Payment Gateway:</b> {{strtoupper($payment->gateway ?? '')}}
                                            </li>
                                            <li><b>Jumlah Langganan Syer:</b> RM {{number_format($payment->amount,2,'.',',')}}</li>
                                            <li><b>Fi Pendaftaran Anggota:</b> RM {{number_format($payment->first_time_fees,2,'.',',')}}</li>
                                            <li><b>Langganan Modal Syer:</b> RM {{number_format($payment->first_time_share,2,'.',',')}}</li>
                                            <li><b>Langganan Syer Tambahan:</b> RM {{number_format($payment->additional_share,2,'.',',')}}</li>
                                        </ul>
                                    </p>
                                    <p>{{$payment->text}}</p>
                                </div>
                            </div>
                        </div>
                    </li>
                    @empty
                    <div class="text-center">
                        <h3 class="text-uppercase font-weight-bold">tiada rekod</h3>
                    </div>
                    @endforelse
                </ul>

            </div>
        </div>
    </div>
</div><!-- card -->
