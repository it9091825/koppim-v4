<div class="card">
    <div class="card-body pd-20">
        <div style="height:300px;overflow-y:scroll">
            <div class="left-timeline mt-3 mb-3 pl-4">
                <ul class="list-unstyled events mb-0">
                    @forelse($statuslogs as $statuslog)
                    <li class="event-list">
                        <div class="pb-4">
                            <div class="media">
                                <div class="event-date text-center mr-4">
                                    <div class="bg-soft-primary p-1 rounded text-primary font-size-14">
                                        {{$statuslog->created_at->format('d/m/Y h:i:sa')}}</div>
                                </div>
                                <div class="media-body">
                                    <p><b>DILAKSANAKAN OLEH:</b> {{\App\User::find($statuslog->causer_id)->name}}</p>
                                    <p>
                                        <ul class="list-unstyled">
                                            <li>
                                                @forelse(array($statuslog->properties) as $key=> $property)
                                                    <ul>
                                                    @forelse(json_decode($property) as $key=>$item)
                                                        <li>
                                                        <b>{{strtoupper($key)}}</b>
                                                        <ul>
                                                            @forelse($item as $key=>$i)
                                                                <li>
                                                                    <b>{{$key}}</b>: {{$i}}
                                                                </li>
                                                            @empty
                                                            @endforelse
                                                        </ul>
                                                    </li>
                                                    @empty
                                                    @endforelse
                                                        
                                                    </ul>
                                                @empty
                                                @endforelse
                                            </li>
                                        </ul>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </li>
                    @empty
                    <div class="text-center">
                        <h3 class="text-uppercase font-weight-bold">tiada rekod</h3>
                    </div>
                    @endforelse
                </ul>
            </div>
        </div>
    </div>
</div><!-- card -->
