<div class="card">
    <div class="card-body">
        <div class="row">
            <div class="col pb-3">
                <h4>Rumusan Transaksi Langganan Syer</h4>
                <p>Rumusan transaksi langganan syer yang <span class="badge badge-success text-uppercase">berjaya</span> oleh pelanggan ini.</p>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Fi Pendaftaran</th>
                            <th>Langganan Modal Syer</th>
                            <th>Langganan Syer Tambahan</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>RM{{ number_format($fee,2,'.',',')}}</td>
                            <td>RM{{ number_format($share,2,'.',',')}}</td>
                            <td>RM{{ number_format($addShare,2,'.',',')}}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="row">
            <div class="col pb-3">
                <h4>Transaksi Langganan Syer Terdahulu</h4>
                <p>Senarai transaksi langganan syer terdahulu yang <span class="badge badge-success text-uppercase">berjaya</span> oleh pelanggan ini.</p>
            </div>
        </div>
        <div class="row">
            <div class="col">
                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>tarikh transaksi bayaran</th>
                            <th>jumlah keseluruhan (RM)</th>
                            <th>fi pendaftaran anggota (RM)</th>
                            <th>langganan modal syer (RM)</th>
                            <th>langganan tambahan (RM)</th>
                            <th>cara bayaran</th>
                            <th>
                                tindakan
                            </th>
                        </tr>
                    </thead>
                    <tbody>

                        @forelse($successPayments as $payment)
                        <tr>
                            <td>{{($payment->payment_date) ? $payment->payment_date : ''}}</td>
                            <td>{{number_format($payment->amount,2,'.',',')}}</td>
                            <td>{{number_format($payment->first_time_fees,2,'.',',')}}</td>
                            <td>{{number_format($payment->first_time_share,2,'.',',')}}</td>
                            <td>{{number_format($payment->additional_share,2,'.',',')}}</td>
                            <td>{{strtoupper($payment->channel)}}</td>
                            <td class="text-center">
                                @can('lihat-transaksi')
                                <a href="{{route('admin.tx.manual.kemaskini-transaksi-page',['id'=>$payment->_id,'user_id'=>$user->id])}}" class="text-primary">
                                    <span data-toggle="tooltip" data-placement="auto" data-original-title="Lihat transaksi ini">
                                        <svg class="w-4 h-4" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 12a3 3 0 11-6 0 3 3 0 016 0z"></path>
                                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M2.458 12C3.732 7.943 7.523 5 12 5c4.478 0 8.268 2.943 9.542 7-1.274 4.057-5.064 7-9.542 7-4.477 0-8.268-2.943-9.542-7z"></path>
                                        </svg>
                                    </span></a>
                                @endcan
                                @can('edit-transaksi')
                                <a href="{{route('admin.tx.manual.kemaskini-transaksi-page',['id'=>$payment->_id,'user_id'=>$user->id])}}" class="text-primary">
                                    <span data-toggle="tooltip" data-placement="auto" data-original-title="Edit transaksi ini">
                                        <svg class="w-4 h-4" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M11 5H6a2 2 0 00-2 2v11a2 2 0 002 2h11a2 2 0 002-2v-5m-1.414-9.414a2 2 0 112.828 2.828L11.828 15H9v-2.828l8.586-8.586z"></path>
                                        </svg>
                                    </span></a>
                                @endcan

                            </td>
                        </tr>
                        @empty
                        <td colspan="10" class="text-center font-weight-bold text-uppercase">
                            <h3>tiada rekod</h3>
                        </td>
                        @endforelse
                    </tbody>
                </table>


            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col pb-1">
                <h4>Tambah Transaksi Langganan</h4>
                <p>Setiap transaksi langganan wajib disertakan dengan bukti bayaran yang sah.</p>
            </div>
        </div>
        <form action="{{route('admin.tx.manual.simpan-transaksi')}}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col">

                    <div class="row mb-3">

                        <input type="hidden" name="user_id" value="{{$user->id}}">
                        <input type="hidden" name="user_ic" value="{{$user->ic}}">
                        <input type="hidden" name="user_name" value="{{$user->name}}">
                        <input type="hidden" name="user_mobile_no" value="{{$user->mobile_no}}">
                        <input type="hidden" name="user_email" value="{{$user->email}}">
                        <div class="col text-right">
                            <a id="addrow" class="btn btn-primary btn-sm font-weight-bold" href="javascript:void;"><svg class="w-4 h-4" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 9v3m0 0v3m0-3h3m-3 0H9m12 0a9 9 0 11-18 0 9 9 0 0118 0z"></path>
                                </svg> Tambah Transaksi Langganan</a>
                            <button type="submit" class="btn btn-success btn-sm font-weight-bold" href="javascript:void;"><svg class="w-4 h-4" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z"></path>
                                </svg> Simpan Semua Transaksi</button>
                        </div>
                    </div>
                    <div class="row order-list">
                        <div class="col-6">
                            <div class="card border">
                                <div class="card-header d-flex justify-content-between align-items-center">
                                    <h4>Transaksi #<span class="indexId">1</span></h4>
                                </div>
                                <div class="card-body check1">
                                    {{-- <div class="row mb-3">
                                        <div class="col text-right">
                                            <button type="button" class="btn btn-primary btn-sm" onclick="clicker(this)" data-id="check1" data-toggle="tooltip" data-placement="auto" data-original-title="Hanya masukkan jumlah keseluruhan dan klik butang ini untuk mengira pecahan transaksi pertama pelanggan ini secara automatik"><svg class="w-4 h-4" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 7h6m0 10v-3m-3 3h.01M9 17h.01M9 14h.01M12 14h.01M15 11h.01M12 11h.01M9 11h.01M7 21h10a2 2 0 002-2V5a2 2 0 00-2-2H7a2 2 0 00-2 2v14a2 2 0 002 2z"></path>
                                                </svg> Set Sebagai Transaksi Pertama </button>
                                        </div>

                                    </div> --}}
                                    <div class="row">
                                        <div class="col">
                                            <div class="form-group">
                                                <label for="">JUMLAH KESELURUHAN</label>
                                                <input type="text" class="form-control mask amount" id="amount" name="amount[1]" data-inputmask="'alias': 'numeric', 'autoGroup': true, 'digits': 2,'rightAlign': false, 'digitsOptional': false, 'placeholder': '0'">
                                            </div>
                                        </div>
                                        <div class="col">
                                            <div class="form-group">
                                                <label for="">fi pendaftaran Anggota</label>
                                                <input type="text" class="form-control mask first_time_fees" id="regfee" name="regfee[1]" data-inputmask="'alias': 'numeric', 'autoGroup': true, 'digits': 2,'rightAlign': false, 'digitsOptional': false, 'placeholder': '0'">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            <div class="form-group">
                                                <label for="">langganan modal syer</label>
                                                <input type="text" class="form-control mask first_time_share" id="modalsyer" name="modalsyer[1]" data-inputmask="'alias': 'numeric', 'autoGroup': true, 'digits': 2,'rightAlign': false, 'digitsOptional': false, 'placeholder': '0'">
                                            </div>
                                        </div>
                                        <div class="col">
                                            <div class="form-group">
                                                <label for="">langganan tambahan</label>
                                                <input type="text" class="form-control mask additional_share" id="additionalsyer" name="additionalsyer[1]" data-inputmask="'alias': 'numeric', 'autoGroup': true, 'digits': 2,'rightAlign': false, 'digitsOptional': false, 'placeholder': '0'">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            <div class="form-group">
                                                <label for="">Tarikh Transaksi Bayaran</label>
                                                <input type="text" name="paid_at[1]" class="form-control paid_at" value="">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            <div class="form-group">
                                                <label for="">bukti pembayaran</label><br>
                                                <input type="file" id="proof" class="" name="proof[1]">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            <div class="form-group">
                                                <label for="">Status Transaksi</label><br>
                                                <select name="status[1]" class="form-control select2" id="">
                                                    <option value="">Sila Pilih</option>
                                                    <option value="payment-completed">Transaksi Berjaya</option>
                                                    <option value="waiting-for-verification">Untuk Disemak</option>
                                                    <option value="waiting-for-approval">Untuk Diluluskan</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            <div class="form-group">
                                                <label for="">Channel Transaksi</label><br>
                                                <select name="channel[1]" class="form-control select2" id="">
                                                    <option value="">Sila Pilih</option>
                                                    <option value="manual">Manual</option>
                                                    <option value="ikoop">IKoop</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            <div class="form-group">
                                                <label for="">nota transaksi</label><br>
                                                <textarea name="remark[1]" id="" cols="10" rows="3" class="form-control"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div> <!-- end card-body-->
                            </div> <!-- end card-->
                        </div>
                        <!-- end col -->
                    </div>


                </div>
            </div>
        </form>
    </div>
</div>
</form>

@push('script')
<script>
    $(document).ready(function() {

        var counter = 2;

        $("#addrow").on("click", function() {
            var newRow = $("<div class='col-6'>");
            var cols = `
                    <div class="card border">
                        <div class="card-header d-flex align-items-center justify-content-between">
                            <h4>Transaksi #<span class="indexId"></span></h4>
                            <a href="javascript:void;" class="ibtnDel btn btn-danger btn-sm mt-2 float-right"><svg class="w-4 h-4" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16"></path></svg> Padam</a>
                        </div>
                        <div class="card-body check${counter}">
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="">JUMLAH KESELURUHAN</label>
                                        <input type="text" class="form-control mask amount" id="amount${counter}" name="amount[${counter}]" data-inputmask="'alias': 'numeric', 'autoGroup': true, 'digits': 2,'rightAlign': false, 'digitsOptional': false, 'placeholder': '0'">
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <label for="">fi pendaftaran Anggota</label>
                                        <input type="text" class="form-control mask first_time_fees" id="regfee${counter}" name="regfee[${counter}]" data-inputmask="'alias': 'numeric', 'autoGroup': true, 'digits': 2,'rightAlign': false, 'digitsOptional': false, 'placeholder': '0'">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="">langganan modal syer</label>
                                        <input type="text" class="form-control mask first_time_share" id="modalsyer${counter}" name="modalsyer[${counter}]" data-inputmask="'alias': 'numeric', 'autoGroup': true, 'digits': 2,'rightAlign': false, 'digitsOptional': false, 'placeholder': '0'">
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <label for="">langganan tambahan</label>
                                        <input type="text" class="form-control mask additional_share" id="additionalsyer${counter}" name="additionalsyer[${counter}]"data-inputmask="'alias': 'numeric', 'autoGroup': true, 'digits': 2,'rightAlign': false, 'digitsOptional': false, 'placeholder': '0'">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="">Tarikh Transaksi Bayaran</label>
                                        <input type="text"  id="paid_at${counter}" name="paid_at[${counter}]" class="form-control paid_at" value="">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="">bukti pembayaran</label><br>
                                        <input type="file" id="proof${counter}" class="" name="proof[${counter}]">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="">Status Transaksi</label><br>
                                        <select name="status[${counter}]" class="form-control select2" id="status${counter}">
                                            <option value="">Sila Pilih</option>
                                            <option value="payment-completed">Transaksi Berjaya</option>
                                            <option value="waiting-for-verification">Untuk Disemak</option>
                                            <option value="waiting-for-approval">Untuk Diluluskan</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="">Channel Transaksi</label><br>
                                        <select name="channel[${counter}]" class="form-control select2" id="channel${counter}">
                                            <option value="">Sila Pilih</option>
                                            <option value="manual">Manual</option>
                                            <option value="ikoop">IKoop</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="">nota transaksi</label><br>
                                        <textarea name="remark[${counter}]" id="remark${counter}" cols="10" rows="3" class="form-control"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div> <!-- end card-body-->
                    </div> <!-- end card-->
            `;

            newRow.append(cols);
            $(".order-list").append(newRow);

            counter++;
            reIndex();
            //initialize balik script
            utilitiesinit();
        });

        $(".order-list").on("click", ".ibtnDel", function(event) {
            $(this).closest(".col-6").remove();
            counter--;
            reIndex();
        });

        function reIndex() {
            var rows = $('.col-6');
            rows.each(function(i, row) {
                $(row).find('.indexId').html(i + 1);
            });
        }
    });


    var clicker = function(obj) {

        var id = $(obj).data('id');
        var amountSelector = $('.' + id).find('.amount');
        var amount = amountSelector.val();

        if (amount > 100) {
            var first_time_fees = 30;
            var first_time_share = 100;
            var additional_share = amount - 130;

            $('.' + id).find('.first_time_fees').val(first_time_fees);
            $('.' + id).find('.first_time_share').val(first_time_share);
            $('.' + id).find('.additional_share').val(additional_share);
        }
    };

</script>
@endpush
