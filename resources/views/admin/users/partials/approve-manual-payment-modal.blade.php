<div id="sahkanModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="sahkanModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="sahkanModalLabel">SEMAKAN BAYARAN MANUAL</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <meta name="csrf-token" content="{{ csrf_token() }}">
            <form method="POST" action="{{route('admin.tx.manual.sahkan')}}">

                @csrf
                <div class="modal-body">
                    <div class="row">
                        <div class="col">
                            <input type="hidden" class="form-control" id="proof_id" name="proof_id">
                            <div class="form-group mb-1">
                                <label for="name" class="col-form-label">Nama:</label>
                                <input type="text" class="form-control" id="name" name="name" required>
                            </div>
                            <div class="form-group mb-1">
                                <label for="ic" class="col-form-label">No. Mykad:</label>
                                <input type="text" class="form-control" id="ic" name="ic" required>
                            </div>
                            <div class="form-group mb-1">
                                <label for="mobile_no" class="col-form-label">No. Telefon:</label>
                                <input type="text" class="form-control" id="mobile_no" name="mobile_no" required>
                            </div>
                            <div class="form-group mb-1">
                                <label for="email" class="col-form-label">Emel:</label>
                                <input type="text" class="form-control" id="email" name="email" required>
                            </div>
                            <div class="form-group mb-1">
                                <label for="payment_date" class="col-form-label">TARIKH TRANSAKSI:</label>
                                <input type="text" class="form-control" id="payment_date" name="payment_date" required>
                            </div>
                            <div class="form-group mb-1">
                                <label for="amount" class="col-form-label">Jumlah Keseluruhan:</label>
                                <input type="text" class="form-control mask" id="amount" name="amount"
                                       data-inputmask="'alias': 'numeric', 'autoGroup': true, 'digits': 2,'rightAlign': false, 'digitsOptional': false, 'placeholder': '0'"
                                       required>
                            </div>
                            <div class="form-group mb-1">
                                <label for="first_time_fees" class="col-form-label">Fi Pendaftaran Anggota
                                    Baharu:</label>
                                <input type="text" class="form-control mask" id="first_time_fees" name="first_time_fees"
                                       data-inputmask="'alias': 'numeric', 'autoGroup': true, 'digits': 2,'rightAlign': false, 'digitsOptional': false, 'placeholder': '0'"
                                       required>

                            </div>
                            <div class="form-group mb-1">
                                <label for="first_time_share" class="col-form-label">Langganan Modal Syer:</label>
                                <input type="text" class="form-control mask" id="first_time_share"
                                       name="first_time_share"
                                       data-inputmask="'alias': 'numeric', 'autoGroup': true, 'digits': 2,'rightAlign': false, 'digitsOptional': false, 'placeholder': '0'"
                                       required>

                            </div>
                            <div class="form-group mb-1">
                                <label for="additional_share" class="col-form-label">Langganan Tambahan:</label>
                                <input type="text" class="form-control mask" id="additional_share"
                                       name="additional_share"
                                       data-inputmask="'alias': 'numeric', 'autoGroup': true, 'digits': 2,'rightAlign': false, 'digitsOptional': false, 'placeholder': '0'"
                                       required>
                            </div>
                            <div class="form-group mb-1">
                                <label for="note" class="col-form-label">Nota Admin:</label>
                                <textarea name="note" id="note" cols="10" rows="3" class="form-control"></textarea>
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group mb-1">
                                <div id="originalProof"></div>
                                <div id="proof"></div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info" id="simpanData">Kemaskini</button>
                    @can('sahkan-bayaran-manual')
                        <button type="submit" name="submitButton" value="hantar" class="btn btn-primary">Hantar</button>
                    @endcan

                        <button type="submit" name="submitButton" value="semakan_lanjut" class="btn btn-danger">Untuk Semakan Lanjut</button>

                    <button type="button" class="btn btn-light" data-dismiss="modal">Tutup</button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
@push('script')

    <script>
        $('#sahkanModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget)
            var userId = button.data('userid')

            $.get("/admin/tx/manual/data/" + userId, function (data) {
                var modal = $('#sahkanModal');
                modal.find('#proof_id').val(data._id)
                modal.find('#name').val(data.name)
                modal.find('#ic').val(data.ic)
                modal.find('#mobile_no').val(data.phone)
                modal.find('#email').val(data.email)
                modal.find('#amount').val(data.amount)
                modal.find('#first_time_fees').val(data.first_time_fees)
                modal.find('#first_time_share').val(data.first_time_share)
                modal.find('#additional_share').val(data.additional_share)
                modal.find('#note').val(data.note)
                modal.find('#payment_date').val(data.payment_date)
                modal.find('#proof').html(`<img src="{{env('DO_SPACES_FULL_URL')}}${data.payment_proof}" class="img-fluid img-thumbnail">`);
                modal.find('#originalProof').html(`<a href="{{env('DO_SPACES_FULL_URL')}}${data.payment_proof}" target="_blank">Lihat Bukti Pembayaran dalam saiz asal</a>`)
                modal.find('#proofEditUrl').attr("href", "/admin/tx/manual/bukti-bayaran/edit/" + data._id);
            });
        });

        $("#simpanData").click(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.post("{{route('admin.tx.manual.simpan')}}",
                {
                    proof_id: $('#sahkanModal').find('#proof_id').val(),
                    name: $('#sahkanModal').find('#name').val(),
                    ic: $('#sahkanModal').find('#ic').val(),
                    mobile_no: $('#sahkanModal').find('#mobile_no').val(),
                    email: $('#sahkanModal').find('#email').val(),
                    amount: $('#sahkanModal').find('#amount').val(),
                    first_time_fees: $('#sahkanModal').find('#first_time_fees').val(),
                    first_time_share: $('#sahkanModal').find('#first_time_share').val(),
                    additional_share: $('#sahkanModal').find('#additional_share').val(),
                    note: $('#sahkanModal').find('#note').val(),
                    payment_date: $('#sahkanModal').find('#payment_date').val(),
                },
                function (data, status) {
                    if (status == 'success') {
                        Toast.fire({
                            icon: "success",
                            title: "Perubahan berjaya disimpan"
                        });
                    } else {
                        Toast.fire({
                            icon: "error",
                            title: "Ralat sewaktu simpan perubahan"
                        });
                    }
                });
        });
    </script>
@endpush
