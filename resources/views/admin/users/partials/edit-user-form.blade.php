@include('admin.users.partials.edit-user.akaun')

@include('admin.users.partials.edit-user.kata-laluan')

@include('admin.users.partials.edit-user.peribadi')

@include('admin.users.partials.edit-user.mykad')

@include('admin.users.partials.edit-user.bank')

@if($user->status == 96)
    @include('admin.users.partials.edit-user.sebab-berhenti')
@endif
