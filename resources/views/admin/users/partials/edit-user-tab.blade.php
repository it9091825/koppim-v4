<ul class="nav nav-pills navtab-bg nav-justified" id="pills-tab" role="tablist">
    <li class="nav-item">
        <a class="nav-link active text-uppercase" id="pills-activity-tab" data-toggle="pill" href="#pills-activity" role="tab" aria-controls="pills-activity" aria-selected="true">
            Maklumat Peribadi Anggota
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link text-uppercase" id="pills-transaksi-tab"  data-toggle="pill" href="#pills-transaksi" role="tab" aria-controls="pills-transaksi" aria-selected="false">
            Transaksi Langganan
        </a>
    </li> 
    <li class="nav-item">
        <a class="nav-link text-uppercase" id="pills-log-pembayaran-tab"  data-toggle="pill" href="#pills-log-pembayaran" role="tab" aria-controls="pills-messages" aria-selected="false">
            Log Pembayaran
        </a>
    </li> 
    <li class="nav-item">
        <a class="nav-link text-uppercase" id="pills-log-sms-tab"  data-toggle="pill" href="#pills-log-sms" role="tab" aria-controls="pills-log-sms" aria-selected="false">
            Log SMS
        </a>
    </li>  
    <li class="nav-item">
        <a class="nav-link text-uppercase" id="pills-log-status-anggota-tab"  data-toggle="pill" href="#pills-log-status-anggota" role="tab" aria-controls="pills-log-status-anggota" aria-selected="false">
            Log Status Anggota
        </a>
    </li>  
    <li class="nav-item">
        <a class="nav-link text-uppercase" id="pills-log-surat-tab"  data-toggle="pill" href="#pills-log-surat" role="tab" aria-controls="pills-log-surat" aria-selected="false">
            Log Surat
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link text-uppercase" id="pills-log-dividen-tab"  data-toggle="pill" href="#pills-log-dividen" role="tab" aria-controls="pills-log-dividen" aria-selected="false">
            Log Dividen
        </a>
    </li> 
</ul>