<option value="AFFIN BANK">AFFIN BANK</option>
<option value="ALLIANCE BANK">ALLIANCE BANK</option>
<option value="AL-RAJHI BANK">AL-RAJHI BANK</option>
<option value="AGROBANK">AGROBANK</option>
<option value="AMBANK">AMBANK</option>
<option value="BNP PARIBAS MALAYSIA">BNP PARIBAS MALAYSIA</option>
<option value="BANGKOK BANK">BANGKOK BANK</option>
<option value="BANK OF AMERICA MALAYSIA">BANK OF AMERICA MALAYSIA</option>
<option value="BANK OF CHINA (MALAYSIA)">BANK OF CHINA (MALAYSIA)</option>
<option value="BANK ISLAM">BANK ISLAM</option>
<option value="BANK MUAMALAT">BANK MUAMALAT</option>
<option value="BANK RAKYAT">BANK RAKYAT</option>
<option value="BANK SIMPANAN NASIONAL">BANK SIMPANAN NASIONAL</option>
<option value="CIMB BANK">CIMB BANK</option>
<option value="CHINA CONSTRUCTION BANK (MALAYSIA)">CHINA CONSTRUCTION BANK
    (MALAYSIA)
</option>
<option value="CITIBANK BERHAD">CITIBANK BERHAD</option>
<option value="DEUTSCHE BANK (MALAYSIA)">DEUTSCHE BANK (MALAYSIA)</option>
<option value="HSBC BANK MALAYSIA">HSBC BANK MALAYSIA</option>
<option value="HONG LEONG BANK">HONG LEONG BANK</option>
<option value="INDIA INTERNATIONAL BANK (MALAYSIA)">INDIA INTERNATIONAL BANK
    (MALAYSIA)
</option>
<option value="INDUSTRIAL AND COMMERCIAL BANK OF CHINA (MALAYSIA)">
    INDUSTRIAL AND COMMERCIAL BANK OF CHINA (MALAYSIA)
</option>
<option value="J.P MORGAN CHASE BANK">J.P MORGAN CHASE BANK</option>
<option value="KUWAIT FINANCE HOUSE">KUWAIT FINANCE HOUSE</option>
<option value="MUFG BANK (MALAYSIA)">MUFG BANK (MALAYSIA)</option>
<option value="MALAYAN BANKING (MAYBANK)">MALAYAN BANKING (MAYBANK)</option>
<option value="MIZUHO BANK (MALAYSIA)">MIZUHO BANK (MALAYSIA)</option>
<option value="OCBC BANK">OCBC BANK</option>
<option value="PUBLIC BANK">PUBLIC BANK</option>
<option value="RHB BANK">RHB BANK</option>
<option value="STANDARD CHARTERED BANK">STANDARD CHARTERED BANK</option>
<option value="SUMITOMO MITSUI BANKING">SUMITOMO MITSUI BANKING</option>
<option value="THE BANK OF NOVA SCOTIA">THE BANK OF NOVA SCOTIA</option>
<option value="UNITED OVERSEAS BANK">UNITED OVERSEAS BANK</option>
