<ul class="nav nav-pills navtab-bg nav-justified mb-4" id="pills-tab" role="tablist">
    <li class="nav-item">
    <a href="{{url('/admin/members/ikoop')}}" class="nav-link text-uppercase {{ Helper::active(url('/admin/members/ikoop')) }} {{Helper::active(url(''))}}">
            Semua
        </a>
    </li>
    <li class="nav-item">
        <a href="{{url('/admin/members/ikoop?status=1')}}" class="nav-link text-uppercase {{ Helper::active(url('/admin/members/ikoop?status=1')) }} {{request()->query('status') == '1' ? 'active':''}}" id="pills-tasks-tab">
            Diluluskan
        </a>
    </li>

    <li class="nav-item">
        <a href="{{url('/admin/members/ikoop?status=2')}}" class="nav-link text-uppercase {{ Helper::active(url('/admin/members/ikoop?status=2')) }} {{request()->query('status') == '2' ? 'active':''}}" id="pills-projects-tab">
            Untuk Disemak
        </a>
    </li>
    <li class="nav-item">
        <a href="{{url('/admin/members/ikoop?status=0')}}" class="nav-link text-uppercase {{ Helper::active(url('/admin/members/ikoop?status=0')) }}" id="pills-messages-tab">
            Belum Dikemaskini
        </a>
    </li>
    <li class="nav-item">
        <a href="{{url('/admin/members/ikoop?status=99')}}" class="nav-link text-uppercase {{ Helper::active(url('/admin/members/ikoop?status=99')) }} {{request()->query('status') == '99' ? 'active':''}}" id="pills-tasks-tab">
            Ditolak
        </a>
    </li>
</ul>
