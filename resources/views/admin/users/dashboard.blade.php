@extends('admin.layouts')
@section('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.css" integrity="sha512-SUJFImtiT87gVCOXl3aGC00zfDl6ggYAw5+oheJvRJ8KBXZrr/TMISSdVJ5bBarbQDRC2pR5Kto3xTR0kpZInA==" crossorigin="anonymous" />
@endsection
@pagetitle(['title'=>'Laman Utama','links'=>['Keanggotaan']])@endpagetitle

@section('content')
<div class="container mt-3 mb-5">

    {{-- Anggota --}}
    <div class="row">
        <div class="col">
            <h3>Anggota</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-12">
            <!-- Portlet card -->
            <div class="card">
                <div class="card-body">
                    <h4 class="header-title mt-0 mb-3" style="text-align: center;">PENDAFTARAN ANGGOTA BAHARU</h4>

                    <div id="apex-line-2" class="apex-charts" dir="ltr"></div>

                    <div class="table-responsive">
                        <table class="table">
                            <thead class="thead-primary">
                                <tr>
                                    <th style="text-align: center;">TARIKH</th>
                                    <th style="text-align: center;">PENDAFTARAN ANGGOTA</th>
                                    <th style="text-align: center;">FI PENDAFTARAN ANGGOTA</th>
                                    <th style="text-align: center;">LANGGANAN MODAL SYER</th>
                                    <th style="text-align: center;">LANGGANAN TAMBAHAN</th>
                                    <th style="text-align: center;">JUMLAH KESELURUHAN</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                $sumMember = 0;
                                $sumFee = 0;
                                $sumModal = 0;
                                $sumShare = 0;
                                $sumTotal = 0;
                                @endphp
                                @foreach($users as $date => $user_list)
                                <tr>
                                    <td class="text-center">
                                        {{$date}}
                                    </td>
                                    <td class="text-center">
                                        {{$memberCount = $user_list->count()}}
                                    </td>
                                    @php
                                    $dateStr = strtotime(str_replace("/", "-", $date));
                                    $first = new \MongoDB\BSON\UTCDateTime($dateStr * 1000);
                                    $newDate = strtotime("+1 day", $dateStr);
                                    $second = new \MongoDB\BSON\UTCDateTime($newDate * 1000);
                                    $payments = App\Mongo\Payment::where('channel','online')->where('returncode','100')->where('created_at' ,'>=',$first)->where('created_at' ,'<=',$second)->get();
                                        $one = $payments->sum('first_time_fees');
                                        $two = $payments->sum('first_time_share');
                                        $three = $payments->sum('additional_share');
                                        $total = $one+$two+$three;
                                        $sumMember += $memberCount;
                                        $sumFee += $one;
                                        $sumModal += $two;
                                        $sumShare += $three;
                                        $sumTotal += $total;
                                        @endphp
                                        <td class="text-center">
                                            RM{{number_format($one,2,'.',',')}}
                                        </td>
                                        <td class="text-center">
                                            RM{{number_format($two,2,'.',',')}}
                                        </td>
                                        <td class="text-center">
                                            RM{{number_format($three,2,'.',',')}}
                                        </td>
                                        <td class="text-center">
                                            RM{{number_format($total,2,'.',',')}}
                                        </td>

                                </tr>
                                @endforeach
                                <tr>
                                    <td class="text-center">JUMLAH</td>
                                    <td class="text-center">{{number_format($sumMember,0,'.',',')}}</td>
                                    <td class="text-center">RM{{number_format($sumFee,2,'.',',')}}</td>
                                    <td class="text-center">RM{{number_format($sumModal,2,'.',',')}}</td>
                                    <td class="text-center">RM{{number_format($sumShare,2,'.',',')}}</td>
                                    <td class="text-center">RM{{number_format($sumTotal,2,'.',',')}}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div> <!-- end card-body -->
            </div> <!-- end card-->
        </div> <!-- end col-->

    </div>


</div>
</div>
@endsection

@section('js')
<script src="{{asset('admin/libs/apexcharts/apexcharts.min.js')}}"></script>

<script>
    let e = {
        chart: {
            height: 380
            , type: "line"
            , shadow: {
                enabled: !1
                , color: "#bbb"
                , top: 3
                , left: 2
                , blur: 3
                , opacity: 1
            }
        }
        , stroke: {
            width: 5
            , curve: "smooth"
        }
        , series: [{
            name: "Pendaftaran"
            , data: [ <
                ? php
                for ($i = 0; $i < 15; ++$i) {

                    $date = date('Y-m-d', strtotime('today - '.$i.
                        ' days'));
                    $user = App\ User::whereRaw("DATE(created_at) = '".$date.
                        "'") - > where('type', 'user') - > where('ikoop', 0) - > count();

                    ?
                    >

                    <
                    ? php echo $user; ? > ,

                    <
                    ? php
                } ? >

            ]
        }]
        , xaxis: {
            type: "datetime"
            , categories: [ <
                ? php
                for ($i = 0; $i < 15; ++$i) {
                    $date = date('Y-m-d', strtotime('today - '.$i.
                        ' days')); ?
                    >

                    "<?php echo $date; ?>",

                    <
                    ? php
                } ? >

            ]
        }
        , title: {
            text: "Jumlah Pendaftaran"
            , align: "center"
            , style: {
                fontSize: "14px"
                , color: "#666"
            }
        }
        , fill: {
            type: "gradient"
            , gradient: {
                shade: "dark"
                , gradientToColors: ["#43d39e"]
                , shadeIntensity: 1
                , type: "vertical"
                , opacityFrom: 1
                , opacityTo: 1
                , stops: [0, 100, 100, 100]
            }
        }
        , markers: {
            size: 4
            , opacity: .9
            , colors: ["#50a5f1"]
            , strokeColor: "#fff"
            , strokeWidth: 2
            , style: "inverted"
            , hover: {
                size: 7
            }
        }
        , yaxis: {
            min: 0
            , max: 100
            , title: {
                text: "Jumlah"
            }
        }
        , grid: {
            row: {
                colors: ["transparent", "transparent"]
                , opacity: .2
            }
            , borderColor: "#185a9d"
        }
        , responsive: [{
            breakpoint: 600
            , options: {
                chart: {
                    toolbar: {
                        show: !1
                    }
                }
                , legend: {
                    show: !1
                }
            }
        }]
    };
    new ApexCharts(document.querySelector("#apex-line-2"), e).render();

</script>
@endsection
