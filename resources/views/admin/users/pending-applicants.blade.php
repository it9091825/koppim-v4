@extends('admin.layouts')
@pagetitle(['title'=>'Belum Dikemaskini','links'=>['Pendaftaran']])@endpagetitle

@section('content')
<div class="container">
    <div class="row">
        @include('include.search',['url'=>url()->current(),'placeholder'=>'carian menggunakan nama, no. telefon atau no. mykad'])
    </div>

    <div class="row ">
        <div class="col-lg-12">
            <div class="d-flex justify-content-between align-items-center">
                <div class="d-flex justify-content-center align-items-center mb-3">
                    @if($members)
                    Paparan {{number_format($members->firstItem(),0,"",",")}} Hingga {{ number_format($members->lastItem(),0,"",",")}} Daripada {{number_format($members->total(),0,"",",")}} Dalam Senarai
                    @endif
                </div>
                <div class="d-flex justify-content-center align-items-center">
                    {{ $members->appends(request()->query())->links() }}
                </div>
            </div>
        </div>
        <div class="col-12">
            <div class="form-group">
                <button type="submit" class="btn btn-success text-uppercase font-weight-bold" id="approveAllButton">Hantar SMS <span id="checkboxCountText"></span></button>
            </div>
        </div>
        <div class="col-lg-12 col-xl-12 mg-t-12">
            <div class="card">
                <div class="card-body p-0">
                    <div class="table-responsive">
                        <table class="table">
                            <thead class="">
                                <tr>
                                    <th><input type="checkbox" id="checkAll"></th>
                                    <th class="text-center">@sortablelink('created_at',new HtmlString('TARIKH<BR>DAFTAR'))</th>
                                    <th>@sortablelink('name','NAMA')</th>
                                    <th class="text-center">NO. Anggota KoPPIM</th>
                                    <th class="text-center">NO. MYKAD</th>
                                    <th class="text-center">STATUS KEANGGOTAAN</th>
                                    <th class="text-center"><span data-toggle="tooltip" title="Jumlah sms peringatan yang telah dihantar kepada pelanggan ini"><svg class="w-4 h-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor">
                                        <path fill-rule="evenodd" d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7-4a1 1 0 11-2 0 1 1 0 012 0zM9 9a1 1 0 000 2v3a1 1 0 001 1h1a1 1 0 100-2v-3a1 1 0 00-1-1H9z" clip-rule="evenodd" />
                                      </svg> LOG PERINGATAN</span></th>
                                    <th class="text-center">LOG MASUK</th>
                                    <th class="text-center">JUMLAH BAYARAN</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse($members as $key=>$member)
                                <tr>
                                    <td><input type="checkbox" class="checkItem" name="member[{{$key}}]" value="{{$member->id}}"></td>
                                    <td class="text-center">{!! $member->created_at->format("d/m/Y \<\b\\r\> h:i:sa") !!}</td>
                                    <td><a href="{{route('admin.member.view',$member->id)}}">{{ $member->name }}</a></td>
                                    <td class="text-center">{{ $member->no_koppim ?? 'TIADA NO. ANGGOTA KoPPIM' }}</td>
                                    <td class="text-center"><a href="{{route('admin.member.view',$member->id)}}">{{ $member->ic }}</a></td>
                                    <td class="text-center">
                                        {!! Helper::status_text($member->status) !!}
                                    </td>
                                <td class="text-center">{{$member->sms_reminder_logs->count()}}</td>
                                    <td class="text-center">
                                        @php
                                        $last_login = \Spatie\Activitylog\Models\Activity::where('log_name','user_auth')->where('subject_id',$member->id)->latest()->first();
                                        @endphp
                                        {!! ($last_login) ? $last_login->created_at->format('d/m/Y \<\b\\r\> h:i:sa') : 'TIADA' !!}
                                    </td>
                                    <td class="text-center">
                                        @php
                                        $total = App\Mongo\Payment::where('ic',$member->ic)->where('returncode','100')->sum('amount');
                                        @endphp
                                        RM{{number_format($total,2,".",",")}}
                                    </td>
                                </tr>
                                @empty
                                <tr>
                                    <td colspan="10" class="text-center font-weight-bold text-uppercase">
                                        <h3>tiada rekod</h3>
                                    </td>
                                </tr>
                                @endforelse
                            </tbody>
                        </table>

                    </div>


                </div><!-- card-body -->
            </div><!-- card -->
        </div>


    </div>




</div><!-- container -->

@endsection

@section('js')
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $(document).ready(function() {

        $("#checkAll").prop('checked', false);
        $("#approveAllButton").prop('disabled', true);
        $('input.checkItem').prop('checked', false);

    });

    var checkboxes = $('input.checkItem');

    function initCheckboxesCount() {
        var countCheckedCheckboxes = checkboxes.filter(':checked').length;
        if (countCheckedCheckboxes) {
            $('#checkboxCountText').text(`KEPADA ${countCheckedCheckboxes} PELANGGAN`);
            $("#approveAllButton").prop('disabled', false);
        } else {
            $('#checkboxCountText').text('');
            $("#approveAllButton").prop('disabled', true);
        }
    }
    checkboxes.change(function() {
        initCheckboxesCount();
    });

    $("#checkAll").click(function() {

        $('input.checkItem').not(this).prop('checked', this.checked);
        initCheckboxesCount();
    });

    function queries() {

        var members = []
        var checkboxes = document.querySelectorAll('input.checkItem[type=checkbox]:checked')

        for (var i = 0; i < checkboxes.length; i++) {
            members.push(checkboxes[i].value)
        }

        return members;
    }
    $('#approveAllButton').click(function() {

        Swal.fire({
            title: 'Sila masukkan mesej peringatan untuk dihantar.'
            , input: 'textarea'
            , inputAttributes: {
                autocapitalize: 'off',

            }
            , inputValue: 'Sila kemaskini profil KoPPIM anda.'
            , showCancelButton: true
            , cancelButtonText: 'Batal'
            , confirmButtonText: 'Hantar'
        , }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: 'POST'
                    , url: "{{route('admin.member.send_reminder')}}"
                    , data: {
                        members: queries()
                        , message: result.value
                    }
                    , success: function(data) {
                        if (data.success == true) {
                            Swal.fire({
                                title: 'Berjaya!'
                                ,text:'SMS peringatan kepada pelanggan berjaya dihantar'
                                , icon: 'success'
                            , });
                            location.reload();
                        } else {
                            Swal.fire({
                                title: 'Ralat'
                                , text: 'SMS tidak dapat dihantar kepada pelanggan. Sila maklumkan perkara ini kepada system developer KoPPIM'
                                , icon: 'error'
                            })
                        }
                    }
                });
            }
        })


    });

</script>
<script>
    function search_data() {
        var search = $('#search').val();

        window.location.href = "{{route('admin.member.active')}}?search=" + search + "";
    }

    $(document).on("keypress", ".input-group:has(input:input, span.input-group-btn:has(.btn)) input:input", function(e) {
        if (e.which == 13) {
            $(this).closest(".input-group").find(".input-group-append > .submit").click();
        }
    });

</script>

@endsection
