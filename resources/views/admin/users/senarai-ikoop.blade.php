@extends('admin.layouts')

@pagetitle(['title'=>'Senarai Semua (IKOOP)','links'=>['Keanggotaan']])@endpagetitle

@section('content')
<div class="container">
    <div class="row" style="">
        @include('include.search',['url'=>url()->current(),'placeholder'=>'carian menggunakan nama, no. telefon atau no. mykad'])
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="d-flex justify-content-between align-items-center">
                <div class="d-flex justify-content-center align-items-center mb-3">
                    @if($members)
                    Paparan {{number_format($members->firstItem(),0,"",",")}} Hingga {{ number_format($members->lastItem(),0,"",",")}} Daripada {{number_format($members->total(),0,"",",")}} Dalam Senarai
                    @endif
                </div>
                <div class="d-flex justify-content-center align-items-center">
                    {{ $members->appends(request()->query())->links() }}
                </div>
            </div>
        </div>
        <div class="col-lg-12 col-xl-12">
            <div class="card">
                <div class="card-body">
                    @include('admin.users.partials.ikoop-categories-tab')
                    <div class="table-responsive">
                        <table class="table">
                            <thead class="thead-primary">
                                <tr>
                                    <th class="text-center">@sortablelink('created_at', strtoupper('tarikh daftar'))</th>
                                    <th>@sortablelink('name', strtoupper('nama'))</th>
                                    <th class="text-center">NO. Anggota KoPPIM</th>
                                    <th class="text-center">NO. MYKAD</th>
                                    <th class="text-center">STATUS KEANGGOTAAN</th>
                                    <th class="text-center">LOG MASUK</th>
                                    <th class="text-center">JUMLAH BAYARAN</th>
                                    <th class="text-center">TINDAKAN</th>
                                </tr>
                            </thead>
                            <tbody>

                                @forelse($members as $member)
                                <tr>
                                    <td class="text-center">{!! $member->created_at->format("d/m/Y \<\b\\r\> h:i:sa") !!}</td>
                                    <td><a href="{{route('admin.member.view',$member->id)}}">{{ $member->name }}</a></td>
                                    <td class="text-center">{{ $member->no_koppim ?? 'TIADA NO. ANGGOTA KoPPIM' }}</td>
                                    <td class="text-center"><a href="{{route('admin.member.view',$member->id)}}">{{ $member->ic }}</a></td>
                                    <td class="text-center">{!! Helper::status_text($member->status) !!}
                                    </td>
                                    <td class="text-center">
                                        @php
                                        $last_login = \Spatie\Activitylog\Models\Activity::where('log_name','user_auth')->where('subject_id',$member->id)->latest()->first();
                                        @endphp
                                        {!! ($last_login) ? $last_login->created_at->format('d/m/Y \<\b\\r\> h:i:sa') : 'TIADA' !!}
                                    </td>
                                    <td class="text-center">
                                        @php
                                        $total = App\Mongo\Payment::where('ic',$member->ic)->where('returncode','100')->sum('amount');
                                        @endphp
                                        RM{{number_format($total,2,".",",")}}
                                    </td>
                                    <td class="text-center">
                                        @can('arkib-anggota')
                                        @if($total < 0.01)
                                        <span data-toggle="tooltip" data-placement="auto" data-original-title="Arkib" class="btn">
                                            <svg data-toggle="modal" data-route="{{route('admin.member.archive',$member)}}" data-member="'{{$member}}'" data-name="{!!$member->name!!}" data-ic="{{$member->ic}}" data-target="#modalDelete" class="w-6 h-6 text-danger" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16"></path>
                                            </svg>
                                            </span>
                                            <div class="modal" tabindex="-1" id="modalDelete" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog modal-lg">
                                                    <div class="modal-content">
                                                        <div class="modal-body">
                                                            <form enctype="multipart/form-data" method="post" class="deleteForm">
                                                                @csrf
                                                                <h4>Adakah anda pasti mahu padam rekod pelanggan ini?</h4>
                                                                <h5><span class="text-danger">Peringatan: Rekod yang telah dipadam tidak boleh dipulihkan semula.</span></h5>
                                                                <table class="table table-bordered">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td class="font-weight-bold">Nama</td>
                                                                            <td><span class="name"></span></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td class="font-weight-bold">No. MYKAD</td>
                                                                            <td><span class="ic"></span></td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                                <button class="btn btn-danger btn-lg font-weight-bold">
                                                                    Padam
                                                                </button>
                                                                <button type="button" class="btn btn-light btn-lg font-weight-bold" data-dismiss="modal">
                                                                    Batal
                                                                </button>
                                                            </form>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>

                                            @endif
                                            @endcan
                                    </td>
                                </tr>
                                @empty
                                @tablenoresult
                                @endtablenoresult
                                @endforelse
                            </tbody>
                        </table>

                    </div>
                </div><!-- card-body -->
            </div><!-- card -->
        </div>
    </div>

</div><!-- container -->

@endsection

@section('js')

<script>
    function search_data() {
        var search = $('#search').val();

        window.location.href = "{{route('admin.member.ikoop')}}?search=" + search + "";
    }

    $(document).on("keypress", ".input-group:has(input:input, span.input-group-btn:has(.btn)) input:input", function(e) {
        if (e.which == 13) {
            $(this).closest(".input-group").find(".input-group-append > .submit").click();
        }
    });

    $('#modalDelete').on('show.bs.modal', function(event) {
        var member = $(event.relatedTarget).data('member');
        var route = $(event.relatedTarget).data('route');
        var name = $(event.relatedTarget).data('name');
        var ic = $(event.relatedTarget).data('ic');
        $(this).find(".modal-body .member").text(member);
        $(this).find(".modal-body .name").text(name);
        $(this).find(".modal-body .ic").text(ic);
        $(this).find(".modal-body .deleteForm").attr('action', route);
    });

</script>
@endsection
