@extends('admin.layouts')
@pagetitle(['title'=>'Untuk Diluluskan','links'=>['Pendaftaran']])@endpagetitle
@section('content')
<div class="container">
    <div class="row">
        @include('include.search',['url'=>url()->current(),'placeholder'=>'carian menggunakan nama, no. telefon atau no. mykad'])
    </div>
    @can('luluskan-permohonan-anggota')
    <form action="{{route('admin.membership.approve')}}" method="POST">
        @csrf

        <div class="row">
            <div class="col">
                <div class="form-group">
                    <button type="submit" class="btn btn-success text-uppercase font-weight-bold" id="approveAllButton">Luluskan <span id="checkboxCountText"></span></button>
                </div>
            </div>
        </div>
        @endcan
        <div class="row">
            <div class="col-lg-12">
                <div class="d-flex justify-content-between align-items-center">
                    <div class="d-flex justify-content-center align-items-center mb-3">
                        @if($members)
                        Paparan {{number_format($members->firstItem(),0,"",",")}} Hingga {{ number_format($members->lastItem(),0,"",",")}} Daripada {{number_format($members->total(),0,"",",")}} Dalam Senarai
                        @endif
                    </div>
                    <div class="d-flex justify-content-center align-items-center">
                        {{ $members->appends(request()->query())->links() }}
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-xl-12 mg-t-12">
                <div class="card">
                    <div class="card-body p-0">
                        <div class="table-responsive">
                            <table class="table">
                                <thead class="">
                                    <tr>
                                        @can('luluskan-permohonan-anggota')
                                        <th><input type="checkbox" id="checkAll"></th>
                                        @endcan
                                        <th class="text-center">@sortablelink('created_at','TARIKH DAFTAR')</th>
                                        <th>@sortablelink('name','NAMA')</th>
                                        <th class="text-center">NO. Anggota KoPPIM</th>
                                        <th class="text-center">NO. MYKAD</th>
                                        <th class="text-center">STATUS KEANGGOTAAN</th>
                                        <th class="text-center">JENIS PENDAFTARAN</th>
                                        <th class="text-center">LOG MASUK</th>
                                        <th class="text-center">JUMLAH BAYARAN</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse($members as $key=>$member)
                                    <tr>
                                        @can('luluskan-permohonan-anggota')
                                        <td><input type="checkbox" class="checkItem" name="member[{{$key}}]" value="{{$member->id}}"></td>
                                        @endcan
                                        <td class="text-center">{!! $member->created_at->format("d/m/Y \<\b\\r\> h:i:sa") !!}</td>
                                        <td><a href="{{route('admin.member.view',$member->id)}}">{{ $member->name }}</a></td>
                                        <td class="text-center">{{ $member->no_koppim ?? 'TIADA NO. ANGGOTA KoPPIM' }}</td>
                                        <td class="text-center"><a href="{{route('admin.member.view',$member->id)}}">{{ $member->ic }}</a></td>
                                        <td class="text-center">

                                            {!! Helper::status_text($member->status) !!}
                                        </td>
                                        <td class="text-center">
                                            {{strtoupper($member->registration_type)}}
                                        </td>
                                        <td class="text-center">
                                            @php
                                            $last_login = \Spatie\Activitylog\Models\Activity::where('log_name','user_auth')->where('subject_id',$member->id)->latest()->first();
                                            @endphp
                                            {!! ($last_login) ? $last_login->created_at->format('d/m/Y \<\b\\r\> h:i:sa') : 'TIADA' !!}
                                        </td>
                                        <td class="text-center">
                                            @php
                                            $total = App\Mongo\Payment::where('ic',$member->ic)->where('returncode','100')->sum('amount');
                                            @endphp
                                            RM{{number_format($total,2,".",",")}}
                                        </td>
                                    </tr>
                                    @empty
                                    <tr>
                                        <td colspan="10" class="text-center font-weight-bold text-uppercase">
                                            <h3>tiada rekod</h3>
                                        </td>
                                    </tr>
                                    @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div><!-- card-body -->
                </div><!-- card -->
            </div>
        </div>
        @can('luluskan-permohonan-anggota')
    </form>
    @endcan



</div><!-- container -->

@endsection

@section('js')
<script>
    $(document).ready(function() {

        $("#checkAll").prop('checked', false);
        $("#approveAllButton").prop('disabled',true);
        $('input.checkItem').prop('checked', false);

    });

    var checkboxes = $('input.checkItem');

    function initCheckboxesCount() {
        var countCheckedCheckboxes = checkboxes.filter(':checked').length;
        if (countCheckedCheckboxes) {
            $('#checkboxCountText').text(countCheckedCheckboxes + ' PERMOHONAN');
            $("#approveAllButton").prop('disabled',false);
        } else {
            $('#checkboxCountText').text('');
            $("#approveAllButton").prop('disabled',true);
        }
    }
    checkboxes.change(function() {
        initCheckboxesCount();
    });

    $("#checkAll").click(function() {

        $('input.checkItem').not(this).prop('checked', this.checked);
        initCheckboxesCount();
    });

</script>
<script>
    function search_data() {
        var search = $('#search').val();

        window.location.href = "{{route('admin.member.reviewed')}}?search=" + search + "";
    }

    $(document).on("keypress", ".input-group:has(input:input, span.input-group-btn:has(.btn)) input:input", function(e) {
        if (e.which == 13) {
            $(this).closest(".input-group").find(".input-group-append > .submit").click();
        }
    });

</script>

@endsection
