<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>KoPPIM - Pengurusan Pelanggan</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="KoPPIM" name="description" />
    <meta content="Coderthemes" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <!-- App favicon -->
    <link rel="shortcut icon" href={{asset('img/favicon-ori.ico')}}">

    <!-- App css -->
    <link href="{{asset('admin/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('admin/assets/css/icons.min.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('admin/css/app.min.css')}}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{asset('admin/css/style.css')}}">
</head>

<body class="authentication-bg">

    <div class="account-pages">
        <div class="container">
            <div class="row justify-content-center align-items-center h-screen">
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-body p-0">
                            <div class="row">
                                <div class="col p-5">
                                    <div class="mx-auto mb-5 text-center">
                                        <a href="#">
                                           <img src="{{asset('img/logo-koppim.png')}}" height="24"/>

                                        </a>
                                    </div>

                                    <h6 class="h5 mb-3 mt-4 text-center">SISTEM PENGURUSAN LANGGANAN SYER KOPPIM</h6>
                                    {{-- <p class="text-muted mt-1 mb-4 text-center">SELAMAT DATANG</p> --}}
                                    @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
                                    <form action="/auth/login/exe" method="post" enctype="multipart/form-data">
                                        {{ Csrf_field() }}
                                        <div class="form-group text-center">

                                            <div class="input-group input-group-merge">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">
                                                        <svg class="w-5 h-5" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M3 8l7.89 5.26a2 2 0 002.22 0L21 8M5 19h14a2 2 0 002-2V7a2 2 0 00-2-2H5a2 2 0 00-2 2v10a2 2 0 002 2z"></path>
                                                        </svg>
                                                    </span>
                                                </div>
                                                <input type="email" class="form-control @error('email') is-invalid @enderror" id="email" name="email" placeholder="Masukkan Emel Anda">
                                            </div>
                                        </div>

                                        <div class="form-group mt-4">


                                            <div class="input-group input-group-merge">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">
                                                        <svg class="w-5 h-5" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 15v2m-6 4h12a2 2 0 002-2v-6a2 2 0 00-2-2H6a2 2 0 00-2 2v6a2 2 0 002 2zm10-10V7a4 4 0 00-8 0v4h8z"></path>
                                                        </svg>
                                                    </span>
                                                </div>
                                                <input type="password" class="form-control  @error('password') is-invalid @enderror" id="password" name="password" placeholder="Masukkan Kata Laluan Anda" >
                                            </div>
                                        </div>



                                        <div class="form-group mb-0 text-center">
                                            <button class="btn btn-primary btn-block font-weight-bold text-uppercase" type="submit"> Log Masuk
                                            </button>
                                        </div>
                                    </form>

                                </div>
                                {{-- <div class="col-lg-6 d-none d-md-inline-block">
                                    <div class="auth-page-sidebar">
                                        <div class="overlay"></div>
                                        <div class="auth-user-testimonial">
                                            <p class="font-size-24 font-weight-bold text-white mb-1">Tentang KoPPIM</p>
                                            <p class="lead">Diasaskan oleh Persatuan Pengguna Islam Malaysia(PPIM). Matlamatnya adalah untuk menyatukan kuasa pengguna Islam Malaysia dalam membina teras kekuatan ekonomi.</p>
                                        </div>
                                    </div>
                                </div> --}}
                            </div>

                        </div> <!-- end card-body -->
                    </div>
                    <!-- end card -->

                    <div class="row mt-3">
                        <div class="col-12 text-center">
                            <p class="text-muted">Hak Cipta Terpelihara . 2020 . KoPPIM </p>
                        </div> <!-- end col -->
                    </div>
                    <!-- end row -->

                </div> <!-- end col -->
            </div>
            <!-- end row -->
        </div>
        <!-- end container -->
    </div>
    <!-- end page -->

    <!-- Vendor js -->
    <script src="{{asset('admin/js/vendor.min.js')}}"></script>

    <!-- App js -->
    <script src="{{asset('admin/js/app.min.js')}}"></script>

</body>
</html>
