@extends('admin.layouts')
@section('css')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/jquery-datetimepicker@2.5.21/jquery.datetimepicker.css">
<style>
    #myTable td {
        padding: 2px 1px !important;
    }

</style>
@endsection
@pagetitle(['title'=>'Daftarkan Anggota (Manual)','links'=>['Pendaftaran']])@endpagetitle

@section('content')

<div class="container">
    <form action="{{route('admin.member.store')}}" method="POST" enctype="multipart/form-data">
        @csrf
        {{-- Add User form --}}
        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-body">
                        <div class="card">
                            <div class="card-body">
                                @include('admin.users.partials.add-user-form')
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end card -->
            </div>
        </div>

        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-body">
                        <!-- transaksi langganan -->
                        @include('admin.users.partials.add-user.transaksi-langganan')
                    </div>
                </div>
            </div>
        </div>

        {{-- Save button --}}
        <div class="row">
            <div class="col ">
                <div class="card">
                    <div class="card-body">
                        <div class="form-group">
                            <div class="d-flex align-items-center justify-content-between">
                                <h5>Simpan semua perubahan?</h5>
                                <button type="submit" class="btn btn-lg btn-primary text-capitalize font-weight-bold">Ya, teruskan
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>


@endsection

@section('js')
<script src="https://cdn.jsdelivr.net/npm/jquery-datetimepicker@2.5.21/build/jquery.datetimepicker.full.min.js" integrity="sha256-JADorIjphY5ztERiDARizkSMD2NW4HpsDlAyageYtFc=" crossorigin="anonymous"></script>

<script type="text/javascript">
    var utilitiesinit = () => {
        $('.mask').inputmask();
        $('.mykad-mask').inputmask('999999999999');
        $('.mobile-mask').inputmask({
            mask: '6999999999999'
            , rightAlign: false
        });
        $('.phone-mask').inputmask('6999999999999');
        $('.poskod').inputmask('99999');

        jQuery('.paid_at,.registration_date').datetimepicker({
            datepicker: true
            , timepicker: true
            , format: 'Y-m-d H:i:s'
        });

        $('.select2').select2({
            theme: 'bootstrap4'
        });
    }
    // initialize script
    utilitiesinit();

</script>

<script>
    $('a[data-toggle="pill"]').on('shown.bs.tab', function(e) {
        localStorage.setItem('activeTab', $(e.target).attr('href'));
    });

</script>
<script>
    function generatePassword() {
        var length = 8
            , charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
            , retVal = "";
        for (var i = 0, n = charset.length; i < length; ++i) {
            retVal += charset.charAt(Math.floor(Math.random() * n));
        }
        $('#pass').val(retVal);

    }

</script>

@endsection
