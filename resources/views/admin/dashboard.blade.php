@extends('admin.layouts')

@section('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.css" integrity="sha512-SUJFImtiT87gVCOXl3aGC00zfDl6ggYAw5+oheJvRJ8KBXZrr/TMISSdVJ5bBarbQDRC2pR5Kto3xTR0kpZInA==" crossorigin="anonymous" />
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick-theme.css"/>
@endsection

@section('header')
<div class="container mt-3">
    <div class="row d-none d-print-block">
        <div class="col mt-5">
            <div class="d-flex">
                <h3 class="text-4xl">{{\Carbon\Carbon::now()->translatedFormat('d F Y H:m:s')}}</h3>
            </div>
        </div>
    </div>
    <div class="row d-print-none">
        <div class="col">
            <div class="d-flex justify-content-between align-items-center">
                <div>
                    <h3 class="text-5xl">Selamat Datang, {{Helper::getFirstNameOnly(auth()->user()->name)}}</h3>
                    <p class="text-gray-600">Berikut merupakan statistik dan laporan semasa KoPPIM.</p>
                </div>
                <div>
                    <ul class="navbar-nav flex-row ml-auto d-flex align-items-center list-unstyled topnav-menu float-right mb-0">
                        <li class="dropdown d-none d-lg-block">
                            <a class="dropdown-toggle mr-0 rounded-full p-2 text-gray-600 hover:text-gray-800 active:text-gray-800 focus:text-gray-800 align-items-center" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                                <svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M10.325 4.317c.426-1.756 2.924-1.756 3.35 0a1.724 1.724 0 002.573 1.066c1.543-.94 3.31.826 2.37 2.37a1.724 1.724 0 001.065 2.572c1.756.426 1.756 2.924 0 3.35a1.724 1.724 0 00-1.066 2.573c.94 1.543-.826 3.31-2.37 2.37a1.724 1.724 0 00-2.572 1.065c-.426 1.756-2.924 1.756-3.35 0a1.724 1.724 0 00-2.573-1.066c-1.543.94-3.31-.826-2.37-2.37a1.724 1.724 0 00-1.065-2.572c-1.756-.426-1.756-2.924 0-3.35a1.724 1.724 0 001.066-2.573c-.94-1.543.826-3.31 2.37-2.37.996.608 2.296.07 2.572-1.065z"></path>
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 12a3 3 0 11-6 0 3 3 0 016 0z"></path>
                                </svg>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <!-- item-->
                                <a href="javascript:void;" class="dropdown-item notify-item" id="printImage">
                                    Cetak halaman ini
                                </a>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div> <!-- container-fluid -->
@endsection
@section('content')
<div class="container mt-3 mb-5" id="overall">
    <div class="row">
        <div class="col">
            <h3 class="text-sm text-gray-600 font-medium uppercase letter tracking-wider">Statistik Pendaftaran dan Keanggotaan (Keseluruhan)</h3>
        </div>
    </div>

    {{-- App\Widgets\StatistikKeanggotaan --}}
    {{ Widget::run('StatistikKeanggotaan') }}

    @widget('GrafStatistik')

    <div class="row">
        <div class="col">
            <h3 class="text-sm text-gray-600 font-medium uppercase letter tracking-wider">Statistik Transaksi Bayaran</h3>
        </div>
        <div class="col">
            <div class="d-flex flex-row space-x-2 justify-content-end align-items-end mb-2">
                <button type="button" class="btn btn-info" id="keseluruhan">Keseluruhan</button>
                <select name="" id="cams_month" class="form-control w-1/3">
                    <option value="">Sila Pilih</option>
                    @for($i=1;$i<13;$i++) <option value="{{$i}}">{{Carbon\Carbon::createFromFormat('m',$i)->translatedFormat('F')}}</option>
                        @endfor
                </select>
                <select name="" id="cams_year" class="form-control w-1/3">
                    <option value="">Sila Pilih</option>
                    <?php for($i = 2020;$i <= date("Y");$i++){ ?>
                    <option value="{{ $i }}">{{ $i }}</option>
                    <?php } ?>
                </select>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col">
            @include('widgets.transaksi_bayaran_cams')
        </div>
        <div class="col">
            @include('widgets.transaksi_bayaran_senang_pay')
        </div>
        <div class="col">
            @include('widgets.transaksi_bayaran_raudhah_pay')
        </div>
        <div class="col">
            @include('widgets.transaksi_bayaran_ikoop')
        </div>
    </div>
    <div class="row">
        <div class="col">
            <h3 class="text-sm text-gray-600 font-medium uppercase letter tracking-wider">Statistik Prestasi Program Affiliate</h3>
        </div>
    </div>
    {{ Widget::run('StatistikAffiliate') }}
    <div class="row">
        <div class="col">
            {{  Widget::run('PencapaianAffiliate') }}
        </div>
    </div>
</div>
@endsection
@push('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.11.0/underscore-min.js" integrity="sha512-wBiNJt1JXeA/ra9F8K2jyO4Bnxr0dRPsy7JaMqSlxqTjUGHe1Z+Fm5HMjCWqkIYvp/oCbdJEivZ5pLvAtK0csQ==" crossorigin="anonymous"></script>
<script>
    $('#printImage').on('click', function() {
        window.print();
    })


    function cams_queries() {
        // default this year
        if ($('#cams_year').val() == "") {
            $('#cams_year').val(new Date().getFullYear())
        }

        return {
            year: $('#cams_year').val()
            , month: $('#cams_month').val()
        }
    }

    function selectorValue(data) {
        $('#jumlahKeseluruhanCams').html(data.jumlahKeseluruhanCams);
        $('#jumlahSyerCams').html(data.jumlahSyerCams);
        $('#jumlahFiCams').html(data.jumlahFiCams);
        $('#jumlahKeseluruhanSenangpay').html(data.jumlahKeseluruhanSenangpay);
        $('#jumlahSyerSenangpay').html(data.jumlahSyerSenangpay);
        $('#jumlahFiSenangpay').html(data.jumlahFiSenangpay);
        $('#jumlahKeseluruhanRaudhahpay').html(data.jumlahKeseluruhanRaudhahpay);
        $('#jumlahSyerRaudhahpay').html(data.jumlahSyerRaudhahpay);
        $('#jumlahFiRaudhahpay').html(data.jumlahFiRaudhahpay);
        $('#jumlahKeseluruhanIkoop').html(data.jumlahKeseluruhanIkoop);
        $('#jumlahSyerIkoop').html(data.jumlahSyerIkoop);
        $('#jumlahFiIkoop').html(data.jumlahFiIkoop);
    }

    function fetchTransaksi(data) {
        $.ajax({
            type: 'GET'
            , url: "{{ route('api.transaction.statistic') }}"
            , data: data
            , success: function(data) {
                $('.tarikh_tahun').html("(Keseluruhan)");
                selectorValue(data);
            }
        });
    }

    fetchTransaksi(cams_queries());

    $('#keseluruhan').click(function() {
        fetchTransaksi('');
    });

    $("#cams_month,#cams_year").on("change", function() {

        $.ajax({
            type: 'GET'
            , url: "{{ route('api.transaction.statistic') }}"
            , data: cams_queries()
            , success: function(data) {
                $('.tarikh_tahun').html($('#cams_month').val() + '/' + $('#cams_year').val());
                selectorValue(data);
            }
        });

    });

</script>
@endpush
