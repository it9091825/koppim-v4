@extends('admin.layouts')
@section('header')

    <div class="container">
        <div class="row page-title">
            <div class="col-md-12">
                <nav aria-label="breadcrumb" class="float-right mt-1">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item active" aria-current="page">Migrasi Ikoop</li>
                        <li class="breadcrumb-item" aria-current="page">Duplicate Users</li>
                    </ol>
                </nav>
                <h4 class="mb-1 mt-0 text-uppercase">Duplicate Users</h4>
            </div>
        </div>
    </div> <!-- container-fluid -->

@endsection
@section('content')
    <div class="container">


        <div class="row ">
            <div class="col-lg-12 col-xl-12">
                <div class="card">
                    <div class="card-body">

                        <div class="table-responsive">
                            <table class="table">
                                <thead class="thead-primary">
                                <tr>
                                    <th class="text-center">TARIKH DAFTAR</th>
                                    <th>NAMA</th>
                                    <th class="text-center">NO. MYKAD</th>
                                    <th class="text-center">STATUS KEANGGOTAAN</th>
                                    <th class="text-center">MASALAH</th>
                                    <th class="text-center">YURAN</th>
                                    <th class="text-center">SAHAM TERKUMPUL</th>

                                </tr>
                                </thead>
                                <tbody>

                                @forelse($users as $user)
                                    @php
                                        $realUser = \App\User::where('ic',$user->ic)->first();
                                    @endphp
                                    <tr>
                                        <td class="text-center">{!! $user->created_at->format("d/m/Y \<\b\\r\> h:i:sa") !!}</td>
                                        <td>
                                            @if($realUser)
                                                <a href="{{route('admin.member.view',$realUser->id)}}">{{ $user->name }}</a>
                                            @else
                                                {{ $user->name }}
                                            @endif
                                        </td>
                                        <td class="text-center">
                                            @if($realUser)
                                                <a href="{{route('admin.member.view',$realUser->id)}}">{{ $user->ic }}</a>
                                            @else
                                                {{ $user->ic }}
                                            @endif
                                        </td>
                                        <td class="text-center">
                                            {!! Helper::status_text($user->status) !!}
                                        </td>
                                        <td class="text-center">
                                            {{$user->ikoop_reason}}
                                        </td>
                                        <td class="text-center">
                                            {{number_format($user->first_time_fees,2,'.',',')}}
                                        </td>
                                        <td class="text-center">
                                            {{number_format($user->share_amount,2,'.',',')}}
                                        </td>

                                    </tr>
                                @empty
                                    @tablenoresult
                                    @endtablenoresult
                                @endforelse
                                </tbody>
                            </table>
                            <div class="d-flex justify-content-center align-items-center mb-3">
                                @if($users)
                                    Paparan {{number_format($users->firstItem(),0,"",",")}}
                                    Hingga {{ number_format($users->lastItem(),0,"",",")}}
                                    Daripada {{number_format($users->total(),0,"",",")}} Dalam Senarai
                                @endif
                            </div>
                            <div class="d-flex justify-content-center align-items-center">
                                {{ $users->appends(request()->query())->links() }}
                            </div>
                        </div>
                    </div><!-- card-body -->
                </div><!-- card -->
            </div>
        </div>

    </div><!-- container -->

@endsection

@section('js')


@endsection
