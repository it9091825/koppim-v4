@extends('admin.layouts')
@pagetitle(['title'=>'Untuk Semakan Lanjut','links' => ['Transaksi Bayaran']])@endpagetitle

@section('css')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/jquery-datetimepicker@2.5.21/jquery.datetimepicker.css">
@endsection
@section('content')

    <div class="container">
        <div class="row" style="">
            @include('include.search',['url'=>url()->current(),'placeholder'=>'carian menggunakan nama, no. telefon atau no. mykad'])
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="d-flex justify-content-between align-items-center">
                    <div class="d-flex justify-content-center align-items-center mb-3">
                        @if($payments)
                            Paparan {{number_format($payments->firstItem(),0,"",",")}}
                            Hingga {{ number_format($payments->lastItem(),0,"",",")}}
                            Daripada {{number_format($payments->total(),0,"",",")}} Dalam Senarai
                        @endif
                    </div>
                    <div class="d-flex justify-content-center align-items-center">
                        {{ $payments->appends(request()->query())->links() }}
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-xl-12 mg-t-12">
                <div class="card">
                    <div class="card-body px-2 py-0">
                        <div class="table-responsive">
                            <table class="table">
                                <thead class="thead-primary">
                                <tr>
                                    <th class="text-center">@sortablelink('created_at','TARIKH REKOD')</th>
                                    <th>@sortablelink('name','NAMA')</th>
                                    <th class="text-center">NO. Anggota KoPPIM</th>
                                    <th class="text-center">JUMLAH</th>
                                    <th class="text-center">TARIKH BAYARAN</th>
                                    <th class="text-center">STATUS</th>
                                    <th class="text-center">TINDAKAN</th>
                                </tr>
                                </thead>
                                @forelse($payments as $key=>$payment)
                                    <tbody>
                                    <tr>
                                        <td class="text-center">{!! $payment->created_at ?$payment->created_at->format("d/m/Y \<\b\\r\> h:i:sa") : '' !!}</td>
                                        <td class="">{{ strtoupper($payment->name) }}<br>{{ $payment->ic }}</td>
                                        <td class="text-center">{{Helper::get_user_no_anggota_from_mykad_no($payment->ic)}}</td>
                                        <td class="text-center">{{ $payment->amount != 0.00 ?'RM '.number_format($payment->amount,2,'.',',') : 'MASIH BELUM DIKEMASKINI'}}</td>
                                        {{-- <td class="text-center"><a target="_blank" href="{{ env('DO_SPACES_FULL_URL').$payment->payment_proof }}">LAMPIRAN</a>
                                        </td> --}}
                                        <td class="text-center">{!! $payment->payment_date ?$payment->payment_date->format("d/m/Y \<\b\\r\> h:i:sa") : '' !!}</td>
                                        <td class="text-center">{!! Helper::payment_status_text($payment->status) ?? '' !!}</td>
                                        <td class="text-center d-flex align-items-center space-x-1">
                                            <a target="_blank" href="{{route('admin.tx.print',['id'=>$payment->_id])}}" data-toggle="tooltip" data-placement="auto" data-original-title="Cetak"><svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M17 17h2a2 2 0 002-2v-4a2 2 0 00-2-2H5a2 2 0 00-2 2v4a2 2 0 002 2h2m2 4h6a2 2 0 002-2v-4a2 2 0 00-2-2H9a2 2 0 00-2 2v4a2 2 0 002 2zm8-12V5a2 2 0 00-2-2H9a2 2 0 00-2 2v4h10z"></path>
                                                </svg>
                                            </a>
                                            <a href="javascript:void;" class="row-toggle" data-toggle="modal" data-target="#sahkanModal" data-userid="{{$payment->id}}">
                                            <span data-toggle="tooltip" data-placement="auto" data-original-title="Hantar Untuk Pengesahan">
                                                <svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 9v3m0 0v3m0-3h3m-3 0H9m12 0a9 9 0 11-18 0 9 9 0 0118 0z"></path>
                                                </svg>
                                            </span>
                                            </a>
                                        </td>
                                    </tr>
                                @empty
                                    <tbody>
                                    <td colspan="10" class="text-center font-weight-bold text-uppercase">
                                        <h3>tiada rekod</h3>
                                    </td>
                                    </tbody>
                                @endforelse
                            </table>

                        </div>
                    </div><!-- card-body -->
                </div><!-- card -->
            </div>
        </div>
    </div><!-- container -->
    @include('admin.users.partials.approve-review-payment-modal')
@endsection

@section('js')
    <script src="https://cdn.jsdelivr.net/npm/jquery-datetimepicker@2.5.21/build/jquery.datetimepicker.full.min.js" integrity="sha256-JADorIjphY5ztERiDARizkSMD2NW4HpsDlAyageYtFc=" crossorigin="anonymous"></script>

    <script>
        var utilitiesinit = () => {
            $('.mask').inputmask();
            jQuery('#payment_date').datetimepicker({
                datepicker: true
                , timepicker: true
                , format: 'Y-m-d H:i:s'
            });
        }
        utilitiesinit();

        function search_data() {
            var search = $('#search').val();

            window.location.href = "{{route('admin.transaksi-bayaran.untuk-semakan-lanjut.index')}}?search=" + search + "";
        }

    </script>

@endsection
