@extends('admin.layouts')
@pagetitle(['title'=>'Telah Disahkan','links' => ['Transaksi Bayaran']])@endpagetitle
@section('css')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/jquery-datetimepicker@2.5.21/jquery.datetimepicker.css">
@endsection
@section('content')
<div class="container">
    <div class="row" style="">
        <div class="col-lg-6">
            <div class="input-group mb-3">
                <input type="text" id="search" class="form-control" value="{!! $search ?? ''!!}" placeholder="CARIAN MENGGUNAKAN NAMA, NO. TELEFON ATAU NO. MYKAD">
                <div class="input-group-append">
                    <button class="btn btn-primary font-weight-bold submit" onclick="search_data()">CARI</button>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="d-flex justify-content-between align-items-center">
                <div class="d-flex justify-content-center align-items-center mb-3">
                    @if($payments)
                    Paparan {{number_format($payments->firstItem(),0,"",",")}} Hingga {{ number_format($payments->lastItem(),0,"",",")}} Daripada {{number_format($payments->total(),0,"",",")}} Dalam Senarai
                    @endif
                </div>
                <div class="d-flex justify-content-center align-items-center">
                    {{ $payments->appends(request()->query())->links() }}
                </div>
            </div>
        </div>
        <div class="col-lg-12 col-xl-12 mg-t-12">
            <div class="card">
                <div class="card-body">
                    <ul class="nav nav-pills navtab-bg nav-justified" id="pills-tab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link {{Helper::active(url('admin/tx/verified?type=manual'))}} text-uppercase {{request()->query('type') == 'manual' ? 'active':''}}" id="pills-messages-tab" href="{{url('admin/tx/verified?type=manual')}}" aria-controls="pills-messages" aria-selected="false">
                                bayaran manual
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{Helper::active(url('admin/tx/verified?type=online'))}} text-uppercase {{request()->query('type') == 'online' ? 'active':''}}" id="pills-activity-tab" href="{{url('admin/tx/verified?type=online')}}" aria-controls="pills-activity" aria-selected="true">
                                bayaran online
                            </a>
                        </li>
                    </ul>
                    @if(request()->get('type') == 'online')
                    <div class="table-responsive">
                        <table class="table">
                            <thead class="thead-primary">
                                <tr>
                                    <th>NO. RUJUKAN</th>
                                    <th>@sortablelink('name','NAMA')</th>
                                    <th class="text-center">NO. Anggota KoPPIM</th>
                                    <th class="text-center">JUMLAH DIBAYAR</th>
                                    <th class="text-center">JUMLAH SYER</th>
                                    <th class="text-center">TARIKH BAYARAN</th>
                                    <th class="text-center">TINDAKAN</th>
                                </tr>
                            </thead>

                            @forelse($payments as $key => $org)
                            <tbody>
                                <tr>
                                    <td><a target="_blank" href="{{env('PAYMENT_URL','https://app.senangpay.my')}}/payment/receipt/{{ $org->ord_key }}">{{ $org->order_ref }}</a></td>
                                    <td><a target="_blank" href="{{url('admin/members?search='.$org->name)}}">{{ $org->name }} </a></td>
                                    <td class="text-center">{{Helper::get_user_no_anggota_from_mykad_no($org->ic)}}</td>
                                    <td class="text-center">
                                        RM {{ number_format($org->amount,2) }}
                                    </td>
                                    <td class="text-center">
                                        RM {{ number_format($org->share_amount,2) }}
                                    </td>
                                    <td class="text-center">{{$org->created_at->format('d/m/Y h:i:sa')}}</td>
                                    <td class="text-center">
                                        <a target="_blank" href="{{route('admin.tx.print',['id'=>$org->_id])}}" data-toggle="tooltip" data-placement="auto" data-original-title="Cetak"><svg class="w-6 h-6 text-info" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M17 17h2a2 2 0 002-2v-4a2 2 0 00-2-2H5a2 2 0 00-2 2v4a2 2 0 002 2h2m2 4h6a2 2 0 002-2v-4a2 2 0 00-2-2H9a2 2 0 00-2 2v4a2 2 0 002 2zm8-12V5a2 2 0 00-2-2H9a2 2 0 00-2 2v4h10z"></path>
                                    </svg></a>
                                        <a href="#" class="row-toggle" data-row-counter="{{$key}}" data-toggle="tooltip" data-placement="auto" data-original-title="Papar maklumat lanjut"><svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 9v3m0 0v3m0-3h3m-3 0H9m12 0a9 9 0 11-18 0 9 9 0 0118 0z"></path>
                                            </svg></a>
                                    </td>
                                </tr>
                            </tbody>
                            <tbody class="mb-3" id="row{{$key}}" style="display:none;">
                                <tr class="bg-gray-200">
                                    <td class="font-bold">NO. MYKAD</td>
                                    <td colspan="10"><a target="_blank" href="{{url('admin/members?search='.$org->ic)}}">{{ $org->ic }}</a></td>
                                </tr>
                                <tr class="bg-gray-200">
                                    <td class="font-bold">NO. TELEFON</td>
                                    <td colspan="10">{{ $org->phone }}</td>
                                </tr>
                                <tr class="bg-gray-200">
                                    <td class="font-bold">E-MEL</td>
                                    <td colspan="10">{{ $org->email }}</td>
                                </tr>
                                <tr class="bg-gray-200">
                                    <td class="font-bold">GATEWAY</td>
                                    <td colspan="10">{{ $org->gateway }}</td>
                                </tr>
                                <tr class="bg-gray-200">
                                    <td class="font-bold">CONFIRMATION CODE</td>
                                    <td colspan="10">{{ $org->ord_key }}</td>
                                </tr>
                            </tbody>

                            @empty
                            <tbody>
                                <td colspan="10" class="text-center font-weight-bold text-uppercase">
                                    <h3>tiada rekod</h3>
                                </td>
                            </tbody>

                            @endforelse


                        </table>


                    </div>
                    @endif
                    @if(request()->get('type') == 'manual')
                    <div class="table-responsive">
                        <table class="table">
                            <thead class="thead-primary">
                                <tr>
                                    <th>@sortablelink('name','NAMA')</th>
                                    <th class="text-center">NO. Anggota KoPPIM</th>
                                    <th class="text-center">JUMLAH</th>
                                    <th class="text-center">BUKTI PEMBAYARAN</th>
                                    <th class="text-center">TARIKH TRANSAKSI</th>
                                    {{-- <th>STATUS</th> --}}
                                    <th class="text-center">TINDAKAN</th>
                                </tr>
                            </thead>

                            @forelse($payments as $key=>$payment)
                            <tbody>
                                <tr>
                                    <td>{{ strtoupper($payment->name) }}</td>
                                    <td class="text-center">{{Helper::get_user_no_anggota_from_mykad_no($payment->ic)}}</td>
                                    <td class="text-center">{{ $payment->amount != 0.00 ? 'RM '.number_format($payment->amount,2,'.',',') : 'MASIH BELUM DIKEMASKINI'}}</td>
                                    <td class="text-center"><a target="_blank" href="{{ env('DO_SPACES_FULL_URL').$payment->payment_proof }}">LAMPIRAN</a>
                                    </td>
                                    <td class="text-center">{{ $payment->created_at->format('d/m/Y H:i') }}</td>
                                    {{-- <td>{{$payment->approval_status}}</td> --}}
                                    <td class="text-center">
                                        <a target="_blank" href="{{route('admin.tx.print',['id'=>$payment->_id])}}" data-toggle="tooltip" data-placement="auto" data-original-title="Cetak"><svg class="w-6 h-6 text-info" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M17 17h2a2 2 0 002-2v-4a2 2 0 00-2-2H5a2 2 0 00-2 2v4a2 2 0 002 2h2m2 4h6a2 2 0 002-2v-4a2 2 0 00-2-2H9a2 2 0 00-2 2v4a2 2 0 002 2zm8-12V5a2 2 0 00-2-2H9a2 2 0 00-2 2v4h10z"></path>
                                    </svg></a>
                                        @if($payment->approval_status == 'waiting-for-approval')
                                        @can('sahkan-bayaran-manual')
                                        <a href="javascript:void;" class="text-success" data-toggle="modal" data-target="#sahkanModal" data-userid="{{$payment->id}}" data-username="{{ strtoupper($payment->name) }}" data-useric="{{ $payment->ic }}" data-userphone="{{ $payment->mobile_no }}" data-useremail="{{ $payment->email }}" data-useramount="{{ $payment->amount != 0.00 ? $payment->amount : 'MASIH BELUM DIKEMASKINI'}}" data-usertransaction="{{$payment->payment_date}}"><span data-toggle="tooltip" data-placement="auto" data-original-title="Sahkan bayaran manual"><svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.payment/2000/svg">
                                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z"></path>
                                                </svg> </span></a>
                                        @endcan
                                        @can('batalkan-bayaran-manual')
                                        <a href="javascript:void" class="text-danger" data-toggle="modal" data-target="#batalkanModal" data-userid="{{$payment->id}}" data-username="{{ strtoupper($payment->name) }}" data-useric="{{ $payment->ic }}" data-userphone="{{ $payment->mobile_no }}" data-useremail="{{ $payment->email }}" data-useramount="{{ $payment->amount != 0.00 ? $payment->amount : 'MASIH BELUM DIKEMASKINI'}}" data-usertransaction="{{$payment->payment_date}}">
                                            <span data-toggle="tooltip" data-placement="auto" data-original-title="Batalkan bayaran manual">
                                                <svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z"></path>
                                                </svg>
                                            </span>
                                        </a>
                                        @endcan

                                        @endif
                                        <a href="javascript:void;" class="row-toggle" data-row-counter="{{$key}}" data-toggle="tooltip" data-placement="auto" data-original-title="Papar maklumat lanjut">
                                            <svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 9v3m0 0v3m0-3h3m-3 0H9m12 0a9 9 0 11-18 0 9 9 0 0118 0z"></path>
                                            </svg>
                                        </a>
                                    </td>
                                </tr>
                            <tbody class="mb-3" id="row{{$key}}" style="display:none;">
                                <tr class="bg-gray-200">
                                    <td class="font-bold">NO. MYKAD</td>
                                    <td colspan="10">{{ $payment->ic }}</td>
                                </tr>
                                <tr class="bg-gray-200">
                                    <td class="font-bold">NO. TELEFON</td>
                                    <td colspan="10">{{ $payment->mobile_no }}</td>
                                </tr>
                                <tr class="bg-gray-200">
                                    <td class="font-bold">E-MEL</td>
                                    <td colspan="10">{{ $payment->email }}</td>
                                </tr>
                            </tbody>
                            @empty
                            <tbody>
                                <td colspan="10" class="text-center font-weight-bold text-uppercase">
                                    <h3>tiada rekod</h3>
                                </td>
                            </tbody>
                            @endforelse
                        </table>
                    </div>
                    @endif
                </div><!-- card-body -->
            </div><!-- card -->
        </div>
    </div>
</div><!-- container -->


@endsection

@section('js')
<script>
    $(".row-toggle").click(function(e) {
        e.preventDefault();
        var rowid = $(this).attr("data-row-counter");
        $('#row' + rowid).toggle();
    });

    function search_data() {
        var search = $('#search').val();

        @if(request()->query('type') == 'online')
        window.location.href = "{{route('admin.transaction.verified.index')}}?type=online&search=" + search;
        @elseif(request()->query('type') == 'manual')
        window.location.href = "{{route('admin.transaction.verified.index')}}?type=manual&search=" + search;
        @endif

    }

    $(document).on("keypress", ".input-group:has(input:input, span.input-group-btn:has(.btn)) input:input", function(e) {
        if (e.which == 13) {
            $(this).closest(".input-group").find(".input-group-append > .submit").click();
        }
    });

</script>


@endsection
