@extends('admin.layouts')
@section('css')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/jquery-datetimepicker@2.5.21/jquery.datetimepicker.css">
@endsection
@section('header')

<div class="container">
    <div class="row page-title">
        <div class="col-md-12">
            <nav aria-label="breadcrumb" class="float-right mt-1">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item active" aria-current="page">Keanggotaan</li>
                    <li class="breadcrumb-item active" aria-current="page">Ahli Anggota</li>
                </ol>
            </nav>
            <h4 class="mb-1 mt-0">{{ $user->name }} {!! Helper::status_text($user->status) !!}</h4>
        </div>
    </div>
</div> <!-- container -->

@endsection

@section('content')
<?php
    if ($user->status == 0 || $user->status == 99) {
        $disable = '';
        $open = true;
    } else {
        $disable = 'disabled="disabled"';
        $open = false;
    }
?>
<div class="container">
    <!--@php
    $first_time_fee = \App\Mongo\Payment::where('ic',$user->ic)->where('returncode',
    '100')->where('first_time_fees','>=',30)->first();
    $first_time_share = \App\Mongo\Payment::where('ic',$user->ic)->where('returncode','100')->where('first_time_share','>=',100)->first();
    @endphp -->

    <div class="row mb-3">
        <div class="col">
        <a href="{{Session::get('admin_member_back') ? Session::get('admin_member_back') : url('/admin/members')}}"><span class="font-weight-bold d-flex align-items-center">
            <svg class="w-4 h-4" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M10 19l-7-7m0 0l7-7m-7 7h18"></path></svg>
        Kembali
    </span>
        </a>
        </div>

    </div>
    {{-- Sahkan Maklumat Anggota --}}
    @can('sahkan-maklumat-anggota')
    @if($user->status == 2 && $first_time_fee && $first_time_share)
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex justify-content-between align-items-center">
                        <h4>Anda Pasti Ingin Mengesahkan Permohonan Ini?</h4>
                        <form action="{{route('admin.membership.approval.send')}}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="user_id" value="{{$user->id}}">
                            <button type="submit" class="btn btn-primary btn-lg font-weight-bold">
                                Ya, Teruskan
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif
    @endcan
    {{-- Luluskan Anggota --}}
    @can('luluskan-permohonan-anggota')
    @if($user->status == 3)
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex justify-content-between align-items-center">
                        <h4>Anda Pasti Ingin Meluluskan Permohonan Ini?</h4>
                        <form action="{{route('admin.membership.approve')}}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="member[]" value="{{$user->id}}">
                            <button type="submit" class="btn btn-primary btn-lg font-weight-bold">
                                Ya, Teruskan
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif
    @endcan
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-body">
                    @include('admin.users.partials.edit-user-tab')
                    <div class="tab-content" id="pills-tabContent">
                        <!-- account -->
                        <div class="tab-pane fade show active" id="pills-activity" role="tabpanel" aria-labelledby="pills-activity-tab">
                            <div class="card">
                                <div class="card-body">
                                    @include('admin.users.partials.edit-user-form')
                                </div><!-- card-body -->
                            </div><!-- card -->
                        </div>
                        <!-- transaksi -->
                        <div class="tab-pane" id="pills-transaksi" role="tabpanel" aria-labelledby="pills-transaksi-tab">
                            @include('admin.users.partials.edit-user.transaksi-langganan')
                        </div>
                        <!-- log pembayaran -->
                        <div class="tab-pane" id="pills-log-pembayaran" role="tabpanel" aria-labelledby="pills-log-pembayaran-tab">
                            @include('admin.users.partials.edit-user.log-bayaran')
                        </div>
                        <!-- log sms -->
                        <div class="tab-pane" id="pills-log-sms" role="tabpanel" aria-labelledby="pills-log-sms-tab">
                            @include('admin.users.partials.edit-user.log-sms')
                        </div>
                        <!-- log status anggota -->
                        <div class="tab-pane" id="pills-log-status-anggota" role="tabpanel" aria-labelledby="pills-log-status-anggota-tab">
                            @include('admin.users.partials.edit-user.log-status-anggota')
                        </div>
                        <div class="tab-pane" id="pills-log-surat" role="tabpanel" aria-labelledby="pills-log-surat-tab">
                            @include('admin.users.partials.edit-user.log-surat')
                        </div>
                        <div class="tab-pane" id="pills-log-dividen" role="tabpanel" aria-labelledby="pills-log-dividen-tab">
                            @include('admin.users.partials.edit-user.log-dividen')
                        </div>
                    </div>
                </div>
            </div>
            <!-- end card -->
        </div>
    </div>
    @can('arkib-anggota')
        @if(App\Mongo\Payment::where('ic',$user->ic)->where('returncode','100')->sum('amount') < 0.01)
        <div class="row">
            <div class="col">
                <div class="card bg-danger">
                <div class="card-body">
                    <div class="row">
                    <div class="col">
                        <div class="d-flex align-items-center space-x-3">
                        <svg class="w-8 h-8 text-red-800" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 9v2m0 4h.01m-6.938 4h13.856c1.54 0 2.502-1.667 1.732-3L13.732 4c-.77-1.333-2.694-1.333-3.464 0L3.34 16c-.77 1.333.192 3 1.732 3z"></path></svg><h4 class="text-white">Padam rekod pelanggan ini?</h4></div>
                    </div>
                    <div class="col text-right">
                        <form enctype="multipart/form-data" method="post" action="{{route('admin.member.archive',$user)}}">
                            @csrf
                        <button class="btn btn-dark btn-lg font-bold">Ya, Teruskan</button>
                        </form>
                    </div>
                </div>
                </div>
            </div>
            </div>
        </div>
        @endif
    @endcan
</div>

@endsection

@section('js')
<script src="https://cdn.jsdelivr.net/npm/jquery-datetimepicker@2.5.21/build/jquery.datetimepicker.full.min.js" integrity="sha256-JADorIjphY5ztERiDARizkSMD2NW4HpsDlAyageYtFc=" crossorigin="anonymous"></script>
<script type="text/javascript">
    var utilitiesinit = () => {
        $('.mask').inputmask();
        $('#mykad').inputmask("999999999999",{ "placeholder": "" });
        $('#mobile_no').inputmask("99999999999",{ "placeholder": "" });
        $('#home_no').inputmask("99999999999",{ "placeholder": "" });

        jQuery('.paid_at, .registration_date').datetimepicker({
            datepicker: true
            , timepicker: true
            , format: 'Y-m-d H:i:s'
        });
        $('.select2').select2({
            theme: 'bootstrap4'
        });
    }
    // initialize script
    utilitiesinit();

</script>

<script>
    $('#state').val('{{ $user->state }}').change();
    $('#org_status').val('{{ $user->status }}').change();
    $('#bank_name').val('{{ $user->bank_name }}').change();
    $('#org_parlimen').val('{{$user->parliament}}').change();
    $('#blacklist').val('{{$user->blacklist}}').change();
    $('#blacklist_dividend').val('{{$user->blacklist_dividend}}').change();

    @if($user->dob !== NULL)

    $("#org_dob_day").val("{{$user->dob->format('d')}}").change();
    $("#org_dob_month").val("{{$user->dob->format('n')}}").change();
    $("#org_dob_year").val("{{$user->dob->format('Y')}}").change();
    @endif

    function generatePassword() {
        var length = 8
            , charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
            , retVal = "";
        for (var i = 0, n = charset.length; i < length; ++i) {
            retVal += charset.charAt(Math.floor(Math.random() * n));
        }
        $('#pass').val(retVal);

    }

</script>
<script>
    $('a[data-toggle="pill"]').on('shown.bs.tab', function(e) {
        localStorage.setItem('activeTab', $(e.target).attr('href'));
    });

    var activeTab = localStorage.getItem('activeTab');
    if (activeTab) {
        $('.nav-item a[href="' + activeTab + '"]').tab('show');
    }


	function open_withdraw(status,bank_name,acc,name,amount,rujukan,lampiran) {
        $('#withdraw_status').modal('show');
		var table = "";

		if(status === "SEDANG-DISEMAK")
		{
			table += `<table class='table'>
            <tr>
   					<td>Status</td>
   					<td>:</td>
   					<td><b>`+status+`</b></td>
            </tr>
            <tr>
   					<td>Bank</td>
   					<td>:</td>
   					<td><b>`+bank_name+`</b></td>
            </tr>

            <tr>
   					<td>Akaun No.</td>
   					<td>:</td>
   					<td><b>`+acc+`</b></td>
            </tr>
            <tr>
   					<td>Nama Akaun</td>
   					<td>:</td>
   					<td><b>`+name+`</b></td>
            </tr>
            <tr>
   					<td>Jumlah</td>
   					<td>:</td>
   					<td><b>RM `+amount+`</b></td>
            </tr>
            </table>
            `;
		}
		else if(status === "TELAH-DIBAYAR")
		{

			table += `<table class='table'>
            <tr>
   					<td>Status</td>
   					<td>:</td>
   					<td><b>`+status+`</b></td>
            </tr>
            <tr>
   					<td>Bank</td>
   					<td>:</td>
   					<td><b>`+bank_name+`</b></td>
            </tr>

            <tr>
   					<td>Akaun No.</td>
   					<td>:</td>
   					<td><b>`+acc+`</b></td>
            </tr>
            <tr>
   					<td>Nama Akaun</td>
   					<td>:</td>
   					<td><b>`+name+`</b></td>
            </tr>
            <tr>
   					<td>Jumlah</td>
   					<td>:</td>
   					<td><b>RM `+amount+`</b></td>
            </tr>
            <tr>
   					<td>No. Rujukan</td>
   					<td>:</td>
   					<td><b>`+rujukan+`</b></td>
            </tr>
            <tr>
   					<td>Bukti Pembayaran</td>
   					<td>:</td>
   					<td><a target='_blank' href='{{ env('DO_SPACES_FULL_URL') }}`+lampiran+`'>Lampiran</a></td>
            </tr>
            </table>
            `;

		}


          $('#w_modal_body').html(table);

    }

 function open_dividen(json_encode,to) {
        $('#user_letter_modal').modal('show');
        var obj = JSON.parse(json_encode);
        var table = "";
        for (var i = 0; i < obj.length; i++) {


            table += `<table class='table'>
            <tr>
   					<td>Tarikh Pelaburan</td>
   					<td>:</td>
   					<td><b>`+obj[i].payment_date.date.substring(0, 10)+`</b></td>
            </tr>
            <tr>
   					<td>Sehingga</td>
   					<td>:</td>
   					<td><b>`+to+`</b></td>
            </tr>

            <tr>
   					<td>Jumlah Bulan</td>
   					<td>:</td>
   					<td><b>`+obj[i].month+`</b></td>
            </tr>
            <tr>
   					<td>Jumlah Pelaburan</td>
   					<td>:</td>
   					<td><b>RM `+obj[i].amount.toFixed(2)+`</b></td>
            </tr>
            <tr>
   					<td>Dividen / Bulan</td>
   					<td>:</td>
   					<td><b>RM `+obj[i].dividen_per_month.toFixed(2)+`</b></td>
            </tr>
            <tr>
   					<td>Dividen</td>
   					<td>:</td>
   					<td><b style='color:green;'>RM `+obj[i].dividen.toFixed(2)+`</b></td>
            </tr>
            </table><hr />
            `;
        }


        $('#modal_body').html(table);

    }

</script>
@endsection
