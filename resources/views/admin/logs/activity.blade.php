@extends('admin.layouts')

@pagetitle(['title'=>'Log Aktivity','links'=>['Keanggotaan']])@endpagetitle

@section('content')
    <div class="container">
{{--        <div class="row" style="">--}}
{{--            <div class="col-lg-6">--}}
{{--                <div class="input-group mb-3">--}}
{{--                    <input type="text" id="search" class="form-control" value="{{ $search }}"--}}
{{--                           placeholder="CARI DENGAN KATA NAMA ATAU NO. MYKAD">--}}
{{--                    <div class="input-group-append">--}}
{{--                        <button class="btn btn-primary font-weight-bold submit" onclick="search_data()">CARI</button>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}

        <div class="row ">
            <div class="col-lg-12">
                <div class="d-flex justify-content-between align-items-center">
                    <div class="d-flex justify-content-center align-items-center mb-3">
                        @if($activities)
                            Paparan {{number_format($activities->firstItem(),0,"",",")}} Hingga {{ number_format($activities->lastItem(),0,"",",")}} Daripada {{number_format($activities->total(),0,"",",")}} Dalam Senarai
                        @endif
                    </div>
                    <div class="d-flex justify-content-center align-items-center">
                        {{ $activities->appends(request()->query())->links() }}
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-xl-12">
                <div class="card">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead class="thead-primary">
                                <tr>
                                    <th class="text-center">Bil</th>
                                    <th>Aktiviti</th>
                                    <th class="text-center">Tarikh</th>
                                    <th class="text-center">Oleh</th>
                                    <th class="text-center">Jenis Log</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php
                                    $bil = $activities->firstItem();
                                @endphp
                                @forelse($activities as $activity)
                                    <tr>
                                        <td class="text-center">{{$bil++}}</td>
                                        <td class="text-center">{{$activity->description}}</td>
                                        <td class="text-center">{!! $activity->created_at->format("d/m/Y \<\b\\r\> h:i:sa") !!}</td>
                                        <td class="text-center">{{$activity->causer->name}}</td>
                                        <td class="text-center">{{$activity->log_name}}</td>
                                    </tr>
                                @empty
                                    @tablenoresult
                                    @endtablenoresult
                                @endforelse
                                </tbody>
                            </table>

                        </div>
                    </div><!-- card-body -->
                </div><!-- card -->
            </div>
        </div>

    </div><!-- container -->

@endsection

@section('js')

    <script>
        {{--function search_data() {--}}
        {{--    var search = $('#search').val();--}}

        {{--    window.location.href = "{{route('admin.member.list')}}?search=" + search + "";--}}
        {{--}--}}

        {{--$(document).on("keypress", ".input-group:has(input:input, span.input-group-btn:has(.btn)) input:input", function(e) {--}}
        {{--    if (e.which == 13) {--}}
        {{--        $(this).closest(".input-group").find(".input-group-append > .submit").click();--}}
        {{--    }--}}
        {{--});--}}

    </script>
@endsection
