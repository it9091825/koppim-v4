@extends('admin.layouts')
@section('css')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/jquery-datetimepicker@2.5.21/jquery.datetimepicker.css">
@endsection
@pagetitle(['title'=>'Set Mod Penyelenggaraan','links' => ['Tetapan Admin']])@endpagetitle
@section('content')
<div class="container">
<div class="row">
    <div class="col">
        <div class="card">
            <div class="card-header bg-white font-weight-bold">
                Status Mod Penyelenggaraan
            </div>
            <div class="card-body">
                @if(Storage::disk('framework')->exists('down'))
                    <h2><span class="badge badge-success">
                        Aktif
                    </span></h2>
                    <span><form action="/admin/maintenance/destroy" method="POST">
                        @csrf
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Hentikan Mod Penyelenggaraan</button>
                        </div>
                    </form>
                    </span>
                @else
                <h2><span class="badge badge-warning">
                    Tidak Aktif
                </span></h2>
                @endif
            </div>
        </div>
        <div class="card">
            <div class="card-header bg-white font-weight-bold">
                Sila masukkan tempoh waktu penyelenggaraan
            </div>
            <div class="card-body">
                <form enctype="multipart/form-data" method="post" action="/admin/maintenance/store">
                    @csrf
                    <div class="form-group">
                        <div class="row">
                            <div class="col">
                                <label for="" class="d-block">Tarikh Mula Penyelenggaraan</label>
                            <input type="text" class="form-control" name="from" placeholder="" id="from" value="{{old('from')}}">
                                @error('from')
                            <p class="text-danger">{{$message}}</p>
                                @enderror
                            </div>
                            <div class="col">
                                <label for="" class="d-block">Tarikh Akhir Penyelenggaraan</label>
                            <input type="text" class="form-control" name="to" placeholder="" id="to" value="{{old('to')}}">
                            @error('to')
                            <p class="text-danger">{{$message}}</p>
                                @enderror
                            </div>
                        </div>

                    </div>
                    <div class="form-group">
                        <p class="text-danger">Peringatan! Pelanggan KoPPIM tidak akan dapat mengakses laman web ini
                            ketika
                            dalam
                            Mod Penyelenggaraan. Sila pastikan anda sudah memaklumkan kepada para pelanggan sebelum
                            mulakan Mod Penyelenggaraan.</p>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary font-weight-bold">Mulakan Mod
                            Penyelenggaraan</button>
                    </div>
                </form>
            </div>
        </div>

    </div>
</div>
</div>
@endsection

@section('js')
<script src="https://cdn.jsdelivr.net/npm/jquery-datetimepicker@2.5.21/build/jquery.datetimepicker.full.min.js"
    integrity="sha256-JADorIjphY5ztERiDARizkSMD2NW4HpsDlAyageYtFc=" crossorigin="anonymous"></script>

<script>
    jQuery('#from, #to').datetimepicker({
        format:'d/m/Y H:i'
    });
</script>
@endsection