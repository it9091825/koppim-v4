@extends('admin.layouts')
@pagetitle(['title'=>'Untuk Disahkan','links' => ['Transaksi Bayaran']])@endpagetitle
@section('css')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/jquery-datetimepicker@2.5.21/jquery.datetimepicker.css">
@endsection
@section('content')

<div class="container">
    <div class="row" style="">
        @include('include.search',['url'=>url()->current(),'placeholder'=>'carian menggunakan nama, no. telefon atau no. mykad'])
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="d-flex justify-content-between align-items-center">
                <div class="d-flex justify-content-center align-items-center mb-3">
                    @if($payments)
                    Paparan {{number_format($payments->firstItem(),0,"",",")}}
                    Hingga {{ number_format($payments->lastItem(),0,"",",")}}
                    Daripada {{number_format($payments->total(),0,"",",")}} Dalam Senarai
                    @endif
                </div>
                <div class="d-flex justify-content-center align-items-center">
                    {{ $payments->appends(request()->query())->links() }}
                </div>
            </div>
            <div class="col-lg-12 col-xl-12 mg-t-12">
                <div class="card">
                    <div class="card-body px-2 py-0">
                        <div class="table-responsive">
                            <table class="table">
                                <thead class="thead-primary">
                                <tr>
                                    <th>@sortablelink('name','NAMA')</th>
                                    <th class="text-center">NO. Anggota KoPPIM</th>
                                    <th class="text-center">JUMLAH</th>
                                    {{-- <th class="text-center">BUKTI PEMBAYARAN</th> --}}
                                    <th class="text-center">TARIKH BAYARAN</th>
                                    <th class="text-center">STATUS</th>
                                    <th class="text-center">TINDAKAN</th>
                                </tr>
                                </thead>
                                @forelse($payments as $key=>$payment)
                                    <tbody>
                                    <tr>
                                        <td class="col-3">{{ strtoupper($payment->name) }}</td>
                                        <td class="text-center">{{Helper::get_user_no_anggota_from_mykad_no($payment->ic)}}</td>
                                        <td class="text-center">{{ $payment->amount != 0.00 ? 'RM '.number_format($payment->amount,2,'.',',') : 'MASIH BELUM DIKEMASKINI'}}</td>
                                        <td class="text-center">{{ ($payment->payment_date != null) ? $payment->payment_date->format('d-M-Y H:i') : 'Tarikh Tidak Dimasukkan' }}</td>
                                        <td class="text-center">{!! Helper::payment_status_text($payment->status) ?? '' !!}</td>
                                        <td class="text-center d-flex align-items-center space-x-1">
                                            <a href="javascript:void;" class="row-toggle" data-toggle="modal"
                                               data-target="#sahkanModal"
                                               data-paymentproof="{{ env('DO_SPACES_FULL_URL').$payment->payment_proof }}"
                                               data-userid="{{$payment->id}}"
                                               data-username="{{ strtoupper($payment->name) }}"
                                               data-useric="{{ $payment->ic }}" data-userphone="{{ $payment->phone }}"
                                               data-useremail="{{ $payment->email }}"
                                               data-useramount="{{ $payment->amount != 0.00 ? $payment->amount : 'MASIH BELUM DIKEMASKINI'}}"
                                               data-usertransaction="{{$payment->payment_date}}"
                                               data-userfee="{{$payment->first_time_fees}}"
                                               data-usershare="{{$payment->first_time_share}}"
                                               data-useradd="{{$payment->additional_share}}"
                                               data-usernote="{{$payment->note}}">
                                            <span data-toggle="tooltip" data-placement="auto"
                                                  data-original-title="Tindakan Pengesahan">
                                                <svg class="w-6 h-6" fill="none" stroke="currentColor"
                                                     viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                                      d="M12 9v3m0 0v3m0-3h3m-3 0H9m12 0a9 9 0 11-18 0 9 9 0 0118 0z"></path>
                                            </svg>
                                            </span>
                                            </a>
                                        </td>
                                    </tr>

                                @empty
                                    <tbody>
                                    <td colspan="10" class="text-center font-weight-bold text-uppercase">
                                        <h3>tiada rekod</h3>
                                    </td>
                                    </tbody>
                                @endforelse
                            </table>
                        </div>
                    </div><!-- card-body -->
                </div><!-- card -->
            </div>
        </div>
    </div><!-- container -->

    <!-- sahkan modal content -->
    <div id="sahkanModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="sahkanModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="sahkanModalLabel">PENGESAHAN BAYARAN MANUAL</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <meta name="csrf-token" content="{{ csrf_token() }}">
                <form method="POST" action="{{route('admin.tx.approval.luluskan')}}">
                    @csrf

                    <div class="modal-body">
                        <div class="row">
                            <div class="col">
                                <input type="hidden" class="form-control" id="proof_id" name="proof_id">
                                <div class="form-group mb-1">
                                    <label for="name" class="col-form-label">Nama:</label>
                                    <input type="text" class="form-control mb-1" id="name" name="name" disabled>
                                </div>
                                <div class="form-group mb-1">
                                    <label for="ic" class="col-form-label">No. Mykad:</label>
                                    <input type="text" class="form-control mb-1" id="ic" name="ic" disabled>
                                </div>
                                <div class="form-group mb-1">
                                    <label for="mobile_no" class="col-form-label">No. Telefon:</label>
                                    <input type="text" class="form-control mb-1" id="mobile_no" name="mobile_no"
                                           disabled>
                                </div>
                                <div class="form-group mb-1">
                                    <label for="email" class="col-form-label">Emel:</label>
                                    <input type="text" class="form-control mb-1" id="email" name="email" disabled>
                                </div>
                                <div class="form-group mb-1">
                                    <label for="payment_date" class="col-form-label">TARIKH TRANSAKSI:</label>
                                    <input type="text" class="form-control mb-1" id="payment_date" name="payment_date"
                                           disabled>
                                </div>
                                <div class="form-group mb-1">
                                    <label for="amount" class="col-form-label">Jumlah Keseluruhan:</label>
                                    <input type="text" class="form-control mask mb-1" id="amount" name="amount"
                                           disabled>
                                </div>
                                <div class="form-group mb-1">
                                    <label for="first_time_fees" class="col-form-label">Fi Pendaftaran Anggota
                                        Baharu:</label>
                                    <input type="text" class="form-control mask mb-1" id="first_time_fees"
                                           name="first_time_fees" disabled>

                                </div>
                                <div class="form-group mb-1">
                                    <label for="first_time_share" class="col-form-label">Langganan Modal Syer:</label>
                                    <input type="text" class="form-control mask mb-1" id="first_time_share"
                                           name="first_time_share" disabled>

                                </div>
                                <div class="form-group mb-1">
                                    <label for="additional_share" class="col-form-label">Langganan Tambahan:</label>
                                    <input type="text" class="form-control mask mb-1" id="additional_share"
                                           name="additional_share" disabled>

                                </div>
                                <div class="form-group mb-1">
                                    <label for="note" class="col-form-label">Nota Admin:</label>
                                    <textarea name="note" id="note" cols="10" rows="3"
                                              class="form-control mb-1"></textarea>
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group mb-1">
                                    <div id="originalProof"></div>
                                    <div id="proof"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                            <button type="button" class="btn btn-info" id="simpanData">
                                Hantar Semula Untuk Disemak
                            </button>
                            @can('luluskan-bayaran-manual')
                                <button type="submit" name="sah_button" value="sah" class="btn btn-primary">Sah</button>
                            @endcan
                            @can('gagalkan-bayaran-manual')
                                <button type="submit" name="sah_button" value="batal" class="btn btn-danger">Batal
                                </button>
                            @endcan
                        <button type="button" class="btn btn-light" data-dismiss="modal">Tutup</button>
                    </div>
                </form>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

@endsection

@section('js')
    <script src="https://cdn.jsdelivr.net/npm/jquery-datetimepicker@2.5.21/build/jquery.datetimepicker.full.min.js"
            integrity="sha256-JADorIjphY5ztERiDARizkSMD2NW4HpsDlAyageYtFc=" crossorigin="anonymous"></script>

    <script>
        var utilitiesinit = () => {
            $('.mask').inputmask();
            jQuery('#payment_date').datetimepicker({
                datepicker: true
                , timepicker: true
                , format: 'Y-m-d H:i:s'
            });
        }
        utilitiesinit();

        $('#sahkanModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget)
            var userId = button.data('userid')
            var name = button.data('username')
            var ic = button.data('useric')
            var mobile_no = button.data('userphone')
            var email = button.data('useremail')
            var amount = button.data('useramount')
            var transaction = button.data('usertransaction')
            var proof = button.data('paymentproof')
            var first_time_fees = button.data('userfee')
            var first_time_share = button.data('usershare')
            var additional_share = button.data('useradd')
            var note = button.data('usernote')

            var modal = $(this)
            modal.find('#proof_id').val(userId)
            modal.find('#name').val(name)
            modal.find('#ic').val(ic)
            modal.find('#mobile_no').val(mobile_no)
            modal.find('#email').val(email)
            modal.find('#amount').val(amount)
            modal.find('#first_time_fees').val(first_time_fees)
            modal.find('#first_time_share').val(first_time_share)
            modal.find('#additional_share').val(additional_share)
            modal.find('#note').val(note)
            modal.find('#payment_date').val(transaction)
            modal.find('#proof').html(`<img src="${proof}" class="img-fluid img-thumbnail">`);
            modal.find('#originalProof').html(`<a href="${proof}" target="_blank">Lihat Bukti Pembayaran dalam saiz asal</a>`)
        })

        function search_data() {
            var search = $('#search').val();

            window.location.href = "{{route('admin.tx.approval.index')}}?search=" + search + "";
        }

        $("#simpanData").click(function () {
            //alert($('#sahkanModal').find('#name').val());
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.post("{{route('admin.tx.approval.return.back')}}",
                {
                    proof_id: $('#sahkanModal').find('#proof_id').val(),
                    note: $('#sahkanModal').find('#note').val(),
                },
                function (data, status) {
                    alert(data.data);
                    if (status == 'success') {
                        location.reload();
                    }
                });
        });
    </script>

@endsection
