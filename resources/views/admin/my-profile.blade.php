@extends('admin.layouts')
@section('content')
<div class="container">
    <div class="row mt-5 justify-content-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header bg-white">
                    <h5 class="card-title">Profil Saya</h5>
                </div>
                <div class="card-body">
                    <form action="{{route('administrators.update',['id' => Auth::user()->id])}}" method="POST" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <input type="hidden" name="role_id" value="{{Auth::user()->roles->first()->id}}">
                        <div class="form-group">
                            <label for="" class="form-label">Role</label>
                            <p>
                                @foreach(collect(Auth::user()->roles)->pluck('name') as $role)
                                <span class="badge badge-soft-primary">{{$role}}</span>
                                @endforeach
                            </p>
                        </div>
                        <div class="form-group">
                            <label for="" class="form-label">Nama</label>
                            <input type="text" name="name" id="" class="form-control" value="{{Auth::user()->name ?? ''}}">

                        </div>
                        <div class="form-group">
                            <label for="" class="form-label">E-mail</label>
                            <input type="text" name="email" id="" class="form-control" value="{{Auth::user()->email ?? ''}}" readonly="true">

                            <small class="text-primary">E-mel anda tidak boleh diubah untuk tujuan keselamatan dan integriti data</small>

                        </div>
                        <div class="form-group">
                            <label for="" class="form-label">Kata Laluan</label>
                            <input type="password" name="password" id="" class="form-control" autocomplete="false">
                            <small class="text-primary">Biarkan kosong jika tidak mahu kemaskini kata laluan</small>
                        </div>
                        <div class="form-group">
                            <label for="" class="form-label">Sahkan Kata Laluan</label>
                            <input type="password" name="password_confirmation" id="" class="form-control">
                            <small class="text-primary">Biarkan kosong jika tidak mahu kemaskini kata laluan</small>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-block font-weight-bold">Kemaskini</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection



@section('js')

@endsection
