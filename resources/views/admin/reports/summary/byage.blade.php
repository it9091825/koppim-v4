@extends('admin.layouts')
@section('css')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
@endsection
@pagetitle(['title'=>'Ringkasan Keseluruhan Anggota Mengikut Skala Umur','links'=>['Laporan','Ringkasan Laporan']])@endpagetitle
@section('content')
<div class="container space-y-2">
    <div class="row">
        <div class="col"><a href="/admin/reports/summary" class="font-weight-bold">Kembali</a></div>
    </div>
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-body px-3">
                    <table class="table" id="table">
                        <thead>
                            <tr>
                                <th>Julat Umur (Tahun)</th>
                                <th class="text-center">
                                    Jumlah (Orang)</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    < 20</td>
                                <td class="text-center">{{ isset($ages['less20']) ? $ages['less20']:0}}</td>
                            </tr>
                            <tr>
                                <td>21-25</td>
                                <td class="text-center">{{ isset($ages['between2125']) ? $ages['between2125']:0}}</td>
                            </tr>
                            <tr>
                                <td>26-30</td>
                                <td class="text-center">{{ isset($ages['between2630'])? $ages['between2630']:0}}</td>
                            </tr>
                            <tr>
                                <td>31-35</td>
                                <td class="text-center">{{ isset($ages['between3135']) ? $ages['between3135']:0}}</td>
                            </tr>
                            <tr>
                                <td>36-40</td>
                                <td class="text-center">{{ isset($ages['between3640']) ? $ages['between3640']:0}}</td>
                            </tr>
                            <tr>
                                <td>41-45</td>
                                <td class="text-center">{{ isset($ages['between4145']) ? $ages['between4145']:0}}</td>
                            </tr>
                            <tr>
                                <td>46-50</td>
                                <td class="text-center">{{ isset($ages['between4650']) ? $ages['between4650']:0}}</td>
                            </tr>
                            <tr>
                                <td>>51-55</td>
                                <td class="text-center">{{ isset($ages['between5155']) ? $ages['between5155']:0}}</td>
                            </tr>
                            <tr>
                                <td>>55</td>
                                <td class="text-center">{{ isset($ages['greater55']) ? $ages['greater55']:0}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('script')
<script>
    $(document).ready(function() {

        $('#table').DataTable({
            dom: 'Bfrtip'
            , buttons: [
                'copy', 'csv', 'excel'
            ]
            , language: {
                url: 'https://cdn.datatables.net/plug-ins/1.10.21/i18n/Malay.json'
            }
        });
    });

</script>
@endpush
