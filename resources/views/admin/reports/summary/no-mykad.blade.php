@extends('admin.layouts')
@section('css')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
@endsection
@pagetitle(['title'=>'Ringkasan Keseluruhan Anggota Yang Tiada Lampiran MYKAD','links'=>['Laporan','Ringkasan Laporan']])@endpagetitle
@section('content')
<div class="container space-y-2">
    <div class="row">
        <div class="col"><a href="/admin/reports/summary" class="font-weight-bold">Kembali</a></div>
    </div>
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-body px-3">
                    <table class="table" id="table">
                        <thead>
                            <tr>
                                <th>Nama Pelanggan</th>
                                <th class="text-center">No. MYKAD</th>
                                <th class="text-center">No. Telefon</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($users as $key=>$user)
                            <tr>
                                <td>{!! $user->name ?? '' !!}</td>
                                <td class="text-center">{{ $user->ic ?? ''}}</td>
                                <td class="text-center">{{ $user->mobile_no ?? ''}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('script')
<script>
    $(document).ready(function() {
        $('#table').DataTable({
            dom: 'Bfrtip'
            , buttons: [
                'copy', 'csv', 'excel'
            ]
            , language: {
                url: 'https://cdn.datatables.net/plug-ins/1.10.21/i18n/Malay.json'
            }
            , "processing": true
            , "serverSide": true
            , "ajax": {
                "url": "{{ url('/admin/reports/no-mykad') }}"
                , "dataType": "json"
                , "type": "POST"
                , "data": {
                    _token: "{{csrf_token()}}"
                }
            }
            , "columns": [{
                    "data": "name"
                }
                , {
                    "data": "ic"
                }
                , {
                    "data": "mobile_no"
                }
            ]
        });
    });

</script>
@endpush
