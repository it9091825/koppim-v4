@extends('admin.layouts')
@pagetitle(['title'=>'Ringkasan Keseluruhan Anggota Mengikut Status Pendaftaran','links'=>['Laporan','Ringkasan Laporan']])@endpagetitle
@section('content')
<div class="container space-y-2">
    <div class="row">
        <div class="col"><a href="/admin/reports/summary" class="font-weight-bold">Kembali</a></div>
    </div>
    <div class="row">
        <div class="col-2">
            <div class="pull-right">
                <select name="year" id="year" class="form-control">
                    @for($i=2020;$i <= 2025;$i++) <option value="{{$i}}">{{$i}}</option>
                        @endfor
                </select>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-body px-3">
                    <table class="table" id="table">
                        <thead>
                            <tr>
                                <th>Bulan</th>
                                <th class="text-center">Lulus</th>
                                <th class="text-center">Sedang Disemak</th>
                                <th class="text-center">Belum Dikemaskini</th>
                                <th class="text-center">Gagal</th>
                            </tr>
                        </thead>
                        <tbody>

                            @forelse($users as $key=>$user)
                            <tr>
                                <td>{{$key}}</td>
                                <td class="text-center">{{$user['Diluluskan'] ?? 0}}</td>
                                <td class="text-center">{{$user['SedangDisemak'] ?? 0}}</td>
                                <td class="text-center">{{$user['BelumDikemaskini'] ?? 0}}</td>
                                <td class="text-center">{{$user['Gagal'] ?? 0}}</td>
                            </tr>
                            @empty
                            @tablenoresult
                            @endtablenoresult
                            @endforelse

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('script')
<script>
    $(document).ready(function() {

        $('#table').DataTable({
            dom: 'Bfrtip'
            , buttons: [
                'copy', 'csv', 'excel'
            ]
            , language: {
                url: 'https://cdn.datatables.net/plug-ins/1.10.21/i18n/Malay.json'
            }
        });
    });

</script>
<script>
    $("#year").on("change", function() {
        var year = $(this).val();
        return location.href = "{{route('report.summary.byregistrationstatus')}}?year=" + year;

    });
    @if(request()->has('year'))
    $("#year").val("{{request()->year}}");
    @endif

</script>
@endpush
