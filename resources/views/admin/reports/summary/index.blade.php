@extends('admin.layouts')
@section('css')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
@endsection
@pagetitle(['title'=>'Ringkasan Laporan','links'=>['Laporan']])@endpagetitle
@section('content')
<div class="container">
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-body">
                    <ul>
                        <li><a href="/admin/reports/byparliament">Ringkasan Keseluruhan Anggota Mengikut Cawangan Parlimen
                            </a></li>
                        <li><a href="/admin/reports/bygender">Ringkasan Keseluruhan Anggota Mengikut Jantina</a> </li>
                        <li><a href="/admin/reports/byrace">Ringkasan Keseluruhan Anggota Mengikut Bangsa</a> </li>
                        <li><a href="/admin/reports/byreligion">Ringkasan Keseluruhan Anggota Mengikut Agama</a></li>
                        <li><a href="/admin/reports/byincome">Ringkasan Keseluruhan Anggota Mengikut Skala Gaji</a></li>
                        <li><a href="/admin/reports/byage">Ringkasan Keseluruhan Anggota Mengikut Skala Umur</a></li>
                        <li><a href="/admin/reports/bystate">Ringkasan Keseluruhan Anggota Mengikut Negeri</a></li>
                        <li><a href="/admin/reports/byregistrationstatus">Ringkasan Keseluruhan Anggota Mengikut Status Pendaftaran</a></li>
                        <li><a href="/admin/reports/byinfokoppim">Ringkasan Keseluruhan Anggota Mengikut Info Tentang KoPPIM</a></li>
                        {{-- <li><a href="/admin/reports/no-mykad">Ringkasan Keseluruhan Anggota Yang Tiada Lampiran MYKAD</a></li> --}}

                        {{-- <li><a href="">Ringkasan Keseluruhan Anggota Mengikut Negeri</a></li> --}}
                        {{--
                        <li><a href="">Ringkasan Keseluruhan Simpanan Anggota Mengikut Cawangan Parlimen</a></li>
                        <li><a href="">Ringkasan Keseluruhan Simpanan Anggota Mengikut Nombor Keanggotaan</a></li>
                        <li><a href="">Ringkasan Keseluruhan Simpanan Anggota Mengikut Pegangan Simpanan</a></li>
                        <li><a href="">Ringkasan Keseluruhan Syer Anggota Mengikut Nombor Keanggotaan</a></li>
                        <li><a href="">Ringkasan Baki Akhir & Baki Awal Bagi Tahun</a> </li>
                        <li><a href="">Jumlah Dan Pecahan Pinjaman Yang Dikeluarkan</a> </li>
                        <li><a href="">Jumlah Pecahan Baki Awal dan Baki Akhir Pembiayaan Bagi Tahun</a></li>
                        <li><a href="">Senarai Baki Akhir Keanggotaan Bagi Tahun</a> </li>
                        --}}
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('script')

@endpush
