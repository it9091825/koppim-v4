@extends('admin.layouts')
@section('css')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
@endsection
@pagetitle(['title'=>'Ringkasan Keseluruhan Anggota Mengikut Info Maklumat Koppim','links'=>['Laporan','Ringkasan Laporan']])@endpagetitle
@section('content')
<div class="container space-y-2">
    <div class="row">
        <div class="col"><a href="/admin/reports/summary" class="font-weight-bold">Kembali</a></div>
    </div>
    <div class="row">
        <div class="col">
            <b>Jumlah Keseluruhan Anggota (Pemohon & Anggota): {{number_format($users,0,'',',')}} orang anggota</b>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-body px-3">
                    <table class="table" id="table">
                        <thead>
                            <tr>
                                <th >INFO KOPPIM DARI</th>
                                <th >JUMLAH PENGGUNA TAHU INFO KOPPIM</th>
                            </tr>
                        </thead>

                    <tbody>
                        <tr>
                        <td>WEBSITE</td>
                        <td>{{ $all_infos[0]}}</td>
                        </tr>
                        <tr>
                        <td>RADIO ADS</td>
                        <td>{{ $all_infos[1]}}</td>
                        </tr>
                        <tr>
                        <td>OTHERS</td>
                        <td>{{ $all_infos[2]}}</td>
                        </tr>
                        <tr>
                        <td>INSTAGRAM</td>
                        <td>{{ $all_infos[3]}}</td>
                        </tr>
                        <td>FACEBOOK</td>
                        <td>{{ $all_infos[4]}}</td>
                        </tr>
                        <tr>
                        <td>BILLBOARDS</td>
                        <td>{{ $all_infos[5]}}</td>
                        </tr>
                        <td>AFFILIATE</td>
                        <td>{{ $all_infos[6]}}</td>
                        </tr>
                        <td>ANGGOTA LAMA</td>
                        <td>{{ $all_infos[7]}}</td>
                        </tr>


                    </tbody>

                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('script')
<script>
    $(document).ready(function() {

        $('#table').DataTable({
            dom: 'Bfrtip'
            , buttons: [
                'copy', 'csv', 'excel'
            ]
            , language: {
                url: 'https://cdn.datatables.net/plug-ins/1.10.21/i18n/Malay.json'
            }
        });
    });

</script>
@endpush
