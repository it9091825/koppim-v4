@extends('admin.layouts')
@section('css')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
@endsection
@pagetitle(['title'=>'Ringkasan Keseluruhan Anggota Mengikut Jantina','links'=>['Laporan','Ringkasan Laporan']])@endpagetitle
@section('content')
<div class="container space-y-2">
    <div class="row">
        <div class="col"><a href="/admin/reports/summary" class="font-weight-bold">Kembali</a></div>
    </div>
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-body px-3">
                    <table class="table" id="table">
                        <thead>
                            <tr>
                                <th>Nama Cawangan Parlimen</th>
                                <th class="text-center">Jumlah Lelaki (Orang)</th>
                                <th class="text-center">Jumlah Perempuan (Orang)</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($gender as $key=>$g)
                            <tr>
                                <td>{{array_key_exists('PARLIAMENT',$g) ? $g["PARLIAMENT"]:''}}</td>
                                <td class="text-center">{{ array_key_exists('LELAKI',$g) ? $g["LELAKI"]:0}}</td>
                                <td class="text-center">{{ array_key_exists('PEREMPUAN',$g) ? $g["PEREMPUAN"]:0}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('script')
<script>
    $(document).ready(function() {

        $('#table').DataTable({
            dom: 'Bfrtip'
            , buttons: [
                'copy', 'csv', 'excel'
            ]
            , language: {
                url: 'https://cdn.datatables.net/plug-ins/1.10.21/i18n/Malay.json'
            }
        });
    });

</script>
@endpush
