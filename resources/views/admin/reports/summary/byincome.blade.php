@extends('admin.layouts')
@section('css')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
@endsection
@pagetitle(['title'=>'Ringkasan Keseluruhan Anggota Mengikut Skala Gaji','links'=>['Laporan','Ringkasan Laporan']])@endpagetitle
@section('content')
<div class="container space-y-2">
    <div class="row">
        <div class="col"><a href="/admin/reports/summary" class="font-weight-bold">Kembali</a></div>
    </div>
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-body px-3">
                    <table class="table" id="table">
                        <thead>
                            <tr>
                                <th>Tangga Gaji (RM)</th>
                                <th class="text-center">
                                    Jumlah (Orang)</th>
                            </tr>
                        </thead>
                        <tbody>

                            <tr>
                                <td>
                                    < {{Helper::moneyFormat(1000)}}</td>
                                <td class="text-center">{{ isset($monthly_income) ? $monthly_income['less1000']:0}}</td>
                            </tr>
                            <tr>
                                <td>{{Helper::moneyFormat(1001)}} - {{Helper::moneyFormat(2000)}}</td>
                                <td class="text-center">{{ isset($monthly_income) ? $monthly_income['']:0}}</td>
                            </tr>
                            <tr>
                                <td>{{Helper::moneyFormat(2001)}} - {{Helper::moneyFormat(3000)}}</td>
                                <td class="text-center">{{ isset($monthly_income) ? $monthly_income['between20013000']:0}}</td>
                            </tr>
                            <tr>
                                <td>{{Helper::moneyFormat(3001)}} - {{Helper::moneyFormat(4000)}}</td>
                                <td class="text-center">{{ isset($monthly_income) ? $monthly_income['between30014000']:0}}</td>
                            </tr>
                            <tr>
                                <td>{{Helper::moneyFormat(4001)}} - {{Helper::moneyFormat(5000)}}</td>
                                <td class="text-center">{{ isset($monthly_income) ? $monthly_income['between40015000']:0}}</td>
                            </tr>
                            <tr>
                                <td> > {{Helper::moneyFormat(5000)}}</td>
                                <td class="text-center">{{ isset($monthly_income) ? $monthly_income['greater5001']:0}}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('script')
<script>
    $(document).ready(function() {

        $('#table').DataTable({
            dom: 'Bfrtip'
            , buttons: [
                'copy', 'csv', 'excel'
            ]
            , language: {
                url: 'https://cdn.datatables.net/plug-ins/1.10.21/i18n/Malay.json'
            }
        });
    });

</script>
@endpush
