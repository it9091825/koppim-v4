@extends('admin.layouts')
@section('css')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
@endsection
@pagetitle(['title'=>'Ringkasan Keseluruhan Anggota Mengikut Bangsa','links'=>['Laporan','Ringkasan Laporan']])@endpagetitle
@section('content')
<div class="container space-y-2">
    <div class="row">
        <div class="col"><a href="/admin/reports/summary" class="font-weight-bold">Kembali</a></div>
    </div>
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-body px-3">
                    <table class="table" id="table">
                        <thead>
                            <tr>
                                <th>Nama Cawangan Parlimen</th>
                                <th class="text-center">MELAYU (Orang)</th>
                                <th class="text-center">BUMIPUTERA (Orang)</th>
                                <th class="text-center">CINA (Orang)</th>
                                <th class="text-center">India (Orang)</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($race as $key=>$g)
                            <tr>
                                <td>{{array_key_exists('PARLIAMENT',$g) ? $g["PARLIAMENT"]:''}}</td>
                                <td class="text-center">{{ array_key_exists('MELAYU',$g) ? $g["MELAYU"]:0}}</td>
                                <td class="text-center">{{ array_key_exists('BUMIPUTERA',$g) ? $g["BUMIPUTERA"]:0}}</td>
                                <td class="text-center">{{ array_key_exists('CINA',$g) ? $g["CINA"]:0}}</td>
                                <td class="text-center">{{ array_key_exists('INDIA',$g) ? $g["INDIA"]:0}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('script')
<script>
    $(document).ready(function() {

        $('#table').DataTable({
            dom: 'Bfrtip'
            , buttons: [
                'copy', 'csv', 'excel'
            ]
            , language: {
                url: 'https://cdn.datatables.net/plug-ins/1.10.21/i18n/Malay.json'
            }
        });
    });

</script>
@endpush
