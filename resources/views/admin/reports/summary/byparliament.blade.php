@extends('admin.layouts')
@section('css')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
@endsection
@pagetitle(['title'=>'Ringkasan Keseluruhan Anggota Mengikut Cawangan Parlimen','links'=>['Laporan','Ringkasan Laporan']])@endpagetitle
@section('content')
<div class="container space-y-2">
    <div class="row">
        <div class="col"><a href="/admin/reports/summary" class="font-weight-bold">Kembali</a></div>
    </div>
    <div class="row">
        <div class="col">
            <b>Jumlah Keseluruhan Anggota (Pemohon & Anggota): {{number_format($users,0,'',',')}} orang anggota</b>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-body px-3">
                    <table class="table" id="table">
                        <thead>
                            <tr>
                                <th>Nama Cawangan Parlimen</th>
                                <th>Jumlah (Orang)</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($parliament as $p)
                            <tr>
                                <td>{!! $p->parliament == '' ? 'TIDAK DIISI':$p->parliament !!}</td>
                                <td>{{$p->jumlah}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('script')
<script>
    $(document).ready(function() {

        $('#table').DataTable({
            dom: 'Bfrtip'
            , buttons: [
                'copy', 'csv', 'excel'
            ]
            , language: {
                url: 'https://cdn.datatables.net/plug-ins/1.10.21/i18n/Malay.json'
            }
        });
    });

</script>
@endpush
