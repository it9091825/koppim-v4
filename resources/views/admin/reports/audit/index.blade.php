@extends('admin.layouts')
@section('css')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
@endsection
@pagetitle(['title'=>'Laporan Audit','links'=>['Laporan Audit']])@endpagetitle
@section('content')
<div class="container">
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-body">
                    <form action="{{ route('report.audit') }}" method="POST" enctype="multipart/form-data">
                     <div class="row mt-3">
                                <div class="col-lg-4 col-sm-12">
                                    <label for="">Nama Fail</label>
                                    <input type="text" class="form-control fileName" name="filename" required>
                                    <div class="form-text text-muted font-italic">Sila masukkan nama fail untuk disimpan.</div>
                                </div>

    <div class="col-lg-4 col-sm-12">
        <label for="">JENIS AUDIT LAPORAN</label>

                <select class="form-control fileName" style="text-transform: uppercase;" name="audit_type" id="know_from" required>
                    <option value="">Sila Pilih Jenis Audit Laporan.</option>
                    <option value="specific">AUDIT ANGGOTA JUMLAH SYER </option>
                    <option value="all">AUDIT ANGGOTA PEMBAYARAN TERPERINCI </option>
                </select>
                 <div class="form-text text-muted font-italic">Sila Pilih Jenis Audit Laporan.</div>
            </div>



                            </div>

                              @csrf
                         <div class="row mt-3">
                            <div class="col-6">
                                <div class="form-group">
                                    <label for="" name="start_date" class="form-label ">Sila pilih tarikh untuk diaudit</label>
                                    <div class="flatpickr input-group mb-2">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text w-16">Dari</span>
                                        </div>
                                        <input type="text" class="permohonan_start_date form-control" name="from" data-input>
                                        <div class="input-group-append">
                                            <span class="input-button input-group-text" title="clear" style="cursor: pointer;" data-clear>
                                                <svg class="w-5 h-5 text-danger" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12"></path>
                                                </svg>
                                            </span>
                                        </div>
                                        <div class="form-text text-muted"></div>
                                    </div>
                                    <div class="flatpickr input-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text w-16">Hingga</span>
                                        </div>
                                        <input type="text" class="permohonan_end_date form-control" name="to" data-input>
                                        <div class="input-group-append">
                                            <span class="input-button input-group-text" title="clear" style="cursor: pointer;" data-clear>
                                                <svg class="w-5 h-5 text-danger" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12"></path>
                                                </svg>
                                            </span>
                                        </div>

                                    </div>
                                </div>

                            </div>
                        </div>

                         <div class="row mt-3">
                                <div class="col">
                                    <div class="form-group text-right">
                                        <button type="submit" class="btn btn-success btn-export ">Eksport Data Audit Ke CSV</button>
                                    </div>
                                </div>
                            </div>
    </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection

@push('script')
<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
<script>

    $('.select2').select2({
        theme: 'bootstrap4'
    });


    $(".datepicker").flatpickr();
    $(".flatpickr").flatpickr({
        wrap: true
    });


</script>
@endpush
