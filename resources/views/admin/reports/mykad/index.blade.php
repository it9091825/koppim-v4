<div class="row mt-3">
    <div class="col-6">
        <div class="form-group">
            <label for="" name="start_date" class="form-label ">Sila pilih tarikh keanggotaan untuk dieksport</label>
            <div class="flatpickr input-group mb-2">
                <div class="input-group-prepend">
                    <span class="input-group-text w-16">Dari</span>
                </div>
                <input type="text" class="mykad_start_date form-control" name="mykad_start_date" data-input>
                <div class="input-group-append">
                    <span class="input-button input-group-text" title="clear" style="cursor: pointer;" data-clear>
                        <svg class="w-5 h-5 text-danger" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12"></path>
                        </svg>
                    </span>
                </div>
                <div class="form-text text-muted"></div>
            </div>
            <div class="flatpickr input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text w-16">Hingga</span>
                </div>
                <input type="text" class="mykad_end_date form-control" name="mykad_end_date" data-input>
                <div class="input-group-append">
                    <span class="input-button input-group-text" title="clear" style="cursor: pointer;" data-clear>
                        <svg class="w-5 h-5 text-danger" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12"></path>
                        </svg>
                    </span>
                </div>

            </div>
        </div>

    </div>
</div>
<div class="row">
    <div class="col-6">
        <div class="form-group">
            <label for="" class="form-label">Pilih Negeri</label>
            <select name="states" id="mykad_states" class="mykad_states form-control select2"></select>
        </div>
        <div class="form-group">
            <label for="" class="form-label">Pilih Cawangan Parlimen</label>
            <select name="parliaments" id="mykad_parliaments" class="mykad_parliaments form-control select2"></select>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-6">
        <div class="form-group">
            <label for="" class="form-label">Sila pilih kolum untuk dieksport</label>
            <select name="mykad_columns[]" id="" class="mykad_columns select2 form-control" multiple>
                <option value="name" class="text-uppercase">NAMA PENUH</option>
                <option value="ic" class="text-uppercase">NO. MYKAD</option>
                <option value="dob" class="text-uppercase">TARIKH LAHIR</option>
                <option value="religion" class="text-uppercase">AGAMA</option>
                <option value="race" class="text-uppercase">BANGSA</option>
                <option value="marital_status" class="text-uppercase">STATUS PERKAHWINAN</option>
                <option value="email" class="text-uppercase">E-MEL</option>
                <option value="mobile_no" class="uppercase">NO. TELEFON BIMBIT</option>
                <option value="home_no" class="text-uppercase">NO. TELEFON RUMAH</option>
                <option value="job" class="text-uppercase">PEKERJAAN</option>
                <option value="monthly_income" class="text-uppercase">PENDAPATAN BULANAN</option>
                <option value="heir_name" class="text-uppercase">NAMA WARIS</option>
                <option value="heir_ic" class="text-uppercase">NO. MYKAD WARIS</option>
                <option value="heir_relationship" class="text-uppercase">HUBUNGAN WARIS</option>
                <option value="heir_phone" class="text-uppercase">NO. TEL BIMBIT WARIS</option>

                <option value="address_line_1" class="text-uppercase">ALAMAT BARIS 1</option>
                <option value="address_line_2" class="text-uppercase">ALAMAT BARIS 2</option>
                <option value="address_line_3" class="text-uppercase">ALAMAT BARIS 3</option>
                <option value="postcode" class="text-uppercase">POSKOD</option>
                <option value="city" class="text-uppercase">BANDAR</option>
                <option value="state" class="text-uppercase">NEGERI</option>
                <option value="parliament" class="text-uppercase">KAWASAN PARLIMEN</option>
                <option value="created_at" class="text-uppercase">TARIKH PENDAFTARAN</option>
            </select>
            <div class="form-text text-info">Biarkan ruangan ini kosong untuk mengeksport semua kolum</div>
        </div>
    </div>
</div>
@push('script')
<script>



    $(document).ready(function() {
        var states = $('#mykad_states');
        getMykadStatesList();
    });

    getMykadParliamentListByState()

    function getMykadStatesList() {

        $.ajax({
            type: "GET"
            , url: "/api/states"
            , success: function(res) {
                if (res) {
                    $("#mykad_states").empty();
                    $("#mykad_states").append('<option value="all">SEMUA NEGERI</option>');
                    $.each(res, function(key, value) {
                        $("#mykad_states").append('<option value="' + value + '">' + key + '</option>');
                    });

                } else {
                    $("#mykad_states").empty();
                }
            }
        });

    }

    function getMykadParliamentListByState() {

        $('#mykad_states').on('change', function() {
            var state_id = $(this).val();

            if (state_id) {
                $.ajax({
                    type: "GET"
                    , url: "/api/state/parliaments?state_id=" + state_id
                    , success: function(res) {
                        if (res) {
                            $("#mykad_parliaments").empty();
                            if (res.name != 'LUAR NEGARA') {
                                $('#mykad_parliaments').append('<option value="all">SEMUA CAWANGAN PARLIMEN</option>');
                                $.each(res, function(key, value) {
                                    $("#mykad_parliaments").append('<option value="' + value + '">' + value + '</option>');
                                });
                            } else {
                                $("#mykad_parliaments").empty();
                            }
                        } else {
                            $("#mykad_parliaments").empty();
                        }
                    }
                });
            } else {
                $("#mykad_parliaments").empty();
            }

        });
    }

</script>
@endpush
