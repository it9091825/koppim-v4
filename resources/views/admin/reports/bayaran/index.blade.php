<div class="row mt-3">
    <div class="col-lg-6 col-sm-12">
        <div class="form-group">
            <label for="" class="form-label">Sila pilih tarikh pembayaran untuk dieksport</label>
            <div class="flatpickr input-group mb-2">
                <div class="input-group-prepend">
                    <span class="input-group-text w-16">Dari</span>
                </div>
                <input type="text" class="payment_start_date form-control" name="payment_start_date" data-input>
                <div class="input-group-append">
                    <span class="input-button input-group-text" title="clear" style="cursor: pointer;" data-clear>
                        <svg class="w-5 h-5 text-danger" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12"></path>
                        </svg>
                    </span>
                </div>

            </div>
            <div class="flatpickr input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text w-16">Hingga</span>
                </div>
                <input type="text" class="payment_end_date form-control" name="payment_end_date" data-input>
                <div class="input-group-append">
                    <span class="input-button input-group-text" title="clear" style="cursor: pointer;" data-clear>
                        <svg class="w-5 h-5 text-danger" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12"></path>
                        </svg>
                    </span>
                </div>

            </div>
        </div>

    </div>
</div>
<div class="row">
    <div class="col-lg-6 col-sm-12">
        <div class="form-group">
            <label for="" class="form-label">Sila pilih kaedah pembayaran</label>
            <select name="payment_method" id="" class="payment_method select2 form-control">
                <option value="">Sila Pilih</option>
                <option value="online">ONLINE</option>
                <option value="manual">MANUAL</option>
                <option value="ikoop">IKOOP</option>
            </select>
        </div>

    </div>
</div>
<div class="row">
    <div class="col-lg-6 col-sm-12">
        <div class="form-group">
            <label for="" class="form-label">Sila pilih status transaksi bayaran</label>
            <select name="payment_status" id="" class="payment_status select2 form-control">
                <option value="">Sila Pilih</option>
                <option value="payment-completed">TRANSAKSI BERJAYA</option>
                <option value="waiting-for-verification">UNTUK DISEMAK</option>
                <option value="waiting-for-approval">UNTUK DILULUSKAN</option>
            </select>
        </div>
        <div class="form-group">
            <label for="" class="form-label">Sila pilih kolum untuk dieksport</label>
            <select name="payment_columns[]" id="" class="payment_columns select2 form-control" multiple>
                <option value="name" class="text-uppercase">NAMA PENUH</option>
                <option value="ic" class="text-uppercase">NO. MYKAD</option>
                <option value="email" class="text-uppercase">E-MEL</option>
                <option value="phone" class="uppercase">NO. TELEFON BIMBIT</option>
                <option value="amount" class="uppercase">JUMLAH KESELURUHAN LANGGANAN SYER</option>
                <option value="share_amount" class="uppercase">JUMLAH LANGGANAN SYER</option>
                <option value="first_time_fees" class="uppercase">FI PENDAFTARAN</option>
                <option value="first_time_share" class="uppercase">LANGGANAN MODAL SYER</option>
                <option value="additional_share" class="uppercase">LANGGANAN SYER TAMBAHAN</option>
                <option value="payment_desc" class="uppercase">KETERANGAN BAYARAN</option>
                <option value="status">STATUS BAYARAN</option>
                <option value="channel" class="uppercase">KAEDAH PEMBAYARAN</option>
                <option value="created_at">TARIKH BAYARAN</option>
            </select>
            <div class="form-text text-info">Biarkan ruangan ini kosong untuk mengeksport semua kolum</div>
        </div>
    </div>
</div>
