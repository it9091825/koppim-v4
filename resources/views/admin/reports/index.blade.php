@extends('admin.layouts')
@section('css')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
@endsection
@pagetitle(['title'=>'Jana Laporan','links'=>['Laporan']])@endpagetitle
@section('content')
<div class="container">
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-body">
                    <form action="">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-lg-4 col-sm-12">
                                    <label for="" class="form-label">Jenis Laporan</label>
                                    <select name="report_type" id="report_type" class="report_type form-control select2">
                                        <option value="">Sila Pilih</option>
                                        <option data-selector="permohonan_fields" value="permohonan">Pendaftaran Baharu</option>
                                        <option data-selector="keanggotaan_fields" value="keanggotaan">Keanggotaan</option>
                                        <option data-selector="bayaran_fields" value="bayaran">Transaksi Pembayaran</option>
                                        <option data-selector="mykad_fields" value="mykad">Tiada Lampiran MYKAD</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row mt-3">
                                <div class="col-lg-4 col-sm-12">
                                    <label for="">Nama Fail</label>
                                    <input type="text" class="form-control fileName" name="fileName">
                                    <div class="form-text text-muted font-italic">Sila masukkan nama fail untuk disimpan.</div>
                                </div>
                            </div>
                            <!-- PERMOHONAN FIELDS -->
                            <div class="section permohonan_fields">
                                @include('admin.reports.permohonan.index')
                            </div>
                            <!-- KEANGGOTAAN FIELDS -->
                            <div class="section keanggotaan_fields">
                                @include('admin.reports.keanggotaan.index')
                            </div>
                            <!--BAYARAN FIELDS-->
                            <div class="section bayaran_fields">
                                @include('admin.reports.bayaran.index')
                            </div>
                            <!--BAYARAN FIELDS-->
                            <div class="section affiliates_fields">
                                @include('admin.reports.affiliates.index')
                            </div>
                            <div class="section mykad_fields">
                                @include('admin.reports.mykad.index')
                            </div>
                            <div class="row mt-3">
                                <div class="col">
                                    <div class="form-group text-right">
                                        <button type="button" class="btn btn-success btn-result font-weight-bold">Buat Carian Rekod</button>
                                        <button type="button" class="btn btn-primary btn-export d-none">Eksport</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('script')
<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });


    $('.select2').select2({
        theme: 'bootstrap4'
    });

    $('.columns,.payment_columns').select2({
        multiple: true
    });

    $(".datepicker").flatpickr();
    $(".flatpickr").flatpickr({
        wrap: true
    });

    $('.keanggotaan_fields').hide();
    $('.bayaran_fields').hide();
    $('.affiliates_fields').hide();
    $('.permohonan_fields').hide();
    $('.mykad_fields').hide();

    $('#report_type').on('change', function() {

        $(this).find("option:selected").each(function() {
            var optionValue = $(this).data("selector");
            if (optionValue) {
                $(".section").not("." + optionValue).hide();
                $("." + optionValue).show();
            } else {
                $(".section").hide();
            }
        });
    });



    function queries() {
        return {
            report_type: $(".report_type").val()
            , fileName: $(".fileName").val()
            , permohonan_start_date: $(".permohonan_start_date").val()
            , permohonan_end_date: $(".permohonan_end_date").val()
            , keanggotaan_start_date: $(".keanggotaan_start_date").val()
            , keanggotaan_end_date: $(".keanggotaan_end_date").val()
            , payment_start_date: $(".payment_start_date").val()
            , payment_end_date: $(".payment_end_date").val()

            , permohonan_status: $(".permohonan_status").val()
            , keanggotaan_status: $(".keanggotaan_status").val()
            , payment_status: $(".payment_status").val()

            , payment_method: $(".payment_method").val()

            , permohonan_columns: $(".permohonan_columns").val()
            , keanggotaan_columns: $(".keanggotaan_columns").val()
            , payment_columns: $(".payment_columns").val()

            , permohonan_registration_type: $('.permohonan_registration_type').val()
            , keanggotaan_email: $('.keanggotaan_email').val()
            , keanggotaan_heir: $('.keanggotaan_heir').val()
            , permohonan_states: $('.permohonan_states').val()
            , permohonan_parliaments: $('.permohonan_parliaments').val()
            , keanggotaan_states: $('.keanggotaan_states').val()
            , keanggotaan_parliaments: $('.keanggotaan_parliaments').val()

            , mykad_start_date: $('.mykad_start_date').val()
            , mykad_end_date: $('.mykad_end_date').val()
            , mykad_states: $('.mykad_states').val()
            , mykad_parliaments: $('.mykad_parliaments').val()
            , mykad_columns: $('.mykad_columns').val()
        }
    }


    $(".btn-result").click(function(e) {

        e.preventDefault();

        $.ajax({
            type: 'POST'
            , url: "{{ route('report.getResult') }}"
            , data: queries()
            , success: function(data) {
                if (data.result > 0) {

                    Swal.fire({
                        title: 'Carian Berjaya!'
                        , html: `Klik <b>Eksport</b> untuk eksport semua data ke dalam format Excel`
                        , showCancelButton: true
                        , cancelButtonText: `Batal`
                        , confirmButtonText: `Eksport`
                        , icon: 'success'
                    , }).then((result) => {

                        if (result.isConfirmed) {
                            exportRecords();
                        }
                    })
                } else {
                    Swal.fire({
                        title: 'Harap Maaf'
                        , text: 'Tiada rekod untuk dieksport'
                        , icon: 'error'
                    })
                }
            }
        });

    });

    function exportRecords() {
        var url = "{{route('report.export')}}?" + $.param(queries())

        window.location = url;
    }

</script>
@endpush
