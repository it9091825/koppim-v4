<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/docs/4.0/assets/img/favicons/favicon.ico">

    <title>@if($payment->status == 'waiting-for-verification' || $payment->status == 'waiting-for-approval' || $payment->status == 'waiting-for-reverification')
        BORANG PENGESAHAN
        @elseif($payment->status == 'payment-completed')
        RESIT PEMBAYARAN
        @endif FI PENDAFTARAN/LANGGANAN SYER</title>

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css2?family=Roboto+Condensed&display=swap" rel="stylesheet">
    <style>
        body {
            font-family: 'Roboto Condensed', sans-serif;
        }

        table tr td {
            border: 1px solid black !important;
        }

    </style>
</head>



<body>
    <div class="container py-5">
        <div class="row">
            <div class="col-md-12 mb-3">
                <div class="text-center mb-3">
                    <img src="{{asset('img/logo-koppim.png')}}" alt="" style="width:300px;">
                </div>
                <h3 class="text-center" style="color:#2B2F92">
                    <strong><span>
                            @if($payment->status == 'waiting-for-verification' || $payment->status == 'waiting-for-approval' || $payment->status == 'waiting-for-reverification')
                            BORANG PENGESAHAN<br>
                            @elseif($payment->status == 'payment-completed')
                            RESIT PEMBAYARAN<br>
                            @endif
                            FI PENDAFTARAN / LANGGANAN SYER

                        </span></strong>
                </h3>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h5 class="text-uppercase font-weight-bold">Butiran Anggota</h5>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <table class="table">
                    <tbody>
                        <tr>
                            <td class="text-uppercase font-weight-bold">
                                nama
                            </td>
                            <td>
                                {{ strtoupper($payment->name ?? '') }}
                            </td>
                        </tr>
                        <tr>
                            <td class="text-uppercase font-weight-bold">
                                no. mykad
                            </td>
                            <td>
                                {{ strtoupper($payment->ic ?? '') }}
                            </td>

                        </tr>
                        <tr>
                            <td class="text-uppercase font-weight-bold">
                                no. telefon
                            </td>
                            <td>
                                {{ strtoupper($payment->phone ?? '') }}
                            </td>

                        </tr>
                        <tr>
                            <td class="text-uppercase font-weight-bold">
                                e-mel
                            </td>
                            <td>
                                {{ $payment->email ?? '' }}
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="row mt-3">
            <div class="col-md-12">
                <h5 class="text-uppercase font-weight-bold">Butiran Transaksi</h5>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <table class="table">
                    <tbody>
                        <tr>
                            <td class="text-uppercase font-weight-bold">
                                TARIKH TRANSAKSI
                            </td>
                            <td>
                                {{$payment->payment_date ?? $payment->manual_payment_date}}
                            </td>
                        </tr>
                        <tr>
                            <td class="text-uppercase font-weight-bold">
                                FI PENDAFTARAN ANGGOTA BAHARU
                            </td>
                            <td>
                                {{'RM '. number_format($payment->first_time_fees,2,'.','.')}}
                            </td>

                        </tr>
                        <tr>
                            <td class="text-uppercase font-weight-bold">
                                LANGGANAN MODAL SYER
                            </td>
                            <td>
                                {{'RM '. number_format($payment->first_time_share,2,'.','.')}}
                            </td>

                        </tr>
                        <tr>
                            <td class="text-uppercase font-weight-bold">
                                LANGGANAN TAMBAHAN
                            </td>
                            <td>
                                {{'RM '. number_format($payment->additional_share,2,'.','.')}}
                            </td>
                        </tr>
                        <tr>
                            <td class="text-uppercase font-weight-bold">
                                jumlah keseluruhan
                            </td>
                            <td>
                                {{'RM '. number_format($payment->amount,2,'.','.')}}
                            </td>
                        </tr>
                        @if($payment->channel != 'online')
                        <tr>
                            <td class="text-uppercase font-weight-bold">
                                LAMPIRAN BUKTI PEMBAYARAN
                            </td>
                            <td>
                                <img src="{{env('DO_SPACES_FULL_URL').$payment->payment_proof }}" class="img-fluid img-thumbnail" style="height:300px">
                            </td>
                        </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
        @if($payment->status == 'waiting-for-verification' || $payment->status == 'waiting-for-approval' || $payment->status == 'waiting-for-reverification')
        <div class="row mt-5">
            <div class="col-4">
                <h5 class="text-left text-uppercase">
                    disemak oleh:

                </h5>
                <br><br><br>
                <hr>
                <h6 class="text-uppercase">nama:</h6>
                <h6 class="text-uppercase">Jawatan:</h6>
            </div>
            <div class="col-4">
                <h5 class="text-left text-uppercase">
                    diluluskan oleh:
                </h5>
                <br><br><br>
                <hr>
                <h6 class="text-uppercase">nama:</h6>
                <h6 class="text-uppercase">Jawatan:</h6>
            </div>
            <div class="col-4">
                <h5 class="text-left text-uppercase">
                    dikemaskini oleh (sistem):
                </h5>
                <br><br><br>
                <hr>
                <h6 class="text-uppercase">nama:</h6>
                <h6 class="text-uppercase">Jawatan:</h6>
            </div>
        </div>
        @else
        <div class="row mt-5">
            <div class="col">
                <div class="text-center">
                    <h4>Resit ini dijana oleh sistem. Tiada tandatangan pengesahan diperlukan.</h4>
                </div>
            </div>
        </div>
        @endif
    </div>

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>

    <script>
        window.print();

    </script>
</body>

</html>
