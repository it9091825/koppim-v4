@extends('admin.layouts')
@section('css')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/jquery-datetimepicker@2.5.21/jquery.datetimepicker.css">
@endsection
@section('content')
<div class="container">
    <div class="row mt-5">
        <div class="col-md-12">
            <a href="{{route('sms.index')}}">Back to Announcements</a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">

            <div class="card">
                <div class="card-header bg-white">
                    <h5 class="card-title">{!! $announcement->title !!}</h5>
                </div>
                <div class="card-body">
                    <form action="{{route('sms.update',['id' => $announcement->id])}}" method="POST"
                        enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="form-group">
                            <label class="form-label">Title</label>
                            <input type="text" name="title" id="" class="form-control"
                                value="{{old('title', $announcement->title ?? null)}}">
                        </div>
                        <div class="form-group">
                            <label class="form-label">Message</label>
                            <textarea type="text" name="message" id=""
                                class="form-control">{{old('message', $announcement->message ?? null)}}</textarea>
                        </div>

                        <div class="form-group">
                            <label class="form-label">Published On</label>
                            <input type="text" name="published_from" id="published_from" class="form-control"
                                value="{{old('published_from', $announcement->published_from ?? null)}}">
                            <small class="text-danger">The date you want this announcement to first appear must be
                                unique.</small>
                        </div>
                        <div class="form-group">
                            <label class="form-label">Published Until</label>
                            <input type="text" name="published_until" id="published_until" class="form-control"
                                value="{{old('published_until', $announcement->published_until ?? null)}}">
                            <small class="text-danger">The date for this announcement to end must be unique.</small>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Update Announcement</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection



@section('js')

@endsection