@extends('admin.layouts')

@pagetitle(['title' => 'Hantar SMS','links'=>['Hantar SMS']])@endpagetitle

@section('content')
<div class="container">
    <div class="row">
        <div class="col">
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
        </div>
    </div>
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-header bg-white">
                    <h5 class="card-title">
                        Hantar SMS
                    </h5>
                </div>
                <div class="card-body">
                    <form action="{{route('sms.store')}}" method="POST" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="form-group">
                            <label for="" class="form-label">Tajuk</label>
                            <input type="text" name="title" id="" class="form-control" value="{{old('title', $sms->title ?? null)}}">
                            <small id="" class="form-text text-muted">
                                Tajuk ini tidak akan dihantar bersama SMS. Digunakan sebagai rujukan admin sahaja.
                            </small>
                        </div>

                        <div class="form-group">
                            <label for="" class="form-label">Pilih penerima berdasarkan status</label>
                            <select name="bystatus" id="bystatus" class="form-control select2">
                                <option value="">PILIH PENERIMA SECARA INDIVIDU</option>
                                <option value="all">SEMUA ANGGOTA</option>
                                <option value="0">BELUM DIKEMASKINI</option>
                                <option value="1">DILULUSKAN (ANGGOTA AKTIF)</option>
                                <option value="2">UNTUK DISEMAK</option>
                                <option value="3">UNTUK DILULUSKAN</option>
                            </select>
                        </div>
                        <div class="form-group" id="affiliate_section">
                            <input type="checkbox" name="affiliate">
                            <label for="" class="form-label">Hantar kepada Affiliate Sahaja</label>
                        </div>
                        <div class="form-group">
                            <label for="" class="form-label">Pilih penerima secara individu (boleh pilih lebih dari satu penerima)</label>
                            <select id="user_selector" class="form-control select2" name="q[]" data-placeholder="Cari pelanggan menggunakan Nama, No. MYKAD atau No. Telefon Bimbit" multiple>
                            </select>
                        </div>
                        {{-- state parliament dropdown --}}
                        @include('include.state-parliament')
                        <div class="form-group">
                            <label for="" class="form-label">Mesej</label>
                            <textarea type="text" name="message" id="" class="form-control">{{old('message', $sms->message ?? null)}}</textarea>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary font-weight-bold text-uppercase">Hantar SMS</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        @include('include.pagination',['results'=>$sms])
        <div class="col">
            <div class="card">
                <div class="card-header bg-white">
                    <h5 class="card-title">Senarai SMS</h5>
                </div>
                <table class="table card-table table-vcenter">
                    <thead>
                        <tr>
                            <th>Tajuk</th>
                            <th>Mesej</th>
                            <th class="text-center">Jumlah Penerima (Orang)</th>
                            <th class="text-center">Dihantar Pada</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if($sms)
                        @foreach($sms as $message)
                        <tr>
                            <td><span data-toggle="tooltip" title="Message: {!! $message->message!!}">{{ $message->title }}</span>
                            </td>
                            <td>{!! $message->message ?? '' !!}</td>
                            <td class="text-center">
                                @if($message->recepient == 'all')
                                Semua
                                @else
                                {{ $message->recepient != 'null' ? count(json_decode($message->recepient)):'TIADA' }}
                                @endif
                            </td>
                            <td class="text-center">{{Carbon\Carbon::parse($message->created_at)->translatedFormat('d F Y H:m:s')}}</td>
                        </tr>
                        @endforeach
                        @else
                        <tr>
                            <td colspan="10" class="text-center">
                                <h4>TIADA REKOD</h4>
                            </td>
                        </tr>
                        @endif
                    </tbody>
                </table>

            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script src="https://cdn.jsdelivr.net/npm/jquery-datetimepicker@2.5.21/build/jquery.datetimepicker.full.min.js" integrity="sha256-JADorIjphY5ztERiDARizkSMD2NW4HpsDlAyageYtFc=" crossorigin="anonymous"></script>

<script>
    $(document).ready(function() {
        // $('#states,#parliaments').select2();

        $('#states').prop('disabled', true);
        $('#parliaments').prop('disabled', true);
        $('#affiliate_section').hide();
    });

    $('#bystatus').on('change', function() {
        if ($(this).children(":selected").val() != '') {
            $('#user_selector').prop('disabled', true);
            $('#states').prop('disabled', false);
            $('#parliaments').prop('disabled', false);
            if ($(this).children(":selected").val() == 'all') {
                $('#affiliate_section').show();
            }
            if ($(this).children(":selected").val() != 'all') {
                $('#affiliate_section').hide();
            }
        } else {
            $('#user_selector').prop('disabled', false);
            $('#states').prop('disabled', true);
            $('#states').val('').trigger('change');
            $('#parliaments').prop('disabled', true);
            $('#parliaments').val('').trigger('change');
            $('#affiliate_section').hide();
        }
        $('#user_selector').val('').trigger('change');
    });

    var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

    $("#user_selector").select2({
        multiple: true
        , width: 'resolve'
        , ajax: {
            url: "{{route('sms.find.user')}}"
            , dataType: 'json'
            , data: function(params) {
                return {
                    _token: CSRF_TOKEN
                    , q: $.trim(params.term)
                };
            }
            , processResults: function(data) {
                return {
                    results: data
                };
            }
            , cache: true
        }

    });

</script>
@endsection
