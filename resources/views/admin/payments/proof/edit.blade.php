@extends('admin.layouts')

@section('header')
<div class="container">
    <div class="row page-title">
        <div class="col-md-12">
            <nav aria-label="breadcrumb" class="float-right mt-1">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item active" aria-current="page">Transaksi Bayaran</li>
                    <li class="breadcrumb-item active" aria-current="page">Edit Bukti Bayaran</li>
                </ol>
            </nav>
            <h4 class="mb-1 mt-0 text-uppercase">Edit Bukti Bayaran</h4>
        </div>
    </div>
</div> <!-- container-fluid -->

@endsection


@section('content')

<div class="container">
    <div class="row ">
        <div class="col-lg-12 col-xl-12 mg-t-12">
            <div class="card">
                <div class="card-body px-2 py-0">
                    <img src="{{env('DO_SPACES_FULL_URL').$proof}}" alt="">
                </div><!-- card-body -->
            </div><!-- card -->
        </div>
    </div>
</div><!-- container -->
@endsection
