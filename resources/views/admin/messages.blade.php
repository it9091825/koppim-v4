@extends('admin.layouts')
@pagetitle(['title'=>'Mesej Pelanggan'])@endpagetitle
@section('content')
<div class="container">
    <div class="row" style="">
        <div class="col-lg-6">
            <div class="input-group mb-3">
                <input type="text" id="search" class="form-control" value="{{ $search }}" placeholder="CARI DENGAN NO. MYKAD">
                <div class="input-group-append">
                    <button class="btn btn-primary font-weight-bold submit" onclick="search_data()">CARI</button>
                </div>
            </div>
        </div>
    </div>
    <div class="row ">
        <div class="col-lg-12">
            <div class="d-flex justify-content-between align-items-center">
                <div class="d-flex justify-content-center align-items-center mb-3">
                    @if($messages)
                    Paparan {{number_format($messages->firstItem(),0,"",",")}} Hingga {{ number_format($messages->lastItem(),0,"",",")}} Daripada {{number_format($messages->total(),0,"",",")}} Dalam Senarai
                    @endif
                </div>
                <div class="d-flex justify-content-center align-items-center">
                    {{ $messages->links() }}
                </div>
            </div>
        </div>
        <div class="col-lg-12 col-xl-12 mg-t-12">
            <div class="card">
                <div class="card-body p-0">
                    <div class="table-responsive">
                        <table class="table">
                            <thead class="thead-primary">
                                <tr>
                                    <th class="text-center">TARIKH TERIMA MESEJ</th>
                                    <th class="text-center">NO. MYKAD</th>
                                    <th>MESEJ</th>
                                    <th>INFO LAIN</th>
                                    <th class="text-center">PERINGKAT MESEJ</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse($messages as $key=>$message)
                                <tr>
                                    </td>
                                    <td class="text-center">{!! $message->created_at->format("d/m/Y \<\b\\r\> h:i:sa") !!}</td>
                                    <td><a target="_blank" href="{{url('admin/members?search='.$message->ic)}}">{{$message->ic ?? ''}}</a></td>
                                    <td>
                                        {!! $message->messages ?? '' !!}
                                    </td>
                                    <td>
                                        @if(!empty($message->info))
                                        <ul>
                                            @foreach($message->info as $key => $info)
                                            <li>{{ucwords($key)}}: {{$info}}</li>
                                            @endforeach
                                        </ul>
                                        @endif
                                    </td>
                                    <td class="text-center">{{$message->remarks ?? ''}}</a>
                                    </td>
                                </tr>
                                @empty
                                <tr>
                                    <td colspan="3">TIADA REKOD</td>
                                </tr>

                                @endforelse

                            </tbody>
                        </table>


                    </div>


                </div><!-- card-body -->
            </div><!-- card -->
        </div>


    </div>




</div><!-- container -->


@endsection

@section('js')
<script>
    function search_data() {
        var search = $('#search').val();

        window.location.href = "{{route('admin.mesej.index')}}?search=" + search + "";
    }

    $(document).on("keypress", ".input-group:has(input:input, span.input-group-btn:has(.btn)) input:input", function(e) {
        if (e.which == 13) {
            $(this).closest(".input-group").find(".input-group-append > .submit").click();
        }
    });

</script>

@endsection
