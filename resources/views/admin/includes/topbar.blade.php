<div class="navbar navbar-expand flex-column flex-md-row navbar-custom">
    <div class="container-fluid">
        <!-- LOGO -->
        <a href="/" class="navbar-brand mr-0 mr-md-2 logo">
            <span class="logo-lg">
                <img src="{{asset('img/logo-koppim.png')}}" height="24" />

            </span>
            <span class="logo-sm">
                <img src="{{asset('img/logo-koppim.png')}}" height="24" />
            </span>
        </a>

        <ul class="navbar-nav bd-navbar-nav flex-row list-unstyled menu-left mb-0">
            <li class="">
                <button class="button-menu-mobile open-left disable-btn">
                    <i data-feather="menu" class="menu-icon"></i>
                    <i data-feather="x" class="close-icon"></i>
                </button>
            </li>
        </ul>

        <ul class="navbar-nav flex-row ml-auto d-flex align-items-center list-unstyled topnav-menu float-right mb-0">
            <li class="dropdown d-none d-lg-block">
                <a class="dropdown-toggle mr-0 btn btn btn-sm btn-rounded btn-primary align-items-center" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                    <svg class="w-5 h-5 text-yellow-300" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M13 10V3L4 14h7v7l9-11h-7z"></path>
                    </svg>
                    <span class="font-weight-bold">Tindakan Admin</span>
                </a>
                <div class="dropdown-menu dropdown-menu-right">
                    @can('tambah-anggota')
                    <!-- item-->
                    <a href="{{route('admin.member.create')}}" class="dropdown-item notify-item">
                        Daftarkan Anggota (Manual)
                    </a>
                    @endcan
                    @can('sahkan-maklumat-anggota')
                    <a href="{{route('admin.member.new')}}" class="dropdown-item notify-item">
                        <span> Semak Permohonan Anggota</span>
                    </a>
                    @endcan
                    @can('luluskan-permohonan-anggota')
                    <a href="{{route('admin.member.reviewed')}}" class="dropdown-item notify-item">
                        <span>Luluskan Permohonan Anggota </span>
                    </a>
                    @endcan
                    @can('sahkan-bayaran-manual')
                    <a href="/admin/tx/manual" class="dropdown-item notify-item">
                        <span> Semak Bayaran Manual</span>
                    </a>
                    @endcan
                    @can('luluskan-bayaran-manual')
                    <a href="/admin/tx/approval" class="dropdown-item notify-item">
                        <span> Luluskan Bayaran Manual</span>
                    </a>
                    @endcan
                </div>
            </li>
            <li class="dropdown notification-list ">
                <a class="nav-link dropdown-toggle nav-user mr-0" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                    <div class="media user-profile ">
                        <div class="media-body text-right">
                            <h6 class="pro-user-name ml-2 my-0">
                                <span>{{ Auth::user()->name }}</span>

                            </h6>
                        </div>
                        <span data-feather="chevron-down" class="ml-2 align-self-center"></span>
                    </div>
                </a>
                <div class="dropdown-menu profile-dropdown-items dropdown-menu-right">
                    <a href="/admin/my-profile" class="dropdown-item notify-item">
                        <svg class="w-5 h-5" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M5.121 17.804A13.937 13.937 0 0112 16c2.5 0 4.847.655 6.879 1.804M15 10a3 3 0 11-6 0 3 3 0 016 0zm6 2a9 9 0 11-18 0 9 9 0 0118 0z"></path>
                        </svg>
                        <span>Profil Saya</span>
                    </a>
                    <a href="/admin/logout" class="dropdown-item notify-item">
                        <svg class="w-5 h-5" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M17 16l4-4m0 0l-4-4m4 4H7m6 4v1a3 3 0 01-3 3H6a3 3 0 01-3-3V7a3 3 0 013-3h4a3 3 0 013 3v1"></path>
                        </svg>
                        <span>Log Keluar</span>
                    </a>
                </div>
            </li>
        </ul>
    </div>

</div>
