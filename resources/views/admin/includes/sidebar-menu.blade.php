<ul class="metismenu" id="menu-bar">
    <li>
        <a href="/admin/dashboard">
            <svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M3 12l2-2m0 0l7-7 7 7M5 10v10a1 1 0 001 1h3m10-11l2 2m-2-2v10a1 1 0 01-1 1h-3m-6 0a1 1 0 001-1v-4a1 1 0 011-1h2a1 1 0 011 1v4a1 1 0 001 1m-6 0h6"></path>
            </svg>
            <span> Laman Utama </span>
        </a>
    </li>
    @can('lihat-mesej-pelanggan')
    <li>
        <a href="/admin/mesej" aria-expanded="false">
            <svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M3 8l7.89 5.26a2 2 0 002.22 0L21 8M5 19h14a2 2 0 002-2V7a2 2 0 00-2-2H5a2 2 0 00-2 2v10a2 2 0 002 2z"></path>
            </svg>
            <span> Mesej Pelanggan</span>
        </a>

    </li>
    @endcan

    <li>
        @can('lihat-anggota')
            <a href="#" aria-expanded="false">
                <svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 4.354a4 4 0 110 5.292M15 21H3v-1a6 6 0 0112 0v1zm0 0h6v-1a6 6 0 00-9-5.197M13 7a4 4 0 11-8 0 4 4 0 018 0z"></path>
                </svg>
                <span> Keanggotaaan</span>
                <span class="menu-arrow"></span>
            </a>
        @endcan
        <ul class="nav-second-level mm-collapse" aria-expanded="false">


            @can('lihat-anggota')
                <li>
                    <a href="{{route('admin.member.list')}}">
                        <span> Senarai Semua </span>
                    </a>
                </li>
            @endcan
            @can('lihat-anggota')
                <li>
                    <a href="{{route('admin.member.ikoop')}}">
                        <span> Senarai Semua (IKOOP) </span>
                    </a>
                </li>
            @endcan
            @can('lihat-anggota')
                <li>
                    <a href="{{route('admin.member.termination')}}">
                        <span> Senarai Berhenti </span>
                    </a>
                </li>
            @endcan
        </ul>
    </li>

    <li>
        @can('lihat-anggota')
        <a href="#" aria-expanded="false">
            <svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 5H7a2 2 0 00-2 2v12a2 2 0 002 2h10a2 2 0 002-2V7a2 2 0 00-2-2h-2M9 5a2 2 0 002 2h2a2 2 0 002-2M9 5a2 2 0 012-2h2a2 2 0 012 2m-3 7h3m-3 4h3m-6-4h.01M9 16h.01"></path>
            </svg>
            <span> Pendaftaran</span>
            <span class="menu-arrow"></span>
        </a>
        @endcan
        <ul class="nav-second-level mm-collapse" aria-expanded="false">
            @can('tambah-anggota')
            <li>
                <a href="{{route('admin.member.create')}}">
                    <span data-toggle="tooltip" title="Daftarkan Anggota" data-placement="right">Manual</span>
                </a>
            </li>
            @endcan
            @can('lihat-anggota')
            <li>
                <a href="{{route('admin.member.pending_update')}}">
                    <span>Belum Dikemaskini</span>
                </a>
            </li>
            @endcan
            @can('lihat-anggota')
            <li>
                <a href="{{route('admin.member.new')}}">
                    <span>Untuk Disemak</span>
                </a>
            </li>
            @endcan
            @can('lihat-anggota')
            <li>
                <a href="{{route('admin.member.reviewed')}}">
                    <span>Untuk Diluluskan</span>
                </a>
            </li>
            @endcan
            @can('lihat-anggota')
            <li>
                <a href="{{route('admin.member.active')}}">
                    <span>Telah Diluluskan</span>
                </a>
            </li>
            @endcan
            @can('lihat-anggota')
            <li>
                <a href="{{route('admin.member.no-mykad')}}">
                    <span>Tiada Lampiran MYKAD</span>
                </a>
            </li>
            @endcan

        </ul>
    </li>


    <li>
        <a href="#">
            <svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M17 9V7a2 2 0 00-2-2H5a2 2 0 00-2 2v6a2 2 0 002 2h2m2 4h10a2 2 0 002-2v-6a2 2 0 00-2-2H9a2 2 0 00-2 2v6a2 2 0 002 2zm7-5a2 2 0 11-4 0 2 2 0 014 0z"></path>
            </svg>
            <span> Transaksi Bayaran</span>
            <span class="menu-arrow"></span>
        </a>
        <ul class="nav-second-level mm-collapse" aria-expanded="false">
{{--            @can('lihat-transaksi')--}}
{{--            <li>--}}
{{--                <a href="/admin/tx">--}}

{{--                    <span> Bayaran Online</span>--}}
{{--                </a>--}}
{{--            </li>--}}
{{--            @endcan--}}
            @can('lihat-transaksi')
            <li>
                <a href="/admin/tx/manual">
                    <span> Untuk Disemak <br>(Bayaran Manual)</span>
                </a>
            </li>
            @endcan
            @can('lihat-transaksi')
                <li>
                    <a href="{{route('admin.transaksi-bayaran.untuk-semakan-lanjut.index')}}">
                        <span> Untuk Semakan Lanjut</span>
                    </a>
                </li>
            @endcan
            @can('lihat-transaksi')
                <li>
                    <a href="/admin/tx/approval">
                        <span> Untuk Disahkan</span>
                    </a>
                </li>
            @endcan
            @can('lihat-transaksi')
            <li>
                <a href="/admin/tx/verified">
                    <span data-toggle="tooltip" data-placement="top" title="" data-original-title="Bayaran Manual/Bayaran Online"> Telah Disahkan <br>(Resit Bayaran)</span>
                </a>
            </li>
            @endcan
            @can('lihat-transaksi')
                <li>
                    <a href="{{route('admin.transaksi-bayaran.telah-dibatalkan.index')}}">
                        <span> Telah Dibatalkan</span>
                    </a>
                </li>
            @endcan



        </ul>
    </li>
    @if(env('ENABLE_AFFILIATES',false))
    <li>
        <a href="#">
            <svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 15l-2 5L9 9l11 4-5 2zm0 0l5 5M7.188 2.239l.777 2.897M5.136 7.965l-2.898-.777M13.95 4.05l-2.122 2.122m-5.657 5.656l-2.12 2.122"></path>
            </svg>
            <span>Affiliates</span>
            <span class="menu-arrow"></span>
        </a>
        <ul class="nav-second-level mm-collapse">
{{--            <li>--}}
{{--                <a href="/admin/affiliates/utama">--}}

{{--                    <span>Utama</span>--}}
{{--                </a>--}}
{{--            </li>--}}
            <li>
                <a href="{{route('affiliates.settings.index')}}">

                    <span>Tetapan Affiliate</span>
                </a>
            </li>
{{--            <li class="menu-title" style="padding-left:20px"><b>Permohonan</b></li>--}}
            <li>
                <a href="/admin/affiliates/senarai-permohonan">

                    <span>Senarai Permohonan</span>
                </a>
            </li>
            {{-- <li>
                <a href="/admin/affiliates/senarai-semua">

                    <span>Senarai Semua</span>
                </a>
            </li> --}}
{{--            <li class="menu-title" style="padding-left:20px"><b>Rekod Komisen</b></li>--}}
            <li>
                <a href="/admin/affiliates/transaksi">

                    <span>Senarai Rekod Komisen</span>
                </a>
            </li>

{{--            <li class="menu-title" style="padding-left:20px"><b>Pengeluaran</b></li>--}}
            <li>
                <a href="/admin/affiliates/pengeluaran">

                    <span>Senarai Pengeluaran</span>
                </a>
            </li>

        </ul>
    </li>
    @endif
    <li>
        @can('edit-mod-penyelenggaraan')
        <a href="#">
            <svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 6V4m0 2a2 2 0 100 4m0-4a2 2 0 110 4m-6 8a2 2 0 100-4m0 4a2 2 0 110-4m0 4v2m0-6V4m6 6v10m6-2a2 2 0 100-4m0 4a2 2 0 110-4m0 4v2m0-6V4"></path>
            </svg>
            <span> Tetapan Admin</span>
            <span class="menu-arrow"></span>

            <ul class="nav-second-level mm-collapse">
                @can('edit-mod-penyelenggaraan')
                <li>
                    <a href="/admin/maintenance">

                        <span> Mod Penyelenggaraan</span>
                    </a>
                </li>
                @endcan

                @can('lihat-role')
                <li>
                    <a href="/admin/roles">

                        <span>Roles</span>
                    </a>
                </li>
                @endcan
                @can('lihat-permission')
                <li>
                    <a href="/admin/permissions">

                        <span>Permissions</span>
                    </a>
                </li>
                @endcan
                @can('lihat-administrator')
                <li>
                    <a href="/admin/administrators">

                        <span>Administrators</span>
                    </a>
                </li>
                @endcan
                @can('lihat-pengumuman')
                <li>
                    <a href="/admin/announcements">

                        <span>Pengumuman</span>
                    </a>
                </li>
                @endcan
                <li>
                    <a href="/admin/logs">

                        <span>Log Aktiviti</span>
                    </a>
                </li>
            </ul>
        </a>
        @endcan
    </li>

    <li>
        {{-- @can('edit-mod-penyelenggaraan')--}}
        <a href="#">
            <svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 11H5m14 0a2 2 0 012 2v6a2 2 0 01-2 2H5a2 2 0 01-2-2v-6a2 2 0 012-2m14 0V9a2 2 0 00-2-2M5 11V9a2 2 0 012-2m0 0V5a2 2 0 012-2h6a2 2 0 012 2v2M7 7h10"></path>
            </svg>
            <span> Migrasi Ikoop</span>
            <span class="menu-arrow"></span>

            <ul class="nav-second-level mm-collapse">
                {{-- @can('edit-mod-penyelenggaraan')--}}
                <li>
                    <a href="/admin/migrations/duplicate-users">

                        <span>Duplicate Users</span>
                    </a>
                </li>
                {{-- @endcan--}}


            </ul>
        </a>
        {{-- @endcan--}}
    </li>





    @if(env('ENABLE_LETTER_MODULE',false))
    <li>
        <a href="#">
            <svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 12h6m-6 4h6m2 5H7a2 2 0 01-2-2V5a2 2 0 012-2h5.586a1 1 0 01.707.293l5.414 5.414a1 1 0 01.293.707V19a2 2 0 01-2 2z"></path>
            </svg>
            <span>Surat</span>
            <span class="menu-arrow"></span>
        </a>
        <ul class="nav-second-level mm-collapse">
            <li>
                <a href="/admin/letter/utama">

                    <span>Senarai Kategori Surat</span>
                </a>
            </li>
            <li>
                <a href="/admin/letter/create">

                    <span>Tambah Surat</span>
                </a>
            </li>
            <li>
                <a href="/admin/letter/send">

                    <span>Hantar Surat</span>
                </a>
            </li>
            <li>
                <a href="/admin/letter/jobs">

                    <span>Status Penghantaran</span>
                </a>
            </li>
        </ul>
    </li>
    @endif
    <li>
        <a href="/admin/sms">
            <svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M8 10h.01M12 10h.01M16 10h.01M9 16H5a2 2 0 01-2-2V6a2 2 0 012-2h14a2 2 0 012 2v8a2 2 0 01-2 2h-5l-5 5v-5z"></path></svg>
            <span> Hantar SMS</span>
        </a>
    </li>


    @if(env('ENABLE_DIVIDEN_MODULE',true))
    <li>
        <a href="#">
            <svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 5H7a2 2 0 00-2 2v12a2 2 0 002 2h10a2 2 0 002-2V7a2 2 0 00-2-2h-2M9 5a2 2 0 002 2h2a2 2 0 002-2M9 5a2 2 0 012-2h2a2 2 0 012 2m-3 7h3m-3 4h3m-6-4h.01M9 16h.01"></path>
            </svg>
            <span>Dividen</span>
            <span class="menu-arrow"></span>
        </a>
        <ul class="nav-second-level mm-collapse">
            <li>
                <a href="/admin/dividen/utama">

                    <span>Utama</span>
                </a>
            </li>
            <li>
                <a href="/admin/dividen/create">

                    <span>Tetapan Dividen</span>
                </a>
            </li>
            <li>
                <a href="/admin/dividen/withdraw">

                    <span>Pengeluaran</span>
                </a>
            </li>
        </ul>
    </li>
    @endif


    <li>
        <a href="#">
            <svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 17v-2m3 2v-4m3 4v-6m2 10H7a2 2 0 01-2-2V5a2 2 0 012-2h5.586a1 1 0 01.707.293l5.414 5.414a1 1 0 01.293.707V19a2 2 0 01-2 2z"></path>
            </svg>
            <span> Laporan</span>
            <span class="menu-arrow"></span>
        </a>
        <ul class="nav-second-level mm-collapse" aria-expanded="false">


            <li>
                <a href="/admin/reports">

                    <span> Jana Laporan (Senarai)</span>
                </a>
            </li>

            <li>
                <a href="{{route('report.summary.index')}}">

                    <span> Ringkasan Laporan</span>
                </a>
            </li>

              <li>
                <a href="{{route('report.audit.index')}}">

                    <span>Laporan Audit</span>
                </a>
            </li>



        </ul>
    </li>
</ul>
