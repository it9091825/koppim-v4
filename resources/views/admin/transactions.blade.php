@extends('admin.layouts')
@pagetitle(['title'=>'Bayaran Online','links' => ['Transaksi Bayaran']])@endpagetitle
@section('content')
<div class="container">
    <div class="row" style="">
        @include('include.search',['url'=>url()->current(),'placeholder'=>'carian menggunakan nama, no. telefon atau no. mykad'])
    </div>
    <div class="row ">
        <div class="col-lg-12">
            <div class="d-flex justify-content-between align-items-center">
                <div class="d-flex justify-content-center align-items-center mb-3">
                    @if($orgs)
                    Paparan {{number_format($orgs->firstItem(),0,"",",")}} Hingga {{ number_format($orgs->lastItem(),0,"",",")}} Daripada {{number_format($orgs->total(),0,"",",")}} Dalam Senarai
                    @endif
                </div>
                <div class="d-flex justify-content-center align-items-center">
                    {{ $orgs->links() }}
                </div>
            </div>
        </div>
        <div class="col-lg-12 col-xl-12 mg-t-12">
            <div class="card">

                <div class="card-body px-4 py-0">
                    <div class="table-responsive">
                        <table class="table">
                            <thead class="thead-primary">
                                <tr>
                                    <th>NO. RUJUKAN</th>
                                    <th>NAMA</th>
                                    <th class="text-center">JUMLAH DIBAYAR</th>
                                    <th class="text-center">JUMLAH SYER</th>
                                    <th class="text-center">TARIKH BAYARAN</th>
                                    <th class="text-center">TINDAKAN</th>
                                </tr>
                            </thead>
                            @forelse($orgs as $key => $org)
                            <tbody>
                                <tr>
                                    <td><a target="_blank" href="https://app.senangpay.my/payment/receipt/{{ $org->ord_key }}">{{ $org->order_ref }}</a></td>
                                    <td><a target="_blank" href="{{url('admin/members?search='.$org->name)}}">{{ $org->name }} </a></td>
                                    <td class="text-center">
                                        RM {{ number_format($org->amount,2) }}
                                    </td>
                                    <td class="text-center">
                                        RM {{ number_format($org->share_amount,2) }}
                                    </td>
                                    <td class="text-center">{{$org->created_at->format('d/m/Y h:i:sa')}}</td>
                                    <td class="text-center"><a href="#" class="row-toggle" data-row-counter="{{$key}}" data-toggle="tooltip" data-placement="auto" data-original-title="Papar maklumat lanjut"><svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 9v3m0 0v3m0-3h3m-3 0H9m12 0a9 9 0 11-18 0 9 9 0 0118 0z"></path>
                                            </svg></a>
                                    </td>
                                </tr>
                            </tbody>
                            <tbody class="mb-3" id="row{{$key}}" style="display:none;">
                                <tr class="bg-gray-200">
                                    <td class="font-bold">NO. MYKAD</td>
                                    <td colspan="10"><a target="_blank" href="{{url('admin/members?search='.$org->ic)}}">{{ $org->ic }}</a></td>
                                </tr>
                                <tr class="bg-gray-200">
                                    <td class="font-bold">NO. TELEFON</td>
                                    <td colspan="10">{{ $org->phone }}</td>
                                </tr>
                                <tr class="bg-gray-200">
                                    <td class="font-bold">E-MEL</td>
                                    <td colspan="10">{{ $org->email }}</td>
                                </tr>
                                <tr class="bg-gray-200">
                                    <td class="font-bold">GATEWAY</td>
                                    <td colspan="10">{{ $org->gateway }}</td>
                                </tr>
                                <tr class="bg-gray-200">
                                    <td class="font-bold">CONFIRMATION CODE</td>
                                    <td colspan="10">{{ $org->ord_key }}</td>
                                </tr>
                            </tbody>

                            @empty
                            <tbody>
                                <td colspan="10" class="text-center font-weight-bold text-uppercase">
                                    <h3>tiada rekod</h3>
                                </td>
                            </tbody>

                            @endforelse


                        </table>


                    </div>


                </div><!-- card-body -->
            </div><!-- card -->
        </div>


    </div>




</div><!-- container -->


@endsection

@section('js')

<script>
    $(".row-toggle").click(function(e) {
        e.preventDefault();
        var rowid = $(this).attr("data-row-counter");
        $('#row' + rowid).toggle();
    });

    function search_data() {
        var search = $('#search').val();

        window.location.href = "{{route('admin.transaction.online.index')}}?search=" + search + "";
    }

    $(document).on("keypress", ".input-group:has(input:input, span.input-group-btn:has(.btn)) input:input", function(e) {
        if (e.which == 13) {
            $(this).closest(".input-group").find(".input-group-append > .submit").click();
        }
    });

</script>

@endsection
