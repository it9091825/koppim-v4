@extends('admin.layouts')
@pagetitle(['title'=>"Edit Admin - ".$admin->name,'links' => ['Tetapan Admin','Admin']])@endpagetitle
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <a href="/admin/administrators">Kembali</a>
        </div>
    </div>
    <div class="row mt-3">
        <div class="col-md-6">
            <div class="card-box">
            <h5>Kemaskini Maklumat {!! $admin->name !!}</h5>
                <hr>
                @can('edit-administrator')
                <form action="{{route('administrators.update',['id' => $admin->id])}}" method="POST" enctype="multipart/form-data">
                    {{csrf_field()}}
                    @endcan
                    <div class="form-group">
                        <label for="" class="form-label">Nama</label>
                        <input type="text" name="name" id="" class="form-control" value="{{old('name', $admin->name ?? null)}}">
                    </div>
                    <div class="form-group">
                        <label for="" class="form-label">E-mel</label>
                        <input type="text" name="email" id="" class="form-control" value="{{old('email', $admin->email ?? null)}}">

                    </div>
                    @can('edit-administrator')
                    <div class="form-group">
                        <label for="" class="form-label">Kata Laluan</label>
                        <input type="password" name="password" id="" class="form-control">

                    </div>
                    <div class="form-group">
                        <label for="" class="form-label">Ulang Kata Laluan</label>
                        <input type="password" name="password_confirmation" id="" class="form-control">
                    </div>
                    @endcan
                    @if($roles->count())
                    <div class="form-group">
                        <label for="" class="form-label">Role</label>
                        <select name="role_id[]" id="" class="form-control select2" multiple>
                            <option value="">Sila Pilih</option>
                            @foreach($roles as $role)
                            <option value="{{$role->id}}" {{in_array($role->slug, $admin->roles->pluck('slug')->toArray()) ? 'selected':''}}>{{$role->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    @endif
                    @can('edit-administrator')
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">Simpan Perubahan</button>
                    </div>
                </form>
                @endcan
            </div>
        </div>
        <div class="col-md-6">
            <div class="card-box">
                <h5>Permission Individu - {!!$admin->name!!}</h5>
                <hr>
                @can('edit-administrator')
                <form action="{{route('users.permission.store',['user_id' => $admin->id])}}" class="" enctype="multipart/form-data" method="POST">
                    {{csrf_field()}}
                    @endcan
                    <div class="form-group">
                        <div class="row mb-3">
                            <div class="col-12">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" id="checkAll" class="custom-control-input">
                                    <label for="checkAll" class="custom-control-label">
                                        Tanda Semua
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            @foreach($permissions as $permission)
                            <div class="col-4">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" value="{{$permission->id}}" name="permission[]" {{in_array($permission->id, $admin->permissions->pluck('id')->toArray()) ? 'checked':''}} class="custom-control-input" id="{{$permission->slug}}">
                                    <label for="{{$permission->slug}}" class="custom-control-label" data-toggle="tooltip" data-placement="top" title="{!! $permission->description !!}">
                                        {{$permission->name}}
                                    </label>
                                </div>
                            </div>
                            @endforeach
                        </div>

                    </div>
                    @can('edit-administrator')
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary">Simpan Perubahan</button>
                    </div>
                </form>
                @endcan
            </div>
        </div>
    </div>
</div>
@endsection



@section('js')
<script>
    $('.select2').select2();

    $("#checkAll").click(function() {
        $('input:checkbox').not(this).prop('checked', this.checked);
    });

    $('.deleteAdmin').on('click', function(e) {
        e.preventDefault();
        var confirmation = confirm('Continue delete this admin? This action is cannot be undone.');

        if (confirmation) {
            this.submit();
        } else {
            alert('Delete operation aborted.');
        }
    });

</script>
@endsection
