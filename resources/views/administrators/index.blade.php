@extends('admin.layouts')
@pagetitle(['title'=>"Tambah Admin Baharu",'links' => ['Tetapan Admin']])@endpagetitle
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header bg-white">
                    <h5 class="card-title">Tambah Admin Baharu</h5>
                </div>
                <div class="card-body">
                    @if(isset($admin))
                    <form action="{{route('administrators.update',['id' => $admin->id])}}" method="POST" enctype="multipart/form-data">
                        {{method_field('PUT')}}
                        @else
                        <form action="{{route('administrators.store')}}" method="POST" enctype="multipart/form-data">
                            @endif

                            {{csrf_field()}}
                            <div class="form-group">
                                <label for="" class="form-label">Nama</label>
                                <input type="text" name="name" id="" class="form-control" value="{{old('name', $admin->name ?? null)}}">

                            </div>
                            <div class="form-group">
                                <label for="" class="form-label">E-mel</label>
                                <input type="text" name="email" id="" class="form-control" value="{{old('email', $admin->email ?? null)}}">

                            </div>
                            <div class="form-group">
                                <label for="" class="form-label">Kata Laluan</label>
                                <input type="password" name="password" id="" class="form-control">

                            </div>
                            <div class="form-group">
                                <label for="" class="form-label">Ulang Kata Laluan</label>
                                <input type="password" name="password_confirmation" id="" class="form-control">
                            </div>
                            @if($roles->count())
                            <div class="form-group">
                                <label for="" class="form-label">Role</label>
                                <select name="role_id[]" id="" class="form-control select2" multiple>
                                    <option value="">Sila Pilih</option>
                                    @foreach($roles as $role)
                                    <option value="{{$role->id}}">{{$role->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            @endif
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Simpan Perubahan</button>
                            </div>
                        </form>

                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card">
                <div class="card-header bg-white">
                    <h5 class="card-title">Senarai Admin</h5>
                </div>
                <table class="table card-table table-vcenter">
                    <thead>
                        <tr>
                            <th>Nama</th>
                            <th>Role</th>
                            <th class="text-center">Tindakan</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($admins as $key=>$admin)
                        <tr>
                            <td>{{$admin->name}}</td>
                            <td>{{collect($admin->roles)->implode('name',', ')}}</td>
                            <td class="d-flex justify-content-center align-items-center space-x-2">
                                @can('lihat-administrator')
                                <a href="{{route('administrators.edit',['id'=>$admin->id])}}">{{auth()->user()->can('edit-administrator') ? 'Edit':'Lihat'}}</a>
                                @endcan
                                @can('hapus-administrator')
                                <form action="{{route('administrators.destroy',['id'=>$admin->id])}}" method="POST" class="deleteAdmin">
                                    {{csrf_field()}}
                                    {{method_field('DELETE')}}
                                    <a href="#" class="text-danger">Padam</a>
                                </form>
                                @endcan
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                {{ $admins->links() }}
            </div>
        </div>
    </div>
</div>
@endsection



@section('js')
<script>
    $('.select2').select2();

    $('.deleteAdmin').on('click', function(e) {
        e.preventDefault();
        var confirmation = confirm('Continue delete this admin? This action is cannot be undone.');

        if (confirmation) {
            this.submit();
        } else {
            alert('Delete operation aborted.');
        }
    });

</script>
@endsection
