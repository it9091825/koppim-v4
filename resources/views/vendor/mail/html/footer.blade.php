<tr>
    <td>
        <table class="footer" align="center" width="570" cellpadding="0" cellspacing="0" role="presentation">
            <tr>
                <td class="content-cell" align="center">
                    {{-- {{ Illuminate\Mail\Markdown::parse($slot) }} --}}
                    <b>Koperasi Pembangunan Pengguna Islam Malaysia Berhad</b><br>
                    KoPPIM @ Raudhah City<br> Jalan SP 10/1, Selangor Cyber Valley<br> 63000 Cyberjaya, Selangor Darul Ehsan.<br>
                    anggota@koppim.com.my <br>
                    Talian Panggilan: +603-84081878 <br>
                    Talian Mesej WhatsApp: +6016-2829075 <br>
                    http://infokoppim.wasap.my
                </td>
            </tr>
        </table>
    </td>
</tr>
