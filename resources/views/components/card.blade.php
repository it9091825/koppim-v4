<div class="card">
    <div class="card-header text-dark font-weight-bold text-uppercase">
        {{$title ?? ''}}
    </div><!-- card-header -->
    <div class="card-body">
        {{ $slot }}
    </div><!-- card-body -->
</div>
