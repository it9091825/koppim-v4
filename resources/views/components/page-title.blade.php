@section('header')
<div class="container">
    <div class="row page-title">
        <div class="col-md-12">
            @if(isset($links))
            <nav aria-label="breadcrumb" class="mt-1 mb-2">
                <ol class="breadcrumb">

                    @foreach($links as $link)
                    <li class="breadcrumb-item">{{$link}}</li>
                    @endforeach
                    <li class="breadcrumb-item">{{$title}}</li>
                </ol>
            </nav>
            @endif
            <h3 class="mb-1 mt-0 text-uppercase">{{$title}}</h3>
        </div>
    </div>
</div>
@endsection
