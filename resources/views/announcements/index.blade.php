@extends('admin.layouts')
@section('css')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/jquery-datetimepicker@2.5.21/jquery.datetimepicker.css">
@endsection
@section('content')
<div class="container">
    <div class="row mt-5">
        @can('tambah-pengumuman')
        <div class="col-md-4">
            <div class="card">
                <div class="card-header bg-white">
                    <h5 class="card-title">
                        Cipta Pengumuman
                    </h5>
                </div>
                <div class="card-body">
                    <form action="{{route('announcements.store')}}" method="POST" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="form-group">
                            <label for="" class="form-label">Tajuk</label>
                            <input type="text" name="title" id="" class="form-control"
                                value="{{old('title', $announcement->title ?? null)}}">

                        </div>
                        <div class="form-group">
                            <label for="" class="form-label">Mesej</label>
                            <textarea type="text" name="message" id=""
                                class="form-control">{{old('message', $announcement->message ?? null)}}</textarea>

                        </div>

                        <div class="form-group">
                            <label for="" class="form-label">Mula Pada</label>
                            <input type="text" name="published_from" id="published_from" class="form-control"
                                value="{{old('published_from', $announcement->published_from ?? null)}}">
                            <small class="text-danger">The date you want this announcement to first appear must be
                                unique.</small>
                        </div>
                        <div class="form-group">
                            <label for="" class="form-label">Tamat Pada</label>
                            <input type="text" name="published_until" id="published_until" class="form-control"
                                value="{{old('published_until', $announcement->published_until ?? null)}}">
                            <small class="text-danger">The date for this announcement to end must be unique.</small>
                        </div>
                        <div class="form-group">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="customCheck2" name="send_email">
                            <label class="custom-control-label" for="customCheck2">Hantar Pengemuman Kepada Semua Anggota Melalui Emel?</label>
                        </div>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Tambah</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        @endcan
        @can('lihat-pengumuman')
        <div class="col-md-8">
            <div class="card">
                <div class="card-header bg-white">
                    <h5 class="card-title">Senarai Pengumuman</h5>
                </div>
                <table class="table card-table table-vcenter">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Tajuk</th>

                            <th>Mula Pada</th>
                            <th>Tamat Pada</th>
                            <th>Tindakan</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if($announcements->count())
                        @foreach($announcements as $key=>$announcement)
                        <tr>
                            <td>{{++$key}}</td>
                            <td><span data-toggle="tooltip"
                                    title="Message: {!! $announcement->message!!}">{{ $announcement->title }}</span>
                            </td>
                            <td>{{Carbon\Carbon::parse($announcement->published_from)->format('d F Y')}}</td>
                            <td>{{Carbon\Carbon::parse($announcement->published_until)->format('d F Y')}}</td>
                            <td class="d-flex items-align-center justify-content-center">
                                <a href="{{route('announcements.edit',['id' => $announcement->id])}}"><i
                                        data-feather="edit" style="width:16px;"></i>
                                </a>
                                <form action="{{route('announcements.destroy',['id'=>$announcement->id])}}"
                                    method="POST" class="deleteAnnouncement">
                                    {{csrf_field()}}
                                    {{method_field('DELETE')}}
                                    <a href="#" class="text-danger"><i data-feather="trash" style="width:16px;"></i></a>
                                </form>

                            </td>
                        </tr>
                        @endforeach
                        @else
                        <tr>
                            <td colspan="10" class="text-center">
                                <h4>TIADA REKOD</h4>
                            </td>
                        </tr>
                        @endif
                    </tbody>
                </table>
                {{ $announcements->links() }}
            </div>
        </div>
        @endcan
    </div>
</div>
@endsection

@section('js')
<script src="https://cdn.jsdelivr.net/npm/jquery-datetimepicker@2.5.21/build/jquery.datetimepicker.full.min.js"
    integrity="sha256-JADorIjphY5ztERiDARizkSMD2NW4HpsDlAyageYtFc=" crossorigin="anonymous"></script>

<script>

    jQuery('#published_from, #published_until').datetimepicker({
        datepicker:true,
        timepicker:false,
        format:'Y-m-d',
        minDate:0
    });

    $('.deleteAnnouncement').on('click',function(e){
    e.preventDefault();
    var confirmation = confirm('Continue delete this announcement? This action is cannot be undone.');

    if(confirmation){
        this.submit();
    }else{
        alert('Delete operation aborted.');
    }
});


$(function() {

    $('input[name="published_from"], input[name="published_until"]').daterangepicker({
        singleDatePicker: true,
        locale:{
            format:'DD MMM YYYY'
        }
    });

});

</script>
@endsection
