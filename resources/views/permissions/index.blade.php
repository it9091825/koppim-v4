@extends('admin.layouts')
@pagetitle(['title'=>"Tambah Permission Baharu",'links' => ['Tetapan Admin']])@endpagetitle
@section('content')
<div class="container">
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-header bg-white">
                    <h5 class="card-title">Tambah Permission Baharu</h5>
                </div>
                <div class="card-body">
                    @can('tambah-permission')
                    <form action="{{route('permissions.store')}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        @endcan
                        <div class="form-group">
                            <label for="" class="form-label">Nama</label>
                            <input type="text" name="permission_name" id="" class="form-control" value="{{old('permission_name', $permission->name ?? null)}}">

                        </div>
                        <div class="form-group">
                            <label for="" class="form-label">Keterangan</label>
                            <textarea name="permission_description" id="" class="form-control">{{old('permission_description', $permission->description ?? null)}}</textarea>

                        </div>
                        @can('tambah-permission')
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Simpan Perubahan</button>
                        </div>
                        @endcan
                    </form>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card">
                <div class="card-header bg-white">
                    <h5 class="card-title">Senarai Permission</h5>
                </div>
                <table class="table card-table table-striped table-vcenter">
                    <thead>
                        <tr>
                            <th>Nama</th>
                            <th>Keterangan</th>
                            <th class="text-center">Tindakan</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if($permissions->count())
                        @foreach($permissions as $key=>$permission)
                        <tr>
                            <td>{{$permission->name}}</td>
                            <td>{!! $permission->description !!}</td>
                            <td class="d-flex justify-content-center align-items-center space-x-2">
                                @can('lihat-permission')
                                <a href="{{route('permissions.edit',['id' => $permission->id])}}">EDIT</a>
                                @endcan
                                @can('hapus-permission')
                                <form action="{{route('permissions.destroy',['id'=>$permission->id])}}" method="POST" class="deletePermissions">
                                    {{csrf_field()}}
                                    {{method_field('DELETE')}}
                                    <a href="#" class="text-danger">HAPUS</a>

                                </form>
                                @endcan

                            </td>
                        </tr>
                        @endforeach
                        @else
                        <tr>
                            <td colspan="4" class="text-center">
                                TIADA REKOD
                            </td>
                        </tr>
                        @endif
                    </tbody>
                </table>

                <div class="d-flex align-items-center justify-content-center mt-4">
                    {{ $permissions->links() }}
                </div>

            </div>
        </div>
    </div>
</div>
@endsection



@section('js')
<script>
    $('.deletePermissions').on('click', function(e) {
        e.preventDefault();
        var confirmation = confirm('Continue delete this permissions? This action is cannot be undone.');

        if (confirmation) {
            this.submit();
        } else {
            alert('Delete operation aborted.');
        }
    });

</script>
@endsection
