@extends('admin.layouts')
@pagetitle(['title'=>"Edit Permission - ".$permission->name,'links' => ['Tetapan Admin','Permission']])@endpagetitle
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <a href="{{route('permissions.index')}}">Kembali</a>
        </div>
    </div>
    <div class="row mt-3">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header bg-white">
                    <h5 class="card-title">Permission - {!! $permission->name !!}</h5>
                </div>
                <div class="card-body">
                    <form action="{{route('permissions.update',['id' => $permission->id])}}" method="POST" enctype="multipart/form-data">

                        {{csrf_field()}}
                        <div class="form-group">
                            <label for="" class="form-label">Nama</label>
                            <input type="text" name="permission_name" id="" class="form-control" value="{{old('permission_name', $permission->name ?? null)}}">
                        </div>
                        <div class="form-group">
                            <label for="" class="form-label">Keterangan</label>
                            <textarea name="permission_description" id="" class="form-control">{{old('permission_description', $permission->description ?? null)}}</textarea>

                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Simpan Perubahan</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
