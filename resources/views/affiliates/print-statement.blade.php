<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <title>PENYATA KOMISEN</title>

    <style>
        body {
            font-family: Poppins, sans-serif !important;
        }

        * {
            margin: 0;
            padding: 0;
        }

        body {
            font: 14px/1.4 'Nunito', sans-serif;
        }

        #page-wrap {
            width: 800px;
            margin: 0 auto;
        }

        textarea {
            border: 0;
            font: 14px Helvetica, Sans-Serif;
            overflow: hidden;

        }

        table {
            border-collapse: collapse;
        }

        table td,
        table th {
            border: 1px solid black;
            padding: 5px;
        }

        #header {
            height: 15px;
            width: 100%;
            margin: 20px 0;
            background: #222;
            text-align: center;
            color: white;
            font: bold 15px Helvetica, Sans-Serif;
            text-decoration: uppercase;
            letter-spacing: 5px;
            padding: 8px 0px;
        }

        #identity {
            margin: 2%;
        }

        #address {
            width: 260px;
            height: 150px;
            float: right;
        }

        #employee {
            overflow: hidden;
        }

        .company-address {
            margin-top: 10px;
        }

        #logo {
            text-align: right;
            float: right;
            position: relative;
            border: 1px solid #fff;
            max-width: 540px;
            max-height: 100px;
            overflow: hidden;
        }


        #employee-details {
            float: left;
        }

        #employee-details-table {
            font-size: 12px;

        }

        .font-bold {
            font-weight: bold !important;
        }

        .text-center {
            text-align: center;
        }

        #items {
            width: 97%;
            border: 1px solid black;
            float: left;
            margin: 15px;
        }

        #items th {
            background: #eee;
        }

        #items tr.item-row td {
            border: 1px solid black;
            vertical-align: top;
            font-size: 12px;
        }

        footer {
            position: fixed;
            bottom: 0.3cm;
            left: 0cm;
            right: 1cm;
            height: 1cm;

            /** Extra personal styles **/

            color: black;
            text-align: right;

        }

    </style>
</head>

<body>

    <div id="page-wrap">
        <div class="text-center" style="margin-top:30px">

            <img src="{{ public_path() . '/img/logo-koppim-700px.jpg'}}" alt="" style="width:150px;">
            <p style="margin-top:10px;font-size:12px">Tarikh: {{\Carbon\Carbon::now()->format('d/m/Y H:m:s')}}</p>
            <p style="margin-top:3px;font-size:12px">Penyata ini telah dijana oleh sistem pada {{\Carbon\Carbon::now()->format('d/m/Y H:m:s')}}<br><b>Koperasi Pembangunan Pengguna Islam Malaysia Berhad (KoPPIM)
                    (W-5-1153)</b><br>70X, Jalan Keramat Hujung, Bukit Keramat, 54000 Kuala Lumpur.</p>
        </div>
        <div id="header">PENYATA KOMISEN BAGI JANUARI 2020</div>

        <div id="identity">
            <div id="employee-details">
                <table id="employee-details-table">
                    <tr>
                        <td class="font-bold">No. Anggota</td>
                        <td>{{$statement->no_anggota ?? 'TIADA'}}</td>
                    </tr>
                    <tr>
                        <td class="font-bold">Nama</td>
                        <td>{!! $statement->name !!}</td>
                    </tr>
                    <tr>
                        <td class="font-bold">No. MyKad</td>
                        <td>{{$statement->ic}}</td>
                    </tr>
                    <tr>
                        <td class="font-bold">Nama Bank</td>
                        <td>{{$statement->bank}}</td>
                    </tr>
                    <tr>
                        <td class="font-bold">Nama Akaun Bank</td>
                        <td>{{$statement->account_name}}</td>
                    </tr>
                    <tr>
                        <td class="font-bold">No. Akaun Bank</td>
                        <td>{{$statement->account_no}}</td>
                    </tr>
                </table>
            </div>
            <div id="address">
                <div class="company-address">
                    <p style="margin-bottom:10px;"><span class="font-bold">No. Rujukan:</span> {{$statement->reference_no}}</p>
                    <p style="margin-bottom:10px;"><span class="font-bold">Bulan Penyata:</span> {{ $statement->statement_date ? strtoupper($statement->statement_date->format('F Y')):''}}</p>
                    <p style="margin-bottom:10px;"><span class="font-bold">Status Bayaran:</span>
                        {{$statement->status == 'paid'?'SUDAH DIPROSES': ($statement->status == 'processing' ? 'DALAM PROSES' : 'DIBATALKAN')}}
                    </p>
                    <p style="margin-bottom:10px;"><span class="font-bold">Jumlah:</span>
                        RM{{number_format($statement->amount,2,'.',',')}}
                    </p>
                    <p style="margin-bottom:10px;"><span class="font-bold">Dicetak Pada:</span>
                        {{\Carbon\Carbon::now()->format('d/m/Y')}}
                    </p>

                </div>

            </div>
        </div>
        <br>
        @if($statement->status != 'cancelled')
        <table id="items" style="margin-top:150px;">
            <thead>
                <th class="">Bil.</th>
                <th class="">Tarikh &amp; Masa</th>
                <th class="">Pendaftar</th>
                <th class="">Status Pendaftaran</th>
                <th class="">Komisen</th>
                <th class="">Status Komisen</th>
            </thead>
            <tbody>
                @forelse($statement->payments as $index=>$payment)
                <tr class="item-row">
                    <td class="text-center"> {{$index+1}} </td>
                    <td class="text-center">{{$payment->created_at}}</td>
                    <td class="text-center">{!! $payment->name !!} <br>{{$payment->phone}}</td>
                    <td class="text-center">{!!Helper::anggota_status_text($payment->user->status)!!}</td>
                    <td class="text-center">@if($payment->introducer_commission == null)
                        <span class="tx-success">
                            -
                        </span>
                        @else
                        <span class="tx-success">
                            <i class="icon ion-android-add mg-r-5"></i>
                            RM {{ number_format($payment->introducer_commission,2) }}
                        </span>
                        @endif</td>
                    <td class="text-center">{!!Helper::commission_status_text($payment->introducer_commission_status)!!}</td>
                </tr>
                @empty
                <tr class="item-row">
                    <td colspan="10" style="text-align: center;font-weight:bold">TIADA REKOD</td>
                </tr>
                @endforelse

            </tbody>

        </table>
        @endif


    </div>
    <footer>
        <p style="margin-bottom:10px;"><span class="font-bold">Tarikh Muat Turun:</span> {{ date("d/m/Y H:i:s")}}</p>
    </footer>
    <script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
</body>

</html>
