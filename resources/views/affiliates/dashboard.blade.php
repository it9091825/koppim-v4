@extends('admin.layouts')
@section('css')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/jquery-datetimepicker@2.5.21/jquery.datetimepicker.css">
@endsection
@section('content')

<div class="container py-5 space-y-5">
    <div class="row d-none d-print-block">
        <div class="col">
            <div class="d-flex">
                <h3 class="text-4xl">{{\Carbon\Carbon::now()->format('d F Y H:m:s')}}</h3>
            </div>
        </div>
    </div>
    <div class="row align-items-center">
        <div class="col">
            <h3 class="text-2xl text-dark font-bold uppercase letter tracking-wider">Statistik semasa affiliate</h3>
        </div>
        <div class="col">
            <ul class="navbar-nav flex-row ml-auto d-flex align-items-center list-unstyled topnav-menu float-right mb-0">
                <li class="dropdown d-none d-lg-block">
                    <a class="dropdown-toggle mr-0 rounded-full p-2 text-gray-600 hover:text-gray-800 active:text-gray-800 focus:text-gray-800 align-items-center" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
                        <svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M10.325 4.317c.426-1.756 2.924-1.756 3.35 0a1.724 1.724 0 002.573 1.066c1.543-.94 3.31.826 2.37 2.37a1.724 1.724 0 001.065 2.572c1.756.426 1.756 2.924 0 3.35a1.724 1.724 0 00-1.066 2.573c.94 1.543-.826 3.31-2.37 2.37a1.724 1.724 0 00-2.572 1.065c-.426 1.756-2.924 1.756-3.35 0a1.724 1.724 0 00-2.573-1.066c-1.543.94-3.31-.826-2.37-2.37a1.724 1.724 0 00-1.065-2.572c-1.756-.426-1.756-2.924 0-3.35a1.724 1.724 0 001.066-2.573c-.94-1.543.826-3.31 2.37-2.37.996.608 2.296.07 2.572-1.065z"></path>
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 12a3 3 0 11-6 0 3 3 0 016 0z"></path>
                        </svg>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <!-- item-->
                        <a href="javascript:void;" class="dropdown-item notify-item" id="printImage">
                            Cetak halaman ini
                        </a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-3">
            <div class="card">
                <div class="card-body">
                    <div class="">
                        <h3 class="text-2xl">{{ $permohonan_baharu }}</h3>
                    </div>
                    <span class="text-muted">Permohonan Baharu</span>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-3">
            <div class="card">
                <div class="card-body">
                    <div class="">
                        <h3 class="text-2xl">{{ $jumlah_ejen }}</h3>
                    </div>
                    <span class="text-muted">Jumlah Ejen</span>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-3">
            <div class="card">
                <div class="card-body">
                    <div class="">
                        <h3 class="text-2xl">{{ $jumlah_komisen }}</h3>
                    </div>
                    <span class="text-muted">Jumlah Komisen</span>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-3">
            <div class="card">
                <div class="card-body">
                    <div class="">
                        <h3 class="text-2xl">RM {{ number_format($jumlah_nilai_komisen,2) }}</h3>
                    </div>
                    <span class="text-muted">Jumlah Nilai Komisen</span>
                </div>
            </div>
        </div>

    </div>
    <div class="row h-100">
        <div class="col-sm-12 col-md-3">
            <div class="card h-100">
                <div class="card-body">
                    <div class="">
                        <h3 class="text-2xl">{{ $transaksi_pengeluaran_baharu }}</h3>
                    </div>
                    <span class="text-muted">Permohonan Pengeluaran Baharu</span>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-3">
            <div class="card h-100 ">
                <div class="card-body">
                    <div class="">
                        <h3 class="text-2xl">{{ $transaksi_pengeluaran_batal }}</h3>
                    </div>
                    <span class="text-muted">Jumlah Batal Pengeluaran</span>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-3">
            <div class="card h-100 ">
                <div class="card-body">
                    <div class="">
                        <h3 class="text-2xl">{{ $transaksi_pengeluaran_lulus }}</h3>
                    </div>
                    <span class="text-muted">Jumlah Lulus Pengeluaran</span>
                </div>
            </div>
        </div>
        <div class="col-sm-12 col-md-3">
            <div class="card h-100 ">
                <div class="card-body">
                    <div class="">
                        <h3 class="text-2xl">RM {{ number_format($transaksi_pengeluaran_lulus_nilai,2,'.',',') }}</h3>
                    </div>
                    <span class="text-muted">Jumlah Lulus Nilai Pengeluaran</span>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('script')
<script>
    $('#printImage').on('click', function() {
        window.print();
    })

</script>
@endpush
