@extends('admin.layouts')
@section('css')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/jquery-datetimepicker@2.5.21/jquery.datetimepicker.css">
@endsection
@pagetitle(['title'=>$pageTitle,'links'=>['Affiliates']])@endpagetitle
@section('content')
<div class="container">
    <div class="row" style="">
        <div class="col-lg-6">
            <div class="input-group mb-3">
                <input type="text" id="search" class="form-control" value="{!! $search ?? ''!!}" placeholder="CARI DENGAN NAMA ATAU NO. MYKAD">
                <div class="input-group-append">
                    <button class="btn btn-primary font-weight-bold submit" onclick="search_data()">CARI</button>
                </div>
            </div>

        </div>
        <div class="col-lg-6 text-right">
            @can('jana-pengeluaran-komisen-affiliate')
            <a class="btn btn-danger font-weight-bold text-uppercase" href="{{'/admin/affiliates/pengeluaran/jana-pengeluaran'}}">Jana Pengeluaran</a>
            @endcan
            <a class="btn btn-success font-weight-bold text-uppercase" href="{{'/admin/affiliates/pengeluaran/export?status='.request()->get('status')}}">Eksport Senarai Kepada Excel</a>
        </div>
    </div>
    <div class="row ">
        @include('include.pagination',['results' => $withdraws])
    </div>
    <div class="row ">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                    @include('affiliates.withdrawal-category-tab')

                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th class="text-center">@sortablelink('created_at','Tarikh')</th>
                                    <th>@sortablelink('name','Nama Ejen')</th>
                                    <th>No. Rujukan</th>
                                    <th>Maklumat Bank</th>
                                    <th class="text-center">Status</th>
                                    <th class="text-center">Jumlah</th>
                                    <th class="text-center">Tindakan</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse($withdraws as $withdraw)
                                <tr>
                                    <td class="text-center">
                                        {!! $withdraw->created_at ? $withdraw->created_at->format("d/m/Y \<\b\\r\> h:i:sa") : ' ' !!}

                                    </td>
                                    <td>{{ $withdraw->name }}
                                        <br> <small>{{ $withdraw->ic }}</small>
                                        <br> <small>{{ $withdraw->no_anggota }}</small>
                                    </td>
                                    <td>
                                        {{$withdraw->reference_no}}
                                        <br> <small>Penyata: {{ $withdraw->statement_date ? strtoupper($withdraw->statement_date->format('F Y')) : ''}}</small>
                                    </td>
                                    <td>{!! $withdraw->account_name ?? '-' !!}<br>
                                        <small>{{ $withdraw->account_no }}</small>
                                        <br><small>{{ strtoupper($withdraw->bank) }}</small>
                                    </td>
                                    <td class="text-center">
                                        <span class="tx-info">
                                            {!!Helper::withdrawal_status_text($withdraw->status)!!}
                                        </span>
                                        @if($withdraw->amount > 0)
                                            <br><small>
                                                <span class='badge badge-info'>
                                                    SUDAH DIBAYAR
                                                </span>
                                            </small>
                                        @endif
                                    </td>
                                    <td class="text-center">RM {{ number_format($withdraw->amount,2,'.',',') }}</td>
                                    <td class="text-center">
                                        <a href="/admin/affiliates/pengeluaran/terperinci?id={{ $withdraw->_id }}" data-toggle="tooltip" title="Lihat rekod terperinci">

                                            <svg class="w-5 h-5" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 12a3 3 0 11-6 0 3 3 0 016 0z"></path>
                                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M2.458 12C3.732 7.943 7.523 5 12 5c4.478 0 8.268 2.943 9.542 7-1.274 4.057-5.064 7-9.542 7-4.477 0-8.268-2.943-9.542-7z"></path>
                                            </svg></a></td>
                                </tr>
                                @empty
                                @tablenoresult
                                @endtablenoresult
                                @endforelse
                            </tbody>
                        </table>

                    </div>
                </div><!-- card-body -->
            </div><!-- card -->
        </div>
    </div>
</div><!-- container -->

@endsection
@push('script')

<script>
    function search_data() {
        var search = $('#search').val();

        window.location.href = "{{url('/admin/affiliates/pengeluaran')}}?search=" + search + "";
    }

    $(document).on("keypress", ".input-group:has(input:input, span.input-group-btn:has(.btn)) input:input", function(e) {
        if (e.which == 13) {
            $(this).closest(".input-group").find(".input-group-append > .submit").click();
        }
    });

</script>
@endpush
