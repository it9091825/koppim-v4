<div class="row row-sm mg-t-20">
    <div class="col-lg-12">
        <div class="d-flex justify-content-between align-items-center">
            <div class="d-flex justify-content-center align-items-center mb-3">
                @if($transaksi)
                Paparan {{number_format($transaksi->firstItem(),0,"",",")}} Hingga {{ number_format($transaksi->lastItem(),0,"",",")}} Daripada {{number_format($transaksi->total(),0,"",",")}} Dalam Senarai
                @endif
            </div>
            <div class="d-flex justify-content-center align-items-center">
                {{ $transaksi->appends(request()->query())->links() }}
            </div>
        </div>
    </div>
    <div class="col-lg-12">
        <div class="card card-table">
            <div class="card-header">
                <div class="pull-left">
                    <div>
                        <h6 class="slim-card-title">PENDAFTARAN ANGGOTA</h6>
                    </div>
                    <div>
                        PELAWAT : <b>{{ $jumlah_pautan_unik_month }} </b>
                        | JUMLAH PENDAFTARAN : <b>{{ $transaksi_berjaya_count_month }}</b>
                        | JUMLAH KOMISEN : <b>RM {{ number_format($jumlah_commision_month,2) }}</b></div>
                </div>
                <div class="pull-right">
                    <div class="pull-left">
                        <select class="form-control" id="month" onchange="onchangemonth()">
                            <option value="all">SEMUA</option>
                            <option value="1">JANUARI</option>
                            <option value="2">FEBRUARI</option>
                            <option value="3">MAC</option>
                            <option value="4">APRIL</option>
                            <option value="5">MEI</option>
                            <option value="6">JUN</option>
                            <option value="7">JULAI</option>
                            <option value="8">OGOS</option>
                            <option value="9">SEPTEMBER</option>
                            <option value="10">OKTOBER</option>
                            <option value="11">NOVEMBER</option>
                            <option value="12">DISEMBER</option>
                        </select>
                    </div>
                    <div class="pull-right">
                        <select class="form-control" id="year" onchange="onchangemonth()" style="margin-left: 10px;">
                            <?php for($i = 2020;$i <= date('Y');$i++){ ?>
                            <option value="{{ $i }}">{{ $i }}</option>
                            <?php } ?>

                        </select>
                    </div>
                </div>
            </div><!-- card-header -->
            <div class="table-responsive">
                <table class="table mg-b-0 tx-13">
                    <thead>
                        <tr class="tx-10">
                            <th class="pd-y-5">@sortablelink('created_at', strtoupper('Tarikh & Masa'))</th>
                            <th class="pd-y-5 text-center">No. Mykad</th>
                            <th class="pd-y-5 text-center">Nama</th>
                            <th class="pd-y-5 text-center">No. Telefon</th>
                            <th class="pd-y-5 text-center">Alamat Emel</th>
                            <th class="pd-y-5 text-center">Status Pendaftaran</th>
                            <th class="pd-y-5 text-center">Komisen</th>
                            <th class="pd-y-5 text-center">Status Komisen</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse($transaksi as $trans)
                        <tr>
                            <td class="pd-l-20">
                                {{ $trans->created_at->translatedFormat('d M Y h:i') }}
                            </td>
                            <td class="text-center">
                                <a href="" class="tx-inverse tx-14 tx-medium d-block">K/P
                                    : {{ Helper::ccMasking($trans->ic,'X') }}</a>
                                <span class="tx-11 d-block">{{ $trans->gateway }}</span>
                            </td>
                            <td class="valign-middle text-center">
                                {{$trans->name}}
                            </td>
                            <td class="valign-middle text-center">
                                {{$trans->phone}}
                            </td>
                            <td class="valign-middle text-center">
                                {{$trans->email}}
                            </td>
                            <td class="valign-middle text-center">
                                <span class="tx-info">
                                    @if(isset($trans->user))
                                    {!! Helper::anggota_status_text($trans->user->status) ?? '' !!}
                                    @endif
                                </span>
                            </td>
                            @if($trans->introducer_commission == null)
                                <td class="valign-middle text-center">
                                    <span class="tx-success">
                                        -
                                    </span>
                                </td>
                                <td>
                                    -
                                </td>
                            @else
                                <td class="valign-middle text-center">
                                    <span class="tx-success">
                                        <i class="icon ion-android-add mg-r-5"></i>
                                        RM {{ number_format($trans->introducer_commission,2) }}
                                    </span>
                                </td>
                                <td>
                                    {!! Helper::commission_status_text($trans->introducer_commission_status) ?? '' !!}
                                </td>
                            @endif
                        </tr>
                        @empty
                        <tr>

                            <td colspan="8" align="center">
                                TIADA REKOD
                            </td>


                        </tr>
                        @endforelse


                    </tbody>
                </table>
            </div><!-- table-responsive -->

        </div><!-- card -->
    </div><!-- col-6 -->

</div><!-- row -->
