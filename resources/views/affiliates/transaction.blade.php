@extends('admin.layouts')
@section('css')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/jquery-datetimepicker@2.5.21/jquery.datetimepicker.css">
@endsection

@pagetitle(['title'=>$pageTitle,'links'=>['Affiliates']])@endpagetitle

@section('content')

<div class="container">
    <div class="row">

        @include('include.search',['url'=>'/admin/affiliates/transaksi','placeholder'=>'buat carian menggunakan nama atau no. mykad ejen'])

    </div>
    <div class="row ">

            @include('include.pagination',['results'=>$users])

        <div class="col-lg-12 col-xl-12">
            <div class="card" style="">
                <div class="card-body px-2 py-0">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th class="text-uppercase">@sortablelink('name','Nama Ejen')</th>
                                    <th class="text-center text-uppercase">No. MYKAD Ejen</th>
                                    <th class="text-center text-uppercase">No. Anggota</th>
                                    <th class="text-center text-uppercase">Jumlah Downline</th>
                                    <th class="text-center text-uppercase">Jumlah Komisen</th>
                                    <th class="text-center text-uppercase">Jumlah Pengeluaran</th>
                                    <th class="text-center text-uppercase">Baki</th>
                                    <th class="text-center text-uppercase">Tindakan</th>
                                </tr>
                            </thead>

                            @forelse($users as $key=>$user)
                            @if($user)
                            <tbody>
                                <tr>
                                    <td>
                                        <a target="_blank" href="{{route('affiliates.view_commissions',['no_koppim' => $user->no_koppim ])}}" class="d-flex  align-items-center space-x-1">
                                            {{ $user->name }}
                                            <svg class="w-3 h-3" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M10 6H6a2 2 0 00-2 2v10a2 2 0 002 2h10a2 2 0 002-2v-4M14 4h6m0 0v6m0-6L10 14"></path>
                                            </svg>
                                        </a>
                                    </td>
                                    <td class="text-center">{{ $user->ic }}</td>
                                    <td class="text-center">{{ $user->no_koppim }}</td>
                                    <td class="text-center">{{ $user->downlines->count() }}</td>
                                    <td class="text-center">
                                        RM{{ number_format(\App\Mongo\Payment::where('introducer_no_anggota', (string)$user->no_koppim)->where('returncode', '100')->where('introducer', true)->sum('introducer_commission'),2,'.',',' )}}
                                    </td>
                                    <td class="text-center">
                                        RM{{number_format($user->commission_withdraws->where('status','paid')->sum('amount'),2,'.',',')}}
                                    </td>
                                    <td>
                                        RM{{number_format($user->downlines->where('returncode', '100')->where('introducer', true)->where('introducer_commission_status', 'available')->sum('introducer_commission'),2,'.',',')}}
                                    </td>
                                    <td class="d-flex justify-content-center align-items-center space-x-1">
                                        <a href="{{route('affiliates.view_commissions',['no_koppim' => $user->no_koppim ])}}">
                                            <span data-toggle="tooltip" data-placement="auto" data-original-title="Lihat Senarai Komisen" class="text-warning">
                                                <svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 9v3m0 0v3m0-3h3m-3 0H9m12 0a9 9 0 11-18 0 9 9 0 0118 0z"></path>
                                            </svg>
                                                </span></a>
                                        <a href="{{route('affiliates.view_statements',['user_id' => $user->id ])}}">
                                            <span data-toggle="tooltip" data-placement="auto" data-original-title="Lihat Penyata Komisen" class="text-warning">
                                                <svg class="w-6 h-6" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 5H7a2 2 0 00-2 2v12a2 2 0 002 2h10a2 2 0 002-2V7a2 2 0 00-2-2h-2M9 5a2 2 0 002 2h2a2 2 0 002-2M9 5a2 2 0 012-2h2a2 2 0 012 2m-3 7h3m-3 4h3m-6-4h.01M9 16h.01"></path>
                                                </svg>
                                            </span></a>
                                    </td>
                                </tr>
                            </tbody>
                            @endif
                            @empty
                            <tbody>
                                @tablenoresult
                                @endtablenoresult
                            </tbody>
                            @endforelse
                        </table>
                    </div>
                </div><!-- card-body -->
            </div><!-- card -->
        </div>
    </div>
</div><!-- container -->

@endsection

@section('js')
<script>

</script>
@endsection
