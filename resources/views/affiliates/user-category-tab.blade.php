<ul class="nav nav-pills navtab-bg nav-justified mb-4" id="pills-tab" role="tablist">
    <li class="nav-item">
        <a href="{{url('/admin/affiliates/senarai-permohonan')}}" class="nav-link text-uppercase {{ Helper::active(url('/admin/affiliates/senarai-permohonan')) }} {{Helper::active(url('/admin/affiliates/senarai-permohonan'))}}">
            Semua
        </a>
    </li>
    <li class="nav-item">
        <a href="{{url('/admin/affiliates/senarai-permohonan?status=apply')}}" class="nav-link text-uppercase {{ Helper::active(url('/admin/affiliates/senarai-permohonan?status=apply')) }} {{request()->query('status') == 'apply' ? 'active':''}}" id="pills-tasks-tab">
            Permohonan Baharu
        </a>
    </li>

    <li class="nav-item">
        <a href="{{url('/admin/affiliates/senarai-permohonan?status=yes')}}" class="nav-link text-uppercase {{ Helper::active(url('/admin/affiliates/senarai-permohonan?status=yes')) }} {{request()->query('status') == 'yes' ? 'active':''}}" id="pills-projects-tab">
            Permohonan Lulus
        </a>
    </li>
    <li class="nav-item">
        <a href="{{url('/admin/affiliates/senarai-permohonan?status=reject')}}" class="nav-link text-uppercase {{ Helper::active(url('/admin/affiliates/senarai-permohonan?status=reject')) }} {{request()->query('status') == 'reject' ? 'active':''}}" id="pills-tasks-tab">
            Permohonan Ditolak
        </a>
    </li>
</ul>
