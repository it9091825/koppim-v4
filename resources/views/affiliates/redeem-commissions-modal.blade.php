<div class="modal" id="tebusKomisen">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header pd-y-20 pd-x-25">
                <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Tebus Baki Komisen</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body pd-25">
                <div class="text-center">
                    <h1 class="text-center" style="font-size:48px">{{ 'RM '.number_format($balance,2) }}</h1>
                    <p class="pt-0">Jumlah Baki Komisen Yang Akan Ditebus</p>
                </div>
                <hr>
                <p>Jumlah baki komisen yang ditebus akan dimasukkan ke dalam akaun bank seperti yang berikut. <br><span class="text-danger">Sila pastikan maklumat akaun bank yang tertera adalah tepat.</span></p>
                <table class="table table-bordered mt-3" style="border-top:1px solid #ced4da">

                    <tbody>
                        <tr class="font-weight-bold text-center">
                            <td colspan="2">Maklumat Akaun Bank Saya</td>
                        </tr>
                        <tr>
                            <td class="font-weight-bold">Nama Bank</td>
                            <td>{{Auth::user()->bank_name}}</td>
                        </tr>
                        <tr>
                            <td class="font-weight-bold">No. Akaun Bank</td>
                            <td>{{Auth::user()->bank_account}}</td>
                        </tr>
                    </tbody>
                </table>
            </div><!-- modal-body -->
            <div class="modal-footer">
                <form name="form" enctype="multipart/form-data" action="/affiliates/withdraw" method="post">
                    @csrf
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batalkan</button>
                <button type="submit" class="btn btn-primary">Teruskan</button>
                </form>
            </div>
        </div>
    </div><!-- modal-dialog -->
</div>
