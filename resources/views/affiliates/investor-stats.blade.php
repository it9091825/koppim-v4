<div class="card card-dash-one" style="margin-top: 20px;">
    <div class="row no-gutters">
        <div class="col">
            <i class="icon ion-ios-analytics-outline"></i>
            <div class="dash-content">
                <label class="tx-primary">Jumlah Belum Diproses</label>
                <h2>RM {{ number_format($balance,2) }}</h2>
            </div><!-- dash-content -->
        </div><!-- col-3 -->
        <div class="col">
            <i class="icon ion-ios-pie-outline"></i>
            <div class="dash-content">
                <label class="tx-success">Jumlah Dalam Proses</label>
                <h2>RM {{ number_format($processing,2) }}</h2>
            </div><!-- dash-content -->
        </div><!-- col-3 -->
        <div class="col">
            <i class="icon ion-ios-pie-outline"></i>
            <div class="dash-content">
                <label class="tx-success">Jumlah Sudah Diproses</label>
                <h2>RM {{ number_format($withdraw,2) }}</h2>
            </div><!-- dash-content -->
        </div><!-- col-3 -->
        <div class="col">
            <i class="icon ion-ios-analytics-outline"></i>
            <div class="dash-content">
                <label class="tx-primary">Jumlah Pendaftaran</label>
                <h2>{{ $transaksi_count }}</h2>
            </div><!-- dash-content -->
        </div><!-- col-3 -->

        <div class="col">
            <i class="icon ion-ios-pie-outline"></i>
            <div class="dash-content">
                <label class="tx-success">Jumlah Pelawat</label>
                <h2>{{ $jumlah_pautan_unik }}</h2>
            </div><!-- dash-content -->
        </div><!-- col-3 -->

    </div><!-- row -->
</div><!-- card -->
