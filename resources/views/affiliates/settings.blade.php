@extends('admin.layouts')
@section('css')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/jquery-datetimepicker@2.5.21/jquery.datetimepicker.css">
@endsection
@section('content')
    @pagetitle(['title'=>'Tetapan Affiliate','links'=>['Affiliates']])@endpagetitle
    <div class="container">

        <div class="row">
            <div class="col">
                <div class="card">
                    <div class="card-header">
                        <h3 class="text-lg">Tetapan Nilai Komisen Umum</h3>
                    </div>
                    <div class="card-body">
                        <form action="{{route('affiliates.settings.store.general')}}" method="POST">
                            @csrf
                            <div class="d-flex align-items-center justify-content-between">
                                <div class="w-1/2">
                                    <label for="">RM Nilai Komisen Umum</label>
                                </div>
                                <div class="w-1/2">
                                    <input type="number" class="form-control" name="registration_amount"
                                           value="{{$settings->registration_amount}}">
                                </div>
                            </div>
                            <div class="mt-3 d-flex align-items-center justify-content-end">
                                <div class="w-1/2 text-right">
                                    <button type="submit" class="btn font-bold btn-primary">Simpan Perubahan</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col">
                <div class="card card-table">
                    <div class="card-header">
                        <h3 class="text-lg">Tetapan Nilai Komisen Ejen</h3>
                    </div>
                    <div class="card-body">
                        <div class="col-lg-12">
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="d-flex justify-content-center align-items-center mb-3">
                                    @if($userSettings)
                                        Paparan {{number_format($userSettings->firstItem(),0,"",",")}}
                                        Hingga {{ number_format($userSettings->lastItem(),0,"",",")}}
                                        Daripada {{number_format($userSettings->total(),0,"",",")}} Dalam Senarai
                                    @endif
                                </div>
                                <div class="d-flex justify-content-center align-items-center">
                                    {{ $userSettings->appends(request()->query())->links() }}
                                </div>
                            </div>
                        </div>
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Nama Ejen</th>
                                <th>RM Nilai Komisen Ejen</th>
                                <th class="text-center">Tindakan</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td colspan="10"><span class="text-sm font-bold text-dark">Tambah Ejen</span></td>
                            </tr>
                            <tr>
                                <form action="{{route('affiliates.settings.store.agent')}}" method="POST">
                                    @csrf
                                <td class="w-1/3">
                                    <select name="q" id="" class="form-control select2" style=""></select>
                                </td>
                                <td class="w-1/3">
                                    <input type="number" class="form-control" name="registration_amount">
                                </td>
                                <td class="w-1/3 text-center">
                                    <button type="submit" class="btn btn-success font-bold">Simpan</button>
                                </td>
                                </form>
                            </tr>
                            @forelse($userSettings as $key => $userSetting)
                                <form action="{{route('affiliates.settings.edit.agent')}}" method="POST">
                                    @csrf
                                <tr>
                                    <td>{!!$userSetting->user->name!!}</td>
                                    <td>
                                        <input type="hidden" name="user_setting_id" value="{{$userSetting->id}}">
                                        <input type="number" class="form-control" name="registration_amount"
                                               value="{{$userSetting->registration_amount}}">
                                    </td>
                                    <td>
                                        <button name="type" type="submit" class="btn btn-info font-bold" value="tukar">Tukar</button>
                                        <button name="type" type="submit" class="btn btn-danger font-bold" value="padam">Padam</button>
                                    </td>
                                </tr>
                                </form>
                            @empty

                            @endforelse

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('script')
    <script>
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

        $(".select2").select2({
            theme: 'bootstrap4'
            , width: 'resolve'
            , ajax: {
                url: "{{route('affiliates.settings.find.user')}}"
                , dataType: 'json'
                , data: function (params) {
                    return {
                        _token: CSRF_TOKEN
                        , q: $.trim(params.term)
                    };
                }
                , processResults: function (data) {
                    return {
                        results: data
                    };
                }
                , cache: true
            }

        });

    </script>
@endpush
