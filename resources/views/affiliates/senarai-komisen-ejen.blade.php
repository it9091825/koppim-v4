@extends('admin.layouts')
@section('css')
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/jquery-datetimepicker@2.5.21/jquery.datetimepicker.css">
@endsection

@pagetitle(['title'=>$pageTitle,'links'=>['Affiliates']])@endpagetitle
@section('content')
<div class="container">
    <div class="row">
        <div class="col">
            <a href="/admin/affiliates/transaksi"><span class="font-weight-bold d-flex align-items-center">
                <svg class="w-4 h-4" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M10 19l-7-7m0 0l7-7m-7 7h18"></path></svg>
            Kembali
        </span></a>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="d-flex justify-content-between align-items-center">
                <div class="d-flex justify-content-center align-items-center mb-3">
                    @if($transactions)
                    Paparan {{number_format($transactions->firstItem(),0,"",",")}} Hingga {{ number_format($transactions->lastItem(),0,"",",")}} Daripada {{number_format($transactions->total(),0,"",",")}} Dalam Senarai
                    @endif
                </div>
                <div class="d-flex justify-content-center align-items-center">
                    {{ $transactions->links() }}
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="card card-table">
                <div class="table-responsive">
                    <table class="table mg-b-0">
                        <thead>
                            <tr class="tx-10">
                                <th class="pd-y-5">Tarikh & Masa</th>
                                <th class="pd-y-5 text-center">No. Mykad</th>
                                <th class="pd-y-5 text-center">Nama</th>
                                <th class="pd-y-5 text-center">Status Anggota</th>
                                <th class="pd-y-5 text-center">Komisen</th>

                                <th class="pd-y-5 text-center">Status Komisen</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($transactions as $trans)
                            <tr>
                                <td class="">
                                    {!! $trans->created_at->format("d/m/Y \<\b\\r\> h:i:sa") !!}
                                </td>
                                <td class="text-center">
                                    <a href="{{url('admin/members?search='.$trans->ic)}}" class="tx-inverse tx-14 tx-medium d-block">{{$trans->ic}}</a>
                                    <span class="tx-11 d-block">{{ $trans->gateway }}</span>
                                </td>
                                <td class="valign-middle text-center">
                                    {{$trans->name}} <br>{{$trans->phone}} <br>{{$trans->email}}
                                </td>
                                <td class="valign-middle text-center">
                                    <span class="tx-info">{!! $trans->user ? Helper::anggota_status_text($trans->user->status) : 'tiada' !!}</span>
                                </td>
                                <td class="valign-middle text-center">
                                    @if($trans->introducer_commission == null)
                                    <span class="tx-success">
                                        -
                                    </span>
                                    @else
                                    <span class="tx-success">
                                        <i class="icon ion-android-add mg-r-5"></i>
                                        RM {{ number_format($trans->introducer_commission,2) }}
                                    </span>
                                    @endif
                                </td>
                                <td>
                                    {!!Helper::commission_status_text($trans->introducer_commission_status)!!}
                                </td>
                            </tr>
                            @empty
                            @tablenoresult @endtablenoresult
                            @endforelse
                        </tbody>
                    </table>
                </div><!-- table-responsive -->

            </div><!-- card -->

        </div>
    </div>
</div><!-- container -->

@endsection

@section('js')
<script>
    $(".row-toggle").click(function(e) {
        e.preventDefault();
        var rowid = $(this).attr("data-row-counter");
        $('#row' + rowid).toggle();
    });

</script>
@endsection
