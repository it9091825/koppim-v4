<ul class="nav nav-pills navtab-bg nav-justified mb-4" id="pills-tab" role="tablist">
    <li class="nav-item">
        <a href="{{url('admin/affiliates/pengeluaran')}}" class="nav-link text-uppercase {{ Helper::active(url('admin/affiliates/pengeluaran')) }} {{Helper::active(url('admin/affiliates/pengeluaran'))}}">
            Semua
        </a>
    </li>
    <li class="nav-item">
        <a href="{{url('admin/affiliates/pengeluaran?status=processing')}}" class="nav-link text-uppercase {{ Helper::active(url('admin/affiliates/pengeluaran?status=processing')) }} {{request()->query('status') == 'processing' ? 'active':''}}" id="pills-tasks-tab">
            DALAM PROSES
        </a>
    </li>

    <li class="nav-item">
        <a href="{{url('admin/affiliates/pengeluaran?status=paid')}}" class="nav-link text-uppercase {{ Helper::active(url('admin/affiliates/pengeluaran?status=paid')) }} {{request()->query('status') == 'paid' ? 'active':''}}" id="pills-projects-tab">
            SUDAH DIPROSES
        </a>
    </li>
    <li class="nav-item">
        <a href="{{url('admin/affiliates/pengeluaran?status=cancelled')}}" class="nav-link text-uppercase {{ Helper::active(url('admin/affiliates/pengeluaran?status=cancelled')) }} {{request()->query('status') == 'cancelled' ? 'active':''}}" id="pills-tasks-tab">
            DIBATALKAN
        </a>
    </li>
</ul>
