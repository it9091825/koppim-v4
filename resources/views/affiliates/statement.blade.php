@extends('investor.layouts')
@section('css')
<link href="/investor/lib/chartist/css/chartist.css" rel="stylesheet">

@endsection
@section('navigation')

@include('investor.navigation',['tab'=>'affiliate'])

@endsection

@section('content')
<div class="slim-mainpanel">
    <div class="container">
        <div class="slim-pageheader">
            <ol class="breadcrumb slim-breadcrumb">
                <li class="breadcrumb-item">Program Affiliates</li>
                <li class="breadcrumb-item active" aria-current="page">
                    Penyata Komisen
                </li>
            </ol>
            <h6 class="slim-pagetitle">Penyata Komisen</h6>
        </div><!-- slim-pageheader -->
        <div class="row my-3">
            <div class="col-lg-5">
                <div class="d-flex justify-content-between align-items-center">
                    {{-- <select class="form-control" id="month" onchange="onchangemonth()">
                        <option value="1">JANUARI</option>
                        <option value="2">FEBRUARI</option>
                        <option value="3">MAC</option>
                        <option value="4">APRIL</option>
                        <option value="5">MEI</option>
                        <option value="6">JUN</option>
                        <option value="7">JULAI</option>
                        <option value="8">OGOS</option>
                        <option value="9">SEPTEMBER</option>
                        <option value="10">OKTOBER</option>
                        <option value="11">NOVEMBER</option>
                        <option value="12">DISEMBER</option>
                    </select> --}}
                    <select class="form-control col-4" id="year" onchange="onchangemonth()" style="">
                        <?php for($i = 2020;$i <= date('Y');$i++){ ?>
                        <option value="{{ $i }}">{{ $i }}</option>
                        <?php } ?>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="card card-table">
                    <div class="table-responsive">
                        <table class="table mg-b-0 tx-13">
                            <thead>
                                <tr class="tx-10">
                                    <th class="pd-y-5">Bulan Penyata</th>
                                    <th class="pd-y-5 text-center">No. Rujukan</th>
                                    <th class="pd-y-5 text-center">Nama Bank</th>
                                    <th class="pd-y-5 text-center">No. Akaun</th>
                                    <th class="pd-y-5 text-center">Jumlah Pengeluaran</th>
                                    <th class="pd-y-5 text-center">Status Pengeluaran</th>
                                    <th class="pd-y-5 text-center">No. Rujukan Bayaran</th>
                                    <th class="pd-y-5 text-center">Bukti Bayaran</th>
                                    <th class="pd-y-5 text-center">Muat Turun Penyata</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse($list as $li)
                                <tr>
                                    <td class="pd-l-20">
                                        {{$li->statement_date ? $li->statement_date->format('F Y'):''}}
                                    </td>
                                    <td class="text-center">
                                        {{$li->reference_no}}
                                    </td>
                                    <td class="valign-middle text-center">
                                        {{$li->bank}}
                                    </td>
                                    <td class="valign-middle text-center">
                                        {{$li->account_no}}
                                    </td>
                                    <td class="valign-middle text-center">
                                        RM{{number_format($li->amount,2,'.',',')}}
                                    </td>
                                    <td class="valign-middle text-center">
                                        <span class="tx-info">
                                            {!!Helper::withdrawal_status_text($li->status)!!}
                                        </span>
                                    </td>
                                    <td class="valign-middle text-center">
                                        @if($li->status == 'paid')
                                            {{$li->payment_ref}}
                                            @else
                                        -
                                            @endif
                                    </td>
                                    <td class="valign-middle text-center">
                                        @if($li->status == 'paid')
                                            @if($li->payment_receipt)
                                        <div style="margin-top: 20px;">
                                            <a class="d-flex align-items-center" target="_blank" href="{{ env('DO_SPACES_FULL_URL').$li->payment_receipt }}">LIHAT LAMPIRAN <svg width="16px" class="ml-2" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M10 6H6a2 2 0 00-2 2v10a2 2 0 002 2h10a2 2 0 002-2v-4M14 4h6m0 0v6m0-6L10 14"></path>
                                                </svg></a>
                                        </div>
                                                @else
                                                -
                                                @endif
                                        @else
                                            -
                                        @endif
                                    </td>
                                    <td class="valign-middle text-center">
                                    <a href="/affiliates/penyata-komisen/cetak/{{$li->_id}}" data-toggle="tooltip" title="Muat turun penyata" data-placement="auto">
                                            <svg class="text-muted" width="20px" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 16v1a3 3 0 003 3h10a3 3 0 003-3v-1m-4-4l-4 4m0 0l-4-4m4 4V4"></path>
                                            </svg>
                                        </a>
                                    </td>
                                </tr>
                                @empty
                                @tablenoresult
                                @endtablenoresult
                                @endforelse
                            </tbody>
                        </table>
                    </div><!-- table-responsive -->
                </div><!-- card -->
            </div><!-- col-6 -->
        </div><!-- row -->
    </div>
</div>
@endsection
@push('script')
@if(request()->has('year'))
<script>
    $('#year').val("{{request()->get('year')}}");

</script>
@else
<script>
    $('#year').val("{{Carbon\Carbon::now()->year}}");

</script>
@endif
<script>
    function onchangemonth() {
        var year = $('#year').val();
        window.location = "/affiliates/penyata-komisen?year=" + year;
    }

</script>
@endpush
