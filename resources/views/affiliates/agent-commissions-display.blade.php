<div class="dash-headline" style="height:100%;">
    <div class="dash-headline-left">
        <div class="dash-headline-item-one" id="mainLeft" style="height:318px;">
            <div class="dash-item-overlay">
                <h1><span class="tx-24">RM</span> <span>{{ number_format($commission,2,'.',',') }}</span></h1>
                <p class="earning-label">Jumlah Keseluruhan Komisen</p>
{{--                <p class="earning-desc">Jumlah baki komisen yang belum ditebus.</p>--}}
{{--                <div>--}}
{{--                    <div class="pull-right" style="width: 49%;">--}}
{{--                        @if((integer)date("j") < 25)--}}
{{--                            Komisen hanya boleh ditebus pada setiap 25 hari bulan--}}
{{--                        @else--}}
{{--                            @if( $balance > 0)--}}
{{--                            <a href="#" data-toggle="modal" data-target="#tebusKomisen" class="btn btn-outline-primary" style="width: 100%;">Tebus Baki--}}
{{--                                Komisen <i class="fa fa-angle-right mg-l-5"></i></a>--}}
{{--                            @endif--}}
{{--                        @endif--}}
{{--                    </div>--}}
{{--                </div>--}}
            </div>
        </div><!-- dash-headline-item-one -->
    </div><!-- dash-headline-left -->

    <div class="dash-headline-right" id="mainRight">
        <div class="row row-sm">
            <div class="col-lg-12">
                <div class="card card-table">
                    <div class="card-header">
                        <h6 class="slim-card-title">Kongsi</h6>
                    </div><!-- card-header -->
                    <div class="table-responsive" style="padding: 20px;">
                        <div style="margin-bottom: 10px;"><b>Link untuk dikongsikan</b></div>
                        <div><input type="text" class="form-control" id="linkpromote" value="https://{{ env('INVESTOR_DOMAIN') }}/affiliate?u={{ Auth::user()->no_koppim }}">
                        </div>
                        <div align="center" style="margin-top: 20px;"><a href="javascript:copylink()" class="btn btn-outline-success btn-sm" style="width: 40%;">Salin <i class="fa fa-angle-right mg-l-5"></i></a></div>
                        <div>
                            <ul style="margin-left: -20px; margin-top: 20px; list-style: decimal;">
                                <li>Salin URL diatas dan kongsi dengan rakan anda</li>
                                <li>Setiap transaksi berjaya menggunakan link diatas akan menjana
                                    pendapatan
                                    di akaun anda secara <i>Real Time</i></li>
                            </ul>
                        </div>
                    </div><!-- table-responsive -->
                </div><!-- card -->
            </div><!-- col-6 -->
        </div><!-- row -->
    </div><!-- wd-50p -->
</div><!-- d-flex ht-100v -->
