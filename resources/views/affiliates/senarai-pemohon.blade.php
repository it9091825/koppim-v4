@extends('admin.layouts')
@section('css')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/jquery-datetimepicker@2.5.21/jquery.datetimepicker.css">
@endsection

@pagetitle(['title'=>$pageTitle,'links'=>['Affiliates']])@endpagetitle

@section('content')

    <div class="container">
        <div class="row">

            @include('include.search',['url'=>'admin/affiliates/senarai-permohonan','placeholder' => 'buat carian menggunakan nama atau no. mykad'])
        </div>
        <div class="row ">

            @include('include.pagination',['results'=>$users])

            {{-- only will display on pemohonan baharu page --}}
            @can('luluskan-semua-permohonan-affiliate')
            @if(request()->has('status') && request()->get('status') == 'apply')
                <div class="col-12">
                    <div class="form-group">
                        <button type="submit" class="btn btn-success text-uppercase font-weight-bold"
                                id="approveAllButton">Luluskan <span class="checkboxCountText"></span></button>
                        <button type="submit" class="btn btn-danger text-uppercase font-weight-bold"
                                id="cancelAllButton">Batalkan <span class="checkboxCountText"></span></button>
                    </div>
                </div>
            @endif
            @endcan
            <div class="col-lg-12 col-xl-12">
                <div class="card" style="">
                    <div class="card-body">
                        @include('affiliates.user-category-tab')
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    {{-- only will display on pemohonan baharu page --}}
                                    @can('luluskan-semua-permohonan-affiliate')
                                    @if(request()->has('status') && request()->get('status') == 'apply')
                                        <th><input type="checkbox" id="checkAll"></th>
                                    @endif
                                    @endcan
                                    <th class="text-uppercase">@sortablelink('name','Nama Pemohon')</th>
                                    <th class="text-center text-uppercase">NO. Anggota KoPPIM</th>
                                    <th class="text-center text-uppercase">No. K/P Pemohon</th>
                                    <th class="text-center text-uppercase">No. Telefon</th>
                                    <th class="text-center text-uppercase">Tarikh Mohon</th>
                                    <th class="text-center text-uppercase">Status Permohonan</th>
                                    @if(!request()->has('status') || (request()->has('status') && request()->get('status') == 'yes'))
                                        <th class="text-center text-uppercase">Tarikh Lulus</th>
                                    @endif
                                    @if(request()->has('status') && request()->get('status') == 'reject')
                                        <th class="text-center text-uppercase">Tarikh Tolak</th>
                                    @endif
                                    <th class="text-center text-uppercase">Tindakan</th>
                                </tr>
                                </thead>

                                @forelse($users as $key=>$user)
                                    @if($user)
                                        <tbody>
                                        <tr>
                                            {{-- only will display on pemohonan baharu page --}}
                                            @can('luluskan-semua-permohonan-affiliate')
                                            @if(request()->has('status') && request()->get('status') == 'apply')
                                                <td><input type="checkbox" class="checkItem" name="user[{{$key}}]"
                                                           value="{{$user->id}}"></td>
                                            @endif
                                            @endcan
                                            <td class="text-uppercase">
                                                <a href="/admin/members/view/{{$user->id}}">{{$user->name}}</a>
                                            </td>
                                            <td class="text-center text-uppercase">{{ $user->no_koppim ?? 'TIADA NO. ANGGOTA KoPPIM' }}</td>
                                            <td class="text-center text-uppercase">
                                                {{$user->ic}}
                                            </td>
                                            <td class="text-center text-uppercase">
                                                {{$user->mobile_no}}
                                            </td>
                                            <td class="text-center text-uppercase">
                                                {{$user->affiliate_apply ? $user->affiliate_apply->format('d M Y') : 'Tiada Rekod'}}
                                            </td>
                                            <td class="text-center text-uppercase">
                                                @if($user->is_affiliate == 'yes')
                                                    <div class="badge badge-success">Permohonan Lulus</div>

                                                @elseif($user->is_affiliate == 'apply')
                                                    <div class="badge badge-info">Permohonan Baharu</div>
                                                @else
                                                    <div class="badge badge-danger">Permohanan Gagal</div>
                                                @endif
                                            </td>
                                            @if(!request()->has('status') || (request()->has('status') && request()->get('status') != 'apply'))
                                            <td class="text-center text-uppercase">
                                                        {{$user->affiliate_apply_result ? $user->affiliate_apply_result->format('d M Y') : 'Tiada Rekod'}}
                                            <td>
                                            @endif
                                            <td class="d-flex justify-content-center align-items-center space-x-1">
                                                @if($user->is_affiliate == 'apply')
                                                    <form action="{{route('affiliates.application.result')}}"
                                                          method="POST">
                                                        @csrf
                                                        <input type="hidden" name="user_id" value="{{$user->id}}">
                                                        <button style="border: none;padding: 0;background: none;"
                                                                name="type" type="submit" class="btn btn-info font-bold"
                                                                value="lulus">
                                            <span data-toggle="tooltip" data-placement="auto"
                                                  data-original-title="Luluskan" class="text-success">
                                                <svg class="w-6 h-6" fill="none" stroke="currentColor"
                                                     viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path
                                                        stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                                        d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z"></path></svg></span>
                                                        </button>
                                                        <button style="border: none;padding: 0;background: none;"
                                                                name="type" type="submit" class="btn btn-info font-bold"
                                                                value="gagal">
                                            <span data-toggle="tooltip" data-placement="auto"
                                                  data-original-title="Batalkan" class="text-danger">
                                                <svg class="w-6 h-6" fill="none" stroke="currentColor"
                                                     viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path
                                                        stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                                        d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z"></path></svg></span>
                                                        </button>
                                                    </form>
                                                @endif
                                            </td>
                                        </tr>
                                        </tbody>
                                    @endif
                                @empty
                                    <tbody>
                                    @tablenoresult
                                    @endtablenoresult
                                    </tbody>
                                @endforelse
                            </table>
                        </div>
                    </div><!-- card-body -->
                </div><!-- card -->
            </div>
        </div>
    </div><!-- container -->

@endsection

@section('js')
    <script>
        $(document).ready(function () {

            $("#checkAll").prop('checked', false);
            $("#approveAllButton").prop('disabled', true);
            $("#cancelAllButton").prop('disabled', true);
            $('input.checkItem').prop('checked', false);

        });

        var checkboxes = $('input.checkItem');

        function initCheckboxesCount() {
            var countCheckedCheckboxes = checkboxes.filter(':checked').length;
            if (countCheckedCheckboxes) {
                $('.checkboxCountText').text(`${countCheckedCheckboxes} PERMOHONAN`);
                $("#approveAllButton").prop('disabled', false);
                $("#cancelAllButton").prop('disabled', false);
            } else {
                $('.checkboxCountText').text('');
                $("#approveAllButton").prop('disabled', true);
                $("#cancelAllButton").prop('disabled', true);
            }
        }

        checkboxes.change(function () {
            initCheckboxesCount();
        });

        $("#checkAll").click(function () {

            $('input.checkItem').not(this).prop('checked', this.checked);
            initCheckboxesCount();
        });

        function queries() {
            return {
                users: [...document.querySelectorAll('.checkItem:checked')].map(e => e.value)
            }
        }

        $('#approveAllButton').click(function (e) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            e.preventDefault();

            // console.log(queries())
            $.ajax({
                type: 'POST',
                url: "{{route('affiliates.application.approveAll')}}",
                data: queries(),
                success: function (data) {
                    location.reload();
                }
            });

        });

        $('#cancelAllButton').click(function (e) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            e.preventDefault();

            $.ajax({
                type: 'POST',
                url: "{{route('affiliates.application.cancelAll')}}",
                data: queries(),
                success: function (data) {
                    location.reload();
                }
            });

        });
    </script>
@endsection
