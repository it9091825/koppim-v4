<div class="dash-headline" style="height:100%;">
    <div class="dash-headline-left">
        <div class="dash-headline-item-one" id="mainLeft" style="height: 250px">
            <div class="dash-item-overlay">
                <h3>Program Affiliate KoPPIM</h3>
                <p>Jana pendapatan sampingan dengan menjadi ejen affiliate Sistem Langganan Syer
                    KoPPIM</p>
                <div>
                    @if(Auth::user()->is_affiliate == 'no')
                        <div class="pull-right" style="width: 49%;">
                            <form method="POST" action="/affiliates/apply">
                                @csrf
                                <button class="btn btn-outline-primary" style="width: 100%;">
                                    Hantar Permohonan
                                    <i class="fa fa-angle-right mg-l-5"></i>
                                </button>
                            </form>
                        </div>
                    @elseif(Auth::user()->is_affiliate == 'apply')
                        <div class="bg-success py-3 px-2 text-center text-white">
                            Permohonan Anda Telah Dihantar Untuk Semakan
                        </div>
                    @else
                        <div class="bg-danger py-3 px-2 text-center text-white">
                            Permohonan Anda Telah Ditolak
                        </div>
                    @endif
                </div>
            </div>
        </div><!-- dash-headline-item-one -->
    </div><!-- dash-headline-left -->
</div><!-- d-flex ht-100v -->