@component('mail::message')
# IKOOP

Sila log masuk ke Akaun Anggota KoPPIM anda di https://anggota.koppim.com.my/ dengan kata laluan ini: " {{ $pin }} ".

Terima Kasih,<br>
{{ config('app.name') }}
@endcomponent
