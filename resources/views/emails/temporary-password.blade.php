@component('mail::message')
# Kata Lalua Sementara

Kata laluan sementara anda adalah {{ $temporaryPassword }}. Hubungi KoPPIM di 0384081878 sekiranya anda tidak membuat permohonan ini.

Thanks,<br>
{{ config('app.name') }}
@endcomponent
