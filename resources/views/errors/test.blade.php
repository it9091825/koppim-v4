<!doctype html>
<title>Mod Penyelenggaraan</title>
<head>
    <link href="https://fonts.googleapis.com/css2?family=Roboto+Condensed&display=swap" rel="stylesheet">
</head>
<style>
    body {
        text-align: center;
        padding: 150px;
    }

    h1 {
        font-size: 50px;
    }

    body {
        font: 20px Helvetica, sans-serif;
        color: #333;
    }

    article {
        display: block;
        text-align: left;
        width: 960px;
        margin: 0 auto;
        font-family: 'Roboto Condensed','Helvetica','Arial',sans-serif;
    }
    article h3{
        font-size:36px !important;
    }
    article p{
        font-size:25px !important;
    }
    a {
        color: #dc8100;
        text-decoration: none;
    }

    a:hover {
        color: #333;
        text-decoration: none;
    }

</style>

<article style="text-align:center">
    <a href="https://www.koppim.com.my">
        <img src="{{asset('img/logo-koppim.png')}}" alt="" width="500px" />
    </a>
    <h3>Harap Maaf. Naik Taraf Sistem.</h3>
    <div>
        <h3></h3>
        <p>Laman pendaftaran anggota KoPPIM sedang dinaik taraf bermula
            <br><b>5 September 2020 12:00AM</b> sehingga <b>5 September 2020 11:59PM (24 jam)</b>.</p>
        <p>Sekiranya anda berminat untuk mendaftar sebagai anggota KoPPIM,
            sila masukkan maklumat ringkas di pautan ini: <a
                href="https://ppim.typeform.com/to/BqkIdmQ3">https://ppim.typeform.com/to/BqkIdmQ3.</a>
        </p>
        <p>Pihak kami akan menghubungi anda semula dalam tempoh 5 hari bekerja.</p>
        <p>Mohon maaf di atas sebarang kesulitan.</p>
        <p>Terima kasih.</p>

    </div>
</article>
