<!doctype html>
<title>Site Maintenance</title>
<style>
  body { text-align: center; padding: 150px; }
  h1 { font-size: 50px; }
  body { font: 20px Helvetica, sans-serif; color: #333; }
  article { display: block; text-align: left; width: 960px; margin: 0 auto; }
  a { color: #dc8100; text-decoration: none; }
  a:hover { color: #333; text-decoration: none; }
</style>

<article>
  <a href="{{url('APP_URL')}}">
    <img src="{{asset('img/logo-koppim.png')}}" alt="" width="200px"/>
  </a>
    <h1>Sistem Sedang Diselenggara</h1>
    <div>
    <p>
      {!! $exception->getMessage() !!}</p>
        <p>&mdash; Pihak KoPPIM</p>
    </div>
</article>
