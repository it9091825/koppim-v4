@extends('investor.layoutfront')

@section('css')

@endsection

@section('content')


<div align="center">
    <h5 class="text-success">PERMOHONAN KATA LALUAN ANDA BERJAYA!</h5>
    <h5 class="mt-3" style="text-transform: uppercase;">KATA LALUAN SEMENTARA ANDA TELAH DIHANTAR
        MELALUI EMEL KE  <span class="text-dark font-weight-bold">{{$email}}</span>.
    </h5>
    <p class="text-primary text-center" style="font-size:12px;">SILA SEMAK MESEJ MELALUI EMEL DALAM MASA 5 MINIT, SILA JANGKAKAN KELEWATAN PENERIMAAN EMEL</p>
    <div class="mt-3">
        {{-- <a href="https://www.koppim.com.my" class="btn btn-default mb-3">Laman Utama KoPPIM</a><br> --}}
        <a href="{{env('APP_URL')}}" class="font-weight-bold" style="font-size:16px;">LOG MASUK KE AKAUN ANGGOTA KOPPIM</a>
        <p class="mt-2 text-center">Perlu Bantuan? Ada Pertanyaan? <a href="http://bit.ly/KoPPIM-Pertanyaan"><b
            class="text-danger">KLIK DISINI!</b></a></p>
        {{-- <a href="/langganan-syer" class="btn btn-default mb-3">Tambah Langganan Syer</a><br> --}}
        {{-- <a href="/hantar-bukti-pembayaran" class="btn btn-default mb-3">Muat Naik Bukti Pembayaran Lain</a> --}}
    </div>
</div>

@endsection

@section('js')
<script>

</script>

@endsection
