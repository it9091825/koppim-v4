@extends('investor.layoutfront')

@section('css')


@endsection

@section('content')
<form action="/kata-laluan/sahkan-kad-pengenalan" method="post" enctype="multipart/form-data">
    @csrf

    <div align="center">
        <h3 class="signin-title-primary text-uppercase">
            ANDA PERNAH MENDAFTAR SEBAGAI ANGGOTA?<br>
            ANDA TERLUPA KATA LALUAN?</h3>
    </div>

    <p align="center">SILA MASUKKAN NO. MYKAD DI RUANGAN BERIKUT
        UNTUK MENDAPATKAN KATA LALUAN SEMENTARA</p>

    <div class="form-group">
        <input type="text" class="form-control text-center" name="ic" id="ic" placeholder="MASUKKAN NO. MYKAD ANDA">
        <p class="text-primary text-center" style="font-size:12px;">KATA LALUAN SEMENTARA AKAN DIHANTAR MELALUI EMEL KEPADA EMEL YANG DIDAFTARKAN.</p>
    </div><!-- form-group -->
    <button class="btn btn-primary btn-block btn-signin" style="margin-bottom:10px;">TERUSKAN</button>

    <p class="mg-b-0 text-center"><a href="/"> Kembali Ke Log Masuk</a></p>
    <p class="mg-b-0 text-center">Perlu Bantuan? Ada Pertanyaan? <a href="http://bit.ly/KoPPIM-Pertanyaan"><b
                class="text-danger">KLIK DISINI!</b></a></p>
</form>

@endsection

@section('js')
<script>
    $(document).ready(function(){
       $('#ic').mask('000000000000');
   });
</script>

@endsection
