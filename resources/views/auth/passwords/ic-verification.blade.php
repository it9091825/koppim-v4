@extends('investor.layoutfront')

@section('css')


@endsection

@section('content')

{{-- <div style="margin-bottom: 20px;" class="d-lg-none" align="center"> <img src="https://www.koppim.com.my/wp-content/uploads/2020/06/Logo-KoPPIM-small.png" width="150" /> </div> --}}
<div align="center">
  <h2 class="signin-title-primary text-uppercase">ADAKAH INI EMEL YANG ANDA DAFTARKAN?</h2>
</div>
<form action="/kata-laluan/hantar-kata-laluan-sementara" method="POST">
  @csrf
  {{-- <div align="center">
    <h5>NO. MYKAD : <span style="color: #000000;">{{ $user->ic }}</span></h5>
  </div> --}}

  <div align="center" class="">
{{--    <h2 class="signin-title-primary font-weight-bold">{{ substr($user->mobile_no, 0, -4) }}XXXX</h2>--}}
{{--    <p align="center">SILA MASUKKAN <b>4 DIGIT TERAKHIR NO. TELEFON</b> ANDA--}}
{{--      DI RUANGAN DIBAWAH UNTUK TUJUAN VERIFIKASI (PENGESAHAN)</p>--}}
      <p>SILA MASUKKAN <b>EMEL</b> ANDA DI RUANGAN DIBAWAH UNTUK TUJUAN VERIFIKASI (PENGESAHAN)</p>
  </div>

  <div class="form-group mb-2">
    <input type="email" class="form-control text-center" name="email" id="last4Digit" placeholder="MASUKKAN EMEL ANDA">
{{--    <input type="hidden" name="ic" value="{{$user->ic}}">--}}

  </div><!-- form-group -->
  <button class="btn btn-primary btn-block btn-signin" type="submit"
    style="margin-bottom:5px;">TERUSKAN</button>
    <p class="text-primary text-center" style="font-size:12px;">KATA LALUAN SEMENTARA AKAN DIHANTAR MELALUI EMEL KEPADA EMEL YANG DIDAFTARKAN.</p>
  <p class="text-center mb-2">Perlu Bantuan? Ada Pertanyaan? <a href="http://bit.ly/KoPPIM-Pertanyaan"><b
        class="text-danger">KLIK DISINI!</b></a>
  </p>
  <p class="mg-b-0 text-center"><a href="/"> Kembali Ke Log Masuk</a></p>

</form>


@endsection

@section('js')
<script>
  $(document).ready(function(){
       $('#phone').mask('00000000000');
       $('#amount_display').mask('00000.00');
       $('#ic').mask('000000000000');
      //$('#myModal').modal();
   });

</script>

@endsection
