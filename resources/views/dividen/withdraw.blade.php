@extends('admin.layouts')
@css
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/jquery-datetimepicker@2.5.21/jquery.datetimepicker.css">
@endcss
@pagetitle(['title'=>'Pengeluaran Dividen','links' => ['Dividen']])@endpagetitle
@section('content')
<div class="container" style="margin-bottom: 20px;">
    <div class="row">
        <div class="col-lg-12" align="right">
	        <a href="/admin/dividen/withdraw/export?status={{ $status }}" class="btn btn-success">Export</a>
        </div>
    </div>
</div>


<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body pd-b-0">
					<ul class="nav nav-pills navtab-bg nav-justified mb-4" id="pills-tab" role="tablist">
    <li class="nav-item">
        <a href="{{url('admin/dividen/withdraw')}}" class="nav-link text-uppercase {{ Helper::active(url('admin/dividen/withdraw')) }} {{Helper::active(url('admin/dividen/withdraw'))}}">
            Semua
        </a>
    </li>
    <li class="nav-item">
        <a href="{{url('admin/dividen/withdraw?status=sedang-disemak')}}" class="nav-link text-uppercase {{ Helper::active(url('admin/dividen/withdraw?status=sedang-disemak')) }} {{request()->query('status') == 'sedang-disemak' ? 'active':''}}" id="pills-tasks-tab">
            SEMAKAN DIPERLUKAN
        </a>
    </li>

    <li class="nav-item">
        <a href="{{url('admin/dividen/withdraw?status=telah-dibayar')}}" class="nav-link text-uppercase {{ Helper::active(url('admin/dividen/withdraw?status=telah-dibayar')) }} {{request()->query('status') == 'telah-dibayar' ? 'active':''}}" id="pills-projects-tab">
            TELAH DIBAYAR
        </a>
    </li>
</ul>
                    				<table class="table">
								 		<thead>
								 			<tr>
												<th>Tarikh</th>
												<th>Nama</th>
												<th>K/P</th>
												<th>Status</th>
												<th>Jumlah</th>
																				 		
								 			</tr>
								 		</thead>
								 		<tbody>
									 		@forelse($dividens as $dividen)
								 			<tr>
									 			<td><a href="/admin/dividen/withdraw/{{ $dividen->_id }}">{{ $dividen->created_at->format('d M Y H:i:s') }}</a></td>
												<td><a href="/admin/dividen/withdraw/{{ $dividen->_id }}">{{ $dividen->account_name }}</a></td>
												<td><a href="/admin/dividen/withdraw/{{ $dividen->_id }}">{{ $dividen->ic }}</a></td>
												<td><a href="/admin/dividen/withdraw/{{ $dividen->_id }}">{{ $dividen->status }}</a></td>
												<td><a href="/admin/dividen/withdraw/{{ $dividen->_id }}">RM {{ number_format($dividen->amount*-1,2) }}</a></td>		
												<td></td>
																			 		
								 			</tr>
								 			@empty
								 			<tr>
												<td colspan="3">Tiada Rekod</td>								 		
								 			</tr>
								 			@endforelse
								 		</tbody>
							 		</table>

                </div><!-- card-body -->
            </div><!-- card -->
        </div>


    </div>
</div>



@endsection


