@extends('admin.layouts')
@css
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/jquery-datetimepicker@2.5.21/jquery.datetimepicker.css">
@endcss
@pagetitle(['title'=>'Utama','links' => ['Dividen']])@endpagetitle
@section('content')



<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body pd-b-0">
					<ul class="nav nav-pills navtab-bg nav-justified mb-4" id="pills-tab" role="tablist">
    <li class="nav-item">
        <a href="{{url('admin/dividen/utama')}}" class="nav-link text-uppercase {{ Helper::active(url('admin/dividen/utama')) }} {{Helper::active(url('admin/dividen/utama'))}}">
            Semua
        </a>
    </li>
    <li class="nav-item">
        <a href="{{url('admin/dividen/utama?status=draft')}}" class="nav-link text-uppercase {{ Helper::active(url('admin/dividen/utama?status=draft')) }} {{request()->query('status') == 'draft' ? 'active':''}}" id="pills-tasks-tab">
            Draf
        </a>
    </li>

    <li class="nav-item">
        <a href="{{url('admin/dividen/utama?status=calculated')}}" class="nav-link text-uppercase {{ Helper::active(url('admin/dividen/utama?status=calculated')) }} {{request()->query('status') == 'calculated' ? 'active':''}}" id="pills-projects-tab">
            Calculated
        </a>
    </li>
    <li class="nav-item">
        <a href="{{url('admin/dividen/utama?status=published')}}" class="nav-link text-uppercase {{ Helper::active(url('admin/dividen/utama?status=published')) }} {{request()->query('status') == 'published' ? 'active':''}}" id="pills-tasks-tab">
            Published
        </a>
    </li>
</ul>
                    				<table class="table">
								 		<thead>
								 			<tr>
									 			<th>Tarikh</th>
												<th>Tahun</th>
												<th>Durasi</th>
												
												<th>Dividen %</th>
												<th>Status</th>
																				 		
								 			</tr>
								 		</thead>
								 		<tbody>
									 		@forelse($dividens as $dividen)
								 			<tr>
									 			<td><a href="/admin/dividen/detail/{{ $dividen->uid }}">{{ $dividen->created_at->format('d M Y H:i') }}</a></td>
												<td><a href="/admin/dividen/detail/{{ $dividen->uid }}">{{ $dividen->year }}</a></td>
												<td><a href="/admin/dividen/detail/{{ $dividen->uid }}">{{ strtoupper($dividen->from->format('d M Y')) }} - {{ strtoupper($dividen->to->format('d M Y')) }} ({{ $dividen->month_total }} BULAN)</a></td>
												<td><a href="/admin/dividen/detail/{{ $dividen->uid }}">{{ $dividen->dividen_precentage }} %</a></td>	
												<td><a href="/admin/dividen/detail/{{ $dividen->uid }}">{{ strtoupper($dividen->status) }}</a></td>
																			 		
								 			</tr>
								 			@empty
								 			<tr>
												<td colspan="3">Tiada Rekod</td>								 		
								 			</tr>
								 			@endforelse
								 		</tbody>
							 		</table>

                </div><!-- card-body -->
            </div><!-- card -->
        </div>


    </div>
</div>



@endsection


