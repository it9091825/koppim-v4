@extends('admin.layouts-detail')
@css
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/jquery-datetimepicker@2.5.21/jquery.datetimepicker.css">
@endcss
@pagetitle(['title'=>'Dividen : '.$dividen->year.'','links' => ['Dividen']])@endpagetitle
@section('content')
<div class="container">
    <div class="row">
        <div class="col-lg-12">
	        <div class="card">
		        <div class="card-body pd-b-0">
	        <div>CronJob URL 1 : <a href="https://{{ env('ADMIN_INVESTOR_DOMAIN') }}/admin/dividen/cron/worker1">https://{{ env('ADMIN_INVESTOR_DOMAIN') }}/admin/dividen/cron/worker1</a></div>
	        <div>CronJob URL 2 : <a href="https://{{ env('ADMIN_INVESTOR_DOMAIN') }}/admin/dividen/cron/worker2">https://{{ env('ADMIN_INVESTOR_DOMAIN') }}/admin/dividen/cron/worker2</a></div>
	        <div>CronJob URL 3 : <a href="https://{{ env('ADMIN_INVESTOR_DOMAIN') }}/admin/dividen/cron/worker3">https://{{ env('ADMIN_INVESTOR_DOMAIN') }}/admin/dividen/cron/worker3</a></div>
		        </div>
	        </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-lg-5">
            <div class="card">
                <div class="card-body pd-b-0">

                    <table class="table">
	                    <tr>
		                    <td>Status</td>
		                    <td>:</td>
		                    <td><b>{{ strtoupper(DividenHelper::status_helper($dividen->status)) }}</b></td>
	                    </tr>
	                    <tr>
		                    <td>AGM</td>
		                    <td>:</td>
		                    <td><b>{{ strtoupper($dividen->agm->format('d M Y')) }}</b></td>
	                    </tr>
	                    <tr>
		                    <td>Tempoh</td>
		                    <td>:</td>
		                    <td><b>{{ $dividen->tempoh }} Bulan</b></td>
	                    </tr>
	                    <tr>
		                    <td>Tahun</td>
		                    <td>:</td>
		                    <td><b>{{ $dividen->year }}</b></td>
	                    </tr>
	                    <tr>
		                    <td>Dividen %</td>
		                    <td>:</td>
		                    <td><b>{{ $dividen->dividen_precentage }}%</b></td>
	                    </tr>
	                    <tr>
		                    <td>Tarikh AGM</td>
		                    <td>:</td>
		                    <td><b>{{ strtoupper($dividen->from->format('d M Y')) }}</b></td>
	                    </tr>
	                    <tr>
		                    <td>Sehingga</td>
		                    <td>:</td>
		                    <td><b>{{ strtoupper($dividen->to->format('d M Y')) }}</b></td>
	                    </tr>
	                    <tr>
		                    <td>Jumlah Bulan</td>
		                    <td>:</td>
		                    <td><b>{{ $dividen->month_total }}</b></td>
	                    </tr>
                    </table>
                    @if($dividen->status == "draft")
                     <div class="row" >
	                                <div class="col-12" align="center" >
		                             <a href="/admin/dividen/detail/{{ $dividen->uid }}/calculate" class="btn btn-info btn-sm">CALCULATE</a>
	                                </div>
                               </div>
                       @elseif($dividen->status == "calculated")
                      <div class="row" >
	                                <div class="col-12" align="center" >
		                             <a href="/admin/dividen/detail/{{ $dividen->uid }}/publish" class="btn btn-success btn-sm">PUBLISH</a>
	                                </div>
                               </div>
                      @endif

                </div><!-- card-body -->
            </div><!-- card -->
        </div>

		<div class="col-lg-7">
            <div class="card">
                <div class="card-body pd-b-0">
						 <div class="row" >
						 		<div class="col-12"  >
							 		<h5>Keputusan</h5>
							 		<hr />
							 		@if($dividen->status == "calculated" || $dividen->status == "published")
							 		<table class="table">
					                    <tr>
						                    <td>Jumlah Dividen</td>
						                    <td>:</td>
						                    <td><b>RM {{ strtoupper(number_format($dividen->total_dividen,2)) }}</b></td>
					                    </tr>
					                    <tr>
						                    <td>Jumlah Pelabur</td>
						                    <td>:</td>
						                    <td><b>{{ $dividen->user_count }}</b></td>
					                    </tr>
				                    </table>
				                    @elseif($dividen->status == "calculate-in-progress-2")
				                    <table class="table">

					                    <tr>
						                    <td>Progress</td>
						                    <td>:</td>
						                    <td><b>{{ $dividen->progress_count }} / {{ $dividen->user_count }}</b></td>
					                    </tr>
				                    </table>
				                    @elseif($dividen->status == "published-in-progress")
				                    <table class="table">

					                    <tr>
						                    <td>Progress</td>
						                    <td>:</td>
						                    <td><b>{{ $dividen->published_count }} / {{ $dividen->user_count }}</b></td>
					                    </tr>
				                    </table>
				                    @else
				                    Masih belum membuat pengiraan
				                    @endif
						 		</div>
						  </div>
                </div><!-- card-body -->
            </div><!-- card -->


        </div>

    </div>

    @if($dividen->status == "calculated" || $dividen->status == "published")
    <div class="row">



		<div class="col-lg-12">
            <div class="card">
                <div class="card-body pd-b-0">
						 <div class="row" >
						 		<div class="col-12"  >
							 		<h5>Pelabur</h5>
									 <div class="container" style="margin-bottom: 20px;">

									<div class="row">
										<div class="col-lg-12" align="right">
											<span data-href="csvdividen/{{ $dividen->uid }}" id="export" class="btn btn-success btn-sm" onclick="exportTasks(event.target);">Export To CSV</span>
										</div>
									</div>
								</div>
							 		<hr />
							 		<table class="table table-hover" id="datatable">
								 		<thead>
								 			<tr>
												<th>Nama</th>
												<th>K/P</th>
												<th>Jumlah Syer</th>
												<th>Dividen</th>
								 			</tr>
								 		</thead>
								 		<tbody>
									 		@foreach($users as $user)
                                            <?php $sharev2 = number_format($user->share,2); ?>
								 			<tr>
												<td>{{ $user->name }}</td>
												<td>{{ $user->ic }}</td>
												<td>RM {{ $sharev2  }}</td>
												<td><a href="javascript:open_dividen('{{ json_encode($user->contributions) }}')">RM {{ number_format($user->dividen,2) }}</a></td>
								 			</tr>
								 			@endforeach
								 		</tbody>
							 		</table>
							 		{{ $users->links() }}
						 		</div>
						  </div>
                </div><!-- card-body -->
            </div><!-- card -->


        </div>

    </div>
    @endif
</div>

<!-- The Modal -->
<div class="modal" id="user_letter_modal">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header">
                <h4 class="modal-title" id="modal_letter_header_name">Terperinci</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body" id="modal_body">
                Sedang di proses
            </div>

            <!-- Modal footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Tutup</button>
            </div>

        </div>
    </div>
</div>

@endsection


@section('js')
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script>

<script>

        $(document).ready(function(){
            $('#datatable').DataTable();

        });

          function exportTasks(_this) {
      let _url = $(_this).data('href');
      window.location.href = _url;
   }


 function open_dividen(json_encode) {
        $('#user_letter_modal').modal('show');
        var obj = JSON.parse(json_encode);
        var table = "";
        for (var i = 0; i < obj.length; i++) {


            table += `<table class='table'>
            <tr>
   					<td>Tarikh Pelaburan</td>
   					<td>:</td>
   					<td><b>`+obj[i].payment_date.date.substring(0, 10)+`</b></td>
            </tr>
            <tr>
   					<td>Sehingga</td>
   					<td>:</td>
   					<td><b>{{ strtoupper($dividen->to->format('Y-m-d')) }}</b></td>
            </tr>

            <tr>
   					<td>Jumlah Bulan</td>
   					<td>:</td>
   					<td><b>`+obj[i].month+`</b></td>
            </tr>
            <tr>
   					<td>Jumlah Pelaburan</td>
   					<td>:</td>
   					<td><b>RM `+obj[i].amount.toFixed(2)+`</b></td>
            </tr>
            <tr>
   					<td>Dividen / Bulan</td>
   					<td>:</td>
   					<td><b>RM `+obj[i].dividen_per_month.toFixed(2)+`</b></td>
            </tr>
            <tr>
   					<td>Dividen</td>
   					<td>:</td>
   					<td><b style='color:green;'>RM `+obj[i].dividen.toFixed(2)+`</b></td>
            </tr>
            </table><hr />
            `;
        }


        $('#modal_body').html(table);

    }

</script>
@endsection

