@extends('admin.layouts')
@css
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/jquery-datetimepicker@2.5.21/jquery.datetimepicker.css">
@endcss
@pagetitle(['title'=>'Tetapan Dividen','links' => ['Dividen']])@endpagetitle
@section('content')

<div class="container">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body pd-b-0">
					
                    <form enctype="multipart/form-data" method="post" id="form" action="/admin/dividen/create">
                               @csrf
                               
                               
                               <div class="form-group row">
                                    <label class="col-lg-2 col-form-label" for="pembayaran-dari">Tarikh AGM</label>
                                        <div class="col-lg-10">
                                            <input class="form-control" id="tarikh-agm" type="date" name="tarikh-agm">
                                        </div>
                                </div>
                               
                                
                                <div class="form-group row">
                                    <label class="col-lg-2 col-form-label" for="tempoh">Tempoh</label>
                                        <div class="col-lg-10">
                                            <select class="form-control" name="tempoh" id="tempoh">
	                                            <option value="0">Sila pilih tempoh</option>
	                                            <option value="1">1</option>
	                                            <option value="2">2</option>
	                                            <option value="3">3</option>
	                                            <option value="4">4</option>
	                                            <option value="5">5</option>
	                                            <option value="6">6</option>
	                                            <option value="7">7</option>
	                                            <option value="8">8</option>
	                                            <option value="9">9</option>
	                                            <option value="10">10</option>
	                                            <option value="11">11</option>
	                                            <option value="12">12</option>
                                            </select>
                                        </div>
                                </div>
                                
                                
                               <div class="form-group row">
                                    <label class="col-lg-2 col-form-label" for="pembayaran-dari">Tarikh Dari</label>
                                        <div class="col-lg-10">
                                            <input class="form-control" id="pembayaran-dari" type="date" name="pembayaran-dari">
                                        </div>
                                </div>
                               
                               <div class="form-group row">
                                    <label class="col-lg-2 col-form-label" for="pembayaran-sehingga">Tarikh Sehingga</label>
                                        <div class="col-lg-10">
                                            <input class="form-control" id="pembayaran-sehingga" type="date" name="pembayaran-sehingga">
                                            
                                        </div>
                                </div>
                                
                               
                               
                               
                               
                               <div class="row" style="margin-bottom: 20px;" >
	                                <div class="col-12" >
		                                <input type="text" class="form-control" name="dividen" id="dividen" placeholder="Masukkan Jumlah Percentage Dividen %"  />
	                                </div>
                               </div>
                               
                               
                               <div class="row" >
	                                <div class="col-12" align="right" >
		                             <button class="btn btn-info btn-sm">SETERUSNYA</button>
	                                </div>
                               </div>
                               
                    </form>
                </div><!-- card-body -->
            </div><!-- card -->
        </div>


    </div>
</div>



@endsection
