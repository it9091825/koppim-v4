<!doctype html>

<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

  <title>Penyata Dividen KoPPIM</title>
  <meta name="description" content="Penyata Dividen KoPPIM">
  <meta name="author" content="koppim">
  <style>


	  body {
        font-family: 'Arial',Arial, Helvetica, sans-serif;
    }
	  td {
  padding: 5px;
}

	  </style>

</head>

<body>
  <font size="2" face="Arial" >
  <table style="width: 100%; font-family: Arial; " >

	  <tr>

		  <td align="center" colspan="5"><img width="200" src="{{asset('img/logo-koppim.png')}}" /></td>

	  </tr>
	  <tr>

		  <td colspan="5" align="center">&nbsp;</td>
	  </tr>
	  <tr>

		  <td colspan="5" align="center" style="font-family: 'Arial';"><b>PENYATA PENGAGIHAN PENDAPATAN TAHUNAN BAGI TAHUN KEWANGAN BERAKHIR</b></td>
	  </tr>
	  <tr>

		  <td colspan="5" align="center">&nbsp;</td>
	  </tr>
	  <tr>
		  <td colspan="4">
			  <b>{{ $name }}</b><br>
			  {!! $address !!}.
		   </td>
		  <td align="center" colspan="2" valign="top" >NO. ANGGOTA<br><b>{{ $no_anggota }}</b><br><br>NO. K/P<br><b>{{ $kp }}</b></td>
	  </tr>
	  <tr>

		  <td colspan="5" align="center">&nbsp;</td>
	  </tr>
	  <tr>
		  <td colspan="4" align="left"><b>RIGKASAN PELABURAN</b></td>
	  </tr>
	  <tr>

		  <td colspan="5" align="center">&nbsp;</td>
	  </tr>
	  <tr>
		  <td colspan="4" >PELABURAN TUNAI</td>
		  <td  align="center"><b>RM {{ number_format($share_amount,2) }}</b></td>
	  </tr>
	  <tr>
		  <td colspan="4" >JUMLAH DIVIDEN TERKUMPUL SETAKAT</td>
		  <td align="center"><b>RM {{ number_format($dividen,2) }}</b></td>
	  </tr>
	  <tr>
		  <td align="center">&nbsp;</td>
		  <td align="center">&nbsp;</td>
		  <td align="center">&nbsp;</td>
		  <td align="center">&nbsp;</td>
		  <td align="center">&nbsp;</td>
	  </tr>
	  <tr>
		  <td colspan="4" align="left"><b>PENYATA DIVIDEN</b></td>
	  </tr>

	  <tr>

		  <td colspan="5" align="center">&nbsp;</td>
	  </tr>
	  @forelse($statements as $statement)

	  @if($statement->type == 'in')
	  <tr>
		  <td colspan="1" >{{ strtoupper($statement->created_at->format('d M Y')) }}</td>
		  <td colspan="3" >DIVIDEN UNTUK DURASI {{ strtoupper($statement->from->format('d M Y')) }} - {{ strtoupper($statement->to->format('d M Y')) }}</td>
		  <td align="center"><b>RM {{ number_format($statement->amount,2) }}</b></td>
	  </tr>
	  @elseif($statement->type == 'out')
	  <tr>
		  <td colspan="1" >{{ strtoupper($statement->created_at->format('d M Y')) }}</td>
		  <td colspan="3" >PENGELUARAN DIVIDEN KE {{ $statement->bank }}</td>
		  <td align="center"><b>RM {{ number_format($statement->amount,2) }}</b></td>
	  </tr>
	  @endif
	  @empty

		<tr>
			<td colspan="5">Tiada rekod</td>
		</tr>

	  @endforelse

	  <tr>

		  <td colspan="5" align="center">&nbsp;</td>
	  </tr>

	  <tr>

		  <td colspan="5" align="center">- TAMAT - </td>
	  </tr>
  </table>
  </font>
</body>

</html>
