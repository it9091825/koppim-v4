@extends('admin.layouts')
@section('css')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/jquery-datetimepicker@2.5.21/jquery.datetimepicker.css">
@endsection
@pagetitle(['title'=>'pengeluaran terperinci','links'=>['Dividen']])@endpagetitle
@section('content')
    <div class="container justify-content-center">
        <div class="h-100 row align-items-center">
            <div class="col-lg-12 col-xl-12 mg-t-12">
                <div class="card" style="margin-top: 20px;">
                    <div class="card-body px-2 py-0">
                        <div class="table-responsive">

                            <table class="table">
                                <tr>
                                    <td>Tarikh Masa</td>
                                    <td>:</td>
                                    <td>{{ $withdraw->created_at }}</td>
                                </tr>
                                <tr>
                                    <td>Nama</td>
                                    <td>:</td>
                                    <td>{{ $withdraw->account_name }}</td>
                                </tr>
                                <tr>
                                    <td>K/P</td>
                                    <td>:</td>
                                    <td>{{ $withdraw->ic }}</td>
                                </tr>
                                <tr>
                                    <td>Bank</td>
                                    <td>:</td>
                                    <td>{{ $withdraw->bank }}</td>
                                </tr>
                                <tr>
                                    <td>Nama Akaun</td>
                                    <td>:</td>
                                    <td>{{ $withdraw->account_name }}</td>
                                </tr>
                                <tr>
                                    <td>No. Akaun</td>
                                    <td>:</td>
                                    <td>{{ $withdraw->account_no }}</td>
                                </tr>
                                <tr>
                                    <td>Status</td>
                                    <td>:</td>
                                    <td>
                                        <span class="tx-info">
                                            {!! $withdraw->status !!}
                                        </span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Jumlah</td>
                                    <td>:</td>
                                    <td>RM {{ number_format($withdraw->amount*-1,2) }}</td>
                                </tr>
                                @if($withdraw->status == 'telah-dibayar')
                                    <tr>
                                        <td>NO. RUJUKAN BAYARAN</td>
                                        <td>:</td>
                                        <td>{{$withdraw->payment_ref}}</td>
                                    </tr>
                                    <tr>
                                        <td>BUKTI BAYARAN</td>
                                        <td>:</td>
                                        <td>
                                            @if($withdraw->payment_receipt)
                                            <a class="d-flex align-items-center" target="_blank" href="{{ env('DO_SPACES_FULL_URL').$withdraw->payment_receipt }}">LIHAT LAMPIRAN <svg width="16px" class="ml-2" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M10 6H6a2 2 0 00-2 2v10a2 2 0 002 2h10a2 2 0 002-2v-4M14 4h6m0 0v6m0-6L10 14"></path>
                                                </svg></a>
                                                @else
                                            -
                                                @endif
                                        </td>
                                    </tr>
                                    @endif
                            </table>


                        </div>
                        <hr/>
                        @if($withdraw->status == 'sedang-disemak')
                        <div id="lulus" style="">
                            <form name="form" enctype="multipart/form-data" action="/admin/dividen/withdraw/{{ $withdraw->_id }}" method="post">
                                @csrf
                                <hr/>
                                <input type="hidden" name="id" value="{{ $withdraw->_id }}"/>
                                <div>
                                    <input type="text" class="form-control" id="transaction_ref" name="transaction_ref" placeholder="Rujukan Transaksi">
                                </div>
                                <div style="margin-top: 20px;">Resit Bukti Pembayaran Jika Ada</div>
                                <div style="margin-top: 20px; margin-bottom: 20px;">
                                    <input type="file" name="tx_receipt" id="tx_receipt"></div>
                                <div style="margin-bottom: 20px;">
                                    
                                    <button type="submit" class="btn btn-success" name="kelulusan" value="lulus">LULUS</button>
                                </div>
                            </form>
                        </div>
                        @endif
                    </div><!-- card-body -->
                </div><!-- card -->
            </div>
        </div>
    </div><!-- container -->

@endsection

@section('js')

    <script>

    </script>

@endsection
