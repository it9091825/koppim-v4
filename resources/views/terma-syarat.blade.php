<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Meta -->
    <meta name="description" content="KoPPIM - Pelanggan">

    <title>KoPPIM - Terma & Syarat</title>
    <!-- Vendor css -->
    <link href="/investor/lib/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="/investor/lib/Ionicons/css/ionicons.css" rel="stylesheet">
    <!-- Slim CSS -->
    <link rel="stylesheet" href="/investor/css/slim.css">
    @yield('css')
</head>
<body class="bg-white">
    <div class="container" style="height:100vh">
        <div class="row align-items-center h-100">
            <div class="col">
                <h2 class="slim-logo"><a href="{{env('APP_URL')}}"><img src="{{asset('img/logo-koppim.png')}}" width="200" /></a></h2>

                <h3 class="mt-5 text-dark font-weight-bold">Syarat-Syarat</h3>
                <ol class="text-dark">
                    <li>Fi pendaftaran anggota baharu adalah RM30.00</li>
                    <li>Keanggotaan KoPPIM terbuka kepada semua warganegara Malaysia yang beragama Islam berumur 18 tahun dan ke atas yang menetap di Malaysia.</li>
                    <li>Pemohon mestilah seorang yang waras dan tiada kesalahan jenayah serta bukan seorang yang bankrap atau tidak pernah dibuang daripada mana-mana koperasi dalam tempoh kurang daripada 1 tahun.</li>
                    <li>Pemohon TIDAK DIBENARKAN mengeluarkan syer (saham) dalam tempoh 12 bulan pertama.</li>
                    <li>Permohonan yang lengkap akan disemak dalam tempoh 7 hari waktu bekerja. Sekiranya pihak KoPPIM tidak menerima apa-apa dokumen dalam tempoh 4 minggu, permohonan keanggotaan tersebut akan dihapuskan.</li>
                    <li>Pemohon hendaklah mengisi borang akuan (akan diberi secara online kepada anggota setelah bayaran Fi diselesaikan)</li>
                </ol>



                <p><a href="https://www.koppim.com.my" class="btn btn-outline-secondary pd-x-25" target="_blank">Halaman Utama KoPPIM</a></p>

                <p class="tx-12">&copy; Hak Cipta KoPPIM - 2020</p>
            </div>

        </div><!-- d-flex -->
    </div>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="/investor/lib/popper.js/js/popper.js"></script>
    <script src="/investor/lib/bootstrap/js/bootstrap.js"></script>

    <script src="/investor/js/slim.js"></script>

    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <script src="{{url('/js/jquery.mask.js')}}"></script>
    <script type="text/javascript" src="/js/bootstrap-inputmask.min.js"></script>

    @yield('js')

</body>
</html>
