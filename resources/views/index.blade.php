@extends('layouts.main')
@section('content')
<div class="card bg-light">
    <div class="card-body">
        <img src="{{url('/img/banner_sumbangan_V8.jpg')}}" class="img-fluid rounded mx-auto d-block" alt="Sumbangan PPIM Banner" >
        {{-- <div style="margin-top:50px;margin-bottom:50px;text-align:center">
            <h2>Sumbangan Dana PPIM</h2>
        </div> --}}
        <br>
        <form method="POST" action="{{ route('create') }}">
            @csrf
            
            <div class="form-group">
                <label>Nama Penuh</label>
                <input type="text" class="form-control" id="name" name="name" aria-describedby="name" placeholder="" required value="{{ old('name') }}">
                @if ($errors->has('name'))
                <span class="form-text text-danger"><small class="form-text">{{ $errors->first('name') }}</small></span>
                @endif
            </div>
            
            <div class="form-group">
                <label>No. Kad Pengenalan</label>
                <input type="text" class="form-control ic" id="ic" name="ic" aria-describedby="ic" placeholder="" required value="{{ old('ic') }}">
                @if ($errors->has('ic'))
                <span class="form-text text-danger"><small class="form-text">{{ $errors->first('ic') }}</small></span>
                @endif
            </div>
            
            <div class="form-group">
                <label>No. Telefon</label>
                <input type="text" class="form-control phone" id="phone" name="phone" aria-describedby="phone" placeholder="" required value="{{ old('phone') }}">
                @if ($errors->has('phone'))
                <span class="form-text text-danger"><small class="form-text">{{ $errors->first('phone') }}</small></span>
                @endif
            </div>
            
            <div class="form-group">
                <label>Alamat Emel</label>
                <input type="email" class="form-control" id="email" name="email" aria-describedby="email" placeholder="" value="{{ old('email') }}">
            </div>
            
            <div class="form-group">
                <label>Jumlah Sumbangan</label>
                <select class="form-control" id="amount" name="amount" aria-describedby="amount" placeholder="" required>
                    <option value="">Sila Pilih</option>
                    <option value="25">RM25.00 - Syahbandar</option>
                    <option value="50">RM50.00 - Laksamana</option>
                    <option value="75">RM75.00 - Laksamana</option>
                    <option value="100">RM100.00 - Temenggung</option>
                    <option value="125">RM125.00 - Temenggung</option>
                    <option value="150">RM150.00 - Bendahara</option>
                    <option value="200">RM200.00 - Bendahara</option>
                    <option value="250">RM250.00 - Bendahara</option>
                    <option value="300">RM300.00 - Bendahara</option>
                    <option value="350">RM350.00 - Bendahara</option>
                    <option value="500">RM500.00 - Bendahara</option>
                </select>
                <small class="form-text text-muted">* Setiap transaksi akan dikenakan caj perkhidmatan RM1 dari KiplePay.</small>
                @if ($errors->has('fpx_txnAmount'))
                <span class="form-text text-danger"><small class="form-text">{{ $errors->first('fpx_txnAmount') }}</small></span>
                @endif
            </div>
            
            <div style="margin-top:50px;margin-bottom:50px;text-align:center">
                <button type="submit" class="btn btn-primary">Buat Sumbangan</button>
            </div>
            
        </form>  
    </div>
</div>
@endsection

@section('page-js')
<script src="{{url('/js/jquery.mask.js')}}" ></script>
<script type="text/javascript" src="{{url('')}}/js/bootstrap-inputmask.min.js"></script>
<script>
    $(document).ready(function(){
        $('.phone').mask('000000000000');
        $('.ic').mask('000000-00-0000');
    });
</script>
@endsection
