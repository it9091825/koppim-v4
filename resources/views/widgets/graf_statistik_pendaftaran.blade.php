<div class="d-flex align-items-stretch">
    <div class="card flex-fill">
        <div class="card-body">
            <div id="apex-line-2" class="apex-charts" dir="ltr"></div>
        </div>
    </div>
</div>

@push('script')
<script src="{{asset('admin/libs/apexcharts/apexcharts.min.js')}}"></script>
<script>
    var e = {
        theme:{
            mode:'light',
            palette:'palette2'
        },
        chart: {
            height: 380
            , type: "line"
            , shadow: {
                enabled: !1
                , color: "#bbb"
                , top: 3
                , left: 2
                , blur: 3
                , opacity: 1
            }
        }
        , stroke: {
            width: 6
            , curve: "smooth"
        }
        , series: [{
            name: "Pendaftaran"
            , data: [ <?php
                for ($i = 0; $i < 15; ++$i) {

                    $date = date('Y-m-d', strtotime('today - '.$i.
                        ' days'));
                    $user = App\User::whereRaw("DATE(created_at) = '".$date."'")-> where('type', 'user')->where('ikoop', 0)->count();?>

                    <?php echo $user; ?>,

                    <?php } ?>
            ]
        }]
        , xaxis: {
            type: "datetime"
            , categories: [ <?php
                for ($i = 0; $i < 15; ++$i) {
                    $date = date('Y-m-d', strtotime('today - '.$i.
                        ' days')); ?>

                    "<?php echo $date; ?>",

                    <?php }?>

            ]
        }
        , title: {
            text: ""
            , align: "center"
            , style: {
                fontSize: "14px"
                , color: "#666"
            }
        }
        , fill: {
            type: "gradient"
            , gradient: {
                shade: "dark"
                , gradientToColors: ["#43d39e"]
                , shadeIntensity: 1
                , type: "vertical"
                , opacityFrom: 1
                , opacityTo: 1
                , stops: [0, 100, 100, 100]
            }
        }
        , markers: {
            size: 4
            , opacity: .9
            , colors: ["#50a5f1"]
            , strokeColor: "#fff"
            , strokeWidth: 2
            , style: "inverted"
            , hover: {
                size: 7
            }
        }
        , yaxis: {
            min: 0
            , max: 100
            , title: {
                text: "Jumlah"
            }
        }
        , grid: {
            row: {
                colors: ["transparent", "transparent"]
                , opacity: .2
            }
            , borderColor: "#185a9d"
        }
        , responsive: [{
            breakpoint: 600
            , options: {
                chart: {
                    toolbar: {
                        show: !1
                    }
                }
                , legend: {
                    show: !1
                }
            }
        }]
    };
    new ApexCharts(document.querySelector("#apex-line-2"), e).render();

</script>
@endpush