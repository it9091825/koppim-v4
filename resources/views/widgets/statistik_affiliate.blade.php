<div class="row">
    <div class="col-sm-12 col-md-3">
        <div class="card">
            <div class="card-body">
                <div class="">
                    <h3 class="text-2xl">{{ number_format($agent_count,0,'.',',') }}</h3>
                </div>
                <span class="text-muted">Bilangan Affiliate</span>
            </div>
        </div>
    </div>
    <div class="col-sm-12 col-md-3">
        <div class="card">
            <div class="card-body">
                <div class="">
                    <h3 class="text-2xl">{{ number_format($new_agents,0,'.',',') }}</h3>
                </div>
                <span class="text-muted">Bilangan Pendaftaran Baru</span>
            </div>
        </div>
    </div>
    <div class="col-sm-12 col-md-3">
        <div class="card">
            <div class="card-body">
                <div class="">
                    <h3 class="text-2xl">RM{{ number_format($commission_all,2,'.',',') }}</h3>
                </div>
                <span class="text-muted">Jumlah Komisen</span>
            </div>
        </div>
    </div>
    <div class="col-sm-12 col-md-3">
        <div class="card">
            <div class="card-body">
                <div class="">
                    <h3 class="text-2xl">RM {{ number_format($commission_paid,2,'.',',') }}</h3>
                </div>
                <span class="text-muted">Jumlah Komisen Telah Ditebus</span>
            </div>
        </div>
    </div>
</div>

