<div class="card bg-indigo-800">
    <div class="card-body">
        <h3 class="font-bold text-xl text-white">Sistem CAMS <span class="tarikh_tahun"></span></h3>
        <ul class="list-unstyled space-y-5 mt-4">
            <li>
                <span class="text-white">Jumlah Keseluruhan </span>
                <h4 class="mt-0 mb-1 text-2xl font-weight-normal text-indigo-200"><span id="jumlahKeseluruhanCams"></span></h4>
            </li>
            <li>
                <span class="text-white">Jumlah Fi Pendaftaran</span>
                <h4 class="mt-0 mb-1 text-2xl font-weight-normal text-indigo-200"><span id="jumlahFiCams"></span></h4>

            </li>
            <li>
                <span class="text-white">Jumlah Langganan Syer</span>
                <h4 class="mt-0 mb-1 text-2xl font-weight-normal text-indigo-200"><span id="jumlahSyerCams"></span></h4>

            </li>
        </ul>
    </div>
</div>

