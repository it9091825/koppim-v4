<div class="row">
    <div class="col">
        <h3 class="text-sm text-gray-600 font-medium uppercase letter tracking-wider" id="charts-title"></h3>
    </div>
    <div class="col">
        <div class="d-flex flex-row space-x-2 justify-content-end align-items-end mb-2">
            <select name="" id="month-select" class="form-control w-1/3">
                <option value="">Bulan</option>
                <?php 
                    for($i=1; $i<13; $i++) {
                        echo '<option value="' . $i . '">' . Carbon\Carbon::createFromFormat('m', $i)->translatedFormat('F') . '</option>';
                    } 
                ?>
            </select>
            <select name="" id="charts-type" class="form-control w-1/3">
                <option value="">Jenis Laporan</option>
                <option value="reg_stat">Jumlah Pendaftaran</option>
                <option value="payment_stat">Pembayaran</option>
                <option value="state_stat">Negeri</option>
                <option value="race_stat">Bangsa</option>
                <!-- <option value="religion_stat">Agama</option> -->
                <option value="sex_stat">Jantina</option>
                <option value="age_stat">Umur</option>
            </select>
        </div>
    </div>
</div>
<div id="charts-area" class="carousel slide">
    <div class="d-flex align-items-stretch">
        <div class="card flex-fill">
            <div class="card-body">
                <div id="apex-line-2" class="apex-charts" dir="ltr" ></div>
            </div>
            <div class="card-body">
                <div id="apex-pie-2" class="apex-charts" dir="ltr" style="display:none"></div>
            </div>
        </div>
    </div>
</div>
@push('script')
<script src="{{asset('admin/libs/apexcharts/apexcharts.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.11.0/underscore-min.js" integrity="sha512-wBiNJt1JXeA/ra9F8K2jyO4Bnxr0dRPsy7JaMqSlxqTjUGHe1Z+Fm5HMjCWqkIYvp/oCbdJEivZ5pLvAtK0csQ==" crossorigin="anonymous"></script>

<script>
$(document).ready(function() {
    // for line/bar charts
    var options = {
        theme: {
            mode: 'light',
            palette: 'palette2'
        },
        chart: {
            height: 380,
        },
        stroke: {
            width: 1,
        },
        series: [],
        xaxis: {
            type: "datetime",
            categories: []
        },
        grid: {
            row: {
                colors: ["transparent", "transparent"], 
                opacity: .2
            },
            borderColor: "#185a9d"
        },
        responsive: [{
            breakpoint: 600,
            options: {
                chart: {
                    toolbar: {
                        show: !1
                    }
                },
                legend: {
                    show: !1
                }
            }
        }],
        noData: {
            text: 'Tiada Data'
        }
    };

    //for pie/donut charts
    var options2 = {
        theme: {
            mode: 'light',
            palette: 'palette2'
        },
        chart: {
            height: 380,
            type: 'pie',
        },
        stroke: {
            width: 1,
        },
        series: [],
        labels: [],
        grid: {
            row: {
                colors: ["transparent", "transparent"], 
                opacity: .2
            },
            borderColor: "#185a9d"
        },
        responsive: [{
            breakpoint: 600,
            options: {
                chart: {
                    toolbar: {
                        show: !1
                    }
                },
                legend: {
                    show: !1
                }
            }
        }],
        noData: {
            text: 'Tiada Data'
        }
    };

    var charts = new ApexCharts(
        document.querySelector("#apex-line-2"), options
    );
    var charts2 = new ApexCharts(
        document.querySelector("#apex-pie-2"), options2
    );
    charts.render();
    charts2.render();

    function charts_queries() {
        return {
            year: new Date().getFullYear(),
            month: $('#month-select').val(),
            type: $('#charts-type').val()
        }
    }

    function fetchCharts(data) {
        $.ajax({
            type: 'GET', 
            url: "{{ route('api.charts.statistic') }}", 
            data: data, 
            success: function(res) {
                $("#charts-title").html("GRAF BILANGAN PENDAFTARAN BARU MENGIKUT HARI");
                $('#apex-line-2').show();
                $('#apex-pie-2').hide();
                charts.updateOptions({
                    series: [{
                        name: 'Pendaftaran',
                        data: res.data_online
                    }],
                    xaxis: {
                        type: "datetime",
                        categories: res.date
                    },
                    yaxis: {
                        min: 0,
                        max: 100,
                        title: {
                            text: "Jumlah"
                        }
                    },
                    chart: {
                        type: 'line',
                        stacked: true,
                        shadow: {
                            enabled: !1
                            , color: "#bbb"
                            , top: 3
                            , left: 2
                            , blur: 3
                            , opacity: 1
                        },
                        events: {dataPointSelection: function(event, chartContext, config){}},
                    },
                    fill: {
                        type: "gradient"
                        , gradient: {
                            shade: "dark"
                            , gradientToColors: ["#43d39e"]
                            , shadeIntensity: 1
                            , type: "vertical"
                            , opacityFrom: 1
                            , opacityTo: 1
                            , stops: [0, 100, 100, 100]
                        }
                    },
                    markers: {
                        size: 4
                        , opacity: .9
                        , colors: ["#50a5f1"]
                        , strokeColor: "#fff"
                        , strokeWidth: 2
                        , style: "inverted"
                        , hover: {
                            size: 7
                        }
                    },
                    stroke: {
                        width: 6
                        , curve: "smooth"
                    },
                    plotOptions: {
                        bar: {
                            horizontal: false,
                        }
                    },
                })
            }
        });
    }

    fetchCharts(charts_queries());

    $("#month-select, #charts-type").on("click", function() {
        var count = [];
        var label = [];
        var queries = charts_queries();
        $.ajax({
            type: 'GET', 
            url: "{{ route('api.charts.statistic') }}", 
            data: charts_queries(),
            success: function(res) {
                switch($('#charts-type').val()) {
                    case 'reg_stat':
                        $("#charts-title").html("GRAF BILANGAN PENDAFTARAN BARU MENGIKUT HARI");
                        $('#apex-line-2').show();
                        $('#apex-pie-2').hide();
                        charts.updateOptions({
                            series: [{
                                name: 'Pendaftaran',
                                data: res.data_online
                            }],
                            xaxis: {
                                type: "datetime",
                                categories: res.date
                            },
                            yaxis: {
                                min: 0,
                                max: 100,
                                title: {
                                    text: "Jumlah"
                                }
                            },
                            chart: {
                                type: 'line',
                                stacked: true,
                                shadow: {
                                    enabled: !1
                                    , color: "#bbb"
                                    , top: 3
                                    , left: 2
                                    , blur: 3
                                    , opacity: 1
                                },
                                events: {dataPointSelection: function(event, chartContext, config){}},
                            },
                            fill: {
                                type: "gradient"
                                , gradient: {
                                    shade: "dark"
                                    , gradientToColors: ["#43d39e"]
                                    , shadeIntensity: 1
                                    , type: "vertical"
                                    , opacityFrom: 1
                                    , opacityTo: 1
                                    , stops: [0, 100, 100, 100]
                                }
                            },
                            markers: {
                                size: 4
                                , opacity: .9
                                , colors: ["#50a5f1"]
                                , strokeColor: "#fff"
                                , strokeWidth: 2
                                , style: "inverted"
                                , hover: {
                                    size: 7
                                }
                            },
                            stroke: {
                                width: 6
                                , curve: "smooth"
                            },
                            plotOptions: {
                                bar: {
                                    horizontal: false,
                                }
                            },
                        })
                        break;
                    case 'payment_stat':
                        $("#charts-title").html("GRAF BILANGAN PENDAFTARAN BARU MENGIKUT KAEDAH PEMBAYARAN (online vs manual)");
                        $('#apex-line-2').hide();
                        $('#apex-pie-2').show();
                        charts2.updateOptions({
                            series: res.count,
                            labels: res.label,
                            chart: {
                                type: 'pie',  
                            },
                        });
                        
                        break;
                    case 'sex_stat':
                        $("#charts-title").html("GRAF BILANGAN ANGGOTA MENGIKUT JANTINA");
                        count = [];
                        label = [];
                        res.data.forEach(function (item) {
                            count.push(item.jumlah);
                            label.push(item.sex.toUpperCase());    
                        });
                        $('#apex-line-2').hide();
                        $('#apex-pie-2').show();
                        charts2.updateOptions({
                            series: count,
                            labels: label,
                            chart: {
                                type: 'pie',  
                            },
                        });
                        
                        break;
                    case 'race_stat':
                        $("#charts-title").html("GRAF BILANGAN ANGGOTA MENGIKUT BANGSA");
                        count = [];
                        label = [];
                        res.data.forEach(function (item) {
                            count.push(item.jumlah);
                            label.push(item.race.toUpperCase());    
                        });
                        $('#apex-line-2').hide();
                        $('#apex-pie-2').show();
                        charts2.updateOptions({
                            series: count,
                            labels: label,
                            chart: {
                                type: 'pie',  
                            },
                        });
                        
                        break;
                    case 'religion_stat':
                        $("#charts-title").html("GRAF BILANGAN ANGGOTA MENGIKUT AGAMA");
                        count = [];
                        label = [];
                        console.log(res);
                        res.data.forEach(function (item) {
                            count.push(item.jumlah);
                            label.push(item.religion.toUpperCase());    
                        });
                        $('#apex-line-2').hide();
                        $('#apex-pie-2').show();
                        charts2.updateOptions({
                            series: count,
                            labels: label,
                            chart: {
                                type: 'pie',  
                            },
                        });
                        
                        break;
                    case 'age_stat':
                        $("#charts-title").html("GRAF BILANGAN ANGGOTA MENGIKUT SKALA UMUR");
                        $('#apex-line-2').show();
                        $('#apex-pie-2').hide();
                        charts.updateOptions({
                            series: [{
                                name: 'Jumlah Anggota',
                                data: res.age_count,
                            }],
                            xaxis: {
                                type: "categories",
                                categories: res.age_group,
                                title: "Jumlah"
                            },
                            yaxis: {
                                min: 0,
                                title: {
                                    text: "Skala Umur"
                                }
                            },
                            chart: {
                                type: 'bar', 
                                events: {dataPointSelection: function(event, chartContext, config){}},
                            },
                            fill: {
                                type: "solid",
                            },
                            stroke: {
                                width: 1,
                                curve: "straight"
                            },
                            plotOptions: {
                                bar: {
                                    horizontal: true,
                                }
                            },
                        });

                        break;
                    case 'state_stat':
                        $("#charts-title").html("GRAF BILANGAN ANGGOTA MENGIKUT NEGERI");
                        count = [];
                        label = [];
                        res.data.forEach(function (item) {
                            count.push(item.jumlah);
                            label.push(item.state.toUpperCase());    
                        });
                        $('#apex-line-2').show();
                        $('#apex-pie-2').hide();
                        charts.updateOptions({
                            series: [{
                                name: 'Jumlah Anggota',
                                data: count
                            }],
                            xaxis: {
                                type: "categories",
                                categories: label,
                                title: "Jumlah"
                            },
                            yaxis: {
                                min: 0,
                            },
                            chart: {
                                type: 'bar',
                                stacked: false,
                                events: {
                                    dataPointSelection: function(event, chartContext, config) {
                                        var state_select = label[config.dataPointIndex];
                                        count = [];
                                        label = [];
                                        queries = charts_queries();
                                        queries.state_select = state_select;
                                        $.ajax({
                                            type: 'GET', 
                                            url: "{{ route('api.charts.substatistic') }}",
                                            data: queries,
                                            success: function(res) {
                                                res.data.forEach(function (item) {
                                                    count.push(item.jumlah);
                                                    label.push(item.parliament.toUpperCase());    
                                                });
                                                $('#apex-line-2').hide();
                                                $("#charts-title").html("GRAF BILANGAN ANGGOTA MENGIKUT PARLIMEN: "+state_select);
                                                $('#apex-pie-2').show();
                                                charts2.updateOptions({
                                                    series: count,
                                                    labels: label,
                                                    chart: {
                                                        type: 'pie',  
                                                    },
                                                });
                                            }
                                        }); 
                                    }
                                }
                            },
                            fill: {
                                type: "solid",
                            },
                            stroke: {
                                width: 1,
                                curve: "straight"
                            },
                            plotOptions: {
                                bar: {
                                    horizontal: true,
                                }
                            },
                        })
                        break;
                }
            }
        });
    });
});



</script>
@endpush

