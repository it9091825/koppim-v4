<div class="row">
    <div class="col-sm-12 col-md-3">
        <div class="card">
            <div class="card-body">
                <div class="">
                    <h3 class="text-2xl">{{ number_format($today_user,0,'.',',') }}</h3>
                </div>
                <span class="text-muted">Pendaftaran Hari Ini</span>
            </div>
        </div>
    </div>
    <div class="col-sm-12 col-md-3">
        <div class="card">
            <div class="card-body">
                <div class="">
                    <h3 class="text-2xl">{{ number_format($new_user,0,'.',',') }}</h3>
                </div>
                <span class="text-muted">Jumlah Pendaftaran Keanggotaan</span>
            </div>
        </div>
    </div>
    <div class="col-sm-12 col-md-3">
        <div class="card">
            <div class="card-body">
                <div class="">
                    <h3 class="text-2xl">{{ number_format($semak,0,'.',',') }}</h3>
                </div>
                <span class="text-muted"><a href="/admin/members/new">Sedang Disemak </a> & <br> <a href="/admin/members/reviewed">Untuk Diluluskan</a></span>
            </div>
        </div>
    </div>
    <div class="col-sm-12 col-md-3">
        <div class="card">
            <div class="card-body">
                <div class="">
                    <h3 class="text-2xl">{{ number_format($belumDikemaskini,0,'.',',') }}</h3>
                </div>
                <span class="text-muted"><a href="/admin/members?status=0"> Belum Dikemaskini</a></span>
            </div>
        </div>
    </div>
    <div class="col-sm-12 col-md-3">
        <div class="card">
            <div class="card-body">
                <div class="">
                    <h3 class="text-2xl">{{ number_format($lulus,0,'.',',') }}</h3>
                </div>
                <span class="text-muted"><a href="/admin/members/active">Telah Diluluskan</a></span>
            </div>
        </div>
    </div>
    <div class="col-sm-12 col-md-3">
        <div class="card">
            <div class="card-body">
                <div class="">
                    <h3 class="text-2xl">{{ number_format($berhenti,0,'.',',') }}</h3>
                </div>
                <span class="text-muted"><a href="/admin/members/terminations?status=97"> Berhenti</a></span>
            </div>
        </div>
    </div>
</div>
