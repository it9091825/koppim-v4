<div class="card bg-dark">
    <div class="card-body">
        <h3 class="font-bold text-xl text-white text-center">Senarai Top 5 Performing Affiliate</h3>
        <div class="table-responsive">
            <table class="table">
                <thead class="thead-primary text-white">
                    <tr>
                        <th class="border-0"></th>
                        <th class="text-center border-0">AFFILIATE</th>
                        <th class="text-center border-0">BILANGAN PENDAFTARAN</th>
                    </tr>
                </thead>
                <?php $i=0; ?>
                @foreach($affiliate as $key=>$item)
                    @if($item['count'] == 0)
                    @else
                        <tbody class="border-0">
                            <tr>
                                <td class="text-center text-white border-0">{{ ++$i }}.</td>
                                <td class="text-center text-white border-0"><a href="/admin/affiliates/transaksi?search=<?php echo $item['name']; ?>"> <?php echo $item['name']; ?> </td></a>
                                <td class="text-center text-white border-0"> <?php echo $item['count']; ?></td>
                            </tr>
                        </tbody>
                    @endif
                @endforeach
            </table>
        </div>
    </div>
</div>
