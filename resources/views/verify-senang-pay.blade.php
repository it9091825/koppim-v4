@extends('layouts.main')
@section('page-css')

<style>
    .btn-group button,
    .btn {
        background-color: #22445d;
    }

    .input-group-text,
    .form-control,
    .btn {
        border-radius: 0;
    }

    .btn {
        border-color: inherit;
    }

    input[type="text"]:disabled {
        background-color: white;
    }

</style>


@endsection
@section('content')
<div class="card">
    <div class="card-body">
        <div align="center"><img src="{{asset('img/logo-koppim.png')}}" width="200" class="img-fluid rounded mx-auto" alt="koPPIM"></div>
        <div style="margin-top:20px;margin-bottom:20px;text-align:center">
            <h4>Sila Semak Maklumat Sebelum Meneruskan Transaksi.</h4>
        </div>
        <form method="POST" enctype="multipart/form-data" action="{{ $url }}">





            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text" id="btnGroupAddon2" style="background-color: #22445d; color: white;">JUMLAH SHARE</div>
                    </div>
                    <input type="text" class="form-control" value="RM {{ $donation->amount }}" disabled name="amount" id="amount" aria-describedby="btnGroupAddon2">
                </div>
            </div>




            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text" id="btnGroupAddon2" style="background-color: #22445d; color: white;">NAMA</div>
                    </div>
                    <input type="text" class="form-control" value="{{ $donation->name }}" disabled name="name" id="name" aria-describedby="btnGroupAddon2">
                </div>
            </div>

            <div class="form-group" style="display: none;">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text" id="btnGroupAddon2" style="background-color: #22445d; color: white;">NO. K/P</div>
                    </div>
                    <input type="text" class="form-control" value="{{ $donation->ic }}" disabled name="ic" id="ic" aria-describedby="btnGroupAddon2">
                </div>
            </div>

            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text" id="btnGroupAddon2" style="background-color: #22445d; color: white;">NO. TEL.</div>
                    </div>
                    <input type="text" class="form-control" value="+6{{ $donation->phone }}" disabled name="phone" id="phone" aria-describedby="btnGroupAddon2">
                </div>
            </div>

            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text" id="btnGroupAddon2" style="background-color: #22445d; color: white;">EMEL</div>
                    </div>
                    <input type="text" class="form-control" disabled value="{{ ($donation->email == '') ? '' : $donation->email }}" name="email" id="email" aria-describedby="btnGroupAddon2">

                </div>
                <small class="form-text text-muted" align="center">* Penyedia Gerbang Pembayaran, Raudhah Pay.
                    <br>* Dengan Melanggan Share, Anda Bersetuju Dengan Terma & Syarat Yang Ditetapkan.</small>
            </div>






            <div>

                @if($donation->type == "recurring")


                <input type="hidden" name="recurring_id" value="{{ $recurring_id[''.$amount.''] }}">


                <input type="hidden" name="order_id" value="{{$donation->order_ref}}">
                <input type="hidden" name="name" value="{{$donation->name}}">
                <input type="hidden" name="email" value="{{ ($donation->email == '') ? '' : $donation->email }}">
                <input type="hidden" name="phone" value="+6{{$donation->phone}}">
                <input type="hidden" name="hash" value="{{$hashValue}}">

                @else
                <input type="hidden" name="detail" value="Sumbangan">
                <input type="hidden" name="amount" value="{{$amount}}">
                <input type="hidden" name="order_id" value="{{$donation->order_ref}}">
                <input type="hidden" name="name" value="{{$donation->name}}">
                <input type="hidden" name="email" value="{{ ($donation->email == '') ? '' : $donation->email }}">
                <input type="hidden" name="phone" value="+6{{$donation->phone}}">
                <input type="hidden" name="hash" value="{{$hashValue}}">
                @endif




            </div>

            <div style="text-align:center">
                <img class="bid_plate img-fluid" src="https://app.senangpay.my/public/img/pay.png" alt=""> <br>
            </div>

            <div style="margin-top:10px;margin-bottom:10px;text-align:center">
                <button type="submit" class="btn btn-primary">SETERUSNYA</button>
            </div>

        </form>
    </div>
</div>
@endsection

@section('page-js')
<script src="{{url('/js/jquery.mask.js')}}"></script>
<script type="text/javascript" src="{{url('')}}/js/bootstrap-inputmask.min.js"></script>
<script>

</script>
@endsection
