@extends('investor.layoutfront')

@section('css')


@endsection

@section('content')
<form action="/ikoop-migration/verification/otp/submit" method="post" enctype="multipart/form-data">
	@csrf
{{-- <div style="margin-bottom: 20px;" class="d-lg-none" align="center"> <img src="https://www.koppim.com.my/wp-content/uploads/2020/06/Logo-KoPPIM-small.png" width="150" /> </div> --}}

					@if(Session::has('success'))

				      <div class="alert alert-success">
					      SMS OTP BERAJAYA DIHANTAR KE {{ $user->mobile_no }}
				      </div>
				      @endif

<div align="center"><h5>NO. MYKAD : <span style="color: #000000;">{{ $user->ic }}</span></h5></div>
 <p align="center" >Sila Klik Butang "Hantar Semula OTP" untuk mendapatkan kod OTP semula dan masukkan no pengesahan di borang di bawah</p>
          <div align="center"><h4 class="signin-title-primary">{{ $user->mobile_no }}</h4></div>

		   <input type="hidden" class="form-control" name="ic" id="ic" value="{{ $user->ic }}">

          <div class="form-group">
            <input type="text" class="form-control" name="otp" id="otp" placeholder="6 DIGIT OTP YANG TELAH DIHANTAR">
          </div><!-- form-group -->
          <div align="center" style="margin-bottom: 20px;"><a href="/ikoop-migration/verification/otp/send?ic={{ $user->ic }}&code={{ $code }}">Hantar Semula OTP</a></div>
          <button class="btn btn-primary btn-block btn-signin" style="margin-bottom:10px">Hantar</button>
          <p class="text-center mb-2">Perlu Bantuan? Ada Pertanyaan? <a href="http://bit.ly/KoPPIM-Pertanyaan"><b class="text-danger">KLIK DISINI!</b></a>
          </p>
           <p class="mg-b-0"><a href="/"> Kembali Ke Log Masuk</a></p>
</form>

@endsection

@section('js')
<script>
   $(document).ready(function(){
       $('#phone').mask('00000000000');
       $('#amount_display').mask('00000.00');
       $('#ic').mask('000000000000');
      //$('#myModal').modal();
   });




</script>

@endsection
