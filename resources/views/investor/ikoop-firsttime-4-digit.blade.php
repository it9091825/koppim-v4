@extends('investor.layoutfront')

@section('css')


@endsection

@section('content')

{{-- <div style="margin-bottom: 20px;" class="d-lg-none" align="center"> <img src="https://www.koppim.com.my/wp-content/uploads/2020/06/Logo-KoPPIM-small.png" width="150" /> </div> --}}
<div align="center"><h5>NO. MYKAD : <span style="color: #000000;">{{ $user->ic }}</span></h5></div>
 <p align="center" >Sila masukkan 4 digit terakhir no. telefon bimbit yang didaftarkan</p>
          <div align="center"><h4 class="signin-title-primary">{{ substr($user->mobile_no, 0, -4) }}XXXX</h4></div>



          <div class="form-group">
            <input type="text" class="form-control" name="code" id="code" placeholder="4 DIGIT TERAKHIR">
          </div><!-- form-group -->
          <button class="btn btn-primary btn-block btn-signin" type="button" onclick="sendCode()" style="margin-bottom:10px;">Hantar</button>          
          <p class="text-center mb-2">Perlu Bantuan? Ada Pertanyaan? <a href="http://bit.ly/KoPPIM-Pertanyaan"><b class="text-danger">KLIK DISINI!</b></a>
          </p>
          <p class="mg-b-0 text-center"><a href="/"> Kembali Ke Log Masuk</a></p>


@endsection

@section('js')
<script>
   $(document).ready(function(){
       $('#phone').mask('00000000000');
       $('#amount_display').mask('00000.00');
       $('#ic').mask('000000000000');
      //$('#myModal').modal();
   });

   function sendCode()
   {
	   var vake = $('#code').val();
	   window.location = "/ikoop-migration/verification/otp?ic={{ $user->ic }}&code="+vake+"";
   }


</script>

@endsection
