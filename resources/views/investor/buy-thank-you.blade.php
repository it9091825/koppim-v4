@extends('investor.layoutfront')

@section('css')

@endsection

@section('content')

    @if($status_id == 1)
        <div align="center">
            <h4 class="signin-title-primary" style="text-transform: uppercase;">
                Terima kasih diatas pendaftaran dan pembayaran.
            </h4>
        </div>
        <hr/>
        <p align="center" style="text-transform: uppercase; color:red;">
            PERMOHONAN ANDA TELAH DITERIMA DAN AKAN DIPROSES.
        </p>
        <p align="center" style="text-transform: uppercase; color:red;">
            Pengesahan pendaftaran dan resit pembayaran akan dihantar melalui emel yang telah didaftarkan.
        </p>
        <p align="center" style="text-transform: uppercase; color:red;">
            Sila semak emel dan sms untuk tindakan selanjutnya. Kata laluan sementara akan dihantar melalui sms untuk log masuk ke akaun anda.
        </p>
    @else
        <div align="center">
            <h4 class="signin-title-primary" style="text-transform: uppercase;">
                Langganan syer tidak berjaya
            </h4>
        </div>
        <hr/>
        <p align="center" style="text-transform: uppercase; color: #000000;">
            Sila Semak Akaun Bank/Kad anda dan cuba lagi.
        </p>
    @endif

    <div class="form-group">
        <div class="input-group">
            <div class="input-group-prepend">
                <div class="input-group-text" id="btnGroupAddon2" style="background-color: white; color: black;">
                    NO. RUJUKAN
                </div>
            </div>
            <input type="text" class="form-control" value="{{ ($payment) ? $payment->order_ref : ''}}" disabled
                   style="color: black;" name="order_ref" id="order_ref" aria-describedby="btnGroupAddon2">
        </div>
    </div>

    <div class="form-group">
        <div class="input-group">
            <div class="input-group-prepend">
                <div class="input-group-text" id="btnGroupAddon2" style="background-color: white; color: black;">
                    NAMA
                </div>
            </div>
            <input type="text" class="form-control" value="{{ ($payment) ? $payment->name  : ''}}" disabled
                   style="color: black;" name="name" id="name" aria-describedby="btnGroupAddon2">
        </div>
    </div>

    <div class="form-group">
        <div class="input-group">
            <div class="input-group-prepend">
                <div class="input-group-text" id="btnGroupAddon2" style="background-color: white; color: black;">
                    NO. MYKAD
                </div>
            </div>
            <input type="text" class="form-control" value="{{ ($payment) ? $payment->ic : '' }}" disabled
                   style="color: black;" name="ic" id="ic" aria-describedby="btnGroupAddon2">
        </div>
    </div>

    <div class="form-group">
        <div class="input-group">
            <div class="input-group-prepend">
                <div class="input-group-text" id="btnGroupAddon2" style="background-color: white; color: black;">
                    NO. TEL.
                </div>
            </div>
            <input type="text" class="form-control" value="+6{{ ($payment) ? $payment->phone : ''}}" disabled
                   style="color: black;" name="phone" id="phone" aria-describedby="btnGroupAddon2">
        </div>
    </div>

    <div class="form-group">
        <div class="input-group">
            <div class="input-group-prepend">
                <div class="input-group-text" id="btnGroupAddon2" style="background-color: white; color: black;">
                    EMEL
                </div>
            </div>
            <input type="text" class="form-control" value="{{ ($payment) ? $payment->email : ''}}" disabled
                   style="color: black;" name="email" id="email" aria-describedby="btnGroupAddon2">
        </div>
    </div>

    <div align="center" style="color: black; margin-top: 10px;">
        <h6>JUMLAH BAYARAN DITERIMA</h6>
    </div>
    <div align="center" style="color: black; margin-top: -10px;">
        <h3>RM {{ ($payment) ? number_format($payment->amount, 2, ".", ",") : '' }}</h3>
    </div>
    @if($status_id == 1)
        <p class="text-center"><b>Sila log masuk ke Akaun Anggota KoPPIM anda di sini:</b></p>
        <div style="margin-top:10px;margin-bottom:10px;text-align:center">
            @if(Auth::check())
                <a href="/home" class="btn btn-primary btn-block btn-signin" style="color: white;">PAPARAN UTAMA</a>
            @else
                <a href="/" class="btn btn-primary btn-block btn-signin" style="color: white;">LOG MASUK KE AKAUN</a>
            @endif
        </div>
    @else
        <div style="margin-top:10px;margin-bottom:10px;text-align:center">
            <a href="/langganan-syer/pengesahan?order_ref={{ ($payment) ? $payment->order_ref : ''}}"
               class="btn btn-primary btn-block btn-signin" style="color: white;">CUBA LAGI</a>
        </div>
    @endif

@endsection

@section('js')
    @if($status_id == 1)
        <script>
            !function (f, b, e, v, n, t, s) {
                if (f.fbq) return;
                n = f.fbq = function () {
                    n.callMethod ?
                        n.callMethod.apply(n, arguments) : n.queue.push(arguments)
                };
                if (!f._fbq) f._fbq = n;
                n.push = n;
                n.loaded = !0;
                n.version = '2.0';
                n.queue = [];
                t = b.createElement(e);
                t.async = !0;
                t.src = v;
                s = b.getElementsByTagName(e)[0];
                s.parentNode.insertBefore(t, s)
            }(window, document, 'script',
                'https://connect.facebook.net/en_US/fbevents.js');
            fbq('init', '564539721164739');
            fbq('track', 'PageView');
            fbq('track', 'Purchase', {
                currency: "MYR",
                value: "{{ ($payment) ? number_format($payment->amount, 2, '.', ',') : '' }}"
            });
        </script>
        <noscript>
            <img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=564539721164739&ev=PageView&noscript=1"/>
        </noscript>
        <!-- End Facebook Pixel Code -->
    @endif
@endsection
