@extends('investor.layouts')

@section('navigation')

@include('investor.navigation',['tab'=>''])

@endsection

@section('content')

	<div class="slim-mainpanel" >
      <div class="container" style="max-width: 800px; min-width: 320px; width: 90%;">
        <div class="slim-pageheader">
          <ol class="breadcrumb slim-breadcrumb">
            <li class="breadcrumb-item"><a href="#"></a></li>
          </ol>
          <h6 class="slim-pagetitle">PENGELUARAN KOMISYEN</h6>
        </div><!-- slim-pageheader -->

        <div class="section-wrapper"  >
	     <div>
		     <div>JUMLAH KOMISYEN : <b>RM {{ number_format($commisyen,2) }}</b></div>
	     </div>
          
		    <form action="/affiliates/withdraw" method="post" enctype="multipart/form-data">
			    @csrf
			    
          <div class="row">
	           
		            <div class="col-lg">
		              <div style="margin-top: 20px;">
			                <select class="form-control" name="withdraw_type" id="withdraw_type" onchange="show_instruction()" >
				                <option value="bank">PENGELUARAN KE BANK AKAUN</option>
				                <option value="addinvestment">TAMBAH LANGGANAN</option>
			                </select>
			                
		                </div>
		                <div id="show_bank">
		               <div style="margin-top: 20px;"><input class="form-control" name="jumlah_pengeluaran" id="jumlah_pengeluaran" placeholder="Jumlah Untuk Dikeluarkan" type="text"></div>
		                
		                <div style="margin-top: 20px;">
			                <table class="table" >
				                <tr>
					                <td colspan="3" align="center"><b>KOMISYEN AKAN DIKREDITKAN KE AKAUN DIBAWAH</b></td>
				                </tr>
				                <tr>
					                <td align="center">BANK</td>
					                 <td align="center">:</td>
					                <td align="center"><b>{{ Auth::user()->bank_name }}</b></td>
				                </tr>
				                <tr>
					                <td align="center">AKAUN NO.</td>
					                 <td align="center">:</td>
					                <td align="center"><b>{{ Auth::user()->bank_account }}</b></td>
				                </tr>
				                <tr>
					                <td align="center">NAMA AKAUN</td>
					                 <td align="center">:</td>
					                <td align="center"><b>{{ Auth::user()->bank_account_name }}</b></td>
				                </tr>
			                </table>
		                </div>
		                </div>
		                
		                 <div id="show_transafer" style="display: none;">
			                 
			                 
		                 </div>
		                
		                <div style="margin-top: 20px;"><button class="btn btn-primary btn-block btn-signin" type="submit" >Hantar</button></div>
		            </div><!-- col -->
            
            		
              
          </div><!-- row -->
		  </form>
		
          
        </div><!-- section-wrapper -->
      </div>
	</div>


<div class="slim-mainpanel" >
      <div class="container" style="max-width: 800px; min-width: 320px; width: 90%;">
        <div class="slim-pageheader">
          <ol class="breadcrumb slim-breadcrumb">
            <li class="breadcrumb-item"><a href="#"></a></li>
          </ol>
          <h6 class="slim-pagetitle">PENGELUARAN</h6>
        </div><!-- slim-pageheader -->

        <div class="section-wrapper"  >
	    
			    
          <div class="row">
	           
		            <div class="col-lg">
			             <div class="table-responsive">
		              	<table class="table">
			              	<thead>
				              	<tr>
					              	<th>TARIKH / MASA</th>
					              	<th>JENIS</th>
					              	<th>BANK</th>
					              	<th>AKAUN NO.</th>
					              	<th>NAMA AKAUN</th>
					              	<th>JUMLAH</th>
					              	<th>STATUS</th>
					              	<th>NO. RUJUKAN</th>
					              	<th>BUKTI PEMBAYARAN</th>
				              	</tr>
			              	</thead>
			              	<tbody>
				              	@foreach($withdraws as $withdraw)
				              	@if($withdraw->status == "LULUS")
				              	<tr>
					              	<td>{{ $withdraw->created_at->format('d M Y H:i') }}</td>
					              	@if( $withdraw->type == 'bank')
					              	<td>KREDIT BANK</td>
					              	@elseif( $withdraw->type == 'addinvestment')
					              	<td>PEMBELIAN SYER</td>
					              	@endif
					              	<td>{{  $withdraw->bank }}</td>
					              	<td>{{  $withdraw->account_no }}</td>
					              	<td>{{  $withdraw->account_name }}</td>
					              	<td>RM {{  number_format($withdraw->amount,2) }}</td>
					              	<td ><span class="tx-success">{{  $withdraw->status }}</span></td>
					              	<td ><span >{{  $withdraw->ref_no }}</span></td>
					              	<td ><a href="{{  env('DO_SPACES_FULL_URL').$withdraw->tx_receipt }}" >LIHAT</a></td>
				              	</tr>
				              	
				              	@elseif($withdraw->status == "REFUND")
				              	<tr>
					              	<td>{{ $withdraw->created_at->format('d M Y H:i') }}</td>
					              	@if( $withdraw->type == 'bank')
					              	<td>KREDIT BANK</td>
					              	@elseif( $withdraw->type == 'addinvestment')
					              	<td>PEMBELIAN SYER</td>
					              	@endif
					              	<td>{{  $withdraw->bank }}</td>
					              	<td>{{  $withdraw->account_no }}</td>
					              	<td>{{  $withdraw->account_name }}</td>
					              	<td>RM {{  number_format($withdraw->amount,2) }}</td>
					              	<td ><span class="tx-danger">{{  $withdraw->status }}</span></td>
					              	<td ><span >{{  $withdraw->justification }}</span></td>
					              	<td >-</td>
				              	</tr>
				              	@elseif($withdraw->status == "SEMAKAN")
				              	<tr>
					              	<td>{{ $withdraw->created_at->format('d M Y H:i') }}</td>
					              	@if( $withdraw->type == 'bank')
					              	<td>KREDIT BANK</td>
					              	@elseif( $withdraw->type == 'addinvestment')
					              	<td>PEMBELIAN SYER</td>
					              	@endif
					              	<td>{{  $withdraw->bank }}</td>
					              	<td>{{  $withdraw->account_no }}</td>
					              	<td>{{  $withdraw->account_name }}</td>
					              	<td>RM {{  number_format($withdraw->amount,2) }}</td>
					              	<td ><span class="tx-warning">PERLU SEMAKAN</span></td>
					              	<td ><span >{{  $withdraw->justification }}</span></td>
					              	<td >-</td>
				              	</tr>
				              	@else
				              	
				              	<tr>
					              	<td>{{ $withdraw->created_at->format('d M Y H:i') }}</td>
					              	@if( $withdraw->type == 'bank')
					              	<td>KREDIT BANK</td>
					              	@elseif( $withdraw->type == 'addinvestment')
					              	<td>PEMBELIAN SYER</td>
					              	@endif
					              	<td>{{  $withdraw->bank }}</td>
					              	<td>{{  $withdraw->account_no }}</td>
					              	<td>{{  $withdraw->account_name }}</td>
					              	<td>RM {{  number_format($withdraw->amount,2) }}</td>
					              	<td><span class="tx-warning">{{  $withdraw->status }}</span></td>
					              	<td>-</td>
					              	<td>-</td>
				              	</tr>
				              	@endif
				              	@endforeach
			              	</tbody>
		              	</table>
			             </div>
		                
		                
		            </div><!-- col -->
            
            		
              
          </div><!-- row -->
		 
		
          
        </div><!-- section-wrapper -->
      </div>
	</div>

@endsection


@section('js')
<script>
	function show_instruction() {
  var sfsfd = $('#withdraw_type').val();
  
  if(sfsfd === 'bank')
  {
	  $('#show_bank').show();
	  $('#show_transafer').hide();
	  
  }
  else if(sfsfd === 'addinvestment')
  {
	  $('#show_bank').hide();
	  $('#show_transafer').show();
  }

}

	</script>
@endsection
