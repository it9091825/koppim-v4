@extends('investor.layoutfront')

@section('css')


@endsection

@section('content')
<div align="center">
    <h4 class="signin-title-primary" style="text-transform: uppercase;">
        PEMBAYARAN SECARA MANUAL
    </h4>
</div>

<hr />
<p align="center" style="text-transform: uppercase; color: #000000;">
    SILA MUAT NAIK BUKTI PEMBAYARAN ANDA
</p>
<p class="text-justify">
    Terima kasih. Kami memerlukan bayaran berjumlah
    <b>RM {{ number_format($payment->amount, 2, ".", ",") }}</b>
    daripada anda untuk kami mengemaskini rekod
    pembayaran. Sila muat naik bukti pembayaran setelah membuat pembayaran ke akaun berikut:
</p>

<table class="table" style="margin-top: 20px; width: 100%;">
    <tr>
        <td style="width: 40%;" valign="middle">NAMA BANK</td>
        <td style="width: 60%;" valign="middle" align="right"><b>BANK ISLAM</b></td>
    </tr>
    <tr>
        <td style="width: 40%;" valign="middle">NAMA AKAUN</td>
        <td style="width: 60%; text-transform: uppercase;" valign="middle" align="right">
            <b>Koperasi Pembangunan Pengguna Islam Malaysia Berhad</b>
        </td>
    </tr>
    <tr>
        <td>NO. AKAUN</td>
        <td align="right"><b>12261010013350</b></td>
    </tr>
</table>

<form method="POST" enctype="multipart/form-data" action="/langganan-syer/pembayaran-manual?order_ref={{ $payment->order_ref }}">

    @csrf
    <div class="form-layout form-layout-2">
        <div class="row no-gutters">
            <div class="col">
                <div class="form-group">
                    <label class="form-control-label">No. Rujukan</label>
                    <input type="text" class="form-control" value="{{ $payment->order_ref }}" readonly style="color: black;" name="ref_no" id="ref_no" aria-describedby="btnGroupAddon2">
                </div>
            </div>
        </div>
        <div class="row no-gutters">
            <div class="col">
                <div class="form-group">
                    <label class="form-control-label">Nama</label>
                    <input type="text" class="form-control" value="{{ $payment->name }}" readonly style="color: black;" name="name" id="name" aria-describedby="btnGroupAddon2">
                </div>
            </div>
        </div>
        <div class="row no-gutters">
            <div class="col">
                <div class="form-group">
                    <label class="form-control-label">No. MYKAD</label>
                    <input type="text" class="form-control" value="{{ $payment->ic }}" style="color: black;" readonly name="ic" id="ic" aria-describedby="btnGroupAddon2">
                </div>
            </div>
        </div>
        <div class="row no-gutters">
            <div class="col">
                <div class="form-group">
                    <label class="form-control-label">No. Telefon</label>
                    <input type="text" class="form-control" value="+6{{ $payment->phone }}" readonly style="color: black;" name="phone" id="phone" aria-describedby="btnGroupAddon2">
                </div>
            </div>
        </div>
        <div class="row no-gutters">
            <div class="col">
                <div class="form-group">
                    <label class="form-control-label">E-mel</label>
                    <input type="text" class="form-control" readonly style="color: black;" value="{{ ($payment->email == '') ? '' : $payment->email }}" name="email" id="email" aria-describedby="btnGroupAddon2">
                </div>
            </div>
        </div>
    </div>
    <div class="form-group">
        <table class="table" style="margin-top: 20px;">


            @if($payment->first_time_fees > 0)
            <tr>
                <td>Fi Pendaftaran Anggota Baharu</td>
                <td align="right"><b>RM {{ number_format($payment->first_time_fees,2) }}</b></td>
            </tr>
            @endif
            @if($payment->first_time_share > 0)
            <tr>
                <td>Langganan Modal Syer</td>
                <td align="right"><b>RM {{ number_format($payment->first_time_share,2) }}</b></td>
            </tr>
            @endif
            @if($payment->additional_share > 0)
            <tr>
                <td>Langganan Tambahan</td>
                <td align="right"><b>RM {{ number_format($payment->additional_share,2) }}</b></td>
            </tr>
            @endif


        </table>

        <div align="center" style="color: black; margin-top: 10px;">
            <h6>JUMLAH KESELURUHAN</h6>
        </div>
        <div align="center" style="color: black; margin-top: -10px;">
            <h3>
                RM {{ number_format($payment->amount, 2, ".", ",") }}</h3>
        </div>
        <div align="center" style="color: black; margin-top: 10px;">
            <p>Sila daftar semula di link <a href="https://www.anggota.koppim.com.my">www.anggota.koppim.com.my</a> selepas bayaran langganan syer dibuat dan muatnaik resit bayaran anda</p>
        </div>


    </div>


    <div>
        {{-- <table class="table" style="margin-top: 20px; width: 100%;">

                <tr>
                    <td colspan="2" align="center">




                   <div align="center">SILA MUAT NAIK BUKTI PEMBAYARAN ANDA</div>



               <input type="file" class="form-control"  style="color: black;"      name="payment_proof" id="paymentProof">
            <small align="center" style="text-transform:uppercase">Jumlah bukti pembayaran mestilah sama dengan jumlah yang dimasukkan bagi mengelakkan kelewatan pada pengesahan langganan syer.</small>

                    </td>
                </tr>

            </table> --}}


    </div>


    <div style="margin-top:10px;margin-bottom:10px;text-align:center">
        <button type="submit" class="btn btn-primary btn-block btn-signin" style="margin-bottom:10px;">
            MUAT NAIK
        </button>
        <p class="mt-2 text-center">
            Perlu Bantuan? Ada Pertanyaan?
            <a href="http://bit.ly/KoPPIM-Pertanyaan">
                <b class="text-danger">KLIK DISINI!</b>
            </a>
        </p>
    </div>

    <div style="margin-top:10px;margin-bottom:10px;text-align:center">
        <a href="/langganan-syer/pengesahan?order_ref={{ $payment->order_ref }}">
            Kembali Ke Pembayaran Atas Talian
        </a>
    </div>
</form>

@endsection

@section('js')
<script>
    $(document).ready(function() {
        $('#phone').mask('000000000000');
        $('#amount_display').mask('00000.00');
        $('#ic').mask('000000000000');
        //$('#myModal').modal();
    });


    function numberWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }

    function emailIsValid(email) {
        var re = /^(?:[a-z0-9!#$%&amp;'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&amp;'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])$/;
        return re.test(email);
    }

    function submit_donation() {
        var amount = $('#amount').val();
        var name = $('#name').val();
        var ic = $('#ic').val();
        var phone = $('#phone').val();
        var email = $('#email').val();

        var phonelenght = phone.length;

        if (amount < 100 || amount === "") {
            Swal.fire({
                type: 'error'
                , title: 'Maklumat Tidak Lengkap'
                , confirmButtonColor: '#1b84e7'
                , text: 'Sila Masukkan Jumlah Syer'
            , });

        } else if (name === "") {
            Swal.fire({
                type: 'error'
                , title: 'Maklumat Tidak Lengkap'
                , confirmButtonColor: '#1b84e7'
                , text: 'Maklumat Nama Anda'
            , });
        } else if (phone === "") {
            Swal.fire({
                type: 'error'
                , title: 'Maklumat Tidak Lengkap'
                , confirmButtonColor: '#1b84e7'
                , text: 'Masukkan No. Telefon Yang Lengkap'
            , });
        } else if (phonelenght < 9) {
            Swal.fire({
                type: 'error'
                , title: 'Maklumat Tidak Lengkap'
                , confirmButtonColor: '#1b84e7'
                , text: 'Masukkan No. Telefon Yang Lengkap'
            , });
        } else if (ic.length < 12) {
            Swal.fire({
                type: 'error'
                , title: 'Maklumat Tidak Lengkap'
                , confirmButtonColor: '#1b84e7'
                , text: 'Masukkan No. MyKad Yang Lengkap'
            , });
        } else if (email === "") {
            Swal.fire({
                type: 'error'
                , title: 'Maklumat Tidak Lengkap'
                , confirmButtonColor: '#1b84e7'
                , text: 'Maklumat Emel Tidak Lengkap'
            , });
        } else if (emailIsValid(email) === false) {
            Swal.fire({
                type: 'error'
                , title: 'Maklumat Tidak Lengkap'
                , confirmButtonColor: '#1b84e7'
                , text: 'Emel ' + email + ' Tidak Sah'
            , });
        } else {
            document.dona.submit();
        }

    }

</script>

@endsection
