@extends('investor.layoutfront')

@section('css')

@endsection

@section('content')
<div align="center">
    <h4 class="signin-title-primary" style="text-transform: uppercase;">
        semakan status keanggotaan KoPPIM
    </h4>
</div>
<hr />
<p align="center" style="text-transform: uppercase; color: #000000;">
    SILA masukkan no. mykad pelanggan untuk menyemak status keanggotaan koppim
</p>
<form action="{{route('member.getuser')}}" enctype="multipart/form-data">

    <div class="form-group">
        <input type="text" id="ic" name="ic" class="form-control text-center" placeholder="No. MYKAD Pelanggan">
    </div>
    <div class="form-group mb-0 text-center">
        <button type="submit" class="btn btn-primary btn-block btn-signin" style="margin-bottom:5px;">Semak Keanggotaan</button>
        <a href="/" class="btn btn-block btn-secondary">{{request()->get('ic') != '' ? 'Teruskan':'Kembali'}} Ke Log Masuk</a></p>
    </div>
</form>
@if(isset($info))
<hr>
<div class="mt-3">
    <h5 class="font-weight-bold text-dark mb-2">Maklumat Keanggotaan Pelanggan</h5>
    <form>
        <div class="form-layout form-layout-2">
            <div class="row no-gutters">
                <div class="col">
                    <div class="form-group">
                        <label class="form-control-label">Nama Penuh</label>
                        <input type="text" class="form-control" value="{!! $info->name ?? '' !!}" disabled style="color: black;" name="name" id="name" aria-describedby="btnGroupAddon2">
                    </div>
                </div>
            </div>
            <div class="row no-gutters">
                <div class="col">
                    <div class="form-group">
                        <label class="form-control-label">No. MYKAD (Paparan No. MYKAD disulitkan bagi tujuan keselamatan)</label>
                        <input type="text" class="form-control" value="{{ Helper::ccMasking($info->ic,'X') ?? ''}}" style="color: black;" disabled name="ic" id="ic" aria-describedby="btnGroupAddon2">
                    </div>
                </div>
            </div>
            <div class="row no-gutters">
                <div class="col">
                    <div class="form-group">
                        <label class="form-control-label">Tarikh Permohonan</label>
                        <input type="text" class="form-control" value="{{$info->created_at ? $info->created_at->format('d F Y H:m'):''}}" disabled style="color: black;" name="phone" id="phone" aria-describedby="btnGroupAddon2">
                    </div>
                </div>
            </div>
            <div class="row no-gutters">
                <div class="col">
                    <div class="form-group">
                        <label class="form-control-label">Status</label>
                        {!! Helper::anggota_status_text($info->status) !!}
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
@endif

@endsection

@section('js')
<script>
    $(document).ready(function() {
        $('#ic').mask('000000000000');
    });

    @if(session('anggota-tidak-wujud'))

    Swal.fire({
        icon: "error"
        , title: "Harap Maaf"
        , text: `Anda belum mendaftar sebagai Anggota KoPPIM. Sila ke halaman pendaftaran untuk daftar sebagai Anggota KoPPIM.`
        ,confirmButtonText: 'Daftar Sekarang'
    }).then((result) => {
        if (result.isConfirmed) {
            location.href = "/langganan-syer"
        }
    });
    @endif

</script>

@endsection
