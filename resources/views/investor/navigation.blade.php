<?php

if ($tab == "main") {
    $main = "active";
    $penyata = "";
    $pelaburan = "";
    $profil = "";
    $affiliate = "";
} else if ($tab == "penyata") {
    $main = "";
    $penyata = "active";
    $pelaburan = "";
    $profil = "";
    $affiliate = "";
} else if ($tab == "pelaburan") {
    $main = "";
    $penyata = "";
    $pelaburan = "active";
    $profil = "";
    $affiliate = "";
} else if ($tab == "profil") {
    $main = "";
    $penyata = "";
    $pelaburan = "";
    $profil = "active";
    $affiliate = "";
} else if ($tab == "affiliate") {
    $main = "";
    $penyata = "";
    $pelaburan = "";
    $profil = "";
    $affiliate = "active";
} else {
    $main = "";
    $penyata = "";
    $pelaburan = "";
    $profil = "";
    $affiliate = "";
}
?>

<div class="slim-navbar">
    <div class="container">
        <ul class="nav">
            <li class="nav-item {{ $main }}">
                <a class="nav-link" href="/home">
                    <i class="icon fa fa-home"></i>
                    <span>Utama</span>
                </a>
            </li>

            <li class="nav-item {{ $pelaburan }}">
                <a class="nav-link " href="/syer">
                    <i class="icon fa fa-university"></i>
                    <span>Syer</span>
                </a>
            </li>
            <li class="nav-item  {{ $penyata }}">
                <a class="nav-link" href="/dividen">
                    <i class="icon fa fa-percent"></i>
                    <span>Dividen</span>
                </a>

            </li>
            @if(env('ENABLE_AFFILIATES',false))
            @if(auth()->user()->status == 1)
            <li class="nav-item with-sub {{ $affiliate }}">
                <a class="nav-link" href="#" data-toggle="dropdown">
                    <i class="icon fa fa-handshake-o"></i>
                    <span>Program Affiliate</span>
                </a>
                <div class="sub-item">
                    <ul>
                        <li><a href="/affiliates">{{auth()->user()->is_affiliate == 'yes' ? 'Paparan Prestasi':'Mohon Sebagai Ejen Affliate'}}</a></li>
                        @if(auth()->user()->is_affiliate == 'yes')
                        <li><a href="/affiliates/penyata-komisen">Penyata Komisen</a></li>
                        @endif
                    </ul>
                </div><!-- dropdown-menu -->
            </li>
            @endif
            @endif
            <li class="nav-item {{ $profil }}">
                <a class="nav-link " href="/profile">
                    <i class="icon fa fa-user"></i>
                    <span>Profil</span>
                </a>
            </li>
        </ul>
    </div><!-- container -->
</div><!-- slim-navbar -->
