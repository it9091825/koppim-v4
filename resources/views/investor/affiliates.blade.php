@extends('investor.layouts')
@section('css')
    <link href="/investor/lib/chartist/css/chartist.css" rel="stylesheet">

@endsection
@section('navigation')

    @include('investor.navigation',['tab'=>'affiliate'])

@endsection

@section('content')

    <div class="slim-mainpanel">
        <div class="container">
            <div class="slim-pageheader">
                <ol class="breadcrumb slim-breadcrumb">
                    <li class="breadcrumb-item">Program Affiliates</li>
                    <li class="breadcrumb-item active" aria-current="page"><a
                            href="/affiliates">{{auth()->user()->is_affiliate == 'yes' ? 'Paparan Prestasi':'Mohon Sebagai Ejen Affliate'}}</a>
                    </li>
                </ol>
                <h6 class="slim-pagetitle">Program Affiliates</h6>
            </div><!-- slim-pageheader -->

            @if(Auth::user()->is_affiliate != 'yes')
                @include('affiliates.non-agent-dashboard')
            @else
                @include('affiliates.agent-commissions-display')
                @include('affiliates.investor-stats')
                @include('affiliates.investor-transactions')
            @endif

        </div><!-- container -->
    </div><!-- slim-mainpanel -->
@include('affiliates.redeem-commissions-modal')
@endsection

@section('js')
    <script>
        $(document).ready(function () {
            var e = document.getElementById('mainRight');
            var height = e.offsetHeight;
            document.getElementById('mainLeft').style.height = height;
        });

        function copylink() {
            /* Get the text field */
            var copyText = document.getElementById("linkpromote");

            /* Select the text field */
            copyText.select();
            copyText.setSelectionRange(0, 99999); /*For mobile devices*/

            /* Copy the text inside the text field */
            document.execCommand("copy");
        }

        $('#month').val('{{ $current_date->format('n')}}');
        @if(request()->get('month') == 'all')
        $('#month').val('all');
        @endif
        $('#year').val('{{ $current_date->format('Y')}}');

        function copycode() {
            /* Get the text field */
            var copyText = document.getElementById("code");

            /* Select the text field */
            copyText.select();
            copyText.setSelectionRange(0, 99999); /*For mobile devices*/

            /* Copy the text inside the text field */
            document.execCommand("copy");
        }

        function onchangemonth() {
            var month = $('#month').val();
            var year = $('#year').val();
            window.location = "/affiliates?month=" + month + "&year=" + year + "";
        }

    </script>

@endsection
