@extends('investor.layouts')

@section('navigation')

@include('investor.navigation',['tab'=>'profil'])

@endsection
@section('css')
<style>
    .nav-pills .nav-link.active {
        color: #1b84e7 !important;
        background-color: transparent !important;
        font-weight: bold !important;
    }

    .select2 {
        width: 100% !important;
    }

</style>
@endsection
@section('content')
<?php
if($user->status != 9999)
{
//    $user->status == 0 || $user->status == 96 || $user->status == 99
  $disable = '';
  $open = true;
}
else
{
  $disable = 'disabled="disabled"';
  $open = false;
}
?>

<div class="slim-mainpanel">
    <div class="container">
        <div class="slim-pageheader">
            <ol class="breadcrumb slim-breadcrumb">
                <li class="breadcrumb-item"><a href="#"></a></li>
            </ol>
            <h6 class="slim-pagetitle d-flex align-items-center"><span class="mr-2">PROFIL SAYA</span> {!!Helper::anggota_status_text(auth()->user()->status)!!}</h6>
        </div>
        @if(auth()->user()->status != 2 && auth()->user()->status != 1)
        <div class="row">
            <div class="col">
                <p class="text-dark">Sila lengkapkan semua ruangan bertanda (<span class="tx-danger">*</span>) sebelum menghantar permohonan.</p>
            </div>
        </div>
        @endif
        <div class="row">
            <div class="col-sm-12 col-md-4 mb-3">
                @include('investor.semakan-permohonan.nav-tab')
                {{-- <button type="button" data-toggle="modal" data-target="#staticBackdrop" class="btn btn-danger btn-block mt-3">Permohonan Berhenti Keanggotaan</button> --}}
            </div>
            <div class="col-sm-8 col-md-8">
                @include('investor.semakan-permohonan.tab-content')
            </div>
        </div>
    </div>
</div>
{{-- @include('investor.berhenti-keanggotaan') --}}
@endsection


@section('js')
<script>

    var text_max = 180;
    $('#textarea_feedback').html(text_max + ' patah perkataan sahaja');

    $('#mesej').keyup(function() {
        var text_length = $('#mesej').val().length;
        var text_remaining = text_max - text_length;

        $('#textarea_feedback').html('Huruf Berbaki: ' + text_remaining);
    });


    $("#org_agama option[value='{{$user->religion}}']").prop("selected", "selected");
    $("#org_jantina option[value='{{ $user->sex }}']").prop("selected", "selected");
    $("#org_kahwin option[value='{{ $user->marital_status }}']").prop("selected", "selected");
    $("#org_bangsa option[value='{{$user->race}}']").prop("selected", "selected");
    $("#org_state option[value='{{$user->state}}']").prop("selected", "selected");
    $("#org_parlimen option[value='{{$user->parliament}}']").prop("selected", "selected");
    $("#org_bank_name option[value='{{$user->bank_name}}']").prop("selected", "selected");
    $("#org_dividen option[value='{{ $user->blacklist_dividend }}']").prop("selected", "selected");
    $("#org_blacklist option[value='{{ $user->blacklist }}']").prop("selected", "selected");
    $("#pendapatan_bulanan option[value='{{ $user->monthly_income }}']").prop("selected", "selected");

    // $('#org_bank_name').val('{{ $user->bank_name }}');

    @if($user->dob !== NULL)

    $("#org_dob_day option[value='{{$user->dob->format('j')}}']").prop("selected", "selected");
    $("#org_dob_month option[value='{{$user->dob->format('n')}}']").prop("selected", "selected");
    $("#org_dob_year option[value='{{$user->dob->format('Y')}}']").prop("selected", "selected");

    @endif

    $('.select2').select2({
        theme: 'bootstrap4'
    });


    $(".row-toggle").click(function(e) {
        e.preventDefault();
        var rowid = $(this).attr("data-row-counter");
        $('#row' + rowid).toggle();
    });

    $("#checkAll").click(function() {
        $('input:checkbox').not(this).prop('checked', this.checked);
    });

</script>
<script>
    $('a[data-toggle="pill"]').on('shown.bs.tab', function(e) {
        localStorage.setItem('activeTab', $(e.target).attr('href'));
    });

    var activeTab = localStorage.getItem('activeTab');
    if (activeTab) {
        $('.nav-pills a[href="' + activeTab + '"]').tab('show');
    }

</script>
@endsection
