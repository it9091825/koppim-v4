@extends('investor.layouts')

@section('navigation')

@include('investor.navigation',['tab'=>'profil'])

@endsection

@section('content')


 <?php
	if($user->status == 0 || $user->status == 96 || $user->status == 99)
	{
		$disable = '';
		$open = true;
	}
	else
	{
		$disable = 'disabled="disabled"';
		$open = false;
	}
	?>
 <div class="slim-mainpanel">
<div class="container pd-t-50">

		@if(Auth::user()->email_verified_at == NULL)

					<div style="margin-top: -20px;">

											    <div class="alert alert-info" align="center">
											     Pengesahan Emel telah dihantar ke <b>{{ Auth::user()->email }}</b>. Sila semak emel anda dan klik pautan didalam emel tersebut.
											     <br><br>
											     <a href="/investor/edit/resend-email" class="btn btn-info btn-xs">HANTAR KEMBALI</a>
											    </div>

				            </div>

				            @endif


        <div class="d-sm-flex align-items-center justify-content-between mg-b-20 mg-lg-b-25 mg-xl-b-30">
          <div>
            @if($open == true)
            <h4 class="mg-b-0 tx-spacing--1" style="text-transform: uppercase;">PERMOHONAN KEAHLIAN</h4>
            @else
             <h4 class="mg-b-0 tx-spacing--1" style="text-transform: uppercase;">PROFIL KEAHLIAN</h4>
            @endif
          </div>

        </div>







         <div class="row row-xs">
			<div class="col-lg-6 col-xl-6 mg-t-10">
            <div class="card">
              <div class="card-header ">
                <h6 class="mg-b-0">Info</h6>

              </div><!-- card-header -->
              <div class="card-body pos-relative pd-0">
                <div class="" >


                    	<table class="table table-striped">
	                    	<form enctype="multipart/form-data"   method="post" action="/investor/edit/address"  >
		                    	@csrf


	                    	<tr>
		                    	<td style="width: 30%;">Status</td>
		                    	<td style="width: 1%;">:</td>
		                    	<td style="width: 69%;">{!! Helper::anggota_status_text($user->status) !!}
		                    	</td>
	                    	</tr>
		                    <tr>
		                    	<td style="width: 30%;">No. Kad Pengenalan</td>
		                    	<td style="width: 1%;">:</td>
		                    	<td style="width: 69%;">{{ $user->ic }}</td>
	                    	</tr>
		                    <tr>
		                    	<td style="width: 30%;">Nama Penuh</td>
		                    	<td style="width: 1%;">:</td>
		                    	<td style="width: 69%;"><input type="text" {!! $disable !!} class="form-control" name="org_name"  value="{{ $user->name }}"></td>
	                    	</tr>




	                    	<tr>
		                    	<td style="width: 30%;">Alamat Baris 1</td>
		                    	<td style="width: 1%;">:</td>
		                    	<td style="width: 69%;"><input type="text" {!! $disable !!} class="form-control" name="org_address_line_1" placeholder="Alamat Baris 1" value="{{ $user->org_address_line_1 }}"></td>
	                    	</tr>
	                    	<tr>
		                    	<td style="width: 30%;">Alamat Baris 2</td>
		                    	<td style="width: 1%;">:</td>
		                    	<td style="width: 69%;"><input type="text" {!! $disable !!} class="form-control" name="org_address_line_2" placeholder="Alamat Baris 2" value="{{ $user->org_address_line_2 }}"></td>
	                    	</tr>
	                    	<tr>
		                    	<td style="width: 30%;">Alamat Baris 3</td>
		                    	<td style="width: 1%;">:</td>
		                    	<td style="width: 69%;"><input type="text" {!! $disable !!} class="form-control" name="org_address_line_3" placeholder="Alamat Baris 3" value="{{ $user->org_address_line_3 }}"></td>
	                    	</tr>

	                    	<tr>
		                    	<td style="width: 30%;">Poskod</td>
		                    	<td style="width: 1%;">:</td>
		                    	<td style="width: 69%;"><input type="text" {!! $disable !!} class="form-control" name="org_poscode" placeholder="Poskod" value="{{ $user->org_poscode }}"></td>
	                    	</tr>

	                    	<tr>
		                    	<td style="width: 30%;">Daerah</td>
		                    	<td style="width: 1%;">:</td>
		                    	<td style="width: 69%;"><input type="text" {!! $disable !!} class="form-control" name="org_city" placeholder="Daerah" value="{{ $user->org_city }}"></td>
	                    	</tr>

	                    	<tr>
		                    	<td style="width: 30%;">Negeri</td>
		                    	<td style="width: 1%;">:</td>
		                    	<td style="width: 69%;">
			                    	<select class="form-control" {!! $disable !!} style="text-transform: uppercase;" name="org_state" id="org_state">
				                    	@if($user->org_state == NULL)
				                    	<option value="">[Negeri]</option>
                                          @endif
                                                <option value="Johor" >Johor</option>
						<option value="KEDAH" >Kedah</option>
						<option value="KELANTAN" >Kelantan</option>
						<option value="MELAKA" >Melaka</option>
						<option value="NEGERI SEMBILAN" >Negeri Sembilan</option>
						<option value="PAHANG" >Pahang</option>
						<option value="PERAK" >Perak</option>
						<option value="PERLIS" >Perlis</option>
						<option value="PULAU PINANG" >Pulau Pinang</option>
						<option value="SABAH" >Sabah</option>
						<option value="SARAWAK" >Sarawak</option>
						<option value="SELANGOR" >Selangor</option>
						<option value="TERENGGANU" >Terengganu</option>
						<option value="W.P. KUALA LUMPUR" >W.P. Kuala Lumpur</option>
						<option value="W.P. LABUAN" >W.P. Labuan</option>
						<option value="W.P. PUTRAJAYA" >W.P. Putrajaya</option>


			                    	</select>
			                    	</td>
	                    	</tr>
	                    	@if($open == true)
	                    	<tr >
		                    	<td colspan="3" align="right"><button type="submit" class="btn btn-primary">Kemaskini</button></td>
	                    	</tr>
	                    	@endif
	                    	</form>

	                    	<tr >
		                    	<td colspan="3"><b>MUAT NAIK KAD PENGENALAN</b></td>
	                    	</tr>

	                    	<form enctype="multipart/form-data"   method="post" action="/organisation/edit/attachment"  >
		                    	@csrf
	                    	<tr>
		                    	<td style="width: 30%;">Lampiran</td>
		                    	<td style="width: 1%;">:</td>
		                    	<td style="width: 69%;">@if($user->org_attachment == NULL)
			                    	<div style="margin-bottom: 20px;">[TIADA LAMPIRAN]</div>
			                    	@else
			                    	<div style="margin-bottom: 20px;"><a target="_blank" href="{{ env('DO_SPACES_FULL_URL').$user->org_attachment }}">[LAMPIRAN]</a></div>
			                    	@endif

			                    @if($open == true)
			                    	<div class="custom-file">
  <input type="file" class="custom-file-input" name="attachment" id="attachment">
  <label class="custom-file-label" for="customFile">Choose file</label>
</div>
								@endif

		                    	</td>
	                    	</tr>
	                    	@if($open == true)
	                    	<tr >
		                    	<td colspan="3" align="right"><button type="submit" class="btn btn-primary">Kemaskini</button></td>
	                    	</tr>
	                    	@endif
	                    	</form>

	                    	<tr >
		                    	<td colspan="3"><b>KONTAK</b></td>
	                    	</tr>
	                    	<form enctype="multipart/form-data"   method="post" action="/investor/edit/contact"  >
		                    	@csrf

	                    	<tr>
		                    	<td style="width: 30%;">Emel

			                    	@if($user->email_verified_at == NULL)
			                    	<i class="fa fa-times" style="color: red;" aria-hidden="true"></i>
			                    	@else
			                    	<i class="fa fa-check-circle" style="color: green;"  aria-hidden="true"></i>
			                    	@endif
		                    	</td>
		                    	<td style="width: 1%;">:</td>
		                    	<td style="width: 69%;"><input type="email" {!! $disable !!} class="form-control" name="email" placeholder="Emel Organisasi" value="{{ $user->email }}"></td>
	                    	</tr>


	                    	<tr>
		                    	<td style="width: 30%;">No. Telefon Bimbit
			                    	<i class="fa fa-check-circle" style="color: green;"  aria-hidden="true"></i>

		                    	</td>
		                    	<td style="width: 1%;">:</td>
		                    	<td style="width: 69%;"><input type="text" {!! $disable !!} class="form-control" name="officer_mobile"  value="{{ $user->mobile_no }}"></td>
	                    	</tr>

	                    	@if($open == true)
	                    	<tr >
		                    	<td colspan="3" align="right"><button type="submit" class="btn btn-primary">Kemaskini</button></td>
	                    	</tr>
	                    	@endif
	                    	</form>





                    	</table>



                </div>


              </div><!-- card-body -->
            </div><!-- card -->
          </div>
          <div class="col-lg-6 col-xl-6 mg-t-10">
            <div class="card">
              <div class="card-header pd-t-20 pd-b-0 bd-b-0">
                <h6 class="mg-b-5">Semak</h6>
              </div><!-- card-header -->
              <div class="card-body ">

                <div class="row">
                  <table class="table table-striped">

	                    	<form enctype="multipart/form-data"   method="post" action="/investor/edit/submit"  >
		                    	@csrf
	                    	<tr >
		                    	<td style="width: 30%;">STATUS</td>
		                    	<td style="width: 1%;">:</td>
		                    	<td style="width: 69%;">{!! Helper::anggota_status_text($user->status) !!}</td>
	                    	</tr>
	                    	@if($user->status_reason !== NULL)
	                    	<tr style="background-color: #ffd11c;" >

		                    	<td style="width: 100%;" colspan="3">
			                    	<b>JUSTIFIKASI:</b><br><br>
			                    	{!! nl2br($user->status_reason) !!}</td>
	                    	</tr>
	                    	@endif
	                    	<tr >
		                    	<td colspan="3"><b>SEMAKAN PERMOHONAN</b></td>
	                    	</tr>
	                    	<tr >
		                    	<td style="width: 75%;">PENGESAHAN EMEL<br><span class="text-muted" style="font-size: 11px;">SILA SEMAK EMEL YANG TELAH DIHANTAR</span></td>
		                    	<td style="width: 1%;">:</td>
		                    	<td style="width: 24%;">
			                    	@if($user->email_verified_at == NULL)
			                    	<i class="fa fa-times" style="color: red;" aria-hidden="true"></i>
			                    	@else
			                    	<i class="fa fa-check-circle" style="color: green;" aria-hidden="true"></i>
			                    	@endif
			                    	</td>
	                    	</tr>
	                    	<tr >
		                    	<td style="width: 30%;">INFO PEMOHON<br><span class="text-muted" style="font-size: 11px;">ALAMAT,POSKOD,NEGERI</span></td>
		                    	<td style="width: 1%;">:</td>
		                    	<td style="width: 69%;">
			                    	@if($user->org_name == NULL || $user->org_address_line_1 == NULL || $user->org_poscode == NULL || $user->org_city == NULL || $user->org_state == NULL)
			                    	<i class="fa fa-times" style="color: red;" aria-hidden="true"></i></td>
			                    	@else
			                    	<i class="fa fa-check-circle" style="color: green;" aria-hidden="true"></i>
			                    	@endif
	                    	</tr>
	                    	<tr >
		                    	<td style="width: 30%;">MUAT NAIK GAMBAR KAD PENGENALAN<br><span class="text-muted" style="font-size: 11px;">GAMBAR DEPAN KAD PENGENALAN</span></td>
		                    	<td style="width: 1%;">:</td>
		                    	<td style="width: 69%;">
			                    	@if($user->org_attachment == NULL)
			                    	<i class="fa fa-times" style="color: red;" aria-hidden="true"></i></td>
			                    	@else
			                    	<i class="fa fa-check-circle" style="color: green;" aria-hidden="true"></i>
			                    	@endif
		                    	</td>
	                    	</tr>
	                    	<tr >
		                    	<td style="width: 30%;">KONTAK<br><span class="text-muted" style="font-size: 11px;">TELEFON,EMEL</span></td>
		                    	<td style="width: 1%;">:</td>
		                    	<td style="width: 69%;">
			                    	@if($user->email == NULL || $user->mobile_no == NULL)
			                    	<i class="fa fa-times" style="color: red;" aria-hidden="true"></i></td>
			                    	@else
			                    	<i class="fa fa-check-circle" style="color: green;" aria-hidden="true"></i>
			                    	@endif


			                    	</td>
	                    	</tr>





	                    	@if($open == true)
	                    	<tr >
		                    	<td colspan="3" align="center"><button type="submit" class="btn btn-success">HANTAR</button></td>
	                    	</tr>
	                    	@endif
	                    	</form>
                  </table>


                </div><!-- row -->
              </div><!-- card-body -->
            </div><!-- card -->
          </div>
         </div>



      </div><!-- container -->
      </div>

@endsection
