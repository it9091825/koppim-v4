@card(['title'=>'Hantar Permohonan'])
@if($user->status != 2)
<h5 class="text-dark font-weight-bold">Deklarasi</h5>

<p class="text-justify text-dark">Segala yang telah saya perakui di sini adalah benar dan mematuhi undang-undang kecil koperasi. <br>Jika terdapat sebarang maklumat yang tidak tepat dan diragui, pihak KoPPIM berhak membatalkan atau tidak meluluskan permohonan anda</p>
        <p class="text-danger">Permohonan anda akan diproses dalam masa 7-14 hari bekerja.</p>
<form enctype="multipart/form-data" method="post" action="{{route('investor.profile.submit')}}">

    @csrf
    {{-- <div class="row mg-b-25">
        <div class="col">
            <div class="form-group mg-b-10-force">
                <label for="">Mesej Kepada Admin KoPPIM</label>
                <textarea name="mesej" id="mesej" cols="30" rows="3" class="form-control" placeholder="TULISKAN MESEJ RINGKAS UNTUK ADMIN KOPPIM SEKIRANYA ADA" maxlength="180"></textarea>
                <p id="textarea_feedback"></p>
            </div>
        </div><!-- col-8 -->
    </div><!-- row --> --}}
    <div class="form-layout-footer pull-right">
        @if($open == true)
        <button type="submit" class="btn btn-success bd-0 font-weight-bold d-flex justify-content-between align-items-center flash">
            HANTAR PERMOHONAN
            <svg style="margin-left:10px; width:20px; -webkit-transform: rotate(90deg); -moz-transform: rotate(90deg); -o-transform: rotate(90deg);" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 19l9 2-9-18-9 18 9-2zm0 0v-8"></path>
            </svg>
        </button>
        @endif
    </div><!-- form-layout-footer -->
</form>
@else
<div class="d-flex justify-content-center">
    <img src="{{asset('investor/img/mail-sent.svg')}}" alt="" width="200px">
</div>

<div class="d-flex justify-content-center mt-3">
    <div class="col-6">
        <h4 class="text-center">Terima kasih! Permohonan anda telah berjaya dihantar. Pihak KoPPIM sedang menyemak permohonan anda. </h4>

    </div>
</div>
@endif
@endcard

@section('css')
<style>
    .flash {
        -webkit-animation: glowing 1500ms infinite;
        -moz-animation: glowing 1500ms infinite;
        -o-animation: glowing 1500ms infinite;
        animation: glowing 1500ms infinite;
    }

    @-webkit-keyframes glowing {
        0% {
            background-color: #B20000;
            -webkit-box-shadow: 0 0 3px #B20000;
        }

        50% {
            background-color: #FF0000;
            -webkit-box-shadow: 0 0 40px #FF0000;
        }

        100% {
            background-color: #B20000;
            -webkit-box-shadow: 0 0 3px #B20000;
        }
    }

    @-moz-keyframes glowing {
        0% {
            background-color: #B20000;
            -moz-box-shadow: 0 0 3px #B20000;
        }

        50% {
            background-color: #FF0000;
            -moz-box-shadow: 0 0 40px #FF0000;
        }

        100% {
            background-color: #B20000;
            -moz-box-shadow: 0 0 3px #B20000;
        }
    }

    @-o-keyframes glowing {
        0% {
            background-color: #B20000;
            box-shadow: 0 0 3px #B20000;
        }

        50% {
            background-color: #FF0000;
            box-shadow: 0 0 40px #FF0000;
        }

        100% {
            background-color: #B20000;
            box-shadow: 0 0 3px #B20000;
        }
    }

    @keyframes glowing {
        0% {
            background-color: #B20000;
            box-shadow: 0 0 3px #B20000;
        }

        50% {
            background-color: #FF0000;
            box-shadow: 0 0 40px #FF0000;
        }

        100% {
            background-color: #B20000;
            box-shadow: 0 0 3px #B20000;
        }
    }

</style>
@endsection
