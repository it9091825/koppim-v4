@card(['title'=>'Info Perbankan'])
<form enctype="multipart/form-data" method="post" action="/investor/edit/bank">
    @csrf

    <div class="row mg-b-25">


        <div class="col-lg-6">
            <div class="form-group mg-b-10-force">
                <label class="form-control-label">Nama Bank: <span class="tx-danger">*</span></label>
                <select {!! $disable !!} name="org_bank_name" id="org_bank_name" value="" class="form-control select2 @error('org_bank_name') is-warning @enderror" placeholder="">
                    <option value="">[Pilih Bank]</option>
                    <option value="AFFIN BANK">AFFIN BANK</option>
                    <option value="ALLIANCE BANK">ALLIANCE BANK</option>
                    <option value="AL-RAJHI BANK">AL-RAJHI BANK</option>
                    <option value="AGROBANK">AGROBANK</option>
                    <option value="AMBANK">AMBANK </option>
                    <option value="BNP PARIBAS MALAYSIA">BNP PARIBAS MALAYSIA </option>
                    <option value="BANGKOK BANK">BANGKOK BANK </option>
                    <option value="BANK OF AMERICA MALAYSIA">BANK OF AMERICA MALAYSIA </option>
                    <option value="BANK OF CHINA (MALAYSIA)">BANK OF CHINA (MALAYSIA) </option>
                    <option value="BANK ISLAM">BANK ISLAM </option>
                    <option value="BANK MUAMALAT">BANK MUAMALAT</option>
                    <option value="BANK RAKYAT">BANK RAKYAT</option>
                    <option value="BANK SIMPANAN NASIONAL">BANK SIMPANAN NASIONAL </option>
                    <option value="CIMB BANK">CIMB BANK</option>
                    <option value="CHINA CONSTRUCTION BANK (MALAYSIA)">CHINA CONSTRUCTION BANK (MALAYSIA) </option>
                    <option value="CITIBANK BERHAD">CITIBANK BERHAD</option>
                    <option value="DEUTSCHE BANK (MALAYSIA)">DEUTSCHE BANK (MALAYSIA) </option>
                    <option value="HSBC BANK MALAYSIA">HSBC BANK MALAYSIA </option>
                    <option value="HONG LEONG BANK">HONG LEONG BANK </option>
                    <option value="INDIA INTERNATIONAL BANK (MALAYSIA)">INDIA INTERNATIONAL BANK (MALAYSIA)</option>
                    <option value="INDUSTRIAL AND COMMERCIAL BANK OF CHINA (MALAYSIA)">INDUSTRIAL AND COMMERCIAL BANK OF
                        CHINA (MALAYSIA)</option>
                    <option value="J.P MORGAN CHASE BANK">J.P MORGAN CHASE BANK</option>
                    <option value="KUWAIT FINANCE HOUSE">KUWAIT FINANCE HOUSE </option>
                    <option value="MUFG BANK (MALAYSIA)">MUFG BANK (MALAYSIA)</option>
                    <option value="MALAYAN BANKING (MAYBANK)">MALAYAN BANKING (MAYBANK)</option>
                    <option value="MIZUHO BANK (MALAYSIA)">MIZUHO BANK (MALAYSIA)</option>
                    <option value="OCBC BANK">OCBC BANK</option>
                    <option value="PUBLIC BANK">PUBLIC BANK</option>
                    <option value="RHB BANK">RHB BANK</option>
                    <option value="STANDARD CHARTERED BANK">STANDARD CHARTERED BANK</option>
                    <option value="SUMITOMO MITSUI BANKING">SUMITOMO MITSUI BANKING</option>
                    <option value="THE BANK OF NOVA SCOTIA">THE BANK OF NOVA SCOTIA</option>
                    <option value="UNITED OVERSEAS BANK">UNITED OVERSEAS BANK</option>
                </select>
            </div>
        </div><!-- col-8 -->

        <div class="col-lg-6">
            <div class="form-group">
                <label class="form-control-label">No. Akaun Bank: <span class="tx-danger">*</span></label>
                <input {!! $disable !!} class="form-control @error('org_bank_acc') is-warning @enderror" type="text" name="org_bank_acc" value="{{ old('org_bank_acc') ?? $user->bank_account }}">
            </div>
        </div><!-- col-4 -->

        <div class="col-lg-6">
            <div class="form-group">
                <label class="form-control-label">Nama Pemegang Akaun: <span class="tx-danger">*</span></label>
                <input type="text" name="bank_account_name" class="form-control @error('bank_account_name') is-warning @enderror" value="{!! old('bank_account_name') ?? $user->bank_account_name !!}">
            </div>
        </div><!-- col-4 -->

        <div class="col-lg-6">
            <div class="form-group">
                <label class="form-control-label">Status Senarai Hitam: <span class="tx-danger">*</span></label>

                <select {!! $disable !!} class="form-control select2 @error('org_blacklist') is-warning @enderror" name="org_blacklist" id="org_blacklist" data-placeholder="[Status Senarai Hitam]">

                    <option value="">[Status Senarai Hitam]</option>

                    <option value="1">YA</option>
                    <option value="0">TIDAK</option>

                </select>
            </div>
        </div><!-- col-4 -->



    </div><!-- row -->

    <div class="form-layout-footer text-right">
        @if($open == true)
        <button class="btn btn-primary bd-0 font-weight-bold text-uppercase">SIMPAN</button>
        @endif
    </div><!-- form-layout-footer -->
</form>
@endcard
