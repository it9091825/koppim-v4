@card(['title' => 'Info Kontak'])
<form enctype="multipart/form-data" method="post" action="/investor/edit/contact">
    @csrf
    <div class="row mg-b-25">


        <div class="col-lg-4">
            <div class="form-group mg-b-10-force">
                <label class="form-control-label">No. Tel. Bimbit: <span class="tx-danger">*</span></label>
                <input {!! $disable !!} class="form-control @error('mobile_no') is-warning @enderror" type="text" name="mobile_no" value="{{ old('mobile_no') ??$user->mobile_no }}">
            </div>
        </div><!-- col-8 -->
        <div class="col-lg-4">
            <div class="form-group">
                <label class="form-control-label">Alamat Emel: <span class="tx-danger">*</span></label>
                <input {!! $disable !!} class="form-control @error('email') is-warning @enderror" type="text" name="email" value="{{ old('email') ?? $user->email }}">
            </div>
        </div><!-- col-4 -->
        <div class="col-lg-4">
            <div class="form-group">
                <label class="form-control-label">No. Tel. Rumah: </label>

                <input {!! $disable !!} class="form-control @error('house_tel') is-warning @enderror" type="text" name="house_tel" value="{{ old('house_tel') ?? $user->home_no }}">

            </div>
        </div><!-- col-4 -->



    </div><!-- row -->

    <div class="form-layout-footer text-right">
        @if($open == true)
        <button class="btn btn-primary bd-0 font-weight-bold text-uppercase">SIMPAN</button>
        @endif
    </div><!-- form-layout-footer -->
</form>
@endcard
