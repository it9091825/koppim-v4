@card(['title'=>'Info Alamat'])
<form enctype="multipart/form-data" method="post" action="/investor/edit/address">
    @csrf
    <div class="row mg-b-25">
        <div class="col-lg-4">
            <div class="form-group">
                <label class="form-control-label">Alamat Baris 1: <span class="tx-danger">*</span></label>
                <input {!! $disable !!} class="form-control @error('org_address_line_1') is-warning @enderror" type="text" name="org_address_line_1" value="{{ old('org_address_line_1') ?? $user->address_line_1 }}">
            </div>
        </div><!-- col-4 -->
        <div class="col-lg-4">
            <div class="form-group">
                <label class="form-control-label">Alamat Baris 2: <span class="tx-danger">*</span></label>

                <input {!! $disable !!} class="form-control @error('org_address_line_2') is-warning @enderror" type="text" name="org_address_line_2" value="{{ old('org_address_line_2') ?? $user->address_line_2 }}">

            </div>
        </div><!-- col-4 -->
        <div class="col-lg-4">
            <div class="form-group">
                <label class="form-control-label">Alamat Baris 3:</label>

                <input {!! $disable !!} class="form-control" type="text" name="org_address_line_3" value="{{ old('org_address_line_3') ?? $user->address_line_3 }}">

            </div>
        </div><!-- col-4 -->

        <div class="col-lg-4">
            <div class="form-group">
                <label class="form-control-label">Poskod: <span class="tx-danger">*</span></label>

                <input {!! $disable !!} class="form-control @error('org_poscode') is-warning @enderror" type="text" name="org_poscode" value="{{ old('org_poscode') ?? $user->postcode }}">

            </div>
        </div><!-- col-4 -->

        <div class="col-lg-4">
            <div class="form-group">
                <label class="form-control-label">Bandar: <span class="tx-danger">*</span></label>

                <input {!! $disable !!} class="form-control @error('org_city') is-warning @enderror" type="text" name="org_city" value="{{ old('org_city') ?? $user->city }}">

            </div>
        </div><!-- col-4 -->

        <div class="col-lg-4">
            <div class="form-group mg-b-10-force">
                <label class="form-control-label">Negeri: <span class="tx-danger">*</span></label>

                <select {!! $disable !!} class="form-control select2 @error('org_state') is-warning @enderror" name="org_state" id="org_state" data-placeholder="[Negeri]">
                    @include('include.state-options')
                </select>
            </div>
        </div><!-- col-4 -->

        <div class="col">
            <div class="form-group mg-b-10-force">
                <label class="form-control-label">Kawasan Parlimen: <span class="tx-danger">*</span></label>
                <select {!! $disable !!} class="form-control select2 @error('org_parlimen') is-warning @enderror" name="org_parlimen" id="org_parlimen" placeholder="[Pilih Kawasan Parlimen]" style="width:100%">
                    @include('include.parliament-options')
                </select>
            </div>
        </div><!-- col-4 -->


    </div><!-- row -->

    <div class="form-layout-footer text-right">
        @if($open == true)
        <button class="btn btn-primary bd-0 font-weight-bold text-uppercase">SIMPAN</button>
        @endif
    </div><!-- form-layout-footer -->
</form>
@endcard
