@card(['title' => 'Info Mykad'])
<form enctype="multipart/form-data" method="post" action="/investor/edit/attachment">
    @csrf
    <div class="row mg-b-10">

        <div class="col-lg-12">
            <div class="form-group">
                <label class="form-control-label">No. Kad Pengenalan: </label>
                <b>{{ $user->ic }}</b>
            </div>
        </div><!-- col-8 -->
    </div>
    <div class="row">

        <div class="col-lg-6">
            <div class="form-group">
                <label class="form-control-label">Imej MyKad Bahagian Depan: <span class="tx-danger">*</span></label>


                @if($open == true)
                <div>
                    <input {!! $disable !!} type="file" name="mycard_front" id="mycard_front">
                </div>
                @endif

                @if($user->mykad_front == NULL)
                <div style="margin-top: 20px;">[TIADA LAMPIRAN]</div>
                @else
                <div style="margin-top: 20px;"><a class="d-flex align-items-center" target="_blank" href="{{ env('DO_SPACES_FULL_URL').$user->mykad_front }}">LIHAT LAMPIRAN <svg width="16px" class="ml-2" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M10 6H6a2 2 0 00-2 2v10a2 2 0 002 2h10a2 2 0 002-2v-4M14 4h6m0 0v6m0-6L10 14"></path>
                        </svg></a></div>
                @endif

            </div>
        </div><!-- col-8 -->

    </div>
    <hr>
    {{-- <div class="row">
        <div class="col-lg-6">
            <div class="form-group ">
                <label class="form-control-label">MyKad Belakang: <span class="tx-danger">*</span></label>


                @if($open == true)
                <div>
                    <input {!! $disable !!} type="file" name="mycard_back" id="mycard_back">
                </div>
                @endif

                @if($user->mykad_back == NULL)
                <div style="margin-top: 20px;">[TIADA LAMPIRAN]</div>
                @else
                <div style="margin-top: 20px;"><a class="d-flex align-items-center" target="_blank" href="{{ env('DO_SPACES_FULL_URL').$user->mykad_back }}">LIHAT LAMPIRAN <svg width="16px" class="ml-2" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M10 6H6a2 2 0 00-2 2v10a2 2 0 002 2h10a2 2 0 002-2v-4M14 4h6m0 0v6m0-6L10 14"></path>
                        </svg></a></div>
                @endif
            </div>
        </div><!-- col-4 -->
    </div><!-- row --> --}}

    <div class="form-layout-footer text-right">
        @if($open == true)
        <button class="btn btn-primary bd-0 font-weight-bold text-uppercase">SIMPAN</button>
        @endif
    </div><!-- form-layout-footer -->
</form>
@endcard
