@card(['title'=>'Info Pekerjaan'])
<form enctype="multipart/form-data" method="post" action="/investor/edit/jawatan">
    @csrf
    <div class="row mg-b-25">


        <div class="col-lg-6">
            <div class="form-group mg-b-10-force">
                <label class="form-control-label">Jawatan Pekerjaan: <span class="tx-danger">*</span></label>

                <input {!! $disable !!} class="form-control @error('jawatan_pekerjaan') is-warning @enderror" type="text" name="jawatan_pekerjaan" value="{{ old('jawatan_pekerjaan')??$user->job }}">

            </div>
        </div><!-- col-8 -->

        <div class="col-lg-6">
            <div class="form-group">
                <label class="form-control-label">Pendapatan Bulanan Di Bawah: <span class="tx-danger">*</span></label>
                <select {!! $disable !!} name="pendapatan_bulanan" id="pendapatan_bulanan" class="form-control select2 @error('pendapatan_bulanan') is-warning @enderror" style="width:100%">
                    <option value="RM 0 - RM 1000">RM 0 - RM 1000</option>
                    <option value="RM 1000 - RM 2000">RM 1000 - RM 2000</option>
                    <option value="RM 3000 - RM 4000">RM 3000 - RM 4000</option>
                    <option value="RM 4000 - RM 5000">RM 4000 - RM 5000</option>
                    <option value="RM 5000 - RM 10000">RM 5000 - RM 10000</option>
                    <option value="RM 10000 - RM 20000">RM 10000 - RM 20000</option>
                    <option value="RM 20000 - RM 30000">RM 20000 - RM 30000</option>
                    <option value="RM 30000 - RM 40000">RM 30000 - RM 40000</option>
                    <option value="RM 40000 - RM 50000">RM 40000 - RM 50000</option>
                    <option value="RM 50000 Keatas">RM 50000 Keatas</option>
                </select>
            </div>
        </div><!-- col-4 -->



    </div><!-- row -->

    <div class="form-layout-footer text-right">
        @if($open == true)
        <button class="btn btn-primary bd-0 font-weight-bold text-uppercase">SIMPAN</button>
        @endif
    </div><!-- form-layout-footer -->
</form>
@endcard
