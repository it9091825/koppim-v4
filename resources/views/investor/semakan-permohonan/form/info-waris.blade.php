@card(['title'=>'Info Waris'])
<form enctype="multipart/form-data" method="post" action="/investor/edit/waris">
    @csrf
    <div class="row mg-b-25">


        <div class="col-lg-6">
            <div class="form-group mg-b-10-force">
                <label class="form-control-label">Nama Penuh Penama (Seperti dalam MYKAD): <span class="tx-danger">*</span></label>
                <input {!! $disable !!} class="form-control @error('org_nama_waris') is-warning @enderror" type="text" name="org_nama_waris" value="{{ old('org_nama_waris') ??$user->heir_name }}">
            </div>
        </div><!-- col-8 -->

        <div class="col-lg-6">
            <div class="form-group">
                <label class="form-control-label">No. MyKad Penama: <span class="tx-danger">*</span></label>

                <input {!! $disable !!} class="form-control @error('org_no_mykad_waris') is-warning @enderror" type="text" name="org_no_mykad_waris" value="{{ old('org_no_mykad_waris')?? $user->heir_ic }}">

            </div>
        </div><!-- col-4 -->

        <div class="col-lg-6">
            <div class="form-group">
                <label class="form-control-label">No. Tel Bimbit Penama: <span class="tx-danger">*</span></label>

                <input {!! $disable !!} class="form-control @error('org_no_tel_bimbit_waris') is-warning @enderror" type="text" name="org_no_tel_bimbit_waris" value="{{ old('org_no_tel_bimbit_waris') ?? $user->heir_phone }}">

            </div>
        </div><!-- col-4 -->

        <div class="col-lg-6">
            <div class="form-group">
                <label class="form-control-label">Hubungan Penama: <span class="tx-danger">*</span></label>

                <input {!! $disable !!} class="form-control @error('org_hubungan') is-warning @enderror" type="text" name="org_hubungan" value="{{ old('org_hubungan') ?? $user->heir_relationship }}">

            </div>
        </div><!-- col-4 -->
        <div class="col-lg-12">
            <div class="form-group">
                <label class="form-control-label">Nota</label>
                <p class="text-danger">Maklumat waris di sini berperanan sebagai wakil yang akan menguruskan urusan anggota selepas anggota tersebut meninggal dunia.</p>
            </div>
        </div><!-- col-4 -->
    </div><!-- row -->

    <div class="form-layout-footer text-right">
        @if($open == true)
        <button class="btn btn-primary bd-0 font-weight-bold text-uppercase">SIMPAN</button>
        @endif
    </div><!-- form-layout-footer -->
</form>
@endcard
