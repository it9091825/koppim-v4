@card(['title'=>'Mesej Kepada Admin'])
<form enctype="multipart/form-data" method="post" action="">
    @csrf
    <div class="row mg-b-25">
        <div class="col">
            <div class="form-group mg-b-10-force">
                <textarea name="mesej" id="mesej" cols="30" rows="3" class="form-control" placeholder="TULISKAN NOTA RINGKAS SEKIRANYA ADA" maxlength="180"></textarea>
                <p id="textarea_feedback"></p>
            </div>
        </div><!-- col-8 -->
    </div><!-- row -->

    <div class="form-layout-footer text-right">
        @if($open == true)
        <button class="btn btn-primary bd-0 font-weight-bold">SIMPAN</button>
        @endif
    </div><!-- form-layout-footer -->
</form>

@endcard
