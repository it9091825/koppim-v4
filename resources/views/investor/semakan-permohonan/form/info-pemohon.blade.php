@card(['title'=>'Info Peribadi'])
<form enctype="multipart/form-data" method="post" action="/investor/edit/info">
    @csrf
    <div class="row mg-b-25">

        <div class="col-lg-12">
            <div class="form-group">
                <label class="form-control-label">Nama Penuh: <span class="tx-danger">*</span></label>
                <input {!! $disable !!} class="form-control @error('org_name') is-warning @enderror" type="text" name="org_name" value="{{ old('org_name') ?? $user->name }}">
            </div>
        </div><!-- col-8 -->


        <div class="col-lg-6">
            <div class="form-group">
                <label class="form-control-label">Jantina: <span class="tx-danger">*</span></label>
                <select {!! $disable !!} class="form-control select2 @error('org_jantina') is-warning @enderror" name="org_jantina" id="org_jantina" data-placeholder="[Pilih Jantina]">
                    <option value="">[Pilih Jantina]</option>
                    <option value="LELAKI">Lelaki</option>
                    <option value="PEREMPUAN">Perempuan</option>
                </select>
            </div>
        </div><!-- col-4 -->

        <div class="col-lg-6">
            <div class="form-group">
                <label class="form-control-label">Bangsa: <span class="tx-danger">*</span></label>
                <select {!! $disable !!} class="form-control select2 @error('org_bangsa') is-warning @enderror" name="org_bangsa" id="org_bangsa" data-placeholder="[Bangsa]">
                    <option value="">[Bangsa]</option>
                    <option value="MELAYU">Melayu</option>
                    <option value="BUMIPUTERA">Bumiputera</option>
                </select>
            </div>
        </div><!-- col-4 -->



        <div class="col-lg-6">
            <div class="form-group">
                <label class="form-control-label">Agama: <span class="tx-danger">*</span></label>
                <select {!! $disable !!} class="form-control select2 @error('org_agama') is-warning @enderror" name="org_agama" id="org_agama" data-placeholder="[Agama]">
                    <option value="ISLAM">Islam</option>
                </select>
            </div>
        </div><!-- col-4 -->


        <div class="col-lg-6">
            <div class="form-group">
                <label class="form-control-label">Status Perkahwinan: <span class="tx-danger">*</span></label>
                <select {!! $disable !!} class="form-control select2 @error('org_kahwin') is-warning @enderror" name="org_kahwin" id="org_kahwin" data-placeholder="[Status Perkahwinan]">
                    <option value="">[Status Perkahwinan]</option>
                    <option value="BUJANG">Bujang</option>
                    <option value="BERKAHWIN">Berkahwin</option>
                    <option value="BERCERAI">Bercerai</option>
                </select>
            </div>
        </div><!-- col-4 -->


        <div class="col-lg-4">
            <div class="form-group">
                <label class="form-control-label">Tarikh Lahir (Hari): <span class="tx-danger">*</span></label>
                <select {!! $disable !!} class="form-control select2 @error('org_dob_day') is-warning @enderror" name="org_dob_day" id="org_dob_day" data-placeholder="[Tarikh Lahir Hari]">
                    <option value="">[Tarikh Lahir Hari]</option>
                    @for ($i = 1; $i <= 31; $i++) <option value="{{ $i }}">{{ $i }}</option>
                        @endfor
                </select>
            </div>
        </div><!-- col-4 -->


        <div class="col-lg-4">
            <div class="form-group ">
                <label class="form-control-label">Tarikh Lahir (Bulan): <span class="tx-danger">*</span></label>
                <select {!! $disable !!} class="form-control select2 @error('org_dob_month') is-warning @enderror" name="org_dob_month" id="org_dob_month" data-placeholder="[Tarikh Lahir Bulan]" >
                    <option value="">[Tarikh Lahir Bulan]</option>
                    @for ($i = 1; $i <= 12; $i++) <option value="{{ $i }}">{{ $i }}</option>
                        @endfor
                </select>
            </div>
        </div><!-- col-4 -->

        <div class="col-lg-4">
            <div class="form-group ">
                <label class="form-control-label">Tarikh Lahir (Tahun): <span class="tx-danger">*</span></label>
                <select {!! $disable !!} class="form-control select2 @error('org_dob_year') is-warning @enderror" name="org_dob_year" id="org_dob_year" data-placeholder="[Tarikh Lahir Tahun]">
                    <option value="">[Tarikh Lahir Tahun]</option>
                    @for ($i = 1920; $i <= 2020; $i++) <option value="{{ $i }}">{{ $i }}</option>
                        @endfor
                </select>
            </div>
        </div><!-- col-4 -->
        <div class="col-lg-12" style="display: none;">
            <div class="form-group mg-b-10-force">
                <label class="form-control-label">Saksi Pencadang: </label>
                <input {!! $disable !!} class="form-control @error('org_saksi') is-warning @enderror" type="text" name="org_saksi" id="org_saksi" value="{{ old('org_saksi') ?? $user->witness }}">
            </div>
        </div><!-- col-8 -->
    </div><!-- row -->
    <div class="form-layout-footer text-right">
        @if($open == true)
        <button class="btn btn-primary bd-0 font-weight-bold text-uppercase">SIMPAN</button>
        @endif
    </div><!-- form-layout-footer -->
</form>
@endcard
