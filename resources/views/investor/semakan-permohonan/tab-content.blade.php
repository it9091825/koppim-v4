<div class="tab-content" id="v-pills-tabContent">
    <div class="tab-pane fade show active" id="info-pemohon" role="tabpanel" aria-labelledby="info-pemohon-tab">
        @include('investor.semakan-permohonan.form.info-pemohon')
    </div>
    <div class="tab-pane fade" id="info-mykad" role="tabpanel" aria-labelledby="info-mykad-tab">
        @include('investor.semakan-permohonan.form.info-mykad')
    </div>
    <div class="tab-pane fade" id="info-alamat" role="tabpanel" aria-labelledby="info-alamat-tab">
        @include('investor.semakan-permohonan.form.info-alamat')
    </div>
    <div class="tab-pane fade" id="info-kontak" role="tabpanel" aria-labelledby="info-kontak-tab">
        @include('investor.semakan-permohonan.form.info-kontak')
    </div>
    <div class="tab-pane fade" id="info-pekerjaan" role="tabpanel" aria-labelledby="info-pekerjaan-tab">
        @include('investor.semakan-permohonan.form.info-pekerjaan')
    </div>
    <div class="tab-pane fade" id="info-perbankan" role="tabpanel" aria-labelledby="info-perbankan-tab">
        @include('investor.semakan-permohonan.form.info-perbankan')
    </div>
    <div class="tab-pane fade" id="info-waris" role="tabpanel" aria-labelledby="info-waris-tab">
        @include('investor.semakan-permohonan.form.info-waris')
    </div>
    <div class="tab-pane fade" id="mesej-admin" role="tabpanel" aria-labelledby="mesej-admin-tab">
        @include('investor.semakan-permohonan.form.mesej-admin')
    </div>
    <div class="tab-pane fade" id="submit" role="tabpanel" aria-labelledby="submit-tab">
        @include('investor.semakan-permohonan.form.submit')
    </div>
</div>
