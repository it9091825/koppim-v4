<div class="card">
    <div class="card-header text-dark font-weight-bold text-uppercase">
        Semakan Permohonan
    </div>
    <div class="card-body">
        <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
            {{-- info pemohon --}}
            <a class="nav-link active text-uppercase d-flex justify-content-between align-items-center" id="info-pemohon-tab" data-toggle="pill" href="#info-pemohon" role="tab" aria-controls="info-pemohon" aria-selected="true">info peribadi
                @include('investor.semakan-permohonan.status-checker.info-pemohon')</a>
            {{-- info mykad --}}
            <a class="nav-link text-uppercase d-flex justify-content-between align-items-center" id="info-mykad-tab" data-toggle="pill" href="#info-mykad" role="tab" aria-controls="info-mykad" aria-selected="false">info mykad
                @include('investor.semakan-permohonan.status-checker.info-pemohon')</a>
            {{-- info alamat --}}
            <a class="nav-link text-uppercase d-flex justify-content-between align-items-center" id="info-alamat-tab" data-toggle="pill" href="#info-alamat" role="tab" aria-controls="info-alamat" aria-selected="false">info alamat
                @include('investor.semakan-permohonan.status-checker.info-alamat')
            </a>
            {{-- info kontak --}}
            <a class="nav-link text-uppercase d-flex justify-content-between align-items-center" id="info-kontak-tab" data-toggle="pill" href="#info-kontak" role="tab" aria-controls="info-kontak" aria-selected="false">info kontak
                @include('investor.semakan-permohonan.status-checker.info-kontak')
            </a>
            {{-- info pekerjaan --}}
            <a class="nav-link text-uppercase d-flex justify-content-between align-items-center" id="info-pekerjaan-tab" data-toggle="pill" href="#info-pekerjaan" role="tab" aria-controls="info-pekerjaan" aria-selected="false">info pekerjaan
                @include('investor.semakan-permohonan.status-checker.info-pekerjaan')
            </a>
            {{-- info perbankan --}}
            <a class="nav-link text-uppercase d-flex justify-content-between align-items-center" id="info-perbankan-tab" data-toggle="pill" href="#info-perbankan" role="tab" aria-controls="info-perbankan" aria-selected="false">info perbankan
                @include('investor.semakan-permohonan.status-checker.info-perbankan')
            </a>
            {{-- info waris --}}
            <a class="nav-link text-uppercase d-flex justify-content-between align-items-center" id="info-waris-tab" data-toggle="pill" href="#info-waris" role="tab" aria-controls="info-waris" aria-selected="false">info waris
                @include('investor.semakan-permohonan.status-checker.info-waris')
            </a>
            @if(auth()->user()->status != 2 && auth()->user()->status != 1)
            <a class="nav-link text-uppercase" id="submit-tab" data-toggle="pill" href="#submit" role="tab" aria-controls="submit" aria-selected="false"><span class="btn btn-primary btn-block font-weight-bold d-flex justify-content-center align-items-center">Hantar Permohonan</span>
            </a>
            @endif
        </div>

    </div>
</div>
