@extends('investor.layouts')

@section('navigation')

@include('investor.navigation',['tab'=>'penyata'])

@endsection

@section('content')


<div class="slim-mainpanel">
    <div class="container pd-t-50">
        @maintenance(['description'=>'Laman ini masih dalam pembangunan dan penyelenggaraan. Notifikasi melalui emel akan dihantar selepas pengaktifan laman ini.'])
        @endmaintenance
    </div><!-- container -->
</div><!-- slim-mainpanel -->

@endsection
