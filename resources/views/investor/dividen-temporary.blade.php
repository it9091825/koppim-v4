@extends('investor.layouts')

@section('navigation')

@include('investor.navigation',['tab'=>'penyata'])

@endsection

<style>
video {
  width: 100%;
  height: auto;
}
</style>

@section('content')
    <div class="slim-mainpanel">
                <div class="container">
                <div class="d-flex justify-content-between align-items-center">
                    <div class="slim-pageheader">
                        <div>
                            <ol class="breadcrumb slim-breadcrumb">
                                <li class="breadcrumb-item"><a href="#"></a></li>
                            </ol>
                            <h6 class="slim-pagetitle d-flex align-items-center"><span class="mr-2">MAKLUMAN TENTANG DIVIDEN</span></h6>
                        </div>
                    </div>
                </div>
                <div class="row no-gutters" style="margin-bottom: 20px;">
                    <div class="col-sm-12">
                        <div class="card">
                            <h2 class="slim-logo mt-3" style="margin-left:auto; margin-right:auto;"><a href=""><img src="{{asset('img/logo-koppim.png')}}" width="150" /></a></h2>
                            <div class="row mt-3">
                                <div class="col">
                                    <p class="text-justify ml-3 mr-3">Assalammualaikum wbh.<br>
Sidang pejuang ekonomi yang dimuliakan Allah SWT dengan iman. <br><br>
Dengan penuh hormat takzim dan rendah diri saya mendoakan kesihatan dan kesejahteraan YBhg Dato’/ Tuan/ Puan/ Cik sekeluarga serta semoga sentiasa diberikan perlindungan dan rahmat Allah SWT. <br> <br>
Seperti mana yang sudah di umumkan oleh Pengerusi KOPPIM pada sesi Pengumuman Khas 29 Jun 2021, pembahagian insentif akan di lakukan dalam masa seperti di umumkan. Pihak pengurusan ingin meminta untuk ahli memuat naik aplikasi Malakat Eco di Playstore Android atau Apple App Store kemudian kongsi QR Code di google form di sediakan di bawah bagi pihak KOPPIM membuat pengagihan insentif <br><br>
<a href="https://forms.gle/HBr2QTj5ox5pg9mw5" target="_blank">Klik Link ini ke Google Form</a>
 <br><br>
Pihak kami akan menyediakan langkah-langkah untuk cara untuk memuatnaik dan mendaftar sebagai ahli Malakat Eco untuk rujukan Tuan / Puan.
Sekian, Terima Kasih.</p>
                                </div>
                            </div>
                            <div class="row mt-3">
                                <div class="col">
                                    <p class="text-justify ml-3 mr-3">Daripada <br>
Pihak Pengurusan KoPPIM</p>
                                </div>

                            </div>

                        </div>

                    </div>

                </div>

                 <div class="container">
                <div class="d-flex justify-content-between align-items-center">
                    <div class="slim-pageheader">
                        <div>
                            <ol class="breadcrumb slim-breadcrumb">
                                <li class="breadcrumb-item"><a href="#"></a></li>
                            </ol>
                            <h6 class="slim-pagetitle d-flex align-items-center"><span class="mr-2">VIDEO TUTORIAL CARA MEMUAT NAIK APLIKASI MALAKAT ECO </span></h6>
                        </div>
                    </div>
                </div>
                <div class="row no-gutters" style="margin-bottom: 20px;">
                    <div class="col-sm-12">
                        <div class="card">
<video width="400" controls>
 <source src="{{asset('/video/tutorial-install-malakat-eco.mp4')}}" type="video/mp4">
  Your browser does not support HTML5 video.
</video>
                                <div class="col">

                                </div>
                            </div>
                            <div class="row mt-3">
                                <div class="col">

                                </div>

                            </div>

                        </div>

                    </div>

                </div>
            </div>
    </div>




@endsection


