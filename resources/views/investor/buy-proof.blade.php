@extends('investor.layoutfront')

@section('css')


@endsection

@section('content')

{{-- <div style="margin-bottom: 20px;" class="d-lg-none" align="center"> <a class="{{env('APP_URL')}}"><img src="https://www.koppim.com.my/wp-content/uploads/2020/06/Logo-KoPPIM-small.png" width="200" /></a> </div> --}}

<div align="center">
    <h4 class="signin-title-primary" style="text-transform: uppercase;">
        HANTAR BUKTI PEMBAYARAN
    </h4>
</div>

<form method="POST" name="dona" enctype="multipart/form-data" action="/hantar-bukti-pembayaran">
    @csrf
    <div class="row" style="margin-bottom: 40px;">

        <div class="col-lg-12">
            <hr />
            <p align="center" style="text-transform: uppercase; color: #000000;">
                SILA ISIKAN MAKLUMAT BERIKUT UNTUK MENGHANTAR BUKTI PEMBAYARAN.
            </p>

            @if(Session::has('success'))
            <div class="alert alert-success" style="padding:20px 30px;font-size:16px;">
                PENGHANTARAN BERJAYA. KEMASKINI AKAN DIBUAT DALAM TEMPOH 7-14 HARI BEKERJA.
            </div>
            @endif
            <div>
                <hr>
            </div>
            <div style="margin-bottom: 10px;">MAKLUMAT PERIBADI (SEKSYEN INI TIDAK BOLEH DIEDIT)</div>
            <div class="form-layout form-layout-2">
                @if(request()->get('order_ref'))
                <div class="row no-gutters">
                    <div class="col">
                        <div class="form-group">
                            <label class="form-control-label">No. Rujukan</label>
                            <input type="text" class="form-control" id="order_ref" name="order_ref" style="text-transform: uppercase;" aria-describedby="order_ref" placeholder="No. Rujukan KoPPIM" value="{{ request()->get('order_ref') ?? old('order_ref') }}" readonly>
                            @if ($errors->has('order_ref'))
                            <span class="form-text text-danger"><small class="form-text">{{ $errors->first('order_ref') }}</small></span>
                            @endif
                        </div>
                    </div>
                </div>
                @endif

                <div class="row no-gutters">
                    <div class="col">
                        <div class="form-group">
                            <label for="" class="form-control-label">Nama Penuh</label>
                            <input type="text" class="form-control" id="name" name="name" style="text-transform: uppercase;" aria-describedby="name" placeholder="Nama Penuh (Seperti MyKad)" value="{{  request()->get('name') ?? old('name') }}" readonly>
                            @if ($errors->has('name'))
                            <span class="form-text text-danger"><small class="form-text">{{ $errors->first('name') }}</small></span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row no-gutters">
                    <div class="col">
                        <div class="form-group">
                            <label for="" class="form-control-label">No. MYKAD</label>
                            <input type="text" class="form-control ic" id="ic" name="ic" style="text-transform: uppercase;" aria-describedby="ic" placeholder="No. MyKad" value="{{ request()->get('ic') ?? old('ic') }}" readonly>
                            @if ($errors->has('ic'))
                            <span class="form-text text-danger"><small class="form-text">{{ $errors->first('ic') }}</small></span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row no-gutters">
                    <div class="col">
                        <div class="form-group">
                            <label for="" class="form-control-label">No. telefon</label>
                            <input type="text" class="form-control" value="{{ request()->get('phone') ?? old('phone') }}" placeholder="NO. TELEFON" name="phone" id="phone" aria-describedby="btnGroupAddon2" readonly>
                        </div>
                    </div>
                </div>
                <div class="row no-gutters">
                    <div class="col">
                        <div class="form-group">
                            <label for="" class="form-control-label">E-mel</label>
                            <input type="email" class="form-control" id="email" name="email" aria-describedby="email" placeholder="ALAMAT EMEL" value="{{ request()->get('email') ?? old('email') }}" readonly>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <hr>
            </div>
            <div style="margin-bottom: 10px;">JUMLAH (DALAM RM)</div>
            <input type="text" class="form-control" style="color: black;margin-bottom: 10px;" id="jumlah" name="jumlah" value="{{old('jumlah')}}" data-inputmask="'alias': 'numeric', 'autoGroup': true, 'digits': 2,'rightAlign': false, 'digitsOptional': false, 'placeholder': '0'">
            <div style="margin-bottom: 10px;">MASA BAYARAN</div>
            <div style="margin-bottom: 10px;">
                <div style="width: 49%; float: left; margin-right: 1%;">
                    <select class="form-control" name="hour" id="hour">
                        <option value="">JAM</option>
                        <?php for($i = 0;$i <= 23;$i++){ ?>
                        <option value="{{ $i }}">{{ $i }}</option>
                        <?php } ?>
                    </select>
                </div>
                <div style="width: 50%;float: left;">
                    <select class="form-control" name="minute" id="minute">
                        <option value="">MINIT</option>
                        <?php for($i = 0;$i <= 60;$i++){ ?>
                        <option value="{{ $i }}">{{ $i }}</option>
                        <?php } ?>
                    </select>
                </div>
                <div style="clear: both;"></div>
            </div>
            <div style="margin-bottom: 10px;">TARIKH BAYARAN</div>
            <div>
                <div style="width: 29%; float: left; margin-right: 1%;">
                    <select class="form-control" name="day" id="day">
                        <option value="">HARI</option>
                        <?php for($i = 1;$i <= 31;$i++){ ?>
                        <option value="{{ $i }}">{{ $i }}</option>
                        <?php } ?>
                    </select>
                </div>
                <div style="width: 29%; float: left; margin-right: 1%;">
                    <select class="form-control" name="month" id="month">
                        <option value="">BULAN</option>
                        <?php for($i = 1;$i <= 12;$i++){ ?>
                        <option value="{{ $i }}">{{ $i }}</option>
                        <?php } ?>
                    </select>
                </div>
                <div style="width: 40%;float: left;">
                    <select class="form-control" name="year" id="year">
                        <option value="">TAHUN</option>
                        <?php for($i = 2020;$i <= date("Y");$i++){ ?>
                        <option value="{{ $i }}">{{ $i }}</option>
                        <?php } ?>
                    </select>
                </div>
                <div style="clear: both;"></div>
            </div>

            <div class="form-group" style="margin-top: 10px;">
                <div class="input-group">
                    <textarea class="form-control" name="note" id="note" placeholder="TULISKAN NOTA RINGKAS (SEKIRANYA ADA)" maxlength="180">{{old('note')}}</textarea>
                </div>
                <p id="textarea_feedback"></p>
            </div>
            <div style="margin-bottom:20px;text-align:center">
                <input type="file" class="form-control" style="color: black;" name="proof" id="proof">

            </div>

            <div align="center">
                <span><input type="checkbox" id="terms" checked="checked">
                    SAYA MENGESAHKAN BAHAWA SEMUA MAKLUMAT YANG TELAH SAYA MASUKKAN DI ATAS ADALAH BENAR DAN SAYA AKAN BERTANGGUNGJAWAB SEPENUHNYA SEKIRANYA ADA MAKLUMAT YANG TIDAK BENAR</span>
            </div>

            <div style="margin-top:50px;text-align:center; margin-top: 20px;">
                <button type="button" id="submitBtn" onclick="submit_donation()" class="btn btn-primary btn-block btn-signin" style="margin-bottom:10px;">TERUSKAN
                </button>
                <p class="mt-2 text-center">Perlu Bantuan? Ada Pertanyaan?
                    <a href="http://bit.ly/KoPPIM-Pertanyaan"><b class="text-danger">KLIK DISINI!</b></a>
                </p>
            </div>

        </div>
    </div>

</form>

@endsection

@section('js')
<script>
    $(document).ready(function() {
        $("#phone").on("load", function() {
            if (/^[1-9]/.test(this.value)) {
                this.value = this.value.replace(/^[1-9]/, "")
            }
        })
        $('#jumlah').inputmask();
        $('#phone').mask('000000000000');
        // $('#amount_display').mask('00000.00');
        $('#ic').mask('000000000000');

    });



    // var times = 1;
    // var times2 = 10;
    // var initprice = 100;
    // var initprice2 = 1000;
    // var total = 100;

    $('#terms').change(function() {
        $('#submitBtn').attr('disabled', !this.checked);
    });


    // function addShare() {


    //     times = times + 1;

    //     total = total + initprice;

    //     $('#amount').val(total);

    //     $('#big_price').html('RM ' + numberWithCommas(total) + '.00');

    // }


    // function minusShare() {
    //     if (total > 100) {
    //         times = times - 1;


    //         total = total - initprice;

    //         $('#amount').val(total);

    //         $('#big_price').html('RM ' + numberWithCommas(total) + '.00');
    //     }


    // }

    // $(document).ready(function () {
    //     var text_max = 180;
    //     $('#textarea_feedback').html(text_max + ' patah perkataan sahaja');

    //     $('#note').keyup(function () {
    //         var text_length = $('#note').val().length;
    //         var text_remaining = text_max - text_length;

    //         $('#textarea_feedback').html('Huruf Berbaki: ' + text_remaining);
    //     });
    // });

    // function addShare1000() {


    //     times = times + 1;

    //     total = total + initprice2;

    //     $('#amount').val(total);

    //     $('#big_price').html('RM ' + numberWithCommas(total) + '.00');

    // }


    // function minusShare1000() {
    //     if (total > 100) {
    //         times = times - 1;


    //         total = total - initprice2;

    //         $('#amount').val(total);

    //         $('#big_price').html('RM ' + numberWithCommas(total) + '.00');
    //     }


    // }

    function numberWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }

    function emailIsValid(email) {
        var re = /^(?:[a-z0-9!#$%&amp;'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&amp;'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])$/;
        return re.test(email);
    }

    function submit_donation() {
        var amount = $('#jumlah').val();
        var name = $('#name').val();
        var ic = $('#ic').val();
        var phone = $('#phone').val();
        var email = $('#email').val();
        var hour = $('#hour').val();
        var minute = $('#minute').val();
        var day = $('#day').val();
        var month = $('#month').val();
        var year = $('#year').val();
        var proof = $("#proof").val();


        var phonelenght = phone.length;

        if (amount < 100 || amount === "") {
            Swal.fire({
                type: 'error'
                , title: 'Maklumat Tidak Lengkap'
                , confirmButtonColor: '#1b84e7'
                , text: 'Sila Masukkan Jumlah Syer'
            , });

        } else if (name === "") {
            Swal.fire({
                type: 'error'
                , title: 'Maklumat Tidak Lengkap'
                , confirmButtonColor: '#1b84e7'
                , text: 'Maklumat Nama Anda'
            , });
        } else if (phone === "") {
            Swal.fire({
                type: 'error'
                , title: 'Maklumat Tidak Lengkap'
                , confirmButtonColor: '#1b84e7'
                , text: 'Masukkan No. Telefon Yang Lengkap'
            , });
        } else if (phonelenght < 9) {
            Swal.fire({
                type: 'error'
                , title: 'Maklumat Tidak Lengkap'
                , confirmButtonColor: '#1b84e7'
                , text: 'Masukkan No. Telefon Yang Lengkap'
            , });
        } else if (ic.length < 12) {
            Swal.fire({
                type: 'error'
                , title: 'Maklumat Tidak Lengkap'
                , confirmButtonColor: '#1b84e7'
                , text: 'Masukkan No. MyKad Yang Lengkap'
            , });
        } else if (email === "") {
            Swal.fire({
                type: 'error'
                , title: 'Maklumat Tidak Lengkap'
                , confirmButtonColor: '#1b84e7'
                , text: 'Maklumat Emel Tidak Lengkap'
            , });
        } else if (emailIsValid(email) === false) {
            Swal.fire({
                type: 'error'
                , title: 'Maklumat Tidak Lengkap'
                , confirmButtonColor: '#1b84e7'
                , text: 'Emel ' + email + ' Tidak Sah'
            , });
        } else if (hour === "") {
            Swal.fire({
                type: 'error'
                , title: 'Maklumat Tidak Lengkap'
                , confirmButtonColor: '#1b84e7'
                , text: 'Sila masukkan Jam pada Waktu Bayaran'
            , });
        } else if (minute === "") {
            Swal.fire({
                type: 'error'
                , title: 'Maklumat Tidak Lengkap'
                , confirmButtonColor: '#1b84e7'
                , text: 'Sila masukkan Minit pada Waktu Bayaran'
            , });
        } else if (day === "") {
            Swal.fire({
                type: 'error'
                , title: 'Maklumat Tidak Lengkap'
                , confirmButtonColor: '#1b84e7'
                , text: 'Sila masukkan Hari pada Tarikh Bayaran'
            , });
        } else if (month === "") {
            Swal.fire({
                type: 'error'
                , title: 'Maklumat Tidak Lengkap'
                , confirmButtonColor: '#1b84e7'
                , text: 'Sila masukkan Bulan pada Tarikh Bayaran'
            , });
        } else if (year === "") {
            Swal.fire({
                type: 'error'
                , title: 'Maklumat Tidak Lengkap'
                , confirmButtonColor: '#1b84e7'
                , text: 'Sila masukkan Tahun pada Tarikh Bayaran'
            , });
        } else if (proof === "") {
            Swal.fire({
                type: 'error'
                , title: 'Maklumat Tidak Lengkap'
                , confirmButtonColor: '#1b84e7'
                , text: 'Sila masukkan Bukti Bayaran (resit, dokumen atau sewaktu dengannya)'
            , });
        } else {
            document.dona.submit();
        }


    }


    // function change_donation_type(type) {
    //     if (type === 'recurring') {
    //         $('#type').val('recurring');

    //         $('#sumbangan_sekali').css({"background-color": "#22445d"});
    //         ;
    //         $('#sumbangan_berulang').css({"background-color": "#3fac87"});
    //         ;

    //         $('#amount').val('');
    //         $('#amount_display').val('');

    //         document.getElementById("amount_display").disabled = true;

    //     } else {
    //         $('#type').val('one-time');

    //         $('#sumbangan_sekali').css({"background-color": "#3fac87"});
    //         ;
    //         $('#sumbangan_berulang').css({"background-color": "#22445d"});
    //         ;

    //         $('#amount').val('');
    //         $('#amount_display').val('');
    //         document.getElementById("amount_display").disabled = false;
    //     }

    // }

</script>

@endsection
