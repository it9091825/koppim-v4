@extends('investor.layouts')

@section('navigation')

    @include('investor.navigation',['tab'=>'main'])

@endsection
@section('css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.min.css"
          integrity="sha512-/zs32ZEJh+/EO2N1b0PEdoA10JkdC3zJ8L5FTiQu82LR9S/rOQNfQN7U59U9BC12swNeRAz3HSzIL2vpp4fv3w=="
          crossorigin="anonymous"/>
@endsection

@section('content')


    <div class="slim-mainpanel">

        <div class="container pd-t-50">
            <div class="alert alert-info" role="alert">
    <h4 class="tx-inverse mg-b-15" style="text-align:center;text-transform: uppercase; font-weight: bold;"><a href="{{ asset('/Makluman-Pembayaran-Dividen.pdf') }}" target="_blank"> PERHATIAN SILA KLIK LINK INI UNTUK MAKLUMAN PEMBAYARAN DIVIDEN TAHUN KEWANGAN 2020 </a></h4>

</div>
            <div class="row align-items-center" style="margin-bottom:20px">

                <div class="col">
                    <h3 class="tx-inverse mg-b-15" style="text-transform: uppercase;">
                        Selamat
                        Datang, {{ strtoupper(Helper::getFirstNameOnly(auth()->user()->name)) }} </h3>
                        @if(auth()->user()->status == 1)
                        <span><b>No. Anggota Anda:</b> {{auth()->user()->no_koppim ?? ''}} </span>
                        @endif
                        <span class="d-flex align-items-center"><b class="mr-3">Status Keanggotaan Anda:</b> {!!Helper::anggota_status_text(auth()->user()->status)!!}</span>

                </div>
                <div class="col text-right">
                    <p class="text-gray-500">Log masuk terakhir
                        anda: {{ count($last_login) > 1 ? $last_login[1]['created_at']->format('d F Y H:m:s') : 'Tiada' }}</p>
                </div>
            </div><!-- row -->
            @include('investor.investor-pie-share')
            <div class="row no-gutters" style="margin-bottom: 20px;">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header d-flex align-items-center justify-content-between pd-y-5"
                             style="font-size:14px;">
                            <span class="text-uppercase font-weight-bold text-dark">Rekod Transaksi Langganan Syer Terkini</span>
                            <div class="card-option tx-24">
                                <a target="_blank" href="{{route('investor.transactions.logs.index')}}"
                                   class="btn btn-primary btn-sm text-white">Lihat Log Transaksi Langganan Syer</a>
                                @if(auth()->user()->status == 1)
                                    <a target="_blank" href="{{route('share.buy')}}"
                                       class="btn btn-success btn-sm text-white">Tambah Langganan Syer</a>
                                @endif
                            </div>
                        </div>
                        @if($latest_transaction)
                            <div class="card-body p-0">
                                <table class="table table-bordered mb-0">
                                    <thead>
                                    <tr>
                                        <th style="font-size:14px" class="text-center">status</th>

                                        <th style="font-size:14px" class="text-center">tarikh transaksi</th>
                                        @if($latest_transaction->first_time_fees)
                                            <th style="font-size:14px" class="text-center">fi pendaftaran</th>
                                        @endif
                                        @if($latest_transaction->first_time_share)
                                            <th style="font-size:14px" class="text-center">langganan modal syer</th>
                                        @endif
                                        <th style="font-size:14px" class="text-center">langganan syer tambahan</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if($latest_transaction)
                                        <tr>
                                            <td class="text-uppercase font-weight-bold text-center">
                                                {!! Helper::payment_status_text($latest_transaction->status)!!}
                                            </td>
                                            <td class="text-center">{!! $latest_transaction->payment_date ? $latest_transaction->payment_date->format("d/m/Y \<\b\\r\> h:i:s"): '' !!}</td>

                                            @if($latest_transaction->first_time_fees)
                                                <td class="text-center"> {{'RM '.number_format($latest_transaction->first_time_fees,2,'.',',') ?? 'Tidak Dapat Dikira Buat Masa Ini'}}</td>
                                            @endif

                                            @if($latest_transaction->first_time_share)
                                                <td class="text-center">{{'RM '.number_format($latest_transaction->first_time_share,2,'.',',') ?? 'Tidak Dapat Dikira Buat Masa Ini'}}</td>
                                            @endif

                                            <td class="text-center">{{'RM '.number_format($latest_transaction->additional_share,2,'.',',') ?? 'Tidak Dapat Dikira Buat Masa Ini'}}</td>
                                        </tr>
                                    @else
                                        @tablenoresult @endtablenoresult
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        @endif
                        <div class="card-footer text-danger text-center font-weight-bold">
                            Proses semakan dan pengesahan transaksi langganan syer akan mengambil masa 7-14 hari bekerja dan jumlah yang disemak tertakluk kepada kelulusan pihak KoPPIM.
                        </div>
                    </div>
                </div>

             <img src="{{asset('img/koppim-info.jpg')}}" class="img-fluid" alt="Responsive image">

            </div>
            <div class="row">
                <div class="col">
                    <div class="card card-blog-split">
                        <div class="row no-gutters flex-row-reverse">
                            <div class="col-12">
                                <h5 class="blog-title"><a href="">Ekosistem Baru Ekonomi Islam</a></h5>
                                <p class="blog-text pb-3">{!! nl2br($jsonData['content']) !!}</p>

                                <ul>
                                    @foreach($jsonData['telegram'] as $content)
                                        <li><strong>{{ $content['name'] }}</strong> (<a target="_blank" href="{{ $content['link'] }}">{{ $content['link'] }}</a>)</li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- container -->
    </div><!-- slim-mainpanel -->

@endsection

