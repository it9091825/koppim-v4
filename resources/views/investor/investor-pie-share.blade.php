<div class="row mb-4">
    <div class="col">
        <div class="card card-dash-chart-one">
            <div class="row no-gutters">
                <div class="col-lg-6">
                    <div class="left-panel">
                        <div class="active-visitor-wrapper">
                            <p class="text-dark font-weight-bold">Jumlah Langganan Syer Semasa</p>
                            <span class="d-flex align-items-baseline">
                                <small style="font-size:24px" class="text-dark">RM</small>
                                <h1 style="font-size:64px;" class="text-dark">{{number_format($total_share,2,'.',',') ?? 'Tidak Dapat Dikira Buat Masa Ini'}}</h1>
                            </span>
                        </div><!-- active-visitor-wrapper -->
                        <hr class="mg-t-30 mg-b-40">

                        <h6 class="visitor-operating-label">Taburan Jumlah Langganan Syer</h6>

                        <div>
                            <div class="d-flex align-items-center">
                                <span class="square-10 bg-purple rounded-circle"></span>
                                <span class="mg-l-10">Fi Pendaftaran</span>
                                <span class="mg-l-auto tx-lato tx-right">{{'RM '.number_format($first_time_fees,2,'.',',') ?? 'Tidak Dapat Dikira Buat Masa Ini'}}</span>
                            </div>
                            <div class="d-flex align-items-center mg-t-5">
                                <span class="square-10 bg-info rounded-circle"></span>
                                <span class="mg-l-10">Langganan Modal Syer</span>
                                <span class="mg-l-auto tx-lato tx-right">{{'RM '.number_format($first_time_share,2,'.',',') ?? 'Tidak Dapat Dikira Buat Masa Ini'}}</span>
                            </div>
                            <div class="d-flex align-items-center mg-t-5">
                                <span class="square-10 bg-warning rounded-circle"></span>
                                <span class="mg-l-10">Langganan Syer Tambahan</span>
                                <span class="mg-l-auto tx-lato tx-right">{{'RM '.number_format($additional_share,2,'.',',') ?? 'Tidak Dapat Dikira Buat Masa Ini'}}</span>
                            </div>
                        </div>
                    </div><!-- left-panel -->
                </div><!-- col-4 -->
                <div class="col-lg-6">
                    <div class="right-panel">
                        <h6 class="slim-card-title">Taburan Carta Pai Jumlah Langganan Syer</h6>
                        <canvas id="myChart" height="150"></canvas>
                    </div><!-- right-panel -->
                </div><!-- col-8 -->
            </div><!-- row -->
        </div>
    </div>
</div>
@push('script')
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.bundle.min.js"></script>
<script>
    var ctx = document.getElementById('myChart');
    var first_time_fees = "{{$first_time_fees}}";
    var first_time_share = "{{$first_time_share}}";
    var additional_share = "{{$additional_share}}";
    var myDoughnutChart = new Chart(ctx, {
        type: 'doughnut'
        , data: {
            labels: ['Fi Pendaftaran', 'Langganan Modal Syer', 'Langganan Syer Tambahan']
            , datasets: [{
                data: [first_time_fees, first_time_share, additional_share]
                , backgroundColor: [
                    '#6f42c1'
                    , '#5B93D3'
                    , '#F49917'
                ]
                , borderColor: [
                    '#6f42c1'
                    , '#5B93D3'
                    , '#F49917'
                ]
                , borderWidth: 1
            }]
        }
    });

</script>
@endpush
