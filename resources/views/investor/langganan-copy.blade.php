<!-- slim-pageheader -->
        {{-- <div class="section-wrapper" style="margin-bottom: 20px;">
            <label class="section-title">Transaksi Langganan Syer</label>
            <table class="table table-bordered text-center">
                <thead>
                    <tr>
                        <th class="text-center">TARIKH REKOD</th>
                        <th class="text-center">JUMLAH LANGGANAN</th><th class="text-center">JENIS BAYARAN</th>
                        <th class="text-center">TARIKH BAYARAN</th>
                        
                        <th class="text-center">TINDAKAN</th>
                    </tr>
                </thead>

                @forelse($payments as $key=>$payment)
                <tbody>
                    <tr>
                        <td class="text-center">{!! $payment->created_at ?$payment->created_at->format("d/m/Y \<\b\\r\> h:i:sa") : '' !!}</td>
                        <td class="text-center">{{ $payment->amount != 0.00 ?'RM '.number_format($payment->amount,2,'.',',') : 'MASIH BELUM DIKEMASKINI'}}</td>
                        
                        <td class="text-uppercase">{{$payment->channel}}</td>
                        <td class="text-center">{!! $payment->payment_date ?$payment->payment_date->format("d/m/Y \<\b\\r\> h:i:sa") : '' !!}</td>
                        </td>
                        <td>
                          @if($payment->channel == 'online' || $payment->channel == 'manual' || $payment->channel == 'ikoop') 
                            <a href="javascript:void;" class="row-toggle" data-row-counter="{{$key}}" data-toggle="tooltip" data-placement="auto" data-original-title="Papar maklumat lanjut">
                                <svg style="width:30px" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 9v3m0 0v3m0-3h3m-3 0H9m12 0a9 9 0 11-18 0 9 9 0 0118 0z"></path>
                                </svg>
                            </a>
                            @endif
                        </td>
                    </tr>
                @if($payment->channel == 'online' || $payment->channel == 'manual' || $payment->channel == 'ikoop')    
                <tbody class="table table-bordered text-center" id="row{{$key}}" style="display:none">
                    <tr>
                        <td class="font-weight-bold">Jenis Pembayaran</td>
                        <td class="font-weight-bold text-danger" colspan="2">Sedang Diproses</td>
                        <td class="font-weight-bold text-success" colspan="2">Telah Diluluskan</td>
                    </tr>
                    <tr>
                        <td>Fi Pendaftaran Anggota Baru</td>
                        @if($payment->channel == 'online')
                        <td colspan="2"></td>
                        <td colspan="2">{{'RM '.number_format($payment->first_time_fees,'2','.',',')}}</td>
                        @elseif($payment->channel == 'manual' || $payment->channel == 'ikoop')
                        <td colspan="2">
                            {{$payment->status == 'waiting-for-verification' || 'waiting-for-approval' ? 'RM '.number_format($payment->first_time_fees,'2','.',','):'-'}}
                        </td>
                        <td colspan="2">
                          {{$payment->status == 'payment-completed' ? 'RM '.number_format($payment->first_time_fees,'2','.',','):'-'}}
                        </td> 
                        @endif                     
                    </tr>
                    <tr>
                        <td>Langganan Modal Syer</td>
                        @if($payment->channel == 'online')
                        <td colspan="2"></td>
                        <td colspan="2">{{'RM '.number_format($payment->first_time_share,'2','.',',')}}</td>
                        @elseif($payment->channel == 'manual' || $payment->channel == 'ikoop')
                        <td colspan="2">
                            {{$payment->status == 'waiting-for-verification' || 'waiting-for-approval' ? 'RM '.number_format($payment->first_time_share,'2','.',','):'-'}}
                        </td>
                        <td colspan="2">
                          {{$payment->status == 'payment-completed' ? 'RM '.number_format($payment->first_time_share,'2','.',','):'-'}}
                        </td> 
                        @endif
                    </tr>
                    <tr>
                        <td>Langganan Tambahan</td>
                        @if($payment->channel == 'online')
                        <td colspan="2"></td>
                        <td colspan="2">{{'RM '.number_format($payment->additional_share,'2','.',',')}}</td>
                        @elseif($payment->channel == 'manual' || $payment->channel == 'ikoop')
                        <td colspan="2">
                            {{$payment->status == 'waiting-for-verification' || 'waiting-for-approval'? 'RM '.number_format($payment->additional_share,'2','.',','):'-' }}
                        </td>
                        <td colspan="2">
                          {{$payment->status == 'payment-completed' ? 'RM '.number_format($payment->additional_share,'2','.',','):'-'}}
                        </td> 
                        @endif
                    </tr>
                    @if($payment->channel == 'online')
                    <tr>
                        <td class="font-weight-bold">Jumlah Telah Diterima</td>
                        <td colspan="4">{{'RM '.number_format($payment->amount,'2','.',',')}}</td>
                    </tr>
                    @elseif($payment->channel == 'manual' || $payment->channel == 'ikoop')
                    <tr>
                      <td class="font-weight-bold">Jumlah Telah Diterima</td>
                      <td colspan="4">{{$payment->status == 'payment-completed' ? 'RM '.number_format($payment->additional_share,'2','.',','):'-'}}</td>
                    </tr>
                    @endif
                </tbody>
                @endif
                @empty
                <tbody>
                    <td colspan="10" class="text-center font-weight-bold text-uppercase">
                        <h3>tiada rekod</h3>
                    </td>
                </tbody>
                @endforelse

            </table>
            <div class="d-flex justify-content-center align-items-center">
              {{ $payments->links() }}
          </div>
        </div> --}}