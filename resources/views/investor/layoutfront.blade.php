<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Meta -->
    <meta name="description" content="KoPPIM - Pelanggan">

    <title>KoPPIM</title>

    <!-- Vendor css -->
    <link href="/investor/lib/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="/investor/lib/Ionicons/css/ionicons.css" rel="stylesheet">
    <link rel="shortcut icon" href="{{asset('img/favicon-ori.ico')}}" type="image/x-icon">
    <link rel="icon" href="{{asset('img/favicon-ori.ico')}}" type="image/x-icon">
    <!-- Slim CSS -->
    <link rel="stylesheet" href="{{ asset('investor/css/slim.css') }}">
    <style>
        body {
            background: #fff;
        }

        .stepwizard-step p {
            margin-top: 10px;
        }

        .stepwizard-row {
            display: table-row;
        }

        .stepwizard {
            display: table;
            width: 50%;
            position: relative;
        }

        .stepwizard-step button[disabled] {
            background-color: red;
            opacity: 1;
            filter: alpha(opacity=100);
        }

        .stepwizard-row:before {
            top: 14px;
            bottom: 0;
            position: absolute;
            content: " ";
            width: 100%;
            height: 1px;
            background-color: #ccc;
        }

        .stepwizard-step {
            display: table-cell;
            text-align: center;
            position: relative;
        }

        .btn-circle {
            width: 30px;
            height: 30px;
            text-align: center;
            padding: 6px 0;
            font-size: 12px;
            line-height: 1.428571429;
            border-radius: 15px;
            cursor: default !important;
        }

        .btn-default {
            background: #fff;
            color: #1b84e7;
            border-color: #ccc;
        }

        .w-b {
            width: 120px !important;
        }

        @media only screen and (min-width:290px) and (max-width:580px) {
            .stepwizard {
                width: 300px;
            }

            .signin-box {
                padding: 10px 40px !important;
            }

            .w-b {
                width: 100px !important;
            }

            .poster {
                width: 400px !important;
            }

            .signin-left .signin-box {
                width: auto !important;
            }
        }

    </style>
    @yield('css')
    @if(env('APP_ENV') == 'production')
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-157538637-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());
        gtag('config', 'UA-157538637-1');

    </script>
    @endif
</head>

<body>

    <div class="d-md-flex flex-row-reverse">
        <div class="signin-right flex-column">
            <div style="margin-bottom: 20px;" class="d-lg-none" align="center"> <a href="{{ env('APP_URL') }}"><img src="{{asset('img/logo-koppim.png')}}" width="200" /></a>
            </div>
            @include('include.step')
            <div class="signin-box">
                @include('announcements.body')
                @if ($errors->any())
                <div style="margin-top: 20px;">
                    <div class="alert alert-danger text-center text-uppercase">
                        @foreach ($errors->all() as $error)
                        {!! $error !!}
                        @endforeach
                    </div>
                </div>
                @endif

                @yield('content')
            </div>

        </div><!-- signin-right -->
        <div class="signin-left">
            <div class="signin-box">
                <h2 class="slim-logo text-center"><a href="{{ env('APP_URL') }}"><img src="{{asset('img/logo-koppim.png')}}" width="200" /></a></h2>

                {{-- @if (URL::current() == url('/langganan-syer')) --}}
                <img src="{{ asset('img/poster.png') }}" alt="" class="poster" style="width:100%">



                <p class="text-center mb-2"><a href="https://www.koppim.com.my" class="btn btn-outline-secondary pd-x-25" target="_blank">Halaman
                        Utama KoPPIM</a></p>
                <p class="text-center"><a href="{{env('APP_URL')}}" class="btn btn-outline-secondary pd-x-25">Kembali Ke Log Masuk</a></p>

                <p class="tx-12 text-center">&copy; Hak Cipta KoPPIM - 2020</p>
            </div>
        </div><!-- signin-left -->
    </div><!-- d-flex -->

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
    </script>
    <script src="/investor/lib/popper.js/js/popper.js"></script>
    <script src="/investor/lib/bootstrap/js/bootstrap.js"></script>

    <script src="/investor/js/slim.js"></script>

    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <script src="{{ url('/js/jquery.mask.js') }}"></script>
    <script type="text/javascript" src="https://rawgit.com/RobinHerbots/jquery.inputmask/3.x/dist/jquery.inputmask.bundle.js"></script>

    <script>
        const Toast = Swal.mixin({
            toast: true
            , position: 'start-end'
            , showConfirmButton: false
            , timer: 3000
            , timerProgressBar: true
            , onOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
            }
        });

        @if(session('success'))
        Toast.fire({
            icon: "success"
            , title: "{{ session('success') }}"
        });
        @endif

        @if(session('error'))
        Toast.fire({
            icon: "error"
            , title: "{{ session('error') }}"
        });
        @endif

    </script>

<!--Start of Tawk.to Script-->
<script type="text/javascript">
    var Tawk_API = Tawk_API || {}
        , Tawk_LoadStart = new Date();
    (function() {
        var s1 = document.createElement("script")
            , s0 = document.getElementsByTagName("script")[0];
        s1.async = true;
        s1.src = 'https://embed.tawk.to/5e4c8e79298c395d1ce89f49/1ehblbka1';
        s1.charset = 'UTF-8';
        s1.setAttribute('crossorigin', '*');
        s0.parentNode.insertBefore(s1, s0);
    })();

</script>
<!--End of Tawk.to Script-->
    @yield('js')


</body>

</html>
