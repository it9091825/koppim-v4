@extends('investor.layoutfront')

@section('css')
@endsection

@section('content')
<div align="center">
    <span class="signin-title-primary " style="text-transform: uppercase;font-size:20px">Pendaftaran Anggota Baharu ATAU
        Langganan Syer KoPPIM
    </span>
</div>
<form method="POST" name="invest" action="/langganan-syer">
    @csrf
    <div class="row" style="margin-bottom: 40px;">
        <div class="col-lg-12">
            <input type="hidden" class="form-control" name="type" id="type" value="one-time">
            <input type="hidden" class="form-control" name="no_anggota" id="no_anggota" value="{{ $no_anggota }}">
            <hr style="margin:10px 0" />
            <p style="text-transform:uppercase; color: #000000;margin-bottom:5px;" align="center">
                LENGKAPKAN RUANGAN BERIKUT UNTUK DAFTAR ANGGOTA ATAU LANGGAN SYER KOPPIM
            </p>
            <div class="form-group mb-1">
                <input type="text" class="form-control text-center" id="name" name="name" style="text-transform: uppercase;" aria-describedby="name" placeholder="Nama Penuh (Seperti MyKad)" required value="{{ old('name') }}">
                @if ($errors->has('name'))
                <span class="form-text text-danger"><small class="form-text">{{ $errors->first('name') }}</small></span>
                @endif
            </div>
            <div class="form-group mb-1">
                <input type="text" class="form-control ic text-center" id="ic" name="ic" style="text-transform: uppercase;" aria-describedby="ic" placeholder="No. MyKad" required value="{{ old('ic') }}">
                @if ($errors->has('ic'))
                <span class="form-text text-danger"><small class="form-text">{{ $errors->first('ic') }}</small></span>
                @endif
            </div>
            <div class="form-group mb-1">
                <div class="input-group">
                    <div class="input-group-prepend">
                        <div class="input-group-text" id="btnGroupAddon2" style="background-color: #ffffff; color: black;">+6
                        </div>
                    </div>
                    <input type="text" class="form-control text-center" value="{{old('phone')}}" placeholder="NO. TELEFON" name="phone" id="phone" aria-describedby="btnGroupAddon2">
                </div>
            </div>
            <div class="form-group mb-1">
                <input type="email" class="form-control text-center" id="email" name="email" aria-describedby="email" placeholder="ALAMAT EMEL" value="{{ old('email') }}">
            </div>


            <div class="input-group">
                <select class="form-control" style="text-transform: uppercase;" name="know_from" id="know_from" required>
                    <option value="">BAGAIMANA ANDA TAHU TENTANG KoPPIM?</option>
                    <option value="RADIO_ADS">RADIO ADS</option>
                    <option value="BILLBOARDS">BILLBOARDS</option>
                    <option value="WEBSITE">WEBSITE</option>
                    <option value="FACEBOOK">FB POST</option>
                    <option value="INSTAGRAM">INSTAGRAM</option>
                    <option value="AFFILIATE">AFFILIATE</option>
                    <option id="others" value="OTHERS">OTHERS</option>
                </select>
            </div>

            <div class="form-group mb-1">
                <input name="mesej" id="mesej" class="form-control text-center" placeholder="TULISKAN NOTA RINGKAS (SEKIRANYA ADA)" maxlength="180">
                <p id="textarea_feedback" class="text-center mb-1"></p>
            </div>
            <div align="center" style="margin-top: 12px;">
                <div style="border:1px solid #929292;padding:10px;text-align:center;margin-bottom:15px;">
                    ANGGOTA DIGALAKKAN MELANGGAN SYER MINIMA <b>RM1,200.00 / TAHUN</b> AGAR KOPPIM DAPAT
                    MELAKSANAKAN <b>PROJEK BERIMPAK TINGGI</b> SEKALIGUS <b>MEMBINA EKOSISTEM BAHARU EKONOMI
                        ISLAM!</b>
                </div>
                <h6 class="card-subtitle mb-1 text-muted" style="text-transform: uppercase;">
                    SILA TENTUKAN LANGGANAN SYER ANDA
                </h6>
                <h1 id="big_price" class="">RM 100.00</h1>
                <input type="hidden" class="form-control" name="amount" id="amount" value="100">
            </div>
            <div class="form-group" align="center" margin-bottom:2px;width:100%>
                <div class="btn-group" role="group">
                    <button type="button" class="btn btn-primary w-b" onclick="addShare()">+RM100.00</button>
                    <button type="button" class="btn btn-primary w-b" onclick="addShare1000()">+RM1,000.00</button>
                    <button type="button" class="btn btn-primary w-b" onclick="addShare5000()">+RM5,000.00</button>
                </div>
                <div class="btn-group" role="group">
                    <button type="button" class="btn btn-secondary w-b" onclick="minusShare()">-RM100.00</button>
                    <button type="button" class="btn btn-secondary w-b" onclick="minusShare1000()">-RM1,000.00</button>
                    <button type="button" class="btn btn-secondary w-b" onclick="minusShare5000()">-RM5,000.00</button>
                </div>
            </div>
            <div class="text-small text-primary text-center" style="font-size:12px">
                *Fi Pendaftaran RM30 Bagi Anggota Baharu.
            </div>
            <div class="form-group" style="text-align: center">
                <input type="checkbox" class="" checked name="terms" id="terms">
                <span>
                    Saya bersetuju dengan <a href="{{ url('terma-syarat') }}">terma & syarat</a> serta bertanggungjawab terhadap maklumat yang diberikan.
                </span>
            </div>
            <div style="margin-top:50px;text-align:center; margin-top: 20px;">
                <button type="button" onclick="submit_donation()" id="submitBtn" class="btn btn-primary btn-block btn-signin" style="margin-bottom:10px;">TERUSKAN
                </button>
                <p class="mt-2 text-center">
                    Perlu Bantuan? Ada Pertanyaan?
                    <a href="http://bit.ly/KoPPIM-Pertanyaan">
                        <b class="text-danger">KLIK DISINI!</b>
                    </a>
                </p>
            </div>
        </div>
    </div>
</form>
<div class="modal fade" id="termsconditions" aria-labelledby="termsconditions" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title text-dark" id="terms">Syarat-Syarat Pendaftaran/Langganan Syer KoPPIM</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <ol class="text-dark">
                    <li>Fi pendaftaran anggota baharu adalah RM30.00</li>
                    <li>Keanggotaan KoPPIM terbuka kepada semua warganegara Malaysia yang beragama Islam berumur 18 tahun dan ke atas yang menetap di Malaysia.</li>
                    <li>Pemohon mestilah seorang yang waras dan tiada kesalahan jenayah serta bukan seorang yang bankrap atau tidak pernah dibuang daripada mana-mana koperasi dalam tempoh kurang daripada 1 tahun.</li>
                    <li>Pemohon TIDAK DIBENARKAN mengeluarkan syer (saham) dalam tempoh 12 bulan pertama.</li>
                    <li>Permohonan yang lengkap akan disemak dalam tempoh 7 hari waktu bekerja. Sekiranya pihak KoPPIM tidak menerima apa-apa dokumen dalam tempoh 4 minggu, permohonan keanggotaan tersebut akan dihapuskan.</li>
                    <li>Pemohon hendaklah mengisi borang akuan (akan diberi secara online kepada anggota setelah bayaran Fi diselesaikan)</li>
                </ol>
                <p class="text-danger">Dengan klik butang 'Saya Setuju' anda telah faham dan bersetuju dengan syarat-syarat pendaftaran/langganan syer KoPPIM.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Saya Setuju</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script>
    $(document).ready(function() {
        $("#phone").on("input", function() {
            if (/^[1-9]/.test(this.value)) {
                this.value = this.value.replace(/^[1-9]/, "")
            }
        });
        $('#phone').mask("000000000000");
        $('#amount_display').mask('00000.00');
        $('#ic').mask('000000000000');
    });

    var times = 1;
    var times2 = 10;
    var initprice = 100;
    var initprice2 = 1000;
    var initprice3 = 5000;
    var total = 100;

    $('#terms').change(function() {
        $('#submitBtn').attr('disabled', !this.checked);
    });

    function addShare() {
        times = times + 1;
        total = total + initprice;
        $('#amount').val(total);
        $('#big_price').html('RM ' + numberWithCommas(total) + '.00');
    }

    function minusShare() {
        if (total > 100) {
            times = times - 1;
            total = total - initprice;
            if (total <= 100) {
                total = 100;
            }
            $('#amount').val(total);
            $('#big_price').html('RM ' + numberWithCommas(total) + '.00');
        }
    }

    function addShare1000() {
        times = times + 1;
        total = total + initprice2;
        $('#amount').val(total);
        $('#big_price').html('RM ' + numberWithCommas(total) + '.00');
    }

    function minusShare1000() {
        if (total > 100) {
            times = times - 1;
            total = total - initprice2;
            if (total <= 100) {
                total = 100;
            }
            $('#amount').val(total);
            $('#big_price').html('RM ' + numberWithCommas(total) + '.00');
        }
    }

    function addShare5000() {
        times = times + 1;
        total = total + initprice3;
        $('#amount').val(total);
        $('#big_price').html('RM ' + numberWithCommas(total) + '.00');
    }

    function minusShare5000() {
        if (total > 100) {
            times = times - 1;
            total = total - initprice3;
            if (total <= 100) {
                total = 100;
            }
            $('#amount').val(total);
            $('#big_price').html('RM ' + numberWithCommas(total) + '.00');
        }
    }

    function numberWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }

    function emailIsValid(email) {
        var re = /^(?:[a-z0-9!#$%&amp;'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&amp;'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])$/;
        return re.test(email);
    }

    $(document).ready(function() {
        var text_max = 180;
        $('#textarea_feedback').html(text_max + ' Huruf Sahaja');
        $('#mesej').keyup(function() {
            var text_length = $('#mesej').val().length;
            var text_remaining = text_max - text_length;
            $('#textarea_feedback').html('Huruf Berbaki: ' + text_remaining);
        });
    });

    function submit_donation() {
        const Toast = Swal.mixin({
            toast: true
            , position: 'center'
            , showConfirmButton: false
            , timer: 3000
            , timerProgressBar: true
            , onOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
            }
        });
        var amount = $('#amount').val();
        var name = $('#name').val();
        var ic = $('#ic').val();
        var phone = $('#phone').val();
        var email = $('#email').val();
        var know_from = $('#know_from').val();
        var phonelength = phone.length;
        if (amount < 100 || amount === "") {
            Swal.fire({
                type: 'error'
                , title: 'Maklumat Tidak Lengkap'
                , confirmButtonColor: '#1b84e7'
                , text: 'Sila Masukkan Jumlah Syer'
            , });
        } else if (name === "") {
            Toast.fire({
                icon: 'error'
                , title: 'RUANGAN NAMA PENUH WAJIB DIISI'
            });
            $('#name').addClass('is-invalid');
        } else if (ic === "") {
            Toast.fire({
                icon: 'error'
                , title: 'RUANGAN NO. MYKAD WAJIB DIISI'
            });
            $('#ic').addClass('is-invalid');
        } else if (ic.length < 12) {
            Toast.fire({
                icon: 'error'
                , title: 'NO. MYKAD TIDAK LENGKAP'
            });
            $('#ic').addClass('is-invalid');
        } else if (phone === "") {
            Toast.fire({
                icon: 'error'
                , title: 'RUANGAN NO. TELEFON WAJIB DIISI'
            });
            $('#phone').addClass('is-invalid');

        } else if (phonelength < 9) {
            Toast.fire({
                icon: 'error'
                , title: 'NO. TELEFON TIDAK LENGKAP'
            });
            $('#phone').addClass('is-invalid');
        } else if (email === "") {
            Toast.fire({
                icon: 'error'
                , title: 'RUANGAN ALAMAT EMEL WAJIB DIISI'
            });
            $('#email').addClass('is-invalid');
        } else if (emailIsValid(email) === false) {
            Toast.fire({
                icon: 'error'
                , title: 'ALAMAT EMEL TIDAK SAH'
            });
            $('#email').addClass('is-invalid');
         } else if (know_from === "") {
            Toast.fire({
                icon: 'error'
                , title: 'RUANGAN TAHU TENTANG KOPPIM. WAJIB DIISI'
            });
            $('#know_from').addClass('is-invalid');
        } else {
            document.invest.submit();
        }
    }

</script>
<script>
    if (sessionStorage.getItem('#agreeToTerms') !== 'true') {

        $('#termsconditions').modal('show');
        sessionStorage.setItem('#agreeToTerms', 'true');
    }

</script>

<script>

    $(function() {
    $("#know_from").change(function() {
        var val = $(this).val();

        if (val == 'OTHERS') {
            Swal.fire({
        title: "Bagaimana Anda Tahu Tentang Koppim",
        text: "Tolong Nyatakan :)",
        input: 'text',
        allowOutsideClick: false,
        showCancelButton: false ,
        confirmButtonColor: 'green'

        }).then((result) => {


        if (result.value) {
            document.getElementById("others").value = result.value
            document.getElementById("others").innerHTML = result.value;
        }});
                }
            });
        });


</script>
@endsection
