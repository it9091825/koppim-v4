@extends('investor.layouts')

@section('navigation')

    @include('investor.navigation',['tab'=>'main'])

@endsection
@section('css')
<style>
  .table-borderless th,
.table-borderless td,
.table-borderless thead th,
.table-borderless tbody + tbody {
  border: 0 !important;
  padding:2px 10px; }
</style>
@endsection
@section('content')


    <div class="slim-mainpanel">
        <div class="container">
          <h6 class="slim-pagetitle mt-4 mb-3">Rekod Transaksi Anda</h6>
            <div class="row row-sm row-timeline">
              <div class="col-lg-9">
                @if(count($transactions))
                <div class="card pd-30">
                  <div class="timeline-group">
                    @foreach($transactions as $transaction)
                  <div class="timeline-item {{$transaction->status == 'payment-completed' ? 'timeline-day':''}}">
                    <div class="timeline-time">{{$transaction->created_at->format('d/m/Y h:i:sa')}}</div>
                      <div class="timeline-body">
                        <table class="table-borderless">
                          <tbody>
                            @if($transaction->amount)
                            <tr>
                              <td class="font-weight-bold"><span class="square-10 bg-success rounded-circle"></span> Jumlah Keseluruhan</td>
                              <td class="text-right font-weight-bold">RM {{number_format($transaction->amount,2,'.',',')}}</td>
                            </tr>
                            @endif
                            @if($transaction->first_time_fees != 0.00)
                            <tr>
                              <td class="font-weight-bold"><span class="square-10 bg-info rounded-circle"></span> Fi Pendaftaran</td>
                              <td class="text-right">RM {{number_format($transaction->first_time_fees,2,'.',',')}}</td>
                            </tr>
                            @endif
                            @if($transaction->first_time_share != 0.00)
                            <tr>
                              <td class="font-weight-bold"><span class="square-10 bg-info rounded-circle"></span> Langganan Modal Syer</td>
                              <td class="text-right">RM {{number_format($transaction->first_time_share,2,'.',',')}}</td>
                            </tr>
                            @endif
                            @if($transaction->additional_share != 0.00)
                            <tr>
                              <td class="font-weight-bold"><span class="square-10 bg-info rounded-circle"></span> Langganan Tambahan</td>
                              <td class="text-right">RM {{number_format($transaction->additional_share,2,'.',',')}}</td>
                            </tr>
                            @else
                            <tr>
                              <td class="font-weight-bold"><span class="square-10 bg-info rounded-circle"></span> Langganan Tambahan</td>
                              <td class="text-right">RM 0.00</td>
                            </tr>
                            @endif
                            @if($transaction->gateway)
                            <tr>
                              <td class="font-weight-bold"><span class="square-10 bg-info rounded-circle"></span> Cara Pembayaran</td>
                              <td class="text-right text-uppercase">{{$transaction->gateway ?? ''}}</td>
                            </tr>
                            @endif
                            @if($transaction->status)
                            <tr>
                              <td class="font-weight-bold"><span class="square-10 bg-info rounded-circle"></span> Status Bayaran</td>
                              <td class="text-right text-uppercase">
                                {{$transaction->status == 'payment-completed' ? 'Sudah Diluluskan':'Sedang Diproses'}}</td>
                            </tr>
                            @endif
                          </tbody>
                        </table>                        
                      </div><!-- timeline-body -->
                    </div><!-- timeline-item -->                
                    @endforeach
                  </div><!-- timeline-group -->
                </div><!-- card -->
                @else
                  <div class="card pd-30">
                    <p>Tiada transaksi dijumpai.</p>
                  </div>
                @endif
              </div><!-- col-9 -->

            </div><!-- row -->
            
          </div>
    </div><!-- slim-mainpanel -->

@endsection
