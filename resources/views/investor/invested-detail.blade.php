@extends('investor.layouts')

@section('navigation')

@include('investor.navigation',['tab'=>'pelaburan'])

@endsection

@section('content')


 <div class="slim-mainpanel">
      <div class="container pd-t-50">
         <div class="d-sm-flex align-items-center justify-content-between mg-b-20 mg-lg-b-25 mg-xl-b-30">
          <div>
            <h4 class="mg-b-0 tx-spacing--1" style="text-transform: uppercase;">PENYATA LANGGANAN</h4>
          </div>
          
        </div>
        
        <div class="row row-sm">
	        
	        <div class="col-lg-12" style="margin-bottom: 20px;">
            <div class="card card-sales">
              <h6 class="slim-card-title tx-primary">1 JAN 2018 | ID:243553</h6>
              <div class="row">
                <div class="col">
                  <label class="tx-12">Jumlah Langganan Syer</label>
                  <p>RM 1,000.00</p>
                </div><!-- col -->
                <div class="col">
                  <label class="tx-12">Jumlah Hari</label>
                  <p>330 Hari</p>
                </div><!-- col -->
                <div class="col">
                  <label class="tx-12">Keuntungan 2019 (5.5%)</label>
                  <p>RM 120.25</p>
                </div><!-- col -->
              </div><!-- row -->

              <div class="progress mg-b-5">
                <div class="progress-bar bg-primary wd-70p" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100">70%</div>
              </div>
              <p class="tx-12 mg-b-0">Tarikh 1 JAN 2020 - 31 DEC 2020 (330 / 365 Hari)</p>
            
            </div><!-- card -->
          </div><!-- col-6 -->
	        
          
          
          
          
        </div><!-- row -->

        
        
        <div class="row" style="margin-top: 30px;">
          <div class="col-lg-12">
           
           

            <h6 class="slim-card-title mg-b-15">PENYATA KEUNTUNGAN</h6>
            <div class="row no-gutters">
             <div class="col-sm-12">
                <div class="card card-earning-summary">
	                <div class="table-responsive">
	                
             	<table class="table">
	             	<thead>
	             	<tr>
		             	<td>Tahun</td>
		             	<td>Tarikh</td>
		             	<td>Hari</td>
		             	<td>Keuntungan(%)</td>
		             	<td>Langganan Syer</td>
		             	<td>Jumlah</td>
		             	<td>Status</td>
	             	</tr>
	             	</thead>
	             	
	             	<tr>
							<td>2018</td>
							<td>06 Jun 2018 - 10 Jun 2018</td>
							<td>4</td>
							<td>4.3%</td>
							<td>RM 1,500.00</td>
							<td>RM 4.70</td>
															<td><span class="square-8 bg-success mg-r-5 rounded-circle"></span> Dibayar</td>
														
						</tr>
	             		
						<tr>
							<td>2018</td>
							<td>10 Jun 2018 - 31 Dec 2018</td>
							<td>183</td>
							<td>4.3%</td>
							<td>RM 1,000.00</td>
							<td>RM 70.54</td>
															<td><span class="square-8 bg-success mg-r-5 rounded-circle"></span> Dibayar</td>
														
						</tr>
						
						<tr>
							<td>2019</td>
							<td>01 Jan 2019 - 31 Dec 2019</td>
							<td>365</td>
							<td>5.5%</td>
							<td>RM 1,000.00</td>
							<td>RM 120.25</td>
															<td><span class="square-8 bg-success mg-r-5 rounded-circle"></span> Dibayar</td>
														
						</tr>
						<tr>
							<td>2020</td>
							<td>01 Jan 2020 - 31 Dec 2020</td>
							<td>366</td>
							<td>5%</td>
							<td>RM 1,000.00</td>
							<td>-</td>
															<td></td>
														
						</tr>
											
						             	
             	</table>
	                </div>
                </div>
             </div>
              
            </div><!-- row -->
          </div><!-- col-6 -->
          
        </div><!-- row -->
        
        
         <div class="row" style="margin-top: 30px;">
          <div class="col-lg-12" >
           
           

            <h6 class="slim-card-title mg-b-15">BORANG PENGELUARAN LANGGANAN</h6>
            <div class="row no-gutters">
             <div class="col-sm-12">
                <div class="card card-earning-summary">
	                
	                <div style="margin-bottom: 20px;">
	                	<input id="bankaccountNo" name="bankaccountNo" required value="" class="form-control " placeholder="Masukkan Jumlah Pengeluaran. Minima RM 10.00" type="text">
	                </div>
	                <div align="center" style="margin-bottom: 20px;">
	                	Pengiraan dividen akan berubah selepas pengeluaran dibuat dan akan mengambil masa 3 hari bekerja untuk memasuki ke dalam akaun anda.
	                </div>
	                
	                <table class="table">
		                	<tr>
			                	<td>Bank</td>
			                	<td>{{ Auth::user()->org_bank_name }}</td>
		                	</tr>
		                	<tr>
			                	<td>Akaun Name.</td>
			                	<td>{{ Auth::user()->org_bank_acc }}</td>
		                	</tr>
		                	<tr>
			                	<td>Akaun No</td>
			                	<td>{{ Auth::user()->org_bank_account_name }}</td>
		                	</tr>
	                	</table>
	                
	                <button class="btn btn-success btn-block" type="submit">Hantar Untuk Pengeluaran</button>
                </div>
             </div>
              
            </div><!-- row -->
          </div><!-- col-6 -->
          
        </div><!-- row -->
        
        
        <div class="row" style="margin-top: 30px;">
          <div class="col-lg-12" >
           
           

            <h6 class="slim-card-title mg-b-15">TRANSAKSI PENGELUARAN</h6>
            <div class="row no-gutters">
             <div class="col-sm-12">
                <div class="card card-earning-summary">
	                
	              <div class="table-responsive">
	                
             	<table class="table">
	             	<thead>
	             	<tr>
		             	<td>Tarikh</td>
		             	<td>Jumlah</td>
		             	<td>No. Rujukan</td>
		             	<td>Lampiran</td>
		             	<td>Status</td>
	             	</tr>
	             	</thead>
						<tr>
							<td>10 Jun 2018</td>
							<td>RM 500.00</td>
							<td>AK7636372883737828837</td>
							<td><a href="https://images.says.com/uploads/story_source/source_image/700071/becd.jpg?_ga=2.36868261.485961520.1596446281-793671852.1596446279" target="_blank">Lampiran</a></td>
							<td><span class="square-8 bg-success mg-r-5 rounded-circle"></span> Dibayar</td>
														
						</tr>
											
						             	
             	</table>
	                </div>
                </div>
             </div>
              
            </div><!-- row -->
          </div><!-- col-6 -->
          
        </div><!-- row -->
        
      </div><!-- container -->
    </div><!-- slim-mainpanel -->

@endsection