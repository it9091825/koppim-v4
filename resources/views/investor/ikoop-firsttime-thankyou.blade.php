@extends('investor.layoutfront')

@section('css')


@endsection

@section('content')

{{-- <div style="margin-bottom: 20px;" class="d-lg-none" align="center"> <img src="https://www.koppim.com.my/wp-content/uploads/2020/06/Logo-KoPPIM-small.png" width="200" /> </div> --}}


<div>Terima Kasih. Anda akan mendapat Kata Laluan Sementara Melalui SMS yang dihantar. Log Masuk Ke Akaun Untuk
   Mengemaskini Butiran.</div>


<div style="margin-top:10px;margin-bottom:10px;text-align:center">
   <a href="/" class="btn btn-primary btn-block btn-signin" style="color: white;margin-bottom:10px;">LOG MASUK KE AKAUN</a>
   <p class="text-center mb-2">Perlu Bantuan? Ada Pertanyaan? <a href="http://bit.ly/KoPPIM-Pertanyaan"><b
            class="text-danger">KLIK DISINI!</b></a>
   </p>
</div>


@endsection

@section('js')
<script>
   $(document).ready(function(){
       $('#phone').mask('00000000000');
       $('#amount_display').mask('00000.00');
       $('#ic').mask('000000000000');
      //$('#myModal').modal();
   });
   
   var times = 1;
   var initprice = 100;
   var total = 100;
      
   
   
   function addShare()
   {
	  
	   
	   times = times + 1;
	   
	   total = initprice*times;
	    
	   $('#amount').val(total);
	  
	   $('#big_price').html('RM '+total+'.00');
	    
   }
   
   
   function minusShare()
   {
	  if(times > 1)
	   {
		   times = times - 1;
	   
	   
	   total = initprice*times;
	    
	   $('#amount').val(total);
	  
	   $('#big_price').html('RM '+total+'.00');
	   }
	   
	    
   }
   
    	       
   
   
   function emailIsValid(email) {
   var re = /^(?:[a-z0-9!#$%&amp;'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&amp;'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])$/;
   return re.test(email);
   }
   
   function submit_donation()
   {
    var amount = $('#amount').val(); 
    var name = $('#name').val(); 
    var ic = $('#ic').val(); 
    var phone = $('#phone').val(); 
   var email = $('#email').val(); 
   
   var phonelenght = phone.length;
   
   if(amount === "0.00" || amount === "")
   {
   Swal.fire({
    type: 'error',
    title: 'Oops...',
    confirmButtonColor: '#3fac87',
    text: 'Sila Masukkan Jumlah Sumbangan',
   });	
   
   }
   else if(name === "")
   {
   Swal.fire({
    type: 'error',
    title: 'Oops...',
    confirmButtonColor: '#3fac87',
    text: 'Maklumat Tidak Lengkap',
   });
   }
   else if(phone === "")
   {
   Swal.fire({
    type: 'error',
    title: 'Oops...',
    confirmButtonColor: '#3fac87',
    text: 'Maklumat Tidak Lengkap',
   });
   }
   else if(phonelenght < 10)
   {
   Swal.fire({
    type: 'error',
    title: 'Oops...',
    confirmButtonColor: '#3fac87',
    text: 'No. Telefon anda tidak lengkap',
   });
   }  	
   else if(email === "")
   {
   Swal.fire({
    type: 'error',
    title: 'Oops...',
    confirmButtonColor: '#3fac87',
    text: 'Maklumat Tidak Lengkap',
   });
   } 	
   else if(emailIsValid(email) === false)
   {
   Swal.fire({
    type: 'error',
    title: 'Oops...',
    confirmButtonColor: '#3fac87',
    text: 'Emel '+email+' Tidak Sah',
   });
   }
   else
   {
   document.dona.submit();
   }
    
    
   }    
   
   
   function change_donation_type(type)
   {
   if(type === 'recurring')
   {
   $('#type').val('recurring');
     
    $('#sumbangan_sekali').css({"background-color": "#22445d"});; 
      $('#sumbangan_berulang').css({"background-color": "#3fac87"});; 
     
       $('#amount').val('');
        $('#amount_display').val('');
        
        document.getElementById("amount_display").disabled = true;
        
   }
   else
   {
    $('#type').val('one-time');
     
   $('#sumbangan_sekali').css({"background-color": "#3fac87"});; 
      $('#sumbangan_berulang').css({"background-color": "#22445d"});; 
      
       $('#amount').val('');
        $('#amount_display').val('');
        document.getElementById("amount_display").disabled = false;
   }
    
   }
   
   
</script>

@endsection