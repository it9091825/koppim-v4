<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Twitter -->
    <meta name="twitter:site" content="@themepixels">
    <meta name="twitter:creator" content="@themepixels">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="Slim">
    <meta name="twitter:description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="twitter:image" content="http://themepixels.me/slim/img/slim-social.png">

    <!-- Facebook -->
    <meta property="og:url" content="http://themepixels.me/slim">
    <meta property="og:title" content="Slim">
    <meta property="og:description" content="Premium Quality and Responsive UI for Dashboard.">

    <meta property="og:image" content="http://themepixels.me/slim/img/slim-social.png">
    <meta property="og:image:secure_url" content="http://themepixels.me/slim/img/slim-social.png">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="600">

    <!-- Meta -->
    <meta name="description" content="Koperasi PPIM">
    <meta name="author" content="Koperasi PPIM">

    <title>KoPPIM</title>

    <!-- vendor css -->
    <link href="{{asset('investor/lib/font-awesome/css/font-awesome.css')}}" rel="stylesheet">
    <link href="{{asset('investor/lib/Ionicons/css/ionicons.css')}}" rel="stylesheet">
    <link href="{{asset('investor/lib/jqvmap/css/jqvmap.min.css')}}" rel="stylesheet">
    <link href="{{asset('investor/lib/flag-icon-css/css/flag-icon.min.css')}}" rel="stylesheet">
    <link href="{{asset('investor/lib/jquery.steps/css/jquery.steps.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="https://unicons.iconscout.com/release/v3.0.3/css/line.css">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="{{asset('admin/css/select2-bootstrap4.css')}}">
    <!-- Slim CSS -->
    <link rel="stylesheet" href="{{asset('investor/css/slim.css')}}">
    @yield('css')
    <style>
        /* @media only screen and (min-width:640px){

      } */
        .logged-user .menu-utama {
            display: none;
        }

        @media only screen and (min-width:290px) and (max-width:1020px) {
            .logged-user .menu-utama {
                display: inline-block;
            }
        }

        td,
        th {
            vertical-align: middle !important;
        }

    </style>
</head>

<body class="dashboard-4">
    <div class="slim-header">
        <div class="container">
            <div class="slim-header-left">
                <h2 class="slim-logo"><a href="/home"><img src="{{asset('img/logo-koppim.png')}}" width="150" /></a></h2>
            </div><!-- slim-header-left -->
            <div class="slim-header-right">
                <div class="dropdown dropdown-c">
                    <a href="#" class="logged-user" data-toggle="dropdown">
                        {{-- <img src="/investor/img/userplaceholder.png" alt=""> --}}
                        <span class="menu-utama font-weight-bold">
                            <h5>MENU</h5>
                        </span>
                        <span>{{ strtoupper(Auth::user()->name) }}</span>
                        <i class="fa fa-angle-down"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <nav class="nav">

                            <a href="/profile" class="nav-link"><i class="icon ion-person"></i> Lihat Profil</a>
                            <a href="/set-kata-laluan-baru" class="nav-link"><i class="icon ion-key"></i> Set Kata Laluan</a>
                            @if(auth()->user()->status != 97)
                            <a href="/permohonan-berhenti" class="nav-link"><i class="icon ion-android-walk"></i></i> Permohonan Berhenti</a>
                            @else
                            <a href="/permohonan-aktif" class="nav-link"><i class="icon ion-android-walk"></i></i> Aktifkan Keangotaan</a>
                            @endif
                            <a href="/logout" class="nav-link"><i class="icon ion-forward"></i> Log Keluar</a>
                        </nav>
                    </div><!-- dropdown-menu -->
                </div><!-- dropdown -->
            </div><!-- header-right -->
        </div><!-- container -->
    </div><!-- slim-header -->


    @yield('navigation')
    {{-- <div align="center">
        <div style="max-width: 770px; min-width: 320px; width: 90%;" align="left">
            @if($errors->any())
            <div style=" padding-top: 10px;">
                <div class="alert alert-danger">
                    <ul style=" margin-left: -20px; margin-top: 10px;;">
                        @foreach ($errors->all() as $error)
                        <li style="margin-bottom: 10px;">{!! $error !!}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
            @endif
        </div>
    </div> --}}
    <div class="container">
        <div class="row mt-3">
            <div class="col">
                @include('announcements.body')
            </div>
        </div>
    </div>
    @yield('content')

    <div class="slim-footer">
        <div class="container">
            <p>&copy; KoPPIM Akaun Anggota 2020</p>
            <p>Version: 1.1.9</p>
        </div><!-- container -->
    </div><!-- slim-footer -->

    <script src="{{asset('investor/lib/jquery/js/jquery.js')}}"></script>
    <script src="{{asset('investor/lib/popper.js/js/popper.js')}}"></script>
    <script src="{{asset('investor/lib/bootstrap/js/bootstrap.js')}}"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
    <script src="{{asset('investor/lib/jquery.cookie/js/jquery.cookie.js')}}"></script>
    <script src="{{asset('investor/lib/jqvmap/js/jquery.vmap.min.js')}}"></script>
    <script src="{{asset('investor/lib/jqvmap/js/jquery.vmap.world.js')}}"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <script src="{{asset('investor/lib/chartist/js/chartist.js')}}"></script>
    <script src="{{asset('investor/lib/d3/js/d3.js')}}"></script>
    <script src="{{asset('investor/lib/rickshaw/js/rickshaw.min.js')}}"></script>
    <script src="{{asset('investor/lib/jquery.sparkline.bower/js/jquery.sparkline.min.js')}}"></script>
    <script src="{{asset('investor/js/ResizeSensor.js')}}"></script>
    <script src="{{asset('investor/js/dashboard.js')}}"></script>
    <script src="{{asset('investor/js/slim.js')}}"></script>
    {{-- sweetalert initialize --}}
    @if($errors->any())
    <script>
        var data = [];
        @foreach($errors->all() as $error)
        data.push("{{$error}}")
        @endforeach

        Swal.fire('Ralat', data.join("<br>"), 'error');

    </script>
    @endif
    <script>
        const Toast = Swal.mixin({
            toast: true
            , position: 'center'
            , showConfirmButton: false
            , timer: 3000
            , timerProgressBar: true
            , onOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
            }
        });
        @if(session('firsttime'))
        Toast.fire({
            icon: "error"
            , title: "ANDA PERLU MELENGKAPKAN PROFIL ANDA UNTUK MENGAKSES HALAMAN TERSEBUT"
        });
        @endif

        @if(session('success'))
        Toast.fire({
            icon: "success"
            , title: "{{ session('success') }}"
        });
        @endif

        @if(session('error'))
        Toast.fire({
            icon: "success"
            , title: "{{ session('error') }}"
        });
        @endif

        @if(session('profile_submitted'))
        Toast.fire({
            icon: "success"
            , title: "Penghantaran maklumat anda telah BERJAYA. Kami akan memproses permohonan tersebut dan menyemak untuk kelulusan. Pegawai kami akan menghubungi anda sekiranya ada tindakan lanjut diperlukan.<br><br>Terima kasih kerana berterusan menyokong KoPPIM."
        });
        @endif

    </script>
    @yield('js')
    @stack('script')

</body>

</html>
