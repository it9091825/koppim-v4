@extends('investor.layoutfront')

@section('css')


@endsection

@section('content')

{{-- <div style="margin-bottom: 20px;" class="d-lg-none" align="center"> <img src="https://www.koppim.com.my/wp-content/uploads/2020/06/Logo-KoPPIM-small.png" width="150" /> </div> --}}
          <div align="center"><h2 class="signin-title-primary">TERLUPA KATA LALUAN?</h2></div>
          <div align="center"><h5 class="signin-title-secondary">PORTAL PENGURUSAN LANGGANAN KOPPIM</h5></div>
		  
		   <p align="center" >Masukkan No. MyKad Anda Untuk Mendapatkan Kata Laluan Sementara. Kata Laluan Akan Dihantar Melalui SMS Kepada No. Telefon Yang Didaftarkan.</p>
		  
          <div class="form-group">
            <input type="text" class="form-control" placeholder="NO. MYKAD">
          </div><!-- form-group -->
          <button class="btn btn-primary btn-block btn-signin">Hantar</button>
           <p class="mg-b-0"><a href="/"> Kembali Ke Log Masuk</a></p>

@endsection

@section('js')
<script>
   $(document).ready(function(){
       $('#phone').mask('00000000000');
       $('#amount_display').mask('00000.00');
       $('#ic').mask('000000000000');
      //$('#myModal').modal();
   });
   
   var times = 1;
   var times2 = 10;
   var initprice = 100;
   var initprice2 = 1000;
   var total = 100;
      
   
   
   function addShare()
   {
	  
	   
	   times = times + 1;
	   
	   total = total+initprice;
	    
	   $('#amount').val(total);
	  
	   $('#big_price').html('RM '+numberWithCommas(total)+'.00');
	    
   }
   
   
   function minusShare()
   {
	  if(total > 100)
	   {
		   times = times - 1;
	   
	   
	   total = total-initprice;
	    
	   $('#amount').val(total);
	  
	   $('#big_price').html('RM '+numberWithCommas(total)+'.00');
	   }
	   
	    
   }
   
    
    
    function addShare1000()
   {
	  
	   
	   times = times + 1;
	   
	   total = total + initprice2;
	   
	   $('#amount').val(total);
	   
	   $('#big_price').html('RM '+numberWithCommas(total)+'.00');
	    
   }
   
   
   function minusShare1000()
   {
	  if(total > 100)
	   {
		   times = times - 1;
	   
	   
	   total = total - initprice2;
	    
	   $('#amount').val(total);
	  
	   $('#big_price').html('RM '+numberWithCommas(total)+'.00');
	   }
	   
	    
   }
        
   function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}
   
   function emailIsValid(email) {
   var re = /^(?:[a-z0-9!#$%&amp;'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&amp;'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])$/;
   return re.test(email);
   }
   
   function submit_donation()
   {
    var amount = $('#amount').val(); 
    var name = $('#name').val(); 
    var ic = $('#ic').val(); 
    var phone = $('#phone').val(); 
   var email = $('#email').val(); 
   
   var phonelenght = phone.length;
   
   if(amount < 100 || amount === "")
   {
   Swal.fire({
    type: 'error',
    title: 'Maklumat Tidak Lengkap',
    confirmButtonColor: '#1b84e7',
    text: 'Sila Masukkan Jumlah Syer',
   });	
   
   }
   else if(name === "")
   {
   Swal.fire({
    type: 'error',
    title: 'Maklumat Tidak Lengkap',
    confirmButtonColor: '#1b84e7',
    text: 'Maklumat Nama Anda',
   });
   }
   else if(phone === "")
   {
   Swal.fire({
    type: 'error',
    title: 'Maklumat Tidak Lengkap',
    confirmButtonColor: '#1b84e7',
    text: 'Masukkan No. Telefon Yang Lengkap',
   });
   }
   else if(phonelenght < 9)
   {
   Swal.fire({
    type: 'error',
    title: 'Maklumat Tidak Lengkap',
    confirmButtonColor: '#1b84e7',
    text: 'Masukkan No. Telefon Yang Lengkap',
   });
   } 
   else if(ic.length < 12)
   {
   Swal.fire({
    type: 'error',
    title: 'Maklumat Tidak Lengkap',
    confirmButtonColor: '#1b84e7',
    text: 'Masukkan No. MyKad Yang Lengkap',
   });
   }  	
   else if(email === "")
   {
   Swal.fire({
    type: 'error',
    title: 'Maklumat Tidak Lengkap',
    confirmButtonColor: '#1b84e7',
    text: 'Maklumat Emel Tidak Lengkap',
   });
   } 	
   else if(emailIsValid(email) === false)
   {
   Swal.fire({
    type: 'error',
    title: 'Maklumat Tidak Lengkap',
    confirmButtonColor: '#1b84e7',
    text: 'Emel '+email+' Tidak Sah',
   });
   }
   else
   {
   document.dona.submit();
   }
    
    
   }    
   
   
   function change_donation_type(type)
   {
   if(type === 'recurring')
   {
   $('#type').val('recurring');
     
    $('#sumbangan_sekali').css({"background-color": "#22445d"});; 
      $('#sumbangan_berulang').css({"background-color": "#3fac87"});; 
     
       $('#amount').val('');
        $('#amount_display').val('');
        
        document.getElementById("amount_display").disabled = true;
        
   }
   else
   {
    $('#type').val('one-time');
     
   $('#sumbangan_sekali').css({"background-color": "#3fac87"});; 
      $('#sumbangan_berulang').css({"background-color": "#22445d"});; 
      
       $('#amount').val('');
        $('#amount_display').val('');
        document.getElementById("amount_display").disabled = false;
   }
    
   }
   
   
</script>

@endsection