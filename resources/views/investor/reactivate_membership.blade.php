@extends('investor.layouts')

@section('navigation')

@include('investor.navigation',['tab'=>''])

@endsection

@section('content')
    <div class="slim-mainpanel">
        <div class="container" style="max-width: 800px; min-width: 320px; width: 90%;">
            <div class="slim-pageheader">
                <ol class="breadcrumb slim-breadcrumb">
                    <li class="breadcrumb-item"><a href="#"></a></li>
                </ol>
                <h6 class="slim-pagetitle">Aktifkan Semula Keanggotaan</h6>
            </div>
            <div class="row">
                <div class="col">
                    <p class="help-block">Aktifkan Semula Keanggotaan Koperasi Pembangunan Islam Malaysia (KoPPIM)</p>
                </div>
            </div>
            <div class="section-wrapper"  >
                <p style="margin-bottom: -10px;">
                    Anda telah berhenti daripada keanggotaan KoPPIM. Untuk mengaftifkan semula akaun anda, sila klik butang dibawah.
                </p>
                <form action="/permohonan-aktif" method="post" enctype="multipart/form-data"> 
                    @csrf
                    <div class="row">
                        <div class="col-lg">
                            <div style="margin-top: 20px;"><button class="btn btn-primary btn-block btn-signin" type="submit" >Aktifkan Semula Keanggotaan</button></div>
                        </div>
                    </div>
                </form>  
            </div>
        </div>
    </div>
@endsection
