@extends('investor.layouts')

@section('navigation')

@include('investor.navigation',['tab'=>'penyata'])

@endsection

@section('content')
    <div class="slim-mainpanel">
        @if($total <= 0 && $all_dividen <= 0)
            <div class="container">
                <div class="d-flex justify-content-between align-items-center">
                    <div class="slim-pageheader">
                        <div>
                            <ol class="breadcrumb slim-breadcrumb">
                                <li class="breadcrumb-item"><a href="#"></a></li>
                            </ol>
                            <h6 class="slim-pagetitle d-flex align-items-center"><span class="mr-2">MAKLUMAN TENTANG DIVIDEN</span></h6>
                        </div>
                    </div>
                </div>
                <div class="row no-gutters" style="margin-bottom: 20px;">
                    <div class="col-sm-12">
                        <div class="card">
                            <h2 class="slim-logo mt-3" style="margin-left:auto; margin-right:auto;"><a href=""><img src="{{asset('img/logo-koppim.png')}}" width="150" /></a></h2>
                            <div class="row mt-3">
                                <div class="col">
                                    <p class="text-justify ml-3 mr-3">Dividen tahunan akan dikira selepas akaun kewangan KoPPIM ditutup pada 31 Disember setiap tahun dan selepas akaun kewangan diaudit dan barulah dividen akan diumumkan pada mesyuarat agong KoPPIM yang akan diadakan akhir Jun {{ now()->year }}</p>
                                </div>
                            </div>
                            <div class="row mt-3">
                                <div class="col">
                                    <p class="text-justify ml-3 mr-3">Bahagian Pengurusan KoPPIM</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @else
            <div class="container pd-t-50">
                <div class="row mb-4">
                    <div class="col">
                        <div class="card card-dash-chart-one">
                            <div class="row no-gutters">
                                <div class="col-lg-6">
                                    <div class="left-panel">
                                        <div class="active-visitor-wrapper">
                                            {{-- <p class="text-dark font-weight-bold">Jumlah Dividen Semasa</p>
                                            <span class="d-flex align-items-baseline">
                                                <small style="font-size:24px" class="text-dark">RM</small>
                                                <h1 style="font-size:64px;" class="text-dark"></h1>
                                                {{ number_format($total,2) }}
                                            </span> --}}
                                            {{-- <span class="d-flex align-items-baseline">
                                                <small style="font-size:16px" class="text-dark">Keseluruhan : <b>RM {{ $dividen_static}} </b></small>
                                                {{ number_format($all_dividen,2) }}
                                            </span> --}}
                                        </div><!-- active-visitor-wrapper -->
                                        <hr class="mg-t-30 mg-b-40">
                                        <h6 class="visitor-operating-label">Notis Tentang Dividen</h6>
                                        @if($withdraw_bool >= 6)
                                            <div>
                                                {{-- <a href="/dividen/withdraw" class="btn btn-success">Pengeluaran Dividen </a> --}}
                                                <div class="alert alert-warning" role="alert">
                                                <b>Maaf, bahagian ini sedang di kemaskini buat masa ini. Harap maklum</b>
                                                </div>

                                            </div>
                                        @else
                                            <div>
                                                Mengikut undang-undang kecil koperasi, hanya langganan syer yang mencukupi tempoh <b>{{ 6-$withdraw_bool }} bulan</b> haul dari tarikh pengumuman dividen akan layak menerima pulangan dividen pada tahun ini.
                                                Walaubagaimanapun, kiraan dividen langganan syer yang kurang dari tempoh 6 bulan haul tersebut akan dikira pada tahun berikutnya.
                                            </div>
                                        @endif
                                        <hr>
                                        <h6 class="visitor-operating-label">Log Pengeluaran Dividen</h6>
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>Tarikh</th>
                                                    <th>Keterangan</th>
                                                    <th>Status</th>
                                                    <th>Jumlah</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @forelse($statements as $statement)
                                                    @if($statement->type == 'in')
                                                        <tr>
                                                            <td><a href="javascript:open_dividen('{{ json_encode(DividenHelper::get_dividen_detail($statement->dividen_id)->contributions) }}','{{ $statement->to->format("d M Y") }}')">{{ $statement->created_at->format('d M Y') }}</a></td>
                                                            <td><a href="javascript:open_dividen('{{ json_encode(DividenHelper::get_dividen_detail($statement->dividen_id)->contributions) }}','{{ $statement->to->format("d M Y") }}')">Dividen Untuk Durasi {{ $statement->from->format('d M Y') }} - {{ $statement->to->format('d M Y') }}</th></td>
                                                                <td>{{ strtoupper($statement->status) }}</td>
                                                            <td><b class="text-success">RM {{ number_format($statement->amount,2) }}</b></td>
                                                        </tr>
                                                    @else
                                                        <tr>
                                                            <td><a href="javascript:open_withdraw('{{ strtoupper($statement->status) }}','{{ $statement->bank }}','{{ $statement->account_no }}','{{ $statement->account_name }}','{{ $statement->amount*-1 }}','{{ $statement->payment_ref }}','{{ $statement->payment_receipt }}')">{{ $statement->created_at->format('d M Y') }}</a></td>
                                                            <td><a href="javascript:open_withdraw('{{ strtoupper($statement->status) }}','{{ $statement->bank }}','{{ $statement->account_no }}','{{ $statement->account_name }}','{{ number_format($statement->amount*-1,2) }}','{{ $statement->payment_ref }}','{{ $statement->payment_receipt }}')">Pengeluaran Dividen ke {{ $statement->bank }}</a></td>
                                                            <td>{{ strtoupper($statement->status) }}</td>
                                                            <td><b >RM {{ number_format($statement->amount*-1,2) }}</b></td>
                                                        </tr>
                                                    @endif
                                                    @empty

                                                    <tr>
                                                        <td colspan="4">Tiada rekod</td>
                                                    </tr>
                                                @endforelse
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="right-panel">
                                        <h6 class="slim-card-title">Penyata</h6>
                                        <div>
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th>Tarikh</th>
                                                        <th>Keterangan</th>
                                                        <th>Tindakan</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @forelse($dividen_cals as $dividen_cal)
                                                        @if(DividenHelper::get_dividen($dividen_cal->calculation_id)->status == 'published')
                                                        <tr>
                                                                <td>{{ $dividen_cal->created_at->format('d M Y') }}</td>
                                                                <td>Penyata Tahunan {{ $dividen_cal->from->format('d M Y') }} - {{ $dividen_cal->to->format('d M Y') }}</td>
                                                                <td><a href="/dividen/download">Muat Turun</a></td>
                                                            </tr>
                                                        @endif
                                                    @empty
                                                    @endforelse
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>

    <!-- The Modal -->
    <div class="modal" id="user_letter_modal">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title" id="modal_letter_header_name">Terperinci</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <!-- Modal body -->
                <div class="modal-body" style="width: 500px;" id="modal_body">
                    Sedang di proses
                </div>
                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Tutup</button>
                </div>
            </div>
        </div>
    </div>

    <!-- The Modal -->
    <div class="modal" id="withdraw_status">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title" id="modal_letter_header_name">Status Pengeluaran</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body" style="width: 500px;" id="w_modal_body">
                    Sedang di proses
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Tutup</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script>

        function open_withdraw(status,bank_name,acc,name,amount,rujukan,lampiran) {
            $('#withdraw_status').modal('show');
            var table = "";

            if(status === "SEDANG-DISEMAK")
            {
                table += `<table class='table'>
                <tr>
                        <td>Status</td>
                        <td>:</td>
                        <td><b>`+status+`</b></td>
                </tr>
                <tr>
                        <td>Bank</td>
                        <td>:</td>
                        <td><b>`+bank_name+`</b></td>
                </tr>
                <tr>
                        <td>Akaun No.</td>
                        <td>:</td>
                        <td><b>`+acc+`</b></td>
                </tr>
                <tr>
                        <td>Nama Akaun</td>
                        <td>:</td>
                        <td><b>`+name+`</b></td>
                </tr>
                <tr>
                        <td>Jumlah</td>
                        <td>:</td>
                        <td><b>RM `+amount+`</b></td>
                </tr>
                </table>
                `;
            }
            else if(status === "TELAH-DIBAYAR")
            {
                table += `<table class='table'>
                <tr>
                        <td>Status</td>
                        <td>:</td>
                        <td><b>`+status+`</b></td>
                </tr>
                <tr>
                        <td>Bank</td>
                        <td>:</td>
                        <td><b>`+bank_name+`</b></td>
                </tr>
                <tr>
                        <td>Akaun No.</td>
                        <td>:</td>
                        <td><b>`+acc+`</b></td>
                </tr>
                <tr>
                        <td>Nama Akaun</td>
                        <td>:</td>
                        <td><b>`+name+`</b></td>
                </tr>
                <tr>
                        <td>Jumlah</td>
                        <td>:</td>
                        <td><b>RM `+amount+`</b></td>
                </tr>
                <tr>
                        <td>No. Rujukan</td>
                        <td>:</td>
                        <td><b>`+rujukan+`</b></td>
                </tr>
                <tr>
                        <td>Bukti Pembayaran</td>
                        <td>:</td>
                        <td><a target='_blank' href='{{ env('DO_SPACES_FULL_URL') }}`+lampiran+`'>Lampiran</a></td>
                </tr>
                </table>
                `;

            }


            $('#w_modal_body').html(table);

        }

        function open_dividen(json_encode,to) {
            $('#user_letter_modal').modal('show');
            var obj = JSON.parse(json_encode);
            var table = "";
            for (var i = 0; i < obj.length; i++) {


                table += `<table class='table'>
                <tr>
                        <td>Tarikh Pelaburan</td>
                        <td>:</td>
                        <td><b>`+obj[i].payment_date.date.substring(0, 10)+`</b></td>
                </tr>
                <tr>
                        <td>Sehingga</td>
                        <td>:</td>
                        <td><b>`+to+`</b></td>
                </tr>

                <tr>
                        <td>Jumlah Bulan</td>
                        <td>:</td>
                        <td><b>`+obj[i].month+`</b></td>
                </tr>
                <tr>
                        <td>Jumlah Pelaburan</td>
                        <td>:</td>
                        <td><b>RM `+obj[i].amount.toFixed(2)+`</b></td>
                </tr>
                <tr>
                        <td>Dividen / Bulan</td>
                        <td>:</td>
                        <td><b>RM `+obj[i].dividen_per_month.toFixed(2)+`</b></td>
                </tr>
                <tr>
                        <td>Dividen</td>
                        <td>:</td>
                        <td><b style='color:green;'>RM `+obj[i].dividen.toFixed(2)+`</b></td>
                </tr>
                </table><hr />
                `;
            }
            $('#modal_body').html(table);
        }

    </script>

@endsection
