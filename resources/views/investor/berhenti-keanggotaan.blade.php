<div class="modal fade" id="staticBackdrop" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header text-danger">
                <h5 class="modal-title" id="staticBackdropLabel">Permohonan Berhenti Keanggotaan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Lorem, ipsum dolor sit amet consectetur adipisicing elit. Nulla, qui aliquam officiis odio, mollitia ex alias hic beatae distinctio architecto illum assumenda neque ad facilis id. Ex nisi quos quam.
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="button" class="btn btn-primary">Saya Faham, Teruskan</button>
            </div>
        </div>
    </div>
</div>
