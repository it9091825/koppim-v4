@extends('investor.layouts')

@section('navigation')
    @include('investor.navigation',['tab'=>'pelaburan'])
@endsection

@section('content')
    @php
        // determine status display
        if(request()->has('status')){
        $urlstatus = request()->status;
        }
    @endphp
    <div class="slim-mainpanel">
        <div class="container">
            <div class="d-flex justify-content-between align-items-center">
                <div class="slim-pageheader">
                    <div>
                        <ol class="breadcrumb slim-breadcrumb">
                            <li class="breadcrumb-item"><a href="#"></a></li>
                        </ol>
                        <h6 class="slim-pagetitle d-flex align-items-center"><span class="mr-2">TRANSAKSI LANGGANAN SYER SAYA</span>

                        </h6>
                    </div>
                </div>
            </div>
            <div class="row no-gutters" style="margin-bottom: 20px;">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header d-flex align-items-center justify-content-between pd-y-5"
                             style="font-size:14px;">
                            <span
                                class="text-uppercase font-weight-bold text-dark">Rumusan Transaksi Langganan Syer</span>
                            <div class="card-option tx-24">
                                <a target="_blank" href="{{route('investor.transactions.logs.index')}}"
                                   class="btn btn-primary btn-sm text-white">Lihat Log Transaksi Langganan Syer</a>
                                @if(auth()->user()->status == 1)
                                    <a target="_blank" href="{{route('share.buy')}}"
                                       class="btn btn-success btn-sm text-white">Tambah Langganan Syer</a>
                                @endif
                            </div>
                        </div>

                        <div class="card-body p-0">
                            <div class="table-responsive">
                                <table class="table table-bordered mb-0">
                                    <thead>
                                    <tr>
                                        <th style="font-size:14px" class="text-center">fi pendaftaran</th>
                                        <th style="font-size:14px" class="text-center">langganan modal syer</th>
                                        <th style="font-size:14px" class="text-center">langganan syer tambahan</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    <tr>
                                        <td class="text-center"> {{'RM '.number_format($first_time_fees,2,'.',',') ?? 'Tidak Dapat Dikira Buat Masa Ini'}}</td>
                                        <td class="text-center">{{'RM '.number_format($first_time_share,2,'.',',') ?? 'Tidak Dapat Dikira Buat Masa Ini'}}</td>
                                        <td class="text-center">{{'RM '.number_format($additional_share,2,'.',',') ?? 'Tidak Dapat Dikira Buat Masa Ini'}}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="card-footer text-danger text-center">
                            Proses semakan dan pengesahan transaksi langganan syer akan mengambil masa 7-14 hari bekerja dan jumlah yang disemak tertakluk kepada kelulusan pihak KoPPIM.
                        </div>
                    </div>
                </div>

            </div>
            <div class="row no-gutters" style="margin-bottom: 20px;">
                <div class="col-lg-12">
                    <div class="d-flex justify-content-between align-items-center">
                        <div class="d-flex justify-content-center align-items-center mb-3">
                            @if($payments)
                                Paparan {{number_format($payments->firstItem(),0,"",",")}}
                                Hingga {{ number_format($payments->lastItem(),0,"",",")}}
                                Daripada {{number_format($payments->total(),0,"",",")}} Dalam Senarai
                            @endif
                        </div>
                        <div class="d-flex justify-content-center align-items-center">
                            {{ $payments->links() }}
                        </div>
                    </div>
                </div>
                <div style="margin-bottom: 20px;"class="col-sm-12">
                    <div class="card">
                        <div class="card-header d-flex align-items-center justify-content-between pd-y-5"
                             style="font-size:14px;">
                        <span class="text-uppercase font-weight-bold text-dark">Rekod Transaksi Langganan Syer  @if(isset($urlstatus) && $urlstatus)
                                {!! Helper::payment_status_text($urlstatus)!!}
                            @else
                                <badge class="badge badge-info">Semua Transaksi</badge>
                            @endif</span>
                            <div class="card-option">
                                <select name="" id="statusTransaksi" class="form-control select2">
                                    <option value="">Pilih status transaksi</option>
                                    <option value="syer">Semua Transaksi</option>
                                    <option value="payment-completed">Telah Diluluskan</option>
                                    <option value="waiting-for-approval">Untuk Diluluskan</option>
                                    <option value="waiting-for-verification">Untuk Disemak</option>
                                    <option value="verification-cancelled">Pengesahan Gagal</option>
                                </select>
                            </div>
                        </div>
                        <div class="card-body p-0">
                            <div class="table-responsive">
                                <table class="table table-bordered mb-0">
                                    <thead>
                                    <tr>
                                        <th style="font-size:14px" class="text-center">tarikh</th>
                                        <th style="font-size:14px" class="text-center">fi pendaftaran</th>
                                        <th style="font-size:14px" class="text-center">langganan modal syer</th>
                                        <th style="font-size:14px" class="text-center">langganan syer tambahan</th>
                                        <th style="font-size:14px" class="text-center">status</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @forelse($payments as $payment)
                                        <tr>
                                            <td class="text-center">{!! $payment->payment_date ? $payment->payment_date->format("d/m/Y \<\b\\r\> h:i:sa"): '' !!}</td>
                                            <td class="text-center">{{ number_format($payment->first_time_fees,2,'.',',') }}</td>
                                            <td class="text-center">{{ number_format($payment->first_time_share,2,'.',',') }}</td>
                                            <td class="text-center">{{ number_format($payment->additional_share,2,'.',',') }}</td>
                                            <td class="text-center">{!! Helper::payment_status_text($payment->status)!!}</td>
                                        </tr>
                                    @empty
                                        @tablenoresult @endtablenoresult
                                    @endforelse
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-header d-flex align-items-center justify-content-between pd-y-5 py-3"
                             style="font-size:14px;">
                            <span class="text-uppercase font-weight-bold text-dark">Automasi Pembelian Syer Bulanan Bagi Tahun <?= \Carbon\Carbon::today()->year ?></span>
                        </div>
                        <div class="card-body p-0">
                            <div class="table-responsive">
                                <table class="table table-bordered mb-0">
                                    <thead>
                                    <tr>
                                        <th style="font-size:14px" class="text-center">Bulan</th>
                                        <th style="font-size:14px" class="text-center">langganan syer tambahan</th>
                                        <th style="font-size:14px" class="text-center">status</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    @for($i=1; $i<=12; $i++)
                                        <tr>
                                            <td class="text-center">{{ $i }}</td>
                                            @foreach($recurringData as $recurringDatum)
                                                @if($recurringDatum['month'] === $i)
                                                    <td class="text-center">MYR {{ number_format($recurringDatum['total']) }}</td>
                                                @else
                                                    <td class="text-center">Tiada Maklumat</td>
                                                @endif
                                            @endforeach
                                            @if(count($recurringData) === 0)
                                                <td class="text-center">Tiada Maklumat</td>
                                            @endif
                                            <td class="text-center" style="width: 40%;">
                                                @foreach($recurringData as $recurringDatum)
                                                    @if($recurringDatum['month'] === $i)
                                                        <table>
                                                            <thead>
                                                            <tr>
                                                                <th style="font-size:14px" class="text-center">Jumlah Bayaran</th>
                                                                <th style="font-size:14px" class="text-center">Tarikh Bayaran</th>
                                                                <th style="font-size:14px" class="text-center">Status</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                                @foreach($recurringDatum['status'] as $status)
                                                                    <tr>
                                                                        <td>MYR {{ number_format($status['paymentAmount']) }}</td>
                                                                        <td>{{ \Carbon\Carbon::createFromTimestamp($status['paymentDate']['$date']['$numberLong'] / 1000)->toDateString() }}</td>
                                                                        <td>{{ ($status['status'] === 'fail') ? 'Gagal' : 'Berjaya' }}</td>
                                                                    </tr>
                                                                @endforeach
                                                            </tbody>
                                                        </table>
                                                    @else
                                                        Tiada Maklumat
                                                    @endif
                                                @endforeach

                                                @if(count($recurringData) === 0)
                                                    Tiada Maklumat
                                                @endif
                                            </td>
                                        </tr>
                                    @endfor
                                    </tbody>
                                </table>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{-- @maintenance(['description'=>'Laman ini masih dalam pembangunan dan penyelenggaraan. Notifikasi melalui emel akan dihantar selepas pengaktifan laman ini.'])
    @endmaintenance --}}


        </div><!-- container -->
    </div><!-- slim-mainpanel -->

@endsection
@section('js')
    <script>
        $('.select2').select2({
            theme: 'bootstrap4'
        });

        $('#statusTransaksi').on('change', function () {

            var href = $(this).val();
            if (href === 'syer') {

                return location.href = '/syer';
            }
            return location.href = '?status=' + href;
        })

    </script>
@endsection
