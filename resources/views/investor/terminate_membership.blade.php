@extends('investor.layouts')

@section('navigation')

@include('investor.navigation',['tab'=>''])

@endsection

@section('content')
    <div class="slim-mainpanel">
        <div class="container" style="max-width: 800px; min-width: 320px; width: 90%;">
            <div class="slim-pageheader">
                <ol class="breadcrumb slim-breadcrumb">
                    <li class="breadcrumb-item"><a href="#"></a></li>
                </ol>
                <h6 class="slim-pagetitle">Permohonan Berhenti Keanggotaan</h6>
            </div>
            <div class="row">
                <div class="col">
                    <p class="help-block">Berhenti dari menjadi anggota Koperasi Pembangunan Islam Malaysia (KoPPIM)</p>
                </div>
            </div>
            @if(auth()->user()->status == 96)
            <div class="section-wrapper"  >
                <p style="margin-bottom: -10px;">
                    Anda telah memohon untuk berhenti dari KoPPIM. Permohonan anda sedang disemak oleh pihak KoPPIM.
                </p>
            </div>
            @else
            <div class="section-wrapper"  >
                <p style="margin-bottom: -10px;">
                    Sila nyatakan alasan pemberhentian bagi kegunaan pihak pengurusan.
                </p>
                <form action="/permohonan-berhenti" method="post" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="user_id" value="{{auth()->user()->id}}">
                    <input type="hidden" name="name" value="{{auth()->user()->name}}">
                    <div class="row">
                        <div class="col-lg">
                            <div style="margin-top: 20px;"><textarea class="form-control" name="leave_reason" type="text"></textarea></div>
                            <div style="margin-top: 20px;">
                                <p style="margin-bottom: 10px;">Sila muatnaik surat rasmi untuk memohon pemberhentian keanggotaan</p>
                                <input type="file" class="form-control" style="color: black;" name="terminate_letter" id="terminate_letter">
                            </div>
                            <div style="margin-top: 20px;"><button class="btn btn-primary btn-block btn-signin" type="submit" >Hantar Permohonan</button></div>
                        </div>
                    </div>
                </form>
            </div>
            @endif
        </div>
    </div>
@endsection
