@extends('investor.layouts')

@section('navigation')

@include('investor.navigation',['tab'=>''])

@endsection

@section('content')

	<div class="slim-mainpanel" >
      <div class="container" style="max-width: 800px; min-width: 320px; width: 90%;">
        <div class="slim-pageheader">
          <ol class="breadcrumb slim-breadcrumb">
            <li class="breadcrumb-item"><a href="#"></a></li>
          </ol>
          <h6 class="slim-pagetitle">PENGELUARAN DIVIDEN</h6>
        </div><!-- slim-pageheader -->

        <div class="section-wrapper"  >
	     
          
		    <form action="/dividen/withdraw" method="post" enctype="multipart/form-data">
			    @csrf
			    
          <div class="row">
	           
		            <div class="col-lg">
		              
		                <div id="show_bank">
		               <div >
			               
			               <h3 align="center">Baki Dividen : <b>RM {{ number_format($total,2) }}</b></h3>
		               </div>
		             
		                <div style="margin-top: 20px;">
			                <table class="table" >
				                <tr>
					                <td colspan="3" align="center"><b>DIVIDEN AKAN DIKREDITKAN KE AKAUN DIBAWAH</b></td>
				                </tr>
				                <tr>
					                <td align="center">BANK</td>
					                 <td align="center">:</td>
					                <td align="center"><b>{{ Auth::user()->bank_name }}</b></td>
				                </tr>
				                <tr>
					                <td align="center">AKAUN NO.</td>
					                 <td align="center">:</td>
					                <td align="center"><b>{{ Auth::user()->bank_account }}</b></td>
				                </tr>
				                <tr>
					                <td align="center">NAMA AKAUN</td>
					                 <td align="center">:</td>
					                <td align="center"><b>{{ Auth::user()->bank_account_name }}</b></td>
				                </tr>
			                </table>
		                </div>
		                </div>
		                
		                 <div id="show_transafer" style="display: none;">
			                 
			                 
		                 </div>
		                @if($withdraw_bool >= 6)
                        <div>
                            <div style="margin-top: 20px;"><button class="btn btn-primary btn-block btn-signin" type="submit" >Hantar</button></div>
                        </div>
                        @else
                        <div>
                            Akaun anda masih memupunyai <b>{{ 6-$withdraw_bool }} bulan</b> sebelum anda boleh membuat sebarang pengeluaran
                        </div>
                        @endif
		                
		            </div><!-- col -->
            
            		
              
          </div><!-- row -->
		  </form>
		
          
        </div><!-- section-wrapper -->
      </div>
	</div>




@endsection


@section('js')
<script>
	

	</script>
@endsection
