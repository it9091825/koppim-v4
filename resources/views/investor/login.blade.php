@extends('investor.layoutfront')

@section('css')


@endsection

@section('content')

<form action="/auth/login/anggota" method="post" enctype="multipart/form-data">

    {{ csrf_field() }}
    <div align="center">
        <h4 class="signin-title-primary" style="text-transform: uppercase;">SELAMAT DATANG KE PORTAL
            PENGURUSAN AKAUN ANGGOTA KOPPIM
        </h4>
    </div>
    <div class="form-group mb-1">
        <input type="text" class="form-control only-numeric text-center" name="email" placeholder="SILA MASUKKAN NO. MYKAD ANDA" value="" maxlength="12">
    </div><!-- form-group -->
    <div class="form-group mg-b-20">
        <input type="password" class="form-control text-center" name="password" placeholder="SILA MASUKKAN KATA LALUAN ANDA" value="">
    </div><!-- form-group -->


    <div style="margin-top: -20px;text-align: center;font-size:13px;"><small><b>*LOG MASUK KALI PERTAMA?</b> SEMAK EMEL UNTUK KATA LALUAN SEMENTARA.</b></small></div>


    <p class="mg-t-5 text-center"><a href="/dapatkan-kata-laluan">Dapatkan Semula Kata Laluan</a></p>
    <div style="margin-top: 20px;margin-bottom: 20px;">
        <button class="btn btn-primary btn-block btn-signin" style="margin-bottom:5px;">Log Masuk</button>

    </div>

    <p class="mg-b-3 text-center">ATAU</p>
    <p class="mg-b-0 text-center"><a href="/keanggotaan/semak">Semak Status Keanggotaan Anda Dahulu</a></p>
    {{-- <p class="mg-b-2 text-center"><a href="/ikoop-migration">Log Masuk Dari Sistem Pendaftaran Terdahulu</a></p> --}}

    <p class="mg-b-0 text-center">Perlu Bantuan? Ada Pertanyaan? <a href="http://bit.ly/KoPPIM-Pertanyaan"><b class="text-danger">KLIK DISINI!</b></a></p>

</form>


@endsection

@section('js')

<script>
    $(document).ready(function() {
        $(".only-numeric").bind("keypress", function(e) {

            var keyCode = e.which ? e.which : e.keyCode
            if (!(keyCode >= 48 && keyCode <= 57)) {
                $(".error").css("display", "inline");
                return false;
            } else {
                $(".error").css("display", "none");
            }
        });
    });

</script>

@endsection
