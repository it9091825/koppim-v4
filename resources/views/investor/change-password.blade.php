@extends('investor.layouts')

@section('navigation')

@include('investor.navigation',['tab'=>''])

@endsection

@section('content')

	<div class="slim-mainpanel" >
      <div class="container" style="max-width: 800px; min-width: 320px; width: 90%;">
        <div class="slim-pageheader">
          <ol class="breadcrumb slim-breadcrumb">
            <li class="breadcrumb-item"><a href="#"></a></li>
          </ol>
          <h6 class="slim-pagetitle">Tukar Kata Laluan</h6>
        </div><!-- slim-pageheader -->

        <div class="section-wrapper"  >
	      <p style="margin-bottom: -10px;">Tahniah! Anda telah berjaya log masuk ke Portal Pengurusan Anggota KoPPIM. <br>
Sila tukar kata laluan pilihan anda. Jangan kongsi kata laluan dengan pihak lain.</p>
          
		    <form action="/change-password" method="post" enctype="multipart/form-data">
			    @csrf
			    
          <div class="row">
	           
		            <div class="col-lg">
		              
		               <div style="margin-top: 20px;"><input class="form-control" name="new_password" placeholder="Masukkan Kata Laluan Baharu" type="password"></div>
		                <div style="margin-top: 20px;"><input class="form-control" name="new_password_retype" placeholder="Ulang Kata Laluan Baharu" type="password"></div>
		                
		                <div style="margin-top: 20px;"><button class="btn btn-primary btn-block btn-signin" type="submit" >Tukar Kata Laluan</button></div>
		            </div><!-- col -->
            
            		
              
          </div><!-- row -->
		  </form>
		
          
        </div><!-- section-wrapper -->
      </div>
	</div>


@endsection