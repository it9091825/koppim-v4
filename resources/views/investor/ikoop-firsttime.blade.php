@extends('investor.layoutfront')

@section('css')


@endsection

@section('content')
<form action="/ikoop-migration" method="post" enctype="multipart/form-data">
	      @csrf

          <div align="center"><h2 class="signin-title-primary text-uppercase">Log Masuk Dari Sistem Pendaftaran Terdahulu</h2></div>
		  
		   <p align="center" >Sila Masukkan No. MyKad Pada Ruang Di Bawah Sekiranya Anda Pernah Mendaftar Sebagai Anggota KoPPIM Menggunakan Sistem Terdahulu.</p>
		  
          <div class="form-group">
            <input type="text" class="form-control text-center" name="ic" id="ic" placeholder="NO. MYKAD">
          </div><!-- form-group -->
          <button class="btn btn-primary btn-block btn-signin" style="margin-bottom:10px;">Hantar</button>
          
           <p class="mg-b-0 text-center"><a href="/"> Kembali Ke Log Masuk</a></p>
           <p class="mg-b-0 text-center">Perlu Bantuan? Ada Pertanyaan? <a href="http://bit.ly/KoPPIM-Pertanyaan"><b class="text-danger">KLIK DISINI!</b></a></p>
</form>

@endsection

@section('js')
<script>
   $(document).ready(function(){
       $('#phone').mask('00000000000');
       $('#amount_display').mask('00000.00');
       $('#ic').mask('000000000000');
      //$('#myModal').modal();
   });
   
   
   
   
</script>

@endsection