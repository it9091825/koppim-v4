@extends('investor.layoutfront')

@section('css')

@endsection

@section('content')


    <div align="center">
        <h4 class="signin-title-primary" style="text-transform: uppercase;">
            Langganan Syer KoPPIM
        </h4>
    </div>

{{--    <form method="POST" name="dona" action="{{route('share.buy')}}">--}}
        <form method="POST" name="dona" action="/langgan-syer">
        @csrf
        <div class="row" style="margin-bottom: 40px;">

            <div class="col-lg-12">

                <input type="hidden" class="form-control" name="type" id="type" value="one-time">
                {{--                <input type="hidden" class="form-control" name="campaign_id" id="campaign_id" value="2">--}}

                <hr/>

                <p align="center" style="text-transform: uppercase; color: #000000;">
                    SILA ISIKAN MAKLUMAT BERIKUT UNTUK MELANGGAN SYER KOPPIM
                </p>

                <div class="form-group mb-1">
                    <input type="text" class="form-control" id="name" name="name" style="text-transform: uppercase;"
                           aria-describedby="name" placeholder="Nama Penuh (Seperti MyKad)" required
                           value="{{ (Auth::check()) ? Auth::user()->name : old('name') }}" {{auth()->check() ? 'readonly':''}}>
                    @if ($errors->has('name'))
                        <span class="form-text text-danger">
                            <small class="form-text">{{ $errors->first('name') }}</small>
                        </span>
                    @endif
                </div>

                <div class="form-group mb-1">
                    <input type="text" class="form-control ic" id="ic" name="ic" style="text-transform: uppercase;"
                           aria-describedby="ic" placeholder="No. MyKad" required value="{{ (Auth::check()) ? Auth::user()->ic : old('ic') }}" {{auth()->check() ? 'readonly':''}}>
                    @if ($errors->has('ic'))
                        <span class="form-text text-danger"><small class="form-text">{{ $errors->first('ic') }}</small></span>
                    @endif
                </div>

                <div class="form-group mb-1">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <div class="input-group-text" id="btnGroupAddon2"
                                 style="background-color: #ffffff; color: black;">+6
                            </div>
                        </div>
                        <input type="text" class="form-control" value="{{ (Auth::check()) ? Auth::user()->mobile_no : old('phone') }}" placeholder="NO. TELEFON"
                               name="phone" id="phone" aria-describedby="btnGroupAddon2" {{auth()->check() ? 'readonly':''}}>
                    </div>
                </div>

                <div class="form-group mb-1">
                    <input type="email" class="form-control" id="email" name="email" aria-describedby="email"
                           placeholder="ALAMAT EMEL" value="{{ (Auth::check()) ? Auth::user()->email : old('email') }}" {{auth()->check() ? 'readonly':''}}>
                </div>
                @if(!auth()->check())
                <div class="form-group mb-1">
                    <textarea name="mesej" id="mesej" cols="30" rows="3" class="form-control"
                              placeholder="TULISKAN NOTA RINGKAS (SEKIRANYA ADA)" maxlength="180"></textarea>
                    <p id="textarea_feedback"></p>
                </div>
                @endif
                <div align="center" style="margin-top: 10px;">

                    <div style="border:1px solid #929292;padding:10px;text-align:center;margin-bottom:15px;">
                        KAMI MENGGALAKKAN SETIAP ANGGOTA KOPPIM MELANGGAN SYER MINIMA <b>RM1,200.00</b> SETAHUN AGAR
                        KOPPIM DAPAT MELAKSANAKAN <b>PROJEK BERIMPAK TINGGI</b> SEKALIGUS <b>MEMBINA EKOSISTEM BAHARU EKONOMI ISLAM!</b>
                    </div>
                    <h6 class="card-subtitle mb-2 text-muted" style="text-transform: uppercase;">
                        SILA TENTUKAN LANGGANAN SYER ANDA
                    </h6>
                    <h1 id="big_price">RM 100.00</h1>
                    <input type="hidden" class="form-control" name="amount" id="amount" value="100">

                </div>

                {{-- <div class="form-group" style="width: 100%;" align="center">
                    <div class="btn-group" role="group">
                        <button type="button" class="btn btn-secondary" style="width: 150px;"
                                onclick="minusShare()">-RM100.00
                        </button>
                        <button type="button" class="btn btn-primary" style="width: 150px;"
                                onclick="addShare()">+RM100.00
                        </button>
                    </div>

                    <div class="btn-group" role="group" style="margin-top: 1px;">
                        <button type="button" class="btn btn-secondary" style="width: 150px;"
                                onclick="minusShare1000()">-RM1,000.00
                        </button>
                        <button type="button" class="btn btn-primary" style="width: 150px;"
                                onclick="addShare1000()">+RM1,000.00
                        </button>
                    </div>

                    <div class="btn-group" role="group" style="margin-top: 1px;">
                        <button type="button" class="btn btn-secondary" style="width: 150px;"
                                onclick="minusShare5000()">-RM5,000.00
                        </button>
                        <button type="button" class="btn btn-primary" style="width: 150px;"
                                onclick="addShare5000()">+RM5,000.00
                        </button>
                    </div>
                </div> --}}
                <div class="form-group" align="center" margin-bottom:2px;width:100%>
                    <div class="btn-group" role="group">
                        <button type="button" class="btn btn-primary w-b" onclick="addShare()">+RM100.00</button>
                        <button type="button" class="btn btn-primary w-b" onclick="addShare1000()">+RM1,000.00</button>
                        <button type="button" class="btn btn-primary w-b" onclick="addShare5000()">+RM5,000.00</button>
                    </div>
                    <div class="btn-group" role="group">
                        <button type="button" class="btn btn-secondary w-b" onclick="minusShare()">-RM100.00</button>
                        <button type="button" class="btn btn-secondary w-b" onclick="minusShare1000()">-RM1,000.00</button>
                        <button type="button" class="btn btn-secondary w-b" onclick="minusShare5000()">-RM5,000.00</button>
                    </div>

                </div>
                <div class="form-group text-small text-primary text-center " style="font-size:12px">
                    *Fi RM30 Akan Dikenakan Bagi
                    Pendaftaran Anggota Baharu.
                </div>
                <div class="form-group" style="text-align: center">
                    <input type="checkbox" class="" checked name="terms" id="terms">
                    <span>
                        Saya bersetuju dengan <a href="{{ url('terma-syarat') }}">terma & syarat</a> serta bertanggungjawab
                        terhadap maklumat yang diberikan.
                    </span>
                </div>
                <div style="margin-top:50px;text-align:center; margin-top: 20px;">
                    <button type="button" onclick="submit_donation()" id="submitBtn"
                            class="btn btn-primary btn-block btn-signin" style="margin-bottom:10px;">
                        TERUSKAN
                    </button>
                    <p class="mt-2 text-center">
                        Perlu Bantuan? Ada Pertanyaan?
                        <a href="http://bit.ly/KoPPIM-Pertanyaan">
                            <b class="text-danger">KLIK DISINI!</b>
                        </a>
                    </p>
                </div>
            </div>
        </div>

    </form>

@endsection

@section('js')
    <script>
        $(document).ready(function () {

            $("#phone").on("input", function () {
                if (/^[1-9]/.test(this.value)) {
                    this.value = this.value.replace(/^[1-9]/, "")
                }
            })
            $('#phone').mask('00000000000');
            $('#amount_display').mask('00000.00');
            $('#ic').mask('000000000000');
            //$('#myModal').modal();
        });

        var times = 1;
        var times2 = 10;
        var initprice = 100;
        var initprice2 = 1000;
        var initprice3 = 5000;
        var total = 100;

        $('#terms').change(function () {
            $('#submitBtn').attr('disabled', !this.checked);
        });

        function addShare() {

            times = times + 1;

            total = total + initprice;

            $('#amount').val(total);

            $('#big_price').html('RM ' + numberWithCommas(total) + '.00');

        }


        function minusShare() {
            if (total > 100) {
                times = times - 1;
                total = total - initprice;
                if (total <= 100) {
                    total = 100;
                }

                $('#amount').val(total);

                $('#big_price').html('RM ' + numberWithCommas(total) + '.00');
            }


        }


        function addShare1000() {


            times = times + 1;

            total = total + initprice2;

            $('#amount').val(total);

            $('#big_price').html('RM ' + numberWithCommas(total) + '.00');

        }


        function minusShare1000() {
            if (total > 100) {
                times = times - 1;
                total = total - initprice2;
                if (total <= 100) {
                    total = 100;
                }
                $('#amount').val(total);

                $('#big_price').html('RM ' + numberWithCommas(total) + '.00');
            }


        }

        function addShare5000() {

            times = times + 1;

            total = total + initprice3;

            $('#amount').val(total);

            $('#big_price').html('RM ' + numberWithCommas(total) + '.00');

        }


        function minusShare5000() {
            if (total > 100) {
                times = times - 1;
                total = total - initprice3;
                if (total <= 100) {
                    total = 100;
                }
                $('#amount').val(total);

                $('#big_price').html('RM ' + numberWithCommas(total) + '.00');
            }
        }

        function numberWithCommas(x) {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }

        function emailIsValid(email) {
            var re = /^(?:[a-z0-9!#$%&amp;'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&amp;'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])$/;
            return re.test(email);
        }

        $(document).ready(function () {
            var text_max = 180;
            $('#textarea_feedback').html(text_max + ' patah perkataan sahaja');

            $('#mesej').keyup(function () {
                var text_length = $('#mesej').val().length;
                var text_remaining = text_max - text_length;

                $('#textarea_feedback').html('Huruf Berbaki: ' + text_remaining);
            });
        });

        function submit_donation() {

            const Toast = Swal.mixin({
                toast: true,
                position: 'center',
                showConfirmButton: false,
                timer: 3000,
                timerProgressBar: true,
                onOpen: (toast) => {
                    toast.addEventListener('mouseenter', Swal.stopTimer)
                    toast.addEventListener('mouseleave', Swal.resumeTimer)
                }
            });

            var amount = $('#amount').val();
            var name = $('#name').val();
            var ic = $('#ic').val();
            var phone = $('#phone').val();
            var email = $('#email').val();

            var phonelenght = phone.length;

            if (amount < 100 || amount === "") {
                Swal.fire({
                    type: 'error',
                    title: 'Maklumat Tidak Lengkap',
                    confirmButtonColor: '#1b84e7',
                    text: 'Sila Masukkan Jumlah Syer',
                });
            } else if (name === "") {
                Toast.fire({
                    icon: 'error',
                    title: 'RUANGAN NAMA PENUH WAJIB DIISI'
                });
                $('#name').addClass('is-invalid');
            } else if (ic === "") {
                Toast.fire({
                    icon: 'error',
                    title: 'RUANGAN NO. MYKAD WAJIB DIISI'
                });
                $('#ic').addClass('is-invalid');

            } else if (ic.length < 12) {
                Toast.fire({
                    icon: 'error',
                    title: 'NO. MYKAD TIDAK LENGKAP'
                });
                $('#ic').addClass('is-invalid');

            } else if (phone === "") {
                Toast.fire({
                    icon: 'error',
                    title: 'RUANGAN NO. TELEFON WAJIB DIISI'
                });
                $('#phone').addClass('is-invalid');
            } else if (phonelenght < 9) {
                Toast.fire({
                    icon: 'error',
                    title: 'NO. TELEFON TIDAK LENGKAP'
                });
                $('#phone').addClass('is-invalid');
            } else if (email === "") {
                Toast.fire({
                    icon: 'error',
                    title: 'RUANGAN ALAMAT EMEL WAJIB DIISI'
                });
                $('#email').addClass('is-invalid');
            } else if (emailIsValid(email) === false) {
                Toast.fire({
                    icon: 'error',
                    title: 'ALAMAT EMEL TIDAK SAH'
                });
                $('#email').addClass('is-invalid');
            } else {
                document.dona.submit();
            }


        }

    </script>

@endsection
