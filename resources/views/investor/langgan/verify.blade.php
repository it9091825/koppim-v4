@extends('investor.layoutfront')

@section('css')


@endsection

@section('content')

    <div align="center">
        <h4 class="signin-title-primary" style="text-transform: uppercase;">
            PENGESAHAN MAKLUMAT ANGGOTA & LANGGANAN SYER KOPPIM
        </h4>
    </div>
    <hr/>
    @if(!$registered)
        <p align="center" style="text-transform: uppercase; color: #000000;">
            SILA SAHKAN MAKLUMAT SEBELUM MENERUSKAN PERMOHONAN DAN PEMBAYARAN.
        </p>
    @else
        <p align="center" style="text-transform: uppercase; color: #000000;">
            {{strtoupper('NO. MYKAD ANDA TELAH PUN DIDAFTARKAN DIDALAM SISTEM KOPPIM. SILA TERUSKAN PEMBAYARAN BAGI MEMBOLEHKAN PERMOHONAN ANDA DIPROSES.')}}
        </p>
    @endif

    <form method="POST" enctype="multipart/form-data" id="formverify" action="{{ $url }}">
        @csrf
        <div class="form-group mb-1">
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text" id="btnGroupAddon2" style="background-color: white; color: black;">
                        NO. RUJUKAN
                    </div>
                </div>
                <input type="text" class="form-control" value="{{ $payment->order_ref }}" disabled
                       style="color: black;" name="ref_no" id="ref_no" aria-describedby="btnGroupAddon2">
            </div>
        </div>

        <div class="form-group mb-1">
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text" id="btnGroupAddon2" style="background-color: white; color: black;">
                        NAMA
                    </div>
                </div>
                <input type="text" class="form-control" value="{{ $payment->name }}" disabled style="color: black;"
                       name="name" id="name" aria-describedby="btnGroupAddon2">
            </div>
        </div>

        <div class="form-group mb-1">
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text" id="btnGroupAddon2" style="background-color: white; color: black;">
                        NO. MYKAD
                    </div>
                </div>
                <input type="text" class="form-control" value="{{ $payment->ic }}" style="color: black;" disabled
                       name="ic" id="ic" aria-describedby="btnGroupAddon2">
            </div>
        </div>

        <div class="form-group mb-1">
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text" id="btnGroupAddon2" style="background-color: white; color: black;">
                        NO. TEL.
                    </div>
                </div>
                <input type="text" class="form-control" value="+6{{ $payment->phone }}" disabled style="color: black;"
                       name="phone" id="phone" aria-describedby="btnGroupAddon2">
            </div>
        </div>

        <div class="form-group mb-1">
            <div class="input-group">
                <div class="input-group-prepend">
                    <div class="input-group-text" id="btnGroupAddon2" style="background-color: white; color: black;">
                        EMEL
                    </div>
                </div>
                <input type="text" class="form-control" disabled style="color: black;"
                       value="{{ ($payment->email == '') ? '' : $payment->email }}" name="email" id="email"
                       aria-describedby="btnGroupAddon2">

            </div>
            <table class="table" style="margin-top: 20px;">
                            <tr>
                                <td>Langganan Modal Syer</td>
                                <td align="right"><b>RM 0.00</b></td>
                                <input type="hidden" name="" id="infoLanggananModalSyer" value="0.00">
                            </tr>
                            <tr>
                                <td>Langganan Syer</td>
                                <td align="right"><b>RM {{ number_format($payment->amount,2) }}</b></td>
                            </tr>
            </table>

            <div align="center" style="color: black; margin-top: 10px;"><h6>JUMLAH KESELURUHAN</h6></div>
            <div align="center" style="color: black; margin-top: -10px;"><h3>
                    RM {{ number_format($payment->amount, 2, ".", ",") }}</h3></div>
        </div>

        <div>
            <input type="hidden" id="infoOrderId" name="order_ref" value="{{$payment->order_ref}}">
        </div>

        <div style="text-align:center">
                <span>
                  Saya mahu bersama-sama KoPPIM dalam perjuangan <b>Membina Ekosistem Baharu Ekonomi Islam</b> serta bersetuju dengan terma & syarat yang ditetapkan.
                </span>

                <div style="margin-top: 20px;">*Anda boleh log masuk ke akaun anda untuk menyemak langganan anda</div>
        </div>

        <div class="form-group" style="margin-top: 20px;">
            <div class="input-group">
                <select class="form-control" name="payment_method" id="payment_method">
                    <option value="">SILA PILIH KAEDAH PEMBAYARAN</option>
                    <option value="senangpay">PERBANKAN INTERNET</option>
                    @if(!$recurringCheck)
                        <option value="raudhahpay-r">AUTOMASI PEMBELIAN SYER BULANAN</option>
                    @endif
                    <option value="manual">BAYARAN MANUAL (DEPOSIT TUNAI / CEK)</option>
                </select>
            </div>
        </div>

        <div style="margin-top:10px;margin-bottom:10px;text-align:center">
            <button type="button" id="btnSeterusnya" class="btn btn-primary btn-block btn-signin" data-toggle="modal"
                    data-target="#modaldemo3" style="margin-bottom:10px;">SETERUSNYA
            </button>
            <p class="mt-2 text-center">
                Perlu Bantuan? Ada Pertanyaan?
                <a href="http://bit.ly/KoPPIM-Pertanyaan">
                    <b class="text-danger">KLIK DISINI!</b>
                </a>
            </p>
        </div>


    </form>
    <!-- LARGE MODAL -->
    <div id="modaldemo3" class="modal fade">
        <div class="modal-dialog modal-dialog-vertical-center" role="document">
            <div class="modal-content tx-size-sm">
                <div class="modal-header pd-x-20">
                    <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">PENGESAHAN AKHIR</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="alert alert-warning recurring_text" role="alert">
                    <b>Nota Penting:
                        Harap maklum dengan klik pilihan "AUTOMASI PEMBELIAN SYER BULANAN",
                        anda telah bersetuju untuk KoPPIM membuat penolakan automatik bayaran setiap bulan
                        dari akaun bank anda dan akan menerima emel makluman jika pembayaran tidak berjaya.
                    </b>
                </div>
                <div class="modal-body pd-20">
                    <p class=" lh-3 mg-b-20">
                        Sila pastikan maklumat yang diisi adalah tepat sebelum meneruskan pembayaran.
                    </p>

                    <table class="table">
                        <tr>
                            <td>NAMA</td>
                            <td>:</td>
                            <td><b>{{ $payment->name }}</b></td>
                        </tr>
                        <tr>
                            <td>NO. MYKAD</td>
                            <td>:</td>
                            <td><b>{{ $payment->ic }}</b></td>
                        </tr>
                        <tr>
                            <td>NO. TEL</td>
                            <td>:</td>
                            <td><b>6{{ $payment->phone }}</b></td>
                        </tr>
                        <tr>
                            <td>EMEL</td>
                            <td>:</td>
                            <td><b>{{ $payment->email }}</b></td>
                        </tr>

                    </table>
                    <table class="table" style="margin-top: 20px;">
                        @if($payment->first_time_fees > 0 && $payment->share_amount > 0)
                            <tr>
                                <td>Fi Pendaftaran Anggota Baharu</td>
                                <td>:</td>
                                <td><b>RM {{ number_format($payment->first_time_fees,2) }}</b></td>
                            </tr>
                            <tr>
                                <td>Langganan Modal Syer</td>
                                <td>:</td>
                                <td><b>RM 100.00</b></td>
                                <input type="hidden" name="" id="infoLanggananModalSyer" value="100.00">
                            </tr>
                            <tr>
                                <td>Langganan Tambahan</td>
                                <td>:</td>
                                <td><b>RM {{ number_format($payment->share_amount-100,2) }}</b>
                                    <input type="hidden" name="" id="infoLanggananTambahan"
                                           value="{{number_format($payment->share_amount-100,2)}}">

                                </td>
                            </tr>
                        @elseif($payment->first_time_fees > 0)
                            <tr>
                                <td>Fi Pendaftaran Anggota Baharu</td>
                                <td>:</td>
                                <td><b>RM {{ number_format($payment->first_time_fees,2) }}</b></td>
                            </tr>

                        @else
                            <tr>
                                <td>Langganan Modal Syer</td>
                                <td>:</td>
                                <td><b>RM 0.00</b></td>
                                <input type="hidden" name="" id="infoLanggananModalSyer" value="0.00">
                            </tr>
                            <tr>
                                <td>Langganan Syer</td>
                                <td>:</td>
                                <td><b>RM {{ number_format($payment->share_amount,2) }}</b></td>

                            </tr>
                        @endif
                        <tr class="recurring_text">
                            <td>
                                Jumlah Langganan Bulanan
                                <div id="recurring_text">
                                    <p style="font-size: 11px">
                                        <b>*POTONGAN SECARA BULANAN BERMULA BULAN HADAPAN</b>
                                    </p>
                                </div>
                            </td>
                            <td>:</td>
                            <td><b>RM {{ number_format($payment->share_amount,2) }}</b>
                                <input type="hidden" name="" id="infoLanggananModalSyer" value="100.00">
                        </tr>
                        <tr>
                            <td>JUMLAH PEMBAYARAN</td>
                            <td>:</td>
                            <td><b>RM {{ number_format($payment->amount, 2, ".", ",") }} </b></td>
                        </tr>
                    </table>
                    <p class="mg-b-3 manual" style="color:red;display:none" >Sila pastikan pembayaran telah dilakukan
                        sebelum memuatnaik resit ke dalam sistem. Sila
                        daftar semula di
                        link www.anggota.koppim.com.my selepas
                        bayaran langganan syer dilakukan dan muatnaik
                        resit bayaran anda</p>
                    @if(!$registered)
                        <p class="mg-b-5" style="color: red;" align="center">Anda akan menerima <b>Emel & SMS</b> untuk
                            arahan log masuk ke Akaun Pengurusan Anggota KoPPIM selepas proses pembayaran berjaya.<br>
                        </p>
                    @endif
                </div><!-- modal-body -->
                <div class="modal-footer">
                    <button type="button" onclick="changeFormAction()" class="btn btn-primary">TERUSKAN</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">KEMBALI</button>
                </div>
            </div>
        </div><!-- modal-dialog -->
    </div><!-- modal -->
@endsection

@section('js')
    <script>
        $(document).ready(function () {
            $('#phone').mask('00000000000');
            $('#amount_display').mask('00000.00');
            $('#ic').mask('000000000000');
            $('#btnSeterusnya').attr('disabled', 'disabled');
            //$('#myModal').modal();
        });

        $('#payment_method').on('change', function () {

            var val = $(this).val();

            if (val == "raudhahpay-r") {
                $(".recurring_text").show();
                $(".manual").hide();
            }
            else if(val == "manual")
            {
                $(".manual").show();
                $(".recurring_text").hide();
            }
            else
            {
                $(".manual").hide();
                $(".recurring_text").hide();
            }

            $('#btnSeterusnya').prop('disabled', this.value == '');

        });

        function changeFormAction() {
            var url = "{{ $url }}";

            var payment_method = $('#payment_method').val();

            if (payment_method === "manual") {

                window.location.href = "/langganan-syer/pembayaran-manual?order_ref={{ $payment->order_ref }}&name={{$payment->name}}&ic={{$payment->ic}}&phone={{$payment->phone}}&email={{$payment->email}}&total_amount={{ number_format($payment->amount, 2, ".", ",") }}";
            } else if (payment_method === "raudhahpay-r") {
                $("#formverify").prop('action', '{{ $url_recurring }}').submit()

            } else {
                $("#formverify").submit();
            }


        }

        function emailIsValid(email) {
            var re = /^(?:[a-z0-9!#$%&amp;'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&amp;'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])$/;
            return re.test(email);
        }

        function submit_donation() {
            var amount = $('#amount').val();
            var name = $('#name').val();
            var ic = $('#ic').val();
            var phone = $('#phone').val();
            var email = $('#email').val();

            var phonelenght = phone.length;

            if (amount === "0.00" || amount === "") {
                Swal.fire({
                    type: 'error',
                    title: 'Oops...',
                    confirmButtonColor: '#3fac87',
                    text: 'Sila Masukkan Jumlah Sumbangan',
                });

            } else if (name === "") {
                Swal.fire({
                    type: 'error',
                    title: 'Oops...',
                    confirmButtonColor: '#3fac87',
                    text: 'Maklumat Tidak Lengkap',
                });
            } else if (phone === "") {
                Swal.fire({
                    type: 'error',
                    title: 'Oops...',
                    confirmButtonColor: '#3fac87',
                    text: 'Maklumat Tidak Lengkap',
                });
            } else if (phonelenght < 10) {
                Swal.fire({
                    type: 'error',
                    title: 'Oops...',
                    confirmButtonColor: '#3fac87',
                    text: 'No. Telefon anda tidak lengkap',
                });
            } else if (email === "") {
                Swal.fire({
                    type: 'error',
                    title: 'Oops...',
                    confirmButtonColor: '#3fac87',
                    text: 'Maklumat Tidak Lengkap',
                });
            } else if (emailIsValid(email) === false) {
                Swal.fire({
                    type: 'error',
                    title: 'Oops...',
                    confirmButtonColor: '#3fac87',
                    text: 'Emel ' + email + ' Tidak Sah',
                });
            } else {
                document.dona.submit();
            }

        }
    </script>

@endsection
