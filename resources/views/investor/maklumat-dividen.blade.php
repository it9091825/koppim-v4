@extends('investor.layouts')

@section('navigation')

@include('investor.navigation',['tab'=>'penyata'])

@endsection

@section('content')


<div class="slim-mainpanel">
    <div class="container pd-t-50">

       <div class="row mb-4">
    <div class="col">
        <div class="card card-dash-chart-one">
            <div class="row no-gutters">
                <div class="col-lg-12">
                    <div class="left-panel" align="center">
    					<div style="margin-bottom: 30px;"><img src="{{asset('img/logo-koppim.png')}}" width="200" /></div>
                        <div>Dividen akan dikira selepas akaun kewangan KoPPIM ditutup pada 31 Disember 2020 dan selepas akaun kewangan diaudit dan barulah dividen akan diumumkan pada mesyuarat agong KoPPIM yang akan diadakan sebelum Jun 2021.</div>

                    </div><!-- left-panel -->
                </div><!-- col-4 -->

            </div><!-- row -->
        </div>
    </div>
</div>




    </div><!-- container -->
</div><!-- slim-mainpanel -->






@endsection




@section('js')


@endsection


