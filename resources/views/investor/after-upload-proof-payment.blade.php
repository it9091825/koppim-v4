@extends('investor.layoutfront')

@section('css')

@endsection

@section('content')


<div align="center">
    <h4 class="signin-title-primary mt-3 text-success" style="text-transform: uppercase;">PENGHANTARAN BUKTI BAYARAN ANDA TELAH
        BERJAYA. SEMAKAN & PENGESAHAN AKAN DIBUAT DALAM TEMPOH 7-14 HARI WAKTU BEKERJA.
    </h4>
    <div class="mt-5">
        <a href="https://www.koppim.com.my" class="btn btn-default mb-3">Laman Utama KoPPIM</a><br>
        <a href="{{env('APP_URL')}}" class="btn btn-default mb-3">Kembali Ke Log Masuk</a><br>
        <a href="/langganan-syer" class="btn btn-default mb-3">Tambah Langganan Syer</a><br>
        <a href="/hantar-bukti-pembayaran" class="btn btn-default mb-3">Muat Naik Bukti Pembayaran Lain</a>
    </div>
</div>

@endsection

@section('js')
<script>

</script>

@endsection