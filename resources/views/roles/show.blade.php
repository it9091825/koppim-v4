@extends('admin.layouts')
@section('content')
@pagetitle(['title'=>"Edit Role - ".$role->name,'links' => ['Tetapan Admin','Role']])@endpagetitle
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <a href="{{route('roles.index')}}">Kembali</a>
        </div>
    </div>
    <div class="row mt-3">
        <div class="col-md-4">
            <div class="card">
                <div class="card-header bg-white">
                    <h5 class="card-title">{!! $role->name !!}</h5>
                </div>
                <div class="card-body">
                    @can('edit-role')
                    <form action="{{route('roles.update',['id' => $role->id])}}" method="POST" enctype="multipart/form-data">
                        {{csrf_field()}}
                        @endcan
                        <div class="form-group">
                            <label for="" class="form-label">Nama</label>
                            <input type="text" name="name" id="" class="form-control" value="{{old('name', $role->name ?? null)}}">
                        </div>
                        @can('edit-role')
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Kemaskini Role Ini</button>
                        </div>
                    </form>
                    @endcan
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="card">
                <div class="card-header bg-white">

                    <h5 class="card-title">Senarai Permission {{$role->name}} </h5>

                </div>
                <div class="card-body">
                    @can('edit-role')
                    <form action="{{route('roles.permission.store',['role_id' => $role->id])}}" class="" enctype="multipart/form-data" method="POST">
                        {{csrf_field()}}
                        @endcan
                        <div class="form-group">
                            <label for="" class="form-label">Senarai Permission Role Ini</label>
                            <p>
                                @foreach(collect($role->permissions->pluck('name')->toArray()) as $permission)
                                <span class="badge badge-soft-primary">{{$permission}}</span>
                                @endforeach
                            </p>
                        </div>
                        <hr>
                        <div class="form-group">
                            <div class="row mb-3">
                                <div class="col-12">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" id="checkAll" class="custom-control-input">
                                        <label for="checkAll" class="custom-control-label">
                                            Tanda Semua
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                @foreach($permissions as $permission)
                                <div class="col-4">
                                    <div class="custom-control custom-checkbox">
                                        <input type="checkbox" value="{{$permission->id}}" name="permission[]" {{in_array($permission->slug, $role->permissions->pluck('slug')->toArray()) ? 'checked="checked"':''}} class="custom-control-input" id="custom{{$permission->slug}}">
                                        <label for="custom{{$permission->slug}}" class="custom-control-label" data-toggle="tooltip" data-placement="top" title="{!! $permission->description !!}">
                                            {{$permission->name}}
                                        </label>
                                    </div>
                                </div>
                                @endforeach
                            </div>

                        </div>
                        @can('edit-role')
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Simpan Perubahan</button>
                        </div>
                    </form>
                    @endcan
                </div>
            </div>
        </div>
    </div>
</div>
@endsection



@section('js')
<script>
    $("#checkAll").click(function() {
        $('input:checkbox').not(this).prop('checked', this.checked);
    });

</script>
@endsection
