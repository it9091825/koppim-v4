@extends('admin.layouts')
@pagetitle(['title'=>"Tambah Role Baharu",'links' => ['Tetapan Admin']])@endpagetitle
@section('content')
<div class="container">
    <div class="row">
        @can('tambah-role')
        <div class="col">
            <div class="card">
                <div class="card-header bg-white">
                    <h5 class="card-title">Tambah Role Baharu</h5>
                </div>
                <div class="card-body">

                    <form action="{{route('roles.store')}}" method="POST" enctype="multipart/form-data">


                        {{csrf_field()}}
                        <div class="form-group">
                            <label for="" class="form-label">Nama</label>
                            <input type="text" name="name" id="" class="form-control" value="{{old('name', $role->name ?? null)}}">

                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Simpan Perubahan</button>
                        </div>
                    </form>
                </div>
            </div>

        </div>
        @endcan
        @can('lihat-role')
        <div class="col">
            <div class="card card-table">
                <div class="card-header bg-white">
                    <h5 class="card-title">Senarai Role</h5>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Nama</th>
                                    <th class="text-center">Tindakan</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if($roles->count())
                                @foreach($roles as $key=>$role)
                                <tr>
                                    <td>{{$role->name}}</td>
                                    <td class="d-flex justify-content-center align-items-center space-x-2">
                                        @can('lihat-role')
                                        <a href="{{route('roles.edit',['id' => $role->id])}}">EDIT</i>
                                        </a>
                                        @endcan
                                        @can('hapus-role')
                                        <form action="{{route('roles.destroy',['id'=>$role->id])}}" method="POST" class=" deleteRole mr-2">
                                            {{csrf_field()}}
                                            {{method_field('DELETE')}}
                                            <a href="#" class="text-danger">HAPUS</a>

                                        </form>
                                        @endcan


                                    </td>
                                </tr>
                                @endforeach
                                @else
                                <tr>
                                    <td colspan="3" class="text-center">
                                        No role found.
                                    </td>
                                </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
                {{ $roles->links() }}
            </div>
        </div>
        @endcan
    </div>
</div>
@endsection
@section('js')
<script>
    $('.deleteRole').on('click', function(e) {
        e.preventDefault();
        var confirmation = confirm('Continue delete this role? This action is cannot be undo.');

        if (confirmation) {
            this.submit();
        } else {
            alert('Delete operation aborted.');
        }
    });

</script>
@endsection
