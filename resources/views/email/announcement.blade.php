@component('mail::message')
    Assalamualaikum & Salam Sejahtera,

    {!! $announcement->message ?? '' !!}

    Sekian. Terima kasih.

    “Pengguna Pemilik Ekonomi, Bersama Bina Ekosistem Baharu Ekonomi Islam”
@endcomponent
