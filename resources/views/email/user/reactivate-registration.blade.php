@component('mail::message')
Assalamualaikum & Salam Sejahtera {{strtoupper($name ?? '')}},

Terima kasih atas sokongan yang berterusan kepada KoPPIM.

Sukacita dimaklumkan akaun YBhg. Tuan / Puan sebagai Anggota KoPPIM telahpun kembali diaktifkan.

Sila hubungi 03-84081878 atau mesej WhatsApp di http://infokoppim.wasap.my sekiranya YBhg. Tuan / Puan mempunyai sebarang pertanyaan atau maklum balas.

Sekian. Terima kasih.

Untuk mengetahui lebih lanjut berkenaan manfaat KoPPIM, sila klik pautan di bawah:<br>
https://www.koppim.com.my/manfaat-anggota-koppim/

<b><i>“Pengguna Pemilik Ekonomi, Bersama Bina Ekosistem Baharu Ekonomi Islam”</i></b>

@endcomponent
