@component('mail::message')
Assalamualaikum & Salam Sejahtera {{strtoupper($name ?? '')}},

Terima kasih atas sokongan yang berterusan kepada KoPPIM.

Sukacita dimaklumkan bahawa permohonan pendaftaran YBhg. Tuan / Puan sebagai Anggota KoPPIM telahpun diterima dan akan diproses dalam masa 14 hari bekerja.

Kami amat mengalu-alukan penglibatan YBhg. Tuan / Puan dalam usaha membina ekosistem baharu ekonomi Islam.

Berikut adalah maklumat pendaftaran YBhg. Tuan / Puan:

<b>
    Nama: {{$name ?? ''}}<br>
    No. MyKad: {{$ic ?? ''}}<br>
    No. Telefon: {{$mobile_no ?? ''}}<br>
    Emel: {{$email ?? ''}}<br>
    Tarikh & Masa Permohonan: {{$user_created_at ?? ''}}<br>
    Fi Pendaftaran Anggota:	RM 30.00<br>
    Langganan Syer Permulaan: RM {{number_format($share,2,'.',',') ?? 0.00}}<br>
    Langganan Tambahan: RM {{number_format($additionalShare,2,'.',',') ?? 0.00}}<br>
    Jumlah Pembayaran: RM {{number_format($totalAmount,2,'.',',') ?? 0.00}}
</b>

Sila log masuk ke Akaun Anggota KoPPIM anda di https://anggota.koppim.com.my dan kemaskini maklumat peribadi. Kata laluan sementara telahpun dihantar melalui SMS ke no telefon bimbit yang didaftarkan.

Sila hubungi 03-84081878 atau mesej WhatsApp di http://infokoppim.wasap.my sekiranya YBhg. Tuan / Puan mempunyai sebarang pertanyaan atau maklum balas.

Sekian. Terima kasih.

Untuk mengetahui lebih lanjut berkenaan manfaat KoPPIM, sila klik pautan di bawah:<br>
https://www.koppim.com.my/manfaat-anggota-koppim/

<b><i>“Pengguna Pemilik Ekonomi, Bersama Bina Ekosistem Baharu Ekonomi Islam”</i></b>

@endcomponent
