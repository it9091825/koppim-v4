@component('mail::message')
Assalamualaikum & Salam Sejahtera {{strtoupper($name ?? '')}},

Terima kasih atas sokongan yang berterusan kepada KoPPIM.

Tambahan langganan syer terbaharu YBhg. Tuan / Puan telah berjaya. Sila rujuk maklumat berikut untuk pengesahan transaksi.

Berikut adalah maklumat transaksi YBhg. Tuan / Puan:

<b>
    Nama: {{$name ?? ''}}<br>
    Tarikh Transaksi: {{$transaction_date ?? ''}}<br>
    Kaedah Pembayaran: Gerbang Pembayaran Raudhah Pay<br>
    Nombor Rujukan: {{$ref_no ?? ''}} <br>
    Langganan Tambahan: RM{{number_format($amount,2,".",",") ?? ''}}
</b>

Sila log masuk ke Akaun Anggota KoPPIM di https://anggota.koppim.com.my untuk melihat jumlah syer YBhg. Tuan / Puan.

Sila hubungi 03-84081878 atau mesej WhatsApp di http://infokoppim.wasap.my sekiranya YBhg. Tuan / Puan mempunyai sebarang pertanyaan atau maklum balas.

Sekian. Terima kasih.

Untuk mengetahui lebih lanjut berkenaan manfaat KoPPIM, sila klik pautan di bawah:<br>
https://www.koppim.com.my/manfaat-anggota-koppim/

<b><i>“Pengguna Pemilik Ekonomi, Bersama Bina Ekosistem Baharu Ekonomi Islam”</i></b>
@endcomponent
