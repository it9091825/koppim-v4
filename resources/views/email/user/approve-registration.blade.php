@component('mail::message')
Assalamualaikum & Salam Sejahtera {{$name ?? ''}},

Terima kasih atas sokongan yang berterusan kepada KoPPIM.

Tahniah! Permohonan anda telah diluluskan dan akaun anda sudah diaktifkan.

Nombor keanggotaan anda adalah {{$no_koppim ?? ''}}

YBhg. Tuan / Puan boleh menghubungi kami di talian 03-84081878 atau mesej WhatsApp di http://infokoppim.wasap.my sekiranya tiada maklum balas selepas tempoh 7-14 hari waktu bekerja.

Sekian. Terima kasih.

Untuk mengetahui lebih lanjut berkenaan manfaat KoPPIM, sila klik pautan di bawah:<br>
https://www.koppim.com.my/manfaat-anggota-koppim/

<b><i>“Pengguna Pemilik Ekonomi, Bersama Bina Ekosistem Baharu Ekonomi Islam”</i></b>
@endcomponent
