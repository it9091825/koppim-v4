@component('mail::message')
Assalamualaikum & Salam Sejahtera {SITI NURSHAFIQAH BINTI MOHAMAD},

Terima kasih atas sokongan yang berterusan kepada KoPPIM.

Kami telah menerima permintaan YBhg. Tuan / Puan untuk menetapkan semula kata laluan.

Sila klik pautan dibawah untuk menukar kata laluan YBhg. Tuan / Puan:
https://koppim.com.my/tukar-kata-laluan

Sekiranya YBhg. Tuan / Puan tidak membuat permintaan bagi penetapan kata laluan, sila hubungi 03-84081878 atau mesej WhatsApp di http://infokoppim.wasap.my.

Sekian. Terima kasih.

Untuk mengetahui lebih lanjut berkenaan manfaat KoPPIM, sila klik pautan di bawah:<br>
https://www.koppim.com.my/manfaat-anggota-koppim/

<b><i>“Pengguna Pemilik Ekonomi, Bersama Bina Ekosistem Baharu Ekonomi Islam”</i></b>
@endcomponent
