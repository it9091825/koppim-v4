@component('mail::message')
Assalamualaikum & Salam Sejahtera {{$name ?? ''}},

Terima kasih atas sokongan yang berterusan kepada KoPPIM.

Berikut adalah rujukan maklumat kata laluan yang telah YBhg. Tuan / Puan tetapkan:

<b>No. MyKad: {{$ic ?? ''}}<br>
Kata Laluan Baharu: {{$pin ?? ''}}
</b>

Sekiranya YBhg. Tuan / Puan tidak membuat permintaan bagi penetapan kata laluan, sila hubungi 03-84081878 atau mesej WhatsApp di http://infokoppim.wasap.my.

Sekian. Terima kasih.

Untuk mengetahui lebih lanjut berkenaan manfaat KoPPIM, sila klik pautan di bawah:<br>
https://www.koppim.com.my/manfaat-anggota-koppim/

<b><i>“Pengguna Pemilik Ekonomi, Bersama Bina Ekosistem Baharu Ekonomi Islam”</i></b>
@endcomponent
