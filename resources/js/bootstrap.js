window._ = require('lodash');
try {
    window.Popper = require('popper.js').default;
    window.$ = window.jQuery = require('jquery');

    require('bootstrap');
} catch (e) {}

window.axios = require('axios');

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

import "./dashforge.js";
import "./jquery.mask";

import 'jquery-datetimepicker/build/jquery.datetimepicker.min.css';
import "jquery-datetimepicker/build/jquery.datetimepicker.full.min.js";

