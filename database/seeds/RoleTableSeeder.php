<?php

use App\Role;
use App\Permission;
use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
                
        //RoleTableSeeder.php
        $dev_role = new Role();
        $dev_role->slug = 'developer';
        $dev_role->name = 'Developer';
        $dev_role->save(); 
        
        $sadmin_role = new Role();
        $sadmin_role->slug = 'super-admin';
        $sadmin_role->name = 'Super Administrator';
        $sadmin_role->save();

        $admin_role = new Role();
        $admin_role->slug = 'admin';
        $admin_role->name = 'Administrator';
        $admin_role->save();

        $csupport_role = new Role();
        $csupport_role->slug = 'customer-support';
        $csupport_role->name = 'Customer Support';
        $csupport_role->save();

        $hcsupport_role = new Role();
        $hcsupport_role->slug = 'head-customer-support';
        $hcsupport_role->name = 'Head Customer Support';
        $hcsupport_role->save();



    }
}
