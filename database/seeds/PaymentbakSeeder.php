<?php

use App\Mongo\PaymentBak;
use Illuminate\Database\Seeder;
use App\Mongo\Payment as MPayment;

class PaymentbakSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


        //backup data to a new collection
        $payments =
            MPayment::where('channel', 'ikoop')
            ->get();


        //create backup collection
        foreach ($payments as $key => $value) {
            PaymentBak::create([
                'name' =>  $value["name"],
                'ic' => $value["ic"],
                'phone' => $value["phone"],
                'email' => $value["email"],
                'amount' => $value["amount"],
                'share_amount' => $value["share_amount"],
                'first_time_fees' => $value["first_time_fees"],
                'first_time_share' => $value["first_time_share"],
                'additional_share' => $value["additional_share"],
                'payment_desc' => $value["payment_desc"],
                'status' => $value["status"],
                'wcID' => $value["wcID"],
                'ord_key' => '',
                'returncode' => $value["returncode"],
                'channel' => $value["channel"],
                'order_ref' => $value["order_ref"],
                'type' => $value["type"],
                'payment_date' => $value["payment_date"],
                'created_at' => $value["created_at"]
            ]);
            // }
        }
    }
}
