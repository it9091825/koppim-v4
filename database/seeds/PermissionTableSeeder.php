<?php

use App\Role;
use App\Permission;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $administrator = Role::where('slug', 'admin')->first();

        $permissions = [
            [
                'name' => 'Tambah Anggota',
                'description' => 'tambah rekod anggota'
            ],
            [
                'name' => 'Edit Anggota',
                'description' => 'edit rekod anggota'
            ],
            [
                'name' => 'Hapus Anggota',
                'description' => 'hapuskan rekod anggota'
            ],
            [
                'name' => 'Lihat Anggota',
                'description' => 'lihat rekod anggota'
            ],
            [
                'name' => 'Lihat Transaksi',
                'description' => 'lihat rekod trasaksi'
            ],
            [
                'name' => 'Tambah Transaksi',
                'description' => 'tambah rekod trasaksi'
            ],
            [
                'name' => 'Edit Transaksi',
                'description' => 'edit rekod trasaksi'
            ],
            [
                'name' => 'Hapus transaksi',
                'description' => 'hapuskan rekod trasaksi'
            ],
            [
                'name' => 'Lihat Anggota',
                'description' => 'lihat rekod anggota'
            ],
            [
                'name' => 'Edit Mod Penyelenggaraan',
                'description' => 'edit mod penyelenggaraan'
            ],
            [
                'name' => 'Lihat Mesej Pelanggan',
                'description' => 'lihat mesej dari pelanggan'
            ],
            [
                'name' => 'Tambah Mesej Pelanggan',
                'description' => 'tambah mesej dari pelanggan'
            ],
            [
                'name' => 'Edit Mesej Pelanggan',
                'description' => 'edit mesej dari pelanggan'
            ],
            [
                'name' => 'Hapus Mesej Pelanggan',
                'description' => 'hapuskan mesej pelanggan'
            ],
            [
                'name' => 'Tambah Role',
                'description' => 'tambah role'
            ],
            [
                'name' => 'Lihat Role',
                'description' => 'lihat role'
            ],
            [
                'name' => 'Edit Role',
                'description' => 'edit role'
            ],
            [
                'name' => 'Hapus Role',
                'description' => 'hapuskan role'
            ],
            [
                'name' => 'Tambah Permission',
                'description' => 'tambah Permission'
            ],
            [
                'name' => 'Lihat Permission',
                'description' => 'lihat Permission'
            ],
            [
                'name' => 'Edit Permission',
                'description' => 'edit Permission'
            ],
            [
                'name' => 'Hapus Permission',
                'description' => 'hapuskan Permission'
            ],
            [
                'name' => 'Edit No Telefon Anggota',
                'description' => 'edit no. telefon anggota'
            ],
            [
                'name' => 'Edit MYKAD Anggota',
                'description' => 'edit mykad anggota'
            ],


        ];

        foreach ($permissions as $permission) {

            Permission::create([
                'slug' => Str::slug($permission['name']),
                'name' => $permission['name'],
                'description' => $permission['description'],
            ]);
        }
    }
}
