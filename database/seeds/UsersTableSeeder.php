<?php

use App\Role;
use App\User;
use App\Permission;
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'id' => 1,
            'name' => 'Admin KOPPIM',
            'email' => 'admin@koppim.com.my',
            'password' => Hash::make('secret123@'),
            'type' => 'admin',
            'ic' => '000000000000',
            'know_from' => '-'
        ]);

        //Iman punya, live data
        DB::table('users')->insert([
            'id' => 2,
            'no_koppim' => 30000,
            'name' => 'MOHD AL IMAN BIN ZAKARIA',
            'email' => 'al.iman666@gmail.com',
            'password' => Hash::make('123456'),
            'type' => 'user',
            'ic' => '850716025117',
            'mobile_no' => '0162021666',
            'status' => 1,
            'created_at' => '2020-09-03 16:00:09',
            'updated_at' => '2020-09-04 23:03:10',
            'know_from' => '-'
        ]);
        DB::table('payments')->insert([
            'id' => 1,
            'name' => 'MOHD AL IMAN BIN ZAKARIA',
            'email' => 'al.iman666@gmail.com',
            'ic' => '850716025117',
            'phone' => '0162021666',
            'amount' => 130.00,
            'share_amount' => 100.00,
            'first_time_fees' => 30.00,
            'status' => 'payment-completed',
            'order_ref' => '1599119118-37-20-09-03',
            'ord_key' => '1599119879270225332',
            'returncode' => '100',
            'gateway' => 'senangpay',
            'senang_pay_hash' => '7cca9243eb50ccc96c1dea1113fbdb6d4df12cd7e1791d1bab86a176b4bb62ab',
            'type' => 'one-time',
            'campaign_id' => 2,
            'agent_id' => 0,
            'created_at' => '2020-09-03 15:45:18',
            'updated_at' => '2020-09-03 17:02:10'
        ]);
        DB::table('payment_items')->insert([
            'id' => 1,
            'item' => 'Jumlah Syer',
            'amount' => 100.00,
            'order_no' => '1',
            'created_at' => '2020-09-03 15:45:18',
            'updated_at' => '2020-09-03 15:45:18'
        ]);
        DB::table('payment_items')->insert([
            'id' => 2,
            'item' => 'Fi Pendaftaran Anggota',
            'amount' => 30.00,
            'order_no' => '1',
            'created_at' => '2020-09-03 15:45:18',
            'updated_at' => '2020-09-03 15:45:18'
        ]);
        DB::table('payment_gateway_tx')->insert([
            'id' => 1,
            'order_id' => '1599119118-37-20-09-03',
            'confirmation_no' => '1599119879270225332',
            'msg' => 'Payment_was_successful',
            'status' => 1,
            'txn_type' => 'one-time',
            'created_at' => '2020-09-03 16:00:08',
            'updated_at' => '2020-09-03 16:00:08'
        ]);

        $dev_role = Role::where('slug', 'developer')->first();
        $manager_role = Role::where('slug', 'manager')->first();
        $dev_perm = Permission::where('slug', 'create-tasks')->first();
        $manager_perm = Permission::where('slug', 'edit-users')->first();

        // $developer = User::where('email', 'uzzair@datakraf.com')->first();
        // $developer->roles()->attach($dev_role);
        // $developer->permissions()->attach($dev_perm);
    }
}
