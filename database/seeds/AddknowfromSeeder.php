<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class AddknowfromSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function __construct()
    {
        $this->user = DB::table('users')->where('type', 'user');
    }

    public function run()
    {

        $datas = [
            [0 => 'RADIO_ADS', 1 => 'BILLBOARDS', 2 => 'WEBSITE', 3 => 'FACEBOOK', 4 => 'INSTAGRAM', 5 => 'AFFILIATE', 6 => 'OTHERS'],

        ];


        foreach ($datas as $data) {

            shuffle($data);

            $surveys = $this->user->where('type', 'user')->where('know_from', '')
                ->limit(1000)
                ->update(['know_from' => $data[0]]);

            return $surveys;
        }



    }
}

class DeleteknowfromSeeder extends Seeder
{

    public function __construct()
    {
        $this->user = DB::table('users')->where('type', 'user');
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $surveys = $this->user->where('type', 'user')->select('name', 'ic', 'know_from')
        ->update(['know_from' => '']);

        return $surveys;

        // return $data;
    }
}

