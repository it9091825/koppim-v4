<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserArchivesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_archives', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('type')->default('user');
            $table->string('name')->nullable();
            $table->string('email')->nullable();
            $table->string('email_token')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('ic')->nullable();
            $table->string('password')->nullable();
            $table->rememberToken();
            $table->string('home_no')->nullable();
            $table->string('mobile_no')->nullable();
            $table->string('otp')->nullable();
            $table->tinyInteger('status')->default(0);
            $table->string('status_reason')->nullable();
            $table->text('address_line_1')->nullable();
            $table->text('address_line_2')->nullable();
            $table->text('address_line_3')->nullable();
            $table->string('postcode')->nullable();
            $table->string('city')->nullable();
            $table->string('state')->nullable();
            $table->string('parliament')->nullable();
            $table->string('religion')->nullable();
            $table->string('race')->nullable();
            $table->string('sex')->nullable();
            $table->string('marital_status')->nullable();
            $table->dateTime('dob', 0)->nullable();
            $table->string('job')->nullable();
            $table->string('monthly_income')->nullable();
            $table->string('staff_no')->nullable();
            $table->string('witness')->nullable();
            $table->string('heir_name')->nullable();
            $table->string('heir_ic')->nullable();
            $table->string('heir_phone')->nullable();
            $table->string('heir_relationship')->nullable();
            $table->text('avatar')->nullable();
            $table->text('mykad_front')->nullable();
            $table->text('mykad_back')->nullable();
            $table->text('attachment')->nullable();
            $table->text('bank_attachment')->nullable();
            $table->string('bank_name')->nullable();
            $table->string('bank_account')->nullable();
            $table->string('bank_account_name')->nullable();
            $table->tinyInteger('blacklist')->default(0);
            $table->tinyInteger('blacklist_dividend')->default(0);
            $table->unsignedDecimal('total_shares', 13, 2)->default(0.00);
            $table->bigInteger('ikoop_id')->nullable();
            $table->tinyInteger('ikoop')->default(0);
            $table->unsignedInteger('no_koppim')->nullable();
            $table->integer('password_reset')->default(0)->unsigned();
            $table->enum('registration_type', ['online', 'manual', 'ikoop'])->default('online');
            $table->datetime('approved_date')->nullable();
            $table->datetime('registration_date')->nullable();
            $table->string('no_borang')->nullable();
            $table->tinyInteger('cron_flag')->default(0);
            $table->timestamp('cron_datetime')->nullable();
            $table->enum('is_affiliate', ['no', 'yes','apply','reject'])->default('no');
            $table->tinyInteger('dividend_flag')->default(0);
            $table->timestamp('dividend_datetime')->nullable();
            $table->timestamp('affiliate_apply')->nullable();
            $table->timestamp('affiliate_apply_result')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_archives');
    }
}
