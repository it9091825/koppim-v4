<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddJobsAffiliatesToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->tinyInteger('dividend_flag')->default(0);
            $table->timestamp('dividend_datetime')->nullable();
            $table->timestamp('affiliate_apply')->nullable();
            $table->timestamp('affiliate_apply_result')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn(['dividend_flag', 'dividend_datetime','affiliate_apply','affiliate_apply_result']);
        });
    }
}
