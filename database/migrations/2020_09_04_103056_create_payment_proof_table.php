<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentProofTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_proof', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('ic')->index();
            $table->string('mobile_no');
            $table->string('email')->nullable();
            $table->text('payment_proof');
            $table->dateTime('payment_date', 0)->nullable();
            $table->text('note')->nullable();
            $table->string('approval_status')->default('waiting-for-verification');
            $table->unsignedBigInteger('approval_by')->nullable();
            $table->text('approval_note')->nullable();
            $table->dateTime('approval_datetime', 0)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_proof');
    }
}
