<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddYuranAndSyerToUserDuplicatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_duplicates', function (Blueprint $table) {
            $table->unsignedInteger('no_koppim')->nullable()->after('id');
            $table->enum('registration_type', ['online', 'manual', 'ikoop'])->default('online')->after('type');
            $table->datetime('approved_date')->nullable()->change();
            $table->unsignedDecimal('share_amount', 13, 2)->default(0.00);
            $table->unsignedDecimal('first_time_fees', 13, 2)->default(0.00);
            $table->integer('password_reset')->default(0)->unsigned()->after('password');
            $table->string('ikoop_reason')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_duplicates', function (Blueprint $table) {
            $table->dropColumn(['share_amount', 'first_time_fees','ikoop_reason', 'no_koppim','registration_type','approved_date','password_reset']);
        });
    }
}
