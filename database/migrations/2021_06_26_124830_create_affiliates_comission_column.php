<?php

use App\Mongo\Payment;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAffiliatesComissionColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $payments =
            Payment::where('introducer', true)
            ->where('status', 'payment-completed')
            ->where('introducer_commission', '!=', 10)
            ->get();

        foreach ($payments as $payment) {
            $payment->introducer_commission = 10;
            $payment->introducer_commission_status = 'available';
            $payment->save();
        }
        return response()->json($payments);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $payments =
            Payment::where('introducer', true)
            ->where('status', 'payment-completed')
            ->where('introducer_commission', '=', 10)
            ->get();

        foreach ($payments as $payment) {
            $payment->unset("introducer_commission");
            $payment->unset("introducer_commission_status");
        }

        return response()->json($payments);

    }
}

