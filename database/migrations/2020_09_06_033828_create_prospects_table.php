<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProspectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prospects', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('order_ref')->nullable();
            $table->string('name')->nullable();
            $table->string('ic')->nullable();
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->index('ic');
            $table->unsignedDecimal('amount', 13, 2)->nullable()->default(0.00);
            $table->unsignedDecimal('share_amount', 13, 2)->nullable()->default(0.00);
            $table->unsignedDecimal('first_time_fees', 13, 2)->nullable()->default(0.00);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prospects');
    }
}
