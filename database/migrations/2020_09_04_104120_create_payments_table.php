<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('ic')->index();
            $table->string('phone');
            $table->string('email')->nullable();
            $table->unsignedDecimal('amount', 13, 2)->default(0.00);
            $table->unsignedDecimal('share_amount', 13, 2)->default(0.00);
            $table->unsignedDecimal('first_time_fees', 13, 2)->default(0.00);
            $table->string('status');
            $table->string('order_ref')->nullable();
            $table->string('wcID')->nullable();
            $table->string('ord_key')->nullable();
            $table->string('returncode')->nullable();
            $table->string('gateway')->nullable();
            $table->text('senang_pay_hash')->nullable();
            $table->text('payment_proof')->nullable();
            $table->dateTime('uploaded_datetime', 0)->nullable();
            $table->string('type')->default('one-time')->nullable();
            $table->unsignedBigInteger('campaign_id')->default(0)->nullable();
            $table->unsignedBigInteger('agent_id')->default(0)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payments');
    }
}
