<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIkoopsTwoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ikoops_two', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('ikoopId')->nullable();
            $table->string('newIC')->nullable();
            $table->string('cawangan')->nullable();
            $table->string('status')->nullable();
            $table->string('yuran')->nullable();
            $table->string('sahamTerkumpul')->nullable();
            $table->string('imagePath')->nullable();
            $table->string('name')->nullable();
            $table->string('approvedDate')->nullable();
            $table->string('email')->nullable();
            $table->string('dateBirth')->nullable();
            $table->string('job')->nullable();
            $table->string('city')->nullable();
            $table->string('homeNo')->nullable();
            $table->string('postcode')->nullable();
            $table->string('mobileNo')->nullable();
            $table->string('accTabungan')->nullable();
            $table->string('grossPay')->nullable();
            $table->string('staffNo')->nullable();
            $table->string('sex')->nullable();
            $table->string('maritalID')->nullable();
            $table->string('BlackListDIV')->nullable();
            $table->string('totPay')->nullable();
            $table->string('w_name1')->nullable();
            $table->string('w_ic1')->nullable();
            $table->string('w_relation1')->nullable();
            $table->string('saksi1')->nullable();
            $table->string('stateID')->nullable();
            $table->string('bankID')->nullable();
            $table->string('raceID')->nullable();
            $table->string('religionID')->nullable();
            $table->string('created_at')->nullable();
            $table->string('updated_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ikoops_two');
    }
}
